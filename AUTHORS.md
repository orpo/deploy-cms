Deploy CMS was originally developed in 2009 as an in house CMS for Snailwhale LLP and associated companies.

Current contributors are (or have been):

- Anthony Blackshaw
- Nick Snell
- Andy Blackshaw
- John Blackshaw
- Ross Tarran
- Richard Maggs
