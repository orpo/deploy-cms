from setuptools import setup, find_packages

from deploy import __version__

long_description = """Deploy Web Framework"""

setup(
	name='DeployCMS',
	version=__version__,
	description='Deploy Content Management System & Web Framework',
	long_description=long_description,
	author='Anthony Blackshaw & Nick Snell',
	author_email='ant@getme.co.uk, nick@orpo.co.uk',
	url='http://deploycms.com/',
	download_url='http://deploycms.com/',
	license='Private',
	platforms=['Linux',],
	classifiers=[
		'Environment :: Web Environment',
		'Natural Language :: English',
		'Operating System :: OS Independent',
		'Programming Language :: Python',
		'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
	],
	zip_safe=False,
	packages=find_packages(exclude=['tests',]),
	package_data={
		'deploy': [
			'cms/templates/*.html',
			'cms/templates/email/*.txt',
			'media/edit/js/*.js',
			'media/edit/js/core/*.js',
			'media/edit/js/deploy/*.js',
			'media/edit/js/deploy/controllers/*.js',
			'media/edit/js/deploy/gui/*.js',
			'media/edit/js/editable/*.js',
			'media/edit/js/fluid/*.js',
			'media/edit/js/fluid/form/*.js',
			'media/edit/js/fluid/form/fields/*.js',
			'media/edit/themes/sw-theme/*.css',
			'media/edit/themes/sw-theme/*.js',
			'media/edit/themes/sw-theme/*.png',
			'media/edit/themes/sw-theme/img/*.png',
			'templates/*.html',
		]
	},
	entry_points={
		'console_scripts': [
			'deployserver = deploy.bin.server:main',
			'deploysetup = deploy.bin.setup:main',
		]
	},
	install_requires=[
		'webob==1.1.1',
		'storm>=0.18',
		'jinja2>=2.6',
		'pydispatcher',
		'markupsafe',
		'whoosh',
		'BeautifulSoup',
		'html5lib',
		'lxml>=2.2.4',
	]
)
