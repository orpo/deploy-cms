from deploy.shortcuts import redirect_to

def is_ssl(fn, *args, **kwargs):
	"""Decorator to check a request is an SSL one, if not it redirects to the
	SSL (https) version"""

	def check_request_is_ssl(request, *args, **kwargs):
		# Check the request is SSL enabled
		if request.environ.get('UWSGI_SCHEME',
			request.environ.get('HTTP_X_FORWARDED_PROTO', 'HTTP')).upper() == 'HTTP':
			return redirect_to(request.path, ssl=True)

		# Run the view...
		return fn(request, *args, **kwargs)

	return check_request_is_ssl