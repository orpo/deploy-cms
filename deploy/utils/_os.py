"""Deploy utility methods relating to Operating System functionality"""

import os
import os.path
import stat
import datetime
import pwd
import grp

__all__ = ('last_modified', 'get_uid_gid', 'switch_uid_gid', 'get_file_location')

def last_modified(f):
	"""Get the last modified date of a file as a datetime object"""

	if os.path.exists(f):
		epoch_time = os.stat(f)[stat.ST_MTIME]
		return datetime.datetime.fromtimestamp(epoch_time)

	return None

def get_uid_gid(uid, gid=None):
	"""Utility to get the current user and group ID"""
	uid, default_grp = pwd.getpwnam(uid)[2:4]

	if gid is None:
		gid = default_grp
	else:
		try:
			gid = grp.getgrnam(gid)[2]
		except KeyError:
			gid = default_grp

	return uid, gid

def switch_uid_gid(uid, gid=None):
	"""Change the current user (and group). Users should be specified as a 'name'"""

	if not os.geteuid() == 0:
		# We are not root so continue.
		return

	uid, gid = get_uid_gid(uid, gid)

	os.setgid(gid)
	os.setuid(uid)

def get_file_location(filename, path_list):
	"""Return the first instance of a file location from a list of specified paths."""

	for path in path_list:
		file_location = os.path.join(path, filename)
		if os.path.exists(file_location):
			return file_location

	return None
