"""Utilities for dealing with text encodings"""

import datetime
import decimal
import json
import markupsafe
import locale

from deploy.config import settings
from deploy.utils.strings import SafeData

__all__ = ('ExtendedJSONEncoder', 'to_unicode')

class ExtendedJSONEncoder(json.JSONEncoder):
	"""A class that extends the encoding capabilities of JSON for various common
	Python types."""

	def default(self, obj):
		if isinstance(obj, datetime.date):
			return unicode(obj)

		if isinstance(obj, datetime.datetime):
			return unicode(obj)

		if isinstance(obj, decimal.Decimal):
			return float(str(obj))

		#elif isinstance(obj, SafeData):
		#	return unicode(obj)

		#elif isinstance(obj, basestring):
		#	return markupsafe.escape(obj)

		if hasattr(obj, 'to_dict'):
			return obj.to_dict()

		return json.JSONEncoder.default(self, obj)

def to_unicode(text, charset=None, lossy=True):
	"""Convert a `str` object to an `unicode` object.

	If `charset` is not specified, we'll make some guesses,
	first trying the UTF-8 encoding then trying the locale
	preferred encoding (this differs from the `unicode` function
	which only tries with the locale preferred encoding, in 'strict'
	mode).

	If the `lossy` argument is `True`, which is the default, then
	we use the 'replace' mode:

	If the `lossy` argument is `False`, we fallback to the 'iso-8859-15'
	charset in case of an error (encoding a `str` using 'iso-8859-15'
	will always work, as there's one Unicode character for each byte of
	the input).
	"""

	# This fuction has been adapted from a legacy 'trac' utility

	if not isinstance(text, str):
		if isinstance(text, Exception):
			# two possibilities for storing unicode strings in exception data:
			try:
				# custom __str__ method on the exception (e.g. PermissionError)
				return unicode(text)
			except UnicodeError:
				# unicode arguments given to the exception (e.g. parse_date)
				return ' '.join([to_unicode(arg) for arg in text.args])
		return unicode(text)

	errors = lossy and 'replace' or 'strict'

	try:
		if charset:
			return unicode(text, charset, errors)
		else:
			try:
				# Try system specified encoding..
				return unicode(text, settings.ENCODING)
			except UnicodeError:
				return unicode(text, locale.getpreferredencoding(), errors)
	except UnicodeError:
		return unicode(text, 'iso-8859-15')
