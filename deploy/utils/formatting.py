"""A collection of classes & functions for performing various common formatting
tasks."""

import re
import datetime
from HTMLParser import HTMLParser

from deploy.utils.encoding import to_unicode
from deploy.utils.strings import mark_safe

__all__ = ('get_suffix_for_number', 'pretty_date', 'pretty_date_time', 'file_size', 'to_kb', 'to_mb',
'paragraphs', 'abbreviate', 'safe_filename', 'safe_url', 'remove_win_line_endings', 'thousands')

class Stripper(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)
		self.reset()
		self.fed = []

	def handle_data(self, d):
		self.fed.append(d)

	def handle_entityref(self, name):
		self.fed.append('&%s;' % name)

	def handle_charref(self, name):
		self.fed.append('&#%s;' % name)

	def get_data(self):
		return ''.join(self.fed)


def get_suffix_for_number(day):
	"""Utility to get the suffix for a day. e.g. 10th"""

	if not day:
		return ''

	day = int(day)

	assert day > 0 and day < 32, 'You must specifiy a valid day! "%s" specified' % day

	if (day >= 4 and day <= 20) or (day >= 24 and day <= 30):
		suffix = 'th'
	else:
		suffix = ['st', 'nd', 'rd'][day % 10 - 1]

	return suffix

def pretty_date(value):
	"""Format as a readable date"""

	if not value:
		return ''

	# If the value passed in is a string, attempt to parse to
	# a datetime object...
	if isinstance(value, str):
		try:
			value = datetime.datetime.strptime(value, '%a %b %d %H:%M:%S %Y')
		except ValueError, e:
			raise RuntimeError('The string value passed to pretty_date is invalid! %r' % value)

	return '%s%s %s' % (value.day, get_suffix_for_number(value.day), value.strftime('%B %Y'))

def pretty_date_time(value):
	"""Format a datetime value as a readable date and time"""

	if not value:
		return ''

	# If the value passed in is a string, attempt to parse to
	# a datetime object...
	if isinstance(value, str):
		try:
			value = datetime.datetime.strptime(value, '%a %b %d %H:%M:%S %Y')
		except ValueError, e:
			raise RuntimeError('The string value passed to pretty_date_time is invalid! %r' % value)

	day = value.date().strftime('%d')

	return '%s%s %s %s' % (day, get_suffix_for_number(day), value.date().strftime(' %B %Y'), value.time().strftime(' %H:%M'))

def thousands(value):
	"""Convert a value to be separated by thousands using commas"""
	return re.sub(r'([-+]?\d{1,3}(\.\d*)?)(?=(\d{3})*(\.|$))', r',\1', str(value))[1:]

def file_size(value):
	"""Format the byte value size of a file as readable"""

	value = max(1, value)

	if value >= 1048576:
		return to_mb(value)

	return to_kb(value)

def to_kb(value):
	"""Format an integer value (in bytes) as kilobytes"""

	return '%s KB' % re.sub(
		r'([-+]?\d{1,3}(\.\d*)?)(?=(\d{3})*(\.|$))', r',\1',
		str(max(1, int(value / 1024.0)))
	)[1:]

def to_mb(value):
	"""Format an integer value (in bytes) as megabytes"""

	return '%.2f MB' % max(1, value / 1048576.0)

def paragraphs(value):
	"""Format a block of plain text as HTML paragraphs"""

	if not value:
		return ''

	# value = xmlescape(value).replace('\r\n', '\n').replace('\r\n', '\n')
	value = value.replace(
		u'&', u'&amp;'
	).replace(
		u'&amp;amp;', u'&amp;'
	).replace(
		u'\r\n', u'\n'
	).replace(
		u'\r\n', u'\n'
	)

	formatted_value = ''

	for line in value.split(u'\n'):
		if line.strip() != '':
			formatted_value += u'<p>%s</p>' % line

	return mark_safe(formatted_value)

def abbreviate(value, length, suffix='...', strip_html=True):
	"""Abbreviate a value to the specified length"""

	value = to_unicode(value)

	# Strip HTML tags?
	if strip_html:
		s = Stripper()

		try:
			s.feed('<div>%s</div>' % value)
			s.close()
		except HTMLParseError:
			pass
		else:
			value = s.get_data()

	# Do we need to abreviate?
	if length >= len(value):
		return value

	# Abbreviate to the nearest full word
	value = '%s' % value[0:length].rsplit(' ', 1)[0].strip()
	value = re.sub(r'\W+\Z', '', value)

	return '%s%s' % (value, suffix)

def safe_filename(text):
	"""Convert a string into a safe filename and return it"""

	if not text:
		return ''

	text = text.lower()
	text = text.replace(' ', '_')
	text = text.replace('/', '@')

	return text

def safe_url(text):
	"""Convert a string into a safe URL and return it. Adapted from code listed
	on 'http://www.djangosnippets.org/snippets/29/' by santuri."""

	remove_word_list = [
		'a', 'an', 'as', 'at', 'before', 'but', 'by', 'for', 'from', 'is', 'in',
		'into', 'like', 'of', 'off', 'on', 'onto', 'per', 'since', 'than', 'the',
		'this', 'that', 'to', 'up', 'via', 'with'
	]

	url = ''

	for word in remove_word_list:
		url = re.sub(r'\b' + word + r'\b', '', text)

	url = re.sub('[^\w\s-]', '', url).strip().lower()
	url = re.sub('\s+', '-', url)

	return url

# For those who have come from django :)
slugify = safe_url

def remove_win_line_endings(source):
	return source.replace('\r\n', '\n')