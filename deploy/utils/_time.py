import datetime

class GMT0(datetime.tzinfo):

	def utcoffset(self,dt):
		return datetime.timedelta(hours=0, minutes=0)

	def tzname(self,dt):
		return "GMT +0"

	def dst(self,dt):
		return datetime.timedelta(0)