"""Tools for importing Python modules"""

import sys

__all__ = ('import_module', 'import_object', 'get_class_list')

def import_module(name, globals=globals(), locals=locals(), fromlist=None, level=-1):

	if fromlist is None:
		fromlist = []

	__import__(name, globals, locals, fromlist, level)
	return sys.modules[name]

def import_object(module_path):
	dot = module_path.rindex('.')
	module, cls_name = module_path[:dot], module_path[dot+1:]
	cls_module = import_module(module)
	return getattr(cls_module, cls_name)

def get_class_list(module_list, strict=False):
	"""Class Iterator from a list of module/classes paths"""

	for module_path in module_list:
		try:
			yield import_object(module_path)

		except Exception, e:
			if strict:
				raise e
