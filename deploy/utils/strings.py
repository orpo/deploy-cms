"""Utilities for Strings and Unicode Python objects"""

__all__ = ('SafeData', 'SafeString', 'SafeUnicode', 'mark_safe')

class SafeData(object):
	pass
	
class SafeString(str, SafeData):
	def __add__(self, value):
		"""Concatenating a safe string object with another safe string or safe
		unicode object is safe. Otherwise, the result is no longer safe."""
		
		new_value = super(SafeString, self).__add__(value)
		
		if isinstance(value, SafeData):
			return SafeUnicode(value)
			
		return new_value

class SafeUnicode(unicode, SafeData):
	def __add__(self, value):
		"""Concatenating a safe unicode object with another safe string or safe
		unicode object is safe. Otherwise, the result is no longer safe."""
		
		new_value = super(SafeUnicode, self).__add__(value)
		
		if isinstance(value, SafeData):
			return SafeUnicode(value)
			
		return new_value

def mark_safe(value):
	"""Mark a string as safe for (HTML) output purposes. The returned
	object can be used everywhere a string or unicode object is appropriate."""
	
	if isinstance(value, SafeData):
		return value
		
	if isinstance(value, str):
		return SafeString(value)
		
	if isinstance(value, unicode):
		return SafeUnicode(value)
		
	return SafeString(str(value))