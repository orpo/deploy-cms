"""Deploy Plugin manager"""

import os.path

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError
from deploy.core.urls import __resolver__
from deploy.utils.importing import import_module

# The CMS isn't always available
try:
	from deploy.cms import __cms__
except ImportError:
	__cms__ = None

__all__ = ('PluginManager', '__plugins__')

class PluginManager(object):

	# Options to be installed from plugins
	components = ('models', 'forms', 'snippets', 'urls', 'views', 'cms', 'init')

	def __init__(self):
		self._installed_plugins = []

	def install(self, plugins, strict=True):
		"""Install/add-s a plugin into the deploy application"""

		if not isinstance(plugins, (tuple, list)):
			plugins = (plugins,)

		configured_plugins = []

		# Install plugins specified in settings
		for plugin in plugins:

			# Check it's not already installed
			if self.has_plugin(plugin):
				if strict:
					raise ConfigurationError('Unable to load plugin "%s" - It is already installed! (strict checking)' % plugin)
				else:
					continue

			original_settings_state = settings.__getstate__()

			# Attempt to get the plugin settings
			try:
				plugin_settings_module = import_module('%s.settings' % plugin)
			except ImportError, e:
				# Don't attempt to load the plugin if it's settings are unavailable
				if not settings.PLUGIN_FAIL_QUIETLY:
					raise ConfigurationError('Unable to load plugin settings "%s": %s' % (plugin, e))

			else:
				if plugin == 'deploy.cms':
					# Deploycms can never overload local settings as it's a fixed plugin
					settings.load(plugin_settings_module, force=False)

				else:
					# Get the plugin settings and update our Settings with them
					settings.load(plugin_settings_module, force=settings.PLUGIN_SETTINGS_OVERRIDE)

				# This plugin has correctly configured
				configured_plugins.append(plugin)

		# Now load the configured plugins
		for plugin in configured_plugins:
			try:
				plugin_module = import_module(plugin, fromlist=self.components)
			except ImportError, e:
				raise ConfigurationError('Unable to load plugin "%s": %s' % (plugin, e))

			else:
				# Plugin has been imported....
				self._installed_plugins.append(plugin)

				# Update the URL resolver with the plugins configuration
				if hasattr(plugin_module, 'urls'):
					__resolver__.add(plugin_module.urls)

				# Add the plugins template path to the template path list
				plugin_template_path = os.path.join(
					os.path.dirname(os.path.abspath(plugin_module.__file__)),
					'templates'
				)

				if os.path.exists(plugin_template_path):
					settings.TEMPLATE_LOCATIONS.append(plugin_template_path)

				# Run the plugin init code - if it is available
				if hasattr(plugin_module, 'init') and callable(plugin_module.init):
					plugin_module.init()

	def uninstall(self, plugins):
		"""Uninstall/remove-s a plugin from the deploy application"""

		# @@ Todo - uninstall plugin
		pass

	def has_plugin(self, plugin):
		"""Return True if the specified plugin has been installed"""
		return plugin in self._installed_plugins

	def all(self):
		"""Return all the installed plugins (names)"""
		return self._installed_plugins

	def get_plugin(self, plugin_name):
		"""Return a plugin module"""

		if not self.has_plugin(plugin_name):
			return None

		try:
			plugin_module = import_module(plugin_name, fromlist=self.components)
		except ImportError, e:
			return None

		return plugin_module

__plugins__ = PluginManager()