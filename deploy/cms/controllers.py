import cgi
import datetime
import inspect
import json
import os
import os.path
import tempfile
import mimetypes

from storm.expr import Select, Desc, Asc, And, Or

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError, ApplicationException
from deploy.core.forms.fields import HiddenField
from deploy.shortcuts import render_json_page
from deploy.utils.datastructures import SortedDict
from deploy.utils.formatting import file_size, safe_url

from deploy.cms.attachments import AttachmentStoreManager
from deploy.cms.attachments.utils import allow_thumbnail_for_type, create_thumbnail_for_type
from deploy.cms.forms import DefaultCMSAddForm, DefaultCMSUpdateForm, DefaultCMSRemoveForm, \
	DefaultSearchForm
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse, JSONFailResponse
from deploy.cms.pagination import Paginator, InvalidPage, EmptyPage
from deploy.cms.store import get_store
from deploy.cms.utils import is_logged_in, get_controller_name, get_model_attachments, \
	get_tmp_filestore

__all__ = ('CMSController', 'CMS_ALLOWED_PAGINATION_TYPES')

CMS_ALLOWED_PAGINATION_TYPES = ('LIST', 'BOX')

DEFAULT_CONTROLLER_META = {
	'model': None,
	'controller_name': None,
	'pagination_row_id': 'id',
	'pagination_columns': None,
	'pagination_type': 'LIST', # or 'BOX'
	'pagination_order_by': 'id',
	'pagination_order_by_direction': 'DESC',
	'pagination_per_page': None,
	'gui': True,
	'gui_verbose_name': None,
	'gui_verbose_name_plural': None,
	'gui_inpage_editing': False,
	'gui_grouping': None,
	'search_fields': ['id'],
	'forms': {'ADD': None, 'UPDATE': None,},
	'form_field_overides': {},
	'force_overload': False,
}

class CMSControllerBuilder(type):

	def __new__(cls, name, bases, attrs):
		# Check if controller meta data has been specified
		attr_meta = attrs.pop('Meta', None)

		if attr_meta is not None:
			# Controller has meta detail specified, merge this with the default options
			base_meta = DEFAULT_CONTROLLER_META.copy()
			base_meta.update(attr_meta.__dict__)

			attrs['Meta'] = type('Meta', (object,), base_meta)

		else:
			attrs['Meta'] = type('Meta', (object,), DEFAULT_CONTROLLER_META)

		new_class = super(CMSControllerBuilder, cls).__new__(cls, name, bases, attrs)

		return new_class

class CMSController(object):

	__metaclass__ = CMSControllerBuilder

	def __init__(self):
		"""Initialise the controller"""

		if self.Meta.model is None:
			raise ConfigurationError('Controller "%s" does not specify a model!' % (self.__class__.__name__))

		self._controller_name = self.Meta.controller_name or get_controller_name(self.Meta.model)

		# We need to init the model to initialise the values we need
		model = self.Meta.model()

		if not self.Meta.gui_verbose_name:
			self.Meta.gui_verbose_name = model.Meta.verbose_name

		if not self.Meta.gui_verbose_name_plural:
			self.Meta.gui_verbose_name_plural = model.Meta.verbose_name_plural

		# Tidy up, we dont need this anymore
		del model

	@is_logged_in(permission_required='EDITOR')
	def list(self, request, administrator, filter_by=None, filter_by_inclusive=False):
		"""List a given number of objects"""

		if filter_by is not None:
			# @@ FIXME: Should probably chack for any iterateable?
			assert isinstance(filter_by, (list, tuple)), 'Argument "filter_by" must be a list/tuple type!'

		# Get the page for the paginator
		page = 1

		if request.GET.has_key('page'):
			page = request.GET['page']

		# Get the page result count. This is set with the following priority:
		# - GET parameter
		# - User preference
		# - Controller default
		# - Settings default

		# Site default
		count = settings.CMS_PAGINATION_COUNT

		# Controller overide
		if self.Meta.pagination_per_page is not None:
			count = self.Meta.pagination_per_page

		# User Overide
		if administrator.has_preference('CMS_ITEMS_PER_PAGE'):
			count = administrator.get_preference('CMS_ITEMS_PER_PAGE')

		# Request Overide
		if request.GET.has_key('page_count'):
			count = request.GET['page_count']

		# Check the count we have is a number
		try:
			count = int(count)
		except (ValueError, TypeError), e:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid per page count "%s"' % (count),
				'code': 'LIST_INVALID_PER_PAGE_COUNT',
			})

		order_by = self.Meta.pagination_order_by
		order_by_dir = self.Meta.pagination_order_by_direction

		# Look for the order param
		if request.GET.has_key('page_order'):
			order_by = request.GET['page_order']

		if request.GET.has_key('page_order_direction'):
			order_by_dir = request.GET['page_order_direction']

		# Sanity check the order by params
		if not hasattr(self.Meta.model, order_by):
			return JSONErrorResponse(request, args={
				'msg': 'Invalid order by column "%s"' % (order_by),
				'code': 'LIST_INVALID_ORDER_BY',
			})

		if not order_by_dir in ('DESC', 'ASC'):
			return JSONErrorResponse(request, args={
				'msg': 'Invalid order by direction "%s"' % (order_by_dir),
				'code': 'LIST_INVALID_ORDER_BY',
			})

		if order_by_dir == 'DESC':
			order_by_arg = Desc(getattr(self.Meta.model, order_by))
		else:
			order_by_arg = Asc(getattr(self.Meta.model, order_by))

		records = []

		# Check if we have multiple filter components
		if filter_by is not None and len(filter_by) > 1:
			if filter_by and filter_by_inclusive:
				filter_by = And(*filter_by)

			elif filter_by:
				filter_by = Or(*filter_by)

		# Lookup the records...
		if self.Meta.pagination_columns:
			# Select only specified columns
			columns = []

			for column in self.Meta.pagination_columns:
				# Check the model has a column specified
				if column.has_key('name') and hasattr(self.Meta.model, column['name']):
					# Get the model column
					columns.append(getattr(self.Meta.model, column['name']))

			# Do we need to also add the paginator id?
			if not self.Meta.pagination_row_id in [
				column['name'] for column in self.Meta.pagination_columns
				if hasattr(self.Meta.model, column['name'])
			]:
				columns.append(getattr(self.Meta.model, self.Meta.pagination_row_id))

		# Are we filtering the pagination result set?
		if filter_by:
			records = request.db.find(self.Meta.model, filter_by).order_by(order_by_arg)
		else:
			records = request.db.find(self.Meta.model).order_by(order_by_arg)

		# Paginate the results
		record_paginator = Paginator(
			records,
			items_per_page=count,
			columns=self.Meta.pagination_columns,
			row_id=self.Meta.pagination_row_id
		)

		# Get the pagination type
		pagination_type = self.Meta.pagination_type

		# Dont' fall over here, if its not a valid type, just reset to the first type
		if not pagination_type in CMS_ALLOWED_PAGINATION_TYPES:
			pagination_type = CMS_ALLOWED_PAGINATION_TYPES[0]

		try:
			the_page = record_paginator.page(page)
		except (InvalidPage, EmptyPage):
			return JSONFailResponse(request, args={
				'msg': 'Invalid page number!',
				'code': 'LIST_INVALID_PAGE',
			})

		# Return the records
		return JSONSuccessResponse(request, args={
			'page': the_page,
			'page_type': pagination_type,
		})

	@is_logged_in(permission_required='EDITOR')
	def search(self, request, administrator):
		"""General purpose search utility for models. Essentially this is a wrapper around
		the list method, if a GET is made a general search form is retuned, if a POST is sent
		the list method is called with the custom search query."""

		form = DefaultSearchForm(
			action=self._get_url_for_view('search'),
			method='POST'
		)

		query = None

		if request.GET.has_key('query') and request.GET['query']:
			form.populate(request.GET)

			if not form.validate(request=request):
				return JSONFailResponse(request, args={'form': form, 'errors': form.field_errors})

			search_arg = form.validated_data['query']
			#search_option = form.validated_data['option']

			# Define the relationship on the search
			query = [(getattr(self.Meta.model, f).like('%%%s%%' % search_arg))
						for f in self.Meta.search_fields if hasattr(self.Meta.model, f)]

		# Default is to return the search form with all the results
		json_response = self.list(request, filter_by=query)

		# Only add the form if the response from list is successful
		if json_response.response_type == 'success':
			json_response.set_data_arg('search_form', form)

		# Return the altered JSON response
		return json_response

	@is_logged_in(permission_required='EDITOR')
	def add(self, request, administrator):
		"""Default CMS add view. On a GET request the Add form is return,
		on a POST the form is validated and the response returned"""

		# Setup the default form
		form = self._get_form('ADD')

		assert form is not None, 'No ADD form specified for controller %s' % self.__class__.__name__

		form.build(request)

		# Look for any init hook:
		if hasattr(self, 'on_add_init'):
			form = self.on_add_init(request, form)

		if request.POST:
			# Populate the form object with the submitted data
			form.populate(request.POST)

			# Validate the submission
			if not form.validate(request=request):
				# Form has not validated, return the form with it's submitted data
				# along with the validation response
				return JSONFailResponse(request, args={'form': form, 'errors': form.field_errors})

			validated_data = form.get_validated_data()
			attachment_data = {}

			# Check if the model has attachments
			if hasattr(self.Meta.model, '__attachments__'):
				# Get a list of all the attachments for the model
				attachments = get_model_attachments(self.Meta.model)

				# Build a map of attachment keys -> attachment handles
				for key in attachments.keys():
					if key in validated_data:
						attachment_data[key] = validated_data[key]

						# Remove the data from the form
						del validated_data[key]

			# Look for any pre-add hook:
			if hasattr(self, 'on_add'):
				validated_data = self.on_add(request, form, validated_data)

			# Save the form data against the new model
			new_model_instance = self.Meta.model(**validated_data)

			# Add the new record to the database
			request.db.add(new_model_instance)

			# Check if we have attachment data we need to save...
			if hasattr(self.Meta.model, '__attachments__') and attachment_data:
				try:
					self._sync_attachments(request.db, new_model_instance, attachment_data)
				except ApplicationException, e:
					# We can't parse the JSON data, send it back
					return JSONFailResponse(request, args={
						'form': form,
						'errors': {
							key: 'Can not sync attachments - %s' % e,
						}
					})

			# Save the work
			request.db.commit()

			# Look for any post-add hook:
			if hasattr(self, 'after_add'):
				self.after_add(request, validated_data, new_model_instance)

			return JSONSuccessResponse(request, args={
				'msg': 'New %s added' % self.Meta.gui_verbose_name,
				'record_id': new_model_instance.id,
			})

		# Return the Form object
		return JSONSuccessResponse(request, args={
			'form': form,
		})

	@is_logged_in(permission_required='EDITOR')
	def update(self, request, administrator, _id=None):
		"""Default CMS update view. On a GET request the Update form is return,
		on a POST the form is validated and the response returned"""

		if not request.GET.has_key('id') and not request.POST.has_key('id') and not _id:
			return JSONErrorResponse(request, args={
				'msg': 'No id specified',
				'code': 'UPDATE_OBJECT_INVALID_ID',
			})

		# Check an ID has come through
		if _id:
			the_id = _id
		else:
			the_id = request.GET['id'] if request.GET.has_key('id') else request.POST['id']

		# Check we have a valid ID
		try:
			id_ = int(the_id)
		except ValueError:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "id"',
				'code': 'UPDATE_OBJECT_INVALID_ID',
			})

		# Look for the record
		try:
			record = request.db.get(self.Meta.model, id_)
		except Exception:
			return JSONFailResponse(request, args={
				'msg': 'No "%s" with ID %s' % (self.Meta.gui_verbose_name, id_),
				'code': 'UPDATE_OBJECT_NOT_FOUND',
			})

		# Setup the default form
		form = self._get_form('UPDATE')

		assert form is not None, 'No UPDATE form specified for controller %s' % self.__class__.__name__

		form.build(request)

		# Look for any init hook:
		if hasattr(self, 'on_update_init'):
			form = self.on_update_init(request, form)

		# Check the form has an ID field, otherwise add one
		if not form.has_field('id'):
			id_field = HiddenField(name='id', datatype=int)
			form.set_field(id_field)

		form.populate({'id': id_})

		if request.POST:
			# Populate the form object with the submitted data
			form.populate(request.POST)

			# Validate the submission
			if not form.validate(request=request):
				# Form has not validated, return the form with it's submitted data
				# along with the validation response
				return JSONFailResponse(request, args={'form': form, 'errors': form.field_errors})

			validated_data = form.get_validated_data()
			attachment_data = {}

			# Check if the model has attachments
			if hasattr(self.Meta.model, '__attachments__'):
				# Get a list of all the attachments for the model
				attachments = get_model_attachments(self.Meta.model)

				# Build a map of attachment keys -> attachment handles
				for key in attachments.keys():
					if key in validated_data:
						attachment_data[key] = validated_data[key]

						# Remove the data from the form
						del validated_data[key]

			# Look for any pre-update hook:
			if hasattr(self, 'on_update'):
				validated_data = self.on_update(request, form, validated_data, record)

			if validated_data is None:
				return JSONErrorResponse(request, args={
					'msg': 'No validated data for update!',
					'code': 'UPDATE_STOPPED',
				})

			elif not isinstance(validated_data, dict):
				return JSONErrorResponse(request, args={
					'msg': 'Validated data argument must be a dict',
					'code': 'UPDATE_OBJECT_INVALID_ID',
				})

			# Set the record data
			for k, v in validated_data.items():
				setattr(record, k, v)

			# Check if we have attachment data we need to save...
			if hasattr(self.Meta.model, '__attachments__') and attachment_data:
				try:
					self._sync_attachments(request.db, record, attachment_data)
				except ApplicationException, e:
					# We can't parse the JSON data, send it back
					return JSONFailResponse(request, args={
						'form': form,
						'errors': {
							key: 'Can not sync attachments - %s' % e,
						}
					})

			request.db.commit()

			# Look for any post-update hook:
			if hasattr(self, 'after_update'):
				self.after_update(request, validated_data, record)

			return JSONSuccessResponse(request, args={
				'msg': '%s updated' % self.Meta.gui_verbose_name,
				'record_id': record.id,
			})

		# Populate the form with the record values
		form.populate(record.serialize(record))

		# Check if it has attachments as this is a seperate process
		if hasattr(self.Meta.model, '__attachments__'):
			# Get a list of all the attachments for the model
			attachments = get_model_attachments(self.Meta.model)

			# Populate the form attachments
			for key, attachment in attachments.items():
				form.populate({key: getattr(record, key).to_json(request.db)})

		# Return the Form object
		return JSONSuccessResponse(request, args={
			'form': form,
			'_identity': unicode(record)
		})

	@is_logged_in(permission_required='EDITOR')
	def remove(self, request, administrator):
		"""Default CMS remove view"""

		if not request.GET.has_key('id') and not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No if specified',
				'code': 'REMOVE_OBJECT_INVALID_ID',
			})

		the_id = request.GET['id'] if request.GET.has_key('id') else request.POST['id']

		try:
			id_ = int(the_id)
		except ValueError:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "id"',
				'code': 'REMOVE_OBJECT_INVALID_ID',
			})

		# Lookup the record..
		# @@ TODO: Add support for arbitary primary keys as opposed to just id (on remove controller)?
		record = request.db.find(self.Meta.model, self.Meta.model.id == id_).one()

		if not record:
			return JSONFailResponse(request, args={
				'msg': 'No "%s" found with ID %s!' % (self.Meta.gui_verbose_name, id_),
				'code': 'REMOVE_OBJECT_NOT_FOUND',
			})

		if request.POST and request.POST.has_key('confirm'):
			# Look for any pre-remove hook:
			if hasattr(self, 'on_remove'):
				remove = self.on_remove(request, record)

				if not remove:
					return JSONFailResponse(request, args={
						'msg': 'Removed cancelled',
						'code': 'REMOVE_CANCELLED',
					})

			# Remove any attachments from the record
			if hasattr(record, '__attachments__'):
				# Get a list of all the attachments for the model
				attachment_mgr_keys = get_model_attachments(record).keys()

				for key in attachment_mgr_keys:
					attachment_mgr = getattr(record, key)

					# Run a remove on all attachments
					attachment_mgr.clear(request.db)

			# Remove the record
			request.db.remove(record)
			request.db.commit()

			# Look for any post-remove hook:
			if hasattr(self, 'after_remove'):
				self.after_remove(request, record)

			return JSONSuccessResponse(request, args={
				'msg': '%s removed' % self.Meta.gui_verbose_name,
				'record_id': record.id
			})

		return JSONSuccessResponse(request, args={'_identity': unicode(record)})

	@is_logged_in(permission_required='EDITOR')
	def realtime(self, request, administrator):
		"""Realtime interface to forms"""

		# API fields
		# _id = <optional_field_id>
		# _form = ADD | UPDATE
		# _action = SAVE | VALIDATE
		# _name = <field_name>
		# <field_value> =

		if not request.POST:
			return JSONErrorResponse(request, args={'msg': 'You most POST to the realtime interface!'})

		realtime_id, realtime_form, realtime_action, realtime_name, realtime_value = None, None, None, None, None

		# Sanity check of the validation submission
		valid_forms = ['ADD', 'UPDATE']
		valid_actions = ['SAVE', 'VALIDATE']

		if not request.POST.has_key('_form') or not str(request.POST['_form']).upper() in valid_forms:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "_form" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_form = str(request.POST['_form']).upper()

		if not request.POST.has_key('_action') or not str(request.POST['_action']).upper() in valid_actions:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "_action" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_action = str(request.POST['_action']).upper()

		# We can't save on an ADD as we don't yet have a model to save too
		if realtime_form == 'ADD' and realtime_action == 'SAVE':
			return JSONErrorResponse(request, args={
				'msg': 'Unable to realtime save with an ADD form!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# UPDATE require that a model id be present
		if realtime_form == 'UPDATE':
			if not request.POST.has_key('_id') or not request.POST['_id']:
				return JSONErrorResponse(request, args={
					'msg': 'No "_id" specified to realtime UPDATE form',
					'code': 'REALTIME_INVALID_SUBMISSION',
				})

			try:
				realtime_id = int(request.POST['_id'])
			except TypeError:
				return JSONErrorResponse(request, args={
					'msg': '"_id" is not a valid integer!',
					'code': 'REALTIME_INVALID_SUBMISSION',
				})

		if not request.POST.has_key('_name'):
			return JSONErrorResponse(request, args={
				'msg': 'No "_name" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_name = request.POST['_name']

		if not request.POST.has_key(realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'No "value" specified to realtime validation!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Get the form
		form = self._get_form(realtime_form)

		assert form is not None, 'No %s form specified for controller %s' % (
			realtime_form,
			self.__class__.__name__
		)

		if not form.has_field(realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'Form has no field "%s"' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Check the form has an ID field, otherwise add one
		if realtime_form == 'UPDATE':
			if not form.has_field('id'):
				id_field = HiddenField(name='id', datatype=int)
				form.set_field(id_field)

			# Populate the ID
			form.populate({'id': realtime_id})

		# Get the form field
		field = form.get_field(realtime_name)

		# Build the field
		field.build(request)

		# Check the field is a realtime field
		if not field.realtime:
			return JSONErrorResponse(request, args={
				'msg': 'Field "%s" does not support realtime!' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Get the value (cater for multiple values)
		if hasattr(field, '_multiple') and getattr(field, '_multiple'):
			realtime_value = request.POST.getall(realtime_name)
		else:
			realtime_value = request.POST[realtime_name]

		# Set the field field value
		form.populate({field.name: realtime_value})

		# We always perform a validation even if we are saving...

		# @@ TODO: This call to validate should be passing form also, we also may need to populate additional form fields?

		if not field.validate(request=request, form=form):
			return JSONFailResponse(request, args={
				'type': 'REALTIME',
				'code': 'INVALID_FIELD',
				'field': field.name,
				'msg': field.error,
			})

		# Check for linked fields
		linked_fields = []

		if form.is_linked_field(field.name):
			linked_fields = form.get_linked_fields(field.name)

			linked_field_populate = {}

			for linked_field_name in linked_fields:
				if request.POST.has_key(linked_field_name):
					# Populate the form field
					form.populate({linked_field_name: request.POST[linked_field_name]})
					linked_field = form.get_field(linked_field_name)

					# Validate that linked field
					if not linked_field.validate(request=request):
						return JSONFailResponse(request, args={
							'type': 'REALTIME',
							'code': 'INVALID_FIELD',
							'field': linked_field.name,
							'msg': linked_field.error,
						})

		# Check if we are just validating as we return here
		if realtime_action == 'VALIDATE':
			return JSONSuccessResponse(request, args={
				'type': 'REALTIME',
				'code': 'VALID',
				'field': field.name,
				'clean': field.value,
			})

		# We are performing a save so we need to lookup the model instance we are saving too
		record = request.db.find(self.Meta.model, self.Meta.model.id == realtime_id).one()

		if not record:
			return JSONFailResponse(request, args={
				'msg': 'No "%s" found with ID %s!' % (self.Meta.gui_verbose_name, realtime_id),
				'code': 'REALTIME_OBJECT_NOT_FOUND',
			})

		if not hasattr(record, realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'Model has no field "%s" - can not save' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Populate the field with the new value
		value = field.prepare(form=self)

		# Check for any pre-realtime save hooks
		if hasattr(self, 'on_realtime_save'):
			value = self.on_realtime_save(request, record, realtime_name, value)

		# Is the field a realtime attachment field or not?
		if hasattr(self.Meta.model, '__attachments__') and realtime_name in get_model_attachments(self.Meta.model).keys():
			try:
				value = self._sync_attachments(request.db, record, {realtime_name: realtime_value})
			except ApplicationException, e:
				# We can't parse the JSON data, send it back
				return JSONFailResponse(request, args={
					'form': form,
					'errors': {
						key: 'Can not sync attachments - %s' % e,
					}
				})

		else:
			# Update the realtime value
			try:
				setattr(record, realtime_name, value)
			except Exception, e:
				return JSONErrorResponse(request, args={
					'msg': 'Unable to set value in realtime! %s' % e,
					'code': 'REALTIME_FAILED',
				})

		# Update any linked field values
		if linked_fields:
			for linked_field_name in linked_fields:
				linked_field = form.get_field(linked_field_name)

				try:
					setattr(record, linked_field.name, linked_field.prepare(form=self))
				except Exception, e:
					return JSONErrorResponse(request, args={
						'msg': 'Unable to set value for "%s" in realtime! %s' % (linked_field.name, e),
						'code': 'REALTIME_FAILED',
					})

		# Commit the changes
		request.db.commit()

		# Check for any pre-realtime save hooks
		if hasattr(self, 'after_realtime_save'):
			value = self.after_realtime_save(request, record, realtime_name, value)

		return JSONSuccessResponse(request, args={
			'type': 'REALTIME',
			'code': 'SAVED',
			'field': field.name,
			'clean': value,
		})

	@is_logged_in(permission_required='EDITOR')
	def upload(self, request, administrator):
		"""Upload a file to the system"""

		# This is a workaround for a problem with Internet Explorer
		# receving application/json back to iframes.
		content_type_fix = 'application/json'

		if not request.GET.has_key('qqfile'):
			content_type_fix = 'text/html'

		def make_file_name_good(file_name):
			# Split the path to remove the extension, then make safe and rebuild
			basename, extension = os.path.splitext(file_name)
			basename = safe_url(basename)
			return basename + extension

		# Sanity checking
		if not request.GET.has_key('_name'):
			return JSONErrorResponse(request, args={
				'msg': 'No "name" specified to upload!',
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		try:
			file_key = str(request.GET['_name'])
		except Exception:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid name sent to upload!',
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		file_name = None
		file_object = None

		# Is this a XHR upload or a standard CGI upload?
		if request.GET.has_key('qqfile'):
			file_name = request.GET['qqfile']

			if file_name == '':
				return JSONErrorResponse(request, args={
					'msg': 'File name is not valid!',
					'code': 'UPLOAD_INVALID_SUBMISSION',
				}, content_type=content_type_fix)

			# Make the name safe
			file_name = make_file_name_good(file_name)

			file_object = request.environ['wsgi.input']

		elif request.POST:
			# NOTE: Due to an issue with webob - we can not safty check the existance
			# of the qqfile key in the POST dictionary

			# This is a 'standard' CGI upload - which is currently only
			# used by Internet explorer
			file_name = request.POST['qqfile'].filename

			if file_name == '':
				return JSONErrorResponse(request, args={
					'msg': 'File name is not valid!',
					'code': 'UPLOAD_INVALID_SUBMISSION',
				}, content_type=content_type_fix)

			# Make the name safe
			file_name = make_file_name_good(file_name)

			file_object = request.POST['qqfile'].file

		else:
			# No upload...
			return JSONErrorResponse(request, args={
				'msg': 'No upload information!',
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		if not file_object:
			return JSONErrorResponse(request, args={
				'msg': 'File does not exist!',
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		# Check the model this file is being uploaded against, firstly has the specified
		# key and secondly will except this file
		if not hasattr(self.Meta.model, file_key):
			return JSONErrorResponse(request, args={
				'msg': 'This object does not except a "%s" attachment!' % file_key,
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		attachments = getattr(self.Meta.model, file_key)

		if not isinstance(attachments, AttachmentStoreManager):
			return JSONErrorResponse(request, args={
				'msg': 'Configuration error %s is not an attachment field' % file_key,
				'code': 'UPLOAD_CONFIG_ERROR',
			}, content_type=content_type_fix)

		# Attempt to get the file contents
		file_contents = ''

		try:
			file_contents = file_object.read()
		except IOError, e:
			return JSONErrorResponse(request, args={
				'msg': 'Unable to read file: %s' % e,
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		# Look for any pre-upload hook:
		if hasattr(self, 'on_upload'):
			save = self.on_upload(file_contents)

			if not save:
				return JSONFailResponse(request, args={
					'msg': 'Unable to upload.',
					'code': 'UPLOAD_CANCELLED',
				}, content_type=content_type_fix)

		# Get the store interface
		file_store = get_store()

		# Get the random file name
		tmp_file_path = file_store.get_tmp_name(
			prefix='upload-',
			suffix='-%s' % file_name
		)

		try:
			file_store.save(tmp_file_path, file_contents)
		except Exception, e:
			return JSONErrorResponse(request, args={
				'msg': 'Unable to save file: %s' % e,
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		# Get the file information
		file_type, file_encoding = mimetypes.guess_type(tmp_file_path, strict=False)
		size = file_store.size(tmp_file_path)

		# Attachment sanity check
		if attachments.allowed_mimetypes is not None and file_type not in attachments.allowed_mimetypes:
			return JSONFailResponse(request, args={
				'msg': 'Attachment of the type "%s" is not allowed.' % file_type,
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		if attachments.max_size is not None and size > attachments.max_size:
			return JSONFailResponse(request, args={
				'msg': 'Attachment is too large! "%s" must be "%s" maximum' % (
					format_file_size(size),
					format_file_size(attachments.max_size),
				),
				'code': 'UPLOAD_INVALID_SUBMISSION',
			}, content_type=content_type_fix)

		args = {
			'code': 'UPLOADED',
			'field': file_key,
			'path': tmp_file_path,
			'expected_meta': attachments.get_expected_meta_for_type(file_type),
			'meta': {
				'encoding': file_encoding,
				'type': file_type,
				'size': size,
				'size_mb': file_size(size),
				'original_filename': file_name,
			}
		}

		# Check thumbnails are turned on and this filetype allows for thumbnails
		# to be created.
		if settings.CMS_ATTACHMENT_THUMBNAIL and allow_thumbnail_for_type(file_type):
			# Attempt to generate a thumbnail
			thumb = create_thumbnail_for_type(file_type, tmp_file_path)

			# Only save a path if we got a thumbnail back, otherwise fall through
			if thumb is not None:
				# Return a full path here to allow for multiple storage backends
				# to work.
				# @@ TODO: Should this be derived from somewhere rather than building manually?
				args['thumbnail_path'] = os.path.join(
					settings.CMS_ATTACHMENT_BASE_URL,
					thumb
				)

		# Look for any post-upload hook:
		if hasattr(self, 'after_upload'):
			self.after_upload(args)

		return JSONSuccessResponse(request, args=args, content_type=content_type_fix)

	@is_logged_in(permission_required='EDITOR')
	def get_id_for_url(self, request, administrator):
		""""Get an object ID for a given URL. It is up to the controller to decide
		how it may resolve a URL to an object ID. By default this function returns
		a Fail response, which is also what it should return if no objet could be found."""

		return JSONFailResponse(request, args={
			'msg': 'Object could not be found for URL.',
			'code': 'OBJECT_NOT_FOUND'
		})

	@is_logged_in(permission_required='EDITOR')
	def get_url_for_id(self, request, administrator):
		"""Get an URL for a given object"""

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND'
			})

		# By default we look for an complete_url attribute against the record
		# if we don't find it we return a fail.
		if not hasattr(record, 'complete_url'):
			return JSONFailResponse(request, args={
				'msg': 'URL could not be resolved for object.',
				'code': 'URL_NOT_AVAILABLE'
			})

		return JSONSuccessResponse(request, args={'msg': 'Got URL.', 'url': record.complete_url})

	def _get_available_methods(self, with_permissions=True):
		"""Get available methods for the controller, and also the permission required
			to call that method"""

		# with_permissions = False - Returns [method]
		# with_permissions = True  - Returns [(method, permission)]

		# Get all the methods
		methods = [
			method for method in dir(self) if not method.startswith('_')
			and callable(getattr(self, method))
			and not inspect.isclass(getattr(self, method))
		]

		if with_permissions:
			method_permission_list = []

			# Get the permission for each method
			for method in methods:
				method_function = getattr(self, method)
				permission = None

				if hasattr(method_function, 'permission_required'):
					permission = method_function.permission_required

				method_permission_list.append((method.replace('-', '_'), permission))

			return method_permission_list

		return methods

	def _get_url_for_view(self, view):
		prefix = settings.CMS_URL_PREFIX

		if prefix.startswith('/'):
			prefix = prefix[1:]

		if prefix.endswith('/'):
			prefix = prefix[:-1]

		return '/%s/%s/%s/' % (prefix, self._controller_name, view)

	def _has_form(self, form_type):
		"""Check if the forms Meta configuration specifies a specifc
		form type"""
		return self.Meta.forms.has_key(form_type)

	def _get_form(self, form_type, *args, **kwargs):
		"""Private fucntion - fetch a form specified in the controllers
		Meta information. Use default forms if no form is available"""

		form = None

		# Is there a form?
		if not self.Meta.forms.has_key(form_type):
			# Add the default form (which builds from the model)
			form = DefaultCMSAddForm(
				model=self.Meta.model,
				action=self._get_url_for_view(form_type.lower()),
				method='POST',
				field_overides=self.Meta.form_field_overides,
				*args,
				**kwargs
			)

		else:
			# Initialise Form
			form = self.Meta.forms[form_type](
				action=self._get_url_for_view(form_type.lower()),
				method='POST',
				*args,
				**kwargs
			)

		return form

	def _sync_attachments(self, db, record, attachment_data):
		"""Sync the attachment data"""

		# Get a list of all the attachments for the model
		attachment_keys = get_model_attachments(self.Meta.model).keys()

		attachment_values = SortedDict()

		for key in attachment_keys:
			if attachment_data.has_key(key):
				# Get a handle to the attachment store
				attachment_mgr = getattr(record, key)

				# Setup a placeholder for the detail (as we may end up passing an
				# empty response to sync)
				attachment_detail = SortedDict()

				# Check we have a value for the attachment JSON
				if attachment_data[key]:
					# Parse the JSON for the attachment
					try:
						attachments_json = json.loads(attachment_data[key])
					except Exception, e:
						raise ApplicationException('Can not parse JSON data %s' % e)

					# Get the attachment definitions from the JSON
					attachment_detail = attachment_mgr.from_json(attachments_json)

				# Run an update on the attachments
				attachment_mgr.sync(db, attachment_detail)

				attachment_values[key] = attachment_mgr.to_json(db)

		return attachment_values