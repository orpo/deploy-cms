import os
import os.path
import shutil

from deploy.config import settings
from deploy.core.forms import Form, ModelFormFactory, fields
from deploy.cms.attachments import Attachments
from deploy.cms.utils import filepath_generator

class AttachmentField(fields.Field):
	"""An attachment input Field."""

	def __init__(self, label='', value=u'', attachment=None, *args, **kwargs):
		self.attachment = attachment

		assert self.attachment is not None, 'You must specify an attachment to attachment field!'

		super(AttachmentField, self).__init__(label=label, value=value, **kwargs)

	def __deepcopy__(self, memo):
		# @@ TODO: Add explanation regarding deepcopy here (http://bugs.python.org/issue1515)

		args = {
			'type': self._type,
			'id': self._id,
			'value': self._value,
			'name': self._name,
			'label': self.label,
			'attribs': self.attribs,
			'help': self.help,
			'placeholder': self.placeholder,
			'validators':  self.validators,
			'attachment': self.attachment,
		}

		return self.__class__(**args)

	def get_value(self, default=None):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		if v is not None:
			self._value = v

	value = property(get_value, set_value)

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = super(AttachmentField, self).to_dict()

		# Add in attachment options
		field['attachment_allow'] = self.attachment.allowed_mimetypes
		field['attachment_max'] = self.attachment.max
		field['attachment_max_size'] = self.attachment.max_size

		return field