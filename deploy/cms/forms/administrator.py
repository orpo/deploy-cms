from deploy.config import settings
from deploy.core import forms
from deploy.core.forms import fields
from deploy.core.forms.validators import EmailValidator, RequiredValidator,\
	ModelUniqueValidator, MaxLengthValidator, MinLengthValidator
from deploy.tools.users import encrypt_password

from deploy.cms.models.administrator import DeployAdministrator
from deploy.cms.utils import storm_emun_options
from deploy.utils.strings import mark_safe

__all__ = (
	'DeployAdministratorSigninForm',
	'DeployAdministratorAddForm',
	'DeployAdministratorUpdateForm',
	'DeployAdministratorManageMyAccountForm')

class DeployAdministratorSigninForm(forms.Form):

	username = fields.EmailField(
		'Email',
		placeholder='me@example.com',
		autofocus=True,
		realtime=False,
		validators=[RequiredValidator(), EmailValidator()],
		classes=['fluid-split']
	)
	password = fields.PasswordField(
		'Password',
		realtime=False,
		classes=['fluid-split', 'fluid-last']
	)

class DeployAdministratorForgottenForm(forms.Form):
	username = fields.EmailField(
		'Email',
		placeholder='me@example.com',
		help='''
Once you enter your email address and click the reset button your new password
will be emailed to you.
			''',
		autofocus=True,
		realtime=False,
		validators=[RequiredValidator(), EmailValidator()],
		classes=['fluid-split']
	)

class DeployAdministratorAddForm(forms.Form):

	active = fields.BooleanField('Is Active?',
		default=True,
		help=mark_safe('''
An administrator must be <strong>active</strong> to edit the website, their
<strong>role</strong> determines what they can access and change.
		'''),
		classes=['fluid-split']
		)
	role = fields.SelectField('Role',
		options=storm_emun_options(settings.CMS_PERMISSIONS),
		classes=['fluid-split', 'fluid-last']
		)

	first_name = fields.TextField('First Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split']
		)
	last_name = fields.TextField('Last Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split', 'fluid-last']
		)

	email = fields.EmailField(
		'Email',
		placeholder='me@example.com',
		validators=[
			RequiredValidator(),
			EmailValidator(),
            MaxLengthValidator(64),
			ModelUniqueValidator(DeployAdministrator),
		],
		classes=['fluid-split']
		)
	password = fields.PasswordField('Password',
		prepare=encrypt_password,
		validators=[
            RequiredValidator(),
            MinLengthValidator(6),
            MaxLengthValidator(20)
            ],
		placeholder='Min. 6 characters',
		classes=['fluid-split', 'fluid-last']
		)

	fieldsets = [
		fields.Fieldset('Status', ['active', 'role']),
		fields.Fieldset('ID', ['first_name', 'last_name', 'email', 'password'])
		]

class DeployAdministratorUpdateForm(forms.Form):

	id = fields.HiddenField(datatype=int)
	active = fields.BooleanField('Is Active?',
		default=True,
		help=mark_safe('''
An administrator must be <strong>active</strong> to edit the website, their
<strong>role</strong> determines what they can access and change.
		'''),
		classes=['fluid-split']
		)
	role = fields.SelectField('Role',
		options=storm_emun_options(settings.CMS_PERMISSIONS),
		classes=['fluid-split', 'fluid-last']
		)

	first_name = fields.TextField('First Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split']
		)
	last_name = fields.TextField('Last Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split', 'fluid-last']
		)

	email = fields.EmailField(
		'Email',
		placeholder='me@example.com',
		validators=[
			RequiredValidator(),
			EmailValidator(),
            MaxLengthValidator(64),
			ModelUniqueValidator(DeployAdministrator),
		],
		classes=['fluid-split']
		)
	password = fields.PasswordField('Password',
		prepare=encrypt_password,
		validators=[
            RequiredValidator(),
            MinLengthValidator(6),
            MaxLengthValidator(20)
            ],
		placeholder='Min. 6 characters',
		classes=['fluid-split', 'fluid-last']
		)

	fieldsets = [
		fields.Fieldset('Status', ['id', 'active', 'role']),
		fields.Fieldset('ID', ['first_name', 'last_name', 'email', 'password'])
		]

class DeployAdministratorManageMyAccountForm(forms.Form):

	id = fields.HiddenField(datatype=int)

	first_name = fields.TextField('First Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split']
		)
	last_name = fields.TextField('Last Name',
		validators=[RequiredValidator(), MaxLengthValidator(32)],
		classes=['fluid-split', 'fluid-last']
		)

	email = fields.EmailField(
		'Email',
		placeholder='me@example.com',
		validators=[
			RequiredValidator(),
			EmailValidator(),
            MaxLengthValidator(64),
			ModelUniqueValidator(DeployAdministrator),
		],
		classes=['fluid-split']
		)
	password = fields.PasswordField('Password',
		prepare=encrypt_password,
		validators=[
            RequiredValidator(),
            MinLengthValidator(6),
            MaxLengthValidator(20)
            ],
		placeholder='Min. 6 characters',
		classes=['fluid-split', 'fluid-last']
		)

	fieldsets = [
		fields.Fieldset('ID', ['id', 'first_name', 'last_name', 'email', 'password'])
		]
