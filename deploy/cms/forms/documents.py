from deploy.config import settings
from deploy.core import forms
from deploy.core.forms import fields
from deploy.cms.forms.fields import AttachmentField

from deploy.cms.models.documents import DeployDocument

class DeployDocumentAddUpdateForm(forms.Form):

	title = fields.TextField('Title')
	description = fields.TextareaField('Description', validators=[])
	file = AttachmentField(
		'File',
		attachment=DeployDocument.file,
		validators=[],
		help='At most 1 document can be uploaded.'
	)