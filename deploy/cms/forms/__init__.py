"""Default CMS Forms"""

from deploy.config import settings
from deploy.core.forms import Form, ModelFormFactory, fields

#### Forms

class DefaultCMSAddForm(ModelFormFactory):

	class Meta:
		name = 'generic-add-form'

class DefaultCMSUpdateForm(ModelFormFactory):

	class Meta:
		name = 'generic-update-form'

class DefaultCMSRemoveForm(ModelFormFactory):

	class Meta:
		name = 'generic-remove-form'

class DefaultSearchForm(Form):

	query = fields.TextField('Keywords', realtime=False, validators=[])

	fieldsets = [
		fields.Fieldset('Search filter', ['query']),
	]
