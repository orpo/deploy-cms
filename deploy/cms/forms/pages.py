from storm.expr import Desc, Asc

from deploy.config import settings
from deploy.core import forms
from deploy.core.forms import fields
from deploy.core.forms.validators import Validator, ValidatorResult, \
	EmailValidator, RequiredValidator, ModelUniqueValidator
from deploy.core.urls import __resolver__
from deploy.tools.users import encrypt_password

from deploy.cms.models.pages import DeployPage, DeployTemplate, PAGE_STATUS
from deploy.cms.utils import storm_emun_options

SOURCE_CODE_LANGUAGES = [
	(unicode(lang), unicode(lang.capitalize())) for lang in settings.TEMPLATE_ENGINES
]

class URLSlashValidator(Validator):

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is in a Deploy URL format"""

		if value is None or value == '':
			return ValidatorResult(False, self._get_error('This field is required.'))

		if not value.startswith('/'):
			return ValidatorResult(False, self._get_error('This field must start with a slash.'))

		if not value.endswith('/'):
			return ValidatorResult(False, self._get_error('This field must end with a slash.'))

		return ValidatorResult(True)

class URLUniquValidator(Validator):

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is in a Deploy URL format"""

		# Check that there isn't a current DeployPage with the URL
		page = request.db.find(
			DeployPage,
			DeployPage.url == unicode(value)
		).one()

		# A page has been found, stop processing
		if page:
			return ValidatorResult(False, self._get_error('This URL is currently being used!'))

		# Attempt to resolve the URL
		url_configuration = __resolver__.resolve(
			{'PATH_INFO': value, 'REQUEST_METHOD': 'GET', 'HTTP_HOST': ''},
			allow_cache=False,
			configuration=True
		)

		if url_configuration:
			return ValidatorResult(False, self._get_error('This URL is currently being used!'))

		return ValidatorResult(True)

class DeployPageAddForm(forms.Form):

	title = fields.TextField('Title')
	url = fields.TextField('URL', validators=[
		RequiredValidator(),
		URLSlashValidator(),
		URLUniquValidator()
	],
	attribs={'slugify': 'title'})

	template = fields.ModelSelectField(
		'Template',
		model=DeployTemplate,
		model_order=(Desc('name')),
		help='''
A number of page layouts are available select one from the templates below, if
your unsure choose default.
		'''
	)

	fieldsets = [
		fields.Fieldset('Page', ['title', 'url']),
		fields.Fieldset('Layout', ['template'])
		]

class DeployPageEditorUpdateForm(forms.Form):

	id = fields.HiddenField(datatype=int)
	status = fields.SelectField('Status',
		options=storm_emun_options(PAGE_STATUS),
		classes=['fluid-split']
		)
	last_modified = fields.DisplayOnlyField('Last modified',
		classes=['fluid-split', 'fluid-last']
		)
	title = fields.TextField('Title')
	url = fields.TextField('URL', validators=[
		RequiredValidator(),
		URLSlashValidator(),
		ModelUniqueValidator(DeployPage)
		], help="""
URLs must start and end with a forward slash, for example /hello-world/. They
should only contain alpha-numeric characters (a-zA-Z0-9) and dashes '-'.
	""")

	fieldsets = [
		fields.Fieldset('Status', ['id', 'status', 'last_modified']),
		fields.Fieldset('Page', ['title', 'url'])
		]

class DeployPageUpdateForm(forms.Form):

	id = fields.HiddenField(datatype=int)
	status = fields.SelectField('Status', options=storm_emun_options(PAGE_STATUS))
	title = fields.TextField('Title')
	url = fields.TextField('URL', validators=[
		RequiredValidator(),
		ModelUniqueValidator(DeployPage)
	])
	source_code_language = fields.SelectField('Language', options=SOURCE_CODE_LANGUAGES)
	source_code = fields.TextareaField('Source code')
	source_code_revision = fields.DisplayOnlyField('Source code revision', classes=['fluid-split'])
	last_modified = fields.DisplayOnlyField('Last modified', classes=['fluid-split', 'fluid-last'])

class DeployTemplateAddUpdateForm(forms.Form):

	name = fields.TextField('Name')
	source_code_language = fields.SelectField('Language', options=SOURCE_CODE_LANGUAGES)
	source_code = fields.TextareaField('Source code')
