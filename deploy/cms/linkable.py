"""Utility to provide a single search interface to item content
and their corresponding URL"""

import os
import os.path
import re
import unicodedata

import whoosh.query
from whoosh.index import create_in, exists_in, open_dir
from whoosh.fields import *
from whoosh.qparser import MultifieldParser
from whoosh.scoring import BM25F
from whoosh.writing import AsyncWriter
from pydispatch import dispatcher

from deploy.config import settings

class LinkableManager(object):
	"""Interface to the 'Linkable' index"""

	def __init__(self):
		# Create the path to the linkable index
		self.linkable_index_name = settings.CMS_LINKABLE_NAME
		self.linkable_index_path = os.path.join(
			settings.SITE_ROOT,
			settings.CMS_LINKABLE_INDEX
		)

		# Create the linkable index if it doesn't exist
		if not exists_in(self.linkable_index_path, indexname=self.linkable_index_name):
			# Create the index schema
			self.linkable_schema = Schema(
				url=ID(unique=True, stored=True),
				title=TEXT(stored=True),
				description=TEXT()
			)

			# Create the directory
			if not os.path.exists(self.linkable_index_path):
				os.mkdir(self.linkable_index_path)

			# Create the linkable index
			create_in(
				self.linkable_index_path,
				self.linkable_schema,
				indexname=self.linkable_index_name
			)

	def subscribe(self, url, title, description=None):
		"""Subscribe a url to the linkable index"""

		# Get a handle to the index
		index = open_dir(self.linkable_index_path, indexname=self.linkable_index_name)

		# Setup a writer
		writer = AsyncWriter(index, writerargs={'postlimitmb': 128 * 1024 * 1024})

		# Delete a previous entry
		writer.delete_by_term('url', unicode(url))

		# Make the title value safe
		title = unicodedata.normalize('NFKD', title).encode('ASCII', 'ignore').lower()

		for char in '\\\'"<>':
			title = title.replace(char, ' ')

		title = re.sub('\s+', ' ', title).strip()
		title = unicode(title)

		# Optional description - check and make safe
		if description is not None:
			description = unicodedata.normalize('NFKD', description).encode('ASCII', 'ignore').lower()

			for char in '\\\'"<>':
				description = description.replace(char, ' ')

			description = re.sub('\s+', ' ', description).strip()
			description = unicode(description)
		else:
			description = unicode('')

		# Write the entry to the index
		writer.add_document(url=unicode(url), title=title, description=description)
		writer.commit()

	def unsubscribe(self, url):
		"""Unsubscribe a url from the linkable index"""

		# Get a handle to the index
		index = open_dir(self.linkable_index_path, indexname=self.linkable_index_name)

		# Setup a writer
		writer = AsyncWriter(index, writerargs={'postlimitmb': 128 * 1024 * 1024})

		# Delete a previous entry
		writer.delete_by_term('url', unicode(url))
		writer.commit()

	def search(self, q):
		"""Search the linkable index"""

		if q is None:
			return None

		q = unicodedata.normalize('NFKD', q).encode('ASCII', 'ignore')

		for char in '\\\'"<>':
			q = q.replace(char, ' ')

		# Remove erroneous spaces
		q = re.sub('\s+', ' ', q).strip()

		# Open the index and setup the searcher
		index = open_dir(self.linkable_index_path, indexname=self.linkable_index_name)
		searcher = index.searcher(weighting=BM25F())

		# Build query
		query_parser = MultifieldParser(
			['url', 'title', 'description'],
			schema=index.schema,
			termclass=whoosh.query.Variations
		)
		query = query_parser.parse('*%s*' % unicode(q.lower()))

		# Get results
		results = searcher.search(query, limit=settings.CMS_LINKABLE_LIMIT)

		values = []

		# Build in a sensible format
		for result in results:
			values.append((
				result.get('url', ''),
				result.get('title', ''),
			))

		# Clean up
		searcher.close()

		return values

__linkable__ = LinkableManager()

def subscribe_linkable(signal=None, sender=None, **kwargs):
	"""Utility function to subscribe an object to the Linkable index"""

	# Check we have sender and it's not anonymous
	if sender is None or sender == isinstance(sender, dispatcher._Anonymous):
		return False

	id_key = '__linkable_id__'
	title_key = '__linkable_title__'
	desc_key = '__linkable_description__'

	if kwargs.get('id_key', None) is not None:
		id_key = kwargs.pop('id_key', None)

	if kwargs.get('title_key', None) is not None:
		title_key = kwargs.pop('title_key', None)

	if kwargs.get('desc_key', None) is not None:
		desc_key = kwargs.pop('desc_key', None)

	# Check we have the required keys on our sender object
	if not hasattr(sender, id_key) or not hasattr(sender, title_key):
		return False

	# Check if we have a desciption field - ignore if not
	desc = None
	if hasattr(sender, desc_key):
		desc = getattr(sender, desc_key)

	# Subscribe the linkable
	__linkable__.subscribe(
		getattr(sender, id_key),
		getattr(sender, title_key),
		description=desc
	)

	return True

def unsubscribe_linkable(signal=None, sender=None, **kwargs):
	"""Utility function to unsubscribe an object from the Linkable index"""

	# Check we have sender and it's not anonymous
	if sender is None or sender == isinstance(sender, dispatcher._Anonymous):
		return False

	id_key = '__linkable_id__'

	if kwargs.get('id_key', None) is not None:
		id_key = kwargs.pop('id_key', None)

	# Check we have the required keys on our sender object
	if not hasattr(sender, id_key):
		return False

	# Subscribe the linkable
	__linkable__.unsubscribe(getattr(sender, id_key))

	return True
