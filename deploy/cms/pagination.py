"""CMS Paginator"""

from deploy.core.pagination import Paginator as DeployPaginator, Page as DeployPage, \
	PageNumberNotAnInteger, InvalidPage, EmptyPage

__all__ = ('Page', 'Paginator')

class Paginator(DeployPaginator):
	"""Simple pagination class for Deploy"""

	def __init__(self, objects, items_per_page=10, orphans=0, columns=None, row_id=None):
		super(Paginator, self).__init__(objects, items_per_page=items_per_page, orphans=orphans, columns=columns)
		self.row_id = row_id

	def page(self, page_number):
		"""Return a page of paginated data"""

		try:
			page_number = int(page_number)
		except ValueError:
			raise PageNumberNotAnInteger('The page number "%s" is not an valid integer' % page_number)

		if page_number < 1:
			raise InvalidPage('Page number specified is less than 1')

		if page_number > self.page_count:
			raise InvalidPage('Page number specified is greater than page count (%s)' % self.page_count)

		page_number = max(page_number, 1)
		page_number = min(page_number, self.page_count)

		first = (page_number - 1) * self.items_per_page
		last = first + self.items_per_page

		if page_number == self.page_count:
			last += self.orphans

		items = self.objects[first:last]

		if self.item_count == 0:
			raise EmptyPage()

		return Page(page_number, self.page_count, first, last, self.item_count, items, self.columns, self.row_id)

class Page(DeployPage):
	"""A object representation of a 'page' of objects."""

	def __init__(self, number, page_count, first, last, total_item_count, items, columns, row_id):
		self.row_id = row_id
		items = list(items)
		super(Page, self).__init__(number, page_count, first, last, total_item_count, items, columns)

	def to_dict(self):
		"""Return the page information as a dictionary"""
		page = {
			'number': self.number,
			'numbers': range(1, self.page_count + 1),
			'next_page_number': self.next_page_number,
			'previous_page_number': self.previous_page_number,
			'page_count': self.page_count,
			'has_next_page': self.has_next_page(),
			'has_previous_page': self.has_previous_page(),
			'has_other_pages': self.has_other_pages(),
			'total_item_count': self.total_item_count,
			'columns': self.columns,
		}

		items = []

		for item in self.items:
			row = {}

			if self.columns and isinstance(self.columns[0], dict):
				for col in self.columns:
					if col.has_key('display_property'):
						row[col['name']] = getattr(item, col['display_property'])
					else:
						row[col['name']] = getattr(item, col['name'])

				row['_id'] = getattr(item, self.row_id)

			else:
				row = dict(zip(self.columns, item[:-1]))
				row['_id'] = item[-1]

			items.append(row)

		page['items'] = items

		return page
