import os
import os.path
import warnings

try:
	from cStingIO import StringIO
except ImportError:
	from StringIO import StringIO

from deploy.config import settings
from deploy.core.exceptions import ApplicationException

from deploy.cms.store import get_store
from deploy.cms.utils import get_tmp_filestore

__all__ = (
	'allow_thumbnail_for_type', 'create_thumbnail_for_type'
	'generate_image_thumbnail',
)

ALLOWED_THUMBNAILS = (
	'image/jpeg',
	'image/png',
	'image/gif',
	'image/tiff',
)

def allow_thumbnail_for_type(filetype):
	return filetype in ALLOWED_THUMBNAILS

def create_thumbnail_for_type(filetype, filepath):
	""""Create a thumbnail for a given filetype"""

	# Safety check
	if not settings.CMS_ATTACHMENT_THUMBNAIL:
		raise ApplicationException('Thumbnails can not be generated when CMS_ATTACHMENT_THUMBNAIL is set to False')

	if filetype in ['image/jpeg', 'image/png', 'image/gif', 'image/tiff']:
		return generate_image_thumbnail(filepath)

	# @@ TODO: Add support for generating PDF previews etc..

	raise ApplicationException('Unable to generate thumbnail for unknown type "%s"' % filetype)

def generate_image_thumbnail(path):
	"""Generate an image thumbnail for a given image path"""

	try:
		from PIL import Image
	except ImportError:
		warnings.warn(
			'You can not generate image thumbnails without PIL installed!',
			RuntimeWarning
		)
		return None

	# Get the store interface
	file_store = get_store(auto_unique=False)

	# Check the file exists
	if not file_store.exists(path):
		return None

	# Generate the thumb name
	thumbnail_name = os.path.basename(path)
	thumbnail_name, thumbnail_ext = os.path.splitext(thumbnail_name)
	thumbnail_name = '%s_thumb%s' % (thumbnail_name, thumbnail_ext)

	# Build the path to the thumbnail
	thumbfile = os.path.join(
		os.path.dirname(path),
		thumbnail_name
	)

	# Get the original file
	original = file_store.open(path)

	# Create the outfile
	outfile = StringIO()

	# Create the thumbnail
	try:
		img = Image.open(original)

		if img.mode == 'P':
			# Pallete based...
			img.thumbnail(settings.CMS_ATTACHMENT_THUMBNAIL_SIZE, Image.ANTIALIAS)
			img.save(outfile, 'PNG', transparency=img.info['transparency'])

		elif img.mode == 'RGBA':
			img = img.convert('RGB').convert('P', palette=Image.ADAPTIVE)
			img.thumbnail(settings.CMS_ATTACHMENT_THUMBNAIL_SIZE, Image.ANTIALIAS)
			img.save(outfile, 'PNG')

		elif img.mode != 'RGB':
			img = img.convert('RGB')
			img.thumbnail(settings.CMS_ATTACHMENT_THUMBNAIL_SIZE, Image.ANTIALIAS)
			img.save(outfile, 'JPEG')

		else:
			img.thumbnail(settings.CMS_ATTACHMENT_THUMBNAIL_SIZE, Image.ANTIALIAS)
			img.save(outfile, 'JPEG')

	except Exception, e:
		return None

	# Save the image
	file_store.save(thumbfile, outfile.getvalue())

	# Return the path segment
	return thumbfile
