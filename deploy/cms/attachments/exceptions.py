from deploy.core.exceptions import ApplicationException

class AttachmentAddError(ApplicationException):
	pass

class AttachmentUpdateError(ApplicationException):
	pass