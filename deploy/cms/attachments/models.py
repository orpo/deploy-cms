"""Deploy Object Attachments"""

import os
import os.path
import datetime

from storm.locals import *

from deploy.config import settings
from deploy.core.models import Model
from deploy.utils.importing import import_object
from deploy.cms.store import get_store

__all__ = ('Attachment', 'AttachmentWrapper')

class AttachmentWrapper(object):

	def __init__(self, path):
		self.path = path

	def __unicode__(self):
		return unicode(self.path)

	def __str__(self):
		return str(self.__unicode__())

	def _get_variation_path(self, variation):
		return os.path.join(
			os.path.dirname(self.path),
			variation.lower().replace(' ', '-'),
			os.path.basename(self.path)
		)

	def _get_variation_url(self, variation):
		return os.path.join(
			settings.CMS_ATTACHMENT_BASE_URL,
			self._get_variation_path(variation)
		)

	def _get_thumbnail_path(self):
		thumbnail_name = os.path.basename(self.path)
		thumbnail_name, thumbnail_ext = os.path.splitext(thumbnail_name)
		thumbnail_name = '%s_thumb%s' % (thumbnail_name, thumbnail_ext)

		# Generate the path for the new attachment thumbnail
		attachment_thumb_path = os.path.join(
			os.path.dirname(self.path),
			thumbnail_name
		)

		return attachment_thumb_path

	def variation(self, name):
		return self._get_variation_url(name)

	def variation_raw_path(self, variation_path):
		return os.path.relname(variation_path, settings.CMS_ATTACHMENT_BASE_URL)

	def raw_path(self, path):
		return path.replace(settings.CMS_ATTACHMENT_BASE_URL, '', 1)

	@property
	def url(self):
		"""Return the absolute URL to an Attachment"""
		return os.path.join(settings.CMS_ATTACHMENT_BASE_URL, self.path)

	@property
	def thumbnail(self):
		"""Return the absolute URL to an Attachment thumbnail - NOTE:
		this does not guarantee the thumbnail exists"""
		return os.path.join(
			settings.CMS_ATTACHMENT_BASE_URL,
			self._get_thumbnail_path()
		)

	@property
	def path_thumbnail(self):
		"""Return the relative path to an Attachment thumbnail - NOTE:
		this does not guarantee the thumbnail exists"""
		return self._get_thumbnail_path()

class Attachment(Model, AttachmentWrapper):

	__storm_table__ = 'deploy_attachment'

	path = Unicode(primary=True)
	created = DateTime()
	last_modified = DateTime()
	mimetype = Unicode()
	meta = Pickle()
	order = Int(default=0)

	def __init__(self, *args, **kwargs):
		self.created = datetime.datetime.now()
		super(Attachment, self).__init__(*args, **kwargs)

	def has_meta(self, meta_key):
		if self.meta and self.meta.has_key(meta_key):
			return True

		return False

	def get_meta(self, meta_key, default=None):
		if not self.has_meta(meta_key):
			return default

		return self.meta[meta_key]

	def set_meta(self, meta_key, meta_value):
		if not self.meta:
			self.meta = {}

		self.meta[meta_key] = meta_value

	def thumbnail_exists(self):
		"""Check a thumbnail exists in the file store"""
		file_store = get_store()

		# Check the file store to see if a thumbnail exists
		return file_store.exists(self.path_thumbnail)

	def variation_exists(self, key):
		"""Check a thumbnail exists in the file store"""

		variation = self.get_variation(key)

		if variation is None:
			return False

		file_store = get_store()

		# Check the file store to see if a variation exists
		return file_store.exists(variation)

	def has_variations(self):
		if not self.has_meta('variations'):
			return None

		return True if self.get_meta('variations') else False

	def has_variation(self, key):
		if not self.has_variations():
			return False

		variations = self.get_meta('variations')

		if not variations.has_key(key):
			return False

		return True

	def get_variations(self):
		if not self.has_variations():
			return []

		variations = []

		for variation_key, variation in self.get_meta('variations').items():
			variations.append(self._get_variation_path(variation_key))

		return variations

	def get_variation(self, key):
		if not self.has_variations():
			return None

		if not self.has_variation(key):
			return None

		# Return the variation path with any prefix
		return self.get_meta('variations')[key]

	def build_variations(self, *dynamic_args):
		if not self.has_variations():
			return False

		for key, variation in self.meta['variations'].items():
			process = variation['process']
			args = variation['args'] if variation.has_key('args') else []
			save_path = self._get_variation_path(key)

			# Combine static args with dynamic args
			args.extend(dynamic_args)

			try:
				process_callable = import_object(process)
			except ImportError, e:
				raise RuntimeError('Unable to import variation processor "%s" for attachment "%s"' % (
					process, self.path
				))

			try:
				process_callable(self, save_path, *args)
			except Exception, e:
				raise RuntimeError('Unable to process variation "%s" using "%s": %s' % (
					self.path, process, e
				))

		return True

	def remove_variations(self, **kwargs):
		if not self.has_variations():
			return False

		for key, variation in self.meta['variations'].items():
			# Get the variation path
			save_path = self._get_variation_path(key)

			# Get the store interface
			file_store = get_store()

			if not file_store.exists(save_path):
				# @@ FIXME: Remove a variation doesn't exist - log warning?
				continue

			# Remove file
			try:
				file_store.remove(save_path)
			except OSError, e:
				raise Exception('Unable to remove variation "%s" for "%s" because: %s' % (
					key, self.path, e
				))

		return True

	@staticmethod
	def get_path_from_variation(variation_path, variation_name):
		"""Get the path for an attachment from a variation path. (Reverse lookup)"""

		# Normalize the path
		variation_path = variation_path.replace(settings.CMS_ATTACHMENT_BASE_URL, '', 1)

		filename = os.path.basename(variation_path)

		variation_name = variation_name.lower().replace(' ', '-')
		removed_variation = False
		path = variation_path

		while not removed_variation:
			# Split the path
			path, tail = os.path.split(path)

			# Does the tail look like the variation name
			if tail == variation_name:
				removed_variation = True

		return os.path.join(path, filename)
