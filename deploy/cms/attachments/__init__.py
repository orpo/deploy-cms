"""Deploy Attachments"""

import os
import os.path
import re
import json
import mimetypes
import datetime

from storm.locals import *

from deploy.config import settings
from deploy.utils.datastructures import SortedDict
from deploy.utils.encoding import ExtendedJSONEncoder
from deploy.utils.formatting import file_size as format_file_size
from deploy.cms.attachments.models import Attachment, AttachmentWrapper
from deploy.cms.attachments.exceptions import AttachmentAddError, AttachmentUpdateError
from deploy.cms.attachments.utils import allow_thumbnail_for_type
from deploy.cms.store import get_store
from deploy.cms.utils import filepath_generator, get_tmp_filestore

TMP_FILE_FORMAT = re.compile(r'upload-(.+?)-(.+?)$')

EXPECTED_META = {
	'image/jpg': [('Alternative Text', 'alt'), ('Long description', 'longdesc')],
	'image/jpeg': [('Alternative Text', 'alt'), ('Long description', 'longdesc')],
	'image/png': [('Alternative Text', 'alt'), ('Long description', 'longdesc')],
	'image/gif': [('Alternative Text', 'alt'), ('Long description', 'longdesc')]
}


def is_temporary_file(path):
	if TMP_FILE_FORMAT.findall(path):
		return True

	return False

class Attachments(object):
	"""Attachment store manager"""

	def __init__(self, model, column, store, allow=None, max=None, max_size=None,
				variations=None, expected_meta=None, **kwargs):

		assert model is not None, 'You must specify a model to attachments!'
		assert column is not None, 'You must specify a column to attachments!'
		assert store is not None, 'You must specify a store to attachments!'

		# Flag the model as having attachments and inject any necessary meta information
		self.model = model

		# Model Column for attachment
		self.store = store
		self.column = column

		# Allowed mime-types - if there is a limit
		self.allowed_mimetypes = allow

		# Limit to number of attachments
		self.max = max

		# Maximum size of the attachments
		self.max_size = max_size

		# Expected meta key information
		self.expected_meta = expected_meta

		# Variations
		self.variations = variations

		# See if model already has a attachment meta information
		if not hasattr(self.model, '__attachments__'):
			setattr(self.model, '__attachments__', [self.column.name])
		else:
			# Meta information already exists, add to it..
			self.model.__attachments__.append(self.column.name)

	def __get__(self, instance, owner):
		"""Descriptor should return a new instance of a store manager interface"""

		return AttachmentStoreManager(
			instance, self.model, self.column, self.store,
			self.allowed_mimetypes, self.max, self.max_size, self.variations,
			self.expected_meta
		)

	@staticmethod
	def convert_to_db(value):
		if value is not None:
			return unicode(','.join(value))

		return unicode('')

	@staticmethod
	def convert_to_python(value):
		if value is not None:
			return tuple(value.split(','))

		return []

class AttachmentStoreManager(object):

	def __init__(self, parent, model, column, store, allowed_mimetypes, max, max_size,
				variations, expected_meta):

		# Setup out attachment store variables
		self.model = model
		self.column = column
		self.store = store
		self.allowed_mimetypes = allowed_mimetypes
		self.max = max
		self.max_size = max_size
		self.variations = variations
		self.expected_meta = expected_meta

		# Internal caches
		self._parent = parent

		if self._parent is not None:
			self._attachments = Attachments.convert_to_python(
				getattr(self._parent, self.column.name)
			)
		else:
			self._attachments = []

	def __iter__(self):
		"""Iterator for the attachment paths (attachment objects are not returned - use all_iter)"""

		attachments = self._get_attachments_as_list()

		for attachment in attachments:
			yield AttachmentWrapper(attachment)

	def __len__(self):
		"""Return the length of the attachment list"""

		attachments = self._get_attachments_as_list()
		return len(attachments)

	def __getitem__(self, key):
		"""Direct indexing of attachments"""

		attachments = self._get_attachments_as_list()
		return AttachmentWrapper(tuple(attachments)[key])

	def __setitem__(self, key, value):
		"""Attachment store does not allow assignment directly!"""
		raise NotImplementedError

	def __delitem__(self, key):
		"""Attachment store does not allow removal directly!"""
		raise NotImplementedError

	def _get_attachments_as_list(self):
		# Get the attachment paths
		attachment_paths = getattr(self._parent, self.column.name)
		attachments = self.convert_to_python(attachment_paths)

		return attachments

	def _get_last_attachment(self, db):
		"""Return the last attachment in the list"""

		return db.find(Attachment,
			Attachment.path.is_in(self._attachments)
		).order_by(Desc(Attachment.order))[:1].one()

	def _get_attachments(self, db):
		"""Internal function for retrieving attachment details from the database"""

		# Check if cache has been invalidated
		if self._attachments is None:
			if self._parent is None:
				raise RuntimeError('You can not get attachments without a parent instance!')

			self._attachments = Attachments.convert_to_python(
				getattr(self._parent, self.column.name)
			)

		return db.find(Attachment,
			Attachment.path.is_in(self._attachments)
		).order_by(Attachment.order)

	def _get_default_meta_for_attachment(self):
		meta = {}

		if self.variations:
			meta['variations'] = self.variations

		return meta

	def to_json(self, db):
		"""Get attachment information as JSON"""

		attachments = self._get_attachments(db)
		output = []

		for attachment in attachments:
			attachment_data = {
				'path': attachment.path,
				'meta': attachment.meta,
				'expected_meta': self.get_expected_meta_for_type(
					attachment.mimetype
				)
			}

			# Get the correct thumbnail for the attachment if they are allowed
			if settings.CMS_ATTACHMENT_THUMBNAIL and allow_thumbnail_for_type(attachment.mimetype):
				# Check we have a thumbnail to use...
				if attachment.thumbnail_exists():
					attachment_data['thumbnail_path'] = attachment.thumbnail

			output.append(attachment_data)

		return output

	def from_json(self, json):
		"""Build attachments from JSON input"""

		# JSON is in the format
		#[{path: '...', meta: {key: '...'}}, ...]

		# Get the store interface
		file_store = get_store()

		# Setup the order counter
		order_counter = 0

		# Container for the parsed attachments
		attachments = SortedDict()

		# Go over the attachment JSON and attempt to build the attachment detail
		for attachment in json:
			# Check we don't go over the max number of allowed attachments
			if self.max is not None and order_counter >= self.max:
				break

			attachment_path = attachment['path']
			attachment_meta = attachment['meta']

			# ATTACHMENT SAVE
			# Check if the attachment currently refers to a temporary file, if so we
			# need to build and save this file using the default file store
			if is_temporary_file(attachment_path):
				# Temporary files need to be saved to the final location before
				# being saved against the model

				# Check that this file exists
				if not file_store.exists(attachment_path):
					# File does not exist in temp format - bail out..

					# @@ TODO Attachment doesnt exist: should this be silent?
					raise Exception('Attachment file "%s" does not exist!' % attachment_path)

				# Read the contents of the temporary file
				try:
					# Get and read the existing file
					tmp_attachment_file = file_store.open(attachment_path, 'r')
					attachment_contents = tmp_attachment_file.read()
					tmp_attachment_file.close()

					# Remove the old file
					file_store.remove(attachment_path)
				except IOError, e:
					raise Exception('Temporary attachment file "%s" can not be read: %s' % (attachment_path, e))

				# Generate to storage path
				new_attachment_path = os.path.join(
					self.store,
					filepath_generator(
						attachment_meta['original_filename'].lower()
					)
				)

				# Check for a unique path..
				if file_store.exists(new_attachment_path):
					new_attachment_path = file_store.get_unique_name(new_attachment_path)

				# Save the attachment in the store
				file_store.save(new_attachment_path, attachment_contents)

				# Do we need to build a thumbnail variation?
				if 'thumbnail_path' in attachment and attachment['thumbnail_path']:
					# A thumbnail has been generated in temporary form, move to file store

					# Generate a name for the existing thumbnail
					thumbnail_name, thumbnail_ext = os.path.splitext(attachment_path)
					thumbnail_name = '%s_thumb%s' % (thumbnail_name, thumbnail_ext)

					try:
						# Get and read in the existing thumb
						tmp_attachment_thumb = file_store.open(thumbnail_name, 'r')
						attachment_thumb_contents = tmp_attachment_thumb.read()
						tmp_attachment_thumb.close()

						# Remove the old upload thumb
						file_store.remove(thumbnail_name)
					except IOError, e:
						raise Exception('Temporary attachment thumbnail "%s" can not be read: %s' % (thumbnail_name, e))

					# Generate a name for new thumbnail
					new_thumbnail_name = os.path.basename(new_attachment_path)
					new_thumbnail_name, new_thumbnail_ext = os.path.splitext(new_thumbnail_name)
					new_thumbnail_name = '%s_thumb%s' % (new_thumbnail_name, new_thumbnail_ext)

					# Generate the path for the new attachment thumbnail
					new_attachment_thumb_path = os.path.join(
						os.path.dirname(new_attachment_path),
						new_thumbnail_name
					)

					# Get a new store handle with auto-unique off...
					file_store = get_store(auto_unique=False)

					# Save the attachment in the store
					file_store.save(new_attachment_thumb_path, attachment_thumb_contents)

				# Reset out attachment path to the new saved image
				attachment_path = new_attachment_path

			# Create the attachment detail
			attachments[attachment_path] = {
				'order': order_counter,
				'meta': attachment_meta,
			}

			# Update the attachment counter
			order_counter += 1

		return attachments

	def add(self, db, attachment=None, path=None, order=None, meta=None, args=None, build_variations=True):
		"""Add an attachment to the attachment store and update the parent model"""

		assert not (attachment is None and path is None), 'You must specify an attachment or a path to add!'

		attachment_count = len(list(self._get_attachments(db)))

		# Check we don't go over the max number of allowed attachments
		if self.max is not None and attachment_count >= self.max:
			raise AttachmentAddError('Max number of attachments reached!')

		if args is None:
			args = {}

		if self._parent is None:
			raise AttachmentAddError('You can not add an attachment without a parent instance!')

		if path is not None:
			# Get the store interface
			file_store = get_store()

			# Check it exists in the file store
			if not file_store.exists(path):
				raise AttachmentAddError('Attachment "%s" does not exist!' % path)

			# Get default meta info for the attachment
			meta_data = self._get_default_meta_for_attachment()

			# Add any additional meta to the dictionary, also
			# defaults can be overridden
			if meta is not None:
				meta_data.update(meta)

			# Attempt to get the mime-type and size of the file
			file_type, file_encoding = mimetypes.guess_type(path, strict=False)
			file_size = file_store.size(path)

			# Sanity checking attachments
			if self.allowed_mimetypes is not None and file_type not in self.allowed_mimetypes:
				raise AttachmentAddError('Attachment of the type "%s" is not allowed.' % file_type)

			if self.max_size is not None and file_size > self.max_size:
				raise AttachmentAddError('Attachment is too large! "%s" must be "%s" maximum' % (
					format_file_size(file_size),
					format_file_size(self.max_size)
				))

			mimetype = u'application/octet-stream'

			if file_type:
				meta_data['type'] = file_type
				mimetype = file_type.decode(settings.ENCODING)

			if file_encoding:
				meta_data['encoding'] = file_encoding

			if file_size:
				meta_data['size'] = file_size

			# Add the attachment to the attachment store
			attachment = Attachment(
				created=datetime.datetime.now(),
				last_modified=datetime.datetime.now(),
				path=path.decode(settings.ENCODING),
				mimetype=mimetype,
				meta=meta_data,
				**args
			)

		# Auto-generate the order value if None specified
		if order is None:
			last_attachment = self._get_last_attachment(db)
			if last_attachment:
				attachment.order = last_attachment.order + 1
			else:
				attachment.order = 0

		else:
			attachment.order = order

		db.add(attachment)

		# Check if we are automatically building variations. It may be desirable to suppress this
		# behavior to build the variations manually.
		if build_variations:
			attachment.build_variations()

		# Update the parent column
		current_attachments = [item.path for item in self._get_attachments(db)]
		current_attachments.insert(attachment.order, attachment.path)

		setattr(self._parent, self.column.name, Attachments.convert_to_db(current_attachments))

		db.commit()

		# Reset internal cache
		self._attachments = current_attachments

		return True

	def update(self, db, attachment=None, path=None, order=None, meta=None,
			rebuild_variations=False, hard_update_meta=False):
		"""Update an attachment in the attachment store and update the parent model"""

		assert not (attachment is None and path is None), 'You must specify an attachment or a path to update!'

		if path is not None:
			attachment = db.find(Attachment, Attachment.path == path).one()

		if attachment is None:
			raise AttachmentUpdateError(
				'Unable to update attachment "%s" can not be found!' % (
					path or 'NO PATH SENT TO UPDATE'
				)
			)

		# Update the attachment
		if order is not None:
			attachment.order = order

		# Update any meta information
		if meta is not None:
			if attachment.meta:
				new_keys = meta.keys()
				attachment.meta.update(meta)

				if hard_update_meta:
					# Remove dead data
					current_keys = attachment.meta.keys()
					for current_key in current_keys:
						if not current_key in new_keys:
							del attachment.meta[current_key]
			else:
				attachment.meta = meta

		attachment.last_modified = datetime.datetime.now()

		db.commit()

		# Are we rebuilding the variations
		if rebuild_variations:
			# Remove the variations
			attachment.remove_variations(store=self.store)

			# Build the variations
			attachment.build_variations()

		db.commit()

	def remove(self, db, attachment=None, path=None):
		"""Remove an attachment from the attachment store and update the parent model"""

		assert not (attachment is None and path is None), 'You must specify an attachment or a path to remove!'

		if self._parent is None:
			raise RuntimeError('You can not remove an attachment without a parent instance!')

		if path is not None:
			attachment = db.find(Attachment, Attachment.path == path).one()

		if attachment is not None:
			path = attachment.path

		# Remove the variations
		attachment.remove_variations(store=self.store)

		# Get the store interface
		file_store = get_store()

		# Is there a thumbnail to remove?
		if file_store.exists(attachment.path_thumbnail):
			try:
				# Remove the thumbnail
				file_store.remove(attachment.path_thumbnail)
			except OSError, e:
				# @@ TODO: Removing an attachment thumbnail file - should have sensible error?
				raise Exception(e)

		# Remove the primary file
		if file_store.exists(path):
			try:
				# Remove the file
				file_store.remove(path)
			except OSError, e:
				# @@ TODO: Removing an attachment file - should have sensible error?
				raise Exception(e)

		# Remove the attachment
		db.remove(attachment)

		# Update the parent model
		current_attachments = [item.path for item in self._get_attachments(db)]

		try:
			index_to_remove = current_attachments.index(path)
			current_attachments.pop(index_to_remove)
		except ValueError:
			pass

		setattr(self._parent, self.column.name, Attachments.convert_to_db(current_attachments))

		db.commit()

		# Reset internal cache
		self._attachments = current_attachments

		return True

	def clear(self, db):
		"""Remove all the attachments (and there variations)"""

		if self._parent is None:
			raise RuntimeError('You can not clear attachments without a parent instance!')

		# Get the current attachments
		current_attachments = [item.path for item in self._get_attachments(db)]

		for attachment_path in current_attachments:
			self.remove(db, path=attachment_path)

		setattr(self._parent, self.column.name, unicode(''))

		db.commit()

		# Reset internal cache
		self._attachments = []

		return True

	def sync(self, db, attachments):
		"""Sync the attachments for the attachment store

			db - Database handle
			attachments - Dictionary of attachment data, typically generated from JSON
		"""

		if self._parent is None:
			raise RuntimeError('You can not sync attachments without a parent instance!')

		# Get the current attachments
		current_attachments = [item.path for item in self._get_attachments(db)]

		# Check if we have been sent any attachments
		if not attachments and not current_attachments:
			# We don't have any attachments and we haven't been sent any - move on
			return False

		# Check if we have been sent any attachments, as we have some and they need to be remove
		if not attachments:
			# Remove each attachment
			for attachment_path in current_attachments:
				self.remove(db, path=attachment_path)

			return True

		# We have been sent attachments, we need to check if we are updating or adding attachments
		# We check in this order - Ones to Remove, Ones to Update, Ones to Add

		# Remove any attachments not specified
		new_attachment_paths = attachments.keys()
		attachments_to_remove = [
			attachment_path for attachment_path in current_attachments
			if not attachment_path in new_attachment_paths
		]

		if attachments_to_remove:
			# Remove the attachments
			for attachment_path in attachments_to_remove:
				self.remove(db, path=attachment_path)

		# Update/Add attachments
		attachment_order = 0
		new_attachments = []

		for attachment_path, attachment_detail in attachments.items():
			# Check what we need to do with the attachments
			if attachment_path in current_attachments:
				# Update
				self.update(
					db,
					path=attachment_path,
					order=attachment_order,
					meta=attachment_detail['meta'],
					hard_update_meta=True
				)

			elif not attachment_path in current_attachments:
				# Add
				self.add(
					db,
					path=attachment_path,
					order=attachment_order,
					meta=attachment_detail['meta']
				)

			new_attachments.append(attachment_path)
			attachment_order += 1

		# Update the parent column
		setattr(self._parent, self.column.name, Attachments.convert_to_db(new_attachments))

		db.commit()

		# Reset internal cache
		self._attachments = new_attachments

		return True

	def build_attachment_variations(self, db):
		"""Build attachment variations"""

		attachments = self.all(db)

		for attachment in attachments:
			attachment.build_variations()

	def exist(self):
		"""Check if any attachments exist"""

		attachments = self._get_attachments_as_list()

		if len(attachments) > 0:
			return True

		return False

	def all(self, db):
		"""Return all of the attachments as 'Attachment' objects"""
		return self._get_attachments(db)

	def all_iter(self, db):
		"""Return an iterator of the attachments as 'Attachment' objects"""
		attachments = self._get_attachments(db)

		for attachment in attachments:
			yield attachment

	def get(self, db, index):
		"""Get a specific attachment"""

		attachments = self._get_attachments_as_list()

		try:
			attachment = attachments[index]
		except IndexError:
			raise RuntimeError('Attachment with index %s does not exist.' % index)

		return self.get_by_path(db, attachment)

	def get_by_path(self, db, path):
		"""Get an attachment by it's path"""
		current_paths = [item.path for item in self._get_attachments(db)]

		if path in current_paths:
			return db.find(Attachment, Attachment.path == path).one()

		return None

	def get_by_type(self, db, type):
		"""Get all the attachments by type"""
		raise NotImplementedError

	def get_expected_meta_for_type(self, _type):
		"""Utility function to get the attachment variations"""

		if self.expected_meta and _type in self.expected_meta:
			return self.expected_meta[_type]

		elif _type in EXPECTED_META:
			return EXPECTED_META[_type]

		return None

	def get_store(self):
		return self.store

	@staticmethod
	def convert_to_db(value):
		if value is not None and not value == '':
			return unicode(','.join(value))

		return unicode('')

	@staticmethod
	def convert_to_python(value):
		if value is not None and not value == '':
			return tuple(value.split(','))

		return []
