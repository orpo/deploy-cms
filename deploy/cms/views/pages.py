"""Public views for Page"""

from deploy import __version__ as v

from deploy.core.http import Response
from deploy.core.http.exceptions import PageNotFound
from deploy.core.templates import TemplateManager
from deploy.utils.encoding import to_unicode

from deploy.cms.models.pages import DeployPage

__all__ = ('view_page', 'build_a_page')

def view_page(request):
	"""Generic view for rendering CMS pages"""

	the_page = None

	# Check if we are in edit mode, if we are then we still show unpublished pages
	if request.mode == 'EDIT':
		# We still need to check if the user is logged in or not
		if request.environ['com.deploycms.cms.administrator_session']:
			the_page = request.db.find(
				DeployPage,
				DeployPage.url == unicode(request.path)
			).one()

		else:
			the_page = request.db.find(
				DeployPage,
				DeployPage.status == u'Published',
				DeployPage.url == unicode(request.path)
			).one()

	else:
		the_page = request.db.find(
			DeployPage,
			DeployPage.status == u'Published',
			DeployPage.url == unicode(request.path)
		).one()

	if not the_page:
		raise PageNotFound(request.path)

	response = Response()
	response.status = 200
	response.unicode_body = to_unicode(the_page.render(request))

	return response

def build_a_page(request):
	"""View to show in edit mode for 404 errors - this allows users to add a
	page directly"""

	# Generate the template
	template_manager = TemplateManager(request, engine='jinja')
	source = template_manager.render('build-a-page.html', data={
		'path': request.path,
		'version': v,
	})

	response = Response()
	response.status = 404
	response.unicode_body = to_unicode(source)

	return response