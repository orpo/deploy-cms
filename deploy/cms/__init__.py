"""Deploy CMS"""

import inspect
import logging
import urlparse

from deploy.config import settings
from deploy.config import settings as wrapper_settings
from deploy.core.exceptions import ConfigurationError
from deploy.core.urls import __resolver__
from deploy.utils.importing import import_module

from deploy.cms.controllers import CMSController
from deploy.cms.utils import has_permission
from deploy.cms.json_response import JSONErrorResponse

__all__ = ('__cms__', 'get_cms_controller')

log = logging.getLogger('deployapp.cms')

class CMSControllerManager(object):

	def __init__(self):
		self.controler_map = {}

	def load(self, module, silent=False):
		"""Utility to automatically register model controllers in a module"""

		# Look at members of the module, if they are a subclass of CMSController,
		# register them with the manager
		if not inspect.ismodule(module):
			try:
				controller_module = import_module(module)
			except ImportError, e:
				if not silent:
					raise ConfigurationError('Unable to load controller module! "%s"' % e)

				return False
		else:
			controller_module = module

		controller_members = inspect.getmembers(controller_module)

		# Only get items that appear to be named as controllers
		controller_members = [(n, v) for n, v in controller_members if n.endswith('Controller')]

		for name, value in controller_members:
			# Controller Hooks

			# @@ FIXME: Check this is safe (checking classname)
			if value and inspect.isclass(value) and issubclass(value, CMSController) and not name == 'CMSController':
				# Get an instance on the controller
				controller_instance = value()

				# Register the controller with the manager
				self.register(
					controller_instance,
					force=controller_instance.Meta.force_overload
				)

		return True

	def register(self, model_controller, force=False):
		"""Register a model controller with the manager"""

		# Get the controller name (this is generated whent he controller is initalised)
		controller_name = model_controller._controller_name

		if self.has_controller(controller_name) and not force:
			raise ConfigurationError(
				'A controller for model "%s" in "%s" already exists!' %
				(model_controller.Meta.model, model_controller.Meta.model.__module__))

		self.controler_map[controller_name] = model_controller

	def has_controller(self, controller_name):
		"""Check if a specific controller is available"""
		return controller_name in self.controler_map

	def get_controller(self, controller_name):
		"""Get a specific controller"""
		if not self.has_controller(controller_name):
			return None

		return self.controler_map[controller_name]

	def get_all_controllers(self):
		"""Return a list of all the controller names"""
		return self.controler_map.keys()

	def get_gui_for_administrator(self, administrator):
		"""Get the CMS GUI options for an administrator"""

		role = administrator.get_role_key()

		if role is None:
			return {}

		controller_method_matrix = self.get_controller_methods(for_permisison=role)

		gui_config = {}

		for controller_key, methods in controller_method_matrix.items():
			controller = self.controler_map[controller_key]

			# Set the controller gui options & allowed methods
			gui_config[controller_key] = {
				'views': methods,
				'gui': controller.Meta.gui,
				'gui_grouping': controller.Meta.gui_grouping,
				'gui_verbose_name': controller.Meta.gui_verbose_name,
				'gui_verbose_name_plural': controller.Meta.gui_verbose_name_plural,
				'gui_inpage_editing': controller.Meta.gui_inpage_editing,
			}

		return gui_config

	def get_controller_methods(self, for_permisison=None):
		"""Return all methods for each available controller. If with_permission
		is specified, permissions are taken into account."""

		matrix = {}

		for controller_key, controller in self.controler_map.items():
			methods_and_permissions = controller._get_available_methods(with_permissions=True)

			allowed_methods = []

			if for_permisison:
				for method, permisison in methods_and_permissions:

					if has_permission(permisison, for_permisison):
						allowed_methods.append(method)
			else:
				allowed_methods = [m for m, p in methods_and_permissions]

			matrix[controller_key] = allowed_methods

		return matrix

	def get_controller_for_url(self, db, request):
		"""Return the controller key for a CMS contoller which is the specified
		controller for a given URL."""

		# Resolve the given URL...
		url_configuration = __resolver__.resolve(
			request.environ,
			allow_cache=False,
			configuration=True
		)

		controller = None

		# If the URL configuration is not found, this means this is either a
		# CMS page or a 404 request - both of which have the same controller
		if not url_configuration:
			return 'deploy-page'

		# Not all URL's have controller details, only "CMSURL" objects do
		if hasattr(url_configuration, 'get_controller'):
			controller = url_configuration.get_controller()

		return controller

__cms__ = CMSControllerManager()

def get_cms_controller(request, controller, method, *args, **kwargs):
	"""Controller bootstrap - this directs requests to corresponding controllers"""

	# Check we have the correct request ID attribute
	if not request.GET.has_key('@') and not request.POST.has_key('@'):
		# Return no request ID
		return JSONErrorResponse(request, 'No Request ID!')

	# @@ TODO: Need to check the contents of @ as it may need to be escaped (especially as it comes back in the JSON)

	controller_obj = __cms__.get_controller(controller)

	if not controller_obj:
		# Return Controller not found
		return JSONErrorResponse(request, args={
			'msg': 'Controller \'%s\' Not Found. Available controllers [%s]' % (
				controller, ', '.join(__cms__.get_all_controllers()))
		})

	# Convert '-' to '_' - as method names can't use '-' but this is better for URL's
	method = method.replace('-', '_')

	# Check the method is not a private, the method exists
	# Can't call private methods (ever) and check the method actually exists
	if method.startswith('_') or not hasattr(controller_obj, method):
		# Return Controller has no method
		return JSONErrorResponse(request, args={
			'msg': 'Controller \'%s\' has no method \'%s\'.' % (controller, method)
		})

	# Get the view
	view = getattr(controller_obj, method)

	# Check this method is exposed
	if not hasattr(view, 'is_exposed') or (hasattr(view, 'is_exposed') and not getattr(view, 'is_exposed')):
		# Return Controller has no method - it does but for security we don't reveal it.
		return JSONErrorResponse(request, args={
			'msg': 'Controller \'%s\' has no method \'%s\'.' % (controller, method)
		})

	# Run the view
	return view(request, *args, **kwargs)