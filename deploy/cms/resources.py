"""Deploy CMS Resources"""

from deploy.core.resources import ResourceList
from deploy.core.resources.types import JSResource, CSSResource

CMS_CSS_RESOURCES = ResourceList()

CMS_JS_RESOURCES = ResourceList(
	JSResource('/media/edit/js/framework.js'),
	JSResource('/media/edit/js/core/utils.js'),
	JSResource('/media/edit/js/core/fsm.js'),
	JSResource('/media/edit/js/core/ixmo.js'),
	JSResource('/media/edit/js/core/text-selection.js'),
	JSResource('/media/edit/js/core/event.js'),
	JSResource('/media/edit/js/core/localization.js'),
	JSResource('/media/edit/js/core/io.js'),

	# FLUID
	JSResource('/media/edit/js/fluid/base.js'),
	JSResource('/media/edit/js/fluid/resource.js'),
	JSResource('/media/edit/js/fluid/icon.js'),
	JSResource('/media/edit/js/fluid/theme.js'),
	JSResource('/media/edit/js/fluid/widget.js'),
	JSResource('/media/edit/js/fluid/desktop.js'),
	JSResource('/media/edit/js/fluid/panel.js'),
	JSResource('/media/edit/js/fluid/button.js'),
	JSResource('/media/edit/js/fluid/menu.js'),
	JSResource('/media/edit/js/fluid/window.js'),
	JSResource('/media/edit/js/fluid/dataset.js'),
	JSResource('/media/edit/js/fluid/toast.js'),
	JSResource('/media/edit/js/fluid/form/form.js'),
	JSResource('/media/edit/js/fluid/form/fieldset.js'),
	JSResource('/media/edit/js/fluid/form/field.js'),
	JSResource('/media/edit/js/fluid/form/fields/text.js'),
	JSResource('/media/edit/js/fluid/form/fields/choice.js'),
	JSResource('/media/edit/js/fluid/form/fields/datetime.js'),

	# Deploy
	JSResource('/media/edit/js/deploy/base.js'),
	JSResource('/media/edit/js/deploy/attachments.js'),
	JSResource('/media/edit/js/deploy/form.js'),
	JSResource('/media/edit/js/deploy/pagination.js'),
	JSResource('/media/edit/js/deploy/controller.js'),
	JSResource('/media/edit/js/deploy/theme.js'),
	JSResource('/media/edit/js/deploy/controllers/administrator.js'),
	JSResource('/media/edit/js/deploy/controllers/site.js'),
	JSResource('/media/edit/js/deploy/init.js'),
	JSResource('/media/edit/js/deploy/gui/standard.js'),
	JSResource('/media/edit/js/deploy/gui/administrator.js'),
	JSResource('/media/edit/js/deploy/editor.js'),

	# Editor.js'),
	JSResource('/media/edit/js/editable/element.js'),
	JSResource('/media/edit/js/editable/text.js'),
	JSResource('/media/edit/js/editable/rule.js'),
	JSResource('/media/edit/js/editable/list.js'),
	JSResource('/media/edit/js/editable/table.js'),
	JSResource('/media/edit/js/editable/image.js'),
	JSResource('/media/edit/js/editable/video.js')
)