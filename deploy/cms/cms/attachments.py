"""Administration classes for the Attachment system of Deploy"""

import datetime
import urlparse

from deploy.config import settings
from deploy.core.pagination import Paginator
from deploy.shortcuts import redirect_to

from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse
from deploy.cms.utils import is_logged_in

__all__ = ('AttachmentController',)

class AttachmentController(CMSController):
	pass