"""Administration classes for the Document system of Deploy"""

import datetime

from deploy.config import settings

from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse
from deploy.cms.models.documents import DeployDocument
from deploy.cms.forms.documents import DeployDocumentAddUpdateForm
from deploy.cms.linkable import __linkable__
from deploy.cms.utils import is_logged_in

__all__ = ('DeployDocumentController',)

class DeployDocumentController(CMSController):

	def after_add(self, request, data, new_document):
		# Set the URL
		new_document.url = new_document.file[0].url
		new_document.created = datetime.datetime.now()
		request.db.commit()

		__linkable__.subscribe(
			new_document.__linkable_id__,
			new_document.__linkable_title__,
			description=new_document.__linkable_description__
		)

	def after_update(self, request, validated_data, document):
		# Set the URL
		document.url = document.file[0].url
		document.last_modified = datetime.datetime.now()
		request.db.commit()

		__linkable__.subscribe(
			document.__linkable_id__,
			document.__linkable_title__,
			description=document.__linkable_description__
		)

	def after_realtime_save(self, request, record, realtime_name, value):
		if realtime_name == 'file':
			# Set the URL
			record.url = record.file[0].url
			request.db.commit()

		# ALWAYS call linkable on realtime
		__linkable__.subscribe(
			record.__linkable_id__,
			record.__linkable_title__,
			description=record.__linkable_description__
		)

	class Meta:
		model = DeployDocument
		controller_name = 'deploy-document'

		gui = False
		gui_verbose_name = 'Document'
		gui_verbose_name_plural = 'Documents'

		forms = {
			'ADD': DeployDocumentAddUpdateForm,
			'UPDATE': DeployDocumentAddUpdateForm,
		}

		pagination_columns = [
			{'name': 'title', 'label': 'Title', 'sortable': True},
			{'name': 'url', 'label': 'URL', 'sortable': True},
		]
		pagination_order_by = 'title'
		pagination_order_by_direction = 'ASC'
