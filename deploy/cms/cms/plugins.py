"""Administration classes for the Plugins system of Deploy"""

import datetime
import urlparse

from deploy.config import settings
from deploy.core.pagination import Paginator
from deploy.shortcuts import redirect_to

from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse
from deploy.cms.utils import is_logged_in

__all__ = ('PluginController',)

class PluginController(CMSController):

	def __init__(self):
		# Override default behaviour as we don't want any checks for model
		# here, or setup of forms etc..
		pass

	@is_logged_in(permission_required='ANY')
	def list(self, request, administrator):
		# Return plugins and controllers

		args = {
			'plugins': [],
			'controllers': [],
		}

		return JSONSuccessResponse(request, args=args)

	class Meta:
		model = None
		controller_name = 'plugins'