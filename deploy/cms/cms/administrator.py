"""Administration classes for the CMS system of Deploy"""

import datetime
import urlparse

from deploy.config import settings
from deploy.core.forms.fields import EmailField
from deploy.core.forms.validators import EmailValidator, RequiredValidator
from deploy.core.http import Request
from deploy.core.pagination import Paginator
from deploy.shortcuts import redirect_to, mail_template
from deploy.tools.users import generate_password, encrypt_password
from deploy.cms import __cms__
from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONFailResponse, JSONErrorResponse
from deploy.cms.models.administrator import DeployAdministrator, DeployAdministratorSession
from deploy.cms.forms.administrator import DeployAdministratorSigninForm, DeployAdministratorAddForm,\
	DeployAdministratorUpdateForm, DeployAdministratorForgottenForm, DeployAdministratorManageMyAccountForm
from deploy.cms.models.pages import DeployPage
from deploy.cms.utils import is_logged_in, expose

__all__ = ('DeployAdministratorController',)

class DeployAdministratorController(CMSController):

	@expose
	def sign_in(self, request):
		"""Sign an Administrator in to the CMS"""

		# Setup the form
		form = DeployAdministratorSigninForm(
			action=self._get_url_for_view('sign-in'),
			method='POST'
		)

		# If the signin request is NOT a POST, return the sign-in form JSON
		# If it is a POST, return a JSON form response (and set the session
		# & cookie if it is a valid response.
		if request.POST:

			form.populate(request.POST)

			if not form.validate():
				# The form is not valid, return the form errors
				return JSONFailResponse(request, args={
					'msg': 'Form is not valid.',
					'form_errors': form.form_errors,
					'field_errors': form.field_errors,
				})

			# Attempt to lookup the Administrator
			the_administrator = request.db.find(DeployAdministrator,
				(DeployAdministrator.active == True) &
				(DeployAdministrator.email == form.validated_data['username'])
			).one()

			if not the_administrator:
				return JSONFailResponse(request, args={'msg': 'Incorrect Sign in details.'})

			if the_administrator.login_attempt_count >= settings.CMS_USER_MAX_LOGIN_ATTEMPT_COUNT:
				last_login = datetime.datetime.now() - the_administrator.login_last_attempt

				if last_login > datetime.timedelta(seconds=settings.CMS_USER_LOGIN_COOLING_OFF_PERIOD):
					the_administrator.login_attempt_count = 0
					request.db.commit()

			if not the_administrator.authenticate(form.validated_data['password']):
				the_administrator.login_last_attempt = datetime.datetime.now()
				the_administrator.login_attempt_count += 1
				request.db.commit()

				remaining_attempt_count = settings.CMS_USER_MAX_LOGIN_ATTEMPT_COUNT - the_administrator.login_attempt_count

				args = {'msg': 'Incorrect Sign in details. %s attempts remaining' % remaining_attempt_count}

				return JSONFailResponse(request, args=args)

			# Return the login count back to zero after a succesful login
			the_administrator.login_attempt_count = 0
			request.db.commit()

			# Destroy any previous sessions
			DeployAdministratorSession.destroy_previous_sessions(request, the_administrator.id)

			# Create a brand new session
			the_administrator_session = DeployAdministratorSession()

			request.db.add(the_administrator_session)

			the_administrator_session.administrator = the_administrator.id

			request.db.commit()

			# Set the Cookie with the JSON success response
			args = {
				'msg': 'Welcome %s' % the_administrator.first_name,
				'administrator': the_administrator
			}

			site_domain = urlparse.urlparse(settings.SITE_URL).netloc

			cookies = [{
				# Session Cookie
				'key':		settings.CMS_ADMINISTRATOR_COOKIE_NAME,
				'value':	the_administrator_session.session_id,
				'domain':	site_domain,
				'max_age':	settings.CMS_ADMINISTRATOR_COOKIE_EXPIRES,
				'path':		settings.CMS_ADMINISTRATOR_COOKIE_PATH,
				'secure':	settings.CMS_ADMINISTRATOR_COOKIE_SECURE,
			},]

			# Return the successful response with the login confirmation
			return JSONSuccessResponse(request, args=args, cookies=cookies)

		else:
			# Return the Form object
			return JSONSuccessResponse(request, args={'form': form})

	@expose
	def forgotten_password(self, request):
		"""Send out the forgotten"""

		form = DeployAdministratorForgottenForm(
			action=self._get_url_for_view('forgotten-password'),
			method='POST'
		)

		if request.POST:

			form.populate(request.POST)

			if not form.validate():
				# The form is not valid, return the form errors
				return JSONFailResponse(request, args={
					'msg': 'Form is not valid.',
					'form_errors': form.form_errors,
					'field_errors': form.field_errors
				})

			# Attempt to lookup the Administrator
			the_administrator = request.db.find(DeployAdministrator,
				(DeployAdministrator.active == True) &
				(DeployAdministrator.email == form.validated_data['username'])
			).one()

			if not the_administrator:
				return JSONFailResponse(request, args={'msg': 'Account not found.'})

			# Generate a new password
			new_password = generate_password()

			# Set the password and log the password change
			the_administrator.password = encrypt_password(new_password)
			the_administrator.last_reset_attempt = datetime.datetime.now()
			request.db.commit()

			# Send the email to confirm
			self._email_password_reset(the_administrator.email, new_password)

			# Return the successful response with the login confirmation
			return JSONSuccessResponse(request, args={'msg': 'Your password has been reset.'})

		else:
			# Return the Form object
			return JSONSuccessResponse(request, args={'form': form})

	@is_logged_in(permission_required='DEVELOPER')
	def list(self, request, administrator):
		return super(DeployAdministratorController, self).list(request, administrator)

	@is_logged_in(permission_required='DEVELOPER')
	def search(self, request, administrator):
		return super(DeployAdministratorController, self).search(request, administrator)

	def on_add(self, request, form, validated_data):
		validated_data['created'] = datetime.datetime.now()
		validated_data['preferences'] = {}
		return validated_data

	def after_add(self, request, validated_data, record):
		# Get password in unencrypted form
		if not request.POST.has_key('password'):
			return False

		passwd = request.POST['password']

		# Email the administrator with a welcome email
		mail_template(
			[record.email],
			'Welcome to %s' % settings.SITE_EDIT_URL,
			'email/cms-welcome.txt',
			template_args={
				'email': record.email,
				'password': passwd,
				'site_url': settings.SITE_EDIT_URL,
			},
			silent=True
		)

		return True

	@is_logged_in(permission_required='DEVELOPER')
	def add(self, request, administrator):
		return super(DeployAdministratorController, self).add(request, administrator)

	@is_logged_in(permission_required='EDITOR')
	def update(self, request, administrator, _id=None):
		"""Update the administrator"""

		if not request.GET.has_key('id') and not request.POST.has_key('id') and not _id:
			return JSONErrorResponse(request, args={
				'msg': 'No if specified',
				'code': 'UPDATE_OBJECT_INVALID_ID',
			})

		# Check an ID has come through
		if _id:
			the_id = _id
		else:
			the_id = request.GET['id'] if request.GET.has_key('id') else request.POST['id']

		# Check we have a valid ID
		try:
			id_ = int(the_id)
		except ValueError:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "id"',
				'code': 'UPDATE_OBJECT_INVALID_ID',
			})

		# Look for the record
		try:
			record = request.db.get(self.Meta.model, id_)
		except Exception:
			return JSONFailResponse(request, args={
				'msg': 'No object with ID %s' % id_,
				'code': 'UPDATE_OBJECT_NOT_FOUND',
			})

		# Check that the user is either updating their own account or that they have developer permissions
		if record != administrator and administrator.get_role_key() != 'DEVELOPER':
			return JSONFailResponse(request, args={
				'msg': 'Permission Denied.',
				'code': 'CMS_PERMISSION_DENIED'
			})

		# Setup the default form
		if record == administrator:
			form = self._get_form('MANAGE_MY_ACCOUNT')

		else:
			form = self._get_form('UPDATE')

		assert form is not None, 'No UPDATE form specified for controller %s' % self.__class__.__name__

		form.populate({'id': id_})

		if request.POST:
			# Populate the form object with the submitted data
			form.populate(request.POST)

			# Validate the submission
			if not form.validate(request=request):
				# Form has not validated, return the form with it's submitted data
				# along with the validation response
				return JSONFailResponse(request, args={
					'form': form,
					'errors': form.field_errors,
				})

			data = form.get_validated_data()

			# Ignore the password if it hasn't been set...
			if data.has_key('password') and data['password'] == '':
				del data['password']

			# Flag for email notification
			trigger_email_notification = False

			# Set the record data
			for k, v in data.items():
				if k == 'email' and v != record.email:
					trigger_email_notification = True

				if k == 'password':
					trigger_email_notification = True

				setattr(record, k, v)

			request.db.commit()

			# Notify user of changes
			if trigger_email_notification:
				mail_template(
					[record.email],
					'Your account details have been changed',
					'email/cms-changed-account.txt',
					template_args={
						'new_email': new_email,
						'old_email': old_email,
						'site_url': settings.SITE_EDIT_URL,
					},
					silent=True
				)

			return JSONSuccessResponse(request, args={
				'msg': '%s updated' % self.Meta.gui_verbose_name,
				'record_id': record.id,
			})

		else:
			form.populate(record.serialize(record, keys=['active', 'first_name', 'last_name', 'email', 'role']))

			# Return the Form object
			return JSONSuccessResponse(request, args={
				'form': form,
				'realtime_validation': {
					'url': self._get_url_for_view('realtime-validate'),
					'_type': 'UPDATE',
				},
				'_identity': unicode(record),
			})

	def on_realtime_save(self, request, record, realtime_name, value):
		if realtime_name == 'email':
			self._email_changed_email(value, record.email)

		if realtime_name == 'password':
			if request.POST.has_key('password'):
				passwd = request.POST['password']
				self._email_password_reset(record.email, passwd)

		return value

	@is_logged_in(permission_required='EDITOR')
	def realtime(self, request, administrator):
		"""Realtime interface to forms"""

		# API fields
		# _id = <optional_field_id>
		# _form = ADD | UPDATE
		# _action = SAVE | VALIDATE
		# _name = <field_name>
		# <field_value> =

		if not request.POST:
			return JSONErrorResponse(request, args={'msg': 'You most POST to the realtime interface!'})

		realtime_id, realtime_form, realtime_action, realtime_name, realtime_value = None, None, None, None, None

		# Sanity check of the validation submission
		valid_forms = ['ADD', 'UPDATE']
		valid_actions = ['SAVE', 'VALIDATE']

		if not request.POST.has_key('_form') or not str(request.POST['_form']).upper() in valid_forms:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "_form" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_form = str(request.POST['_form']).upper()

		if not request.POST.has_key('_action') or not str(request.POST['_action']).upper() in valid_actions:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "_action" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_action = str(request.POST['_action']).upper()

		# We can't save on an ADD as we don't yet have a model to save too
		if realtime_form == 'ADD' and realtime_action == 'SAVE':
			return JSONErrorResponse(request, args={
				'msg': 'Unable to realtime save with an ADD form!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# UPDATE require that a model id be present
		form = None
		if realtime_form == 'UPDATE':
			if not request.POST.has_key('_id') or not request.POST['_id']:
				return JSONErrorResponse(request, args={
					'msg': 'No "_id" specified to realtime UPDATE form',
					'code': 'REALTIME_INVALID_SUBMISSION',
				})

			try:
				realtime_id = int(request.POST['_id'])
			except TypeError:
				return JSONErrorResponse(request, args={
					'msg': '"_id" is not a valid integer!',
					'code': 'REALTIME_INVALID_SUBMISSION',
				})

			# Check that the user is either updating their own account or that they have developer permissions
			if realtime_id != administrator.id and administrator.get_role_key() != 'DEVELOPER':
				return JSONFailResponse(request, args={
					'msg': 'Permission Denied.',
					'code': 'CMS_PERMISSION_DENIED'
				})

			# Setup the default form
			if realtime_id == administrator.id:
				form = self._get_form('MANAGE_MY_ACCOUNT')

			else:
				form = self._get_form('UPDATE')
		else:

			# Get the form
			form = self._get_form(realtime_form)

		if not request.POST.has_key('_name'):
			return JSONErrorResponse(request, args={
				'msg': 'No "_name" specified to realtime forms!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		realtime_name = request.POST['_name']

		if not request.POST.has_key(realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'No "value" specified to realtime validation!',
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		assert form is not None, 'No %s form specified for controller %s' % (
			realtime_form,
			self.__class__.__name__
		)

		if not form.has_field(realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'Form has no field "%s"' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Check the form has an ID field, otherwise add one
		if realtime_form == 'UPDATE':
			if not form.has_field('id'):
				id_field = HiddenField(name='id', datatype=int)
				form.set_field(id_field)

			# Populate the ID
			form.populate({'id': realtime_id})

		# Get the form field
		field = form.get_field(realtime_name)

		# Build the field
		field.build(request)

		# Check the field is a realtime field
		if not field.realtime:
			return JSONErrorResponse(request, args={
				'msg': 'Field "%s" does not support realtime!' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Get the value (cater for multiple values)
		if hasattr(field, '_multiple') and getattr(field, '_multiple'):
			realtime_value = request.POST.getall(realtime_name)
		else:
			realtime_value = request.POST[realtime_name]

		# Set the field field value
		form.populate({field.name: realtime_value})

		# We always perform a validation even if we are saving...

		# @@ TODO: This call to validate should be passing form also, we also may need to populate additional form fields?

		if not field.validate(request=request, form=form):
			return JSONFailResponse(request, args={
				'type': 'REALTIME',
				'code': 'INVALID_FIELD',
				'field': field.name,
				'msg': field.error,
			})

		# Check for linked fields
		linked_fields = []

		if form.is_linked_field(field.name):
			linked_fields = form.get_linked_fields(field.name)

			linked_field_populate = {}

			for linked_field_name in linked_fields:
				if request.POST.has_key(linked_field_name):
					# Populate the form field
					form.populate({linked_field_name: request.POST[linked_field_name]})
					linked_field = form.get_field(linked_field_name)

					# Validate that linked field
					if not linked_field.validate(request=request):
						return JSONFailResponse(request, args={
							'type': 'REALTIME',
							'code': 'INVALID_FIELD',
							'field': linked_field.name,
							'msg': linked_field.error,
						})

		# Check if we are just validating as we return here
		if realtime_action == 'VALIDATE':
			return JSONSuccessResponse(request, args={
				'type': 'REALTIME',
				'code': 'VALID',
				'field': field.name,
				'clean': field.value,
			})

		# We are performing a save so we need to lookup the model instance we are saving too
		record = request.db.find(self.Meta.model, self.Meta.model.id == realtime_id).one()

		if not record:
			return JSONFailResponse(request, args={
				'msg': 'No "%s" found with ID %s!' % (self.Meta.gui_verbose_name, realtime_id),
				'code': 'REALTIME_OBJECT_NOT_FOUND',
			})

		if not hasattr(record, realtime_name):
			return JSONErrorResponse(request, args={
				'msg': 'Model has no field "%s" - can not save' % realtime_name,
				'code': 'REALTIME_INVALID_SUBMISSION',
			})

		# Populate the field with the new value
		value = field.prepare(form=self)

		# Check for any pre-realtime save hooks
		if hasattr(self, 'on_realtime_save'):
			value = self.on_realtime_save(request, record, realtime_name, value)

		# Is the field a realtime attachment field or not?
		if hasattr(self.Meta.model, '__attachments__') and realtime_name in get_model_attachments(self.Meta.model).keys():
			try:
				value = self._sync_attachments(request.db, record, {realtime_name: realtime_value})
			except ApplicationException, e:
				# We can't parse the JSON data, send it back
				return JSONFailResponse(request, args={
					'form': form,
					'errors': {
						key: 'Can not sync attachments - %s' % e,
					}
				})

		else:
			# Update the realtime value
			try:
				setattr(record, realtime_name, value)
			except Exception, e:
				return JSONErrorResponse(request, args={
					'msg': 'Unable to set value in realtime! %s' % e,
					'code': 'REALTIME_FAILED',
				})

		# Update any linked field values
		if linked_fields:
			for linked_field_name in linked_fields:
				linked_field = form.get_field(linked_field_name)

				try:
					setattr(record, linked_field.name, linked_field.prepare(form=self))
				except Exception, e:
					return JSONErrorResponse(request, args={
						'msg': 'Unable to set value for "%s" in realtime! %s' % (linked_field.name, e),
						'code': 'REALTIME_FAILED',
					})

		# Commit the changes
		request.db.commit()

		# Check for any pre-realtime save hooks
		if hasattr(self, 'after_realtime_save'):
			value = self.after_realtime_save(request, record, realtime_name, value)

		return JSONSuccessResponse(request, args={
			'type': 'REALTIME',
			'code': 'SAVED',
			'field': field.name,
			'clean': value,
		})

	@is_logged_in(permission_required='DEVELOPER')
	def remove(self, request, administrator):
		return super(DeployAdministratorController, self).remove(request, administrator)

	@is_logged_in(permission_required='ANY')
	def get_theme(self, request, administrator):
		"""Return available themes"""

		administrator_theme = administrator.get_preference('THEME')

		args = {
			'theme': administrator_theme or settings.CMS_THEMES[0],
		}

		return JSONSuccessResponse(request, args=args)

	@is_logged_in(permission_required='ANY')
	def set_theme(self, request, administrator):
		"""Set a administrator theme"""

		if not request.POST:
			return JSONErrorResponse(request, args={'msg': 'You most POST to set-theme!'})

		if not request.POST.has_key('theme'):
			return JSONErrorResponse(request, args={'msg': 'You must specify a theme to set-theme!'})

		theme = request.GET['theme']

		# Save the theme preference to the administrator

		# Add a container for preferences if there isnt one
		if not administrator.preferences:
			administrator.preferences = {}

		administrator.preferences['THEME'] = theme

		return JSONSuccessResponse(request, args={'msg': 'Theme set.'})

	@is_logged_in(permission_required='ANY')
	def sign_out(self, request, administrator):
		"""Sign the administrator out of the CMS"""

		# Kill all the sessions in the system
		DeployAdministratorSession.destroy_previous_sessions(request, administrator.id)

		# Remove the cookie
		cookies = [
			# Session Cookie
			{
				'key':		settings.CMS_ADMINISTRATOR_COOKIE_NAME,
				'value':	'',
			}
		]

		args = {
			'msg':			'Session Expired.',
			'redirect':		'/',
			'code':			'CMS_SESSION_EXPIRED',
		}

		return JSONSuccessResponse(request, args=args, cookies=cookies)

	@is_logged_in(permission_required='ANY')
	def get_configuration(self, request, administrator):
		"""Return the configuration for the administrator and the GUI"""

		# Get the referer information - this is the actual page the user
		# is viewing
		referer = urlparse.urlparse(request.referer)
		page_request = Request.blank(referer.path)

		administrator_details = {
			# Administrator details
			'created': administrator.created,
			'last_modified': administrator.last_modified,
			'login_last_attempt': administrator.login_last_attempt,
			'login_attempt_count': administrator.login_attempt_count,
			'last_reset_attempt': administrator.last_reset_attempt,
			'first_name': administrator.first_name,
			'last_name': administrator.last_name,
			'id': administrator.id,
			'email': administrator.email,
			'preferences': administrator.preferences,

			# CMS/GUI Controller details
			'controllers': __cms__.get_gui_for_administrator(administrator),

			# Controller for the current page
			'master_controller': __cms__.get_controller_for_url(request.db, page_request),
			'url': referer.path,
		}

		return JSONSuccessResponse(request, args=administrator_details)

	@is_logged_in(permission_required='ANY')
	def get_controllers(self, request, administrator):

		args = {
			'controllers': __cms__.get_controller_method_matrix(for_permission=administrator.role),
		}

		return JSONSuccessResponse(request, args=args)

	@is_logged_in(permission_required='ANY')
	def get_preferences(self, request, administrator):
		"""Return the preferences of the current administrator"""

		return JSONSuccessResponse(request, args=administrator.preferences)

	@is_logged_in(permission_required='ANY')
	def get_preference(self, request, administrator):
		"""Return the preferences of the current administrator"""

		if not request.GET:
			return JSONErrorResponse(request, args={'msg': 'You most GET to get-preference!'})

		if not request.GET.has_key('key'):
			return JSONErrorResponse(request, args={'msg': 'You must specify a Key to set-preference!'})

		if not administrator.preferences:
			return JSONErrorResponse(request, args={'msg': 'Preference not available.'})

		if not administrator.preferences.has_key(request.GET['key']):
			return JSONErrorResponse(request, args={'msg': 'Preference not available.'})

		key = request.GET['key']

		return JSONSuccessResponse(request, args=administrator.preferences[key])

	@is_logged_in(permission_required='ANY')
	def set_preference(self, request, administrator):
		"""Return the preferences of the current administrator"""

		if not request.POST:
			return JSONErrorResponse(request, args={'msg': 'You most POST to set-preference!'})

		if not request.POST.has_key('key'):
			return JSONErrorResponse(request, args={'msg': 'You must specify a Key to set-preference!'})

		if not request.POST.has_key('value'):
			return JSONErrorResponse(request, args={'msg': 'You must specify a Value to set-preference!'})

		# @@ TODO: Validate preference

		# Add a container for preferences if there isnt one
		if not administrator.preferences:
			administrator.preferences = {}

		administrator.preferences[key] = value

		# Save to the DB
		request.db.commit()

		return JSONSuccessResponse(request, args={'args': 'Your Preference has been saved.'})

	@is_logged_in(permission_required='ANY')
	def set_preferences(self, request, administrator):

		# @@ TODO: Add functionality to set multiple preferences

		pass

	@is_logged_in(permission_required='ANY')
	def get_language_table(self, request, administrator):
		"""Return a language table for the administrator"""

		# @@ TODO: Lookup language table for the administrator

		return JSONSuccessResponse(request, args={'string_table': {}})

	@is_logged_in(permission_required='NOUSER')
	def view_activity(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def view_my_activity(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='EDITOR')
	def update_my_preferences(self, request, administrator):
		raise NotImplementedError

	@staticmethod
	def _email_changed_email(new_email, old_email):
		mail_template(
			[old_email, new_email],
			'Your details have been changed',
			'email/cms-changed-email.txt',
			template_args={
				'new_email': new_email,
				'old_email': old_email,
				'site_url': settings.SITE_EDIT_URL,
			},
			template_manager_args={
				'engine': 'jinja',
			},
			silent=True
		)

	@staticmethod
	def _email_password_reset(email, new_password):
		if settings.CMS_EMAIL_PASSWORD:
			mail_template(
				[email],
				'Your password has been reset',
				'email/cms-password-reset.txt',
				template_args={
					'email': email,
					'new_password': new_password,
					'site_url': settings.SITE_EDIT_URL,
				},
				template_manager_args={
					'engine': 'jinja',
				},
				silent=True
			)

			return True

		return False

	# Invalid views
	@is_logged_in(permission_required='NOUSER')
	def upload(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_id_for_url(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_url_for_id(self, request, administrator):
		pass

	class Meta:
		model = DeployAdministrator
		controller_name = 'deploy-administrator'

		gui = False
		gui_verbose_name = 'Administrator'
		gui_verbose_name_plural = 'Administrators'

		forms = {
			'ADD': DeployAdministratorAddForm,
			'UPDATE': DeployAdministratorUpdateForm,
			'MANAGE_MY_ACCOUNT': DeployAdministratorManageMyAccountForm
		}

		pagination_columns = [
			{'name': 'first_name', 'label': 'First Name', 'sortable': True},
			{'name': 'last_name', 'label': 'Last Name', 'sortable': True},
			{'name': 'role', 'label': 'Role', 'sortable': True},
			{'name': 'email', 'label': 'Email', 'sortable': True},
			{'name': 'active', 'label': 'Active', 'sortable': True},
		]

		search_fields = ['first_name', 'last_name', 'email']
		pagination_order_by = 'last_name'
		pagination_order_by_direction = 'ASC'

