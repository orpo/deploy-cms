"""Administration classes for the Site system of Deploy"""

from deploy import __version__ as deploy_version, __build__ as deploy_build
from deploy.config import settings

from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse
from deploy.cms.utils import is_logged_in, expose

__all__ = ('DeploySiteController',)

class DeploySiteController(CMSController):

	def __init__(self):
		# Override default behaviour as we don't want any checks for model
		# here, or setup of forms etc..
		self._controller_name = self.Meta.controller_name

	@expose
	def get_language_table(self, request):
		"""Return the default language table for the site"""

		args = {
			'locale': settings.LOCALE,
			'encoding': settings.ENCODING,
			'string_table': {}
		}

		return JSONSuccessResponse(request, args=args)

	@expose
	def get_theme(self, request):
		"""Return the default theme"""

		args = {
			'theme': settings.CMS_THEMES[0],
			'available_themes': settings.CMS_THEMES,
		}

		return JSONSuccessResponse(request, args=args)

	# @@ Views to be implemented
	@is_logged_in(permission_required='NOUSER')
	def content_pages(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def content_media(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def layout_style_sheets(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def layout_templates(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def development_settings(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def development_about(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def development_logs(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='NOUSER')
	def stats(self, request, administrator):
		raise NotImplementedError

	@is_logged_in(permission_required='EDITOR')
	def system_info(self, request, administrator):
		"""Return information about the system"""

		args = {
			'msg': 'System information',
			'version': deploy_version,
			'build': deploy_build,
		}

		return JSONSuccessResponse(request, args=args)

	# Invalid views
	@is_logged_in(permission_required='NOUSER')
	def list(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def search(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def add(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def update(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def realtime(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def upload(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def remove(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_id_for_url(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_url_for_id(self, request, administrator):
		pass

	class Meta:
		model = None
		gui = False
		controller_name = 'deploy-site'