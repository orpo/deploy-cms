"""Administration classes for the Page system of Deploy"""

import datetime
import json
import urlparse

from deploy.config import settings
from deploy.shortcuts import redirect_to

from deploy.cms.controllers import CMSController
from deploy.cms.json_response import JSONSuccessResponse, JSONErrorResponse, JSONFailResponse
from deploy.cms.models.pages import DeployPage, DeployPageRevision, DeployTemplate
from deploy.cms.forms.pages import DeployPageAddForm, DeployPageUpdateForm, \
	DeployPageEditorUpdateForm, DeployTemplateAddUpdateForm
from deploy.cms.utils import is_logged_in
from deploy.cms.utils.mixins import InpageCMSMixin, LinkableMixin

__all__ = ('DeployPageController', 'DeployTemplateController')

class DeployPageController(CMSController, InpageCMSMixin, LinkableMixin):

	@is_logged_in(permission_required='EDITOR')
	def add(self, request, administrator):
		"""Default CMS add view. On a GET request the Add form is return,
		on a POST the form is validated and the response returned"""

		# Setup the default form
		form = self._get_form('ADD')

		assert form is not None, 'No ADD form specified for controller %s' % self.__class__.__name__

		form.build(request)

		if request.POST:
			# Populate the form object with the submitted data
			form.populate(request.POST)

			# Validate the submission
			if not form.validate(request=request):
				# Form has not validated, return the form with it's submitted data
				# along with the validation response
				return JSONFailResponse(request, args={
					'form': form,
					'errors': form.field_errors,
				})

			data = form.get_data()

			try:
				template_id = int(data['template'])
				del data['template']
			except ValueError:
				form.set_form_error('Invalid template!')

				return JSONFailResponse(request, args={
					'form': form,
					'errors': form.field_errors,
				})

			if hasattr(self, 'on_add'):
				data = self.on_add(request, form, data)

			# Get the template
			template = request.db.get(DeployTemplate, template_id)

			# Form is valid
			new_page = self.Meta.model(**data)
			new_page.created = datetime.datetime.now()
			new_page.last_modified = datetime.datetime.now()
			new_page.meta_data = {}
			new_page.status = 'DRAFT'

			new_page.source_code = template.source_code
			new_page.source_code_language = template.source_code_language
			new_page.source_code_revision = 1

			# Add the new record to the database
			request.db.add(new_page)
			request.db.commit()

			# Look for any post-add hook:
			if hasattr(self, 'after_add'):
				self.after_add(request, data, new_page)

			return JSONSuccessResponse(request, args={
				'msg': 'New Page added',
				'record_id': new_page.id,
			})

		else:
			# Return the Form object
			return JSONSuccessResponse(request, args={
				'form': form,
			})

	@is_logged_in(permission_required='EDITOR')
	def update(self, request, administrator, _id=None):
		"""Default CMS update view. On a GET request the Update form is return,
		on a POST the form is validated and the response returned"""

		# Check we have an identifier
		if not request.GET.has_key('id') and not request.POST.has_key('id') and not _id:
			return JSONErrorResponse(request, args={
				'msg': 'No id specified',
				'code': 'UPDATE_OBJECT_INVALID_ID'
			})

		the_id, record = None, None

		# Get the identifier
		if _id:
			the_id = _id

		elif request.GET.has_key('id') or request.POST.has_key('id'):
			the_id = request.GET['id'] if request.GET.has_key('id') else request.POST['id']

		# Get the record
		if the_id is not None:
			try:
				the_id = int(the_id)
			except ValueError:
				return JSONErrorResponse(request, args={
					'msg': 'Invalid "id"',
					'code': 'UPDATE_OBJECT_INVALID_ID'
				})

			record = request.db.find(
				self.Meta.model,
				self.Meta.model.id == the_id
			).one()

		# Attempt to get the record
		if not record:
			args = {
				'msg': 'No "%s" found!' % self.Meta.model.__class__.__name__,
				'code': 'UPDATE_OBJECT_NOT_FOUND',
			}
			return JSONErrorResponse(request, args=args)

		# Setup the default form - non super users
		# can have an alternative form
		if self._has_form('UPDATE_EDITOR') and not administrator.is_super():
			form = self._get_form('UPDATE_EDITOR')
		else:
			form = self._get_form('UPDATE')

		assert form is not None, 'No UPDATE form specified for controller %s' % self.__class__.__name__

		form.build(request)
		form.populate({'id': the_id})

		if request.POST:
			# Populate the form object with the submitted data
			form.populate(request.POST)

			# Validate the submission
			if not form.validate(request=request):
				# Form has not validated, return the form with it's submitted data
				# along with the validation response
				return JSONFailResponse(request, args={
					'form': form,
					'errors': form.field_errors
				})

			validated_data = form.get_validated_data()

			# Look for any pre-update hook:
			if hasattr(self, 'on_update'):
				validated_data = self.on_update(request, form, validated_data, record)

			if validated_data is None:
				return JSONFailResponse(request, args={
					'msg': 'Unable to update page!',
					'code': 'UPDATE_STOPPED'
				})

			# Save the revision
			revision = DeployPageRevision(
				created=datetime.datetime.now(),
				source_code=record.source_code,
				source_code_language=record.source_code_language,
				source_code_revision=record.source_code_revision,
				page=record
			)

			request.db.add(revision)
			request.db.commit()

			# Set the record data
			record.last_modified = datetime.datetime.now()
			record.status = validated_data['status']
			record.title = validated_data['title']
			record.url = validated_data['url']
			record.source_code = validated_data['source_code']
			record.source_code_language = validated_data['source_code_language']
			record.source_code_revision += 1

			request.db.commit()

			# Look for any post-update hook:
			if hasattr(self, 'after_update'):
				self.after_update(request, validated_data, record)

			return JSONSuccessResponse(request, args={
				'msg': 'Page updated',
				'record_id': record.id,
			})

		else:
			form.populate(record.serialize(
				record,
				keys=['status', 'title', 'url', 'source_code_language',
				'source_code', 'source_code_revision', 'last_modified'
				])
			)

			# Return the Form object
			return JSONSuccessResponse(request, args={
				'form': form,
				'prevent_realtime_save': True if administrator.is_super() else False,
				'_identity': unicode(record),
			})

	@is_logged_in(permission_required='EDITOR')
	def remove(self, request, administrator):
		"""Default CMS remove view"""

		if not request.GET.has_key('id') and not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No if specified',
				'code': 'REMOVE_OBJECT_INVALID_ID'
			})

		the_id = request.GET['id'] if request.GET.has_key('id') else request.POST['id']

		try:
			id_ = int(the_id)
		except ValueError:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid "id"',
				'code': 'REMOVE_OBJECT_INVALID_ID',
			})

		# Lookup the record..
		# @@ TODO: Add support for arbitary primary keys as opposed to just id?
		record = request.db.find(self.Meta.model, self.Meta.model.id == id_).one()

		if not record:
			return JSONErrorResponse(request, args={
				'msg': 'No "Page" found with ID %s!' % id_,
				'code': 'REMOVE_OBJECT_NOT_FOUND',
			})

		if request.POST and request.POST.has_key('confirm'):
			# Remove the revisions
			revision_records = request.db.find(
				DeployPageRevision,
				DeployPageRevision.page == record
			)

			for revision in revision_records:
				request.db.remove(revision)

			# Remove the record
			request.db.remove(record)
			request.db.commit()

			return JSONSuccessResponse(request, args={
				'msg': 'Page removed',
				'record_id': record.id,
			})

		return JSONSuccessResponse(request, args={'_identity': unicode(record)})

	@is_logged_in(permission_required='EDITOR')
	def get_id_for_url(self, request, administrator):
		# Check we have an identifier
		if not request.GET.has_key('url') and not request.POST.has_key('url'):
			return JSONErrorResponse(request, args={
				'msg': 'No url specified',
				'code': 'INVALID_URL_TO_GET_ID',
			})

		the_url = request.GET['url'] if request.GET.has_key('url') else request.POST['url']

		record = request.db.find(
			self.Meta.model,
			self.Meta.model.url == unicode(the_url)
		).one()

		if not record:
			return JSONFailResponse(request, args={
				'msg': 'No page with url "%s"' % the_url,
				'code': 'INVALID_URL_TO_GET_ID',
			})

		return JSONSuccessResponse(request, args={
			'msg': 'Got id.',
			'id': record.id,
		})

	@is_logged_in(permission_required='EDITOR')
	def inpage_sync(self, request, administrator):

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to save page',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('content_map'):
			return JSONErrorResponse(request, args={
				'msg': 'No content map specified',
				'code': 'NO_CONTENT_MAP'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (
					self.Meta.gui_verbose_name,
					record_id
				),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Convert the content map from a JSON string to a dict
		try:
			content_map = json.loads(request.POST['content_map'])
		except Exception:
			return JSONErrorResponse(request, args={
				'msg': 'Unable to parse content map',
				'code': 'INVALID_CONTENT_MAP'
			})

		# Create a page revision
		revision = DeployPageRevision(
			created=datetime.datetime.now(),
			source_code=record.source_code,
			source_code_language=record.source_code_language,
			source_code_revision=record.source_code_revision,
			page=record
		)

		request.db.add(revision)

		# Page sync
		record.sync_content(content_map)
		record.last_modified = datetime.datetime.now()
		record.source_code_revision += 1

		# DONT commit until the end as we don't want to have to remove the revision
		request.db.commit()

		return JSONSuccessResponse(request, args={'msg': 'Page saved.'})

	class Meta:
		model = DeployPage
		controller_name = 'deploy-page'

		gui = False
		gui_inpage_editing = True
		gui_verbose_name = 'Page'
		gui_verbose_name_plural = 'Pages'
		gui_inpage_editing = True

		forms = {
			'ADD': DeployPageAddForm,
			'UPDATE': DeployPageUpdateForm,
			'UPDATE_EDITOR': DeployPageEditorUpdateForm,
		}

		pagination_columns = [
			{'name': 'url', 'label': 'URL', 'sortable': True},
			{'name': 'title', 'label': 'Title', 'sortable': True},
			{'name': 'status', 'label': 'Status'},
		]
		pagination_order_by = 'title'
		pagination_order_by_direction = 'ASC'

		editor_image_model_id = 'url'
		editor_image_model_attachment = 'images'
		editor_image_variation = 'inpage'

class DeployTemplateController(CMSController):

	# Invalid views
	@is_logged_in(permission_required='NOUSER')
	def upload(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_id_for_url(self, request, administrator):
		pass

	@is_logged_in(permission_required='NOUSER')
	def get_url_for_id(self, request, administrator):
		pass

	class Meta:
		model = DeployTemplate
		controller_name = 'deploy-template'

		gui = False
		gui_verbose_name = 'Template'
		gui_verbose_name_plural = 'Templates'

		forms = {
			'ADD': DeployTemplateAddUpdateForm,
			'UPDATE': DeployTemplateAddUpdateForm,
		}

		pagination_columns = [
			{'name': 'name', 'label': 'Name', 'sortable': True},
			{'name': 'source_code_language', 'label': 'Source Code Language', 'sortable': True},
		]
		pagination_order_by = 'name'
		pagination_order_by_direction = 'ASC'
