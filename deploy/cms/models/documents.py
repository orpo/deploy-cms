"""Module contains Model definitions for CMS Documents"""

from storm.locals import *
from pydispatch import dispatcher

from deploy.config import settings
from deploy.core.models import Model

from deploy.cms.attachments import Attachments
from deploy.cms.linkable import subscribe_linkable, unsubscribe_linkable

__all__ = ('DeployDocument',)

class DeployDocument(Model):
	"""Document is used to represent an instance of a CMS document."""

	__storm_table__ = 'deploy_document'

	id = Int(primary=True)
	created = DateTime()
	last_modified = DateTime()
	url = Unicode()
	title = Unicode()
	description = Unicode()

	file_path = Unicode()

	def __unicode__(self):
		return u'Deploy Document: %s' % self.url

	@property
	def complete_url(self):
		"""Return the full URL of the Page"""
		return self.url

	@property
	def __linkable_id__(self):
		return self.url

	@property
	def __linkable_title__(self):
		return u'%s (Document)' % self.title

	@property
	def __linkable_description__(self):
		return self.description

DeployDocument.file = Attachments(
	DeployDocument,
	DeployDocument.file_path,
	settings.CMS_DOC_ATTACHMENT_PATH,
	max=1
)

# Add/Update subscribes are delayed - see document controller

dispatcher.connect(
	unsubscribe_linkable,
	signal='Deploy.DeployDocument.after_remove'
)
