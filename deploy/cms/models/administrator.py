"""Deploy CMS Administrator Models"""

import datetime

from storm.locals import *

from deploy.config import settings
from deploy.core.models import Model
from deploy.tools.sessions import SessionModel
from deploy.tools.users import UserModel

from deploy.cms.properties import ChoiceField

__all__ = ('DeployAdministrator', 'DeployAdministratorSession',
	'DeployAdministratorAudit')

class DeployAdministrator(UserModel):
	"""Deploy CMS Administrator - A user with access to the administration
	interface of Deploy CMS"""

	__storm_table__ = 'deploy_administrator'

	preferences = Pickle(default={})
	role = ChoiceField(map=settings.CMS_PERMISSIONS)

	def __unicode__(self):
		return u'Administrator: %s (%s)' % (self.get_name(), self.role)

	def get_name(self):
		return '%s %s' % (self.first_name, self.last_name)

	def get_unique_identifier(self):
		"""Return a unique identifier for the model"""

		return self.email

	def get_role_key(self):
		"""Get the correct role key"""
		from deploy.cms.utils import get_key_for_choices
		return get_key_for_choices(self.role, settings.CMS_PERMISSIONS)

	def has_preference(self, pref):
		if self.preferences:
			if self.preferences.has_key(pref):
				return True

		return False

	def get_preference(self, pref):
		if self.preferences:
			if self.preferences.has_key(pref):
				return self.preferences[pref]

		return None

	def set_preference(self, pref, value):
		if not self.preferences:
			self.preferences = {pref: value}
		else:
			self.preferences[pref] = value

	def is_super(self):
		return settings.CMS_SUPER_PERMISSION == self.get_role_key()

	def to_dict(self):
		return {
			'active': self.active,
			'created': self.created,
			'last_modified': self.last_modified,
			'first_name': self.first_name,
			'last_name': self.last_name,
			'email': self.email,
			'role': self.get_role_key(),
			'role_formatted': self.role,
			'preferences': self.preferences or {},
		}


class DeployAdministratorSession(SessionModel):
	"""A Deploy Administrators login session while using the Deploy CMS
	Administration interface"""

	__storm_table__ = 'deploy_administrator_session'

	administrator_id = Int()
	administrator = Reference(administrator_id, DeployAdministrator.id)

	@staticmethod
	def retrieve(request):
		"""Retrieve an administrator session for a given request"""

		# NOTE: There is a workaround here to allow deploy to work with
		# early versions of webob

		# Look for the session cookie...
		if hasattr(request.cookies, 'has_key'):
			# @@ TODO: This is to be removed eventually once webob support has been checked completely
			if not request.cookies.has_key(settings.CMS_ADMINISTRATOR_COOKIE_NAME):
				# Cookie does not exist
				return None
		else:
			if not settings.CMS_ADMINISTRATOR_COOKIE_NAME in request.cookies:
				# Cookie does not exist
				return None

		# Get the session ID
		sid = request.cookies[settings.CMS_ADMINISTRATOR_COOKIE_NAME]

		# Lookup the session from the DB
		session = request.db.find(
			DeployAdministratorSession,
			DeployAdministratorSession.session_id == sid
		).one()

		if not session:
			return None

		# Check for session expiry
		expire_after = datetime.datetime.now() - settings.CMS_USER_EXPRIY

		if not session.last_modified > expire_after:
			request.db.remove(session)
			request.db.commit()
			return None

		# Check we have a valid administrator
		if session.administrator is None:
			request.db.remove(session)
			request.db.commit()
			return None

		# Update the last modified time
		session.last_modified = datetime.datetime.now()
		request.db.commit()

		return session

	@staticmethod
	def destroy_previous_sessions(request, administrator_id):
		"""Destroy all/any sessions for a given administrator id"""

		# Lookup any previous sessions
		previous_sessions = request.db.find(
			DeployAdministratorSession,
			DeployAdministratorSession.administrator_id == administrator_id
		)

		if previous_sessions:
			# Remove each one by one
			for session in previous_sessions:
				request.db.remove(session)

			request.db.commit()

class DeployAdministratorAudit(Model):
	"""A Deploy Adminstrators Audit History"""

	__storm_table__ = 'deploy_administrator_audit'

	id = Int(primary=True)
	created = DateTime()
	activity = Unicode()
	message = Unicode()

	administrator_id = Int()
	administrator = Reference(administrator_id, DeployAdministrator.id)
