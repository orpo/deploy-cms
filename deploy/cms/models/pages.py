"""Module contains Model definitions for CMS Page's and Page Templates"""

import re
import datetime

from storm.locals import *
from pydispatch import dispatcher
from chump import RegionParser

from deploy.config import settings
from deploy.core.models import Model
from deploy.core.urls import URL_CHANGE_FREQUENCY
from deploy.core.templates import TemplateManager

from deploy.cms.attachments import Attachments
from deploy.cms.properties import ChoiceField
from deploy.cms.linkable import subscribe_linkable, unsubscribe_linkable

__all__ = ('DeployPage', 'DeployPageRevision', 'DeployTemplate')

PAGE_STATUS = (
	('DRAFT', 'Unpublished'),
	('LIVE', 'Published'),
	('ARCHIVED', 'Archived'),
)

class DeployPage(Model):
	"""Page is used to represent an instance of a CMS page."""

	# 1. Pages are created from a COPY of a base template
	# 2. Pages are rendered and edit regions can be altered
	# 3. When the pae is saved, the source is sent back and edit
	# regions are parsed out and replaced
	# in the page source. (A revision is then saved)

	__storm_table__ = 'deploy_page'

	id = Int(primary=True)
	created = DateTime()
	last_modified = DateTime()
	url = Unicode()
	title = Unicode()
	status = ChoiceField(map=PAGE_STATUS)

	source_code = Unicode()
	source_code_language = Unicode()
	source_code_revision = Int()

	meta_data = Pickle()

	image_paths = Unicode()

	# Linkable info
	__linkable_id__ = url
	__linkable_description__ = None

	def __unicode__(self):
		return u'Deploy Page: %s' % self.url

	def render(self, request=None):
		"""Render a Page"""
		template_manager = TemplateManager(
			engine=self.source_code_language,
			request=request
		)

		template_manager.parse(self.source_code)

		return template_manager.render(data={'page': self})

	def sync_content(self, raw_content_map):
		"""Sync the source code with the raw content"""

		# Setup the parser
		parser = RegionParser(
			tags=settings.CMS_PARSE_TAGS,
			classes=settings.CMS_PARSE_CLASSES
		)

		# Parse the source code
		parser.parse(self.source_code)

		# Loop over the regions and look to replace the content
		for region in parser:
			if region.id in raw_content_map:
				region.set_content(raw_content_map[region.id])

		# All done, return the result to source code
		self.source_code = unicode(parser)

	@property
	def complete_url(self):
		"""Return the full URL of the Page"""
		return self.url

	@property
	def is_homepage(self):
		"""Return a bool to specify if this page is the 'homepage'"""
		return True if self.url == u'/' else False

	@property
	def __linkable_title__(self):
		return u'%s (Page)' % self.title

DeployPage.images = Attachments(
	DeployPage,
	DeployPage.image_paths,
	settings.CMS_PAGE_ATTACHMENT_PATH,
	max=settings.CMS_PAGE_ATTACHMENT_MAX,
	variations={
		'inpage': {
			'process': 'deploy.cms.utils.variations.inpage_attachment',
			'args': []
		},
	}
)

dispatcher.connect(
	subscribe_linkable,
	signal='Deploy.DeployPage.after_add'
)

dispatcher.connect(
	subscribe_linkable,
	signal='Deploy.DeployPage.after_update'
)

dispatcher.connect(
	unsubscribe_linkable,
	signal='Deploy.DeployPage.after_remove'
)

class DeployPageRevision(Model):

	__storm_table__ = 'deploy_page_revision'

	id = Int(primary=True)
	created = DateTime()
	last_modified = DateTime()

	source_code = Unicode()
	source_code_language = Unicode()
	source_code_revision = Int()

	page_id = Int()
	page = Reference(page_id, DeployPage.id)

	def __unicode__(self):
		return u'Deploy Page Revision: %s (%s)' % (
			self.page.url,
			self.source_code_revision
		)

class DeployTemplate(Model):

	__storm_table__ = 'deploy_template'

	id = Int(primary=True)
	name = Unicode()
	source_code = Unicode()
	source_code_language = Unicode()

	def __unicode__(self):
		return u'Deploy Template: %s (%s)' % (
			self.name,
			self.source_code_language
		)
