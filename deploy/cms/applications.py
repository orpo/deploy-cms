"""Deploy WSGI Middleware Application: CMS"""

import os.path
import warnings

from chump import RegionParser

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError
from deploy.core.http import Request
from deploy.core.http.exceptions import PageNotFound
from deploy.core.resources import ResourceList
from deploy.core.urls import URL, Resolver
from deploy.plugins import __plugins__
from deploy.utils.importing import import_module, import_object

from deploy.cms import __cms__
from deploy.cms.resources import CMS_JS_RESOURCES, CMS_CSS_RESOURCES

try:
	from PIL import Image
except ImportError:
	warnings.warn(
		'You are using deploy.cms without PIL installed - some functionality may not work correctly',
		RuntimeWarning
	)

class DeployCMSApp(object):
	"""Middleware Application responsible for the CMS system"""

	def __init__(self, app):
		self.app = app

		# Sanity check here - we need to make sure the CMS plugin is installed
		if not __plugins__.has_plugin('deploy.cms'):
			raise ConfigurationError('The application has a CMS application but does not have "deploy.cms" installed!')

		# Check if we are using local or remote resources, typically on standalone
		# deploy instances remote resources are used for performance. However, local
		# resources can still be used in addition.

		if not settings.CMS_REMOTE_RESOURCES:
			self.cms_js_resources = CMS_JS_RESOURCES
			self.cms_css_resources = CMS_CSS_RESOURCES
		else:
			self.cms_js_resources = ResourceList()
			self.cms_css_resources = ResourceList()

		# Are there any default resources
		try:
			default_resources = import_module('%s.resources' % settings.APPLICATION_NAME)
		except ImportError:
			pass
		else:
			# CSS?
			if hasattr(default_resources, 'CMS_CSS_RESOURCES'):
				self.cms_css_resources += default_resources.CMS_CSS_RESOURCES

			# JS?
			if hasattr(default_resources, 'CMS_JS_RESOURCES'):
				self.cms_js_resources += default_resources.CMS_JS_RESOURCES

		DEPLOY_CMS_ROOT = os.path.dirname(os.path.abspath(__file__))

		# Append the default deploy template path
		if isinstance(settings.TEMPLATE_LOCATIONS, (list, tuple)):
			settings.TEMPLATE_LOCATIONS.insert(
				len(settings.TEMPLATE_LOCATIONS) - 1, os.path.join(DEPLOY_CMS_ROOT, 'templates')
			)

		# Late load as we need to load settings first
		from deploy.cms.models.administrator import DeployAdministratorSession

		# Load the CMS controllers (has to be first)
		__cms__.load('deploy.cms.cms')

		for plugin in __plugins__.all():
			# Don't reload the CMS plugin
			if not unicode(plugin) == 'deploy.cms':
				# Load the plugin controllers
				success = __cms__.load('%s.cms' % plugin, silent=True)

				# Load the plugin CMS resources (if the plugin controllers loaded)
				if not success:
					continue

				try:
					cms_module = import_module('%s.cms' % plugin)
				except ImportError:
					continue

				else:
					# CMS resources are either in the cms.py file or in
					# a sub module
					if hasattr(cms_module, 'resources'):
						# CSS?
						if hasattr(cms_module.resources, 'CMS_CSS_RESOURCES'):
							self.cms_css_resources += cms_module.resources.CMS_CSS_RESOURCES

						# JS?
						if hasattr(cms_module.resources, 'CMS_JS_RESOURCES'):
							self.cms_js_resources += cms_module.resources.CMS_JS_RESOURCES

					else:
						# CSS?
						if hasattr(cms_module, 'CMS_CSS_RESOURCES'):
							self.cms_css_resources += cms_module.CMS_CSS_RESOURCES

						# JS?
						if hasattr(cms_module, 'CMS_JS_RESOURCES'):
							self.cms_js_resources += cms_module.CMS_JS_RESOURCES

		# Set the CMS url dispatcher
		prefix = settings.CMS_URL_PREFIX

		if not prefix.startswith('/'):
			prefix = '/%s' % prefix

		if not prefix.endswith('/'):
			prefix = '%s/' % prefix

		self.cms_request_url_prefix = prefix
		self.cms_url_resolver = Resolver()

		cms_urls = (
			URL(r'%s([a-z\-_]+)/([a-z\-_]+)/' % self.cms_request_url_prefix,
				'deploy.cms.get_cms_controller', host=settings.SITE_EDIT_URL),
		)

		self.cms_url_resolver.add(cms_urls)

		# Compile and Minify the resources
		#self.cms_js_resources.minify()
		#self.cms_css_resources.compile()

		# Setup the default view for view page/build a page
		self._view_page = None
		self._build_a_page = None

		try:
			self._view_page = import_object(settings.CMS_VIEW_PAGE)
		except ImportError:
			raise ConfigurationError('CMS View page "%s" could not be imported!' % settings.CMS_VIEW_PAGE)

		try:
			self._build_a_page = import_object(settings.CMS_BUILD_PAGE)
		except ImportError:
			raise ConfigurationError('CMS Build a page "%s" could not be imported!' % settings.CMS_BUILD_PAGE)

	def __call__(self, environ, start_response):

		# Late load as we need to load settings first...
		from deploy.cms.models.administrator import DeployAdministratorSession

		# Set our flag for CMS initialisation
		self.init_cms_hooks = False

		hostname = environ['HTTP_HOST'] if environ.has_key('HTTP_HOST') else environ['SERVER_NAME']

		if hostname == settings.SITE_EDIT_URL:
			# Set the edit mode status
			environ['com.deploycms.cms.mode'] = 'EDIT'

			# Check if we have a session already, this allows for other authentication subsystems
			# to produce an administrator record
			if environ.has_key('com.deploycms.cms.administrator_session') and \
				environ['com.deploycms.cms.administrator_session']:
				# @@ TODO: Check the administrator record is valid? This is a Hook
				# for other authentication systems
				pass

			else:
				# Retrieve the admin record
				environ['com.deploycms.cms.administrator_session'] = DeployAdministratorSession.retrieve(
					Request(environ)
				)
		else:
			# Set the live mode status
			environ['com.deploycms.cms.mode'] = 'LIVE'

		# Check if this is a CMS or standard view...
		if environ['com.deploycms.cms.mode'] == 'EDIT' and environ['PATH_INFO'].startswith(self.cms_request_url_prefix):
			# CMS Edit Views:
			# These requests are always identified by the CMS URL prefix, we do not pass down the pipeline -
			# this application is responsible for returning a response

			result = self.cms_url_resolver.resolve(environ)

			if not result:
				raise PageNotFound(
					environ['PATH_INFO'],
					available_urls=self.cms_url_resolver.get_all_urls()
				)

			endpoint, args, kwargs = result

			action = self._load_endpoint(endpoint)

			request = Request(environ)

			# An responder/view action was found, run it and fetch the response
			if kwargs:
				response = action(request, **kwargs)
			elif args:
				response = action(request, *args)
			else:
				response = action(request)

			# All done, return the response we have generated
			return response(environ, start_response)

		else:
			# CMS View process:
			# The middleware first passes the request on (as CMS requests are of a lower priority)
			# to the rest of the application pipeline, it waits to catch a 404 error. At this stage
			# it can then attempt to provide a CMS page, if not it can either re-raise a 404 or,
			# if it is in edit mode, provide a special 'add page' facility

			response = None

			def cms_init_buffer(status, headers, exc_info=None):
				"""Utility function to set the CMS hooks flag if a response is suitable
				for the CMS"""

				# Assume a default content type - one which would not require CMS hooks
				content_type = 'text/plain'

				# Look for a content type header
				for name, value in headers:
					if name.lower() == 'content-type':
						content_type = value

				# Check the status and content type are suitable for the CMS to init
				# @@ TODO: Add support for starting the CMS with a 404 status ('Add this page' feature)
				if status == '200 OK' and content_type.startswith('text/html') and \
					environ['com.deploycms.cms.mode'] == 'EDIT':
					self.init_cms_hooks = True

				cms_init_buffer.status = status
				cms_init_buffer.headers = headers
				cms_init_buffer.exc_info = exc_info

				return True

			try:
				# Run the application and attempt to get a response
				response = self.app(environ, cms_init_buffer)

			except PageNotFound, e:
				# The application hasn't returned a valid response, check if the user
				# is requesting a CMS page....

				# Look for a CMS page, must have a db handle to do this
				if not 'com.deploycms.db.conn' in environ:
					raise e

				try:
					response = self._view_page(Request(environ))
					response = response(environ, cms_init_buffer)
				except PageNotFound, e:
					# CMS page hasn't been found, if we are in edit mode, show
					# the 'build a page'
					if not environ['com.deploycms.cms.mode'] == 'EDIT':
						raise e

					elif [u for u in settings.URL_PROTECTED_PATH_LIST if environ['PATH_INFO'].startswith(u)]:
						# Don't show for protected URL's
						raise e

					elif not environ['com.deploycms.cms.administrator_session']:
						# Don't show if there is no user signed in
						raise e

					elif environ['com.deploycms.url_resolved']:
						# The URL matched to a view but the view decided to display a 404
						# so don't allow processing to continue
						raise e

					response = self._build_a_page(Request(environ))
					response = response(environ, cms_init_buffer)

					# Overload the CMS init flag
					self.init_cms_hooks = True

			if self.init_cms_hooks:
				# By adding the CMS hooks we are altering the response object so
				# we need to
				response = self._add_cms_hooks(response)

				joined_response = ''.join(response)

				new_content_length = len(joined_response)

				headers = []

				for name, value in cms_init_buffer.headers:
					if name.lower() == 'content-length':
						value = str(new_content_length)

					headers.append((name, value))

				start_response(cms_init_buffer.status, headers, exc_info=cms_init_buffer.exc_info)

			else:
				# Response has been buffered until now, trigger the start_response
				start_response(
					cms_init_buffer.status,
					cms_init_buffer.headers,
					exc_info=cms_init_buffer.exc_info
				)

			return response

	def _load_endpoint(self, path):
		mod_act = path.split('.')
		import_str = '.'.join(mod_act[:-1])
		action_str = mod_act[-1]

		# Check to see if the action has already been imported
		if action_str in globals():
			return globals()[action_str]

		else:
			# @@ TODO: Switch to import_module in _load_endpoint
			module = __import__(import_str, globals(), locals(), [action_str], -1)
			return getattr(module, action_str)

	def _add_cms_hooks(self, response):
		"""Inject the CMS init hooks in to a HTML response"""

		new_response = []

		# Setup the parser
		parser = RegionParser(
			tags=['body'],
			attributes=[settings.CMS_ATTRIBUTE],
			strict_id=False
		)

		# Parse the response
		parser.parse(''.join(response))

		# Stop here if we don't have any matching regions
		if not parser.regions:
			return response

		# Go over each region and look for processing instructions
		for region in parser:
			processing_instruction = region.attribs.get(settings.CMS_ATTRIBUTE, None)

			if processing_instruction is None:
				continue

			if processing_instruction == 'init':
				# Hook for the Javascript
				js_html = self.cms_js_resources.as_html()

				if settings.CMS_REMOTE_RESOURCES:
					remote_resource_version = settings.CMS_REMOTE_RESOURCES_URL % settings.CMS_REMOTE_RESOURCES_VERSION
					remote_resource_loader = '<script src="%s"></script>' % remote_resource_version
					js_html = '%s\n%s' % (remote_resource_loader, js_html)

				region.append_content(js_html)

		return [str(parser), ]
