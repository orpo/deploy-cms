"""Custom Deploy CMS Storm ORM Properties"""

import datetime

from storm.info import get_cls_info, get_obj_info
from storm.properties import SimpleProperty, List
from storm.variables import Variable, ListVariable

__all__ = ('ChoiceField')

class ChoiceVariable(Variable):
	__slots__ = ("_map",)

	def __init__(self, *args, **kwargs):
		self._map = kwargs['map']
		del kwargs['map']

		Variable.__init__(self, *args, **kwargs)

	def parse_set(self, value, from_db):
		if from_db:
			return value

		if not value in self._map.keys():
			# The item sent through is NOT a key value, attempt to translate it from
			# a value to a key value
			for k, v in self._map.items():
				if v == value:
					return k

			# Nothing was found, raise value
			raise ValueError("Invalid choice value: %s" % repr(value))

		return value

	def parse_get(self, value, to_db):
		if to_db:
			return value
		try:
			return self._map[value]
		except KeyError:
			if value in self._map.values():
				for k, v in self._map.items():
					if v == value:
						return k

			raise ValueError("Invalid choice value: %s" % repr(value))

class ChoiceField(SimpleProperty):

	variable_class = ChoiceVariable

	def __init__(self, name=None, primary=False, **kwargs):

		if kwargs.has_key('map') and not isinstance(kwargs['map'], dict):
			kwargs['map'] = dict(kwargs['map'])

		SimpleProperty.__init__(self, name, primary, **kwargs)
