"""AWS S3 file storage"""

import os
import os.path
import mimetypes

try:
	from cStringIO import StringIO
except ImportError:
	from StringIO import StringIO

from deploy.config import settings
from deploy.utils import s3
from deploy.cms.store import FileStoreInterface, FileInterface

class S3FileStore(FileStoreInterface):

	def __init__(self, bucket=None, access_key=None, secret_key=None, acl=None, preload_metadata=None,
				calling_format=s3.CallingFormat.PATH, headers={}, auto_unique=True):

		# AWS arguments
		self.bucket = bucket if bucket is not None else settings.CMS_AWS_BUCKET
		self.acl = acl if acl is not None else settings.CMS_AWS_DEFAULT_ACL
		self.calling_format = calling_format
		self.headers = headers

		access_key = access_key if access_key is not None else settings.CMS_AWS_ACCESS_KEY
		secret_key = secret_key if secret_key is not None else settings.CMS_AWS_SECRET_KEY

		self.connection = s3.AWSAuthConnection(access_key, secret_key)

		self.generator = s3.QueryStringAuthGenerator(
			access_key, secret_key,
			calling_format=self.calling_format,
			is_secure=settings.CMS_AWS_SECURE_URLS
		)

		self.generator.set_expires_in(settings.CMS_AWS_QUERYSTRING_EXPIRE)
		self.bucket_prefix = settings.CMS_STORE_PREFIX

		# Internal entries
		self._entries = {}
		self._preload_metadata = preload_metadata if preload_metadata is not None else settings.CMS_AWS_PRELOAD_METADATA

		super(S3FileStore, self).__init__()

	def get_tmp_name(self, prefix='upload-', suffix=None,
					path=settings.CMS_ATTACHMENT_TMP_PREFIX, chars=6):

		return super(S3FileStore, self).get_tmp_name(
			prefix=prefix, suffix=suffix, path=path, chars=chars
		)

	def _get_path(self, path):
		# Useful for windows' paths

		return os.path.join(
			self.bucket_prefix,
			os.path.normpath(path).replace('\\', '/')
		)

	def _read(self, path, start_range=None, end_range=None):
		if start_range is None:
			headers = {}
		else:
			headers = {'Range': 'bytes=%s-%s' % (start_range, end_range)}

		response = self.connection.get(self.bucket, path, headers)

		if response.http_response.status not in (200, 206):
			raise IOError("S3StorageError: %s" % response.message)

		headers = response.http_response.msg

		return response.object.data, headers.get('etag', None), headers.get('content-range', None)

	def open(self, path, mode='rb', strict=True):
		# Get the clean path
		path = self._get_path(path)

		# Create the file handle and read in contents
		remote_file = S3File(path, mode, self)

		return remote_file

	def save(self, path, contents, mode='wb', strict=True):
		# Get the clean path
		path = self._get_path(path)

		# Get a file handle
		f = S3File(path, mode, self)
		f.write(contents, strict=strict)
		f.close()

		return path

	def remove(self, path, strict=True):
		# Check the file exists - send raw path
		if not self.exists(path):
			return False

		# Get the clean path
		path = self._get_path(path)

		response = self.connection.delete(self.bucket, path)

		if response.http_response.status != 204:
			raise IOError("S3StorageError: %s" % response.message)

		return True

	def exists(self, path):
		# Get the clean path
		path = self._get_path(path)

		if self.entries:
			return path in self.entries

		response = self.connection._make_request('HEAD', self.bucket, path)

		return response.status == 200

	def size(self, path):
		# Get the clean path
		path = self._get_path(path)

		if self.entries:
			entry = self.entries.get(path)

			if entry:
				return entry.size

			return 0

		response = self.connection._make_request('HEAD', self.bucket, path)
		content_length = response.getheader('Content-Length')

		return content_length and int(content_length) or 0

	@property
	def entries(self):
		if self._preload_metadata and not self._entries:
			self._entries = dict(
				(entry.key, entry)
				for entry in self.connection.list_bucket(self.bucket).entries
			)

		return self._entries

	def _put_file(self, path, contents):

		content_type = mimetypes.guess_type(path)[0] or "application/x-octet-stream"

		self.headers.update({
			'x-amz-acl': self.acl,
			'Content-Type': content_type,
			'Content-Length' : str(len(contents)),
		})

		response = self.connection.put(self.bucket, path, contents, self.headers)

		if response.http_response.status not in (200, 206):
			raise IOError("S3StorageError: %s" % response.message)

class S3File(FileInterface):

	def __init__(self, name, mode, storage):
		self._name = name
		self._storage = storage
		self._mode = mode
		self._is_dirty = False
		self._fetched = False
		self.file = None
		self.start_range = 0

	def __unicode__(self):
		return u'<S3File %s>' % self._name

	def _fetch(self, num_bytes=None):
		if num_bytes is None:
			args = []
			self.start_range = 0
		else:
			args = [self.start_range, self.start_range + num_bytes-1]

		data, etags, content_range = self._storage._read(self._name, *args)

		if content_range is not None:
			current_range, size = content_range.split(' ', 1)[1].split('/', 1)
			start_range, end_range = current_range.split('-', 1)
			self._size, self.start_range = int(size), int(end_range)+1

		self.file = StringIO(data)

		self._fetched = True

	def tell(self):
		if not self._fetched:
			self._fetch()

		return self.file.tell()

	def seek(self, pos, mode=0):
		if not self._fetched:
			self._fetch()

		self.file.seek(pos, mode)

	def read(self, num_bytes=None):
		if not self._fetched:
			# Only get the file once
			self._fetch()

		if num_bytes is not None:
			return self.file.read(num_bytes)
		else:
			return self.file.getvalue()

	def write(self, contents, strict=True):
		if 'w' not in self._mode:
			raise AttributeError('File was opened for read-only access.')

		self.file = StringIO(contents)
		self._is_dirty = True

	def close(self):
		if self._is_dirty:
			self._storage._put_file(self._name, self.file.getvalue())

		self.file.close()

	@property
	def url(self):
		return self._name

	@property
	def size(self):
		if not hasattr(self, '_size'):
			self._size = self._storage.size(self._name)

		return self._size
