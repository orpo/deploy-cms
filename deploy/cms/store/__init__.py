"""File Storage"""

import os.path
import string
import random

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError
from deploy.utils.importing import import_object

class FileStoreInterface(object):

	def __init__(self, *args, **kwargs):
		pass

	def _get_path(self, path):
		"""Get the formatted path to a file in the store"""
		return path

	def get_tmp_name(self, prefix='upload-', suffix=None, path=None, chars=6):
		"""Generate a temporary unique filename"""

		# Generate random name
		random_name = ''.join(
			random.choice(string.ascii_uppercase + string.digits) for x in range(chars)
		)

		if prefix is not None:
			random_name = '%s%s' % (prefix, random_name)

		if suffix is not None:
			random_name = '%s%s' % (random_name, suffix)

		if path is not None:
			random_name = os.path.join(path, random_name)

		# Check it's unique, even though it's random
		random_name = self.get_unique_name(random_name)

		return random_name

	def get_unique_name(self, path):
		"""Get a unique path for the store"""

		if not self.exists(path):
			return path

		while self.exists(path):
			root, ext = os.path.splitext(path)
			path = '%s_%s' % (root, ext)

		return path

	def open(self, path, mode='rb'):
		"""Open and return a handle to a file"""
		return NotImplementedError

	def save(self, path, contents):
		"""Save a file in the store"""
		return NotImplementedError

	def remove(self, path):
		"""Remove a file from the store"""
		return NotImplementedError

	def exists(self, path):
		"""Check if a file exists in the store"""
		return NotImplementedError

	def size(self, path):
		"""Return the size of a file in the store"""
		return NotImplementedError

	def url(self, path):
		"""Get a absolute URL to a file in the store"""
		return NotImplementedError

class FileInterface(object):

	# File properties
	encoding = property(lambda self: self.file.encoding)
	fileno = property(lambda self: self.file.fileno)
	flush = property(lambda self: self.file.flush)
	isatty = property(lambda self: self.file.isatty)
	newlines = property(lambda self: self.file.newlines)
	read = property(lambda self: self.file.read)
	readinto = property(lambda self: self.file.readinto)
	readline = property(lambda self: self.file.readline)
	readlines = property(lambda self: self.file.readlines)
	seek = property(lambda self: self.file.seek)
	softspace = property(lambda self: self.file.softspace)
	tell = property(lambda self: self.file.tell)
	truncate = property(lambda self: self.file.truncate)
	write = property(lambda self: self.file.write)
	writelines = property(lambda self: self.file.writelines)
	xreadlines = property(lambda self: self.file.xreadlines)

	def __init__(self, path, mode):
		self.path = path
		self.mode = mode
		self.file = file

	def __str__(self):
		return str(self.__unicode__())

	def __unicode__(self):
		return u'<FileInterface %s>' % self.path

	def open(self):
		"""Open the given file"""
		return NotImplementedError

def get_store(store=None, *args, **kwargs):
	"""Get the default file storage interface"""

	# Use the default store if one isn't specified
	if store is None:
		store = settings.CMS_DEFAULT_STORE

	try:
		interface_cls = import_object(store)
	except ImportError, e:
		raise ConfigurationError('Unable to load store interface "%s": %s' % (store, e))

	return interface_cls(*args, **kwargs)
