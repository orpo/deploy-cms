"""Local filesystem file storage"""

import os
import os.path
import shutil

from deploy.config import settings
from deploy.cms.store import FileStoreInterface, FileInterface
from deploy.cms.utils import unique_filepath_generator, filepath_generator

class LocalFileStore(FileStoreInterface):

	def __init__(self, root=None, auto_unique=True, auto_build_tree=True,
				auto_truncate_tree=True):

		if root is None:
			# Do we have a prefix?
			if settings.SITE_DATA_ROOT:
				self.root = settings.SITE_DATA_ROOT
			elif settings.CMS_STORE_PREFIX:
				self.root = os.path.join(
					settings.SITE_ROOT,
					settings.CMS_STORE_PREFIX
				)
			else:
				self.root = settings.SITE_ROOT
		else:
			self.root = root

		# Flags
		self.auto_unique = auto_unique
		self.auto_build_tree = auto_build_tree
		self.auto_truncate_tree = auto_truncate_tree

	def get_tmp_name(self, prefix='upload-', suffix=None,
					path=settings.CMS_ATTACHMENT_TMP_PREFIX, chars=6):

		# Special setting for local files using the filesystem
		if settings.SITE_DATA_ROOT:
			path = 'uploads/tmp'

			if not os.path.exists(os.path.join(self.root, path)):
				# If the temp file locaiton does not exist, make it automatically
				os.makedirs(os.path.join(self.root, path))

		return super(LocalFileStore, self).get_tmp_name(
			prefix=prefix, suffix=suffix, path=path, chars=chars
		)

	def _get_path(self, path):
		return os.path.join(self.root, path)

	def open(self, path, mode='rb', strict=True):
		# Get the clean path
		path = self._get_path(path)

		# Get a file handle
		f = LocalFile(path, mode)

		return f.open(strict=strict)

	def save(self, path, contents, mode='wb', strict=True):
		# Check if the path exists
		if self.exists(path):
			# If auto_unique is set, generate a unique path
			if not self.auto_unique:
				raise RuntimeError('Unable to save file "%s" - file already exists!')

			# Build the full attachment path on the filesystem
			path = self._get_path(path)

			# Get the unique path
			path = unique_filepath_generator(path)

		else:
			# Get the clean path
			path = self._get_path(path)

		# Do we need to auto-build the directory tree?
		if self.auto_build_tree:

			# Get the directory tree for the attachment storage
			store_tree = os.path.dirname(path)

			# Check that the tree exists for the attachment storage
			# otherwise create it
			if not os.path.exists(store_tree):
				os.makedirs(store_tree)

		# Get a file handle
		f = LocalFile(path, mode)
		f.write(contents, strict=strict)

		# Get a relative path for the file from the root for brevity
		#path = os.path.relpath(path, self.root)
		path = path.replace(self.root, '', 1)

		return path

	def remove(self, path, strict=True):
		# Check the file exists - send raw path
		if not self.exists(path):
			return False

		# Get the clean path
		path = self._get_path(path)

		try:
			os.remove(path)
		except OSError, e:
			if strict:
				raise e

			return False

		if self.auto_truncate_tree:
			# Remove the directories
			try:
				os.rmdir(os.path.dirname(path))
			except OSError, e:
				# There may still be items in here
				pass

		return True

	def exists(self, path):
		# Get the clean path
		path = self._get_path(path)
		return os.path.exists(path)

	def size(self, path):
		if not self.exists(path):
			return None

		# Get the clean path
		path = self._get_path(path)
		return os.path.getsize(path)

class LocalFile(FileInterface):

	def __unicode__(self):
		return u'<LocalFile %s>' % self.path

	def open(self, strict=True):
		try:
			self.file = open(self.path, self.mode)
		except IOError, e:
			if strict:
				raise e

			return None

		return self.file

	def read(self, strict=True):
		try:
			self.file = open(self.path, self.mode)
			contents = self.f.read()
			self.f.close()
		except IOError, e:
			if strict:
				raise e

			return None

		return contents

	def write(self, contents, strict=True):
		try:
			self.file = open(self.path, self.mode)
			self.file.write(contents)
			self.file.close()
		except IOError, e:
			if strict:
				raise e

			return False

		return True
