"""Mixins used for controllers"""

import json

from deploy.core.http import Response

from deploy.cms.attachments.models import Attachment
from deploy.cms.linkable import __linkable__
from deploy.cms.json_response import JSONSuccessResponse, JSONFailResponse, JSONErrorResponse
from deploy.cms.utils import is_logged_in
from deploy.core.forms.fields import HiddenField


class InpageCMSMixin(object):
	"""
	A mixin class that adds inpage editing support to controllers.
	"""

	@is_logged_in(permission_required='EDITOR')
	def inpage_sync(self, request, administrator):

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to save page',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('content_map'):
			return JSONErrorResponse(request, args={
				'msg': 'No content map specified',
				'code': 'NO_CONTENT_MAP'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Is there a INPAGE form to validate the submission against?
		form = self._get_form('INPAGE')

		# If no form was found then return a error response
		if form is None:
			return JSONErrorResponse(request, args={
				'msg': 'No INPAGE form defined for "%s"' % (self.Meta.gui_verbose_name),
				'code': 'NO_FORM_FOR_ACTION',
			})

		# This is an update so if one of the fields in the content map is
		# invalid no save is applied.

		# Convert the content map from a JSON string to a dict
		try:
			content_map = json.loads(request.POST['content_map'])
		except Exception:
			return JSONErrorResponse(request, args={
				'msg': 'Unable to parse content map',
				'code': 'INVALID_CONTENT_MAP'
			})

		# Update the form to have an ID field if one is not present
		if not form.has_field('id'):
			id_field = HiddenField(name='id', datatype=int)
			form.set_field(id_field)

		# Populate the ID
		form.populate({'id': record_id})

		# Validate each value in the content map
		for field_name in content_map:
			# Does the form have a field that corresponds to the field name,
			# if return an error response
			if not hasattr(form, field_name):
				return JSONErrorResponse(request, args={
					'msg': 'Field "%s" cannot be updated via inpage',
					'code': 'INVALID_CONTENT_MAP'
				})

			# Get the form field
			field = form.get_field(field_name)
			field.build(request)
			form.populate({field_name: content_map[field_name]})
			value = field.prepare(form=self)

			# Validate field
			if not field.validate(request=request, form=form):
				return JSONFailResponse(request, args={
					'type': 'INPAGE',
					'code': 'INVALID_FIELD',
					'field': field.name,
					'msg': field.error,
				})

			# Prepare the value
			value = field.prepare(form=self)

			# Save the value
			try:
				setattr(record, field_name, value)
			except Exception, e:
				return JSONErrorResponse(request, args={
					'msg': 'Unable to set value inpage! %s' % e,
					'code': 'INPAGE_FAILED',
				})

		# Commit the database changes
		request.db.commit()

		return JSONSuccessResponse(request, args={'msg': 'Paged saved'})

	@is_logged_in(permission_required='EDITOR')
	def inpage_get_image(self, request, administrator):
		"""Get an image through the editor interface"""

		# Sanity checks
		if not request.GET:
			return JSONErrorResponse(request, args={
				'msg': 'Must get to get image',
				'code': 'BAD_METHOD'
			})

		if not request.GET.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.GET.has_key('store_images'):
			return JSONErrorResponse(request, args={
				'msg': 'No store_images specified',
				'code': 'NO_ATTACHMENT_FIELD_NAME'
			})

		if not request.GET.has_key('images'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify an image to get',
				'code': 'NO_IMAGE'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Get the image json...

		# This time the image path is actual a pointer to the variation not
		# the attachment itself so we must reverse the path.
		variation_path = request.GET['images']

		# Get the actual attachment path
		path = Attachment.get_path_from_variation(variation_path, 'inpage')

		# Get the attachment details
		attachment = getattr(record, request.GET['store_images']).get_by_path(request.db, path)
		detail = {}
		detail[attachment.path] = {
			'variation_path': attachment.variation('inpage'),
			'meta': attachment.meta,
			'expected_meta': getattr(record, request.GET['store_images']).get_expected_meta_for_type(attachment.meta.get('type', '')),
			'url': attachment.url
		}

		return JSONSuccessResponse(request, args={'attachments': detail})

	@is_logged_in(permission_required='EDITOR')
	def inpage_insert_image(self, request, administrator):
		"""Insert a image through the editor interface"""

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to insert image',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('store_images'):
			return JSONErrorResponse(request, args={
				'msg': 'No store_images specified',
				'code': 'NO_ATTACHMENT_FIELD_NAME'
			})

		if not request.POST.has_key('images'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify an image to insert',
				'code': 'NO_IMAGE'
			})

		image_name = request.POST['store_images']

		# Get the image json...
		try:
			image_json = json.loads(request.POST['images'])
		except Exception, e:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid JSON sent to insert image! %s' % e,
				'code': 'INVALID_IMAGE'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Get the attachment data
		attachment_data = getattr(record, image_name).from_json(image_json)

		editor_path_map = {}

		# Go through the attachment data and add new images
		for path, details in attachment_data.items():

			# Add the attachment
			getattr(record, image_name).add(
				request.db,
				path=path,
				meta=details['meta'],
				order=details['order']
			)

			# Get the attachment data
			attachment = getattr(record, image_name).get_by_path(request.db, path)

			editor_path_map[attachment.path] = {
				'variation_path': attachment.variation('inpage'),
				'meta': attachment.meta,
				'url': attachment.url,
			}

		return JSONSuccessResponse(request, args={'attachments': editor_path_map})

	@is_logged_in(permission_required='EDITOR')
	def inpage_resize_image(self, request, administrator):
		"""Resize an image through the editor interface"""

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to resize image',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('store_images'):
			return JSONErrorResponse(request, args={
				'msg': 'No store_images specified',
				'code': 'NO_ATTACHMENT_FIELD_NAME'
			})

		if not request.POST.has_key('images'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify an image to resize',
				'code': 'NO_IMAGE'
			})

		if not request.POST.has_key('width') and not request.POST.has_key('height'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify a width and height to update image',
				'code': 'NO_DIMENSIONS'
			})

		try:
			width = int(request.POST['width'])
			height = int(request.POST['height'])
		except ValueError, e:
			return JSONErrorResponse(request, args={
				'msg': 'The image dimensions you submit must be integers',
				'code': 'INVALID_DIMENSIONS'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Get the image json...

		# This time the image path is actual a pointer to the variation not
		# the attachment itself so we must reverse the path.
		variation_path = request.POST['images']

		# Get the actual attachment path
		path = Attachment.get_path_from_variation(variation_path, 'inpage')

		# Create a meta dict to update the size of the image
		meta = {'inpage_size': (width, height)}

		# Update the attachment
		getattr(record, request.POST['store_images']).update(
			request.db,
			path=path,
			meta=meta,
			rebuild_variations=True
		)

		# Get the attachment details
		attachment = getattr(record, request.POST['store_images']).get_by_path(request.db, path)
		detail = {}
		detail[attachment.path] = {
			'variation_path': attachment.variation('inpage'),
			'meta': attachment.meta,
			'url': attachment.url
		}

		return JSONSuccessResponse(request, args={})

	@is_logged_in(permission_required='EDITOR')
	def inpage_update_image(self, request, administrator):
		"""Update an image through the editor interface"""

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to update image',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('store_images'):
			return JSONErrorResponse(request, args={
				'msg': 'No store_images specified',
				'code': 'NO_ATTACHMENT_FIELD_NAME'
			})

		if not request.POST.has_key('images'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify an image to resize',
				'code': 'NO_IMAGE'
			})

		if not request.POST.has_key('meta'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify a meta value to update image',
				'code': 'NO_META'
			})

		# Parse the meta
		try:
			meta = json.loads(request.POST['meta'])
		except Exception, e:
			return JSONErrorResponse(request, args={
				'msg': 'Invalid JSON sent to update image! %s' % e,
				'code': 'INVALID_META'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Get the image json...

		# This time the image path is actual a pointer to the variation not
		# the attachment itself so we must reverse the path.
		variation_path = request.POST['images']

		# Get the actual attachment path
		path = Attachment.get_path_from_variation(variation_path, 'inpage')

		# Update the attachment
		getattr(record, request.POST['store_images']).update(
			request.db,
			path=path,
			meta=meta,
			hard_update_meta=True
		)

		return JSONSuccessResponse(request, args={})

	@is_logged_in(permission_required='EDITOR')
	def inpage_remove_image(self, request, administrator):

		# Sanity checks
		if not request.POST:
			return JSONErrorResponse(request, args={
				'msg': 'Must post to save page',
				'code': 'BAD_METHOD'
			})

		if not request.POST.has_key('id'):
			return JSONErrorResponse(request, args={
				'msg': 'No ID specified',
				'code': 'NO_RECORD_ID'
			})

		if not request.POST.has_key('store_images'):
			return JSONErrorResponse(request, args={
				'msg': 'No store_images specified',
				'code': 'NO_ATTACHMENT_FIELD_NAME'
			})

		if not request.POST.has_key('images'):
			return JSONErrorResponse(request, args={
				'msg': 'You must specify an image to remove',
				'code': 'NO_IMAGE'
			})

		# Find the record instance we need to update
		try:
			record_id = int(request.params.get('id', ''))
		except ValueError, e:
			record_id = None
		record = request.db.find(self.Meta.model, self.Meta.model.id == record_id).one()

		# If no record was found return a error response
		if record is None:
			return JSONErrorResponse(request, args={
				'msg': 'No "%s" found with ID %s' % (self.Meta.gui_verbose_name, record_id),
				'code': 'OBJECT_NOT_FOUND',
			})

		# Get the image json...

		# This time the image path is actual a pointer to the variation not
		# the attachment itself so we must reverse the path.
		variation_path = request.POST['images']

		# Get the actual attachment path
		path = Attachment.get_path_from_variation(variation_path, 'inpage')

		# Remove the attachment
		getattr(record, request.POST['store_images']).remove(
			request.db,
			path=path
		)

		return JSONSuccessResponse(request, args={'msg': 'Successfully removed image "%s".' % path})


class LinkableMixin(object):

	@is_logged_in(permission_required='EDITOR')
	def linkable(self, request, administrator):

		if not request.GET.has_key('q') or not request.GET['q']:
			return JSONErrorResponse(request, args={
				'msg': 'You must specify a query to linkable',
				'code': 'LINKABLE_QUERY_INVALID'
			})

		query = request.GET['q']

		# Run the search
		results = __linkable__.search(query)

		if results is None:
			return JSONErrorResponse(request, args={
				'msg': 'Query is invalid',
				'code': 'LINKABLE_QUERY_INVALID'
			})

		# Correct response
		#return JSONSuccessResponse(request, args={
		#	'links': '|'.join(['%s|%s' % (t, u) for u, t in results])
		#})

		# Work around for rubbish auto-complete lib ;).....
		response = Response(content_type='text/plain', request=request)
		response.status = 200
		response.unicode_body = u'|'.join([u'%s|%s' % (t, u) for u, t in results])

		return response
