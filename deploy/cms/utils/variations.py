import os
import os.path
import warnings

try:
	from cStingIO import StringIO
except ImportError:
	from StringIO import StringIO

from deploy.config import settings

from deploy.cms.attachments import Attachment
from deploy.cms.store import get_store

def inpage_attachment(attachment, save_path):
	"""Build a page variation"""

	try:
		from PIL import Image
	except ImportError:
		warnings.warn(
			'You can not process inpage attachment images without PIL installed!',
			RuntimeWarning
		)
		return None

	# Get a handle to the file store
	file_store = get_store(auto_unique=False)

	# Get the attachment
	path = attachment.path

	# Check the original exists
	if not file_store.exists(path):
		raise RuntimeError('Unable to create page image - oringinal %s does not exist!' % path)

	# Create the new save path for the variation
	save_path = os.path.join(
		os.path.dirname(attachment.path),
		'inpage/',
		os.path.basename(attachment.path)
	)

	# Get the size for the image if specified, else use default
	size = attachment.get_meta(
		'inpage_size',
		settings.CMS_PAGE_ATTACHMENT_DEFAULT_SIZE
	)

	# Get the image
	attachment_handler = file_store.open(path)

	# Create the outfile
	outfile = StringIO()

	# Process the image
	im = Image.open(attachment_handler)
	im = im.convert('RGB')

	original_width, original_height = im.size

	if not attachment.has_meta('original_width'):
		attachment.set_meta('original_width', original_width)

	if not attachment.has_meta('original_height'):
		attachment.set_meta('original_height', original_height)

	im.thumbnail(size, Image.ANTIALIAS)
	im.save(outfile, 'JPEG', quality=settings.CMS_PAGE_ATTACHMENT_QUALITY)

	width, height = im.size

	attachment.set_meta('width', width)
	attachment.set_meta('height', height)

	# Save the image
	file_store.save(save_path, outfile.getvalue())
