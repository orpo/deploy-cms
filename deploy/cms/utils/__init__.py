"""Deploy CMS utility functions"""

import os
import os.path
import calendar
import time

from deploy.config import settings
from deploy.core.http import Request
from deploy.core.models.utils import get_model_slug
from deploy.shortcuts import render_page

__all__ = ('is_logged_in', 'get_json_response_id', 'get_controller_name',
	'get_key_for_choices', 'filepath_generator')

UNSAFE_FILE_CHARS = ['.', ' ']

def expose(fn):
	"""Utility to allow for controller methods to be called from a public interface"""
	fn.is_exposed = True
	return fn

def is_logged_in(permission_required='ANY'):
	"""Utility decorator to check if a Administrator is logged in and has permission
	to run view. Excepts an optional keyword argument of valid permissions.
	(WARNING: Magic) Administrator argument is injecteded into the positional arguments
	of the calling function. This decorator also implies that the method is exposed (see @expose)"""

	# Note: This is a special type of decorator that excepts arguments - it must be
	# called @is_logged_in() instead of just @is_logged_in

	def check_permissions(fn, *args, **kwargs):

		def do_check(*args, **kwargs):

			administrator = None

			# We have to be careful here, the decorator is used both on
			# class instances and methods. This means that the first
			# argument isn't always the 'request' object, it may be the
			# class object. As this decorator injects the administrator
			# arg we need to sniff out the request arg, the administrator
			# positional arg ALWAYS follows the request argument
			administrator_positional_arg_index = 1

			# Get the request object
			request = args[0]

			if not isinstance(request, Request):
				request = args[1]
				administrator_positional_arg_index += 1

			# Check we have an administrator and they have the correct permission
			if not request.environ.has_key('com.deploycms.cms.administrator_session') or \
				(request.environ.has_key('com.deploycms.cms.administrator_session') and \
				not request.environ['com.deploycms.cms.administrator_session']):

				# Permission is denied.
				if request.is_xhr:
					# If it is an ajax request, return a JSON response
					from deploy.cms.json_response  import JSONFailResponse
					return JSONFailResponse(request, args={
						'msg': 'Permission Denied.',
						'code': 'CMS_PERMISSION_DENIED'
					})

				else:
					# Return a unauthorized page
					return render_page(request, '401.html', {}, status=401)

			administrator_session = request.environ['com.deploycms.cms.administrator_session']
			administrator = administrator_session.administrator

			# Check if we need permission..
			if permission_required in dict(settings.CMS_PERMISSIONS).keys():

				# If the role required is devloper - nothing can superceed, kill the request
				# if the user isn't a developer
				if permission_required == 'DEVELOPER' and administrator.get_role_key() != 'DEVELOPER':
					# Permission is denied.
					if request.is_xhr:
						# If it is an ajax request, return a JSON response
						from deploy.cms.json_response  import JSONFailResponse
						return JSONFailResponse(request, args={
							'msg': 'Permission Denied.',
							'code': 'CMS_PERMISSION_DENIED'
						})

					else:
						# Return a unauthorized page
						return render_page(request, '401.html', {}, status=401)

				# Checks have passed, permission is granted so fall through

			# We need to check that there isn't already a administrator arg in the positional
			# order (this can happen if decorated functions are inherited and overidden for example)
			try:
				original_administrator = args[administrator_positional_arg_index]
			except IndexError:
				# Argument does not exist, add it
				args = list(args)
				args.insert(administrator_positional_arg_index, administrator)

			else:
				# Argument does exist, fall through..
				pass

			return fn(*args, **kwargs)

		# Set the permission against the check method, We can then look it up
		# later to check a decorated methods permission
		do_check.permission_required = permission_required

		# Set the method as exposed
		do_check.is_exposed = True

		return do_check

	return check_permissions

def get_json_response_id():
	"""Return a JSON response ID"""
	return '%s-%s' % (os.getpid(), calendar.timegm(time.gmtime()))

def get_controller_name(model):
	"""Get the name of a controller from a model"""

	controller_name = ''

	# HERE BE DRAGONS!
	#controller_name = get_model_slug(model.__class__.__name__)
	controller_name = get_model_slug(model.mro()[0].__name__)

	if hasattr(model, '__module__'):
		if model.__module__.startswith('plugins'):
			# Get the plugin name...
			dot = model.__module__.rindex('.')
			plugin_name = model.__module__[dot+1:]

			# We build the controller slug with the plugin name prefixed
			# to the front to stop any clashes from plugins with the same
			# model names
			controller_name = '%s-%s' % (plugin_name, controller_name)

	return controller_name

def has_permission(permission_required, administrator_permission):
	"""Check if a speicifed permission is greater than a specified level"""

	permission_required = unicode(permission_required)

	if not permission_required in dict(settings.CMS_PERMISSIONS).keys():
		return False

	if permission_required == 'ANY':
		return True

	if permission_required == 'EDITOR' and (administrator_permission == 'EDITOR' or administrator_permission == 'DEVELOPER'):
		return True

	if permission_required == 'DEVELOPER' and administrator_permission == 'DEVELOPER':
		return True

	return False

def get_key_for_choices(label, choices, default=None):
	for k, v in choices:
		if v == label:
			return k

	return default

def storm_emun_options(options):
	"""Utility function for Storm"""
	return [(v, v) for k, v in options]

def filepath_generator(filename):
	"""Utility for generating filenames for resource storage"""

	if len(filename) < 3:
		# No formatting on filenames shorter than 3 characters
		return filename

	bits = filename.rsplit('.')

	if len(bits) > 1:
		if len(bits[0]) < 3:
			return filename

	one = filename[0]
	two = filename[1]

	# Saftey check path parts
	if one in UNSAFE_FILE_CHARS:
		one = '-'

	if two in UNSAFE_FILE_CHARS:
		two = '-'

	return os.path.join(one, two, filename)

def unique_filepath_generator(filepath):
	"""Utility for ensuring a file path is unique"""

	if not os.path.exists(filepath):
		return filepath

	while os.path.exists(filepath):
		root, ext = os.path.splitext(filepath)
		filepath = '%s_%s' % (root, ext)

	return filepath

def get_model_attachments(model):
	"""Get a list of tuples containing the column name and type"""

	from deploy.cms.attachments import AttachmentStoreManager

	attachments = {}

	for attr in dir(model):
		if not attr.startswith('__') and isinstance(getattr(model, attr), AttachmentStoreManager):
			model_attachment = getattr(model, attr, None)

			if model_attachment is not None:
				attachments[attr] = model_attachment

	return attachments

def get_tmp_filestore():
	"""Get the path of the temporary filestore"""

	# Default to the OS temp directory
	tmp_file_dir = '/tmp'

	if settings.SITE_DATA_ROOT:
		tmp_file_dir = os.path.join(settings.SITE_DATA_ROOT, 'uploads/tmp')

		if not os.path.exists(tmp_file_dir):
			# If the temp file locaiton does not exist, make it automatically
			os.makedirs(tmp_file_dir)

	return tmp_file_dir