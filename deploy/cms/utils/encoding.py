import markupsafe

from json.encoder import FLOAT_REPR, HAS_UTF8, ESCAPE_DCT, ESCAPE_ASCII

from deploy.utils.strings import SafeData
from deploy.utils.encoding import ExtendedJSONEncoder

__all__ = ('MakeSafeJSONEncoder',)

def py_encode_basestring_ascii(s):
	"""Return an ASCII-only JSON representation of a Python string

	"""
	if isinstance(s, str) and HAS_UTF8.search(s) is not None:
		s = s.decode('utf-8')
	def replace(match):
		s = match.group(0)
		try:
			return ESCAPE_DCT[s]
		except KeyError:
			n = ord(s)
			if n < 0x10000:
				return '\\u{0:04x}'.format(n)
				#return '\\u%04x' % (n,)
			else:
				# surrogate pair
				n -= 0x10000
				s1 = 0xd800 | ((n >> 10) & 0x3ff)
				s2 = 0xdc00 | (n & 0x3ff)
				return '\\u{0:04x}\\u{1:04x}'.format(s1, s2)
				#return '\\u%04x\\u%04x' % (s1, s2)

	if isinstance(s, SafeData):
		return '"' + str(ESCAPE_ASCII.sub(replace, s)) + '"'
	else:
		return '"' + str(ESCAPE_ASCII.sub(replace, markupsafe.escape(s))) + '"'

class MakeSafeJSONEncoder(ExtendedJSONEncoder):
	"""A class that extends the encoding capabilities of JSON for various common
	Python types."""

	def iterencode(self, o, _one_shot=False):
		"""Overloaded iterencode..."""

		if self.check_circular:
			markers = {}
		else:
			markers = None

		# For encoder this encoder to be used
		_encoder = py_encode_basestring_ascii

		def floatstr(o, allow_nan=self.allow_nan, _repr=FLOAT_REPR):
			# Check for specials.  Note that this type of test is processor
			# and/or platform-specific, so do tests which don't depend on the
			# internals.

			if o != o:
				text = 'NaN'
			else:
				return _repr(o)

			if not allow_nan:
				raise ValueError(
					"Out of range float values are not JSON compliant: " +
					repr(o))

			return text

		_iterencode = _make_iterencode(
			markers, self.default, _encoder, self.indent, floatstr,
			self.key_separator, self.item_separator, self.sort_keys,
			self.skipkeys, _one_shot)

		return _iterencode(o, 0)

def _make_iterencode(markers, _default, _encoder, _indent, _floatstr,
		_key_separator, _item_separator, _sort_keys, _skipkeys, _one_shot,
		## HACK: hand-optimized bytecode; turn globals into locals
		ValueError=ValueError,
		basestring=basestring,
		dict=dict,
		float=float,
		id=id,
		int=int,
		isinstance=isinstance,
		list=list,
		long=long,
		str=str,
		tuple=tuple,
	):

	def _iterencode_list(lst, _current_indent_level):
		if not lst:
			yield '[]'
			return
		if markers is not None:
			markerid = id(lst)
			if markerid in markers:
				raise ValueError("Circular reference detected")
			markers[markerid] = lst
		buf = '['
		if _indent is not None:
			_current_indent_level += 1
			newline_indent = '\n' + (' ' * (_indent * _current_indent_level))
			separator = _item_separator + newline_indent
			buf += newline_indent
		else:
			newline_indent = None
			separator = _item_separator
		first = True
		for value in lst:
			if first:
				first = False
			else:
				buf = separator
			if isinstance(value, basestring):
				yield buf + _encoder(value)
			elif value is None:
				yield buf + 'null'
			elif value is True:
				yield buf + 'true'
			elif value is False:
				yield buf + 'false'
			elif isinstance(value, (int, long)):
				yield buf + str(value)
			elif isinstance(value, float):
				yield buf + _floatstr(value)
			else:
				yield buf
				if isinstance(value, (list, tuple)):
					chunks = _iterencode_list(value, _current_indent_level)
				elif isinstance(value, dict):
					chunks = _iterencode_dict(value, _current_indent_level)
				else:
					chunks = _iterencode(value, _current_indent_level)
				for chunk in chunks:
					yield chunk
		if newline_indent is not None:
			_current_indent_level -= 1
			yield '\n' + (' ' * (_indent * _current_indent_level))
		yield ']'
		if markers is not None:
			del markers[markerid]

	def _iterencode_dict(dct, _current_indent_level):
		if not dct:
			yield '{}'
			return
		if markers is not None:
			markerid = id(dct)
			if markerid in markers:
				raise ValueError("Circular reference detected")
			markers[markerid] = dct
		yield '{'
		if _indent is not None:
			_current_indent_level += 1
			newline_indent = '\n' + (' ' * (_indent * _current_indent_level))
			item_separator = _item_separator + newline_indent
			yield newline_indent
		else:
			newline_indent = None
			item_separator = _item_separator
		first = True
		if _sort_keys:
			items = sorted(dct.items(), key=lambda kv: kv[0])
		else:
			items = dct.iteritems()
		for key, value in items:
			if isinstance(key, basestring):
				pass
			# JavaScript is weakly typed for these, so it makes sense to
			# also allow them. Many encoders seem to do something like this.
			elif isinstance(key, float):
				key = _floatstr(key)
			elif key is True:
				key = 'true'
			elif key is False:
				key = 'false'
			elif key is None:
				key = 'null'
			elif isinstance(key, (int, long)):
				key = str(key)
			elif _skipkeys:
				continue
			else:
				raise TypeError("key " + repr(key) + " is not a string")
			if first:
				first = False
			else:
				yield item_separator
			yield _encoder(key)
			yield _key_separator
			if isinstance(value, basestring):
				yield _encoder(value)
			elif value is None:
				yield 'null'
			elif value is True:
				yield 'true'
			elif value is False:
				yield 'false'
			elif isinstance(value, (int, long)):
				yield str(value)
			elif isinstance(value, float):
				yield _floatstr(value)
			else:
				if isinstance(value, (list, tuple)):
					chunks = _iterencode_list(value, _current_indent_level)
				elif isinstance(value, dict):
					chunks = _iterencode_dict(value, _current_indent_level)
				else:
					chunks = _iterencode(value, _current_indent_level)
				for chunk in chunks:
					yield chunk
		if newline_indent is not None:
			_current_indent_level -= 1
			yield '\n' + (' ' * (_indent * _current_indent_level))
		yield '}'
		if markers is not None:
			del markers[markerid]

	def _iterencode(o, _current_indent_level):
		if isinstance(o, basestring):
			yield _encoder(o)
		elif o is None:
			yield 'null'
		elif o is True:
			yield 'true'
		elif o is False:
			yield 'false'
		elif isinstance(o, (int, long)):
			yield str(o)
		elif isinstance(o, float):
			yield _floatstr(o)
		elif isinstance(o, (list, tuple)):
			for chunk in _iterencode_list(o, _current_indent_level):
				yield chunk
		elif isinstance(o, dict):
			for chunk in _iterencode_dict(o, _current_indent_level):
				yield chunk
		else:
			if markers is not None:
				markerid = id(o)
				if markerid in markers:
					raise ValueError("Circular reference detected")
				markers[markerid] = o
			o = _default(o)
			for chunk in _iterencode(o, _current_indent_level):
				yield chunk
			if markers is not None:
				del markers[markerid]

	return _iterencode
