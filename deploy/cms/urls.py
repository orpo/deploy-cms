from deploy.core.urls import URL

from deploy.cms import CMSController

__all__ = ('CMSURL',)

# NOTE: URL's in the CMS application are generated from one core URL -
# found in the CMS application

class CMSURL(URL):

	def __init__(self, url, endpoint, controller=None, **kwargs):
		"""Initialise the CMSURL"""

		self.controller = None
		self.set_controller(controller)

		super(CMSURL, self).__init__(url, endpoint, **kwargs)

	def get_controller(self):
		return self.controller

	def set_controller(self, controller):
		# Has a controller key or controller object been specified?
		# We only want the key...
		if isinstance(controller, CMSController):
			self.controller = controller.Meta.controller_name
		else:
			self.controller = controller