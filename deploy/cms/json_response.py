"""JSON Response Objects"""

from deploy.shortcuts import render_json_page
from deploy.cms.utils import get_json_response_id
from deploy.cms.utils.encoding import MakeSafeJSONEncoder

__all__ = ('JSONResponse', 'JSONSuccessResponse', 'JSONErrorResponse',
	'JSONFailResponse')

class JSONResponse(object):

	response_type = 'response'

	def __init__(self, request, args=None, cookies=None, content_type='application/json', **kwargs):

		self._args = {}
		self._request = request
		self._cookies = cookies
		self._content_type = content_type

		request_id = None

		if request.GET.has_key('@'):
			request_id = request.GET['@']
		elif request.POST.has_key('@'):
			request_id = request.POST['@']

		self._args = {
			'@': request_id,
			'@@': get_json_response_id(),
			'status': self.response_type,
			'data': args,
		}

	def __call__(self, environ, start_response):
		response = render_json_page(
			self._request,
			self._args,
			content_type=self._content_type,
			cookies=self._cookies,
			encoder=MakeSafeJSONEncoder
		)
		return response(environ, start_response)

	def set_data_arg(self, key, arg):
		self._args['data'][key] = arg

	def to_dict(self):
		return self._args

class JSONSuccessResponse(JSONResponse):
	response_type = 'success'

class JSONErrorResponse(JSONResponse):
	response_type = 'error'

class JSONFailResponse(JSONResponse):
	response_type = 'fail'
