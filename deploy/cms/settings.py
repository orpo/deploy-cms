"""Default settings for Deploy CMS"""

from datetime import timedelta

CMS_VIEW_PAGE = 'deploy.cms.views.pages.view_page'
CMS_BUILD_PAGE = 'deploy.cms.views.pages.build_a_page'

# Prepended to cms urls.. e.g /cms/
CMS_URL_PREFIX = '/edit/'

# User Login controls
CMS_USER_MAX_LOGIN_ATTEMPT_COUNT = 5
CMS_USER_LOGIN_COOLING_OFF_PERIOD = 20 * 60 # In seconds
CMS_USER_RESET_COOLING_OFF_PERIOD = 60 * 60 # In seconds
CMS_USER_EXPRIY = timedelta(days=7)

CMS_ADMINISTRATOR_COOKIE_NAME = 'dycms'
CMS_ADMINISTRATOR_COOKIE_EXPIRES = None #60*60*24*7 # One Week
CMS_ADMINISTRATOR_COOKIE_PATH = '/'
CMS_ADMINISTRATOR_COOKIE_SECURE = False

CMS_PAGINATION_COUNT = 10

CMS_SUPER_PERMISSION = u'DEVELOPER'
CMS_PERMISSIONS = (
	(u'EDITOR', u'Site Editor'),
	(u'DEVELOPER', u'Site Developer'),
)

CMS_THEMES = ['sw-theme',]

CMS_ATTRIBUTE = 'data-deploycms'

# Some edit controls...
CMS_PARSE_TAGS = ['div', 'header', 'footer', 'nav', 'section', 'article', 'aside']
CMS_PARSE_CLASSES = ['edit-local']

# Resource Settings
CMS_REMOTE_RESOURCES = False
CMS_REMOTE_RESOURCES_VERSION = 'latest'
CMS_REMOTE_RESOURCES_URL = 'http://media.deploycms.com/app/%s/app.js'

# Files

# This is the default storage interface for deploy.cms - typically used for attachments
CMS_DEFAULT_STORE = 'deploy.cms.store.localfile.LocalFileStore'

# Will be prefixed to the path before any root - this is used if you want to map custom
# URL's to different filesystem locations
CMS_STORE_PREFIX = ''

# Attachment settings

# This is the base URL for all attachment URL's it is ALWAYS prefixed when presenting attachment
# URL's
CMS_ATTACHMENT_BASE_URL = '/'

# Auto generate attachment thumbnails
CMS_ATTACHMENT_THUMBNAIL = True

# Default size of attachment thumbnails
CMS_ATTACHMENT_THUMBNAIL_SIZE = 150, 150

# When attachments are initially uploaded they are stored in a tempoary location, which is relative
# to either the SITE_ROOT + CMS_STORE_PREFIX or the SITE_DATA_ROOT (if defined)
CMS_ATTACHMENT_TMP_PREFIX = 'tmp'

# Allow passwords to be sent in plain text
CMS_EMAIL_PASSWORD = True

# CMS Page attachment options
CMS_PAGE_ATTACHMENT_PATH = 'pages'
CMS_PAGE_ATTACHMENT_MAX = 10
CMS_PAGE_ATTACHMENT_QUALITY = 100
CMS_PAGE_ATTACHMENT_DEFAULT_SIZE = (400, 400)

# Linkable
CMS_LINKABLE_NAME = 'Linkable'
CMS_LINKABLE_INDEX = 'linkable-index'
CMS_LINKABLE_LIMIT = 10

# AWS S3 default settings for file storage
CMS_AWS_BUCKET = None
CMS_AWS_ACCESS_KEY = None
CMS_AWS_SECRET_KEY = None
CMS_AWS_DEFAULT_ACL = 'public-read'
CMS_AWS_SECURE_URLS = False
CMS_AWS_QUERYSTRING_EXPIRE = 60
CMS_AWS_PRELOAD_METADATA = False

# Prefix for all document url paths
CMS_DOC_ATTACHMENT_PATH = 'documents'
