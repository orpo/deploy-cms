/** 
 * Snailwhale theme extensions.
 * Copyright � 2010-2011 Snailwhale LLP.
 */

var fluidExtensions = {'/media/edit/themes/sw-theme/extensions.js': {
    'FLUID.Bubble': function (phase) {
        if (phase === FLUID.Widget.LAYOUT_PHASE_AFTER) {
            if (this.exists()) {
                if (this._transitionState === FLUID.Popup.DORMANT_STATE) {
                    
                    // Resize the borders
                    var size = this.getPopupSize();
                    
                    // Adjust the handle
                    switch (this._orientation) {
                    
                    case FLUID.Bubble.NW_ORIENT:
                        this.style({top: (this._style.top || 0), left: (this._style.left || 0) - 10});
                        break;
                    
                    case FLUID.Bubble.NE_ORIENT:
                        this.style({top: (this._style.top || 0), left: (this._style.left || 0) + 10 + 10});
                        break;
                    
                    case FLUID.Bubble.SW_ORIENT:
                        this.style({top: (this._style.top || 0) - size[1] - 15, left: (this._style.left || 0) - 10});
                        break;
                    
                    case FLUID.Bubble.SE_ORIENT:
                        this.style({top: (this._style.top || 0) - size[1] - 15, left: (this._style.left || 0) + 10});
                        break;
                    
                    }
                }
            }
        }
    },
    'FLUID.BubbleWindow': function (phase) {
        if (phase === FLUID.Widget.LAYOUT_PHASE_AFTER) {
            if (this.exists()) {
                if (this._transitionState === FLUID.Popup.DORMANT_STATE) {
                    
                    // Resize the borders
                    var size = this.getPopupSize();
                    
                    // Adjust the handle
                    switch (this._orientation) {
                    
                    case FLUID.Bubble.NW_ORIENT:
                        this.style({top: (this._style.top || 0), left: (this._style.left || 0) - 10});
                        break;
                    
                    case FLUID.Bubble.NE_ORIENT:
                        this.style({top: (this._style.top || 0), left: (this._style.left || 0) + 10});
                        break;
                    
                    case FLUID.Bubble.SW_ORIENT:
                        this.style({top: (this._style.top || 0) - size[1] - 15, left: (this._style.left || 0) - 10});
                        break;
                    
                    case FLUID.Bubble.SE_ORIENT:
                        this.style({top: (this._style.top || 0)  - size[1] - 15, left: (this._style.left || 0) + 10});
                        break;
                    
                    }
                }
            }
        }
    },
}};