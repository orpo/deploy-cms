/** 
 * Snailwhale theme resources.
 * Copyright � 2010-2011 Snailwhale LLP.
 */

var icons = new FLUID.IconSet({
    imageUrl: '/media/edit/themes/sw-theme/icons.png',
    cols: 32,
    count: 64,
    size: 16
    });

var fluidResources = {'/media/edit/themes/sw-theme/resources.js': {
    
    // Application bar icons (order by how they appear)
    'deploy.icons.appbar.edit': icons.getIcon([0, 0]),
    'deploy.icons.appbar.properties': icons.getIcon([1, 0]),
    'deploy.icons.appbar.new': icons.getIcon([2, 0]),
    'deploy.icons.appbar.site': icons.getIcon([3, 0]),
    'deploy.icons.appbar.plug-ins': icons.getIcon([4, 0]),
    'deploy.icons.appbar.desktop': icons.getIcon([5, 0]),
    'deploy.icons.appbar.me': icons.getIcon([6, 0]),
    'deploy.icons.appbar.accept': icons.getIcon([7, 0]),
    'deploy.icons.appbar.cancel': icons.getIcon([8, 0]),
    
    // View icons (ordered alphabetically)
    'deploy.icons.views.accept': icons.getIcon([9, 0]),
    'deploy.icons.views.add': icons.getIcon([10, 0]),
    'deploy.icons.views.cancel': icons.getIcon([11, 0]),
    'deploy.icons.views.delete': icons.getIcon([12, 0]),
    'deploy.icons.views.go': icons.getIcon([13, 0]),
    'deploy.icons.views.import': icons.getIcon([14, 0]),
    'deploy.icons.views.export': icons.getIcon([15, 0]),
    'deploy.icons.views.next': icons.getIcon([16, 0]),
    'deploy.icons.views.prev': icons.getIcon([17, 0]),
    'deploy.icons.views.password': icons.getIcon([18, 0]),
    'deploy.icons.views.properties': icons.getIcon([19, 0]),
    'deploy.icons.views.report': icons.getIcon([20, 0]),
    'deploy.icons.views.sign': icons.getIcon([21, 0]),
    
    // Editor icons
    'deploy.icons.editor.bold': icons.getIcon([0, 1]),
    'deploy.icons.editor.italic': icons.getIcon([1, 1]),
    'deploy.icons.editor.align-left': icons.getIcon([2, 1]),
    'deploy.icons.editor.align-center': icons.getIcon([3, 1]),
    'deploy.icons.editor.align-right': icons.getIcon([4, 1]),
    'deploy.icons.editor.insert-a': icons.getIcon([5, 1]),
    'deploy.icons.editor.delete-a': icons.getIcon([6, 1]),
    'deploy.icons.editor.h1': icons.getIcon([7, 1]),
    'deploy.icons.editor.h2': icons.getIcon([8, 1]),
    'deploy.icons.editor.h3': icons.getIcon([9, 1]),
    'deploy.icons.editor.p': icons.getIcon([10, 1]),
    'deploy.icons.editor.insert-br': icons.getIcon([11, 1]),
    'deploy.icons.editor.insert-hr': icons.getIcon([12, 1]),
    'deploy.icons.editor.ol': icons.getIcon([13, 1]),
    'deploy.icons.editor.ul': icons.getIcon([14, 1]),
    'deploy.icons.editor.indent': icons.getIcon([15, 1]),
    'deploy.icons.editor.unindent': icons.getIcon([16, 1]),
    'deploy.icons.editor.insert-table': icons.getIcon([17, 1]),
    'deploy.icons.editor.th': icons.getIcon([18, 1]),
    'deploy.icons.editor.insert-image': icons.getIcon([19, 1]),
    'deploy.icons.editor.insert-video': icons.getIcon([20, 1]),
    'deploy.icons.editor.delete': icons.getIcon([21, 1])
    
}};