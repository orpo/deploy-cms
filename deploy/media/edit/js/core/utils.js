/**
 * @fileoverview    JavaScript utilities.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

/* 
Provide a dummy console object if one does not exists, this prevents calls to
console.log causing an error when browser does not have a console object or 
where it is not enabled.
*/
try {
    console.log('Console found.');
} catch (error) {
    console = {log: function log(value) {}};
}

// Ensure indexOf method is defined

if (!Array.indexOf) {
    /** @ignore */
    Array.prototype.indexOf = function (obj) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === obj) {
                return i;
            }
        }
        return -1;
    };
}

/**
 * The UTILS namespace.
 * @final
 * @type Namespace
 * @addon
 */  
var UTILS = {_autoIncId: 0};

/**
 * Return a unique ID. The function simply generates a unique ID by auto 
 * incrementing a static variable, as such it is only suitable for generating
 * unique IDs for use in temporary instances such as the activity of a user
 * during one page load. It would not be safe to store such ID's and then use
 * the function to generate new IDs between pages. 
 * @param A unique ID.
 * @type Object
 * @addon
 */
UTILS.createId = function () {
    return ++UTILS._autoIncId;
};

/**
 * Return true if the value is a string.
 * @param {String} value The value to test.
 * @return If the value is a string.
 * @type Boolean
 */
UTILS.isString = function (value) {
    return (typeof(value) === 'string' || value.constuctor === String);
}

/**
 * Return a string with the first letter uppercased.
 * @param {String} value The string to convert.
 * @return The converted string.
 * @type String
 */
UTILS.title = function (value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
}

/**
 * Return a string in dash format that has been provided in camel case.
 * @param {String} value The string to convert.
 * @return The converted string.
 * @type String
 */
UTILS.camelToDash = function (value) {
    return value.replace(/([A-Z])/g, function ($1) { 
        return '-' + $1.toLowerCase(); 
    });
};

/**
 * Return a date formatted as YYYY-MM-DD
 * @param {Date} value The date to convert.
 * @return The formatted date string.
 * @type String
 */
UTILS.dateToString = function (date) {
    var year = date.getFullYear().toString();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	
	if (month < 10) {
		month = '0' + month;
	}
	month = month.toString();

	if (day < 10) {
		day = '0' + day;
	}
	day = day.toString();
	
	return year + '-' + month + '-' + day;
};


/**
 * Return a date from a string formatted as YYYY-(M)M-(D)D
 * @param {String} value The string to convert.
 * @return The date.
 * @type Date
 */
UTILS.dateFromString = function (dateStr) {
    var parts = dateStr.split('-');
	if (parts.length === 3) {
		try {
			return new Date(parseInt(parts[0], 10), parseInt(parts[1], 10) - 1, parseInt(parts[2], 10));
		} catch (error) { /* Ignore invalid dates */ }
	}
};

/**
 * Return a string in slug format (convert to lower-case, remove special 
 * characters and replace spaces with dashes).
 * @param {String} value The string to convert.
 * @return The converted string.
 * @type String
 */
UTILS.slugify = function (value) {
  value = value.toLowerCase();
  value = value.replace(/^\s+|\s+$/g, '');
  var from = "����������������������/_,:;";
  var to   = "aaaaeeeeiiiioooouuuunc------";
  for (var i=0, l=from.length ; i<l ; i++) {
    value = value.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
  }
  value = value.replace(/[^a-z0-9 -]/g, '');
  value = value.replace(/\s+/g, '-');
  value = value.replace(/-+/g, '-');
  return value;
}

/**
 * Return an object from the specified options which is guarenteed to contain
 * the attributes specified in defaults, the value of an attribute is set to 
 * its counterpart in defaults if it is undefined in options. 
 * @param {Object} options An object containing specified options.
 * @param {Object} defaults An object default options.
 * @return The parsed options.
 * @type Object
 * @addon
 */
UTILS.optionsArg = function (options, defaults) {
    if (defaults === undefined) {
        return options;
    }
    var option;
    if (options === undefined) {
        options = {};
        for (option in defaults) {
            options[option] = defaults[option];
        }
        return options;
    }
    for (option in defaults) {
        if (options[option] === undefined) {
            options[option] = defaults[option];
        }
    }
    return options;
};

/**
 * Copy a prototype from object to another. Originally by Troels 
 * Knak-Nielsen.
 * @param {Object} descendant The object to copy the prototype to.
 * @param {Object} parent The object to copy the prototype from.
 * @addon
 */
UTILS.copyPrototype = function (descendant, parent) {
    var sConstructor = parent.toString();
    var aMatch = sConstructor.match(
        /\s*function\s+([a-zA-Z_$][0-9a-zA-Z_$]*)\s+\(/);
    if (aMatch !== null) {
        descendant.prototype[aMatch[1]] = parent; 
    }
    for (var m in parent.prototype) {
        /* 
        Special case 'for in' loop, in this case we want to access all 
        properites for the mash of both prototypes.
        */
        descendant.prototype[m] = parent.prototype[m];
    }
};

/**
 * Simple JavaScript templating. Originally by John Resig.
 * @param {String} source The template source.
 * @param {Object} data (Optional) The data to compile into the template. If
 * data isn't specified the function returns a compiled function.
 * @return Return the template function.
 * @type Function
 * @addon
 */
UTILS.template = function (source, data) {
    /*
    This function breaks a number of JSLint rules due to the nature of an 
    inline parser. 
    */    
    if (UTILS._templateCache[source] === undefined) {
        UTILS._templateCache[source] = new Function("obj",
        "var p=[],print=function(){p.push.apply(p,arguments);};" +
        "with(obj){p.push('" +
        source
          .replace(/[\r\t\n]/g, " ")
          .split("<%").join("\t")
          .replace(/((^|%>)[^\t]*)'/g, "$1\r")
          .replace(/\t=(.*?)%>/g, "',$1,'")
          .split("\t").join("');")
          .split("%>").join("p.push('")
          .split("\r").join("\\'") +
        "');}return p.join('');");
    }
    var fn = UTILS._templateCache[source];
    return data ? fn(data) : fn;
};

/** @private */
UTILS._templateCache = {};

/**
 * Multiply a string.
 * @param {String} string The string to multiply.
 * @param {Number} number The number of times to multiply the string.
 * @return Return the multilied string.
 * @type Number
 * @addon
 */
UTILS.mulStr = function (str, num) {
	var acc = [];
	for (var i = 0; (1 << i) <= num; i++) {
		if ((1 << i) & num)
			acc.push(str);
		str += str;
	}
	return acc.join('');
};

/**
 * Abbreviate a string.
 * @param {String} value The string to abbreviate.
 * @param {Number} length The number of characters to abbreviate the string
 * to.
 * @param {String} postfix A string postfixed to the end of the abbreviated
 * string (defaults to '...').
 * @return Return the abbreviated string.
 * @type Function
 * @addon
 */
UTILS.abbreviate = function (value, length, postfix) {
    if (value.length < length) {
        return value;
    }
    
    if (postfix === undefined) {
        postfix = '...';
    }
    
    var word_list = value.split(' ');
    var new_value = '';
    var i = 0;
    while (true) {
        if (i >= word_list.length) {
            break;
        }
        
        if ((new_value + word_list[i]).length >= 
            length + postfix.length - 1) {
            
            break;
        }
        
        new_value += ' ' + word_list[i];
        i++;
    }
    return new_value + postfix;
};

/**
 * Make a string of text safe for the HTML environment, normalize the line 
 * endings and strip illegal characters.
 * @params {String} The value to make safe.
 * @return The value made safe.
 * @type String
 */
UTILS.makeTextSafe = function (value) {
    
    // Normalize line endings
    value = value.replace(/\r\n/g, '\n');
    value = value.replace(/\r/g, '\n');
    
    return value;
};