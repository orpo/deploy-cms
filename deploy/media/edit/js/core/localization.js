/**
 * @fileoverview    JavaScript string localization.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

var LOCALIZATION = {stringTable: {}};

/**
 * Return a localized version of the string.
 * @param {String} The string to return a localized version of.
 * @return The localized string.
 * @type String
 **/
var _ = function _(string) {
    if (LOCALIZATION.stringTable[string.toLowerCase()] === undefined) {
        return string;
    }
    return LOCALIZATION.stringTable[string.toLowerCase()];
};