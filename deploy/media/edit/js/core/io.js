/**
 * @fileoverview    IO support for the DeployCMS framework.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 */

'use strict';

/**
 * The IO namespace.
 * @final
 * @type Namespace
 * @addon
 */  
var IO = {
    _autoIncId: 0, 
    
    /**
     * A global flag that enables/disables IO logging (defaults to true).
     * @type Boolean
     */
    logging: true
};

/**
 * A IO logger.
 * @class A class for logging IO with the DeployCMS framework.
 * @constructor
 */
IO.Logger = function() {
    EVENT.EventClass.call(this);
    
    /** @private */
    this._entries = [];
}
UTILS.copyPrototype(IO.Logger, EVENT.EventClass);

// Constants
IO.Logger.INFO = 1;
IO.Logger.ERROR = 2;

$$.extend(IO.Logger.prototype, {
    __className__: 'Logger',
    
    /**
     * Log a request.
     * @param {Number} level The level of the request logged, either 
     * (IO.Logger.INFO or IO.Logger.ERROR).
     * @param {IO.Request} request The request object to be logged.
     */
    log: function (level, request, response) {
        this.trigger('log', function () {
            this._entries.push({
                date: new Date(),
                level: level,
                request: request,
                response: response
                });
        }, {data: {'level': level, 'request': request, 'response': response}});
    },
    
    /**
     * Return a list of the logged entries.
     * @param {Number} level (Optional) If specified, the list will only
     * contain entries for the specified level.
     * @return A list of the logged entries.
     * @type Array
     */
    getEntries: function (level) {
        var entries = [];
        for (var i = 0; i < this._entries.length; i++) {
            var entry = this._entries[i];
            if (level === undefined || level === entry.level) {
                entries.push(entry);
            }
        }
        return entries;
    },
    
    /**
     * Return a string version of the logged entries.
     * @param {Number} level (Optional) If specified, the string will only
     * contain entries for the specified level.
     * @return A string of the logged entries.
     * @type String
     */
    toString: function (level) {
        var entries = this.getEntries(level);
        var logStr = '';
        for (var i = 0; i < entries.length; i++) {
            var entry = this._entries[i];
            
            // Date
            var year = entry.date.getFullYear();
            var month = ('0' + entry.date.getMonth()).substr(-2, 2);
            var day = ('0' + entry.date.getDate()).substr(-2, 2);
            var hours = ('0' + entry.date.getHours()).substr(-2, 2);
            var minutes = ('0' + entry.date.getMinutes()).substr(-2, 2);
            var seconds = ('0' + entry.date.getSeconds()).substr(-2, 2);
            var date = year + '-' + month + '-' + day + ' ' + 
                hours + ':' + minutes + ':' + seconds;
            
            logStr += date + ' >> ' + 
                {1: 'INFO', 2: 'ERROR'}[entry.level] + ' >> ' + 
                '{@' + entry.request._data['@'] + '} ' +
                entry.request._url + ' ' +
                '[' + entry.response.status + ']';
           
            if (entry.response.error !== undefined) {
                logStr += ' - ' + entry.response.error.msg || 'Unknown'; 
            }
            
            logStr += '\n';
        }
        return logStr;
    }
});

/**
 * Global IO logger.
 * @type IO.Logger
 */
IO.logger = new IO.Logger();


/**
 * A IO request.
 * @class A class for making requests to the DeployCMS framework.
 * @constructor
 */
IO.Request = function(options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._args = options.args || [];
    
    /** @private */
    this._controller = options.controller;
    
    /** @private */
    this._data = options.data || {};
    
    /** @private */
    this._method = options.method;
    
    /** @private */
    this._url = options.url;
    
    /** @private */
    this._type = options.type;
}
UTILS.copyPrototype(IO.Request, EVENT.EventClass);

$$.extend(IO.Request.prototype, {
    __className__: 'Request',
    
    /**
     * Send the request.
     */
    send: function () {
        this.trigger('send', function () {
            
            // Get an ID for the request (@)
            this._data['@'] = IO.Request.getId();
                     
            // Build the URL
            var url = this._buildURL();
            
            // Perform the request
            $$.ajax({
                cache: false,
                context: this,
                data: this._data,
                dataType: 'json',
                error: function (xhr, status, error) {
                    this.error(status || 500, {msg: error});
                },
                type: this._type,
                success: function (data, status, xhr) {
                    if (data === null || data.status === 'error') {
                        
                        // Error
                        this.error('error', data);
                    
                    } else if (data.status === 'fail') {
                        
                        // Fail
                        this.fail(status, data);
                    
                    } else if (data.status === 'success') {
                    
                        // Success
                        this.success(data.status, data);
                    
                    }
                },
                // IMPORTANT! If you don't set this lists get sent value[]=1, value[]=2...
                traditional: true, 
                url: url
            });
        });
    },
    
    /** 
     * Receive a successful response.
     */
    success: function(status, data) {
        this.trigger('success', function () {
            if (IO.logging) {
                IO.logger.log(IO.Logger.INFO, 
                    this,
                    {status: status, data: data});
            }
        }, {status: status, data: data});
    },

    /** 
     * Receive a failed response.
     */
    fail: function(status, data) {
        this.trigger('fail', function () {
            if (IO.logging) {
                IO.logger.log(IO.Logger.INFO, 
                    this,
                    {status: status, data: data});
            }
        }, {status: status, data: data});
    },
    
    /** 
     * Receive an error.
     */
    error: function(status, error) {
        this.trigger('error', function () {
            if (IO.logging) {
                IO.logger.log(IO.Logger.ERROR, 
                    this,
                    {status: status, error: error});
            }
        }, {status: status, data: error});
    },
    
    /** @private */
    _buildURL: function () {
        if (this._url !== '') {
            return this._url;
        }
		return IO.Request.buildURL(this._controller, this._method, this._args);
    },

    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        controller: '',
        method: '',
        type: 'GET',
        url: '',
        username: ''
    }
});

/**
 * Build a URL based on the controller, method and args provided.
 * @param {String} controller The name of the controller being called.
 * @param {String} method The name of the method being called against the 
 * controller.
 * @args {Array} args (Optional) A list of arguments for the call (appended 
 * 					  in the form arg1/arg2/argX/...
 * @return The URL for calling the method against the controller.
 * @type String
 */
IO.Request.buildURL = function (controller, method, args) {
	var url = '/edit/' + controller + '/' + method + '/';
	if (args !== undefined && args.length > 0) {
	    url += args.join('/') + '/';
	}
	return url;
}

/** 
 * Return a unique ID for IO requests.
 * @return A unique ID suitable for use with an IO request in the @ parameter.
 * @type String
 */
IO.Request.getId = function () {
    IO._autoIncId++;
    return String(IO._autoIncId) + String(new Date().getTime());
}