/**
 * @fileoverview    Inline (X)HTML(1.0/Strict) manipulation and optimization.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 *                  FSM (/js/core/fsm.js)
 */

'use strict';

/**
 * The IXMO namespace.
 * @final
 * @type Namespace
 */  
var IXMO = {};

/**
 * Create a new tag description.
 * @class A tag descriptor class.
 * @constructor
 * @param {String} name The tags name.
 * @param {Array} requiredAttributes A list of the names of attributes 
 * that must be specified for this tag.
 * @param {Array} optionalAttributes A list of the names of attributes 
 * that can be optionally specified for this tag.
 * @param {Boolean} canShortTag (Optional) If true the tag can be short 
 * tagged (defaults to false).
 * @param {String} preferredName (Optional) If a tag is now deprecated and
 * should be rendered using another name it can be specified as the preferred
 * name.
 */    
IXMO.TagDesc = function (name, requiredAttributes, optionalAttributes, 
    canShortTag, preferredName) {

    /**
     * The name of the tag.
     * @type String
     */
    this.name = name;
    
    /**
     * A list of required attribute names.
     * @type Array
     */
    this.requiredAttributes = requiredAttributes || [];
    
    /**
     * A list of optional attribute names.
     * @type Array
     */
    this.optionalAttributes = optionalAttributes || [];
    
    /**
     * A list of all possible attribute names.
     * @type Array
     */
    this.attributes = this.requiredAttributes + this.optionalAttributes;
    
    /**
     * Specifies if the tag can be short tagged.
     * @type Boolean
     */
    this.canShortTag = canShortTag || false;
    
    /**
     * The preferred name for the tag.
     * @type String
     */
    this.preferredName = preferredName || name;
};

// Constants
var IXMO_ALPHA_CHARS = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYy' + 
    'Zz$'.split('');
var IXMO_ALPHA_NUMERIC_CHARS = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUu' +
    'VvWwXxYyZz1234567890_-:$'.split('');
var IXMO_ENTITY_CHARS = '#AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXx' + 
    'YyZz1234567890'.split('');

/*
A quick note on the '$' (dollar) symbol that appears in IXMO_ALPHA_CHARS and
IXMO_ALPHA_NUMERIC_CHARS. This has been added to get allow custom attribute
names added by the Javascript framworks to certain tags.
*/

var IXMO_ENTITIES = { 
    34: '&quot;',    // '"' quotation mark (= APL quote)
    38: '&amp;',     // '&' ampersand
    39: '&#39;',     // ''' apostrophe (= apostrophe-quote); Can't replace
                     // this with it's verbose version because IE doesn't 
                     // support it.
    60: '&lt;',      // '<' less-than sign
    62: '&gt;',      // '>' greater-than sign
    160: '&#160;',   // ' ' no-break space (= non-breaking space)
    161: '&#161;',   // '�' inverted exclamation mark
    162: '&#162;',   // '�' cent sign
    163: '&#163;',   // '�' pound sign
    164: '&#164;',   // '�' currency sign
    165: '&#165;',   // '�' yen sign (= yuan sign)
    166: '&#166;',   // '�' broken bar (= broken vertical bar)
    167: '&#167;',   // '�' section sign
    168: '&#168;',   // '�' diaeresis (= spacing diaeresis); see German 
                     // umlaut
    169: '&#169;',   // '�' copyright sign
    170: '&#170;',   // '�' feminine ordinal indicator
    171: '&#171;',   // '�' left-pointing double angle quotation mark (= left 
                     // pointing guillemet)
    172: '&#172;',   // '�' not sign
    173: '&#173;',   // ' ' soft hyphen (= discretionary hyphen)
    174: '&#174;',   // '�' registered sign ( = registered trade mark sign)
    175: '&#175;',   // '�' macron (= spacing macron = overline = APL 
                     // overbar)
    176: '&#176;',   // '�' degree sign
    177: '&#177;',   // '�' plus-minus sign (= plus-or-minus sign)
    178: '&#178;',   // '�' superscript two (= superscript digit two = 
                     // squared)
    179: '&#179;',   // '�' superscript three (= superscript digit three = 
                     // cubed)
    180: '&#180;',   // '�' acute accent (= spacing acute)
    181: '&#181;',   // '�' micro sign
    182: '&#182;',   // '�' pilcrow sign ( = paragraph sign)
    183: '&#183;',   // '�' middle dot (= Georgian comma = Greek middle dot)
    184: '&#184;',   // '�' cedilla (= spacing cedilla)
    185: '&#185;',   // '�' superscript one (= superscript digit one)
    186: '&#186;',   // '�' masculine ordinal indicator
    187: '&#187;',   // '�' right-pointing double angle quotation mark (= 
                     // right pointing guillemet)
    188: '&#188;',   // '�' vulgar fraction one quarter (= fraction one 
                     // quarter)
    189: '&#189;',   // '�' vulgar fraction one half (= fraction one half)
    190: '&#190;',   // '�' vulgar fraction three quarters (= fraction three
                     // quarters)
    191: '&#191;',   // '�' inverted question mark (= turned question mark)
    192: '&#192;',   // '�' Latin capital letter A with grave (= Latin 
                     // capital letter A grave)
    193: '&#193;',   // '�' Latin capital letter A with acute
    194: '&#194;',   // '�' Latin capital letter A with circumflex
    195: '&#195;',   // '�' Latin capital letter A with tilde
    196: '&#196;',   // '�' Latin capital letter A with diaeresis
    197: '&#197;',   // '�' Latin capital letter A with ring above (= Latin 
                     // capital letter A ring)
    198: '&#198;',   // '�' Latin capital letter AE (= Latin capital ligature
                     // AE)
    199: '&#199;',   // '�' Latin capital letter C with cedilla
    200: '&#200;',   // '�' Latin capital letter E with grave
    201: '&#201;',   // '�' Latin capital letter E with acute
    202: '&#202;',   // '�' Latin capital letter E with circumflex
    203: '&#203;',   // '�' Latin capital letter E with diaeresis
    204: '&#204;',   // '�' Latin capital letter I with grave
    205: '&#205;',   // '�' Latin capital letter I with acute
    206: '&#206;',   // '�' Latin capital letter I with circumflex
    207: '&#207;',   // '�' Latin capital letter I with diaeresis
    208: '&#208;',   // '�' Latin capital letter ETH
    209: '&#209;',   // '�' Latin capital letter N with tilde
    210: '&#210;',   // '�' Latin capital letter O with grave
    211: '&#211;',   // '�' Latin capital letter O with acute
    212: '&#212;',   // '�' Latin capital letter O with circumflex
    213: '&#213;',   // '�' Latin capital letter O with tilde
    214: '&#214;',   // '�' Latin capital letter O with diaeresis
    215: '&#215;',   // '�' multiplication sign
    216: '&#216;',   // '�' Latin capital letter O with stroke (= Latin 
                     // capital letter O slash)
    217: '&#217;',   // '�' Latin capital letter U with grave
    218: '&#218;',   // '�' Latin capital letter U with acute
    219: '&#219;',   // '�' Latin capital letter U with circumflex
    220: '&#220;',   // '�' Latin capital letter U with diaeresis
    221: '&#221;',   // '�' Latin capital letter Y with acute
    222: '&#222;',   // '�' Latin capital letter THORN
    223: '&#223;',   // '�' Latin small letter sharp s (= ess-zed); see 
                     // German Eszett
    224: '&#224;',   // '�' Latin small letter a with grave
    225: '&#225;',   // '�' Latin small letter a with acute
    226: '&#226;',   // '�' Latin small letter a with circumflex
    227: '&#227;',   // '�' Latin small letter a with tilde
    228: '&#228;',   // '�' Latin small letter a with diaeresis
    229: '&#229;',   // '�' Latin small letter a with ring above
    230: '&#230;',   // '�' Latin small letter ae (= Latin small ligature ae)
    231: '&#231;',   // '�' Latin small letter c with cedilla
    232: '&#232;',   // '�' Latin small letter e with grave
    233: '&#233;',   // '�' Latin small letter e with acute
    234: '&#234;',   // '�' Latin small letter e with circumflex
    235: '&#235;',   // '�' Latin small letter e with diaeresis
    236: '&#236;',   // '�' Latin small letter i with grave
    237: '&#237;',   // '�' Latin small letter i with acute
    238: '&#238;',   // '�' Latin small letter i with circumflex
    239: '&#239;',   // '�' Latin small letter i with diaeresis
    240: '&#240;',   // '�' Latin small letter eth
    241: '&#241;',   // '�' Latin small letter n with tilde
    242: '&#242;',   // '�' Latin small letter o with grave
    243: '&#243;',   // '�' Latin small letter o with acute
    244: '&#244;',   // '�' Latin small letter o with circumflex
    245: '&#245;',   // '�' Latin small letter o with tilde
    246: '&#246;',   // '�' Latin small letter o with diaeresis
    247: '&#247;',   // '�' division sign
    248: '&#248;',   // '�' Latin small letter o with stroke (= Latin small
                     // letter o slash)
    249: '&#249;',   // '�' Latin small letter u with grave
    250: '&#250;',   // '�' Latin small letter u with acute
    251: '&#251;',   // '�' Latin small letter u with circumflex
    252: '&#252;',   // '�' Latin small letter u with diaeresis
    253: '&#253;',   // '�' Latin small letter y with acute
    254: '&#254;',   // '�' Latin small letter thorn
    255: '&#255;',   // '�' Latin small letter y with diaeresis
    338: '&#338;',   // '�' Latin capital ligature oe
    339: '&#339;',   // '�' Latin small ligature oe
    352: '&#352;',   // '�' Latin capital letter s with caron
    353: '&#353;',   // '�' Latin small letter s with caron
    376: '&#376;',   // '�' Latin capital letter y with diaeresis
    402: '&#402;',   // '�' Latin small letter f with hook (= function = 
                     // florin)
    710: '&#710;',   // '�' modifier letter circumflex accent
    732: '&#732;',   // '�' small tilde
    913: '&#913;',   // '?' capital letter Alpha
    914: '&#914;',   // '?' capital letter Beta
    915: '&#915;',   // 'G' Greek capital letter Gamma
    916: '&#916;',   // '?' Greek capital letter Delta
    917: '&#917;',   // '?' capital letter Epsilon
    918: '&#918;',   // '?' capital letter Zeta
    919: '&#919;',   // '?' capital letter Eta
    920: '&#920;',   // 'T' Greek capital letter Theta
    921: '&#921;',   // '?' capital letter Iota
    922: '&#922;',   // '?' capital letter Kappa
    923: '&#923;',   // '?' Greek capital letter Lambda
    924: '&#924;',   // '?' capital letter Mu
    925: '&#925;',   // '?' capital letter Nu
    926: '&#926;',   // '?' Greek capital letter Xi
    927: '&#927;',   // '?' capital letter Omicron
    928: '&#928;',   // '?' capital letter Pi
    929: '&#929;',   // '?' capital letter Rho
    931: '&#931;',   // 'S' Greek capital letter Sigma
    932: '&#932;',   // '?' capital letter Tau
    933: '&#933;',   // '?' Greek capital letter Upsilon
    934: '&#934;',   // 'F' Greek capital letter Phi
    935: '&#935;',   // '?' capital letter Chi
    936: '&#936;',   // '?' Greek capital letter Psi
    937: '&#937;',   // 'O' Greek capital letter Omega
    945: '&#945;',   // 'a' Greek small letter alpha
    946: '&#946;',   // '�' Greek small letter beta
    947: '&#947;',   // '?' Greek small letter gamma
    948: '&#948;',   // 'd' Greek small letter delta
    949: '&#949;',   // 'e' Greek small letter epsilon
    950: '&#950;',   // '?' Greek small letter zeta
    951: '&#951;',   // '?' Greek small letter eta
    952: '&#952;',   // '?' Greek small letter theta
    953: '&#953;',   // '?' Greek small letter iota
    954: '&#954;',   // '?' Greek small letter kappa
    955: '&#955;',   // '?' Greek small letter lambda
    956: '&#956;',   // '�' Greek small letter mu
    957: '&#957;',   // '?' Greek small letter nu
    958: '&#958;',   // '?' Greek small letter xi
    959: '&#959;',   // '?' Greek small letter omicron
    960: '&#960;',   // 'p' Greek small letter pi
    961: '&#961;',   // '?' Greek small letter rho
    962: '&#962;',   // '?' Greek small letter final sigma
    963: '&#963;',   // 's' Greek small letter sigma
    964: '&#964;',   // 't' Greek small letter tau
    965: '&#965;',   // '?' Greek small letter upsilon
    966: '&#966;',   // 'f' Greek small letter phi
    967: '&#967;',   // '?' Greek small letter chi
    968: '&#968;',   // '?' Greek small letter psi
    969: '&#969;',   // '?' Greek small letter omega
    977: '&#977;',   // '?' Greek theta symbol
    978: '&#978;',   // '?' Greek Upsilon with hook symbol
    982: '&#982;',   // '?' Greek pi symbol
    8194: '&#8194;', // ' ' en space
    8195: '&#8195;', // ' ' em space
    8201: '&#8201;', // '?' thin space
    8204: '&#8204;', // ' ' RFC 2070 zero-width non-joiner
    8205: '&#8205;', // ' ' RFC 2070 zero-width joiner
    8206: '&#8206;', // ' ' RFC 2070 left-to-right mark
    8207: '&#8207;', // ' ' RFC 2070 right-to-left mark
    8211: '&#8211;', // '�' en dash
    8212: '&#8212;', // '�' em dash
    8216: '&#8216;', // '�' left single quotation mark
    8217: '&#8217;', // '�' right single quotation mark
    8218: '&#8218;', // '�' single low-9 quotation mark
    8220: '&#8220;', // '�' left double quotation mark
    8221: '&#8221;', // '�' right double quotation mark
    8222: '&#8222;', // '�' double low-9 quotation mark
    8224: '&#8224;', // '�' dagger
    8225: '&#8225;', // '�' double dagger
    8226: '&#8226;', // '�' bullet (= black small circle)
    8230: '&#8230;', // '�' horizontal ellipsis (= three dot leader)
    8240: '&#8240;', // '�' per mille sign
    8242: '&#8242;', // ''' prime (= minutes = feet)
    8243: '&#8243;', // '?' double prime (= seconds = inches)
    8249: '&#8249;', // '�' proposed single left-pointing angle quotation 
                     // mark
    8250: '&#8250;', // '�' proposed single right-pointing angle quotation 
                     // mark
    8254: '&#8254;', // '?' overline (= spacing overscore)
    8260: '&#8260;', // '/' fraction slash (= solidus)
    8364: '&#8364;', // '�' euro sign
    8465: '&#8465;', // 'I' black-letter capital I (= imaginary part)
    8472: '&#8472;', // 'P' script capital P (= power set = Weierstrass p)
    8476: '&#8476;', // 'R' black-letter capital R (= real part symbol)
    8482: '&#8482;', // '�' trademark sign
    8501: '&#8501;', // '?' alef symbol (= first transfinite cardinal)
    8592: '&#8592;', // '?' leftwards arrow
    8593: '&#8593;', // '?' upwards arrow
    8594: '&#8594;', // '?' rightwards arrow
    8595: '&#8595;', // '?' downwards arrow
    8596: '&#8596;', // '?' left right arrow
    8629: '&#8629;', // '?' downwards arrow with corner leftwards (= carriage
                     // return)
    8656: '&#8656;', // '?' leftwards double arrow
    8657: '&#8657;', // '?' upwards double arrow
    8658: '&#8658;', // '?' rightwards double arrow
    8659: '&#8659;', // '?' downwards double arrow
    8660: '&#8660;', // '?' left right double arrow
    8704: '&#8704;', // '?' for all
    8706: '&#8706;', // '?' partial differential
    8707: '&#8707;', // '?' there exists
    8709: '&#8709;', // '�' empty set (= null set = diameter)
    8711: '&#8711;', // '?' nabla (= backward difference)
    8712: '&#8712;', // '?' element of
    8713: '&#8713;', // '?' not an element of
    8715: '&#8715;', // '?' contains as member
    8719: '&#8719;', // '?' n-ary product (= product sign)
    8721: '&#8721;', // '?' n-ary summation
    8722: '&#8722;', // '-' minus sign
    8727: '&#8727;', // '*' asterisk operator
    8730: '&#8730;', // 'v' square root (= radical sign)
    8733: '&#8733;', // '?' proportional to
    8734: '&#8734;', // '8' infinity
    8736: '&#8736;', // '?' angle
    8743: '&#8743;', // '?' logical and (= wedge)
    8744: '&#8744;', // '?' logical or (= vee)
    8745: '&#8745;', // 'n' intersection (= cap)
    8746: '&#8746;', // '?' union (= cup)
    8747: '&#8747;', // '?' integral
    8756: '&#8756;', // '?' therefore
    8764: '&#8764;', // '~' tilde operator (= varies with = similar to)
    8773: '&#8773;', // '?' congruent to
    8776: '&#8776;', // '�' almost equal to (= asymptotic to)
    8800: '&#8800;', // '?' not equal to
    8801: '&#8801;', // '=' identical to; sometimes used for 'equivalent to'
    8804: '&#8804;', // '=' less-than or equal to
    8805: '&#8805;', // '=' greater-than or equal to
    8834: '&#8834;', // '?' subset of
    8835: '&#8835;', // '?' superset of
    8836: '&#8836;', // '?' not a subset of
    8838: '&#8838;', // '?' subset of or equal to
    8839: '&#8839;', // '?' superset of or equal to
    8853: '&#8853;', // '?' circled plus (= direct sum)
    8855: '&#8855;', // '?' circled times (= vector product)
    8869: '&#8869;', // '?' up tack (= orthogonal to = perpendicular)
    8901: '&#8901;', // '�' dot operator
    8968: '&#8968;', // '?' left ceiling (= APL upstile)
    8969: '&#8969;', // '?' right ceiling
    8970: '&#8970;', // '?' left floor (= APL downstile)
    8971: '&#8971;', // '?' right floor
    9001: '&#9001;', // '<' left-pointing angle bracket (= bra)
    9002: '&#9002;', // '>' right-pointing angle bracket (= ket)
    9674: '&#9674;', // '?' lozenge
    9824: '&#9824;', // '?' black spade suit
    9827: '&#9827;', // '?' black club suit (= shamrock)
    9829: '&#9829;', // '?' black heart suit (= valentine)
    9830: '&#9830;'  // '?' black diamond suit
};

// Create a reverse loop up table for entities
var IXMO_ENTITIES_REVERSE = {};
for (var charCode in IXMO_ENTITIES) {
    if (IXMO_ENTITIES.hasOwnProperty(charCode)) {
        IXMO_ENTITIES_REVERSE[IXMO_ENTITIES[charCode]] = charCode;
    }
}

/* 
Tip: A two element Array containing the parenthesis for rendering the Tag,
modifying the value from ['<', '>'] to ['[', ']'] can be useful for debugging.
*/
var IXMO_PARENTHESIS = ['<', '>'];
var IXMO_DELIMITER = '"';
var IXMO_COMMON_ATTRIBUTES = 'id|class|style|title|lang|xml:lang|dir|' + 
    'onclick|ondblclick|onmousedown|onmouseup|onmousemove|onmouseout|' + 
    'onkeypress|onkeydown|onkeyup'.split('|');

/*
Important! The slice() method is called against IXMO_COMMON_ATTRIBUTES where 
it is specified on it's own to ensure only a copy of the array is sent to the
TagDesc instance.
Important! though in XHTML it is legal to short tag many tags, only a 
sensible subset is supported (ones which don't cause issues in browsers).
*/
var IXMO_INLINE_TAGS = {
    'a': new IXMO.TagDesc('a', [], 
        IXMO_COMMON_ATTRIBUTES + ['charset', 'type', 'name', 'href', 
        'hreflang', 'rel', 'rev', 'shape', 'coords'], false),
    'abbr': new IXMO.TagDesc('abbr', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'acronym': new IXMO.TagDesc('acronym', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'b': new IXMO.TagDesc('b', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false, 'strong'),
    'bdo': new IXMO.TagDesc('bdo', ['dir'], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'big': new IXMO.TagDesc('big', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'br': new IXMO.TagDesc('br', [], 
        ['id', 'class', 'style', 'title'], true),
    'wbr': new IXMO.TagDesc('br', [], 
        ['id', 'class', 'style', 'title'], true),
    'cite': new IXMO.TagDesc('cite', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'code': new IXMO.TagDesc('code', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'del': new IXMO.TagDesc('del', [], 
        IXMO_COMMON_ATTRIBUTES + ['cite', 'datetime'], false),
    'dfn': new IXMO.TagDesc('dfn', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'em': new IXMO.TagDesc('em', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'i': new IXMO.TagDesc('i', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false, 'em'),
    'img': new IXMO.TagDesc('img', [], 
        IXMO_COMMON_ATTRIBUTES + ['src', 'alt', 'longdesc', 'height',
        'width', 'usemap', 'ismap'], true),
    'ins': new IXMO.TagDesc('ins', [], 
        IXMO_COMMON_ATTRIBUTES + ['cite', 'datetime'], false),
    'kbd': new IXMO.TagDesc('kbd', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'q': new IXMO.TagDesc('q', [], 
        IXMO_COMMON_ATTRIBUTES + ['cite'], false),
    'samp': new IXMO.TagDesc('samp', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'small': new IXMO.TagDesc('small', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'span': new IXMO.TagDesc('span', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'strong': new IXMO.TagDesc('strong', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'sub': new IXMO.TagDesc('sub', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'sup': new IXMO.TagDesc('sup', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'tt': new IXMO.TagDesc('tt', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false),
    'var': new IXMO.TagDesc('var', [], 
        IXMO_COMMON_ATTRIBUTES.slice(), false)
};

// Parser states
var IXMO_PS_CHAR_OR_ENTITY_OR_TAG = 1;
var IXMO_PS_ENTITY = 2;
var IXMO_PS_OPENNING_OR_CLOSING_TAG = 3;
var IXMO_PS_OPENING_TAG = 4;
var IXMO_PS_CLOSING_TAG = 5;
var IXMO_PS_TAG_NAME_OPENING = 6;
var IXMO_PS_TAG_NAME_CLOSING = 7;
var IXMO_PS_TAG_OPENING_SHORT_TAG = 8;
var IXMO_PS_TAG_NAME_MUST_CLOSE = 9;
var IXMO_PS_ATTR_OR_TAG_END = 10;
var IXMO_PS_ATTR_NAME = 11;
var IXMO_PS_ATTR_NAME_MUST_GET_VALUE = 12;
var IXMO_PS_ATTR_DELIM = 13;
var IXMO_PS_ATTR_VALUE_SINGLE_DELIM = 14;
var IXMO_PS_ATTR_VALUE_DOUBLE_DELIM = 15;
var IXMO_PS_ATTR_VALUE_NO_DELIM = 16;
var IXMO_PS_ATTR_ENTITY_NO_DELIM = 17;
var IXMO_PS_ATTR_ENTITY_SINGLE_DELIM = 18;
var IXMO_PS_ATTR_ENTITY_DOUBLE_DELIM = 19;

/*
Tip: The parser only provides basic validation, not strict validation. Junk 
in will probably provide junk out.
*/

/**
 * Create a new inline XHTML parser.
 * @class A parser for converting inline XHTML into {@link IXMO.Soup}.
 * @constructor
 */ 
IXMO.Parser = function () {
    
    // Parsing attributes 
    
    /** @private **/
    this._index = 0;
    
    /** @private **/
    this._row = 1;
    
    /** @private **/
    this._column = 0; 
    
    /** @private **/
    this._characters = [];
    
    /** @private **/
    this._tagStack = [];
    
    /** @private **/
    this._entity = '';
    
    /** @private **/
    this._tagName = '';
    
    /** @private **/
    this._shortTagged = false;
    
    /** @private **/
    this._attributes = [];
    
    /** @private **/
    this._attributeName = '';
    
    /** @private **/
    this._attributeValue = '';
    
    // Define the machine states to parse the inline HTML
    
    /** @private **/
    this._machine = new FSM.Machine(this);
    this._machine.setInitialState(IXMO_PS_CHAR_OR_ENTITY_OR_TAG);
    
    // Character or Tag
    this._machine.addTransitionAny(IXMO_PS_CHAR_OR_ENTITY_OR_TAG, null,
        function (action) {
            this.data._storeCharacter(action);
        });
    this._machine.addTransition('<', IXMO_PS_CHAR_OR_ENTITY_OR_TAG, 
        IXMO_PS_OPENNING_OR_CLOSING_TAG);
    this._machine.addTransition('&', IXMO_PS_CHAR_OR_ENTITY_OR_TAG, 
        IXMO_PS_ENTITY);
    
    // Entity
    this._machine.addTransitions(IXMO_ENTITY_CHARS, IXMO_PS_ENTITY, null,
        function (action) {
            this.data._entity += action;
        });
    this._machine.addTransition(';', IXMO_PS_ENTITY, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._storeCharacter('&' + this.data._entity + ';');
            this.data._entity = '';
        });
    
    // Opening or Closing Tag
    this._machine.addTransitions([' ', '\n'], 
        IXMO_PS_OPENNING_OR_CLOSING_TAG);
    this._machine.addTransitions(IXMO_ALPHA_CHARS, 
        IXMO_PS_OPENNING_OR_CLOSING_TAG, IXMO_PS_OPENING_TAG,
        function () {
            this.data._index--;
        });
    this._machine.addTransition('/', IXMO_PS_OPENNING_OR_CLOSING_TAG, 
        IXMO_PS_CLOSING_TAG);
    
    // Opening Tag
    this._machine.addTransitions([' ', '\n'], IXMO_PS_OPENING_TAG);
    this._machine.addTransitions(IXMO_ALPHA_CHARS, IXMO_PS_OPENING_TAG, 
        IXMO_PS_TAG_NAME_OPENING,
        function () {
            this.data._index--;
        });
    
    // Closing Tag
    this._machine.addTransitions([' ', '\n'], IXMO_PS_CLOSING_TAG);
    this._machine.addTransitions(IXMO_ALPHA_CHARS, IXMO_PS_CLOSING_TAG, 
        IXMO_PS_TAG_NAME_CLOSING,
        function () {
            this.data._index--;
        });
    
    // Tag name opening
    this._machine.addTransitions(IXMO_ALPHA_NUMERIC_CHARS, 
        IXMO_PS_TAG_NAME_OPENING, null,
        function (action) {
            this.data._tagName += action;
        });   
    this._machine.addTransitions([' ', '\n'], IXMO_PS_TAG_NAME_OPENING,
        IXMO_PS_ATTR_OR_TAG_END);
    this._machine.addTransition('/', IXMO_PS_TAG_NAME_OPENING, 
        IXMO_PS_TAG_OPENING_SHORT_TAG,
        function () {
            this.data._shortTagged = true;
        });
    this._machine.addTransition('>', IXMO_PS_TAG_NAME_OPENING, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._pushTag();
        });
    this._machine.addTransitions([' ', '\n'], 
        IXMO_PS_TAG_OPENING_SHORT_TAG);
    this._machine.addTransition('>', IXMO_PS_TAG_OPENING_SHORT_TAG, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._pushTag();
        });
    this._machine.addTransitions([' ', '\n'],
        IXMO_PS_ATTR_OR_TAG_END);
    this._machine.addTransition('/',
        IXMO_PS_ATTR_OR_TAG_END,
        IXMO_PS_TAG_OPENING_SHORT_TAG,
        function () {
            this.data._shortTagged = true;
        });
    this._machine.addTransition('>', IXMO_PS_ATTR_OR_TAG_END, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._pushTag();
        });
    this._machine.addTransitions(IXMO_ALPHA_CHARS, IXMO_PS_ATTR_OR_TAG_END,
        IXMO_PS_ATTR_NAME,
        function () {
            this.data._index--;
        });
    
    // Tag name closing
    this._machine.addTransitions(IXMO_ALPHA_NUMERIC_CHARS, 
        IXMO_PS_TAG_NAME_CLOSING, null,
        function (action) {
            this.data._tagName += action;
        }); 
    this._machine.addTransitions([' ', '\n'], IXMO_PS_TAG_NAME_CLOSING, 
        IXMO_PS_TAG_NAME_MUST_CLOSE);
    this._machine.addTransition('>', IXMO_PS_TAG_NAME_CLOSING, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._popTag();
        });
    this._machine.addTransitions([' ', '\n'], IXMO_PS_TAG_NAME_MUST_CLOSE);
    this._machine.addTransition('>', IXMO_PS_TAG_NAME_MUST_CLOSE, 
        IXMO_PS_CHAR_OR_ENTITY_OR_TAG,
        function () {
            this.data._popTag();
        });
    
    // Attribute name
    this._machine.addTransitions(IXMO_ALPHA_NUMERIC_CHARS, 
        IXMO_PS_ATTR_NAME, null,
        function (action) {
            this.data._attributeName += action;
        });
    this._machine.addTransitions([' ', '\n'], IXMO_PS_ATTR_NAME, 
        IXMO_PS_ATTR_NAME_MUST_GET_VALUE);
    this._machine.addTransition('=', IXMO_PS_ATTR_NAME, 
        IXMO_PS_ATTR_DELIM);
    this._machine.addTransitions([' ', '\n'], 
        IXMO_PS_ATTR_NAME_MUST_GET_VALUE);
    this._machine.addTransition('=', IXMO_PS_ATTR_NAME_MUST_GET_VALUE, 
        IXMO_PS_ATTR_DELIM);
    
    // Attribute delimiter
    this._machine.addTransitions([' ', '\n'], IXMO_PS_ATTR_DELIM);
    this._machine.addTransition('\'', IXMO_PS_ATTR_DELIM, 
        IXMO_PS_ATTR_VALUE_SINGLE_DELIM);
    this._machine.addTransition('"', IXMO_PS_ATTR_DELIM, 
        IXMO_PS_ATTR_VALUE_DOUBLE_DELIM);
    
    // Fix for browsers (including IE) that output quoted attributes
    this._machine.addTransitions(IXMO_ALPHA_NUMERIC_CHARS + ['&'], 
        IXMO_PS_ATTR_DELIM, IXMO_PS_ATTR_VALUE_NO_DELIM, 
        function () {
            this.data._index--;
        });
    this._machine.addTransition(' ', IXMO_PS_ATTR_VALUE_NO_DELIM, 
        IXMO_PS_ATTR_OR_TAG_END,
        function () {
            this.data._storeAttribute();
        });
    this._machine.addTransitions(['/', '>'], IXMO_PS_ATTR_VALUE_NO_DELIM, 
        IXMO_PS_ATTR_OR_TAG_END, 
        function () {
            this.data._index--;
            this.data._storeAttribute();
        });
    this._machine.addTransition('&', IXMO_PS_ATTR_VALUE_NO_DELIM,
        IXMO_PS_ATTR_ENTITY_NO_DELIM);
    this._machine.addTransitionAny(IXMO_PS_ATTR_VALUE_NO_DELIM, null,
        function (action) {
            this.data._attributeValue += action;
        });
    
    // Attibute value single delimiter
    this._machine.addTransition('\'', IXMO_PS_ATTR_VALUE_SINGLE_DELIM, 
        IXMO_PS_ATTR_OR_TAG_END,
        function () {
            this.data._storeAttribute();
        });
    this._machine.addTransition('&', IXMO_PS_ATTR_VALUE_SINGLE_DELIM,
        IXMO_PS_ATTR_ENTITY_SINGLE_DELIM);
    this._machine.addTransitionAny(IXMO_PS_ATTR_VALUE_SINGLE_DELIM, null,
        function (action) {
            this.data._attributeValue += action;
        });
    
    // Attribute value double
    this._machine.addTransition('"', IXMO_PS_ATTR_VALUE_DOUBLE_DELIM,
        IXMO_PS_ATTR_OR_TAG_END,
        function () {
            this.data._storeAttribute();
        });
    this._machine.addTransition('&', IXMO_PS_ATTR_VALUE_DOUBLE_DELIM,
        IXMO_PS_ATTR_ENTITY_DOUBLE_DELIM);    
    this._machine.addTransitionAny(IXMO_PS_ATTR_VALUE_DOUBLE_DELIM, 
        null,
        function (action) {
            if (IXMO_ENTITIES[action.charCodeAt(0)]) {
                throw new Error('Unescaped character in attribute value: ' + 
                    action);
            }
            this.data._updateAttibuteValue(action);
        });
    
    // Entity in attribute value
    this._machine.addTransitions(IXMO_ENTITY_CHARS, 
        IXMO_PS_ATTR_ENTITY_NO_DELIM, null,
        function (action) {
            this.data._entity += action;
        });
    this._machine.addTransitions(IXMO_ENTITY_CHARS, 
        IXMO_PS_ATTR_ENTITY_SINGLE_DELIM, null,
        function (action) {
            this.data._entity += action;
        });
    this._machine.addTransitions(IXMO_ENTITY_CHARS, 
        IXMO_PS_ATTR_ENTITY_DOUBLE_DELIM, null,
        function (action) {
            this.data._entity += action;
        });
    this._machine.addTransition(';', IXMO_PS_ATTR_ENTITY_NO_DELIM, 
        IXMO_PS_ATTR_VALUE_NO_DELIM,
        function () {
            this.data._updateAttibuteValue('&' + this.data._entity + ';');
            this.data._entity = '';
        });
    this._machine.addTransition(';', IXMO_PS_ATTR_ENTITY_SINGLE_DELIM, 
        IXMO_PS_ATTR_VALUE_SINGLE_DELIM,
        function () {
            this.data._updateAttibuteValue('&' + this.data._entity + ';');
            this.data._entity = '';
        });
    this._machine.addTransition(';', IXMO_PS_ATTR_ENTITY_DOUBLE_DELIM, 
        IXMO_PS_ATTR_VALUE_DOUBLE_DELIM,
        function () {
            this.data._updateAttibuteValue('&' + this.data._entity + ';');
            this.data._entity = '';
        });
};

IXMO.Parser.prototype = {
    
    /**
     * Parse a string of XHTML. 
     * @param {String} source The XHTML to parse.
     * @param {Object} options An object containing parsing options;
     * @param {Boolean} fixShortTags (Option) If this option is set to true
     * the parser will attempt to fix incorrectly short tagged tags such as
     * images and line breaks (true by default).
     * @param {Boolean} trimSpaces (Option) If this option is set to true
     * the parser will trim pre/postfixed whitespace from the source (true 
     * by default).
     * @param {Boolean} escapeEntities (Option) If this option is set to true
     * the parser will automatically convert escape XHTML entities 
     * whilst parsing (true by default).
     * @param {Boolean} validateAttributes (Option) If this option is set to
     * true the parser will validate that only supported attributes ares 
     * specified for tags.
     * @return A soup of the parsed XHTML.
     * @type IXMO.Soup
     */
    parse: function (source, options) {
        options = UTILS.optionsArg(options, {
            trimSpaces: true,
            fixShortTags: true,
            escapeEntities: false,
            validateAttributes: true
        });

        this._escapeEntities = options.escapeEntities;
        this._validateAttributes = options.validateAttributes;
        
        this._index = 0;
        this._row = 1;
        this._column = 0; 
        
        this._characters = [];
        this._tagStack = [];
        
        this._entity = '';
        this._tagName = '';
        this._shortTagged = false;
        this._attributes = [];
        this._attributeName = '';
        this._attributeValue = ''; 
        
        // Pre-processing...
        
        // Format the line breaks to '\n'
        source = source.replace(/\r\n/g, '\n');
        source = source.replace(/\r/g, '\n');
        
        // Fix short tags
        if (options.fixShortTags) {
            source = source.replace(/<\s*BR\s*>/gim, '<br />');
            source = source.replace(/<\s*IMG([\s\S]+?)(\/>|>)/gi, 
                '<img $1 />');
        }
        
        // Remove all comments
        source = source.replace(/<!--[\s\S]-->/g, '');
        
        // Remove double spacing
        source = source.replace(/ +/g, ' ');
        
        // Trim post/prefixed spaces
        if (options.trimSpaces) {
            source = source.replace(/^\s+|\s+$/g, '');
        }
        
        // Parse the XHTML 
        for (this._index = 0; this._index < source.length; this._index++) {
            // Get the next character
            var character = source.charAt(this._index);
            // Debug tracking
            if (character === '\n') {
                this._column = 0;
                this._row++;
                continue;
            }
            this._column++;
            // Attempt to process the next character
            try {
                this._machine.process(character);
            } catch (error) {
                throw new Error('Error at line: ' + this._row + 
                    ', column: ' + this._column + ' >> ' + error.toString());
            }
        }
        // Check the parser had finished when we run out of data to parse
        if (this._machine.getCurrentState() !== 
                IXMO_PS_CHAR_OR_ENTITY_OR_TAG) {
            throw new Error('Invalid XHTML, parser expected more content.');
        }
        
        return new IXMO.Soup(this._characters);
    },
    
    /** @private */
    _pushTag: function () {
        
        // Build a list of Attributes that apply to the Tag
        var attributes = [];
        var attributeNames = [];
        for (var i = 0; i < this._attributes.length; i++) {
            attributes.push(this._attributes[i]);
            attributeNames.push(this._attributes[i]._name);
        }
        
        // Push the Tag on to the stack
        var tag = new IXMO.Tag(this._tagName, attributes, 
            this._shortTagged, this._validateAttributes);
        this._tagStack.push(tag);
        
        // Handle short tagged Tags by adding an empty Character
        if (this._shortTagged) {
            this._storeCharacter('');
            this._tagStack.pop();
        }
        
        // Reset the tag buffers
        this._tagName = '';
        this._shortTagged = false;
        this._attributes = [];
    },    
        
    /** @private */
    _popTag: function () {
        var lastTag = this._tagStack.pop();
        
        // Check the tags are balanced
        if (lastTag._initialName !== this._tagName.toLowerCase()) {
            throw new Error('Unmatched tag \'</' + this._tagName + '>\'.');
        }
        
        // Move white space at the end of a tag out of the tag
        if (this._characters.length > 0) {
            var last = this._characters[this._characters.length - 1];
            var lastCharacter = last.toString();
            if (lastCharacter === ' ') {
                last.removeTags(lastTag);
            }
        }
        
        // Reset the tag buffers
        this._tagName = '';  
    }, 
    
    /** @private */
    _storeAttribute: function () {
        // Store the attribute
        var attribute = new IXMO.Attribute(this._attributeName, 
            this._attributeValue);
        this._attributes.push(attribute);
        
        // Reset the attribute buffers
        this._attributeName = '';
        this._attributeValue = '';
    },
    
    /** @private */
    _updateAttibuteValue: function (character) {
        if (character.length === 1 && 
            IXMO_ENTITIES[character.charCodeAt(0)]) {
            
            throw new Error('Unescaped character in attribute value: ' + 
                character);
        }
        this._attributeValue += character;
    },
    
    /** @private */
    _storeCharacter: function (character) {
        if (character.length === 1 && 
            IXMO_ENTITIES[character.charCodeAt(0)]) {
            
            if (this._escapeEntities) {
                throw new Error('Unescaped character: ' + character);
            } else {
                character = IXMO_ENTITIES[character.charCodeAt(0)];
            }
        }
        
        // Remove double whitespaces inbetween tags
        if (character === ' ' && this._characters.length > 0) {
            var last = this._characters[this._characters.length - 1];
            var lastCharacter = last.toString();
            if (lastCharacter === ' ') {
                return;
            }
        }
        
        this._characters.push(new IXMO.Character(character, 
            this._tagStack.slice()));    
    }
};

// Class methods

/**
 * Escape a string to use XHTML entities.
 * @param {String} value The value to escape.
 * @return The escaped value.
 * @type String
 */
IXMO.Parser.escapeEntity = function (value) {
    var escapedValue = '';
    for (var i = 0; i < value.length; i++) {
        if (IXMO_ENTITIES[value.charCodeAt(i)]) {
            escapedValue += IXMO_ENTITIES[value.charCodeAt(i)];
        } else {
            escapedValue += value.charAt(i);
        }
    }
    return escapedValue;
};

/**
 * Unescape a soup that uses XHTML entities.
 * @param {IXMO.Character|IXMO.Soup} value The value to unescape.
 * @return The unescaped value.
 * @type String
 */
IXMO.Parser.unescapeEntity = function (value) {
    if (value instanceof IXMO.Character) {
        if (IXMO_ENTITIES_REVERSE[value.toString()]) {
            return String.fromCharCode(
                IXMO_ENTITIES_REVERSE[value.toString()]);
        }
        return value.toString();
    } else {
        var unescapedValue = '';
        for (var i = 0; i < value.getLength(); i++) {
            var character = value.charAt(i).toString();
            if (IXMO_ENTITIES_REVERSE[character]) {
                unescapedValue += String.fromCharCode(
                    IXMO_ENTITIES_REVERSE[character]);
            } else {
                unescapedValue += value.charAt(character);
            }
        }
        return unescapedValue;
    }
};

/**
 * Create a new inline XHMTL soup.
 * @class A soup structure for maniplulating inline XHTML.
 * @constructor
 * @param {String|Array} source A string of XHTML to parse or an array of 
 * {@link IXMO.Character}s.
 * @param {Object} options Parsing options (see {@link IXMO.Parser}).
 */ 
IXMO.Soup = function (source, options) {
    if (typeof source === 'string') {
        source = new IXMO.Parser().parse(source, options)._characters;
    }
    
    if (source instanceof IXMO.Soup) {
        source = source._characters;
    }
    
    /** @private */
    this._characters = source;
};

IXMO.Soup.prototype = {
    
    // String selection & manipulation
    
    /**
     * Return the lenth of the soup (in characters).
     * @return The length of the soup.
     * @type Number
     */
    getLength: function () {
        return this._characters.length;
    },
    
    /**
     * Return true if the soup contains only whitespace characters.
     * @return Whether the soup is all whitespace.
     * @type Boolean
     */
    allWhitespace: function () {
        return this.rawCopy().replace(/\s*/g, '') === '';
    },
        
    /**
     * Return true if the specified position is in the range 0 to the length
     * of the soup.
     * @param {Number} position The position to check is in range.
     * @return Whether the position is in range.
     * @type Boolean
     */
    inRange: function (position) {
        return !(position < 0 || position > this._characters.length);
    },

    /**
     * Return a character from the soup at the specified position.
     * @param {Number} position The position of the character to return.
     * @return A character at the specified position.
     * @type IXMO.Character
     * @see #copy
     * @see #paste
     */
    charAt: function (position) {
        if (!this.inRange(position)) {
            throw new Error('Invalid position, outside of character range.');
        }
        
        return this._characters[position].copy();
    },
    
    /**
     * Find the index of a string or soup within the soup starting from the
     * left-hand side.
     * @param {String|IXMO.Soup} searchSoup A string or soup to find within
     * the soup.
     * @param {Number} start (Optional) The position to start looking from.
     * @param {Boolean} loose (Optional) If true the search only checks if 
     * the characters themselves match (defaults to false).
     * @return The position at which any match with the searchSoup occurs.
     * @type Number
     * @see #rindexOf
     */
    indexOf: function (searchSoup, start, loose) {
        if (typeof searchSoup === 'string') {
            searchSoup = new IXMO.Parser().parse(searchSoup);
        }
             
        start = start || 0;
        
        // Loose matches (short-cut)
        if (loose) {
            return this.rawCopy().indexOf(searchSoup.rawCopy(), start);
        }
        
        // Exact macth
        var rawSoup = this.rawCopy();
        var rawSearchSoup = searchSoup.rawCopy();
        
        var index = start;
        while (true) {
            index = rawSoup.indexOf(rawSearchSoup, index);
            if (index === -1) {
                break;
            }
            
            var matchSoup = this.copy([index, 
                index + searchSoup.getLength()]);
            if (matchSoup.toString() === searchSoup.toString()) {
                return index;
            }
            
            index++;
        }
        
        return -1;
    },

    /**
     * Chop the last character from the soup.
     * @return The chopped character.
     * @type IXMO.Character
     */
    chop: function () {
        if (this._characters.length > 0) {
            return this._characters.pop();
        }
    },
    
    /**
     * Chop the last character from the soup.
     * @return The chopped character.
     * @type IXMO.Character
     */
    chomp: function () {
        if (this._characters.length > 0) {
            if (this._characters[
                this._characters.length - 1].isWhitespace()) {
                
                return this._characters.pop();
            }
        }
    },    
    
    /**
     * Cut a selection of characters from the soup.
     * @param {Array} select The selection of characters to cut from the 
     * Soup.
     * @return A soup containing the cut selection.
     * @type IXMO.Soup
     * @see #copy
     * @see #paste
     */
    cut: function (select) {
        select = this._parseSelect(select);
        
        return new IXMO.Soup(this._characters.splice(select[0],
            select[1] - select[0]));
    },
    
    /**
     * Copy a selection of characters from the soup.
     * @param {Array} select The selection of characters to copy from the 
     * Soup.
     * @return A soup containing the copied selection.
     * @type IXMO.Soup
     * @see #cut
     * @see #paste
     */
    copy: function (select) {
        select = this._parseSelect(select);
        
        var characters = [];
        for (var i = select[0]; i < select[1]; i++) {
            characters.push(this._characters[i].copy());
        }
        return new IXMO.Soup(characters);
    },
       
    /**
     * Split the soup at the designated position(s). If position is 
     * specified as an Array of two then the split returns the contents 
     * before the left position, after the right position and ignores the
     * contents inbetween.
     * @param {Number|Array} position Specifies the position at which
     * to make the split.
     * @param {Boolean} trimSpaces If set to true pre/postfixed whitespace 
     * will be removed from the soups resulting from the split (true by 
     * default).
     * @return An array of two soups resulting from the split.
     * @type Array
     */
    split: function (position, trimSpaces) {
        if (position[0] === undefined || position[1] === undefined) {
            position = [position, position];
        }
        
        if (trimSpaces === undefined) {
            trimSpaces = true;
        }
        
        if (!this.inRange(position[0]) || !this.inRange(position[1])) {
            throw new Error('Invalid position, outside of character range.');
        }
        
        // Perform the split
        var i = 0;
        var leftSplit = [];
        for (i = 0; i < position[0]; i++) {
            leftSplit.push(this._characters[i].copy());
        }
        
        var rightSplit = [];
        for (i = position[1]; i < this._characters.length; i++) {
            rightSplit.push(this._characters[i].copy());
        }
        
        // Trim whitespace
        var leftSoup = new IXMO.Soup(leftSplit, trimSpaces);
        var rightSoup = new IXMO.Soup(rightSplit, trimSpaces);
        
        return [leftSoup, rightSoup];
    },
    
    /**
     * Paste a string or soup into the soup.
     * @param {String|IXMO.Soup} source The string or soup to paste.
     * @param {Number} position The position to paste the source at.
     * @see #copy
     * @see #cut
     */
    paste: function (position, source) {
        if (!this.inRange(position)) {
            throw new Error('Invalid position, outside of character range.');
        }
        
        if (typeof source === 'string') {
            source = new IXMO.Parser().parse(source, 
                {trimSpaces: false}).copy();
        }
        
        var start = this._characters.slice(0, position);
        var end = this._characters.slice(position);
        this._characters = start;
        this._characters = this._characters
            .concat(source._characters)
            .concat(end);
    },
    
    /**
     * Insert text into the soup.
     * @param {String} text The text to be inserted.
     * @param {Number} position The position at which to insert the text. 
     * @see #append
     * @see #prepend
     */
    insert: function (position, text) {
        if (!this.inRange(position)) {
            throw new Error('Invalid position, outside of character range.');
        }
        
        var characters = [];
        for (var i = 0; i < this._characters.length + 1; i++) {
            if (i === position) {
                // Get the list of tags the inserted text inherits
                var tags = [];
                if (i > 0) {
                    tags = this._characters[i - 1].getTags().slice(0);
                }
                
                // Insert the text
                for (var j = 0; j < text.length; j++) {
                    var c = IXMO.Parser.escapeEntity(text.charAt(j));
                    var character = new IXMO.Character(c, tags);
                    characters.push(character); 
                }
            }
            if (i < this._characters.length) {
                characters.push(this._characters[i].copy());
            }
        }
        this._characters = characters;
    },
   
    /**
     * Append text to the soup.
     * @param {String} text The text to append.
     * @see #insert
     * @see #prepend
     */
    append: function (text) {
        this.insert(this._characters.length, text);
    },
    
    /**
     * Prepend text to the soup.
     * @param {String} text The text to prepend.
     * @see #append
     * @see #insert
     */
    prepend: function (text) {
        this.insert(0, text);
    },

    /**
     * Erase a selection of characters from the soup.
     * @param {Array} select The selection of characters to erase from the 
     * soup.
     */
    erase: function (select) {
        select = this._parseSelect(select);
        this._characters.splice(select[0], select[1] - select[0]);
    },
    
    /**
     * Trim whitespace characters from either end of the soup.
     */
    trim: function () {
        if (this._characters.length === 0) {
            return;
        }
        while (this._characters[0] && this._characters[0].isWhitespace()) {
            this._characters.shift();
        }
        while (this._characters[this._characters.length - 1] && 
            this._characters[this._characters.length - 1].isWhitespace()) {
            
            this._characters.pop();
        }
    },
      
    /**
     * Return a selection of characters from the soup as a string.
     * @param {Array} select The selection of characters to return from the
     * soup.
     * @return The selected characters as a string.
     * @type String
     */
    rawCopy: function (select) {
        select = this._parseSelect(select);
        var rawString = '';
        for (var i = select[0]; i < select[1]; i++) {
            rawString += IXMO.Parser.unescapeEntity(this._characters[i]);
        }
        return rawString;
    },
    
    /**
     * Return an upper case version of the soup.
     * @return An upper case version of the soup.
     * @type IXMO.Soup
     * @see #toLowerCase
     * @see #capitalize
     */
    toUpperCase: function () {
        var soupCopy = this.copy();
        for (var i = 0; i < soupCopy._characters.length; i++) {
            soupCopy._characters[i] = soupCopy._characters[i].toUpperCase();
        }
        return soupCopy;
    },
    
    /**
     * Return a lower case version of the soup.
     * @return A lower case version of the soup.
     * @type IXMO.Soup
     * @see #capitalize
     * @see #toUpperCase
     */
    toLowerCase: function () {
        var soupCopy = this.copy();
        for (var i = 0; i < soupCopy._characters.length; i++) {
            soupCopy._characters[i] = soupCopy._characters[i].toLowerCase();
        }
        return soupCopy;
    },
    
    /**
     * Return a capitalized version of the soup.
     * @return A capitalized version of the soup.
     * @type IXMO.Soup
     * @see #toLowerCase
     * @see #toUpperCase
     */
    capitalize: function () {
        var soupCopy = this.copy();
        if (soupCopy.getLength() > 0) {
            soupCopy._characters[0] = soupCopy._characters[0].toUpperCase();
        }
        return soupCopy;
    },
    
    /**
     * Reverse the soup.
     */
    reverse: function () {
        this._characters.reverse();
    },
    
    // Tag selection & manipulation
    
    /**
     * Test a selection of characters in the soup for a matching format.
     * @param {Array} select The selection of characters to query within the 
     * Soup.
     * @param {String} tagName The name of the tag to query against.
     * @param {Array|Object} attributes (Optional) An array of 
     * attribute names, or an object containing attribute names/values to 
     * match against.
     * @param {Boolean} partial (Optional) If true the test only needs to 
     * match against one or more of the selection of characters to return a
     * true results (defaults to true).
     * @return Whether the query return a found a match or not.
     * @see #format
     */
    test: function (select, tagName, attributes, partial) {
        select = this._parseSelect(select);
        if (partial === undefined) {
            partial = true;
        }
        
        var i = 0;
        var j = 0;
        var tag = null;
        var character = null;
        if (!attributes) {
            
            // Check tag
            tag = new IXMO.Tag(tagName);
            for (i = select[0]; i < select[1]; i++) {
                if (this._characters[i].hasTags(tag, true)) {
                    if (partial) {
                        return true;
                    }
                } else {
                    if (!partial) {
                        return false;
                    }                
                }
            }
            return !partial;
            
        } else if (attributes instanceof Array) {
            // Check tag and attribute names
            tag = new IXMO.Tag(tagName);
            var looseAttributes = [];
            for (i = 0; i < attributes.length; i++) {
                looseAttributes.push(new IXMO.Attribute(attributes[i]));
            }
            for (i = select[0]; i < select[1]; i++) {
                character = this._characters[i];
                var tags = character.getTags(tagName);
                if (tags.length > 0) {
                    var found = false;
                    for (j = 0; j < tags.length; j++) {
                        if (tags[j].hasAttributes(looseAttributes, true)) {
                            if (partial) {
                                return true;
                            }
                            found = true;
                        }
                    }    
                    if (!found && !partial) {
                        return false;
                    }
                } else {
                    if (!partial) {
                        return false;
                    }               
                }
            }
            if (partial) {
                return false;
            }
            return true;
        
        } else {
            
            // Check tag and attribute names & values
            tag = new IXMO.Tag(tagName, attributes);
            for (i = select[0]; i < select[1]; i++) {
                character = this._characters[i];
                if (character.hasTags(tag)) {
                    if (partial) {
                        return true;
                    }
                } else {
                    if (!partial) {
                        return false;
                    }
                }
            }
            
            if (partial) {
                return false;
            }
            return true;   
        }
    },
    
    /**
     * Return a match value of;
     * <ul>
     * <li><code>-1</code> - if no Characters (loosely) matched all of the 
     * specified tags,</li>
     * <li><code>&nbsp;0</code> - if some Characters (loosely) matched all of
     * the specified tags,</li>
     * <li><code>&nbsp;1</code> - if all Characters (loosely) matched all of 
     * the specified tags.</li> 
     * </ul>
     * @param {IXMO.Tag|Array} tags A tag or list of tags to match.
     * @param {Array} select The selection of characters to match the tags 
     * against within the soup.
     * @param {Boolean} loose (Optional) If true then only the tag name needs
     * to match, the tags attributes are not considered in the comparison.
     * @return The result of the match (defaults to false).
     * @type Number
     * @see #getTags
     */
    hasTags: function (select, tags, loose) {
        select = this._parseSelect(select);
        
        var matchCount = 0;
        for (var i = select[0]; i < select[1]; i++) {
            if (this._characters[i].hasTags(tags, loose)) {
                matchCount++;
            }
        }
        
        switch (matchCount) {
        case 0:
            return -1;
        case select[1] - select[0]:
            return 1;
        default:
            return 0;
        }
    },
   
    /**
     * Return a list of tags applied to the specified selection of characters
     * in the soup.
     * @param {Array} select The selection of characters to select tags from 
     * in the Soup
     * @param {String} tagName (Optional) The tagName of the tag type to
     * return.
     * @return A list of tags applied to this selection.
     * @type Array
     * @see #hasTags
     */
    getTags: function (select, tagName) {
        select = this._parseSelect(select);
        
        var tagIndex = {};
        var tags = [];
        for (var i = select[0]; i < select[1]; i++) {
            var characterTags = this._characters[i].getTags(tagName);
            for (var j = 0; j < characterTags.length; j++) {
                var tag = characterTags[j];
                if (!tagIndex[tag.start()]) {
                    tagIndex[tag.start()] = true;
                    tags.push(tag);
                }
            }
        }
        
        return tags;
    },
    
    /**
     * Format a selection of characters in the soup.
     * @param {Array} select The selection of characters to format within the 
     * soup.
     * @param {String} tagName The name of the tag to apply.
     * @param {Array|Object} attributes (Optional) An array of 
     * {@link IXMO.Attribute}s for the tag, or an object that can be 
     * converted to a list of attributes.
     * @param {Boolean} escapeAttributes If true the value of the attributes
     * will be automatically escaped for use in HTML (defaults to false). 
     * Only applicable if attributes is an object.
     * @see #unformat
     */
    format: function (select, tagName, attributes, escapeAttributes) {
        select = this._parseSelect(select);
        
        // Shrink the selection so that there is whitespace at the start or
        // end.
        
        // Start
        var i = 0;
        while (this._characters[i + select[0]].toString() === ' ') {
            select[0]++;
            i++;
            if (i === this._characters.length) {
                break;
            }
        }
        
        // End
        i = 1;
        while (this._characters[select[1] - i].toString() === ' ') {
            select[1]--;
            i++;
            if (i < 0) {
                break;
            }
        }
        
        if (escapeAttributes && !(attributes instanceof Array)) {
            for (var attributeName in attributes) {
                if (attributes.hasOwnProperty(attributeName)) {
                    attributes[attributeName] = IXMO.Parser.escapeEntity(
                        attributes[attributeName]);
                }
            }
        }        
        this.applyTags(select, new IXMO.Tag(tagName, attributes));
    },
        
    /**
     * Unformat a selection of characters in the soup.
     * @param {Array} select The selection of characters to unformat within 
     * the soup.
     * @param {String} tagName The name of the tag to remove.
     * @param {Array|Object} attributes (Optional) An array of 
     * {@link IXMO.Attribute}s for the tag, or an object that can be 
     * converted to a list of attributes. Specify an empty array or object
     * if you only want to remove tags with no attributes.
     * @param {Boolean} escapeAttributes If true the value of the attributes
     * will be automatically escaped for use in HTML (defaults to false). 
     * Only applicable if attributes is an object.
     * @see #format
     */
    unformat: function (select, tagName, attributes, escapeAttributes) {
        select = this._parseSelect(select);
        if (escapeAttributes && !(attributes instanceof Array)) {
            for (var attributeName in attributes) {
                if (attributes.hasOwnProperty(attributeName)) {
                    attributes[attributeName] = IXMO.Parser.escapeEntity(
                        attributes[attributeName]);
                }
            }
        }        

        if (attributes === undefined || attributes === null) {
            this.removeAllTags(select, tagName);
        } else {
            this.removeTags(select, new IXMO.Tag(tagName, attributes));
        }
    }, 
        
    /**
     * Apply one or more tags to a selection within the soup.
     * @param {Array} tags A tag or list of tags to apply.
     * @param {Array} select The selection of characters to apply the tags to 
     * within the soup.
     */
    applyTags: function (select, tags) {
        if (!(tags instanceof Array)) {
            tags = [tags];
        }
        
        select = this._parseSelect(select);
            
        for (var i = select[0]; i < select[1]; i++) {
            this._characters[i].applyTags(tags);
        }
    },    
  

    /**
     * Remove one or more tags from a selection of characters in the soup.
     * @param {Array} select The selection of characters to remove the tags
     * from.
     * @param {Array} tags A tag or list of tags to remove.
     * @see #removeAllTags
     */
    removeTags: function (select, tags) {
        if (!(tags instanceof Array)) {
            tags = [tags];
        }
        
        select = this._parseSelect(select);
        
        for (var i = select[0]; i < select[1]; i++) {
            this._characters[i].removeTags(tags);
        }
    },    
    
    /**
     * Remove all tags from a selection of characters in the soup.
     * @param {Array} select The selection of characters to remove all tags 
     * from.
     * @param {String} tagName (Optional) The name of the tag type to 
     * remove.
     * @see #removeTags
     */
    removeAllTags: function (select, tagName) {
        select = this._parseSelect(select);
        for (var i = select[0]; i < select[1]; i++) {
            this._characters[i].removeAllTags(tagName);
        }
    },
    
    /**
     * Convert the soup to a string.
     * @param {Boolean} optimise If true the output is optimised before being 
     * rendered (defaults to true).
     * @return The soup as a string of inline XHTML.
     * @type String
     */
    toString: function (optimise) {
        if (optimise || optimise === undefined) {
            this._optimise();
        }
        
        var tagStack = [];
        var compliedTagStack = [];
        var toString = '';
        
        var i = 0;
        for (i = 0; i < this._characters.length; i++) {
            var character = this._characters[i];
            
            // Do we need to close any tags
            var j = 0;
            var closingTags = [];
            for (j = tagStack.length - 1; j >= 0; j--) {
                var tag = tagStack[j];
                closingTags.push(tag);
                if (!character.hasTags(tag)) {
                    for (var k = 0; k < closingTags.length; k++) {
                        toString += closingTags[k].end();
                        tagStack.pop();
                        compliedTagStack.pop();                
                    }
                    closingTags = [];
                }
            }    
            
            // Do we need to open any tags
            var openingTags = character.getTags();
            for (j = 0; j < openingTags.length; j++) {
                var compiledTag = openingTags[j].start();
                if (compliedTagStack.indexOf(compiledTag) === -1) {
                    toString += compiledTag;
                    if (!openingTags[j]._shortTag) {
                        tagStack.push(openingTags[j]);
                        compliedTagStack.push(compiledTag);
                    }                
                }
            }
            
            toString += character.toString();
        }
        
        // Close all remaining tags
        for (i = tagStack.length - 1; i >= 0; i--) {
            toString += tagStack[i].end();
        }
        
        return toString;
    },
    
    /** @private */
    _optimise: function () {        
        // Reset run lengths
        var i = 0;
        for (i = 0; i < this._characters.length; i++) {
            this._characters[i]._resetRunLengths();
        }
        
        var tagStack = [];
        var compliedTagStack = [];
        var tag = null;
        var character = null;
        var prevCharacter = null;
        for (i = this._characters.length; i > 0; i--) {
            character = this._characters[i - 1];
            
            // Do we need to close any tags
            var j = 0;
            var closingTags = [];
            for (j = tagStack.length - 1; j >= 0; j--) {
                tag = tagStack[j];
                closingTags.push(tag);
                if (!character.hasTags(tag)) {
                    for (var k = 0; k < closingTags.length; k++) {
                        tagStack.pop();
                        compliedTagStack.pop();                
                    }
                    closingTags = [];
                }
            }    
            
            // Do we need to open any tags
            var openingTags = character.getTags();
            for (j = 0; j < openingTags.length; j++) {
                var compiledTag = openingTags[j].start();
                if (compliedTagStack.indexOf(compiledTag) === -1) {
                    if (!openingTags[j]._shortTag) {
                        tagStack.push(openingTags[j]);
                        compliedTagStack.push(compiledTag);
                    }                
                }
            }
            
            // Calculate the run length of each tag
            for (j = 0; j < tagStack.length; j++) {
                tag = tagStack[j];
                if (prevCharacter) {
                    character._setRunLength(tag, 
                        prevCharacter._getRunLength(tag) + 1);
                } else {
                    character._setRunLength(tag, 1);
                }
            }
            prevCharacter = character;           
        }
        
        // Re-order all the tags (optimized order)
        for (i = 0; i < this._characters.length; i++) {
            character = this._characters[i]._orderTags();
        }
    },
    
    /*
    Tip: When specifying a selection, if you specify an empty array [] all 
    the characters in the soup will be selected.
    */
    
    /** @private */
    _parseSelect: function (select) {
        if (!select) {
            select = [0, this.getLength()];
        }
        
        if (select[0] === undefined || select[1] === undefined) {
            throw new Error('Invalid select type: ' + select);
        }
        
        if (!this.inRange(select[0]) || !this.inRange(select[1])) {
            throw new Error('Invalid selection, outside of character range.');
        }
        
        return select;
    }           
};

/*
Tip: The Tag, Attribute, and Character classes are not meant typically to be 
used directly, instead the necessary functionality should be found in the 
Parser and Soup classes.
*/

/**
 * Create a new inline XHTML tag.
 * @class A aemantic structure tag {@link IXMO.Character}s within a
 * {@link IXMO.Soup}.
 * @constructor
 * @param {String} name The tags name.
 * @param {Array|Object} attributes (Optional) An array of 
 * {@link IXMO.Attribute}s for the tag, or an object that can be converted to
 * a list of Attributes.
 * @param {Boolean} shortTag (Optional) If true then the tag is output short 
 * tagged (defaults to false).
 * @param {Boolean} validateAttributes If true then the attributes will be 
 * validated when initially set (defaults to true).
 */ 
IXMO.Tag = function (name, attributes, shortTag, validateAttributes) {
    /*
    To improve render and look up performance the result of calls to the 
    'start' method are cached until the tag is modified.
    */
    
    /** @private */
    this._modified = true;
    
    /** @private */
    this._start = '';

    /*
    Partial and fully pre-rendered versions of the attributes applied to the
    tag are stored to optimize 'hasAttributes' lookups.
    */
    
    /** @private */
    this._looseAttributes = [];
    
    /** @private */
    this._exactAttributes = [];    
    
    /** @private */
    this._tagDesc = IXMO_INLINE_TAGS[name.toLowerCase()];
    
    // Validate the tag
    if (!this._tagDesc) {
        throw new Error('Unsupported tag: <' + name + '>');
    }
    
    /** @private */
    this._name = this._tagDesc.preferredName;
    
    /** @private */
    this._initialName = name.toLowerCase();
    
    /** @private */
    this._attributes = [];
    
    var i = 0;
    if (attributes instanceof Array) {
        for (i = 0; i < attributes.length; i++) {
            this.setAttribute(attributes[i]._name, attributes[i]._value, 
                false, validateAttributes);
        }
    } else if (attributes instanceof Object) {
        for (var attributeName in attributes) {
            if (attributes.hasOwnProperty(attributeName)) {
                this.setAttribute(attributeName, 
                    attributes[attributeName], 
                    false, 
                    validateAttributes);
            }
        }
    }
    
    if (validateAttributes) {
        // Validate all required attributes have been specified
        for (i = 0; i < this._tagDesc.requiredAttributes.length; i++) {
            if (!this.hasAttributes(this._tagDesc.requiredAttributes[i])) {
                throw new Error('Tag: <' + name +
                    '>, required the attribute: ' +
                    this._tagDesc.requiredAttributes[i]);
            }
        }
    }
    
    if (shortTag && !this._tagDesc.canShortTag) {
        throw new Error('Cannot short tag: <' + name + '>');
    }
    
    /** @private */
    this._shortTag = shortTag || false;
};

IXMO.Tag.prototype = {
    
    /**
     * Return true if the specified attribute(s) are applied to the tag.
     * @param {IXMO.Tag|Array} attributes An attribute or list of attributes
     * to match.
     * @param {Boolean} loose (Optional) If true then only the attribute name
     * needs to match, the attributes' values are not considered in the 
     * comparison (defaults to false).
     * @return The result of the match.
     * @type Boolean
     */
    hasAttributes: function (attributes, loose) {
        if (!(attributes instanceof Array)) {
            attributes = [attributes];
        }
        
        for (var i = 0; i < attributes.length; i++) {
            if (loose) {
                return this._looseAttributes.indexOf(
                    attributes[i]._name) !== -1;
            } else {
                return this._exactAttributes.indexOf(
                    attributes[i].toString()) !== -1;
            }
        }
        
        return true;    
    },
    
    /**
     * Set an attributes value for the tag.
     * @param {String} name The name of the attribute to set.
     * @param {String} value (Optional) The value of the attribute to set. If 
     * no value is specified ('') the attribute will be removed from the tag.
     * @param {Boolean} escapeValue If true the value will be automatically
     * escaped for use in XHTML (defaults to false).
     * @param {Boolean} validate If true the attribute being set will be 
     * validated for the tag first.
     * @see #getAttribute
     */
    setAttribute: function (name, value, escapeValue, validate) {
        
        if (validate === undefined || validate === true) {
            // Validate the attribute before setting it
            if (this._tagDesc.attributes.indexOf(name) === -1) {
                throw new Error('Attribute ' + name + 
                    ' is not supported by ' + this._name + ' tag.');
            }
        }
        
        var attribute = this.getAttribute(name);
        
        if (escapeValue) {
            value = IXMO.Parser.escapeEntity(value);
        }
        
        var i = 0;
        if (value === null || value === '') {
            // Remove
            if (attribute) {
                i = this._attributes.indexOf(name.toLowerCase());
                this._attributes.splice(i, 1);
                this._looseAttributes.splice(i, 1);
                this._exactAttributes.splice(i, 1);
            }
        } else {
            // Set attribute
            if (attribute) {
                // Update
                attribute._value = value;
                i = this._attributes.indexOf(name.toLowerCase());
                this._exactAttributes[i] = attribute.toString();
            } else {
                // Add
                attribute = new IXMO.Attribute(name, value);
                this._attributes.push(attribute);
                this._looseAttributes.push(attribute._name);
                this._exactAttributes.push(attribute.toString());
            }
        }
        
        this._modified = true;
    },
    
    /**
     * Get an attributes value for the tag.
     * @param {String} name The name of the attribute to get the value of.
     * @return The value of the attribute.
     * @type String
     * @see #setAttribute
     */
    getAttribute: function (name) {
        var i = this._looseAttributes.indexOf(name.toLowerCase());
        if (i !== -1) {
            return this._attributes[i]._value;
        }
    },        
        
    /**
     * Return a string to start the tag.
     * @return A string to start the tag.
     * @type String
     * @see #end
     */
    start: function () {
        if (this._modified) {
            
            // Refresh the cache
            var attributes = this._attributes.slice();
            attributes.sort(IXMO.Tag._attributeSort);
            
            this._start = IXMO_PARENTHESIS[0] + this._name;
            for (var i = 0; i < attributes.length; i++) {
                this._start += ' ' + attributes[i].toString();
            }
            if (this._shortTag) {
                this._start += '/';
            }
            this._start += IXMO_PARENTHESIS[1];
            
            this._modified = false;
        }
        
        return this._start;
    },
    
    /**
     * Return a string to end the tag.
     * @return A string to end the tag.
     * @type String
     * @see #start
     */
    end: function () {
        if (this._shortTag) {
            return '';
        }
        return IXMO_PARENTHESIS[0] + '/' + this._name + IXMO_PARENTHESIS[1];
    },
    
    /**
     * Return a copy of the tag.
     * @return A copy of the tag.
     * @type {IXMO.Tag}
     */
    copy: function () {
        var attributes = [];
        for (var i = 0; i < this._attributes.length; i++) {
            attributes.push(this._attributes[i].copy());
        }
        return new IXMO.Tag(this._name, attributes, this.shortTag);
    }
};

// Class methods

/** @private */
IXMO.Tag._attributeSort = function (a, b) {
    if (a._name > b._name) {
        return 1;
    } else if (a._name < b._name) {
        return -1;
    }
    return 0;
};


/**
 * Create a new tag attribute. 
 * @class An attribute in a {@link IXMO.Tag} instance.
 * @constructor
 * @param {String} name The Attributes name.
 * @param {String} value The Attributes value.
 */ 
IXMO.Attribute = function (name, value) {
    
    /** @private */
    this._name = name.toLowerCase();
    
    /** @private */
    this._value = value || '';
};

IXMO.Attribute.prototype = {
    
    /**
     * Return a copy of the attribute.
     * @return A copy of the attribute.
     * @type {IXMO.Attribute}
     */
    copy: function () {
        return new IXMO.Attribute(this._name, this._value);
    },
    
    /**
     * Return the attribute as a string.
     * @return The attribute as a string.
     * @type String
     */
    toString: function () {
        return this._name + '=' + IXMO_DELIMITER + this._value + 
            IXMO_DELIMITER;
    }
};

/**
 * Create a new character. 
 * @class  A character, entity, or short tagged node (e.g. img, br, etc.) in
 * a {@link IXMO.Soup} instance.
 * @constructor
 * @param {String} character A string representation of the character.
 * @param {Array} tags (Optional) An array of {@link IXMO.Tag}s that apply
 * to the character.
 */ 
IXMO.Character = function (character, tags) {
    
    /** @private */
    this._character = character;
    
    /** @private */
    this._tags = [];
    
    /*
    Partial and fully pre-rendered versions of the tags applied to the
    character are stored to optimize 'hasTag' lookups.
    */
    
    /** @private */
    this._looseTags = [];
    
    /** @private */
    this._exactTags = [];
    
    /*
    A simplistic form of tag optimisation is currently employed to determine
    the point a which a tag is closed. It is based on the tags 'run length'
    which is the number of characters that the tag has been applied at the 
    this character.
    The optimisation favours long run lengths for tags and closes short run
    lengths over longer run lengths.
    */
    
    /** @private */
    this._runLengths = []; 
    
    // Apply tags to the character
    if (tags) {
        this.applyTags(tags);
    }
};

IXMO.Character.prototype = {

    /**
     * Return true if the specified tag(s) are applied to the character.
     * @param {IXMO.Tag|Array} tags A tag or list of tags to match.
     * @param {Boolean} loose (Optional) If true then only the tag name needs
     * to match, the tags attributes are not considered in the comparison.
     * @return The result of the match (defaults to false).
     * @type Boolean
     * @see #isTag
     */
    hasTags: function (tags, loose) {
        if (!(tags instanceof Array)) {
            tags = [tags];
        }
        
        for (var i = 0; i < tags.length; i++) {
            if (loose) {
                if (this._looseTags.indexOf(tags[i]._name) === -1) {
                    return false;
                }
            } else {
                if (this._exactTags.indexOf(tags[i].start()) === -1) {
                    return false;
                }
            }
        }    
        
        return true;    
    },
    
    /**
     * Return true if the character is a tag, optionally of the specified 
     * type.
     * @param tagName (Optional) The name of tag type to match.
     * @return If the character is a tag or a tag of the specified type.
     * @type Boolean  
     * @see #hasTags
     */
    isTag: function (tagName) {
        for (var i = 0; i < this._tags.length; i++) {
            var tag = this._tags[i];
            if (tag._shortTag) {
                if (tagName !== undefined) {
                    return tag._name === tagName;
                }
                return true;
            }
        }    
        return false;    
    },     
    
    /**
     * Return a list of tags applied to the character
     * @param {String} tagName (Optional) The name of tag type to return.
     * @return List of tags applied to the character.
     * @type Array
     * @see #hasTags
     */
    getTags: function (tagName) {
        if (tagName === undefined) {
            return this._tags;    
        }
        
        // Filter by name
        var tags = [];
        for (var i = 0; i < this._tags.length; i++) {
            if (this._tags[i]._name === tagName) {
                tags.push(this._tags[i]);
            }
        }
        return tags;
    },
    
    /**
     * Return true if the character represents a whitespace.
     * @return Whether the character is a whitespace.
     * @type Boolean
     */
    isWhitespace: function () {
        var c = this._character;
        if ([' ', '\n', '&nbsp;'].indexOf(c) !== -1) {
            return true;
        }
        if (this.isTag('br') || this.isTag('wbr')) {
            return true;
        }
    },
    
    /**
     * Apply one or more tags to a character.
     * @param {Array} tags A tag or list of tags to apply.
     */
    applyTags: function (tags) {
        if (!(tags instanceof Array)) {
            tags = [tags];
        }
        
        for (var i = 0; i < tags.length; i++) {
            var tag = tags[i];
            if (!this.hasTags(tag)) {
                this._tags.push(tag);
                this._looseTags.push(tag._name);
                this._exactTags.push(tag.start());
            }
        }    
    },
    
    /**
     * Remove one or more tags from the character.
     * @param {Array} tags A tag or list of tags to remove.
     * @see #removeAllTags
     */
    removeTags: function (tags) {
        if (!(tags instanceof Array)) {
            tags = [tags];
        }
        for (var i = 0; i < tags.length; i++) {
            var index = this._exactTags.indexOf(tags[i].start());
            if (index !== -1) {
                this._tags.splice(index, 1);
                this._looseTags.splice(index, 1);
                this._exactTags.splice(index, 1);
            }            
        }    
    },
    
    /**
     * Remove all tags (optionally of the specified type) from the character.
     * @param {String} tagName (Optional) The name of the tag type to 
     * remove.
     * @see #removeTags
     */
    removeAllTags: function (tagName) {
        if (tagName) {
            // Remove all tags of type > tagName
            var tags = [];
            for (var i = 0; i < this._tags.length; i++) {
                if (this._tags[i]._name === tagName) {
                    tags.push(this._tags[i]);
                }
            }
            this.removeTags(tags);
        } else {
            // Remove all tags
            this._tags = [];
            this._looseTags = [];
            this._exactTags = [];
        }
    },    

    /**
     * Return a lower case version of the character.
     * @return A lower case version of the character.
     * @type IXMO.Character
     * @see #toUpperCase
     */
    toLowerCase: function () {
        return new IXMO.Character(this._character.toLowerCase(),
            this._tags.slice());
    },
    
    /**
     * Return an upper case version of the character.
     * @return An upper case version of the character.
     * @type IXMO.Character
     * @see #toLowerCase
     */
    toUpperCase: function () {
        return new IXMO.Character(this._character.toUpperCase(),
            this._tags.slice());
    },
    
    /**
     * Return a copy of the character.
     * @return A copy of the character.
     * @type {IXMO.Character}
     */
    copy: function () {
        return new IXMO.Character(this._character, this._tags.slice());
    },
    
    /**
     * Return the character as a string.
     * @return The character as a string.
     * @type String
     */
    toString: function () {
        return this._character;
    },
    
    /*
    The following methods all concern the optimisation of tags applied to the
    character using run length.
    */
    
    /** @private */
    _resetRunLengths: function () {
        this._runLengths = [];
    },
    
    /** @private */
    _getRunLength: function (tag) {
        for (var i = 0; i < this._runLengths.length; i++) {
            if (this._runLengths[i].tag === tag) {
                return this._runLengths[i].runLength;
            }
        }
        return 0; 
    },
    
    /** @private */
    _setRunLength: function (tag, runLength) {
        for (var i = 0; i < this._runLengths.length; i++) {
            if (this._runLengths[i].tag === tag) {
                this._runLengths[i].runLength = runLength;
                return;
            }
        }
        this._runLengths.push({'tag': tag, 'runLength': runLength});
    },
    
    /** @private */
    _getPriorityTag: function () {
        if (this._runLengths.length > 0) {
            return this._runLengths[this._runLengths.length - 1].tag;
        }
        return null;
    },
    
    /** @private */
    _orderTags: function () {
        if (this._runLengths.length > 1) {
            this._runLengths.sort(IXMO.Character._runLengthSort);
            this.removeAllTags();
            for (var i = this._runLengths.length; i > 0; i--) {
                this.applyTags(this._runLengths[i - 1].tag);
            }
        }
    }    
};

// Class methods

/** @private */
IXMO.Character._runLengthSort = function (a, b) {
    if (a.runLength > b.runLength) {
        return 1;
    } else if (a.runLength < b.runLength) {
        return -1;
    }
    return 0;
};