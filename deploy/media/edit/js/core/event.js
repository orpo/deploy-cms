/**
 * @fileoverview    Class event handling.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 */

'use strict';

/**
 * The EVENT namespace.
 * @final
 * @type Namespace
 */  
var EVENT = {};

// Constants
var EVENT_PHASE_ON = 1;
var EVENT_PHASE_DEFAULT = 2;
var EVENT_PHASE_AFTER = 3;

var EVENT_DEFAULT_EVENT_DEF = {
    canStopPropagation: true,
    canStopDefault: true
};

/**
 * Modify the provided class prototype so that the public interface calls a
 * a private method that contains the provided class method making it easy
 * to call the method internally without triggering events. This makes
 * class mixins easier.
 * @param {Class} _class The class to attach the method to.
 * @param {Function} _method The method to wrap (Important! make sure the 
 * function has a name defined for it).
 * @param {String} triggerName The event trigger name.
 */
EVENT.classMethod = function (_class, _method, triggerName) {
    // Extract the class name
    var methodName = _method.name;
    if (methodName === undefined) {
        // For browsers that don's support the .name convention do it the 
        // hard way.
        var nameMatch = _method.toString().match(
            /function\s*([\w\$]*)\s*\(/); 
        if (nameMatch !== null) {
            methodName = nameMatch[1];
        }
    }
    // Generate a private but easy to mix class method
    var _prototype = _class.prototype;
    _prototype['_' + methodName] = _method;
    // Wrap the private method in a public interface method
    var code = '_prototype["' + methodName + '"] = function() {' +
        'var _arguments = [];' +
        'for (var i = 0; i < arguments.length; i++ ) {' +
            '_arguments.push(arguments[i]);' + 
        '};' +
        'this.trigger(' +
            '"' + triggerName + '", this["_' + methodName + '"],' +
            '{data: {"_arguments": _arguments}});' +
        '};';
    eval(code);
};

/**
 * This method returns a safe set of arguments for use with classMethod.
 * @param {Object} args The arguments object sent to the calling function.
 * @return A safe set of arguments to use when calling getArg.
 * @string
 */
EVENT.getArgs = function (args) {
    if (args !== undefined) {
        if (args.length === 1 && args[0].__className__ === 'EVENT.Event' && args[0].data._arguments !== undefined) {
            return args[0].data._arguments.slice(0);
        }
        return Array.prototype.slice.call(args);
    }
};

/**
 * This method resolves the discrepency in arguments when a classMethod is
 * called via it's public and private interfaces. 
 * @param {Object} args The arguments object sent to the calling function.
 * @param {Number} index The index of the argument to return.
 * @return The argument (either from the list or from the event data).
 * @string
 */
EVENT.getArg = function (args, index) {
    if (args !== undefined) {
        if (args.length === 1 && args[0].__className__ === 'EVENT.Event' && args[0].data._arguments !== undefined) {
            return args[0].data._arguments[index];
        }
        if (args.length > index) {
            return args[index];
        }
    }
};

/** @private */
EVENT._parsePhase = function (phase) {
    if (typeof phase === 'string') {
        if (phase === 'on') {
            phase = EVENT_PHASE_ON;
        } else if (phase === 'after') {
            phase = EVENT_PHASE_AFTER;
        } else {
            throw new Error('Unknown phase: ' + phase);
        }
    }
    return phase;
};

/**
 * Create a new event class.
 * @class An event class.
 * @constructor
 */    
EVENT.EventClass = function () {
    /** @private */
    this._eventDefinitions = {};
    
    /** @private */
    this._eventHandlers = [];
};

EVENT.EventClass.prototype = {
    __className__: 'EVENT.EventClass',
    
    /**
     * Return a list of event handlers that match the specified criteria.
     * @param {Object} options The criteria to match against.
     * @param {Number|String} phase (Option) The phase to match against.
     * @param {String} eventType (Option) The event type to match against.
     * @param {Array} namespace (Option) The namespace to match against.
     */    
    getHandlers: function (options) {
        options.phase = EVENT._parsePhase(options.phase);
        var eventHandlers = [];
        for (var i = 0; i < this._eventHandlers.length; i++) {
            if (this._eventHandlers[i].eventRule.match(options)) {
                eventHandlers.push(this._eventHandlers[i]);
            }
        }
        
        if (options.namespace === undefined) {
            // Build the method name
            var phase = 'after';
            if (options.phase === EVENT_PHASE_ON) {
                phase = 'on';
            }
            
            var eventType = options.eventType;
            var methodName = phase + eventType.charAt(0).toUpperCase() + eventType.slice(1);
            
            if (this[methodName]) {
                
                // Build a handler for the event
                eventHandlers.push(new EVENT.EventHandler(
                    EVENT.EventRule(options.phase, options.eventType), 
                    this[methodName]));
            }
        }
        return eventHandlers;
    },
    
    /**
     * Define the default behaviour for an event type.
     * @param {String} eventType The event type to define the behaviour for.
     * @param {Object} options The default options for the event type.
     * @param {Object} scope (Option) The scope within which any event 
     * handlers action will be triggered (defaults to this).
     * @param {Boolean} canStopPropagation (Option) If false then all 
     * handlers are triggered even if {@link EVENT.Event#stopPropagation} or
     * {@link EVENT.Event#stopImmediatePropagation} are called (defaults to 
     * true).
     * @param {Boolean} canStopDefault (Option) If false the default action 
     * for the triggered event is called even if 
     * {@link EVENT.Event#stopDefault} is called (defaults to true).
     */
    defineEventType: function (eventType, options) {
        options = UTILS.optionsArg(options, EVENT_DEFAULT_EVENT_DEF);
        this._eventDefinitions[eventType] = options;
    },
    
    /**
     * Bind an action to an event.
     * @param {String|EVENT.EventRule} eventRule The event rule that triggers
     * the action.
     * @param {Function} action The action that will be triggered.
     * @see #unbind
     * @see #unbindAll
     */
    bind: function (eventRule, action, data) {
        action.data = data;
        
        if (!(eventRule instanceof EVENT.EventRule)) {
            eventRule = EVENT.EventRule._parseRule(eventRule);
        }
        
        if (!(eventRule instanceof Array)) {
            eventRule = [eventRule];
        }
        
        for (var i = 0; i < eventRule.length; i++) {
            this._eventHandlers.push(new EVENT.EventHandler(eventRule[i], 
                action, data));
        }
    },
    
    /**
     * Unbind all event handlers for the specified event rule.
     * @param {String|EVENT.EventRule} eventRule The event rule to remove
     * handlers for.
     * @see #bind
     * @see #unbindAll
     */
    unbind: function (eventRule) {
        if (!(eventRule instanceof EVENT.EventRule)) {
            eventRule = EVENT.EventRule._parseRule(eventRule);
        }
        
        if (!(eventRule instanceof Array)) {
            eventRule = [eventRule];
        }
        
        for (var i = 0; i < eventRule.length; i++) {
            var handlers = this.getHandlers({phase: eventRule[i].phase,
                eventType: eventRule[i].eventType,
                namespace: eventRule[i].namespace});
            for (var j = 0; j < handlers.length; j++) {
                var index = this._eventHandlers.indexOf(handlers[j]);
                this._eventHandlers.splice(index, 1);
            }                    
        }
    },
    
    /**
     * Unbind all event handlers, optionally for the specifed namespace.
     * @param {String|Array} namespace (Optional) The namespace to remove 
     * events from.
     * @see #bind
     * @see #unbind
     */
    unbindAll: function (namespace) {
        if (namespace) {
            var handlers = this.getHandlers({namespace: namespace});
            for (var i = 0; i < handlers.length; i++) {
                var index = this._eventHandlers.indexOf(handlers[i]);
                this._eventHandlers.splice(index, 1);
            }
        } else {
            this._eventHandlers = [];
        }
    },

    /**
     * Trigger an event.
     * @param {String} eventType The type of event to trigger.
     * @param {Function} defaultAction (Optional) The 'default' function to 
     * perform.
     * @param {Object} data (Optional) A value (of any type) associated with 
     * the event (event.data).
     * @param {Object} scope (Optional) The scope to execute the function
     * within (defaults to this).
     * @param {Object} target (Optional) The target of the triggered event 
     * (defaults to this).
     */
    trigger: function (eventType, defaultAction, options) {
        options = UTILS.optionsArg(options, {target: this});
        
        // Define the event
        var eventDefinition;
        if (this._eventDefinitions[eventType] === undefined) {
            eventDefinition = EVENT_DEFAULT_EVENT_DEF;
        } else {
            eventDefinition = this._eventDefinitions[eventType];
        }
        eventDefinition.data = options.data;
        eventDefinition.target = options.target || this;
        
        // Create the event
        var event = new EVENT.Event(eventType, eventDefinition);
        
        // Call event handlers
        var scope = options.scope || eventDefinition.scope || this;
        var i = 0;
        
        // 'on'
        event.phase = EVENT_PHASE_ON;
        var onHandlers = this.getHandlers({phase: EVENT_PHASE_ON, 
            'eventType': eventType});
        
		for (i = 0; i < onHandlers.length; i++) {
            if (event._onStopped) {
                break;
            }
            onHandlers[i].action.call(scope, event, onHandlers[i].data);
        }
        
        // 'default'
        event.phase = EVENT_PHASE_DEFAULT;
        if (!event._defaultStopped) {
            defaultAction.call(scope, event);
        }
        
        // 'after'
        event.phase = EVENT_PHASE_AFTER;
        var afterHandlers = this.getHandlers({phase: EVENT_PHASE_AFTER, 
            'eventType': eventType});
        for (i = 0; i < afterHandlers.length; i++) {
            if (event._afterStopped) {
                break;
            }
            afterHandlers[i].action.call(scope, event, afterHandlers[i].data);
        }
    }
};

/**
 * Create a new event handler.
 * @class An event handler. This class is not intended to be used directly by 
 * external libraries.
 * @constructor
 * @param {EVENT.EventRule} eventRule The event rule the handler triggers
 * against.
 * @param {Function} action The action that will be called by the handler 
 * when triggered.
 */    
EVENT.EventHandler = function (eventRule, action, data) {
    
    /**
     * The event rule the handler triggers against.
     * @type EVENT.EventRule
     */
    this.eventRule = eventRule;
    
    /**
     * The action that executes when the event handler is triggered.
     * @type Function
     */
    this.action = action;
    
    /**
     * Data specific to this binding of the event handler.
     * @type Object
     */
    this.data = data;
};


/**
 * Create a new event class.
 * @class An event class. This class is not intended to be used directly by
 * external libraries.
 * @constructor
 * @param {Number} phase The phase the event will be applied at (on or 
 * after).
 * @param {String} eventType The event type identifier.
 * @param {Array} namespace (Optional) An array of namespace qualifiers.
 */    
EVENT.EventRule = function (phase, eventType, namespace) {
    __className__: 'EVENT.EventRule',
    
    /**
     * The phase the event is at in the process.
     * @type Number
     */
    this.phase = phase;
    
    /**
     * The event type.
     * @type String
     */
    this.eventType = eventType.toLowerCase();
    
    /**
     * The namespace for the event rule.
     * @type Array
     */
    this.namespace = namespace || [];
    
    for (var i = 0; i < this.namespace.length; i++) {
        this.namespace[i] = this.namespace[i].toLowerCase();
    }
};

EVENT.EventRule.prototype = {

    /**
     * Return true if the event rule matches the specified criteria.
     * @param {Object} options The criteria to match against.
     * @param {Number} phase (Option) The phase to match against.
     * @param {String} eventType (Option) The event type to match against.
     * @param {Array} namespace (Option) The namespace to match against.
     */    
    match: function (options) {
        // Phase match
        if (options.phase !== this.phase) {
            return false;
        }
            
        // Event type match
        if (options.eventType.toLowerCase() !== this.eventType) {
            return false;
        }
        
        if (options.namespace) {
            if (!(options.namespace instanceof Array)) {
                options.namespace = [options.namespace];
            }
            
            if (options.namespace.length > this.namespace.length) {
                return false;
            }
            
            for (var i = 0; i < options.namespace.length; i++) {
                if (options.namespace[i].toLowerCase() !== 
                    this.namespace[i]) {
                    
                    return false;
                }
            }
        }
                
        return true;
    },
    
    /** @ignore */
    toString: function () {
        var string = '';
        if (this.phase === EVENT_PHASE_ON) {
            string = 'on';
        } else if (this.phase === EVENT_PHASE_AFTER) {
            string = 'after';
        }
        string += this.eventType;
        if (this.namespace.length > 0) {
            string += '.' + this.namespace.join('.');
        }
        return string;
    }
};

// Class methods

/** @private */
EVENT.EventRule._parseRule = function (source) {
    if (source === undefined) {
        return [];
    }
    
    // Event rule format; phaseEventType.namespace1.namespaceN
    var rules = [];
    var ruleStrings = source.toLowerCase().split(' ');
    for (var i = 0; i < ruleStrings.length; i++) {
        var ruleSplit = ruleStrings[i].split('.');
        
        var eventType = ruleSplit[0];
        var namespace = [];
        if (ruleSplit.length > 1) {
            namespace = ruleSplit.slice(1);
        }
        
        // Detemine the phase
        var phase = '';
        if (eventType.indexOf('on') === 0) {
            phase = EVENT_PHASE_ON;
        } else if (eventType.indexOf('after') === 0) {
            phase = EVENT_PHASE_AFTER;
        } else {
            throw new Error('Unknown phase for event rule: ' + source);
        }
        
        // Extract the event
        eventType = eventType.replace(/^(on|after)/, '');
        
        // Create the rule
        rules.push(new EVENT.EventRule(phase, eventType, namespace));
    }
    return rules;
};

/**
 * Create a new event.
 * @class A (dispatched) event. This class is not intended to be created 
 * directly by external libraries though it's methods and attributes may be 
 * called by external libraries. Important! The attributes of an event should
 * be considered read only to external librarues.
 * @constructor
 * @param {String} type The event type.
 * @param {Object} options The options that define the event.
 * @param {Object} data (Option) The data associated with the event.
 * @param {Object} target (Option) The target of the event.
 * @param {Boolean} canStopPropagation (Option) If false then all handlers
 * are triggered even if {@link EVENT.Event#stopPropagation} or 
 * {@link EVENT.Event#stopImmediatePropagation} are called (defaults to 
 * true).
 * @param {Boolean} canStopDefault (Option) If false the default action for
 * the triggered event is called even if  {@link EVENT.Event#stopDefault} is
 * called (defaults to true).
 */    
EVENT.Event = function (type, options) {
    options = UTILS.optionsArg(options, {canStopPropagation: true, 
        canStopDefault: true});
    
    /**
     * The event type.
     * @type String
     */
    this.type = type;
    
    /**
     * Data associated with the event.
     * @type Object
     */
    this.data = options.data;
    
    /**
     * The target of the event.
     * @type Object
     */
    this.target = options.target;
    
    /**
     * The phase the event is at in the process.
     * @type Number
     */
    this.phase = 0;
    
    /** @private */
    this._canStopPropagation = options.canStopPropagation;
    
    /** @private */
    this._canStopDefault = options.canStopDefault;
    
    /** @private */
    this._onStopped = false;
    
    /** @private */
    this._defaultStopped = false;

    /** @private */
    this._afterStopped = false;
};

EVENT.Event.prototype = {
    __className__: 'EVENT.Event',
    
    /**
     * Stop all future subscribed handlers from executing.
     */
    stopPropagation: function () {
        if (this._canStopPropagation) {
            this._onStopped = true;
            this._afterStopped = true;
        }
    },
    
    /**
     * Stop future subscribed handlers in the current phase from executing.
     */
    stopImmediatePropagation: function () {
        if (this._canStopPropagation) {
            if (this.phase === EVENT_PHASE_ON) {
                this._onStopped = true;
            }
            if (this.phase === EVENT_PHASE_AFTER) {
                this._afterStopped = true;
            }
        }
    },
    
    /**
     * Stop the default action (if not already executed) from executing.
     */
    stopDefault: function () {
        if (this._canStopDefault) {
            this._defaultStopped = true;
            this._afterStopped = true;
        }
    },
    
    /**
     * Stop all future subscribed actions and the default action (if not 
     * already executed) from exectuting.
     */
    stop: function () {
        if (this._canStopPropagation && this._canStopDefault) {
            this._onStopped = true;
            this._defaultStopped = true;
            this._afterStopped = true;
        }
    }
};