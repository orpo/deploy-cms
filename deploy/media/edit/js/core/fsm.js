/**
 * @fileoverview    A Finite State Machine (FSM).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

/**
 * The FSM namespace.
 * @final
 * @type Namespace
 */  
var FSM = {};

/**
 * Create a new finite state machine.
 * @class A machine class.
 * @constructor
 * @param {Object} data A value (of any type) sent to any callback function
 * triggered by a state transition.
 */    
FSM.Machine = function (data) {
    /** @private */
    this._stateTransitions = {};
    
    /** @private */
    this._stateTransitionsAny = {};
    
    /** @private */
    this._defaultTransition = null;
    
    /** @private */
    this._initialState = null;
    
    /** @private */
    this._currentState = null;
    
    /**
     * A value of any type sent to any calllback triggered by a state 
     * transition.
     * @type * 
     */
    this.data = data;
};

FSM.Machine.prototype = {
 
    /**
     * Add a transition to the Machine.
     * @param {String} action The action that triggers the transaction.
     * @param {String} state The state under which the action triggers the
     * transaction.
     * @param {String} nextState (Optional) If specified the Machine will 
     * transform from the current state to the next state.
     * @param {Function} callback (Optional) A function called each time the
     * transition occurs.
     * @see #addTransitionAny
     * @see #addTransitions
     * @see #getTransition
     * @see #setDefaultTransition
     */
    addTransition: function (action, state, nextState, 
        callback) {
                
        if (!nextState) {
            nextState = state;
        }
        this._stateTransitions[[action, state]] = [callback, nextState];
    },
    
    /**
     * Add a transition for multiple actions to the Machine.
     * @param {String} actions A list of actions that trigger the 
     * transaction.
     * @param {String} state The state under which the actions trigger the
     * transaction.
     * @param {String} nextState (Optional) If specified the Machine will 
     * transform from the current state to the next state.
     * @param {Function} callback (Optional) A function called each time the
     * transition occurs.
     * @see #addTransition
     * @see #addTransitionAny
     * @see #getTransition
     * @see #setDefaultTransition
     */
    addTransitions: function (actions, state, nextState, 
        callback) {
                
        if (!nextState) {
            nextState = state;
        }
        for (var i = 0; i < actions.length; i++) {
            this.addTransition(actions[i], state, nextState, callback);
        }
    },
    
    /**
     * Add a default transition for a state to the Machine. 
     * @param {String} state The state for which the default transition will 
     * apply.
     * @param {String} nextState (Optional) If specified the Machine will 
     * transform from the current state to the next state.
     * @param {Function} callback (Optional) A function called each time the
     * transition occurs.
     * @see #addTransition
     * @see #addTransitions
     * @see #getTransition
     * @see #setDefaultTransition
     */
    addTransitionAny: function (state, nextState, callback) {
        if (!nextState) {
            nextState = state;
        }
        this._stateTransitionsAny[state] = [callback, nextState];
    },
    
    /**
     * Set the default transition for when no specific transtion can be found.
     * @param {String} state The state to which the Machine will transition.
     * @param {Function} callback (Optional) A function called each time the 
     * transition occurs.
     * @see #addTransition
     * @see #addTransitionAny
     * @see #addTransitions
     * @see #getTransition
     */
    setDefaultTransition: function (state, callback) {
        this._defaultTransition = [callback, state];
    },
    
    /**
     * Get the transition for the specified action and state.
     * @param {String} action The action under which the transition occurs.
     * @param {String} state (Optional) The state under which the transition 
     * occurs.
     * @return The transition that occurs based on the action and state 
     * (callback, nextState).
     * @type Array
     * @see #addTransition
     * @see #addTransitionAny
     * @see #addTransitions
     * @see #setDefaultTransition
     */
    getTransition: function (action, state) {
        if (this._stateTransitions[[action, state]]) {
            return this._stateTransitions[[action, state]];
        } else if (this._stateTransitionsAny[state]) {
            return this._stateTransitionsAny[state];
        } else if (this._defaultTransition) {
            return this._defaultTransition;
        }
        throw new Error('Transition is undefined: (' + 
            action + ', '  + state + ')');
    },

    /**
     * Get the Machines current state.
     * @return The Machines current state.
     * @type String
     */    
    getCurrentState: function () {
        return this._currentState;
    },
    
    /** 
     * Set the initial state of the Machine. This method should always be 
     * called before calling the 
     * {@link #process} method.
     * @param {String} state The state to initially set the Machine as.
     * @see #reset
     */
    setInitialState: function (state) {
        this._initialState = state;
        if (!this._currentState) {
            this.reset();
        }
    },
    
    /** 
     * Reset the Machine to the initial state.
     * @see #setInitialState
     */
    reset: function () {
        this._currentState = this._initialState;
    },
    
    /**
     * Process an action. Before calling process ensure you have set the
     * initial state (see {@link #setInitialState}).
     * @param {String} action The action to process.
     */
    process: function (action) {
        var result = this.getTransition(action, this._currentState);
        if (typeof result[0] === 'function') {
            result[0].call(this, action);
        }
        this._currentState = result[1];
    }
};