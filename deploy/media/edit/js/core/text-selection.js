/**
 * @fileoverview    Cross-browser inline content selection.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 *                  FSM (/js/core/fsm.js) 
 *                  IXMO (/js/core/ixmo.js)
 */

'use strict';

/**
 * The TEXT_SELECTION namespace.
 * @final
 * @type Namespace
 */  
var TEXT_SELECTION = {};

/**
 * Create a new text selection range.
 * @class A text selection range.
 * @constructor
 * @param {Number} start The start index of the range.
 * @param {Number} end The end index of the range. 
 */  
TEXT_SELECTION.Range = function (start, end) {
    
    /** 
     * The start (from 0) of the selection in characters.
     * @type Element
     */
    this[0] = Math.min(start, end);
    
    /** 
     * The end of the selection in characters.
     * @type Element
     */
    this[1] = Math.max(start, end);
};

TEXT_SELECTION.Range.prototype = {

    /**
     * Return the distance between the start and end of the range.
     * @return The ranges span.
     * @type Number
     * @see #collapse
     */
    getSpan: function () {
        return this[1] - this[0];
    },
    
    /**
     * Return true if the range is collapsed.
     * @return Whether the range is collapsed.
     * @type Number
     * @see #collapse
     */
    isCollapsed: function () {
        return this[0] === this[1];
    },
    
    /**
     * Return true if this range is equal to the specified range.
     * @param {TEXT_SELECTION.Range} range The range to test against.
     * @return Whether this range is equal to the specifed range.
     * @type Number
     */
    eq: function (range) {
        return this[0] === range[0] && this[1] === range[1];
    },
    
    /**
     * Collapse the range.
     * @see #isCollapsed
     */
    collapse: function () {
        this[1] = this[0];
    },

    /** @ignore */
    toString: function () {
        return String(this[0]) + ', ' + String(this[1]);
    }
};


/*
Important! 

Unlike IE, FF and Safari (and almost certainly other browsers support) for 
multiple selections concurrently. Since IE does not support this feature, 
(and because it is not supported in the editing environment the library is 
primarily designed for, webprose), multiple selections are not coded for and 
only the first selection is in a set of concurrent selections is recognised.

The text selection library supports for selection of inline content within 
block level elements only. The library has been designed with paragraphs <p> 
and list items <li> specifically in mind, though support for other block 
level elements may also work.
*/

/*
Tips:
 
To ensure the that a selection works with the corresponding soup you must 
ensure that erroneous whitespace is removed from the HTML you are selecting 
or querying against. Parsing and re-rendering an elements content using 
IXMO.Soup will do this for you automatically.

Use of white space at the start of a tag can cause some versions of Safari's 
text selection to behave incorrectly, this is not related to this script and
can occur with JavaScript disabled.

The 'select' and 'query' methods will accept an array of elements, though 
they will only use the first element in the array. This is to make it easy to
pass through elements from frameworks that use arrays to support chaining of 
selected elements (e.g. JQuery).
*/


/**
 * Select a range within the element. 
 * @param {TEXT_SELECTION.Range|Array} range A range or two item array that
 * specifies the start and end of the selection.
 * @param {Element} element The element within which to make the selection.
 */
TEXT_SELECTION.select = function (range, element) {
    if (element instanceof Array) {
        element = element[0];    
    }
    
    range = TEXT_SELECTION._parseRange(range);

    var onPageRange = null;
    if (window.getSelection) {
        // FF/Safari
        window.getSelection().removeAllRanges();
        onPageRange = document.createRange();
        var start = TEXT_SELECTION._findNodeOffset(element, range[0]);
        var end = TEXT_SELECTION._findNodeOffset(element, range[1]);
        onPageRange.setStart(start.node, start.offset);
        onPageRange.setEnd(end.node, end.offset);
        window.getSelection().addRange(onPageRange);
        
    } else if (document.selection) {
        // IE
        onPageRange = document.selection.createRange();
        onPageRange.moveToElementText(element);
        onPageRange.collapse(true);
        onPageRange.moveStart('character', range[0]);
        onPageRange.moveEnd('character', range.getSpan());
        onPageRange.select();
    }
};

/**
 * Get a selection from within the element. 
 * @param {Element} element The element to return a selection from.
 * @return The range of the selection.
 * @type TEXT_SELECTION.Range
 */
TEXT_SELECTION.query = function (element) {
    if (element instanceof Array) {
        element = element[0];
    }
    
    var range = new TEXT_SELECTION.Range(0, 0);
    var onPageRange = null;
    
    if (window.getSelection) {
        // FF/Safari
        var selection = window.getSelection();
        
        if (selection.rangeCount > 0) {
            // Ignore all but the first selection range
            onPageRange = selection.getRangeAt(0);
            
            // Ensure the element that contains the selection starts and
            // ends with text nodes. This ensures inline block elements such 
            // as images and line breaks which start/end the element can be 
            // selected.
            var first = element.firstChild;
            var last = element.lastChild;
            
            // Introduced to cater for firefox 3.6 behaviour
            if (first === null && last === null) {
                return range;
            }
            
            if (first.nodeType !== Node.TEXT_NODE) {
                element.insertBefore(document.createTextNode(''), first);
            }
            
            if (last.nodeType !== Node.TEXT_NODE) {
                element.appendChild(document.createTextNode(''));
            }
            
            var _isParentOf = TEXT_SELECTION._isParentOf;
            if (_isParentOf(element, onPageRange.startContainer) &&
                _isParentOf(element, onPageRange.endContainer)) {
                var innerRange = TEXT_SELECTION._innerRange(element, 
                    selection);
                range[0] = TEXT_SELECTION._findNode(element, 
                    innerRange.startNode).offset;
                range[0] += innerRange.startOffset;
                range[1] = TEXT_SELECTION._findNode(element,
                    innerRange.endNode).offset;
                range[1] += innerRange.endOffset;
            } else {
                throw new Error('Selection is not within parent element');
            }
        }
    } else if (document.selection) {
        // IE
        onPageRange = document.selection.createRange();
        var outerRange = onPageRange.duplicate();
        // Determine the selected text
        outerRange.moveToElementText(element);
        outerRange.setEndPoint('EndToEnd', onPageRange);
        
        // Use IXMO to filter the HTML elements
        var startSoup = new IXMO.Soup(onPageRange.htmlText, 
            {trimSpaces: false, validateAttributes: false});
        var endSoup = new IXMO.Soup(outerRange.htmlText, 
            {trimSpaces: false, validateAttributes: false});
        
        range[0] = endSoup.getLength() - startSoup.getLength();
        range[1] = endSoup.getLength();
    }
    // Ensure range is always in lower to higher
    return new TEXT_SELECTION.Range(range[0], range[1]);
};


/**
 * Clear the current selection.
 */
TEXT_SELECTION.clear = function () { 
    if (window.getSelection && window.getSelection()) {
        // FF/Safari
        window.getSelection().removeAllRanges();
    } else if (document.selection && document.selection.type === 'Text') {
        // IE
        try {
            document.selection.empty();
        } catch (error) {
/*
Important! There is a known error that occurs sometimes when removing a 
selection in Internet Explorer. The error is not easily reproducable and 
varies from page to page. In an attempt to resolve the issue we now check 
that the selection type is equal to 'Text'.
If the error still occurs it can be safely ignored, since the error is thrown 
post a successful empty of the current selection.
*/           
        }
    }
};

// Private library functions

/** @private */
TEXT_SELECTION._parseRange = function (range) {
    if (range instanceof Array) {
        range = new TEXT_SELECTION.Range(range[0], range[1]);
    }
    return range;
};

/** @private */
TEXT_SELECTION._isParentOf = function (parent, child) {
    if (child === parent) {
        return true;
    }
    
    if (child.parentNode) {
        return TEXT_SELECTION._isParentOf(parent, 
            child.parentNode);
    }

    return false;
};

/** @private */
TEXT_SELECTION._findNodeOffset = function (parent, offset) {
    var nodeOffset = {node: parent, offset: offset, found: false};
    var children = parent.childNodes;
    var i = 0;
    while (!nodeOffset.found && children.length > 0) {
        // Update the offset data
        var child = children[i];
        nodeOffset.node = child;
        
        switch (child.nodeType) {
            
        case Node.TEXT_NODE:
            var textContent = child.textContent;
            var contentLength = textContent.length;
            if (contentLength >= nodeOffset.offset) {
                nodeOffset.found = true;
            } else {
                nodeOffset.offset -= contentLength;
            }
            break;
        
        case Node.ELEMENT_NODE:
            var nodeName = child.nodeName.toLowerCase();
            switch (nodeName) {

            case 'img':
                if (nodeOffset.offset === 1) {
                    nodeOffset.found = true;
                    nodeOffset.node = children[i];
                    nodeOffset.offset = 0;
                } else {
                    nodeOffset.offset -= 1;
                }
                break;
                
            case 'br':
                nodeOffset.parent = child;
                if (nodeOffset.offset === 1) {
                    nodeOffset.found = true;
                    nodeOffset.node = children[i];
                    nodeOffset.offset = 0;
                } else {
                    nodeOffset.offset -= 1;
                }
                break;
                
            case 'wbr':
                nodeOffset.parent = child;
                if (nodeOffset.offset === 1) {
                    nodeOffset.found = true;
                    nodeOffset.node = children[i];
                    nodeOffset.offset = 0;
                } else {
                    nodeOffset.offset -= 1;
                }
                break;
                
            default:
                if (child.childNodes) {
                    nodeOffset = TEXT_SELECTION._findNodeOffset(
                        child, nodeOffset.offset);
                }
                break;
            }
            break;
            
        default:
            // Ignore other nodes
            break;

        }   
        i++;
        if (i === children.length) {
            break;
        }
    }
    return nodeOffset;
};

/** @private */
TEXT_SELECTION._findNode = function (parent, child, nodeOffset) {
    var children = parent.childNodes;
    if (nodeOffset === undefined) {
        nodeOffset = {offset: 0, found: false};
    }
    var i = 0;
    while (!nodeOffset.found) {
        var currentChild = children[i];
        if (currentChild === child) {
            nodeOffset.found = true;
        } else {
            switch (currentChild.nodeType) {
                
            case Node.TEXT_NODE:
                var textContent = currentChild.textContent;
                var contentLength = textContent.length;
                nodeOffset.offset += contentLength;
                break;
            
            case Node.ELEMENT_NODE:
                var nodeName = currentChild.nodeName.toLowerCase();
                switch (nodeName) {

                case 'img':
                    nodeOffset.offset += 1;
                    break;
                    
                case 'br':
                    nodeOffset.offset += 1;
                    break;
                    
                case 'wbr':
                    nodeOffset.offset += 1;
                    break;
                    
                default:
                    if (currentChild.childNodes) {
                        TEXT_SELECTION._findNode(currentChild, child, 
                            nodeOffset);
                    }
                    break;
                }
                break;
                
            default:
                // Ignore other nodes
                break;
            }   
        }
        i++;
        if (i === children.length) {
            break;
        }
    }
    return nodeOffset;
};

/** @private */
TEXT_SELECTION._innerRange = function (element, selection) {
    var onPageRange = selection.getRangeAt(0).cloneRange();
    var startRange = selection.getRangeAt(0).cloneRange();
    var endRange = selection.getRangeAt(0).cloneRange();
    startRange.collapse(true);
    endRange.collapse(false);
    
    var innerRange = {startNode: startRange.startContainer, 
        startOffset: startRange.startOffset,
        endNode: endRange.endContainer, 
        endOffset: endRange.endOffset};
    
    var children = element.childNodes;
    var i = 0;
    var child = null;
    var textContent = '';
    var textContentLength = 0;
    var nodeName = '';
       
    if (startRange.startContainer === element) {
        innerRange.startNode = children[children.length - 1];
        innerRange.startOffset = innerRange.startNode.textContent.length;
        for (i = 0; i < children.length; i++) {
            child = children[i];
            if (startRange.comparePoint(child, 0) === 1) {
                if (i > 0) {
                    innerRange.startNode = children[i - 1];
                    textContent = innerRange.startNode.textContent;
                    textContentLength = textContent.length;
                    innerRange.startOffset = textContentLength;
                    nodeName = child.nodeName.toLowerCase();
                } else {
                    innerRange.startNode = child;
                    innerRange.startOffset = 0;
                }
                nodeName = innerRange.startNode.nodeName.toLowerCase();
                if (nodeName === 'img' || nodeName === 'br' || nodeName === 'wbr') {
                    innerRange.startOffset += 1;
                }
                break;
            }
        }
    }
    
    if (onPageRange.collapsed) {
        innerRange.endNode = innerRange.startNode;
        innerRange.endOffset = innerRange.startOffset;
    } else if (endRange.startContainer === element) {
        innerRange.endNode = children[children.length - 1];
        innerRange.endOffset = innerRange.endNode.textContent.length;
        for (i = 0; i < children.length; i++) {
            child = children[i];
            if (endRange.comparePoint(child, 0) === 1) {
                if (i > 0) {
                    innerRange.endNode = children[i - 1];
                    nodeName = innerRange.endNode.nodeName.toLowerCase();
                } else {
                    innerRange.endNode = child;                  
                }
                textContent = innerRange.endNode.textContent;
                textContentLength = textContent.length;
                innerRange.endOffset = textContentLength;
                innerRange.endOffset += 1;
                break;
            }
        }
    }
    
    return innerRange;
};