/**
 * @fileoverview    Fluid window widget. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widgets) (/js/fluid/widget.js)
 */

/**
 * A window widget.
 * @class A window class.
 * @constructor
 */
FLUID.Window = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    // Default the parent to the desktop
    if (options.parent === undefined) {
        options.parent = options.parent || FLUID.widgetByName('desktop');
    }
    
    FLUID.Widget.call(this, options, false);
    FLUID.Helpful.call(this, options);
    FLUID.Labelled.call(this, options);
    this._type.push('Window');
    
    // Groups
    this._childGroups[this.__defaultClient__] = [];
    this._childGroups['info'] = [];
    this._childGroups['actions'] = [];
    
    /** @private */
    this._minimized = false;
    
    /** @private */
    this._maximized = false;
    
    /** @private */
    this._minSize = options.minSize || [300, 200];
    
    /** @private */
    this._maxSize = options.maxSize || [0, 0]
    
    /** @private */
    this._state = FLUID.Window.DORMANT_STATE;
    
    /** @size */
    this._position = options.position || [10, 10];
    
    /** @size */
    this._size = options.size || [300, 200];
    
    /** @private */
    this._flags = options.flags;
    
    /** @private */
    this._resizer = null;
    
    /** @private */
    this._stores = {};
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Window, FLUID.Widget);
UTILS.copyPrototype(FLUID.Window, FLUID.Helpful);
UTILS.copyPrototype(FLUID.Window, FLUID.Labelled);

// Constants
FLUID.Window.MINIMIZED_WIDTH = 300;
FLUID.Window.MAXIMIZED_MARGIN = 10;
FLUID.Window.CONSTRAINED_MARGIN = 30;

// Flags
FLUID.Window.CAN_CLOSE = 1;
FLUID.Window.CAN_MINIMIZE = 2;
FLUID.Window.CAN_MAXIMIZE = 4;
FLUID.Window.CAN_RESIZE = 8;
FLUID.Window.CAN_DRAG = 16;
FLUID.Window.CAN_DOCK = 32;
FLUID.Window.HAS_MINIMUM_BORDER = 64;
FLUID.Window.HAS_INFO = 128;
FLUID.Window.HAS_ACTIONS = 256;
FLUID.Window.CENTERED = 512;

// States
FLUID.Window.DORMANT_STATE = 1;
FLUID.Window.OPEN_STATE = 2;
FLUID.Window.CLOSE_STATE = 4;
FLUID.Window.OPEN_DURATION = 250;
FLUID.Window.CLOSE_DURATION = 250;

$$.extend(FLUID.Window.prototype, {
    __className__: 'FLUID.Window',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
           '<div class="fluid-widget fluid-title">' + 
                '<div class="fluid-widget fluid-tools">' + 
                    '<span class="fluid-minimize fluid-icon" title="Minimize"></span><span class="fluid-maximize fluid-icon" title="Maximize"></span><span class="fluid-close fluid-icon" title="Close"></span>' + 
                '</div>' + 
                '<span class="fluid-icon"></span><span class="fluid-label"><%=widget._label%></span>' + 
            '</div>' + 
            '<div class="fluid-info <%=FLUID_GROUP_PREFIX%>info">' + 
                '<%=group_info%>' +
            '</div>' + 
            '<div class="fluid-client <%=FLUID_GROUP_PREFIX%><%=FLUID_DEFAULT_GROUP%>">' + 
                '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +
            '</div>' + 
            '<div class="fluid-actions <%=FLUID_GROUP_PREFIX%>actions">' + 
                '<%=group_actions%>' +
            '</div>' + 
            '<div class="fluid-widget fluid-resize"></div>' + 
        '</div>',
    __frameworkEvents__: {
        'mouseover': 'mouseOver',
        'mouseout': 'mouseOut',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown'
    },
    __css__: FLUID._cssName('window'),
    
	/**
	 * Return the state of the window.
	 * @return The windows state.
	 * @type Number
	 */
    getState: function () {
        return this._state;
    },

    /**
     * Set/Get the label for the window.
     * @param {String} value (Optional) If undefined the current label for
     * the window is returned, else the window's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this.getHTMLSelection().children('.fluid-title').children('.fluid-label').html(value);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the icon for the window.
     * @param {String} value (Optional) If undefined the current icon for
     * the window is returned, else the window's icon is set.
     * @return The window's icon.
     * @type String
     */
    icon: function (value) {
        if (value === undefined) {
            if (typeof(this._icon) === 'string') { 
                return this.getDesktop().theme().getResources().get(
                    this._icon);
            } else {
                return this._icon;
            }
        }
        this.trigger('icon', function () {
            if (this._icon !== value) {
                this._icon = value;
                var iconSelection = this.getHTMLSelection().children('.fluid-title').children('.fluid-icon');
                if (this.icon() !== null) {        
                    iconSelection.css('background', 
                        this.icon().getBackgroundCSS(16));
                    iconSelection.show();
                } else {
                    iconSelection.hide();
                }
            }
        }, {data: {'value': value}});    
    },
    
    /**
     * Set/Get the position of the window.
     * @param {Array} value (Optional) If undefined the current position of
     * the window is returned, else the window's position is set.
     * @return The position.
     * @type Array
     */
    position: function (value) {
        if (value === undefined) {
            return this._position.slice();
        }
        this.trigger('position', function () {
            if (this._position[0] !== value[0] || this._position[1] !== value[1]) {
                this._position = value;
                this._update();
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the size of the window.
     * @param {Array} value (Optional) If undefined the current size of the
     * the window is returned, else the window size is set.
     * @return The size.
     * @type Array
     */
    size: function (value) {
        if (value === undefined) {
            return this._size.slice();
        }
        this.trigger('size', function () {
            if (this._size[0] !== value[0] || this._size[1] !== value[1]) {
                
                // Constrain the size to min/max
                if (this._minSize[0] > 0) {
                    value[0] = Math.max(this._minSize[0], value[0]);
                }
                if (this._minSize[1] > 0) {
                    value[1] = Math.max(this._minSize[1], value[1]);
                }
                if (this._maxSize[0] > 0) {
                    value[0] = Math.min(this._maxSize[0], value[0]);
                }
                if (this._maxSize[1] > 0) {
                    value[1] = Math.min(this._maxSize[1], value[1]);
                }
                
                this._size = value;
                            
                this._update();
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the minimum size of the window.
     * @param {Array} value (Optional) If undefined the current minimum size 
     * of the the window is returned, else the window's minimum size is set.
     * @return The size.
     * @type Array
     */
    minSize: function (value) {
        if (value === undefined) {
            return this._minSize.slice();
        }
        this.trigger('minSize', function () {
            if (this._minSize[0] !== value[0] || this._minSize[1] !== value[1]) {
                this._minSize = value;
                this.size(this.size());
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the maximum size of the window.
     * @param {Array} value (Optional) If undefined the current maximum size 
     * of the the window is returned, else the window's maximum size is set.
     * @return The size.
     * @type Array
     */
    maxSize: function (value) {
        if (value === undefined) {
            return this._maxSize.slice();
        }
        this.trigger('maxSize', function () {
            if (this._maxSize[0] !== value[0] || this._maxSize[1] !== value[1]) {
                this._maxSize = value;
                this.size(this.size());
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the flags for the window.
     * @param {Number} value (Optional) If undefined the current flags for
     * the window are returned, else the widgets's flags are set.
     * @return The windows flags.
     * @type Number
     */
    flags: function (value) {
        if (value === undefined) {
            return this._flags;
        }
        this.trigger('flags', function () {
            if (this._flags !== value) {
                this._flags = value;
                this._update();
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Prepare the window so that it can be opened.
     * @param {Function} prepareFunc (Optional) A function to call as part of 
     * the preparation. 
     */
    prepare: EVENT.classMethod(FLUID.Window, function prepare(prepareFunc) {
		var localArgs = EVENT.getArgs(arguments);
        var prepareFunc = EVENT.getArg(localArgs, 0);
        if (!this.exists()) { 
            return;
        }
        
        this._show();
        if (prepareFunc !== undefined) {
            prepareFunc(this);
        }

		// Ensure that the window doesn't open on top of another window
		var existingPositions = {};
		var siblings = this.getSiblings();
		for (var i = 0; i < siblings.length; i++) {
			var sibling = siblings[i];
			if (sibling.typeOf('Window')) {
				existingPositions[sibling.position().toString()] = true;
			}
		}
		while (existingPositions[this.position().toString()] !== undefined) {
			var position = this.position();
			this.position([
				position[0] + 10,
				position[1] + 10
				]);
		}
		
        this._update(); 
        this._applyStates();
		this.size(this.size());
		this._show(false);
        
    }, 'prepare'),

    /**
     * Return true if the window is opened or is transitioning to that 
     * state.
     * @return Whether the widget is opened or is transitioning to that 
     * state.
     * @type Boolean
     */
    opened: function () {
        return this._transitionState === FLUID.Window.OPEN_STATE;
    },
    
    /**
     * Open the window.
     */
    open: EVENT.classMethod(FLUID.Window, function open() {
        if (!this.exists()) { 
            return;
        }
        
        switch (this._transitionState) {

        case FLUID.Window.OPEN_STATE:
            return;
                            
        case FLUID.Window.CLOSE_STATE:
            this.getHTMLSelection().stop(true, true);
            this.hide();
            break;
        
        case FLUID.Window.DORMANT_STATE:
            this.hide();
            break;
        
        default:
            break;

        }

        this._transitionState = FLUID.Window.OPEN_STATE; 
		
        this.getHTMLSelection().fadeIn(
            FLUID.Window.OPEN_DURATION,
            function (event) {
                $$(this).data('fluid-widget').trigger('openComplete', 
                    function (event) {
                        this.show();
                    });
            });
        
    }, 'open'),

    /**
     * Close the window.
     */
    close: EVENT.classMethod(FLUID.Window, function close() {
            if (!this.exists()) {
                return;
            }
            
            switch (this._transitionState) {

            case FLUID.Window.OPEN_STATE:
                this.getHTMLSelection().stop(true, true);
                this.show();
                break;
                                
            case FLUID.Window.CLOSE_STATE:
                return;
                
            case FLUID.Window.DORMANT_STATE:
                this.show();
                break;
            
            default:
                break;
            }
            this._transitionState = FLUID.Window.CLOSE_STATE;
            
            this.getHTMLSelection().fadeOut(
                FLUID.Window.CLOSE_DURATION,
                function (event) {
                    $$(this).data('fluid-widget').trigger('closeComplete', 
                        function (event) {
                            this.hide();
						});
                });
            
        }, 'close'),

    /**
     * Return true if the window is minimized.
     * @return Whether the widget is minimized.
     * @type Boolean
     */
    minimized: function () {
        return this._minimized;
    },

    /**
     * Minimize the window.
     */
    minimize: EVENT.classMethod(FLUID.Window, function minimize() {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        
        if (value === undefined) {
            value = true;
        }
        this._minimized = value;
        
        this._update();
        
    }, 'minimize'),

    /**
     * Return true if the window is maximized.
     * @return Whether the widget is maximized.
     * @type Boolean
     */
    maximized: function () {
        return this._maximized;
    },

    /**
     * Maximize the window.
     * @param {Boolean} If true then the window is maximized (true by 
     * default) else it is reset to it's normal size.
     */
    maximize: EVENT.classMethod(FLUID.Window, function maximize() {
			var localArgs = EVENT.getArgs(arguments);
            var value = EVENT.getArg(localArgs, 0);
            
            if (value === undefined) {
                value = true;
            }
            this._maximized = value;
            
            // Maximizing resets minimized windows
            if (this._maximized && this._minimized) {
                this._minimized = false;
            }
            
            this._update();
            
        }, 'maximize'),

    /**
     * Center the window
     */
    center: EVENT.classMethod(FLUID.Window, function center(update) {
			var localArgs = EVENT.getArgs(arguments);
            var update = EVENT.getArg(localArgs, 0);
            
            if (!this.exists()) {
                return;
            }
            
            var screenWidth = $$(window).width();
            var screenHeight = $$(window).height();
            
            var width = this._size[0];
            if (this._minimized) {
                if ((FLUID.Window.HAS_MINIMUM_BORDER & this._flags) !== 
                        FLUID.Window.HAS_MINIMUM_BORDER) {
                    width = FLUID.Window.MINIMIZED_WIDTH;
                }
            }
            
            var height = this.getHTMLSelection().outerHeight(true);
            
            var left = parseInt((screenWidth - width) / 2, 10);
            var top = parseInt((screenHeight - height) / 2, 10);
            
            if (left !== this._position[0] || top !== this._position[1]) {
                this._position = [left, top];
            }
        
            if (update !== false) {
                this.position(this.position());
                this.size(this.size());
            }
            
        }, 'center'),

    /**
     * Constrain the window to the client area
     */
    constrain: EVENT.classMethod(FLUID.Window, function constrain() {
            if (!this.exists()) {
                return;
            }
            
            var screenWidth = $$(window).width();
            var screenHeight = $$(window).height();
            
            var windowWidth = this._size[0];
            var windowHeight = this._size[1];
            if (this._minimized) {
                if ((FLUID.Window.HAS_MINIMUM_BORDER & this._flags) !== 
                        FLUID.Window.HAS_MINIMUM_BORDER) {
                    windowWidth = FLUID.Window.MINIMIZED_WIDTH;
                }                
                windowHeight = this.getHTMLSelection().find('.' + 
                    FLUID._cssName('title')).outerHeight(false, true);
            }
			
			// If the height is auto we have to query for the heieght of the 
			// window.
			if (windowHeight === 'auto') {
				windowHeight = this.getHTMLSelection().outerHeight(false, true);
			}
            
            var right = this._position[0] + windowWidth;
            var bottom = this._position[1] + windowHeight;
            if (right > (screenWidth) || bottom > (screenHeight)) {
                var pos = this.position();
                var orgPos = this.position();

                var size = this.size();
				var orgSize = this.size();
                
                if (windowWidth > screenWidth) {
                    pos[0] = 0;
					size[0] = screenWidth;
                } else {
                    pos[0] = Math.min(Math.max(0, this._position[0]), screenWidth - windowWidth);    
                }
                if (windowHeight > screenHeight - FLUID.Window.CONSTRAINED_MARGIN * 2) {
                    pos[1] = FLUID.Window.CONSTRAINED_MARGIN;
					size[1] = screenHeight - FLUID.Window.CONSTRAINED_MARGIN * 2;
                } else {
                    pos[1] = Math.min(Math.max(0, this._position[1]), screenHeight - windowHeight);
                }
                if (pos[0] !== orgPos[0] || pos[1] !== orgPos[1]) {
                    this.position(pos);
                }

                // Constrain the size to min/max
                if (this._minSize[0] > 0) {
                    size[0] = Math.max(this._minSize[0], size[0]);
                }
                if (this._minSize[1] > 0) {
                    size[1] = Math.max(this._minSize[1], size[1]);
                }
                if (this._maxSize[0] > 0) {
                    size[0] = Math.min(this._maxSize[0], size[0]);
                }
                if (this._maxSize[1] > 0) {
                    size[1] = Math.min(this._maxSize[1], size[1]);
                }                
                
                if (size[0] !== orgSize[0] || size[1] !== orgSize[1]) {
                    this.size(size);
                }
            }
            
        }, 'constrain'),

    /**
     * Send the window to the front.
     */
    sendToFront: function () {
        // Select the sibling windows
        var siblings = this.getSiblings();
        var siblingWindows = [];
        for (var i = 0; i < siblings.length; i++) {
            if (siblings[i].typeOf('Window')) {
                siblingWindows.push(siblings[i]);
            }
        } 
        
        // Sort the sibling windows by their current z-indexs
        siblingWindows.sort(function (a, b) {
            a = parseInt(a.style('z-index') || 0);
            b = parseInt(b.style('z-index') || 0);
            if (a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            } else {
                return 0;
            }
        });
        siblingWindows.push(this);
        
        // Assign the windows new z-indexes
        for (var j = 0; j < siblingWindows.length; j++) {
            siblingWindows[j].style({'z-index': j});
        }
        if (this.exists()) {
            this._applyStates();
        }
    },
    
    /**
     * Return true if the window has focus. 
     */
    hasFocus: function () {
        var zIndex = this.style('z-index');
        var siblings = this.getSiblings();
        for (var i = 0; i < siblings.length; i++) {
            if (siblings[i].typeOf('Window')) {
                if (zIndex < siblings[i].style('z-index')) {
                    return false;
                }
            }
        }
        return true;
    },
    
    /**
     * Store the state of the window.
     * @param {String} name The name to store the state against to make it 
     * simple to perform a restore.
     */
    store: function (name) {
        this._stores[name] = {
            position: this.position(),
            size: this.size(),
            minSize: this.minSize(),
            maxSize: this.maxSize(),
            flags: this.flags()
        };
    },
     
    /**
     * Restore the state of the window.
     * @param {String} name The name of the store to restore from.
     */
    restore: function (name, options) {
        options = UTILS.optionsArg(options, {});
        if (this._stores[name] === undefined) {
            return;
        }
        var store = this._stores[name];
        if (options.minSize !== false) {
            this.minSize(store.minSize);
        }
        if (options.maxSize !== false) {
            this.maxSize(store.maxSize);
        }
        if (options.size !== false) {
            this.size(store.size);
        }
        if (options.flags !== false) {
            this.flags(store.flags);
        }
        if (options.position !== false) {
            this.position(store.position);
        }
    },
    
    /**
     * Return true if the window has the specified store
     * @param {String} name The name of the store to check for.
     * @return True if the named store exists for the window.
     * @type Boolean
     */
    hasStore: function (name) {
        return this._stores[name] !== undefined;
    },
    
    /**
     * Destroy the window.
     */
    destroy: function () {
     	FLUID.Widget.prototype.destroy.apply(this);
		
		// Unbind the window resize event
     	$$(window).unbind('resize.fluid.' + this._id);

		// Tool selections
        var closeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('close');
        var minimizeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('minimize');
        var maximizeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('maximize');
        var toolSelector = closeSelector + ', ' + minimizeSelector + ', ' + 
            maximizeSelector;
        var resizeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('resize');
	
        // Enter, Leave, Down & Up    
        $$(document).undelegate(toolSelector);

        // Close
        $$(document).undelegate(closeSelector);

        // Minimize
        $$(document).undelegate(minimizeSelector);

        // Maximize
        $$(document).undelegate(maximizeSelector);

        // Resize grip
		$$(document).undelegate(resizeSelector);
    },
    
    /** @private */
    _show: function (value) {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        
        if (value === undefined) {
            value = true;
        }
        if (this._visible !== value) {
            this._visible = value;
            if (this.exists()) {
                var selection = this.getHTMLSelection();
                if (value) {
		            this.getHTMLSelection().removeClass(FLUID._cssName('hidden'));
					this._state = FLUID.Window.OPEN_STATE;
                } else {
			        this.getHTMLSelection().addClass(FLUID._cssName('hidden'));
					this._state = FLUID.Window.DORMANT_STATE;
                }
            }
        }        
    },
	
    /** @private */
    _startResize: function () {
        if (!this.exists()) {
            return;
        }
        
        this.hide();
        this._resizer = new FLUID.Window.Resizer({window: this});
        this.getDesktop().appendChild(this._resizer, 'singleModal');
        this._resizer.refresh();
        this.getDesktop().startModal(FLUID.Desktop.SINGLE_MODAL);
    },
    
    /** @private */
    _endResize: function () {
		this.position([this._resizer._style.left, this._resizer._style.top]);
		this.size([this._resizer._style.width, this._resizer._style.height]);

        this._resizer.burst();
        
        this.show();
        this._update();
        
        // Destroy the resize
        this._resizer.destroy();
        this._resizer = null;
    },
    
    /** @private */
    _update: function () {
        var supportExtensions = EVENT.getArg(supportExtensions, 0);
        
        if (!this.exists()) {
            return;
        }
        
        // Drag support
        if (this.getHTMLSelection().data('draggable') === undefined) {
            this.getHTMLSelection().draggable({
                handle: this.getHTMLSelection().find('.' + 
                    FLUID._cssName('title')).first(),
                scroll: false,
                start: function (fwEvent, ui) {
                    $$(this).addClass('fluid-dragging');
                },
                stop: function (fwEvent, ui) {
                    $$(this).removeClass('fluid-dragging');
                    $$(this).data('fluid-widget')._position = [
                        Math.max(0, ui.position.left),
                        Math.max(0, ui.position.top)
                    ];
                    $$(this).data('fluid-widget')._constrain();
                    $$(this).data('fluid-widget')._update();
                }
            });
        }   
        
        if ((this._maximized && !this._minimized) || 
                (FLUID.Window.CAN_DRAG & this._flags) !== 
                FLUID.Window.CAN_DRAG) {
            this.getHTMLSelection().draggable('option', 'disabled', true);
        } else {
            this.getHTMLSelection().draggable('option', 'disabled', false);
        }
        
        // Start of standard update process
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
        
        FLUID.Helpful.prototype._cUpdate.apply(this);
        
        // Icon
        var iconSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('title') + ' .' + 
                FLUID._cssName('icon'));
        if (this.icon() !== null) {        
            iconSelection.css('background', 
                this.icon().getBackgroundCSS(16));
            iconSelection.show();
        } else {
            iconSelection.hide();
        }
        
        // Close button
        var closeSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('title') + ' .' + 
                FLUID._cssName('close'));
        if ((FLUID.Window.CAN_CLOSE & this._flags) === 
                FLUID.Window.CAN_CLOSE) {
            closeSelection.css('display', 'inline-block');
            closeSelection.removeClass(FLUID._cssName('disabled'));
        } else {
            if ((FLUID.Window.CAN_MINIMIZE & this._flags) === 
                    FLUID.Window.CAN_MINIMIZE ||
                (FLUID.Window.CAN_MAXIMIZE & this._flags) === 
                    FLUID.Window.CAN_MAXIMIZE) {
                closeSelection.addClass(FLUID._cssName('disabled'));
            } else {
                closeSelection.hide();
            }
        }
        
        // Minimize button
        var minimizeSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('title') + ' .' + 
                FLUID._cssName('minimize'));
        if ((FLUID.Window.CAN_MINIMIZE & this._flags) === 
                FLUID.Window.CAN_MINIMIZE) {
            minimizeSelection.css('display', 'inline-block');
            minimizeSelection.removeClass(FLUID._cssName('disabled'));
        } else {
            if ((FLUID.Window.CAN_MAXIMIZE & this._flags) === 
                    FLUID.Window.CAN_MAXIMIZE ||
                (FLUID.Window.CAN_CLOSE & this._flags) === 
                    FLUID.Window.CAN_CLOSE) {
                minimizeSelection.addClass(FLUID._cssName('disabled'));
            } else {
                minimizeSelection.hide();
            }
        }
        
        // Maximize button
        var maximizeSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('title') + ' .' + 
                FLUID._cssName('maximize'));
        if ((FLUID.Window.CAN_MAXIMIZE & this._flags) === 
                FLUID.Window.CAN_MAXIMIZE) {
            maximizeSelection.css('display', 'inline-block');
            maximizeSelection.removeClass(FLUID._cssName('disabled'));
        } else {
            if ((FLUID.Window.CAN_MINIMIZE & this._flags) === 
                    FLUID.Window.CAN_MINIMIZE ||
                (FLUID.Window.CAN_CLOSE & this._flags) === 
                    FLUID.Window.CAN_CLOSE) {
                maximizeSelection.addClass(FLUID._cssName('disabled'));
            } else {
                maximizeSelection.hide();
            }
        }
        
        // Resize button
        var resizeSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('resize'));
        if ((FLUID.Window.CAN_RESIZE & this._flags) === 
                FLUID.Window.CAN_RESIZE) {
            resizeSelection.show();
        } else {
            resizeSelection.hide();
        }
	    
        // Position & Size (must be pre-determined)
		
		// We have to apply states (in effect twice), once here and once in
		// the call to the base widget _update method.
		this._applyStates();
        
		if (this._maximized && !this._minimized) {
            this._setStyle({
                top: FLUID.Window.MAXIMIZED_MARGIN, 
                left: FLUID.Window.MAXIMIZED_MARGIN, 
                width: $$(window).width() - 
                    FLUID.Window.MAXIMIZED_MARGIN * 2,
                height: $$(window).height() - 
                    FLUID.Window.MAXIMIZED_MARGIN * 2
            });   
        } else {
			
            this._setStyle({
                width: this._size[0],
                height: this._size[1]
            });
        
            if ((FLUID.Window.CENTERED & this._flags) === 
                        FLUID.Window.CENTERED) {
                // Center the window
                this._center(false);
            } else {
                // Make sure the window is still on screen
                this._constrain();
            }
        
            this._setStyle({
                top: this._position[1], 
                left: this._position[0], 
                width: this._size[0],
                height: this._size[1],
				// @@ Ensure this has no adverse effects, it's required to ensure windows don't open at a height greater than the max height on height: auto (and think about height)
				'max-height': this._maxSize[1] || 'auto' 
            });
            if (this._minimized) {
                if ((FLUID.Window.HAS_MINIMUM_BORDER & this._flags) !== 
                        FLUID.Window.HAS_MINIMUM_BORDER) {
                    this._setStyle({width: FLUID.Window.MINIMIZED_WIDTH});
                }
            }
        }       	
		
		FLUID.Widget.prototype._update.apply(this, [false]);	
		
        this.getHTMLSelection().css('position', 'fixed');

        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);        
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Widget.prototype._applyStates.apply(this);
        FLUID.Labelled.prototype._cApplyStates.apply(this);
        
        // Flags
        if ((FLUID.Window.CAN_CLOSE & this._flags) === 
                FLUID.Window.CAN_CLOSE) {
            this.addClass(FLUID._cssName('can-close'));
        } else {
            this.removeClass(FLUID._cssName('can-close'));
        }
        if ((FLUID.Window.CAN_MINIMIZE & this._flags) === 
                FLUID.Window.CAN_MINIMIZE) {
            this.addClass(FLUID._cssName('can-minimize'));
        } else {
            this.removeClass(FLUID._cssName('can-minimize'));
        }
        if ((FLUID.Window.CAN_MAXIMIZE & this._flags) === 
                FLUID.Window.CAN_MAXIMIZE) {
            this.addClass(FLUID._cssName('can-maximize'));
        } else {
            this.removeClass(FLUID._cssName('can-maximize'));
        }
        if ((FLUID.Window.CAN_DRAG & this._flags) === 
                FLUID.Window.CAN_DRAG) {
            this.addClass(FLUID._cssName('can-drag'));
        } else {
            this.removeClass(FLUID._cssName('can-drag'));
        }
        if ((FLUID.Window.CAN_RESIZE & this._flags) === 
                FLUID.Window.CAN_RESIZE) {
            this.addClass(FLUID._cssName('can-resize'));
        } else {
            this.removeClass(FLUID._cssName('can-resize'));
        }
        if ((FLUID.Window.HAS_MINIMUM_BORDER & this._flags) === 
                FLUID.Window.HAS_MINIMUM_BORDER) {
            this.addClass(FLUID._cssName('has-minimum-border'));
        } else {
            this.removeClass(FLUID._cssName('has-minimum-border'));
        }
        if ((FLUID.Window.HAS_ACTIONS & this._flags) === 
                FLUID.Window.HAS_ACTIONS) {
            this.addClass(FLUID._cssName('has-actions'));
        } else {
            this.removeClass(FLUID._cssName('has-actions'));
        }
        if ((FLUID.Window.HAS_INFO & this._flags) === 
                FLUID.Window.HAS_INFO) {
            this.addClass(FLUID._cssName('has-info'));
        } else {
            this.removeClass(FLUID._cssName('has-info'));
        }
        
        // States
        if (this._maximized && !this._minimized) {
            this.addClass(FLUID._cssName('maximized'));
        } else {
            this.removeClass(FLUID._cssName('maximized'));
        }
        if (this._minimized) {
            this.addClass(FLUID._cssName('minimized'));
        } else {
            this.removeClass(FLUID._cssName('minimized'));
        }
        
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        // Tool events
        var closeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('close');
        var minimizeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('minimize');
        var maximizeSelector = '#' + this._htmlElementId + 
            ' .' + FLUID._cssName('title') + ' .' + 
            FLUID._cssName('maximize');
        var toolSelector = closeSelector + ', ' + minimizeSelector + ', ' + 
            maximizeSelector;
	    var resizeSelector = '#' + this._htmlElementId + 
	        ' .' + FLUID._cssName('resize');
        
        // Enter, Leave, Down & Up    
        $$(document).delegate(toolSelector, 'mouseenter.fluid', 
            function (fwEvent) {
                $$(this).addClass(FLUID._cssName('over'));        
            });
        $$(document).delegate(toolSelector, 'mouseleave.fluid', 
            function (fwEvent) {
                $$(this).removeClass(FLUID._cssName('over')); 
                $$(this).removeClass(FLUID._cssName('down'));     
            });
        $$(document).delegate(toolSelector, 'mousedown.fluid', 
            function (fwEvent) {
                $$(this).addClass(FLUID._cssName('down'));     
            });
        $$(document).delegate(toolSelector, 'mouseup.fluid', 
            function (fwEvent) {
                $$(this).removeClass(FLUID._cssName('down'));     
            });
        
        // Close
        $$(document).delegate(closeSelector, 'click.fluid', {widget: this},
            function (fwEvent) {
                var widget = fwEvent.data.widget;
                if ((FLUID.Window.CAN_CLOSE & widget._flags) === 
                        FLUID.Window.CAN_CLOSE) {
                    widget.close();
                }
            });

        // Minimize
        $$(document).delegate(minimizeSelector, 'click.fluid', 
            {widget: this},
            function (fwEvent) {
                var widget = fwEvent.data.widget;
                if ((FLUID.Window.CAN_MINIMIZE & widget._flags) === 
                        FLUID.Window.CAN_MINIMIZE) {
                    widget.minimize(!widget.minimized());
                }
            });
            
        // Maximize
        $$(document).delegate(maximizeSelector, 'click.fluid', 
            {widget: this},
            function (fwEvent) {
                var widget = fwEvent.data.widget;
                if ((FLUID.Window.CAN_MAXIMIZE & widget._flags) === 
                        FLUID.Window.CAN_MAXIMIZE) {
                    widget.maximize(!widget.maximized());
                }
            });
            
        // Resize grip
        $$(document).delegate(resizeSelector, 'mousedown.fluid', 
            {widget: this},
            function (fwEvent) {
                if (!fwEvent.data.widget.maximized()) {
                    fwEvent.data.widget._startResize();
                }
            });
        
        // Window resize event
        $$(window).bind('resize.fluid.' + this._id, {widget: this}, 
                function (fwEvent) {
            var win = fwEvent.data.widget;
            if (win.exists()) {
                win._update();
            }
        });
        
        // Focus        
        this.bind('onMouseDown.fluid', function(event) {
            this.sendToFront();
        });
    },
    
    /**
     * The default options when creating a new window.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: false,
        enabled: true,
        tooltip: '',
        helpURL: '',
        flags: 0
    }
});

/**
 * A resizer widget for a window.
 * @class A window resizer class.
 * @constructor
 */
FLUID.Window.Resizer = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('Window.Resizer');
    
    /** @private */
    this._window = options.window;
    
    // Set the initial size based on the windows size
    this._setStyle({top: this._window._position[1], 
        left: this._window._position[0],
        width: this._window._size[0],
        height: this._window._size[1]
        });
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Window.Resizer, FLUID.Widget);

$$.extend(FLUID.Window.Resizer.prototype, {
    __className__: 'FLUID.Window.Resizer',
    __template__: '<div id="<%=widget.getHTMLElementId()%>"></div>',
    __css__: FLUID._cssName('window-resizer'),
   
    /**
     * Burst the resizer. 
     */
    burst: EVENT.classMethod(FLUID.Window.Resizer, function burst() {
            this.remove();        
            this.getDesktop().endModal(FLUID.Desktop.SINGLE_MODAL);
        }, 'burst'),
    
    /**
     * Destroy the window.
     */
    destroy: function () {
        // Unbind the various single modal events

        var singleModal = this._window.getDesktop().getHTMLSelection(
            'singleModal');
        
        $$(singleModal).unbind('mouseup.fluid.' + this._id);
        $$(singleModal).unbind('mousemove.fluid.' + this._id);
        FLUID.Widget.prototype.destroy.apply(this);
    },
   
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
        var singleModal = this._window.getDesktop().getHTMLSelection(
            'singleModal');
        
        $$(singleModal).bind('mouseup.fluid.' + this._id, 
            {widget: this}, 
            function (fwEvent) {
                fwEvent.data.widget._window._endResize();
            });
            
        $$(singleModal).bind('mousemove.fluid.' + this._id, 
            {widget: this}, 
            function (fwEvent) {
                
                var widget = fwEvent.data.widget;
                var x = fwEvent.pageX - $$(document).scrollLeft();
                var y = fwEvent.pageY - $$(document).scrollTop();
                var left = widget._window.position()[0];
                var top = widget._window.position()[1];
                var width = x - left;
                var height = y - top;
                var minSize = widget._window.minSize();
                var maxSize = widget._window.maxSize();
                
                // Constrain the size
                if (minSize[0] > 0) {
                    width = Math.max(minSize[0], width);
                }
                if (minSize[1] > 0) {
                    height = Math.max(minSize[1], height);
                }
                if (maxSize[0] > 0) {
                    width = Math.min(maxSize[0], width);
                }
                if (maxSize[1] > 0) {
                    height = Math.min(maxSize[1], height);
                }
                
                if ((FLUID.Window.CENTERED & widget._window._flags) === 
                        FLUID.Window.CENTERED) {
                    
                    var widthDiff = (width - widget._window.size()[0]);
                    var heightDiff = (height - widget._window.size()[1]);
                    
                    if (width + widthDiff >= widget._window.minSize()[0]) {
                        left -= widthDiff;
                        width += widthDiff;
                        widget._setStyle({
                            left: left,
                            width: width
                        });
                    }  
                    
                    if (height + heightDiff >= widget._window.minSize()[1]) {
                        top -= heightDiff;
                        height += heightDiff;
                        widget._setStyle({
                            top: top,
                            height: height
                        });
                    }
                           
                } else {
                    widget._setStyle({
                        width: width,
                        height: height
                    });
                }
            });
    }
});