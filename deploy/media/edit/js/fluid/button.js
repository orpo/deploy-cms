/**
 * @fileoverview    Fluid button classes. (Part of Fluid UI).
 * @version         1.1 (stable)
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widget) (/js/fluid/widget.js)
 * @requires        FLUID (widget) (/js/fluid/menu.js)
 */

'use strict';

/**
 * A button widget.
 * @class A button class.
 * @constructor
 */
FLUID.Button = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Helpful.call(this, options);
    FLUID.Labelled.call(this, options);
    this._type.push('Button');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Button, FLUID.Widget);
UTILS.copyPrototype(FLUID.Button, FLUID.Helpful);
UTILS.copyPrototype(FLUID.Button, FLUID.Labelled);

$$.extend(FLUID.Button.prototype, {
    __className__: 'FLUID.Button',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-click-region"></div>' + 
            '<span class="fluid-icon"></span>' +
            '<span class="fluid-label"><%=widget._label%></span>' +
        '</div>',
    __frameworkEvents__: {
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
    },
    __css__: FLUID._cssName('button'),
    
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Widget.prototype._update.apply(this, [false]);
        FLUID.Helpful.prototype._cUpdate.apply(this);
        
        // Icon
        var iconSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('icon'));
        if (this.icon() !== null) {        
            iconSelection.css('background', 
                this.icon().getBackgroundCSS());
            iconSelection.show();
        } else {
            iconSelection.hide();
        }
    
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Widget.prototype._applyStates.apply(this);
        FLUID.Labelled.prototype._cApplyStates.apply(this);
    },

    /** @private */
    _bindEvents: function () {
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        this.bind('onMouseEnter.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            // Allow text selection again
            document.body.onselectstart = function () {};
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseDown.fluid', function (event) {
            // Prevent text selection
            document.body.onselectstart = function() { return false; }
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.addClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseUp.fluid', function (event) {
            // Allow text selection again
            document.body.onselectstart = function () {};
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
    },
    
    /**
     * The default options when creating a new button.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: true,
        enabled: true,
        tooltip: '',
        helpURL: ''
    }
});


/**
 * A button that triggers a menu.
 * @class A menu button widget.
 * @constructor
 */
FLUID.MenuButton = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Button.call(this, options, false);
    this._type.push('MenuButton');
    
    /** @private */
    this._menu = null;
    this.menu(options.menu);
    
    // Add button class
    this._classes.push(FLUID._cssName('button'));
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.MenuButton, FLUID.Button);

$$.extend(FLUID.MenuButton.prototype, {
    __className__: 'FLUID.MenuButton',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-click-region"></div>' + 
            '<span class="fluid-icon"></span>' +
            '<span class="fluid-label"><%=widget._label%></span>' +
            '<span class="fluid-drop-down"></span>' +
        '</div>',
    __css__: FLUID._cssName('menuButton'),
    
    /**
     * Set/Get the menu associated with the button.
     * @param {FLUID.Menu} value (Optional) If undefined the current 
     * associated menu for the button is returned, else the buttons's 
     * associated menu is set.
     * @return The menu associated with the button.
     * @type FLUID.Menu
     */
    menu: function (value) {
        if (value === undefined) {
            return this._menu;
        }
        this.trigger('menu', function () {
            if (this._menu !== value) {
                this._menu = value;
                this._menu.owner(this);
                this._menu.bind('onBurst.fluid', function (event) {
                    this.owner().removeClass(FLUID._cssName('dropped'));
                });
            }
        }, {data: {'value': value}});
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Button.prototype._bindEvents.apply(this);
        
        this.bind('onClick.fluid', function (event) {
            if (this.enabled() && this._menu !== null) {
                this.addClass(FLUID._cssName('dropped'));
   
                // Determine the orientation & position
                var offset = this.getOffset();
                var height = this.getHTMLSelection().outerHeight(true, false);
                var menuHeight = this._menu.getPopupSize()[1];
                var screenHeight = $$(window).height();
                
                if (offset[1] + height + menuHeight > screenHeight) {
                    this._menu.style({left: offset[0], 
                        top: offset[1] - menuHeight});
                } else {
                    this._menu.style({left: offset[0], 
                        top: offset[1] + height});
                }
                                
                this._menu.popup();
            }
        });
    }
});