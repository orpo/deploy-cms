/**
 * @fileoverview    Fluid resource. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 */

'use strict';

/**
 * A resource manager for the UI. The resource manager helps cut down 
 * creation of duplicate resources (e.g. an 'Add icon'). They also make it 
 * possible to access such resource at a global scope (Provided you define
 * the resource manager as a global). Resources support the use of 
 * namespacing using the dot '.' syntax (e.g. 'add.icons') which is useful
 * for accessing and managing resources as groups.
 * @class A resource manager class. 
 * @constructor
 */    
FLUID.ResourceManager = function () {

    /*
    The underscore '_' group is a special group used when no namespace is 
    specified when setting or getting a resource.
    */
    
    /** @@ private */    
    this._resources = {'_': {}};

};

$$.extend(FLUID.ResourceManager.prototype, {
    
    __className__: 'FLUID.ResourceManager',
    
    /**
     * Set the resource in the manager.
     * @param {String} id The ID of the resource to add.
     * @param {Object} resource The resource to add to the manager.
     **/
    set: function (path, resource) {
        var id = path.split('.', 1)[0];
        
        if (id === '*') {
            throw new Error('\'*\' cannot be used as an ID.');
        }
        
        var nsObj = this._setNSObj(path);
        
        // Add the resource
        nsObj[id] = resource;
    },
    
    /**
     * Return a template for the widget using this theme.
     * @param {Class|FLUID.Widget} The class or an instance of the class
     * to return a template for.
     * @return The template for the widget.
     * @type Object
     **/
    get: function (path) {
        var id = path.split('.', 1)[0];
        var nsObj = this._getNSObj(path);
        
        if (nsObj === undefined) {
            throw new Error('Resource not found: ' + path);
        }
        
        if (id === '*') {
            id = path.split('.', 2)[1];
        }
        
        if (nsObj[id] === undefined) {
            throw new Error('Resource not found: ' + path);
        }
        
        return nsObj[id];
    },
    
    /**
     * Remove a resource from the manager.
     * @param {String} id The ID of the resource to remove.
     **/
    remove: function (path) {
        var id = path.split('.', 1)[0];
        var nsObj = this._getNSObj(path);
        
        if (nsObj === undefined) {
            return;
        }
        
        if (id === '*') {
            id = path.split('.', 2)[1];
        }
        
        delete nsObj[id];
    },
    
    /**
     * Remove all resources from the manager.
     **/
    removeAll: function () {
        this._resources = {'_': {}};
    },
    
    /** @private */
    _setNSObj: function (namespace) {
        var path = namespace.split('.');
        
        // Check for global namespace
        if (path.length === 1) {
            return this._resources._;
        }
        
        var nsObj = this._resources;
        for (var i = path.length - 1; i > 0; i--) {
            var current_namespace = path[i];
            if (nsObj[current_namespace] === undefined) {
                nsObj[current_namespace] = {};
            }
            nsObj = nsObj[current_namespace];
        }
        
        return nsObj;
    },
    
    /** @private */
    _getNSObj: function (namespace) {
        var id = namespace.split('.', 1)[0];
        var path = namespace.split('.');
        
        // Check for global namespace
        if (path.length === 1) {
            return this._resources._;
        }
        
        var nsObj = this._resources;
        var parentNsObj = this._resources;
        for (var i = path.length - 1; i > 0; i--) {
            var current_namespace = path[i];
            if (nsObj[current_namespace] === undefined) {
                return;
            } else {
                parentNsObj = nsObj;
                nsObj = nsObj[current_namespace];
            }
        }
        
        if (id === '*') {
            return parentNsObj;
        }
        
        return nsObj;
    }
});