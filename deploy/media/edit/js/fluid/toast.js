/**
 * @fileoverview    Fluid toast classes (classes that popup). (Part of Fluid 
 *                  UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widget) (/js/fluid/widget.js)
 * @requires        FLUID (text) (/js/fluid/text.js)
 */

'use strict';


/**
 * A bubble class. The purpose of the bubble class is to give users feedback 
 * relative to another widget on the interface. A bubble can be displayed 
 * either by the application or when a user mouses over a widget similar to a
 * tooltip. A bubble is not a tooltip though, it is designed for showing 
 * 'feedback' which changed based on the users behaviour, not tooltips which,
 * in general, the static.
 * Bubbles are designed to display HTML (bubble.content(...)), they do not 
 * support child widgets.
 * @class A feedback bubble.
 * @constructor
 */
FLUID.Bubble = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Popup.call(this, options);
    this._type.push('Bubble');
    
    /** @private */
    this._point = options.point || [0, 0];
    
    /** @private */
    this._content = options.content;
    
    /** @private */
    this._orientation = FLUID.Bubble.SW_ORIENT;
    
    /** @private */
    this._flags = options.flags;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Bubble, FLUID.Widget);
UTILS.copyPrototype(FLUID.Bubble, FLUID.Popup);

// Constants
FLUID.Bubble.DISPLAY_DURATION = 5000;
FLUID.Bubble.NW_ORIENT = 1;
FLUID.Bubble.NE_ORIENT = 2;
FLUID.Bubble.SW_ORIENT = 4;
FLUID.Bubble.SE_ORIENT = 8;
FLUID.Bubble.PIN = 1;

$$.extend(FLUID.Bubble.prototype, {
    __className__: 'FLUID.Bubble',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
                '<span class="fluid-label"><%=widget._content%></span>' +
                '<div class="fluid-tail"></div>' +
            '</div>',
    __css__: FLUID._cssName('bubble'),
    
    /**
     * Set/Get the content for the bubble.
     * @param {String} value (Optional) If undefined the current content for
     * the bubble is returned, else the bubble's content is set.
     * @return The content.
     * @type String
     */
    content: function (value) {
        if (value === undefined) {
            return this._content;
        }
        this.trigger('content', function () {
            if (this._content !== value) {
                this._content = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set the point at which the bubble will be tethered.
     * @param {Number} x The X position to tether the bubble to.
     * @param {Number} y The Y position to tether the bubble to.
     */
    tether: EVENT.classMethod(FLUID.Bubble, function tether() {
		var localArgs = EVENT.getArgs(arguments);
        var x = EVENT.getArg(localArgs, 0);
        var y = EVENT.getArg(localArgs, 1);
    
        // Determine the orientation of the bubble (assume NW initially)
        this._orientation = FLUID.Bubble.SW_ORIENT;
        var size = this.getPopupSize();
        
        var top = y - size[1];
        var right = x + size[0];
        var screenWidth = $$(window).width();
        if (top < 0) {
            if (right > screenWidth) {
                this._orientation = FLUID.Bubble.NE_ORIENT;
                x -= size[0];
            } else {
                this._orientation = FLUID.Bubble.NW_ORIENT;
            }
        } else {
            if (right > screenWidth) {
                this._orientation = FLUID.Bubble.SE_ORIENT;
                x -= size[0];
            }
        }
        
        this.style({left: x, top: y});
        
    }, 'tether'),
    
    /**
     * Popup the bubble.
     */
    popup: EVENT.classMethod(FLUID.Bubble, function popup() {
        switch (this._transitionState) {
            case FLUID.Popup.POPUP_STATE:
                return;
                                
            case FLUID.Popup.BURST_STATE:
                this.getHTMLSelection().stop(true, true);
                this.hide();
                break;
            
            case FLUID.Popup.DORMANT_STATE:
                if (this._parent === undefined) {
                    this.getDesktop().appendChild(this, 
                        this._owner.getDesktopGroup());
                }
                this.refresh();
                this.hide();
                break;
            
            default:
                break;
        }
        this._transitionState = FLUID.Popup.POPUP_STATE;
        
        this.getHTMLSelection().fadeIn(
            FLUID.Popup.POPUP_DURATION,
            function (event) {
                $$(this).data('fluid-widget').trigger('popupComplete', 
                    function (event) {
                        this.show();
                        // Setup timed burst
                        this.getHTMLSelection().oneTime(
                            FLUID.Bubble.DISPLAY_DURATION, 
                            'burst.fluid', 
                            function (event) {
                                if ($$(this).data('fluid-widget') !== null && $$(this).data('fluid-widget') !== undefined) {
                                    $$(this).data('fluid-widget').burst();
                                }
                            }
                        );
                    });
            });
            
    }, 'popup'),

    /**
     * Burst the bubble.
     */
    burst: EVENT.classMethod(FLUID.Bubble, function burst() {
            if (this._transitionState !== FLUID.Popup.POPUP_STATE) {
                return;
            }
            this._transitionState = FLUID.Popup.BURST_STATE;
            
            // Remove timeout burst
            if (this.exists()) {
                this.getHTMLSelection().stopTime('burst.fluid');
            }
            
            this.remove();
        }, 'burst'),
        
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
        
        // Set the class for the bubble's orientation
        this._removeClass(FLUID._cssName('north'));
        this._removeClass(FLUID._cssName('south'));
        this._removeClass(FLUID._cssName('east'));
        this._removeClass(FLUID._cssName('west'));
        
        switch (this._orientation) {
        
        case FLUID.Bubble.NW_ORIENT:
            this._addClass(FLUID._cssName('north'));
            this._addClass(FLUID._cssName('west'));
            break;
        
        case FLUID.Bubble.NE_ORIENT:
            this._addClass(FLUID._cssName('north'));
            this._addClass(FLUID._cssName('east'));
            break;
        
        case FLUID.Bubble.SW_ORIENT:
            this._addClass(FLUID._cssName('south'));
            this._addClass(FLUID._cssName('west'));
            break;
        
        case FLUID.Bubble.SE_ORIENT:
            this._addClass(FLUID._cssName('sout'));
            this._addClass(FLUID._cssName('east'));
            break;
        
        }
        
        FLUID.Widget.prototype._update.apply(this, [false]);
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    

    /**
     * Destroy the window.
     */
    destroy: function () {
     	FLUID.Widget.prototype.destroy.apply(this);
		
		// Unbind the window events
     	$$(window).unbind('resize.fluid.' + this._id);
     	$$(window).unbind('scroll.fluid.' + this._id);
        
    },

    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
		this.bind('onClick.deploy', function (event, data) {
			event.data.fwEvent.stopPropagation();
		});
		
        $$(window).bind('resize.fluid.' + this._id + ' scroll.fluid.' + this._id, {widget: this}, function (fwEvent) {
            var widget = fwEvent.data.widget;
            
            if ((FLUID.Bubble.PIN & widget._flags) === FLUID.Bubble.PIN) {
                if (widget._tether_snapshot === null) {
                    widget._tether_snapshot = [widget.style('left') , widget.style('top')];
                }
                
                var x = widget._tether_snapshot[0] - ($(window).scrollLeft() - widget._scroll_snapshot[0]);
                var y = widget._tether_snapshot[1] - ($(window).scrollTop() - widget._scroll_snapshot[1]);
                
                widget.style({left: x, top: y});
            }
        });
    },
    
    /**
     * The default options when creating a new bubble.
     * @type Object
     */
    defaultOptions: {
        content: '',
        visible: true,
        enabled: true,
        flags: 0
    }
});

/**
 * A bubble window class.
 * @class A bubble window.
 * @constructor
 */
FLUID.BubbleWindow = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Popup.call(this, options);
    this._type.push('BubbleWindow');
    
    /** @private */
    this._point = options.point || [0, 0];
    
    /** @private */
    this._orientation = FLUID.Bubble.SW_ORIENT;
    
    /** @private */
    this._flags = options.flags;
    
    /** @private */
    this._tether_snapshot = null;
    
    /** @private */
    this._scroll_snapshot = [0, 0];
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.BubbleWindow, FLUID.Widget);
UTILS.copyPrototype(FLUID.BubbleWindow, FLUID.Popup);

$$.extend(FLUID.BubbleWindow.prototype, {
    __className__: 'FLUID.BubbleWindow',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
                    '<div class="fluid-group-client">' +
                        '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +  
                    '</div>' + 
                '<div class="fluid-tail"></div>' +
            '</div>',
    __css__: FLUID._cssName('bubble-window'),
    __frameworkEvents__: {
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click'
    },
    
    /**
     * Set the point at which the bubble will be tethered.
     * @param {Number} x The X position to tether the bubble to.
     * @param {Number} y The Y position to tether the bubble to.
     */
    tether: EVENT.classMethod(FLUID.BubbleWindow, function tether() {
		var localArgs = EVENT.getArgs(arguments);
        var x = EVENT.getArg(localArgs, 0);
        var y = EVENT.getArg(localArgs, 1);
    
        // Determine the orientation of the bubble (assume NW initially)
        this._orientation = FLUID.Bubble.SW_ORIENT;
        var size = this.getPopupSize();
        
        var top = y - size[1];
        var right = x + size[0];
        var screenWidth = $$(window).width();
        if (top < 0) {
            if (right > screenWidth) {
                this._orientation = FLUID.Bubble.NE_ORIENT;
                x -= size[0];
            } else {
                this._orientation = FLUID.Bubble.NW_ORIENT;
            }
        } else {
            if (right > screenWidth) {
                this._orientation = FLUID.Bubble.SE_ORIENT;
                x -= size[0];
            }
        }
        
        this._scroll_snapshot = [$$(window).scrollLeft(), $$(window).scrollTop()];
        this.style({left: x, top: y});
        
    }, 'tether'),

    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
        
        // Set the class for the bubble's orientation
        this._removeClass(FLUID._cssName('north'));
        this._removeClass(FLUID._cssName('south'));
        this._removeClass(FLUID._cssName('east'));
        this._removeClass(FLUID._cssName('west'));
        
        switch (this._orientation) {
        
        case FLUID.Bubble.NW_ORIENT:
            this._addClass(FLUID._cssName('north'));
            this._addClass(FLUID._cssName('west'));
            break;
        
        case FLUID.Bubble.NE_ORIENT:
            this._addClass(FLUID._cssName('north'));
            this._addClass(FLUID._cssName('east'));
            break;
        
        case FLUID.Bubble.SW_ORIENT:
            this._addClass(FLUID._cssName('south'));
            this._addClass(FLUID._cssName('west'));
            break;
        
        case FLUID.Bubble.SE_ORIENT:
            this._addClass(FLUID._cssName('south'));
            this._addClass(FLUID._cssName('east'));
            break;
        
        }
        
        FLUID.Widget.prototype._update.apply(this, [false]);
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },

    /**
     * Destroy the window.
     */
    destroy: function () {
     	FLUID.Widget.prototype.destroy.apply(this);
		
		// Unbind the window events
     	$$(window).unbind('resize.fluid.' + this._id);
     	$$(window).unbind('scroll.fluid.' + this._id);
        
    },

    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
		this.bind('onClick.deploy', function (event, data) {
			event.data.fwEvent.stopPropagation();
		});
		
        $$(window).bind('resize.fluid.' + this._id + ' scroll.fluid.' + this._id, {widget: this}, function (fwEvent) {
            var widget = fwEvent.data.widget;
            
            if ((FLUID.Bubble.PIN & widget._flags) === FLUID.Bubble.PIN) {
                if (widget._tether_snapshot === null) {
                    widget._tether_snapshot = [widget.style('left') , widget.style('top')];
                }
            
                var x = widget._tether_snapshot[0] - ($(window).scrollLeft() - widget._scroll_snapshot[0]);
                var y = widget._tether_snapshot[1] - ($(window).scrollTop() - widget._scroll_snapshot[1]);
            
                widget.style({left: x, top: y});
            }
        });
    },
    
    /**
     * The default options when creating a new bubble window.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        flags: 0
    }
});


/**
 * A notifier class. The purpose of the notifier class is to give users 
 * immediate and clear feedback.
 * Notices are designed to display text (notifier.content(...)), they do not 
 * support HTML or child widgets.
 * @class A notifier.
 * @constructor
 */
FLUID.Notice = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Popup.call(this, options);
    this._type.push('Notice');
    
    /** @private */
    this._content = options.content;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Notice, FLUID.Widget);
UTILS.copyPrototype(FLUID.Notice, FLUID.Popup);

// Constants
FLUID.Notice.DISPLAY_DURATION = 2500;

$$.extend(FLUID.Notice.prototype, {
    __className__: 'FLUID.Notice',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<span class="fluid-label"><%=widget._content%></span>' + 
        '</div>',
    __css__: FLUID._cssName('notice'),
    
    /**
     * Set/Get the content for the notice.
     * @param {String} value (Optional) If undefined the current content for
     * the bubble is returned, else the notice's content is set.
     * @return The content.
     * @type String
     */
    content: function (value) {
        if (value === undefined) {
            return this._content;
        }
        this.trigger('content', function () {
            if (this._content !== value) {
                this._content = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Center the notice.
     */
    center: function () {
        var size = this.getPopupSize();
        var modal = this.getDesktop().getHTMLSelection('singleModal');
        var desktopWidth = modal.width();
        var desktopHeight = modal.height();
        
        this.style({left: (desktopWidth - size[0]) / 2,
            top: (desktopHeight - size[1]) / 2
        });
    },
        
    /**
     * Popup the notice.
     */
    popup: EVENT.classMethod(FLUID.Notice, function popup() {
        switch (this._transitionState) {
            case FLUID.Popup.POPUP_STATE:
                return;
                                
            case FLUID.Popup.BURST_STATE:
                this.getHTMLSelection().stop(true, true);
                this.hide();
                break;
            
            case FLUID.Popup.DORMANT_STATE:
                if (this._parent === undefined) {
                    this.getDesktop().appendChild(this, 'singleModal');
                }
                this.refresh();
                this.hide();
                break;
            
            default:
                break;
        }
        this._transitionState = FLUID.Popup.POPUP_STATE;
        
        this.getDesktop().startModal(FLUID.Desktop.SINGLE_MODAL);
        
        this.center();
        
        this.getHTMLSelection().fadeIn(
            FLUID.Popup.POPUP_DURATION,
            function (event) {
                $$(this).data('fluid-widget').trigger('popupComplete', 
                    function (event) {
                        this.show();
                        // Setup timed burst
                        this.getHTMLSelection().oneTime(
                            FLUID.Notice.DISPLAY_DURATION, 
                            'burst.fluid', 
                            function (event) {
                                if ($$(this).data('fluid-widget') !== undefined && 
                                    $$(this).data('fluid-widget') !== null) {
                                    
                                    $$(this).data('fluid-widget').burst();
                                }
                            }
                        );
                    });
            });
            
    }, 'popup'),    

    /**
     * Burst the notice.
     * @param {Boolean} endModal If true then end the desktop's single modal
     * mode (defaults to true). Typically you only specify false when 
     * bursting a popup widget which is owned by another popup widget. 
     */
    burst: EVENT.classMethod(FLUID.Notice, function burst() {
		var localArgs = EVENT.getArgs(arguments);
        var endModal = EVENT.getArg(localArgs, 0);
        
        if (this._transitionState !== FLUID.Popup.POPUP_STATE) {
            return;
        }
        
        // Remove timeout burst
        if (this.exists()) {
            this.getHTMLSelection().stopTime('burst.fluid');
        }
        
        this._transitionState = FLUID.Popup.BURST_STATE;
        this.remove();
        if (endModal === undefined || endModal) {
            this.getDesktop().endModal(FLUID.Desktop.SINGLE_MODAL);
        }
        
    }, 'burst'),
    
    /**
     * The default options when creating a new notice.
     * @type Object
     */
    defaultOptions: {
        content: '',
        visible: true,
        enabled: true
    }
});