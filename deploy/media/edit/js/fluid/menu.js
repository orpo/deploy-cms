/**
 * @fileoverview    Fluid menu classes. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widget) (/js/fluid/widget.js)
 * @requires        FLUID (text) (/js/fluid/text.js)
 */

'use strict';


/**
 * A base item class. Item classes are used by widgets that use a collection
 * of other widgets to provide a user with a choice, like a menu or choice 
 * button. 
 * @class An item in a collection of items.
 * @constructor
 */
FLUID.Item = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Helpful.call(this, options);
    FLUID.Labelled.call(this, options);
    this._type.push('Item');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Item, FLUID.Widget);
UTILS.copyPrototype(FLUID.Item, FLUID.Helpful);
UTILS.copyPrototype(FLUID.Item, FLUID.Labelled);

$$.extend(FLUID.Item.prototype, {
    __className__: 'FLUID.Item',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<span class="fluid-icon"></span>' +
            '<span class="fluid-label"><%=widget._label%></span>' +
        '</div>',
    __frameworkEvents__: {
        'mouseover': 'mouseOver',
        'mouseout': 'mouseOut',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
        },
    __css__: FLUID._cssName('item'),
    
    /**
     * Return true if this is the first item in a collection of items.
     * @return If this is the first item.
     * @type Boolean
     */
    first: function () {
        return this._parent.indexOfChild(this) === 0;
    },
    
    /**
     * Return true if this is the last item in a collection of items.
     * @return If this is the last item.
     * @type Boolean
     */
    last: function () {
        return this._parent.indexOfChild(this) === 
            this._parent.getChildren(this._group).length;
    },
    
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Widget.prototype._update.apply(this, [false]);
        FLUID.Helpful.prototype._cUpdate.apply(this);
        
        // Icon
        var iconSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('icon'));
        if (this.icon() !== null) {        
            iconSelection.css('background', 
                this.icon().getBackgroundCSS(16));
            iconSelection.show();
        } else {
            iconSelection.hide();
        }
    
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Widget.prototype._applyStates.apply(this);
        FLUID.Labelled.prototype._cApplyStates.apply(this);

        // First & Last
        if (this.first()) {
            this.getHTMLSelection().addClass(FLUID._cssName('first'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('first'));
        }
        
        if (this.last()) {
            this.getHTMLSelection().addClass(FLUID._cssName('last'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('last'));
        }
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        this.bind('onMouseEnter.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
            }
        });
        
        this.bind('onMouseLeave.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
            }
        });
    },
    
    /**
     * The default options when creating a new item.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: true,
        enabled: true,
        tooltip: '',
        helpURL: ''
    }
});


/**
 * A separator class. Separator classes are used to break up collections of 
 * widgets.
 * @class An separator.
 * @constructor
 */
FLUID.Separator = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Separator, FLUID.Widget);

$$.extend(FLUID.Separator.prototype, {
    __className__: 'FLUID.Separator',
    __template__: '<div id="<%=widget.getHTMLElementId()%>"></div>',
    __frameworkEvents__: {
        'click': 'click',
        },
    __css__: FLUID._cssName('separator'),
    
    /** @private */
    _bindEvents: function () {
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        this.bind('onClick.fluid', function(event) {
            // Prevents issues when the click event on a separator is passed
            // through to other widgets as if the user had clicked outside
            // the parent widget (e.g. causing a menu to close). 
            event.data.fwEvent.stopPropagation()
            event.stopPropagation();
        });
    }    
});

/**
 * A menu class.
 * @class A menu.
 * @constructor
 */
FLUID.Menu = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Popup.call(this, options);
    this._type.push('Menu');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Menu, FLUID.Widget);
UTILS.copyPrototype(FLUID.Menu, FLUID.Popup);

$$.extend(FLUID.Menu.prototype, {
    __className__: 'FLUID.Menu',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-group-client">' + 
                '<%=group_items%>' +
            '</div>' + 
        '</div>',
    __defaultClient__: 'items',
    __css__: FLUID._cssName('menu'),
    

    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Widget.prototype._applyStates.apply(this);
        
        // Is sub menu
        if (this.owner().isA('MenuItem')) {
            this.getHTMLSelection().addClass(FLUID._cssName('sub'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('sub'));
        }
    },
    
    /** @private */
    _burst: function (endModal) {
        FLUID.Popup.prototype._burst.apply(this, [endModal]);
        var children = this.getChildren();
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (child.isA('MenuItem') && child.menu() !== null && 
                    child.menu().poppedUp()) {
                child._burstSubMenu();
            }
        }
    }
});

/**
 * A menu item for a menu.
 * @class A menu item.
 * @constructor
 */
FLUID.MenuItem = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Item.call(this, options, false);
    this._type.push('MenuItem');
    
    /** @private */
    this._menu = null;
    this.menu(options.menu);
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.MenuItem, FLUID.Item);

// Constants
FLUID.MenuItem.SUB_MENU_TRIGGER = 750;

$$.extend(FLUID.MenuItem.prototype, {
    __className__: 'FLUID.MenuItem',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<span class="fluid-icon"></span>' +
            '<span class="fluid-label"><%=widget._label%></span>' +
            '<span class="fluid-drop-down"></span>' +
        '</div>',
    __css__: FLUID._cssName('menu-item'),
    
    /**
     * Set/Get the menu associated with the item.
     * @param {FLUID.Menu} value (Optional) If undefined the current 
     * associated menu for the item is returned, else the item's associated 
     * menu is set.
     * @return The menu associated with the item.
     * @type FLUID.Menu
     */
    menu: function (value) {
        if (value === undefined) {
            return this._menu;
        }
        this.trigger('menu', function () {
            if (this._menu !== value) {
                this._menu = value;
                this._menu.owner(this);
            }
        }, {data: {'value': value}});
    },

    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Widget.prototype._update.apply(this, [false]);
        FLUID.Helpful.prototype._cUpdate.apply(this);
        
        // Icon
        var iconSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('icon'));
        if (this.icon() !== null) {        
            iconSelection.css('background', 
                this.icon().getBackgroundCSS(16));
            iconSelection.show();
        } else {
            iconSelection.hide();
        }
    
        // Drop-down
        var dropDownSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('drop-down'));
        if (this.menu() !== null) {
            dropDownSelection.show();
        } else {
            dropDownSelection.hide();
        }
    
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    /** @private */
    _highlightPath: function () {
        var parent_owner = this.getParent().owner();
        while (parent_owner.typeOf('MenuItem')) {
            parent_owner.addClass(FLUID._cssName('over'));
            parent_owner = parent_owner.getParent().owner();
        }
    },

    /** @private */
    _clearPath: function () {
        var parent_owner = this.getParent().owner();
        while (parent_owner.isA('MenuItem')) {
            parent_owner.removeClass(FLUID._cssName('over'));
            parent_owner = parent_owner.getParent().owner();
        }
    },

    /** @private */
    _popupSubMenu: function () {
        // Determine the orientation & position
        var offset = this.getOffset();
        var width = this.getHTMLSelection().outerWidth(true, false);
        var height = this.getHTMLSelection().outerHeight(true, false);
        var menuSize = this._menu.getPopupSize();
        var screenWidth = $$(window).width();
        var screenHeight = $$(window).height();
        if (offset[0] + width + menuSize[0] > screenWidth) {
            this._menu.style({left: offset[0] - menuSize[0]});
        } else {
            this._menu.style({left: offset[0] + width});
        }       
        if (offset[1] + menuSize[1] > screenHeight) {
            this._menu.style({top: offset[1] + height - menuSize[1]});
        } else {
            this._menu.style({top: offset[1]});
        }
        // Burst sub-menus of any siblings        
        var siblings = this.getSiblings();
        for (var i=0; i < siblings.length; i++) {
            var sibling = siblings[i];
            if (sibling.isA('MenuItem') && 
                sibling.menu() !== null &&
                    sibling.menu().poppedUp()) {
                sibling._burstSubMenu();
            }          
        }
        // Popup the menu  
        this._menu.popup();
    },
    
    /** @private */
    _burstSubMenu: function () {
        this._menu.burst(false);
        // Burst all children
        var children = this._menu.getChildren();
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (child.isA('MenuItem') && child.menu() !== null && 
                    child.menu().poppedUp()) {
                child._burstSubMenu();
            }
        }
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Item.prototype._applyStates.apply(this);
        
        // Has sub menu
        if (this._menu === null) {
            this.getHTMLSelection().removeClass(FLUID._cssName('has-sub'));
        } else {
            this.getHTMLSelection().addClass(FLUID._cssName('has-sub'));
        }
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Item.prototype._bindEvents.apply(this, [false]);
        
        this.bind('onMouseEnter.fluid', function(event) {
            this.getHTMLSelection().stopTime('showsubmenu.menuitem.fluid');
             
            if (this.enabled() && this._parent.poppedUp()) {
                if (this._menu !== null) {
                    this.getHTMLSelection().oneTime(
                        FLUID.MenuItem.SUB_MENU_TRIGGER,
                        'showsubmenu.menuitem.fluid', 
                        function (ev) { 
                            var widget = $$(this).data('fluid-widget');
                            widget._popupSubMenu();                            
                        });
                }
                // Hide any sibling's sub-menu
                this.getHTMLSelection().oneTime(
                    FLUID.MenuItem.SUB_MENU_TRIGGER,
                    'hidesubmenu.menuitem.fluid', 
                    function (ev) {
                        var widget = $$(this).data('fluid-widget');
                        if (widget !== undefined && widget !== null) {
                            var siblings = widget.getSiblings();
                            for (var i=0; i < siblings.length; i++) {
                                var sibling = siblings[i];
                                if (sibling.isA('MenuItem') && 
                                    sibling.menu() !== null &&
                                        sibling.menu().poppedUp()) {
                                    sibling._burstSubMenu();
                                }                     
                            }
                        }
                    });
                // Mark parent as over 
                this._highlightPath();
            }            
        });
        
        this.bind('onMouseLeave.fluid', function(event) {
            this.getHTMLSelection().stopTime('showsubmenu.menuitem.fluid');
            this.getHTMLSelection().stopTime('hidesubmenu.menuitem.fluid');
            this._clearPath();
        });
        
        this.bind('onClick.fluid', function(event) {
            this.getHTMLSelection().stopTime('showsubmenu.menuitem.fluid');
            if (this.enabled() && this._parent.poppedUp()) {
                if (this._menu !== null) {
                    // Cancel the burst
                    event.stopPropagation();
                    event.data.fwEvent.stopPropagation();
                    // Popup the menu            
                    this._popupSubMenu();
                } else {
                    // Remove any open sub menu items
                    this.getParent().burst();
                    this.removeClass(FLUID._cssName('over'));
                    this._clearPath();
                }
            }
        });
    }
});

/**
 * A menu toggle item for a menu.
 * @class A menu toggle item.
 * @constructor
 */
FLUID.MenuToggleItem = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Item.call(this, options, false);
    FLUID.Toggle.call(this, options);
    this._type.push('MenuToggleItem');
    
    // Add menu item class
    this._classes.push(FLUID._cssName('menu-item'));
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.MenuToggleItem, FLUID.Item);
UTILS.copyPrototype(FLUID.MenuToggleItem, FLUID.Toggle);

$$.extend(FLUID.MenuToggleItem.prototype, {
    __className__: 'FLUID.MenuToggleItem',
    __css__: FLUID._cssName('menu-toggle-item'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
        '<span class="fluid-icon"></span>' +
        '<span class="fluid-label"><%=widget._label%></span>' +
        '<span class="fluid-toggle"></span>' +
    '</div>',
    
    /** @private */
    _highlightPath: function () {
        var parent_owner = this.getParent().owner();
        while (parent_owner.typeOf('MenuItem')) {
            parent_owner.addClass(FLUID._cssName('over'));
            parent_owner = parent_owner.getParent().owner();
        }
    },

    /** @private */
    _clearPath: function () {
        var parent_owner = this.getParent().owner();
        while (parent_owner.isA('MenuItem')) {
            parent_owner.removeClass(FLUID._cssName('over'));
            parent_owner = parent_owner.getParent().owner();
        }
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Item.prototype._applyStates.apply(this);
        FLUID.Toggle.prototype._cApplyStates.apply(this);
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Item.prototype._bindEvents.apply(this, [false]);
        
        this.bind('onMouseEnter.fluid', function(event) {
            if (this.enabled() && this._parent.poppedUp()) {
                // Hide any sibling's sub-menu
                this.getHTMLSelection().oneTime(
                    FLUID.MenuItem.SUB_MENU_TRIGGER,
                    'hidesubmenu.menuitem.fluid', 
                    function (ev) {
                        var widget = $$(this).data('fluid-widget');
                        if (widget !== undefined && widget !== null) {
                            var siblings = widget.getSiblings();
                            for (var i=0; i < siblings.length; i++) {
                                var sibling = siblings[i];
                                if (sibling.isA('MenuItem') && 
                                    sibling.menu() !== null &&
                                        sibling.menu().poppedUp()) {
                                    sibling._burstSubMenu();
                                }                     
                            }
                        }
                    });
                // Mark parent as over
                this._highlightPath();
            }            
        });
        
        this.bind('onMouseLeave.fluid', function(event) {
            this.getHTMLSelection().stopTime('hidesubmenu.menuitem.fluid');
            this._clearPath();
        });
        
        this.bind('onClick.fluid', function(event) {
            this.getHTMLSelection().stopTime('showsubmenu.menuitem.fluid');
            if (this.enabled() && this._parent.poppedUp()) {
                this.toggle();
                this.getParent().burst();
                this.removeClass(FLUID._cssName('over'));
                this._clearPath();
            }
        });
    }
});