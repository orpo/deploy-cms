/**
 * @fileoverview    Fluid (core) widgets (Part of fluid UI library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 */

'use strict';

/**
 * A user interface widget. The base widget class, all UI widgets inherit 
 * from widget class or a derivative.
 * @class A widget class.
 * @constructor
 */
FLUID.Widget = function (options, bindEvents) {
    EVENT.EventClass.call(this);
    
    if (!this._type) {
        /* 
        All widget types are derived from the base widget class. When first 
        initialised we set up the _type attribute which will hold a list of 
        all the widget types the final widget is derived from.
        */
        
        /** @private */
        this._type = ['Widget'];
    }    
    
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._id = UTILS.createId();
    
    /** @private */
    this._name = options.name;
    
    // Register the element in the global registry
    FLUID.__global_ids__[this._id] = this;
    FLUID.__global_names__[this._name] = this;
    
    /** @private */
    this._htmlElementId = FLUID_ID_PREFIX + this._id.toString();
    
    /** @private */
    this._parent = options.parent;
    
    /** @private */
    this._group = options.group;
    
    /** @private */
    this._childGroups = {};
    this._childGroups[this.__defaultClient__] = [];
    
    /** @private */
    this._enabled = options.enabled;

    /** @private */
    this._visible = options.visible;

    /** @private */
    this._style = {};
    this.style(options.style || {});
    
    /** @private */
    this._classes = options.classes || [];
    
    /** @private */
    this._data = {};
    
    /** @private */
    this._preClick = false;
    
    // Attributes that hold cached values and that should not be accessed 
    // directly via either through a public or private interface are named
    // with '_cache_' as a prefix.
    
    /** @private */
    this._cache_desktop;
    
    /** @private */
    this._cache_jQuerySelection;
    
    // Add the child to it's parents list of children
    if (this._parent) {
        this._parent.appendChild(this, this._group);
    }
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Widget, EVENT.EventClass);

// Constants
FLUID.Widget.LAYOUT_PHASE_ON = 1;
FLUID.Widget.LAYOUT_PHASE_AFTER = 2;

$$.extend(FLUID.Widget.prototype, {
    __className__: 'FLUID.Widget',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<div class="<%=FLUID_GROUP_PREFIX%>' + 
                FLUID_DEFAULT_GROUP + '">' + 
                '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +
            '</div>' +
        '</div>',
    __css__: FLUID._cssName('widget'),
    __frameworkEvents__: {},
    __defaultClient__: FLUID_DEFAULT_GROUP,

    /**
     * Return true if the element class or an element class it inherits from 
     * is of the specified type.
     * @return Whether the element class or an element class it inherits from
     * is of the specified type.
     * @type Boolean
     */
    typeOf: function (type) {
        return this._type.indexOf(type) > -1;
    },
    
    /**
     * Return true if the element class is of the specified type.
     * @return Whether the element class is of the specified type.
     * @type Boolean
     */
    isA: function (type) {
        return this.getType() === type;
    },
    
    /**
     * Return the closest parent widget matching the isA or type arguments.
     * @return The closest matching parent widget.
     * @type Widget     
     */
    closest: function (options) {
        var options = UTILS.optionsArg(options, this.defaultOptions);
        
        var parent = this;
        var match = false;
        do {
            parent = parent.getParent();
            if (parent !== undefined) {
                if (options.isA !== undefined) {
                    match = parent.isA(options.isA);
                } else if (options.typeOf) {
                    match = parent.typeOf(options.typeOf);
                }
            }
        }
        while (!match && parent !== undefined);
        
        return parent;
    },
    
    /**
     * Return the widget's ID.
     * @return The widget's ID.
     * @type String
     */
    getId: function () {
        return this._id;
    },
    
    /**
     * Return the widget's name.
     * @return The widget's name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    /**
     * Rename the widget.
     * Warning! There are few cases where it is appropriate to do this, in 
     * general, once named a widget should keep it's name.
     * @param {String} name The widgets new name.
     */
    rename: EVENT.classMethod(FLUID.Widget, 
        function rename() {
			var localArgs = EVENT.getArgs(arguments);
            var name = EVENT.getArg(localArgs, 0);
            // Remove old name
            delete FLUID.__global_names__[this._name];
            
            // Rename
            this._name = name;
            FLUID.__global_names__[this._name] = this;
        }, 'rename'),
    
    /**
     * Return the widget type.
     * @return The widget type.
     * @type String
     */
    getType: function () {
        return this._type[this._type.length - 1];
    },
    
    /**
     * Return the parent for this widget.
     * @return The widgets parent widget.
     * @type FLUID.Widget
     */
    getParent: function () {
        return this._parent;
    },
    
    /**
     * Return the group within the parent for this widget.
     * @return The widgets group.
     * @type String
     */
    getGroup: function () {
        return this._group;
    },
    
    /**
     * Return the siblings for this widget.
     * @return The widgets siblings.
     * @type Array
     */
    getSiblings: function () {
        var siblings = [];
        for (var i = 0; i < this._parent.getChildCount(this._group); i++) {
            var child = this._parent.getChildren(this._group)[i];
            if (child !== this) {
                siblings.push(child);
            }
        }
        return siblings;
    },
    
    /**
     * Return the next sibling widget.
     * @type FLUID.Widget
     */
    nextSibling: function () {
        // If this is the last child there can't be a next
        var index = this._parent.indexOfChild(this, this._group);
        if (index === -1 || 
                index === this._parent.getChildCount(this._group)) {
            return;
        }
        return this._parent.getChildren(this._group)[index + 1];
    },
    
    /**
     * Return the previous sibling widget.
     * @type FLUID.Widget
     */
    prevSibling: function () {
        // If this is the first child there can't be a previous
        var index = this._parent.indexOfChild(this, this._group);
        if (index < 1) {
            return;
        }
        return this._parent.getChildren(this._group)[index - 1];
    },    
    
    /**
     * Return a list of child groups.
     * @param {Boolean} copy If true (true by default) then return a copy of
     * the groups.
     * @return A list of child group names for the widget.
     * @type Array
     */
    getChildGroups: function (copy) {
        if (copy === false) {
            return this._childGroups;
        }
        var groups = {};
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                groups[group] = this._childGroups[group].slice(0);
            }
        }
        return groups;
    },
    
    /**
     * Return true if the specified child group exists.
     * @param {String} group The name of the child group to determine if 
     * exists.
     * @return Whether the child group exists or not.
     * @type Boolean
     */
    hasChildGroup: function(group) {
        return this._childGroups[group] !== undefined;
    },
    
    /**
     * Return the widgets children.
     * @param {String} group (Optional) The group of children to return 
     * (defaults to 'client').
     * @return The widgets children or group of children.
     * @type Array
     */
    getChildren: function (group) {
        if (group === undefined) {
            group = this.__defaultClient__;
        }
        return this._childGroups[group];
    },
    
    /**
     * Return the number of children for the container.
     * @param {String} group (Optional) The group of children to return 
     * (defaults to 'client').
     * @return The number of children for the widget or group.
     * @type Number
     */
    getChildCount: function (group) {
        if (group === undefined) {
            group = this.__defaultClient__;
        }
        return this._childGroups[group].length;    
    },
    
    /**
     * Return the index of a child within the container.
     * @return The index of the child in the container.
     * @type Number
     */
    indexOfChild: function (child) {
        if (child._group in this._childGroups) {
            return this._childGroups[child._group].indexOf(child);
        }
        return -1;
    },
    
    /**
     * Append a child on to the widget.
     * @param {FLUID.Widget} child The widget to append.
     * @param {String} group (Optional) The group to append the child to, if 
     * not specified this will default to the 'client' group.
     */
    appendChild: EVENT.classMethod(FLUID.Widget, 
        function appendChild() {
			var localArgs = EVENT.getArgs(arguments);
            var child = EVENT.getArg(localArgs, 0);
            var group = EVENT.getArg(localArgs, 1);
            
            if (group === undefined) {
                group = this.__defaultClient__;
            }
            this._insertChild(Math.max(0, this._childGroups[group].length), 
                child, group);
    }, 'appendChild'),
    
    /**
     * Insert a child into the widget.
     * @param {Number} index The index at which to insert the child.
     * @param {FLUID.Widget} child The widget to insert.
     * @param {String} group (Optional) The group to insert the child in to
     * (defaults to 'client').
     */
    insertChild: EVENT.classMethod(FLUID.Widget, 
        function insertChild() {
			var localArgs = EVENT.getArgs(arguments);
            var index = EVENT.getArg(localArgs, 0);
            var child = EVENT.getArg(localArgs, 1);
            var group = EVENT.getArg(localArgs, 2);
            
            if (group === undefined) {
                group = this.__defaultClient__;
            }
            // (Re)Set the parent
            var parent = child.getParent();
            if (parent && parent.indexOfChild(child) !== -1) {
                child.getParent().removeChild(child);
            }
            child._parent = this;
            child._group = group;
            if (this._childGroups[group] === undefined) {
                this._childGroups[group] = [];
            }
            this._childGroups[group].splice(index, 0, child);
        }, 'insertChild'),
    
    /**
     * Move a child widget within the containers stack.
     * @param {Number} index The index to move the child to.
     * @param {FLUID.Widget} child The widget to move.
     */
    moveChild: EVENT.classMethod(FLUID.Widget, 
        function moveChild() {
			var localArgs = EVENT.getArgs(arguments);
            var index = EVENT.getArg(localArgs, 0);
            var child = EVENT.getArg(localArgs, 1);
            
            // Ensure the widget is part of the child group
            var prevIndex = this.indexOfChild(child);
            if (prevIndex === -1) {
                throw new Error('Child does not reside within that group');
            }
            this._childGroups[child._group].splice(prevIndex, 1);
            // Re-insert the element
            this._childGroups[child._group].splice(index, 0, child);
        }, 'moveChild'),    
    
    /**
     * Remove self from parent.
     */
    remove: EVENT.classMethod(FLUID.Widget, 
        function remove() {        
            var parent = this._parent;
            if (parent) {
                var index = parent.indexOfChild(this);
                if (index === -1) {
                    throw new Error('Child does not reside within that group');
                }
                if (this.exists()) {
                    this.getHTMLSelection().remove();
                    this.clearHTMLSelectionCache();
                }
                parent.getChildGroups(false)[this._group].splice(index, 1);
                this._parent = undefined;
                this._group = undefined;
            }
        }, 'remove'),
    
    /**
     * Remove a child from the widget.
     * @param {FLUID.Widget} child The widget to be remove.
     */
    removeChild: EVENT.classMethod(FLUID.Widget, 
        function removeChild() {
            var localArgs = EVENT.getArgs(arguments);
			var child = EVENT.getArg(localArgs, 0);
            
            child._remove();
        }, 'removeChild'),      
    
    /**
     * Remove all children from the container.
     * @param {String} group If specified only children from the specified 
     * group are removed, else all children are removed.
     */
    removeAllChildren: function (group) {
        if (group === undefined) {
            for (group in this._childGroups) {
                if (this._childGroups.hasOwnProperty(group)) {
                    var children = this._childGroups[group].slice();
                    for (var i=0; i < children.length; i++) {
                        this.removeChild(children[i]);
                    }
                }
            }
        } else {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group].slice();
                for (var i=0; i < children.length; i++) {
                    this.removeChild(children[i]);
                }
            }
        }
    },
    
    /**
     * Return the desktop the widget belongs to.
     * @return The desktop the widget (potential via it's parent) belongs to.
     * @type FLUID.Desktop
     */
    getDesktop: function () {
        // Check for cached instance
        if (this._cache_desktop === undefined) {
            // No cached instance available find it the hard way
            var parent = this._parent;
            while (parent && !parent.isA('Desktop')) {
                parent = parent._parent;
            }
            this._cache_desktop = parent;
        }
        return this._cache_desktop;
    },
    
    /**
     * Return the desktop group the widget belongs to.
     * @return The desktop group the widget (potential via it's parent) 
     * belongs to.
     * @type String
     */
    getDesktopGroup: function () {
        // Check for cached instance
        if (this._cache_desktopGroup === undefined) {
            // No cached instance available find it the hard way
            var group = '';
            var parent = this._parent;
            while (parent && !parent.isA('Desktop')) {
                if (parent._parent.isA('Desktop')) {
                    group = parent._group;
                }
                parent = parent._parent;
            }
            this._cache_desktopGroup = group;
        }
        return this._cache_desktopGroup;
    },
    
    /**
     * Return the ID of the associated HTML element.
     * @return The HTML elements ID.
     * @type String
     */
    getHTMLElementId: function () { 
        return this._htmlElementId; 
    },
    
    /**
     * A fast way to get a (cached) JQuery selection of the widget's HTML
     * element.
     * @param {String} group (optional) The name of the group HTML element to 
     * return.
     * @return A JQuery selection of the widget's HTML element.
     * @type JQuery
     */
    getHTMLSelection: function (group) {
        if (this._cache_jQuerySelection === undefined) {
            var selection = $$('#' + this._htmlElementId);
            if (selection.length > 0) {
                this._cache_jQuerySelection = selection;
            }
        }
        if (group !== undefined && this._cache_jQuerySelection.length > 0) {
            return this._cache_jQuerySelection.find(
                '.' + FLUID._cssGroup(group)).first();
        }
        return this._cache_jQuerySelection;
    },    

    /**
     * Clear the HTML element query cache.
     */
    clearHTMLSelectionCache: function () {
        delete this._cache_jQuerySelection;
    },

    /**
     * Return the associated HTML element.
     * @param {String} group (optional) The name of the group HTML element to 
     * return.
     * @return The HTML element.
     * @type Element
     */
    getHTMLElement: function (group) { 
        var selection = this.getHTMLSelection(group);
        if (selection) {
            return selection[0];
        }
        return;
    },
    
    /**
     * Return true if the HTML element exists as part of the page.
     * @return Whether the HTML element exists as part of the page.
     * @type Boolean
     */
    exists: function () {
        return this.getHTMLElement() !== undefined;
    },
    
    /**
     * Return the offset of the widget.
     * @param {Boolean} relativeTo What to return the offset of the widget
     * relative to 'desktop', 'window', or 'parent'.
     */
    getOffset: function (relativeTo) {
        var offset = [0, 0];
        var elementOffset = {};
        switch (relativeTo) {
        case 'parent':
            var parentOffset = this.getParent().getOffset();
            var childOffset = this.getOffset();
            offset = [
                childOffset[0] - parentOffset[0], 
                childOffset[1] - parentOffset[1]
                ];
            break;
        case 'window':
            var win = this.getParent();
            while (win !== undefined && !win.isA('Window')) {
                win = this.getParent();
            }
            if (win.isA('Window')) {
                var winOffset = win.getOffset();
                var childOffset = this.getOffset();
                offset = [
                    childOffset[0] - winOffset[0], 
                    childOffset[1] - winOffset[1]
                    ];
            }
            break;
        default: // Desktop
            elementOffset = this.getHTMLSelection().offset();
            offset[0] = elementOffset.left - $$(document).scrollLeft();
            offset[1] = elementOffset.top - $$(document).scrollTop();
            break;
        }
        return offset;
    },
    
    /**
     * Set/Get a value against a widget.
     * @param {String} name The name of the value to set/get.
     * @param {Object} value (Optional) The value to store, if not specified 
     * the value is returned.
     */
    widgetData: function (name, value) {
        if (value === undefined) {
            return this._data[name];
        } else {
            this._data[name] = value;
        }
    },
  
    /**
     * Refresh the widget. This is a complete refresh of the HTML. The 
     * element is removed from the DOM and then re-added. (If called 
     * privately this will return the HTML.)
     * NOTE: This is a slow!
     * @return Private calls will return the widgets HTML.
     * @type String
     */
    refresh: EVENT.classMethod(FLUID.Widget, function refresh() {
        
        // Must have a parent to be refreshed
        if (!this._parent) {
            return;
        }
        
        // Remove any existing element from the page and clear the cached
        // version we have
        if (this.exists()) {
            this.getHTMLSelection().unbind().remove();
            this.clearHTMLSelectionCache();
        }
        
        // Generate the HTML for each of the child groups
        var templateVars = {};
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var varName = 'group_' + group;
                templateVars[varName] = '';
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    templateVars[varName] += child._refresh();
                }
            }
        }
        
        var html = this._paint(templateVars);
        
        // If the parent or previous sibling exists then the buck stops here
        if (this._parent.exists()) {
            var sibling = this.prevSibling(this._group);
            while (sibling && !sibling.exists()) {
                sibling = sibling.prevSibling(this._group);
            }
            if (sibling && sibling.exists()) {
                sibling.getHTMLSelection().after(html);
            } else {
                var selection = this._parent.getHTMLSelection(this._group);
                selection.append(html);
            }
            this._bind();
            
            // This stops the widget being shown when we're rendering
            this.getHTMLSelection().css('visibility', 'hidden');
            this._update();
            this.getHTMLSelection().css('visibility', 'visible');
            
        } else {
            return html;
        }
        
    }, 'refresh'),
  
    /**
     * Update the widget, if the widget doesn't exist then the update is not
     * performed.
     * @param {Boolean} supportExtensions If this is true (defaults to true) 
     * then the update will look for theme exentions.
     */
    update: EVENT.classMethod(FLUID.Widget, function update() {
			var localArgs = EVENT.getArgs(arguments);
            var supportExtensions = EVENT.getArg(localArgs, 0);
            
            if (!this.exists()) {
                return;
            }
         
            this.getDesktop().theme().callExtension(this,
                FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);    
            
            // Apply style
            this.getHTMLSelection().css(this._computedStyle());
            
            // Update children first
            for (var group in this._childGroups) {
                if (this._childGroups.hasOwnProperty(group)) {
                    var children = this._childGroups[group];
                    for (var i = 0; i < children.length; i++) {
                        var child = children[i];
                        child._update();
                    }
                }
            }
            
            // Apply CSS classes
            this.getHTMLSelection().attr('class', 
                FLUID._cssName('widget') + ' ' + this.__css__ + ' ' + this._classes.join(' '));
            
            this._applyStates();
            
            // Apply style
            this.getHTMLSelection().css(this._computedStyle());
            
            this.getDesktop().theme().callExtension(this,
                FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);    
            
    }, 'update'),
  
    /* State functions */  

    /**
     * Return true if the widget is enabled.
     * @Return Whether the widget is enabled.
     * @type Boolean
     */
    enabled: function() {
        if (this._enabled) {
            if (this._parent) {
                return this._parent.enabled();
            }
            return true;
        } else {
            return false;
        }
    },
    
    /**
     * Set/Get the widgets enabled/disabled state.
     * @param {Boolean} value (Optional) If undefined or true the widget is 
     * enabled, if false the widget is disabled.
     */
    enable: EVENT.classMethod(FLUID.Widget, function enable() {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        
        if (value === undefined) {
            value = true;
        }
        if (this._enabled !== value) {
            this._enabled = value;
            if (this.exists()) {
                var selection = this.getHTMLSelection();
                if (value) {
                    selection.removeClass(FLUID._cssName('disabled'));
                } else {
                    selection.addClass(FLUID._cssName('disabled'));
                }
            }
        }        
    }, 'enable'),
    
    /**
     * Return true if the widget is visible.
     * @Return Whether the widget is visible.
     * @type Boolean
     */
    visible: function() {
        return this._visible;
    },
    
    /**
     * Set the widgets visibility.
     * @param {Boolean} value (Optional) If undefined or true the widget is 
     * set to visible, else the widget is hidden.
     */
    show: EVENT.classMethod(FLUID.Widget, function show() {
        var localArgs = EVENT.getArgs(arguments);
		var value = EVENT.getArg(localArgs, 0);
        
        if (value === undefined) {
            value = true;
        }
        if (this._visible !== value) {
            this._visible = value;
            if (this.exists()) {
                var selection = this.getHTMLSelection();
                if (value) {
		            this.getHTMLSelection().removeClass(FLUID._cssName('hidden'));
                } else {
			        this.getHTMLSelection().addClass(FLUID._cssName('hidden'));
                }
            }
        }        
    }, 'show'),
    
    /**
     * Hide the widget.
     */
    hide: function () {
        this.show(false);
    },
    
    /**
     * Return true if the specified CSS class is applied to the widget.
     * @return Return true is the CSS class is applied to the widget.
     * @type Boolean
     */
    hasClass: function (className) {
        return this._classes.indexOf(className) !== -1;
    },
    
    /**
     * Add a CSS class to the widget.
     * @param {String} className The name of the class to add.
     */
    addClass: EVENT.classMethod(FLUID.Widget, function addClass() {
        var localArgs = EVENT.getArgs(arguments);
		var className = EVENT.getArg(localArgs, 0);
        
        if (!this.hasClass(className)) {
            this._classes.push(className);
            if (this.exists()) {
                this.getHTMLSelection().addClass(className);                
            }
        }
    }, 'addClass'),
    
    /**
     * Remove a CSS class from the widget.
     * @param {String} className The name of the class to remove.
     */
    removeClass: EVENT.classMethod(FLUID.Widget, 
        function removeClass() {
			var localArgs = EVENT.getArgs(arguments);
            var className = EVENT.getArg(localArgs, 0);
            if (this.hasClass(className)) {
                var index = this._classes.indexOf(className);
                this._classes.splice(index, 1);
                if (this.exists()) {
                    this.getHTMLSelection().removeClass(className);                
                }
            }
        }, 'removeClass'),
    
    /**
     * Set the style for the widget.
     * @param {String|Object} attr The attribute to set/retrieve, or an 
     * object of values to update the widgets style with.
     * @param {String} value If defined then the value is set against the 
     * style attribute specified, if it is not specified, but an attribute is
     * specified then the value for that attribute is returned.
     */
    style: function (attr, value) {
        if (!UTILS.isString(attr) || value !== undefined) {
            this._setStyle(attr, value);            
        } else {      
            if (typeof(this._style[attr]) === 'function') {
                return this._style[attr](this);
            }
            return this._style[attr];
        }
    },
    
    /**
     * Destroy the widget.
     */
    destroy: function () {
		// Destroy all children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
					child.destroy();
                }
            }
        }
        // Remove the widget
        if (this._parent) {
            this.remove();
        }
		// Remove  any events
        this.unbind();
		// Clear any global references
		delete FLUID.__global_names__[this.getId()];
    	if (FLUID.widgetByName(this.getName()) !== undefined) {
			delete FLUID.__global_names__[this.getName()];
		}
	},
    
    /** @Private */
    _paint: function (templateVars) {
        if (templateVars === undefined) {
            templateVars = {};
        }
        var template = this.__template__;
        if (this.getDesktop()) {
            template = this.getDesktop().theme().getTemplate(this);
        }
        templateVars['widget'] = this;
        return UTILS.template(template, templateVars);
    },
    
    /** @private */
    _bind: function () {
        if (!this.exists()) {
            return;
        }
        
        // Bind children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    child._bind();
                }
            }
        }
        
        // Bind object to the
        this.getHTMLSelection().data('fluid-widget', this);
        
        // Bind native events
        for (var fwEventName in this.__frameworkEvents__) {
            if (this.__frameworkEvents__.hasOwnProperty(fwEventName)) {
                var eventName = this.__frameworkEvents__[fwEventName];
                this.getHTMLSelection().unbind(fwEventName + '.fluid');
                this.getHTMLSelection().bind(fwEventName + '.fluid', 
                    function (fwEvent) {
                        var widget = $$(this).data('fluid-widget');
                        widget.trigger(widget.__frameworkEvents__[fwEvent.type], 
                            function (event) {}, {data: {fwEvent: fwEvent}});
                    });
            }
        }
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return
        }
        if (this.enabled()) {
            this.getHTMLSelection().removeClass(FLUID._cssName('disabled'));
        } else {
            this.getHTMLSelection().addClass(FLUID._cssName('disabled'));
        }
        if (this._visible) {
            this.getHTMLSelection().removeClass(FLUID._cssName('hidden'));
        } else {
            this.getHTMLSelection().addClass(FLUID._cssName('hidden'));
        }        
    },
    
    /** @private */
    _bindEvents: function () {
        
    },
    
    /** @private */
    _setStyle: function (attr, value) {
        this.trigger('style', function () {
            if (!UTILS.isString(attr)) {
                for (var key in attr) {
                    if (attr.hasOwnProperty(key)) {
                        this._style[key] = attr[key];
                        var attr_value = attr[key];
                        // If the value is a function compute it
                        if (typeof(attr_value) === 'function') {
                            attr_value = attr_value(this);
                        }
                        if (this.exists()) {
                            this.getHTMLSelection().css(key, attr_value);
                        }
                    }
                }
            } else {
                this._style[attr] = value;
                // If the value is a function compute it
                if (typeof(value) === 'function') {
                    value = value(this);
                }
                if (this.exists()) {
                    this.getHTMLSelection().css(attr, value);
                }
            }
        }, {data: {'attr': attr, 'value': value}});
    },
    
    /** @private */
    _computedStyle: function () {
        // Return a computed version of the style map
        var computedStyle = {};
        for (var style in this._style) {
            if (this._style.hasOwnProperty(style)) {
                computedStyle[style] = this.style(style);
            }
        }
        return computedStyle;
    },
    
    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true
    }
});


/**
 * A widget for displaying HTML.
 * @class A HTML widget class. 
 * @constructor
 */    
FLUID.Text = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    options._bindEvents = false;
    FLUID.Widget.call(this, options);
    this._type.push('Text');
    
    /** @private */
    this._content = options.content;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Text, FLUID.Widget);

$$.extend(FLUID.Text.prototype, {
    __className__: 'FLUID.Text',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<%=widget.content()%>' +        
        '</div>',
    __css__: FLUID._cssName('text'),
    
    /**
     * Set/Get the HTML for the HTMLView.
     * @param {String} value (Optional) If undefined the current HTML for the
     * HTMLView is returned, else the HTMLView's html is set.
     * @return The html.
     * @type String
     */
    content: function (value) {
        if (value === undefined) {
            return this._content;
        }
        this.trigger('content', function () {
            if (this._content !== value) {
                this._content = value;
                if (this.exists()) {
                    this.getHTMLSelection().html(this._content);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        content: ''
    }
});


/**
 * A compound class that makes a widget more helpful by adding a tooltip 
 * support and a context sensitive help URL. The _cBindEvent method should be
 * called within the mixer classes _bindEvent method.
 * @class A compound class to make a widget more helpful.
 * @constructor
 */
FLUID.Helpful = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._tooltip = options.tooltip;
    
    /** @private */
    this._helpURL = options.helpURL;
};

$$.extend(FLUID.Helpful.prototype, {
    /**
     * Set/Get the tooltip for the widgets.
     * @param {String} value (Optional) If undefined the current tooltip for
     * the widgets is returned, else the widgets's tooltip is set.
     * @return The tooltip.
     * @type String
     */
    tooltip: function (value) {
        if (value === undefined) {
            return this._tooltip;
        }
        this.trigger('tooltip', function () {
            if (this._tooltip !== value) {
                this._tooltip = value;
                if (this.exists()) {
                    this.getHTMLSelection().attr('title', this._tooltip);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the help URL for the widgets.
     * @param {String} value (Optional) If undefined the current help URL for
     * the widgets is returned, else the widgets's help URL is set.
     * @return The help URL.
     * @type String
     */
    helpURL: function (value) {
        if (value === undefined) {
            return this._helpURL;
        }
        this.trigger('helpURL', function () {
            this._helpURL = value;
        }, {data: {'value': value}});
    },
    
    /** @private */
    _cUpdate: function () {
        this.getHTMLSelection().attr('title', this._tooltip);
    },
    
    /** @private */
    _cBindEvents: function () {
        this.bind('onMouseEnter.fluid', function(event) {
            if (this.exists()) {
                this.getDesktop().helpContext(this);
            }
        });
        
        this.bind('onMouseLeave.fluid', function(event) {
            if (this.exists()) {
                this.getDesktop().clearHelpContext(this);
            }
        });    
    }
});


/**
 * A compound class that adds a label to the widget. The _cApplyStates method
 * should be called within the mixer classes _applyStates method.
 * @class A compound class to add a label to a widget.
 * @constructor
 */
FLUID.Labelled = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._label = options.label || '';
    
    /** @private */
    this._icon = options.icon || null;
};

$$.extend(FLUID.Labelled.prototype, {   
    /**
     * Set/Get the label for the widget.
     * @param {String} value (Optional) If undefined the current label for
     * the widget is returned, else the widget's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the icon for the widget.
     * @param {String} value (Optional) If undefined the current icon for
     * the widget is returned, else the widget's icon is set.
     * @return The widget's icon.
     * @type String
     */
    icon: function (value) {
        if (value === undefined) {
            if (typeof(this._icon) === 'string') { 
                return this.getDesktop().theme().getResources().get(
                    this._icon);
            } else {
                return this._icon;
            }
        }
        this.trigger('icon', function () {
            if (this._icon !== value) {
                this._icon = value;
                this._update();
            }
        }, {data: {'value': value}});    
    },
    
    /** @private */
    _cApplyStates: function () {
        if (!this.exists()) {
            return;
        }
        if (this.icon() !== undefined && this._label === '') {
            this.getHTMLSelection().addClass(FLUID._cssName('icon-only'));
            this.getHTMLSelection().find(
                '.' + FLUID._cssName('label')).hide();
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('icon-only'));
            this.getHTMLSelection().find(
                '.' + FLUID._cssName('label')).show();
        }
        if (this.icon() === undefined && this._label !== '') {
            this.getHTMLSelection().addClass(FLUID._cssName('label-only'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('label-only'));
        }
    }
});


/**
 * A compound class that adds popup support to a widget. 
 * @class A compound class to add popup support to a widget.
 * @constructor
 */
FLUID.Popup = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._owner = options.owner;
    
    /** @private */
    this._transitionState = FLUID.Popup.DORMANT_STATE;
};

// Constants
FLUID.Popup.DORMANT_STATE = 1;
FLUID.Popup.POPUP_STATE = 2;
FLUID.Popup.BURST_STATE = 4;
FLUID.Popup.POPUP_DURATION = 500;

$$.extend(FLUID.Popup.prototype, {   
    /**
     * Return the desktop (obtained from the owner).
     * @return The desktop.
     * @type FLUID.Desktop
     */
    getDesktop: function () {
        if (this._owner !== undefined) {
            return this._owner.getDesktop();
        }
    },
    
    /**
     * Set/Get the owner of the widget.
     * @param {FLUID.Widget} value (Optional) If undefined the current owner
     * for the widget is returned, else the widget's owner is set.
     * @return The owner of the widget.
     * @type FLUID.Widget
     */
    owner: function (value) {
        if (value === undefined) {
            return this._owner;
        }
        this.trigger('owner', function () {
            this._owner = value;
        }, {data: {'value': value}});
    },

    /**
     * Return the size of the widget.
     * @return The size of the widget.
     * @type Number
     */
    getPopupSize: function () {
        var size = [0, 0];
        if (this.exists()) {
            this.getDesktop().addClass(FLUID._cssName('measuring'));
            var clone = this.getHTMLSelection().clone();
            clone.css('visibility', 'hidden');
            clone.css('position', 'static');
            this.getDesktop().getHTMLSelection('modeless').append(clone);
            size = [$$(clone).outerWidth(true, true), 
                $$(clone).outerHeight(true, true)];
            this.getDesktop().removeClass(FLUID._cssName('measuring'));
            clone.remove();
        } else {
            this.getDesktop()._appendChild(this, 'modeless');
            this.getDesktop().addClass(FLUID._cssName('measuring'));
            this._refresh();
            this._show();
            var size = [this.getHTMLSelection().outerWidth(true, true), 
                this.getHTMLSelection().outerHeight(true, true)];
            this.getDesktop().removeClass(FLUID._cssName('measuring'));
            this._show(false);
            this._remove();
        }
        return size;
    },

    /**
     * Return true if the widget is popped up or is transitioning to that 
     * state.
     * @return Whether the widget is popped up or is transitioning to that 
     * state.
     * @type Boolean
     */
    poppedUp: function () {
        return this._transitionState === FLUID.Popup.POPUP_STATE;
    },

    /**
     * Remove the widget.
     */
    remove: function () {
        
        // @@ FIX: Shouldn't this trigger an event???
        
        this.getHTMLSelection().stop(true, true);
        FLUID.Widget.prototype._remove.apply(this);
        this._transitionState = FLUID.Popup.DORMANT_STATE;
    },
    
    /**
     * Popup the widget.
     */
    popup: EVENT.classMethod(FLUID.Popup, function popup() {
        
        switch (this._transitionState) {
            case FLUID.Popup.POPUP_STATE:
                return;
                                
            case FLUID.Popup.BURST_STATE:
                this.getHTMLSelection().stop(true, true);
                this.hide();
                break;
            
            case FLUID.Popup.DORMANT_STATE:
                if (this._parent === undefined) {
                    this.getDesktop().appendChild(this, 'singleModal');
                }
                this.refresh();
                this.hide();
                break;
            
            default:
                break;
        }
        this._transitionState = FLUID.Popup.POPUP_STATE;
        
        this.getDesktop().startModal(FLUID.Desktop.SINGLE_MODAL);
        
        this.getHTMLSelection().fadeIn(
            FLUID.Popup.POPUP_DURATION,
            function (event) {
                $$(this).data('fluid-widget').trigger('popupComplete', 
                    function (event) {
                        this.show();
                    });
            });
        
    }, 'popup'),

    /**
     * Burst the widget.
     * @param {Boolean} endModal If true then end the desktop's single modal
     * mode (defaults to true). Typically you only specify false when 
     * bursting a popup widget which is owned by another popup widget. 
     */
    burst: EVENT.classMethod(FLUID.Popup, function burst() {
            var localArgs = EVENT.getArgs(arguments);
			var endModal = EVENT.getArg(localArgs, 0);
        
            if (this._transitionState !== FLUID.Popup.POPUP_STATE) {
                return;
            }
            this._transitionState = FLUID.Popup.BURST_STATE;
            this.remove();
            if (endModal === undefined || endModal) {
                this.getDesktop().endModal(FLUID.Desktop.SINGLE_MODAL);
            }
        }, 'burst')
});

/**
 * A compound class that adds toggle support to a widget. The _cApplyStates 
 * method should be called within the mixer classes _applyStates method.
 * @class A compound class to add toggle support to a widget.
 * @constructor
 */
FLUID.Toggle = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._state = options.state;
};

$$.extend(FLUID.Toggle.prototype, {   
    
    /**
     * Return the state of the toggle widget.
     * @return The state of the toggle widget.
     * @type Boolean
     */
    getState: function () {
        return this._state;
    },
    
    /**
     * Set the toggle widget to on.
     */
    on: EVENT.classMethod(FLUID.Toggle, function on() {
        var localArgs = EVENT.getArgs(arguments);
		var child = EVENT.getArg(localArgs, 0);
        this._state = true;
        this._cApplyStates();
    }, 'on'),      
    
    /**
     * Set the toggle widget to off.
     */
    off: EVENT.classMethod(FLUID.Toggle, function off() {
        var localArgs = EVENT.getArgs(arguments);
		var child = EVENT.getArg(localArgs, 0);
        this._state = false;
        this._cApplyStates();
    }, 'off'),      
    
    /**
     * Toggle the state of the widget.
     */
    toggle: EVENT.classMethod(FLUID.Toggle, function toggle() {
		var localArgs = EVENT.getArgs(arguments);
        var child = EVENT.getArg(localArgs, 0);
        this._state = !this._state;
        this._cApplyStates();
    }, 'toggle'),
    
    /** @private */
    _cApplyStates: function () {
        if (!this.exists()) {
            return;
        }
        if (this._state) {
            this.getHTMLSelection().addClass(FLUID._cssName('on'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('on'));
        }
    }
});

$$.extend(FLUID.Toggle.prototype, {   
    
    /**
     * Return the state of the toggle widget.
     * @return The state of the toggle widget.
     * @type Boolean
     */
    getState: function () {
        return this._state;
    },
    
    /**
     * Set the toggle widget to on.
     */
    on: EVENT.classMethod(FLUID.Toggle, function on() {
		var localArgs = EVENT.getArgs(arguments);
        var child = EVENT.getArg(localArgs, 0);
        this._state = true;
        this._cApplyStates();
    }, 'on'),      
    
    /**
     * Set the toggle widget to off.
     */
    off: EVENT.classMethod(FLUID.Toggle, function off() {
		var localArgs = EVENT.getArgs(arguments);
        var child = EVENT.getArg(localArgs, 0);
        this._state = false;
        this._cApplyStates();
    }, 'off'),      
    
    /**
     * Toggle the state of the widget.
     */
    toggle: EVENT.classMethod(FLUID.Toggle, function toggle() {
		var localArgs = EVENT.getArgs(arguments);
        var child = EVENT.getArg(localArgs, 0);
        this._state = !this._state;
        this._cApplyStates();
    }, 'toggle'),
    
    /** @private */
    _cApplyStates: function () {
        if (!this.exists()) {
            return;
        }
        if (this._state) {
            this.getHTMLSelection().addClass(FLUID._cssName('on'));
        } else {
            this.getHTMLSelection().removeClass(FLUID._cssName('on'));
        }
    }
});