/**
 * @fileoverview    Fluid field widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


/**
 * The Field namespace.
 * @final
 * @type Namespace
 */  
FLUID.Field = {};

/**
 * The Field option namespace.
 * @final
 * @type Namespace
 */  
FLUID.FieldOption = {};

// Constants

// Mode
FLUID.Field.DISPLAY_MODE = 1;
FLUID.Field.SUBMIT_MODE = 2;

// State
FLUID.Field.INITIAL_STATE = 1;

// NOTE: Not all fields support these additional states, they exist to support
// validation.
FLUID.Field.CHECKING_STATE = 2;
FLUID.Field.PENDING_STATE = 4;
FLUID.Field.VALID_STATE = 8;
FLUID.Field.INVALID_STATE = 16;


/**
 * A base form field for the UI. Inherited fields should be assigned as an 
 * attribute against the Field namespace (e.g. FLUID.Field.Text = ...).
 * @class A base form field class.
 * @constructor
 */    
FLUID.Field.BaseField = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('BaseField');
    
    // Form
    this._cache_form;
    
    // Submission
    
    /** @private **/
    this._included = options.included;
    
    /** @private **/
    this._mode = FLUID.Field.SUBMIT_MODE;
    
    // Name
    
    /** @private */
    this._name = options.name;
    
    // Value
    
    /** @private */
    this._initialValue = options.value;
    
    /** @private */
    this._value = options.value;
    
    /** @private */
    this._previousValue = '';
    this._snapshot();
    
    // State

    /** @private **/
    this._state = FLUID.Field.INITIAL_STATE;
    
    /** @private **/
    this._stateMessage = '';
    
    /** @private */
    this._lastModified = null;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.BaseField, FLUID.Widget);

$$.extend(FLUID.Field.BaseField.prototype, {
    __className__: 'FLUID.Field.BaseField',
    __css__: FLUID._cssName('base-field'),
    __template__: '',
        
    /*
    NOTE: Form fields support another special attribute __inputTag__. The
    value should be a string and relate to the tag used to show the form in
    SUBMIT mode. 
    */
    __inputTag__: 'input',
    
    // Selection
  
    /**
     * Get the field's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return this.getHTMLSelection().find(this.__inputTag__).first();
    },
    
    // Form

    /**
     * Return the form this field belongs to.
     * @return the parent form.
     * @type FLUID.Form
     */
    getForm: function () {
        // Check for cached instance
        if (this._cache_form === undefined) {
            // No cached instance available find it the hard way
            var parent = this._parent;
            while (parent && !parent.isA('Form')) {
                parent = parent._parent;
            }
            this._cache_form = parent;
        }
        return this._cache_form;  
    },
    
    /**
     * Return a list of fields linked to this field.
     * @return List of linked fields.
     * @type Array 
     */
    getLinkedFields: function () {
        // We can only do this if the field has a parent
        if (this.getForm()) {
            return this.getForm().getLinkedFields(this._name);
        }
        return [];
    },
    
    // Submission
    
    /**
     * Set the fields included/excluded state.
     * @param {Boolean} value (Optional) If undefined or true the field is 
     * included in submissions, if false the field is exluded.
     */
    include: EVENT.classMethod(FLUID.Field.BaseField, function include() {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        if (value === undefined) {
            value = true;
        }
        if (this._included !== value) {
            this._included = value;
        }        
    }, 'include'),

    /**
     * Exclude the field from submissions.
     */
    exclude: function () {
        this.include(false);
    },
    
    /**
     * Return true if the field should be included in submissions.
     * @return If the field should be included in submissions. 
     * @type Boolean
     */
    included: function () {
        return this._included;
    },

    /**
     * Set/Get the fields mode.
     * @param {Number} value (Optional) If undefined the field's mode is 
     * returned, if false the field's mode is set.
     * @return The field's mode.
     * @type Number
     */
    mode: function (value) {
        if (value === undefined) {
            return this._mode;
        }
        this.trigger('mode', function () {
            if (this._mode !== value) {
                this._mode = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    // Name
    
    /**
     * Get the name for a field
     * @return The name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    // Value
    
    /**
     * Set/Get the value for the field.
     * @param {String} value (Optional) If undefined the current value for
     * the field is returned, else the field's value is set.
     * @return The value.
     * @type String
     */
    value: function (value) {
        if (value === undefined) {
            return this._getValue();
        }
        this.trigger('value', function () {
            if (this.value() !== value) {
                this._setValue(value);
                
                // Set last modified date and take a snapshot
                this._lastModified = new Date();
                this.snapshot();
            }
        }, {data: {'value': value}});
    },    
    
    /**
     * Set/Get the initial value for the field. 
     *
     * NOTE: You should not use this method to set the value of the field.
     *
     * @param {String} value (Optional) If undefined the current initial 
     * value for the field is returned, else the field's initial value is 
     * set.
     * @return The initial value.
     * @type String
     */
    initialValue: function (value) {
        if (value === undefined) {
            return this._initialValue;
        }
        this.trigger('initialValue', function () {
            if (this._initialValue !== value) {
                this._initialValue = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Give the field focus.
     */
    focus: function () {
        
    },
    
    /**
     * Reset the fields value.
     */
    reset: EVENT.classMethod(FLUID.Field.BaseField, function reset() {
        this.value(this.initialValue());
        this.state(FLUID.Field.INITIAL_STATE);
    }, 'reset'),
    
    /**
     * Take a snapshot of the current value, which is then used to determine
     * if the fields value has changed.
     */
    snapshot: EVENT.classMethod(FLUID.Field.BaseField, function snapshot() {
        this._previousValue = this.value();
    }, 'snapshot'),
    
    // State

    /**
     * Set/Get the field state.
     * @param {String} value (Optional) If undefined the current state of the
     * field is returned, else the field's state is set.
     * @return The field state.
     * @type Number
     */
    state: function (value) {
        if (value === undefined) {
            return this._getState();
        }
        this.trigger('state', function () {
            if (this._getState() !== value) {
                this._setState(value);
            }
        }, {data: {'value': value}});
    },

    /**
     * Set/Get the field state message.
     * @param {String} value (Optional) If undefined the current state 
     * message for the field is returned, else the fields state message is 
     * set.
     * @return The field state message.
     * @type String
     */
    stateMessage: function (value) {
        if (value === undefined) {
            return this._stateMessage;
        }
        this.trigger('state', function () {
            if (this._stateMessage !== value) {
                this._stateMessage = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Return true if the field is ready to be submitted.
     * @return Whether the field is in a suitable state to submit.
     * @type Boolean
     */
    ready: function () {
        return this._state === FLUID.Field.INITIAL_STATE;
    },
    
    /**
     * Return true if the field value is empty.
     * @return Whether the fields value is empty.
     * @type Boolean
     */
    empty: function () {
        return this.value() === '';
    },
    
    /**
     * Return true if the field has changed since the last snapshot was 
     * taken.
     * @return Whether the field has changed.
     * @type Boolean
     */
    changed: function () {
        return this.value() !== this._previousValue;
    },
    
    // Private
    
    /** @private */
    _getValue: function () {
        return this._value;
    },
    
    /** @private */
    _setValue: function (value) {
        this._value = value; 
        if (this.exists()) {
            this.getInputSelection().attr('value', this.value());
        }
    },    
       
    /** @private */
    _getState: function () {
        return this._state;
    },
    
    /** @private */
    _setState: function (value) {
        this._state = value;
    },    
    
    /**
     * The default options when creating a new field.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        name: '',
        value: ''
    }
});


/**
 * A compound class that adds basic and real-time interaction support to a 
 * field.
 * @class A compound class to add realtime interaction support to a field.
 * @constructor
 */
FLUID.Field.Realtime = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */ 
    this._required = options.required;
    
    /** @private */
    this._realtime = options.realtime;
    
    /** @private */
    this._bubble = new FLUID.Bubble({owner: this});
};

$$.extend(FLUID.Field.Realtime.prototype, {   
    
    /**
     * Set/Get support for real-time validation of the field's content.
     * @param {String} value (Optional) If undefined the current real-time 
     * setting for the field is returned, else the field's real-time setting
     * is set.
     * @return The real-time validation setting.
     * @type Boolean
     */
    realtime: function (value) {
        if (value === undefined) {
            return this._realtime;
        }
        this.trigger('realtime', function () {
            if (this._realtime !== value) {
                this._realtime = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Inact the realtime call for the field.
     */
    inactRealtime: EVENT.classMethod(FLUID.Field.Realtime, function inactRealtime() {

        // Set the field status to checking        
        this.state(FLUID.Field.CHECKING_STATE);
        
        // Perform the simple required tests first
        
        if (this._required && this.empty()) {
        
            // Required but no value provided
            this.state(FLUID.Field.INVALID_STATE);
            this.stateMessage('This field is required.');
            this.trigger('inactRealtimeComplete', function (event) { 
                
            });
        
        } else if (!this._required && this.empty() && (!this.realtime() || this.getForm().realtimeData()._form !== 'UPDATE' || this.getForm().realtimeData()._action !== 'SAVE')) {
        	
            // Optional condition met
            this.state(FLUID.Field.VALID_STATE);
            this.stateMessage('');
            this.trigger('inactRealtimeComplete', function (event) {
                
            });
        
        } else if (this.realtime()) {
            
            // Real-time validation
            
            // Build the request
            var requestUrl = this.getForm().realtimeURL();
            var requestData = $$.extend({}, 
                this.getForm().realtimeData());
            requestData['_name'] = this._name;
			if (this.value() === undefined || this.value() === null) {
            	requestData[this._name] = '';
			} else {
				requestData[this._name] = this.value();    
			}
            
            // Cater for linked fields
            var linkedFields = this.getLinkedFields();
            for (var i = 0; i < linkedFields.length; i++) {
                var linkedField = linkedFields[i];
                
                // If the linked field is not a hidden field then it must be 
                // ready or we have to set the results to pending.
                if (
                    !linkedField.typeOf('Field.Hidden') && 
                    (
                        linkedField.state() === FLUID.Field.INITIAL_STATE &&
                        linkedField.value() === ''
                    )
                ) {
                    this.state(FLUID.Field.PENDING_STATE);
                    return;
                }
                
				if (linkedField.value() === undefined || linkedField.value() === null) {
	            	requestData[linkedField.getName()] = '';
				} else {
					requestData[linkedField.getName()] = linkedField.value();   
				}
            }
            
            var request = new IO.Request({type: 'POST', 
                url: requestUrl,
                data: requestData});
            
            // Handle responses
            request.bind('afterSuccess', function (event, data) {
                
                var field = data.field;
                var code = event.data.data.code;
                var msg = event.data.data.msg;
                
                // Validated
                if (code === 'VALID' || code === 'SAVED') {
                   field.state(FLUID.Field.VALID_STATE);
                   
                   // Mark any linked fields as valid
                   var linkedFields = field.getLinkedFields();
                   for (var i = 0; i < linkedFields.length; i++) {
                       var linkedField = linkedFields[i];
                       linkedField.state(FLUID.Field.VALID_STATE);
                       linkedField.stateMessage('');
                   }
                   
                   // Clear the msg
                   msg = '';
					
                } else if (code === 'PENDING') {
                   field.state(FLUID.Field.PENDING_STATE);
                }
                
                field.stateMessage(msg);
                field.trigger('inactRealtimeComplete', function (event) {
                    
                });
				if (code === 'SAVED') {
					field.getForm().trigger('realtimeSendSuccess', field.getForm().realtimeSendSuccess, event);
				}
                
            }, {field: this} );
            
            request.bind('afterFail', function (event, data) {
                
                var field = data.field;
                var status = event.data.data.status;
                var msg = event.data.data.msg;
                
                // Failed to validate
                field.state(FLUID.Field.INVALID_STATE);
                field.stateMessage(msg);
                
               // Mark any linked fields as pending
               var linkedFields = field.getLinkedFields();
               for (var i = 0; i < linkedFields.length; i++) {
                   var linkedField = linkedFields[i];
                   linkedField.state(FLUID.Field.PENDING_STATE);
               }                
                
                field.trigger('inactRealtimeComplete', function (event) {
                    
                });
                            
            }, {field: this} );
            
            // Validate
            request.send();
            
        } else {
            
            // Without real-time validation the best we can establish is that
            // the field is required to contain some content and does. It is 
            // valid pending server-side validation.
            
            this.state(FLUID.Field.PENDING_STATE);
            this.stateMessage('');
            this.trigger('inactRealtimeComplete', function (ev) { });
        }
    }, 'inactRealtime'),
    
    /**
     * Return true if the field is ready to be submitted.
     * @return Whether the field is in suitable state to submit.
     * @type Boolean
     */
    ready: function () {
        this._syncValue();
        if (this._required && this.empty()) {
            return false;
        }
        var state = this.state();
        return state === FLUID.Field.INITIAL_STATE || 
            state === FLUID.Field.PENDING_STATE ||
            state === FLUID.Field.VALID_STATE;
    },
    
    // Private
    
    /** @private */
    _setState: function (value) {
        
        // Remove existing state styles...
        this.removeClass('fluid-state-checking');
        this.removeClass('fluid-state-pending');
        this.removeClass('fluid-state-valid');
        this.removeClass('fluid-state-invalid');
        
        // Set the state
        this._state = value;

        // Update the state style
        switch (this._getState()) {
        
        case FLUID.Field.CHECKING_STATE:
            this.addClass('fluid-state-checking');
            break;
            
        case FLUID.Field.PENDING_STATE:
            this.addClass('fluid-state-pending');
            break;
            
        case FLUID.Field.VALID_STATE:
            this.addClass('fluid-state-valid');
            break;
            
        case FLUID.Field.INVALID_STATE:
            this.addClass('fluid-state-invalid');
            break;
        }
    },
    
    /** @private */
    _showBubble: function () {
        if (this._bubble.poppedUp()) {
            return;
        }

        if (this.stateMessage() === '') {
            return;
        }
            
        // Update message
        this._bubble.content(this.stateMessage());
        
        // Position the bubble
        var statusElement = this.getHTMLSelection().children(
            '.' + FLUID._cssName('status'));
        var width = $$(statusElement).outerWidth(true, true);
        var height = $$(statusElement).outerHeight(true, true);
        var position = $$(statusElement).position();
        var offset = this.getOffset();
        
        this._bubble.tether(
            offset[0] + position.left + (width / 2),
            offset[1] + position.top + (height / 2)
            );
        this._bubble.popup();
    }
    
});


/**
 * A base data entry field for the UI.
 * @class A base data entry field class.  
 * @constructor
 */    
FLUID.Field.Entry = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.BaseField.call(this, options, false);
    this._type.push('Field.Entry');

    /** @private */
    
    /** @private */
    this._label = options.label;

    /** @private */
    this._placeholder = options.placeholder;

    /** @private */
    this._autofocus = options.autofocus;

    /** @private */
    this._autocomplete = options.autocomplete;
    
    // Add the additional CSS class to child classes
    this._classes.push(FLUID._cssName('field'));
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Entry, FLUID.Field.BaseField);

$$.extend(FLUID.Field.Entry.prototype, {
    __className__: 'FLUID.Field.Entry',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="text" name="<%=widget.getName()%>" value="<%=widget.value()%>" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-entry'),
    __frameworkEvents__: {
        'blur': 'blur',
        'change': 'change',
        'focus': 'focus',
        'keydown': 'keyDown',
        'keypress': 'keyPress',
        'keyup': 'keyUp',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter'
        },

    /**
     * Get the fields's HTML label element as a selection.
     * @return A JQuery selection of the field's HTML label element.
     * @type JQuery
     */
    getLabelSelection: function () {
        return this.getHTMLSelection().find('label').first();
    },
    
	/**
	 * Return true if the field has focus.
	 * @return Whether the field has focus.
	 * @type Bool
	 */
	hasFocus: function () {
		return this.getInputSelection().is(':focus');
	},

    /**
     * Give the field focus.
     */
    focus: function () {
        this.getInputSelection().focus();
    },
    
    /**
     * Set/Get the label for the entry field.
     * @param {String} value (Optional) If undefined the current label for
     * the entry field is returned, else the entry fields's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this.getLabelSelection().html(this._label);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the field's placeholder.
     * @param {String} value (Optional) If undefined the current placeholder 
     * value for the field is returned, else the field's placeholder value is
     * set.
     * @return The placeholder value.
     * @type String
     */
    placeholder: function (value) {
        if (value === undefined) {
            return this._placeholder;
        }
        this.trigger('placeholder', function () {
            if (this._placeholder !== value) {
                this._placeholder = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the field's autocomplete setting.
     * @param {Boolean} value (Optional) If undefined the current autocomplete 
     * setting is returned, else the setting is set.
     * @return The autcomplete setting.
     * @type Boolean
     */
    autocomplete: function (value) {
        if (value === undefined) {
            return this._autocomplete;
        }
        this.trigger('autcomplete', function () {
            if (this._autocomplete !== value) {
                this._autocomplete = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the field's autofocus setting.
     * @param {Boolean} value (Optional) If undefined the current autofocus 
     * setting is returned, else the setting is set.
     * @return The autofocus setting.
     * @type Boolean
     */
    autofocus: function (value) {
        if (value === undefined) {
            return this._autofocus;
        }
        this.trigger('autofocus', function () {
            if (this._autofocus !== value) {
                this._autofocus = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Return true if the field is ready to be submitted.
     * @return Whether the field is in suitable state to submit.
     * @type Boolean
     */
    ready: function () {
        this._syncValue();
        return this.state() === FLUID.Field.INITIAL_STATE;
    },
    
    /** @private */
    _syncValue: function () {
        this._value = this.getInputSelection().val();
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
        this.bind('onFocus.fluid', function(event) {
            this._syncValue();
            this.snapshot();
        });
        
        this.bind('onBlur.fluid', function(event) {
            this._syncValue();
        });
    },    
    
    /** @private */
    _bind: function () {
        
        // NOTE: We have to custom bind the events because for entry fields 
        // because the events are bound against the input element not the 
        // outer division.
        
        if (!this.exists()) {
            return;
        }
        
        // Bind children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    child._bind();
                }
            }
        }
        
        // Bind object to the
        this.getHTMLSelection().data('fluid-widget', this);
        this.getInputSelection().data('fluid-widget', this);
    
        // Bind native events
        for (var fwEventName in this.__frameworkEvents__) {
            if (this.__frameworkEvents__.hasOwnProperty(fwEventName)) {
                var eventName = this.__frameworkEvents__[fwEventName];
                this.getInputSelection().unbind(fwEventName + '.fluid');
                this.getInputSelection().bind(fwEventName + '.fluid', 
                    function (fwEvent) {
                        var widget = $$(this).data('fluid-widget');
                        widget.trigger(widget.__frameworkEvents__[fwEvent.type], 
                            function (event) {
                        }, {data: {fwEvent: fwEvent}});
                    });
            }
        }    
    },      
    
    /**
     * The default options when creating a new entry field.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: ''
    }
});


/**
 * A field option (used for choice type fields such as selects, radios and 
 * check boxes).
 * @class A field option class.
 * @constructor
 */    
FLUID.FieldOption.Option = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('FieldOption.Option');
    
    /** @private */
    this._label = options.label;
    
    /** @private */
    this._value = options.value;
    
    /** @private */
    this._selected = options.selected;

    /** @private */
    this._cache_field;

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.FieldOption.Option, FLUID.Widget);

$$.extend(FLUID.FieldOption.Option.prototype, {
    __className__: 'FLUID.FieldOption.Option',
    __template__: '<option id="<%=widget.getHTMLElementId()%>" ' + 
        'name="<%=widget.getField().getName()%>" ' + 
        'value="<%=widget.value()%>" ' +
        '<% if (widget.selected()) { %>selected="selected"<% } %>>' + 
        '<%=widget.label()%>' + 
        '</option>',
    __css__: FLUID._cssName('field-option-option'),
    
    /**
     * Get the field this option is associated with.
     * @return The associated field for this option.
     * @type FLUID.Field
     */
    getField: function () {
        // Check for cached instance
        if (this._cache_field === undefined) {
            // No cached instance available find it the hard way
            var parent = this.getParent();
            while (parent && !parent.typeOf('BaseField')) {
                parent = parent.getParent();
            }
            this._cache_field = parent;
        }
        return this._cache_field;
    },

    /**
     * Set/Get the label for the field option.
     * @param {String} value (Optional) If undefined the current label for
     * the field option is returned, else the field option's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
        
    /**
     * Set/Get the value for the field option.
     * @param {String} value (Optional) If undefined the current value for
     * the field option is returned, else the field option's value is set.
     * @return The value.
     * @type String
     */
    value: function (value) {
        if (value === undefined) {
            return this._value;
        }
        this.trigger('value', function () {
            if (this._value !== value) {
                this._value = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },

    /**
     * Select the option.
     * @param {Boolean} value (Optional) If specified as false the option is
     * unselected, if not specified or specified as true the option is
     * selected.
     */
    select: EVENT.classMethod(FLUID.FieldOption.Option, function select(value) {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        if (value === undefined) {
            value = true;
        }
        this._selected = value;
        this._update();
    }, 'select'),
        
    /**
     * Return true if the option is selected.
     * @return Whether the option is selected.
     * @type Boolean
     */
    selected: function () {
        return this._selected;
    }
});