/**
 * @fileoverview    Fluid form widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


/**
 * A form for the UI.
 * @class A form class.  
 * @constructor
 */    
FLUID.Form = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('Form');
    
    // Groups
    this._childGroups[this.__defaultClient__] = [];
    
    // Submission
    
    /** @private */
    this._action = options.action;
    
    /** @private */
    this._method = options.method;
    
    /** @private */
    this._submitMode = options.submitMode;
    
    // Real-time Validation
    
    /** @private */
    this._realtimeURL = options.realtimeURL;
    
    /** @private */
    this._realtimeData = $$.extend({}, options.realtimeData || {});
    
    // Fields
    
    /** @private **/
    this._linkedFields = {};
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Form, FLUID.Widget);

// Constants
FLUID.Form.SINGLE_SUBMIT_MODE = 1;
FLUID.Form.ALL_SUBMIT_MODE = 2;

$$.extend(FLUID.Form.prototype, {
    __className__: 'FLUID.Form',
    __template__:  '<form id="<%=widget.getHTMLElementId()%>" ' + 
                'action="<%=widget._action%>" ' + 
                'method="<%=widget._method%>"' + 
                'novalidate>' +
                    '<div class="fluid-group-client">' + 
                        '<%=group_client%>' +
                    '</div>' +
                    // A submit but is required to allow the form to be
                    // submitted - typically CSS is used to hide this 
                    // (by positioning off screen - display:none wont work).
                    '<input type="submit" name="submit" value="Submit" />' +
					'<div class="fluid-clear"></div>' +
            '</form>',
    __frameworkEvents__: {
        'submit': 'submit'
        },
    __css__: FLUID._cssName('form'),
    
    // Submissions
    
    /**
     * Set/Get the action for the form.
     * @param {String} value (Optional) If undefined the current action for
     * the form is returned, else the form's action is set.
     * @return The action.
     * @type String
     */
    action: function (value) {
        if (value === undefined) {
            return this._action;
        }
        this.trigger('action', function () {
            if (this._action !== value) {
                this._action = value;
                if (this.exists()) {
                    this.getHTMLSelection().attr('action', this._action);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the method for the form.
     * @param {String} value (Optional) If undefined the current method for
     * the form is returned, else the form's method is set.
     * @return The method.
     * @type String
     */
    method: function (value) {
        if (value === undefined) {
            return this._method;
        }
        this.trigger('method', function () {
            if (this._method !== value) {
                this._method = value;
                if (this.exists()) {
                    this.getHTMLSelection().attr('method', this._method);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the submit mode for the form.
     * @param {Number} value (Optional) If undefined the current submit mode
     * for the form is returned, else the form's submit type is set.
     * @return The submit mode.
     * @type Number
     */
    submitMode: function (value) {
        if (value === undefined) {
            return this._submitMode;
        }
        this.trigger('submitMode', function () {
            if (this._submitMode !== value) {
                this._submitMode = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    // Validation
    
    /**
     * Set/Get the real-time URL for the form.
     * @param {String} value (Optional) If undefined the current real-time
     * URL for the form is returned, else the form's real-time URL is set.
     * @return The real-time URL.
     * @type String
     */
    realtimeURL: function (value) {
        if (value === undefined) {
            return this._realtimeURL;
        }
        this.trigger('realtimeURL', function () {
            if (this._realtimeURL !== value) {
                this._realtimeURL = value;
            }
        }, {data: {'value': value}});
    },

    /**
     * Set/Get the real-time data for the form.
     * @param {Object} value (Optional) If undefined the current real-time
     * data for the form is returned, else the form's real-time data is set.
     *
     * NOTE: The real-time data is a way of attaching additional parameters to
 	 * the real-time request when it is fired.
     * 
     * @return The real-time URL.
     * @type Object
     */
    realtimeData: function (value) {
        if (value === undefined) {
            // We only return a copy as direct manipulation of these values
            // is (probably) a bad idea.
            return $$.extend({}, this._realtimeData);
        }
        this.trigger('realtimeData', function () {
            if (this._realtimeData !== value) {
                this._realtimeData = value;
            }
        }, {data: {'value': value}});
    },
    
    // Fields
    
    /**
     * Return the fields for the form.
     * @param {Boolean} included If specified as true then only fields which
     * are included in the form submission are returned.
     * @return The fields for the form.
     * @type Array
     */
    getFields: function (included) {
        var fields = [];
        var fieldsets = this.getChildren('client');
        for (var i = 0; i < fieldsets.length; i++) {
            var fieldset = fieldsets[i];
            if (fieldset.isA('Fieldset')) {
                var fieldsetFields = fieldset.getChildren('client');
                for (var j = 0; j < fieldsetFields.length; j++) {
                    var field = fieldsetFields[j];
                    if (field.typeOf('BaseField')) {
                        if (included === false || field.included()) {
                            fields.push(field);
                        }
                    }
                }
            }
        }
        return fields;
    },
    
    /**
     * Return a map of all fields associated with the form.
     * @param {Boolean} included If specified as true then only fields which
     * are included in the form submission are returned.
     * @return The fields for the form.
     * @type Object
     */
    getFieldMap: function (included) {
        var fields = this.getFields(included);
        var fieldMap = {};
        for (var i = 0; i < fields.length; i++) {
            fieldMap[fields[i].getName()] = fields[i];
        }
        return fieldMap;
    },
    
    /**
     * If the form is in single submit mode then the function returns a list 
     * containing the field currently being updated, otherwise it returns all
     * included fields.
     * @return The send fields for the form.
     * @type Array
     */
    getSendFields: function () {
        var fields = this.getFields();
        if (this.submitMode === FLUID.Form.SINGLE_SUBMIT_MODE) {
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                if (field.mode() === FLUID.Field.SUBMIT_MODE) {
                    // Only one field can be edited in single mode so once we 
                    // find the field return immediately.
                    return field;
                }
            }
            return;
        }
        return fields;
    },

    /**
     * Link a field to one or more other fields, when a field is submitted on 
     * its own, such as on single submissions or real-time validation, each 
     * of its linked fields are also submitted.
     * @param {String} fieldName The name of the field to link to other
     * fields.
     * @param {Array} linkedFieldNames The names of the fields to link to.
     */
    linkField: EVENT.classMethod(FLUID.Form, function linkField(fieldName, linkedFieldNames) {
        
        var localArgs = EVENT.getArgs(arguments);
        var fieldName = EVENT.getArg(localArgs, 0, true);
        var linkedFieldNames = EVENT.getArg(localArgs, 1, true);
        var fieldMap = this.getFieldMap();
        
        // Check if a linked map already exists for the specified field
        if (this._linkedFields[fieldName] === undefined) {
            this._linkedFields[fieldName] = {};
        }
        
        // Link each field 
        for (var i = 0; i < linkedFieldNames.length; i++) {
            var linkedFieldName = linkedFieldNames[i];
            
            // Check if a linked map already exists for the field being 
            // linked to
            if (this._linkedFields[linkedFieldName] === undefined) {
                this._linkedFields[linkedFieldName] = {};
            }
            
            // Link the fields
            this._linkedFields[linkedFieldName][fieldName] = true;
            this._linkedFields[fieldName][linkedFieldName] = true;
        }
    }, 'linkField'),    
    
    /**
     * Unlink a field from one or more fields.
     * @param {String} fieldName The name of the field to unlink.
     * @param {Array} linkedFieldNames (Optional) The names of the fields to
     * unlink from, if not specified (default) the field is unlinked from all
     * fields.
     */
    unlinkField: EVENT.classMethod(FLUID.Form, 
        function unlinkField(fieldName, linkedFieldNames) {
        
		var localArgs = EVENT.getArgs(arguments);
        var fieldName = EVENT.getArg(localArgs, 0);
        var linkedFieldNames = EVENT.getArg(localArgs, 1);

        if (this._linkedFields[fieldName] !== undefined) {
            
            // If no linked field names where specified then we default to 
            // all linked fields for the specified field
            if (linkedFieldNames !== undefined) {
                linkedFieldNames = this._linkedFields[fieldName];
            }
            
            // Unlink fields
            for (i = 0; i < linkedFieldNames.length; i++) {
                var linkFieldName = linkedFieldNames[i];
                if (this._linkedFields[linkFieldName][fieldName] !== undefined) {
                    delete this._linkedFields[linkFieldName][fieldName];
                }  
                if (this._linkedFields[fieldName][linkFieldName] !== undefined) {
                    delete this._linkedFields[fieldName][linkFieldName];
                }                     
            }                           
        }       
    }, 'unlinkField'),
    
    /**
     * Return a list of fields linked to a field.
     * @param {String} fieldName The name of the field to return the linked
     * fields for.
     * @return The linked fields.
     * @type Array
     */
    getLinkedFields: function (fieldName) {
        
        // If the field name specified has no linked field map then we just
        // return an empty array.
        if (this._linkedFields[fieldName] === undefined) {
            return [];
        }
        
        // Build the array of link fields
        var fields = [];
        var fieldMap = this.getFieldMap();
        for (var fieldName in this._linkedFields[fieldName]) {
            if (this._linkedFields.hasOwnProperty(fieldName)) {
                fields.push(fieldMap[fieldName]);
            }
        }
        return fields;
    },
    
    /**
     * Get the data currently held against the form.
     */
    getData: function () {
        var fieldData = {};
        var fields = this.getSendFields();
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            
            // Check the field ready to be submitted
            if (!field.ready()) {
                continue;
            }
            
            // Get the field value
            var value = field.value();
            
            // If it's defined add it to the field data
            if (value !== undefined) {
                fieldData[field.getName()] = field.value();
            }
        }
        return fieldData;
    },
    
    // Send/Reset
    
    /**
     * Validate the form as it it's about to be sent but don't send it.
     */
    puesdoSend: EVENT.classMethod(FLUID.Form, function puesdoSend() {
        
        // Check all 'included' fields are ready to be sent
        var fieldData = {};       
        var fields = this.getSendFields();
        
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            
            // Check the field ready to be submitted
            if (!field.ready() && !field.hasFocus()) {
                this.trigger('sendNotReady', 
                    function (event) {  }, 
                    {data: {field: field}});
                return false;
            }
            
            // Get the field value
            var value = field.value();
            
            // If it's defined add it to the field data
            if (value !== undefined) {
                fieldData[field.getName()] = field.value();
            }
        }    
            
        // If we got this far then in principle the send was successful
        this.trigger('sendSuccess', function (event) { });
    
    }, 'puesdoSend'),
    
    /**
     * Send (submit) the form. 
     */
    send: EVENT.classMethod(FLUID.Form, function send() {
        
        // Check all 'included' fields are ready to be sent
        var fieldData = {};       
        var fields = this.getSendFields();
        
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            
            // Check the field ready to be submitted
            if (!field.ready() && !field.hasFocus()) {
                this.trigger('sendNotReady', 
                    function (event) {  }, 
                    {data: {field: field}});
                return false;
            }
            
            // Get the field value
            var value = field.value();
            
            // If it's defined add it to the field data
            if (value !== undefined) {
                fieldData[field.getName()] = field.value();
            }
        }

        // Create the request to submit the form
        var request = new IO.Request({
            type: 'POST',
            url: this._action,
            data: fieldData
            });
        
        // Manage the various response to the submission
        request.bind('afterSuccess.fluid', function (event, data) { 
            data.form.trigger('sendSuccess', function (event) { }, event);
        }, {form: this, fields: fields});
        
        request.bind('afterFail.fluid', function (event, data) {
            data.form.trigger('sendFail', function (event) { }, event);
        }, {form: this, fields: fields});
        
        // Send the request
        request.send();        
        
    }, 'send'),

    /**
     * Reset all fields in the form.
     */
    reset: EVENT.classMethod(FLUID.Form, function reset() {
        var fields = this.getFields();
        for (var i = 0; i < fields.length; i++) {
            var field = fields[i];
            field.reset();
        }
    }, 'reset'),

	/**
	 * Trigger function called on a successful realtime send.
	 */
    realtimeSendSuccess: function (event, data) {
	
	},

    // Private
    
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
        // The form will call the send function when submitted
        this.bind('onSubmit.fluid', function(event) {
            
            // Prevent the default submit call
            event.data.fwEvent.preventDefault();
            
            // Send the form
            this.send();
            
        });
    },
    
    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        action: '',
        method: 'GET',
        submitMode: FLUID.Form.ALL_SUBMIT_MODE,
        realtimeUrl: ''
    }
});