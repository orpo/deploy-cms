/**
 * @fileoverview    Fluid text field widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */ 

'use strict';


/**
 * A hidden form field.
 * @class A hidden form field class.  
 * @constructor
 */    
FLUID.Field.Hidden = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.BaseField.call(this, options, false);
    this._type.push('Field.Hidden');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Hidden, FLUID.Field.BaseField);

$$.extend(FLUID.Field.Hidden.prototype, {
    __className__: 'FLUID.Field.Hidden',
    __template__: '<input id="<%=widget.getHTMLElementId()%>" type="hidden" name="<%=widget.getName()%>" value="<%=widget.value()%>" />',
    __css__: FLUID._cssName('field-hidden'),

    /**
     * Get the field's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return this.getHTMLSelection();
    },
    
    /**
     * Return true if the field is ready to be submitted.
     * @return Whether the field is in suitable state to submit.
     * @type Boolean
     */
    ready: function () {
        var state = this.state();
        return state === FLUID.Field.INITIAL_STATE || 
            state === FLUID.Field.PENDING_STATE ||
            state === FLUID.Field.VALID_STATE;
    }
});


/**
 * A display only form field.
 * @class A display only form field class.  
 * @constructor
 */    
FLUID.Field.DisplayOnly = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.BaseField.call(this, options, false);
    this._type.push('Field.DisplayOnly');
    
    /** @private */
    this._label = options.label;
    
    // Add the additional CSS class to child classes
    this._classes.push(FLUID._cssName('field'));

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.DisplayOnly, FLUID.Field.BaseField);

$$.extend(FLUID.Field.DisplayOnly.prototype, {
    __className__: 'FLUID.Field.DisplayOnly',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%></label>' +
            '<div id="<%=widget.getHTMLElementId()%>-input" class="fluid-puesdo-input"><% if (widget.value()) { %><%=widget.value()%><% } else { %><span class="fluid-empty">Not specified</span><% } %></div>' +
        '</div>',
    __css__: FLUID._cssName('field-display-only'),
    
    /**
     * Get the field's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return this.getHTMLSelection().find('.fluid-puesdo-input').first();
    },

    /**
     * Set/Get the label for the display only field.
     * @param {String} value (Optional) If undefined the current label for
     * the display only field is returned, else the display only fields's 
     * label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this.getLabelSelection().html(this._label);
                }
            }
        }, {data: {'value': value}});
    },
    
    /** @private */
    _setValue: function (value) {
        this._value = value;
        if (this.exists()) {
            if (this._value) {
                this.getInputSelection().html(this._value);
            } else {
                this.getInputSelection().html('<span class="fluid-empty">Not specified</span>');
            }
        }
    }
});


/**
 * A single line text entry field.
 * @class A single line text entry field class.
 * @constructor.
 */
FLUID.Field.Text = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Entry.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('Field.Text');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Text, FLUID.Field.Entry);
UTILS.copyPrototype(FLUID.Field.Text, FLUID.Field.Realtime);

$$.extend(FLUID.Field.Text.prototype, {
    __className__: 'FLUID.Field.Text',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="text" name="<%=widget.getName()%>" value="<%=widget.value()%>" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-text'),
    
    /** @private */
    _bindEvents: function () {
        
        this.bind('onKeyDown.fluid', function (event) {
            // If some hits enter or return and submits the form we need set
            // the fields state to pending because we want to be able to 
            // submit the form with a return key without waiting for the 
            // field to inact the realtime call.
            if (event.data.fwEvent.keyCode == 13) {
                this._syncValue();
                this.state(FLUID.Field.PENDING_STATE);
                this.inactRealtime();
            }
        });

        this.bind('onFocus.fluid', function (event) {
            this._syncValue();
            this.snapshot();
            this._showBubble();
        });
        
        this.bind('afterChange.fluid', function(event) {
            this._syncValue();
        });
        
        this.bind('onBlur.fluid', function (event) {
            this._syncValue();
            this._bubble.burst();
            
            if (this.changed() || this.state() === FLUID.Field.PENDING_STATE) {
                this.inactRealtime();
            }
        });
         
        this.bind('onMouseEnter.fluid', function (event) {
            this._showBubble();
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            this._bubble._burst();
        });       
    }
});


/**
 * An email field.
 * @class An email field class.
 * @constructor.
 */
FLUID.Field.Email = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Email');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Email, FLUID.Field.Text);

$$.extend(FLUID.Field.Email.prototype, {
    __className__: 'FLUID.Field.Email',
    __css__: FLUID._cssName('field-email'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="email" name="<%=widget.getName()%>" value="<%=widget.value()%>" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>'
});


/**
 * A number field.
 * @class A number field class.
 * @constructor.
 */
FLUID.Field.Number = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Number');
    
    /* @private */
    this._min = options.min || null;
    this._max = options.max || null;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Number, FLUID.Field.Text);

$$.extend(FLUID.Field.Number.prototype, {
    __className__: 'FLUID.Field.Number',
    __css__: FLUID._cssName('field-number'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="text" name="<%=widget.getName()%>" value="<%=widget.value()%>" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',

    /**
     * Set/Get the min value for the field.
     * @param {Number} value (Optional) If undefined the current min value for
     * the field is returned, else the field's min value is set.
     * @return The min value.
     * @type Number
     */
    min: function (value) {
        if (value === undefined) {
            return this._min;
        }
        this.trigger('min', function () {
            if (this._min !== value) {
                this._min = value;
                if (this.exists()) {
                    if (this._min === null) {
                        this.getInputSelection().removeAttr('min');
                    } else {
                        this.getInputSelection().attr('min', this._min);
                    }
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the max value for the field.
     * @param {Number} value (Optional) If undefined the current max value for
     * the field is returned, else the field's max value is set.
     * @return The max value.
     * @type Number
     */
    max: function (value) {
        if (value === undefined) {
            return this._max;
        }
        this.trigger('max', function () {
            if (this._max !== value) {
                this._max = value;
                if (this.exists()) {
                    if (this._max === null) {
                        this.getInputSelection().removeAttr('max');
                    } else {
                        this.getInputSelection().attr('max', this._max);
                    }
                }
            }
        }, {data: {'value': value}});
    }
});


/**
 * A currency field.
 * @class A currency field class.
 * @constructor.
 */
FLUID.Field.Currency = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Currency');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Currency, FLUID.Field.Text);

$$.extend(FLUID.Field.Currency.prototype, {
    __className__: 'FLUID.Field.Currency',
    __css__: FLUID._cssName('field-currency')
});


/**
 * A text area field.
 * @class A text area field class.
 * @constructor.
 */
FLUID.Field.Textarea = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Textarea');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Textarea, FLUID.Field.Text);

$$.extend(FLUID.Field.Textarea.prototype, {
    __className__: 'FLUID.Field.Textarea',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<textarea id="<%=widget.getHTMLElementId()%>-input" name="<%=widget.getName()%>" placeholder="<%=widget.placeholder()%>"><%=widget.value()%></textarea>' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-textarea'),
    __inputTag__: 'textarea',
    
    /** @private */
    _setValue: function (value) {
        this._value = value;
        if (this.exists()) {
            this.getInputSelection().val(value);
        }
    },
    
    _bindEvents: function () {

        this.bind('onFocus.fluid', function (event) {
            this._syncValue();
            this.snapshot();
            this._showBubble();
        });
        
        this.bind('afterChange.fluid', function(event) {
            this._syncValue();
        });
        
        this.bind('onBlur.fluid', function (event) {
            this._syncValue();
        
            this._bubble.burst();
            
            if (this.changed()) {
                this.inactRealtime();
            }
        });
         
        this.bind('onMouseEnter.fluid', function (event) {
            this._showBubble();
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            this._bubble.burst();
        });       
    }
});


/**
 * A password entry field.
 * @class A password text entry field class.
 * @constructor.
 */
FLUID.Field.Password = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Password');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Password, FLUID.Field.Text);

$$.extend(FLUID.Field.Password.prototype, {
    __className__: 'FLUID.Field.Password',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="password" name="<%=widget.getName()%>" value="" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-password'),
    
    /** @private */
    _reset: function () {
        this.value('');
        this.state(FLUID.Field.INITIAL_STATE);
    },

    /** @private */
    _setValue: function (value) {
        this._value = value;
    }
});


/**
 * A map field.
 * @class A map field class.
 * @constructor.
 */
FLUID.Field.Map = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Map');
    
    /** @private */
    this._mapRendered = false;
    
    /** @private */
    this._map = null;
    
    /** @private */
    this._marker = null;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Map, FLUID.Field.Text);


FLUID.Field.Map.setUpDragging = function (widget) {              
    google.maps.event.addListener(widget._marker, 'dragend', function() {
        widget.setLatLon(this.getPosition());
    });
}

$$.extend(FLUID.Field.Map.prototype, {
    __className__: 'FLUID.Field.Map',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<input id="<%=widget.getHTMLElementId()%>" type="hidden" name="<%=widget.getName()%>" value="<%=widget.value()%>" />' + 
            '<div id="<%=widget.getHTMLElementId()%>-map" class="fluid-map" width"553" height"300"></div>' + 
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-map'),
    
    /**
     * Get the field's HTML map element as a selection.
     * @return A JQuery selection of the field's map element.
     * @type JQuery
     */
    getMapSelection: function () {
        return $$('#' + this.getHTMLElementId() + '-map');
    },
    
    /**
     * Return the lat/lon value of the field.
     * @return The value converted into a lat/lon array.
     * @type Array
     */
    getLatLon: function () {
        if (!this.value()) {
            return new google.maps.LatLng(0, 0);
        }
        var latLon = this.value().split(',');
        if (latLon.length !== 2) {
            return new google.maps.LatLng(0, 0);
        }
        return new google.maps.LatLng(
            parseFloat(latLon[0]),
            parseFloat(latLon[1])
        )
    },
    
    /**
     * Set the lat/lon of the advert.
     * @param latLon An array containing the lat/lon to set the value to.
     */
    setLatLon: function (latLon) {
        if (this._map === null || this._marker === null) {
            return;
        }
        if (!latLon) {
            this.value('0.0,0.0');
            this._map.setCenter(new google.maps.LatLng(0.0, 0.0));
            this._marker.setPosition(new google.maps.LatLng(0.0, 0.0));
        }
        if (latLon.lat === undefined) {
            this.value([String(latLon[0]), String(latLon[1])].join(','));
            this._map.setCenter(new google.maps.LatLng(latLon[0], latLon[1]));
            this._marker.setPosition(new google.maps.LatLng(latLon[0], latLon[1]));
        } else {
            this.value([String(latLon.lat()), String(latLon.lng())].join(','));
            this._map.setCenter(latLon);
            this._marker.setPosition(latLon);
        }
        this.inactRealtime();
    },
    
    /**
     * Render the map.
     */
    renderMap: function () {
        if (!this._mapRendered) {
            this._mapRendered = true;
            var myOptions = {
                zoom: 12,
                center: this.getLatLon(),
                mapTypeId: google.maps.MapTypeId.ROADMAP
                };
            this._map = new google.maps.Map(this.getMapSelection()[0], myOptions);
            this._marker = new google.maps.Marker({
                position: this.getLatLon(), 
                map: this._map,
                draggable: true
            });
            FLUID.Field.Map.setUpDragging(this);
        }
    },
    
	/** @private */
	_refresh: function () {
		this._mapRendered = false;
		return FLUID.Field.Entry.prototype._refresh.apply(this);
	}
});


/**
 * An auto-complete entry field.
 * @class A auto-complete text entry field class.
 * @constructor.
 */
FLUID.Field.AutoComplete = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.AutoComplete');
    
    /** @private */
    this._maskValue = options.maskValue;
    
    /** @private */
    this._initialMaskValue = options.maskValue;
    
    /** @private */
    this._datalist = options.datalist;
    
    /** @private */
    this._datalistURL = options.datalistURL;
    
    /** @private */
    this._autocompleteApplied = false;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.AutoComplete, FLUID.Field.Text);

$$.extend(FLUID.Field.AutoComplete.prototype, {
    __className__: 'FLUID.Field.AutoComplete',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-mask"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="hidden" name="<%=widget.getName()%>" value="<%=widget.value()%>" />' +
            '<input id="<%=widget.getHTMLElementId()%>-mask" type="text" name="<%=widget.getName()%>-mask" value="<%=widget.maskValue()%>" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-autocomplete'),
    
    /**
     * Get the field's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return $$('#' + this.getHTMLElementId() + '-input');
    },
    
    /**
     * Get the field's HTML input mask element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getMaskSelection: function () {
        return $$('#' + this.getHTMLElementId() + '-mask');
    },
    
    /**
     * Give the field focus.
     */
    focus: function () {
        this.getMaskSelection().focus();
    },
    
    /**
     * Set/Get the value for the field.
     * @param {String} value (Optional) If undefined the current value for
     * the field is returned, else the field's value is set.
     * @return The value.
     * @type String
     */
    maskValue: function (value) {
        if (value === undefined) {
            if (this.exists()) {
                this._maskValue = this.getMaskSelection().val();
            }
            return this._maskValue;
        }
        this.trigger('value', function () {
            this._maskValue = value;
            if (this.exists()) {
                this.getMaskSelection().attr('value', this._maskValue);
            }
        }, {data: {'value': value}});
    },
        
    /**
     * Set/Get the initial mask value for the field. 
     *
     * @param {String} value (Optional) If undefined the current initial 
     * mask value for the field is returned, else the field's initial mask 
     * value is set.
     * @return The initial mask value.
     * @type String
     */
    initialMaskValue: function (value) {
        if (value === undefined) {
            return this._initialMaskValue;
        }
        this.trigger('initialMaskValue', function () {
            if (this._initialMaskValue !== value) {
                this._initialMaskValue = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Reset the fields value.
     */
    _reset: function () {
        this.value(this.initialValue());
        this.maskValue(this.initialMaskValue());
        this.state(FLUID.Field.INITIAL_STATE);
    },
    
	/** @private */
	_refresh: function () {
		this._autocompleteApplied = false;
		return FLUID.Field.Entry.prototype._refresh.apply(this);
	},
	
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        FLUID.Widget.prototype._update.apply(this, [false]);

        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);

        if (this._autocompleteApplied === false) {
            this._autocompleteApplied = true;
            this.getMaskSelection().autocomplete(
                this._datalist || this._datalistURL, 
                {
                    extraParams: {'@': IO.Request.getId()}
                }
            ).result(function(ev, item) {
                var widget = $$(this).data('fluid-widget');
                widget._snapshot();
                widget.value(item[1]);
                widget.inactRealtime();
            });
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);
    },
    

    /** @private */
    _bind: function () {
        
        if (!this.exists()) {
            return;
        }
        
        // Bind children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    child._bind();
                }
            }
        }
        
        // Bind object to the
        this.getHTMLSelection().data('fluid-widget', this);
        this.getInputSelection().data('fluid-widget', this);
        this.getMaskSelection().data('fluid-widget', this);
    
        // Bind native events
        for (var fwEventName in this.__frameworkEvents__) {
            if (this.__frameworkEvents__.hasOwnProperty(fwEventName)) {
                var eventName = this.__frameworkEvents__[fwEventName];
                this.getMaskSelection().unbind(fwEventName + '.fluid');
                this.getMaskSelection().bind(fwEventName + '.fluid', 
                    function (fwEvent) {
                        var widget = $$(this).data('fluid-widget');
                        widget.trigger(widget.__frameworkEvents__[fwEvent.type], 
                            function (event) {
                        }, {data: {fwEvent: fwEvent}});
                    });
            }
        }    
    },      
    
    /** @private */
    _bindEvents: function () {
        
        this.bind('onKeyDown.fluid', function (event) {
            // If some hits enter or return and submits the form we need set
            // the fields state to pending because we want to be able to 
            // submit the form with a return key without waiting for the 
            // field to inact the realtime call.
            if (event.data.fwEvent.keyCode == 13) {
                this._syncValue();
                this.state(FLUID.Field.PENDING_STATE);
            }
        });

        this.bind('onFocus.fluid', function (event) {
            this.snapshot();
            this._showBubble();
        });
        
        this.bind('onBlur.fluid', function (event) {
            this._bubble.burst();
            
            var maskValue = $$.trim(this.getMaskSelection().val());
            if (!maskValue) {
                this._snapshot();
                this.value('');
                this.inactRealtime();
            }
            
            if (this.changed() || this.state() === FLUID.Field.PENDING_STATE) {
                this.inactRealtime();
            }
        });
         
        this.bind('onMouseEnter.fluid', function (event) {
            this._showBubble();
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            this._bubble._burst();
        });       
    }
});