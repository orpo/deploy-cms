/**
 * @fileoverview    Fluid choice field widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */ 

'use strict';


/**
 * A radio button.
 * @class A radio button class.
 * @constructor
 */    
FLUID.FieldOption.Radio = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.FieldOption.Option.call(this, options, false);
    this._type.push('FieldOption.Radio');
    
    // Add the additional CSS class to child classes
    this._classes.push(FLUID._cssName('tick'));
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.FieldOption.Radio, FLUID.Widget);
UTILS.copyPrototype(FLUID.FieldOption.Radio, FLUID.FieldOption.Option);

$$.extend(FLUID.FieldOption.Radio.prototype, {
    __className__: 'FLUID.FieldOption.Radio',
    __template__: 
        '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<input id="<%=widget.getHTMLElementId()%>-input" type="radio" name="<%=widget.getField().getName()%>" value="<%=widget.value()%>" <% if (widget.selected()) { %>checked="checked"<% } %> />' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%></label>' +
        '</div>',
    __css__: FLUID._cssName('field-option-radio'),
    __frameworkEvents__: {
        'blur': 'blur',
        'change': 'change',
        'focus': 'focus'
        },
	
    /**
     * Get the field option's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return this.getHTMLSelection().find('input').first();
    },
    
    /**
     * Select the option.
     * @param {Boolean} value (Optional) If specified as false the option is
     * unselected, if not specified or specified as true the option is
     * selected.
     */
    select: EVENT.classMethod(FLUID.FieldOption.Radio, function select(value) {
		var localArgs = EVENT.getArgs(arguments);
        var value = EVENT.getArg(localArgs, 0);
        if (value === undefined) {
            value = true;
        }
        this._selected = value;
		if (value) {
			this.getInputSelection().attr('checked', 'checked');
		} else {
			this.getInputSelection().removeAttr('checked');
		}
        this._update();
    }, 'select'),
	
    /** @private */
    _bindEvents: function () {        
        this.bind('onChange.fluid', function(event) {
            
            // Select/Unselect self
            this.select(this._ticked());
            
            // Unselect any siblings
            var siblings = this.getSiblings();
            for (var i = 0; i < siblings.length; i++) {
                siblings[i].select(false);
            }
            
            // Tell the parent field to sync its value
            this.getField()._syncValue();
            this.getField().snapshot();
            this.getField().inactRealtime();
        
        });
    },
    
    /** @private */
    _ticked: function () {
        return this.getHTMLSelection().find('input:checked').first().length === 1;
    },
    
    /** @private */
    _bind: function () {
        
        // NOTE: We have to custom bind the events because for radio field 
        // options because the events are bound against the input element not 
        // the outer division.
        
        if (!this.exists()) {
            return;
        }
        
        // Bind children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    child._bind();
                }
            }
        }
        
        // Bind object to the
        this.getHTMLSelection().data('fluid-widget', this);
        this.getInputSelection().data('fluid-widget', this);
    
        // Bind native events
        for (var fwEventName in this.__frameworkEvents__) {
            if (this.__frameworkEvents__.hasOwnProperty(fwEventName)) {
                var eventName = this.__frameworkEvents__[fwEventName];
                this.getInputSelection().unbind(fwEventName + '.fluid');
                this.getInputSelection().bind(fwEventName + '.fluid', 
                    function (fwEvent) {
                        var widget = $$(this).data('fluid-widget');
                        widget.trigger(widget.__frameworkEvents__[fwEvent.type], 
                            function (event) {
                        }, {data: {fwEvent: fwEvent}});
                    });
            }
        }    
    }
});


/**
 * A checkbox.
 * @class A checkbox class.
 * @constructor
 */    
FLUID.FieldOption.Checkbox = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.FieldOption.Radio.call(this, options, false);
    this._type.push('FieldOption.Checkbox');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.FieldOption.Checkbox, FLUID.FieldOption.Radio);

$$.extend(FLUID.FieldOption.Checkbox.prototype, {
    __className__: 'FLUID.FieldOption.Checkbox',
    __template__:
        '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<input id="<%=widget.getHTMLElementId()%>-input" type="checkbox" name="<%=widget.getField().getName()%>" value="<%=widget.value()%>" <% if (widget.selected()) { %>checked="checked"<% } %> />' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%></label>' +
        '</div>',
    __css__: FLUID._cssName('field-option-checkbox'),
    __frameworkEvents__: {
        'change': 'change',
        },

    /** @private */
    _bindEvents: function () {        
        this.bind('onChange.fluid', function(event) {

            // Select/Unselect self
            this.select(this._ticked());

            // Tell the parent field to sync its value
            this.getField()._syncValue();
            this.getField().snapshot();
            this.getField().inactRealtime();

        });
    }

});


/**
 * A boolean field (shown as a check box).
 * @class A boolean field class.
 * @constructor.
 */
FLUID.Field.Boolean = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Entry.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('Field.Boolean');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Boolean, FLUID.Field.Entry);
UTILS.copyPrototype(FLUID.Field.Boolean, FLUID.Field.Realtime);

$$.extend(FLUID.Field.Boolean.prototype, {
    __className__: 'FLUID.Field.Boolean',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input">' + 
                '<%=widget.label()%>' +
            '</label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" ' + 
            'type="checkbox" name="<%=widget.getName()%>" ' + 
            'value="1" <% if (widget.value()) { %>checked="checked"<% } %> />' +
        '</div>',
    __css__: FLUID._cssName('field-boolean'),
    __frameworkEvents__: {
        'change': 'change',
        },

    /** @private */
    _syncValue: function () {
        this._value = this._ticked();
    },
    
    /** @private */
    _setValue: function (value) {
        this._value = value;
        if (this.exists()) {
            if (this._value) {
                this.getInputSelection().attr('checked', 'checked');
            } else {
                this.getInputSelection().removeAttr('checked');
            }
        }
    },
    
    /** @private */
    _getValue: function (value) {
        if (this._value) {
            return this._value;
        }
        return;
    },
    
    /** @private */
    _ticked: function () {
        return this.getHTMLSelection().find('input:checked').first().length === 1;
    },

	/** @private */
    _bindEvents: function () {
        this.bind('onChange.fluid', function (event) {
            this._syncValue();

            if (this.changed() || this.state() === FLUID.Field.PENDING_STATE) {
                this.inactRealtime();
            }
			
			this._snapshot();
        });
    }
});


/**
 * A select (drop-down) field.
 * @class A select field class.
 * @constructor.
 */
FLUID.Field.Select = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Entry.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('Field.Select');
   
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Select, FLUID.Field.Entry);
UTILS.copyPrototype(FLUID.Field.Select, FLUID.Field.Realtime);

$$.extend(FLUID.Field.Select.prototype, {
    __className__: 'FLUID.Field.Select',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-input">' + 
                '<%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %>' +
            '</label>' +
            '<select id="<%=widget.getHTMLElementId()%>-input" ' + 
            'name="<%=widget.getName()%>" <%=FLUID_GROUP_PREFIX%>client>' +
                '<option value="">Select...</option>' + 
                '<%=group_client%>' +
            '</select>' + 
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-select'),
    __inputTag__: 'select',
    
    /**
     * Get the options for this field.
     * @return A list of options for the field.
     * @type Array
     */
    getFieldOptions: function () {
        var options = [];
        var children = this.getChildren();
        for (var i = 0; i < children.length; i++) {
            if (children[i].typeOf(FLUID.FieldOption.Option)) {
                options.push(children[i]);            
            }
        }
        return options;
    },
    
    /**
     * Return the option class for the select field.
     * @return The option class for the field.
     * @type FLUID.FieldOption.Option
     */
    getOptionClass: function () {
        return FLUID.FieldOption.Option;
    },
    
	/**
	 * Sort the options for the field.
	 */
	sort: function () {
		/* Sort the option */
        var options = this.getFieldOptions();
		options.sort(function (a, b) { 
			if (a.label() > b.label()) {
				return 1;
			}
			return -1;
		});

		/* Remove and re-add the options */
		for (var i = 0; i < options.length; i++) {
            var option = options[i];
			option.remove();
			this.appendChild(option);
        }
	},
	
    /** @private */
    _syncValue: function () {
        this._value = [];
        var selectedOptions = this.getHTMLSelection().find('select:first option:selected');
        for (var i = 0; i < selectedOptions.length; i++) {
            var selectedOption = $$(selectedOptions[i]).data('fluid-widget');
            if (selectedOption !== null && selectedOption !== undefined && selectedOption.value()) {
                this._value.push(selectedOption.value());
            }
        }
        if (this._value.length === 0) {
            this._value = '';
        } else if (this._value.length === 1) {
            this._value = this._value[0];
        }
    },
    
    /** @private */
    _bindEvents: function () {
        
        this.bind('onFocus.fluid', function(event) {
            this._syncValue();
            this.snapshot();
            this._showBubble();
        });
        
        this.bind('onChange.fluid', function(event) {
            this._syncValue();
            this._bubble.burst();
            
            if (this.changed()) {
                this.inactRealtime();
		        this.snapshot();
            }
        });
        
        this.bind('onMouseEnter.fluid', function(event) {
            this._showBubble();
        });
        
        this.bind('onMouseLeave.fluid', function(event) {
            this._bubble.burst();
        });       
    },
    
    /**
     * The default options when creating a new choice field.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: '',
        required: false
    }
});


/**
 * A choice (ticks - radio or checkbox) field.
 * @class A choice field class.
 * @constructor.
 */
FLUID.Field.Choice = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Entry.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('Field.Choice');
   
    this._multiple = options.multiple;
   
    //this._classes.push(FLUID._cssName('fluid-multiple'));
   
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Choice, FLUID.Field.Entry);
UTILS.copyPrototype(FLUID.Field.Choice, FLUID.Field.Realtime);

$$.extend(FLUID.Field.Choice.prototype, {
    __className__: 'FLUID.Field.Choice',
    __template__:
    '<div id="<%=widget.getHTMLElementId()%>">' +
        '<label>' +
            '<label for="<%=widget.getHTMLElementId()%>-input"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
        '</label>' +
        '<div class="fluid-ticks <%=FLUID_GROUP_PREFIX%>client">' +
            '<%=group_client%>' +
        '</div>' + 
        '<div class="fluid-status"></div>' + 
    '</div>',    
    
    __css__: FLUID._cssName('field-choice'),
    __inputTag__: '',
    
    /**
     * Get the options for this field.
     * @return A list of options for the field.
     * @type Array
     */
    getFieldOptions: function () {
        var options = [];
        var children = this.getChildren();
        for (var i = 0; i < children.length; i++) {
            if (children[i].typeOf('FieldOption.Radio')) {
                options.push(children[i]);            
            }
        }
        return options;
    },
    
    /**
     * Return the option class for the select field.
     * @return The option class for the field.
     * @type FLUID.FieldOption.Option
     */
    getOptionClass: function () {
        if (this._multiple) {
            return FLUID.FieldOption.Checkbox;
        }
        return FLUID.FieldOption.Radio;
    },
    
	/**
	 * Sort the options for the field.
	 */
	sort: function () {
		/* Sort the option */
        var options = this.getFieldOptions();
		options.sort(function (a, b) { 
			if (a.label() > b.label()) {
				return 1;
			}
			return -1;
		});

		/* Remove and re-add the options */
		for (var i = 0; i < options.length; i++) {
            var option = options[i];
			option.remove();
			this.appendChild(option);
        }
	},

    /** @private */
    _syncValue: function () {
        var options = this.getFieldOptions();
        
        this._value = [];
        for (var i = 0; i < options.length; i++) {
            var option = options[i];
            if (option.selected()) {
                if (this._multiple) {
                    this._value.push(option.value());
                } else {
                    this._setValue(option.value());
                    return;
                }
            }
        }
        
        if (this._value.length === 0) {
            this._value = '';
        } else if (this._value.length === 1) {
            this._value = this._value[0];
        }
    },
    
    /** @private */
    _bindEvents: function () {
        this.bind('afterValidateComplete.fluid', function(event) {
            if (this.stateMessage() === '') {
                this._bubble.burst();
            }
            this._showBubble();
        });
    },
    
    /**
     * The default options when creating a new choice field.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: '',
        required: false,
        multiple: false
    }
});

/**
 * An tokenizer entry field.
 * @class A tokenizer text entry field class.
 * @constructor.
 */
FLUID.Field.Tokenizer = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Tokenizer');
    
    /** @private */
    this._allowCreate = options.allowCreate;
    
    /** @private */
    this._datalist = options.datalist;
    
    /** @private */
    this._datalistURL = options.datalistURL;
    
    /** @private */
    this._extraParams = options.extraParams || '';
    
    /** @private */
    this._autocompleteApplied = false;
    
    /** @private */
    this._autocompletePreFilled = false;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Tokenizer, FLUID.Field.Text);

$$.extend(FLUID.Field.Tokenizer.prototype, {
    __className__: 'FLUID.Field.Tokenizer',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<label for="<%=widget.getHTMLElementId()%>-mask"><%=widget.label()%><% if (!widget._required) { %><span class="fluid-optional">Optional</span><% } %></label>' +
            '<input id="<%=widget.getHTMLElementId()%>-input" type="hidden" name="<%=widget.getName()%>" value="" />' +
            '<input id="<%=widget.getHTMLElementId()%>-mask" type="text" name="<%=widget.getName()%>-mask" value="" placeholder="<%=widget.placeholder()%>" />' +
            '<div class="fluid-status"></div>' +
        '</div>',
    __css__: FLUID._cssName('field-tokenizer'),
    
    /**
     * Get the field's HTML input element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getInputSelection: function () {
        return $$('#' + this.getHTMLElementId() + '-input');
    },
    
    /**
     * Get the field's HTML input mask element as a selection.
     * @return A JQuery selection of the field's HTML input element.
     * @type JQuery
     */
    getMaskSelection: function () {
        return $$('#' + this.getHTMLElementId() + '-mask');
    },
    
    /**
     * Set/Get the extraParams for the field.
     * @param {String} value (Optional) If undefined the current extra params
     * value for the field is returned, else the field's extra params value is
     * set.
     * @return The extra params value.
     * @type String
     */
    extraParams: function (value) {
        if (value === undefined) {
            return this._extraParams;
        }
        this.trigger('extraParams', function () {
            if (this._extraParams !== value) {
                this._extraParams = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Clear the field.
     */
    clear: function (inactRealtime) {
        this.getHTMLSelection().find('#as-values-tokenizer' + this.getId()).val('');
        this.getHTMLSelection().find('#as-selections-tokenizer' + this.getId() + ' li.as-selection-item').remove();
        if (inactRealtime === true || inactRealtime === undefined) {
            this._syncValue();
            this.inactRealtime();
        }
    },
    
    /**
     * Reset the fields value.
     */
    _reset: function () {
        this.clear(false);
        this.value(this.initialValue());
        this.state(FLUID.Field.INITIAL_STATE);
    },
    
	/** @private */
	_refresh: function () {
		this._autocomplete_applied = false;
		this._autocompletePreFilled = false;
		return FLUID.Field.Entry.prototype._refresh.apply(this);
	},
	
	/** @private */
	_syncValue: function () {
        // In this instance sync value is responsible for recompiling the
        // selected tokens into a dictionary then converting it to JSON and
        // setting it against the values.
        
        // Check the value field is ready
        if ($$('#as-values-tokenizer' + this.getId()).length === 0) {
            return;
        }
        
        var i = 0;
        var values = [];
        var raw_values = $$('#as-values-tokenizer' + this.getId()).attr('value').split(',');
        for (i = 0; i < raw_values.length; i++) {
            var value = $$.trim(raw_values[i]);
            if (value !== '') {
                values.push(parseInt(value));
            }
        }        
        
        var names = [];
        var name_items = $$('#as-selections-tokenizer' + this.getId() + ' > li.as-selection-item');
        for (i = 0; i < name_items.length; i++) {
            var name = $$.trim($$(name_items[i]).text().substring(1));
            if (name !== '') {
                names.push(name);
            }
        }
        
        var duplicate_check = {};
        var value_dicts = [];
        for (i = 0; i < values.length; i++) {
            var value_dict = {value: values[i], name: names[i]};
            if (value_dict['value'] !== undefined && value_dict['name'] !== undefined) {
                if (duplicate_check[value_dict['value']] === undefined) { 
                    value_dicts.push({value: values[i], name: names[i]});
                    duplicate_check[value_dict['value']] = true;
                }
            }
        }
        
        this._snapshot();
        this.value(JSON.stringify(value_dicts));
    },
	
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        FLUID.Widget.prototype._update.apply(this, [false]);

        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);

        if (this._autocomplete_applied === false) {
            this._autocomplete_applied = true;
            
            this.getMaskSelection().bind('keydown', function (ev) {
                var widget = $$('.fluid-field-tokenizer input:focus').closest('.fluid-field').data('fluid-widget');
                
                if (!widget._allowCreate) {
                    return;
                }
                
                var value = $$.trim($$(this).val());
                if(event.keyCode === 9 || event.keyCode === 188) {
                    event.preventDefault();
                    $$('#as-selections-tokenizer' + widget.getId()).find('li:last-child').before('<li class="as-selection-item"><a class="as-close">×</a>' + value + '</li>');
                    $$('#as-values-tokenizer' + widget.getId()).attr('value', $$('#as-values-tokenizer' + widget.getId()).attr('value') + ', -1');
                    $$(this).val('');
                    widget._syncValue();
                    widget.inactRealtime();
                    
                    // Add support for removing the tag
                    var newTag = $$('#as-selections-tokenizer' + widget.getId()).find('li:last-child').prev();
                    $$(newTag).find('.as-close').bind('click', {widget: widget}, function (event) {
                        $$(this).parent().remove();
                        event.data.widget._syncValue();
                        event.data.widget.inactRealtime();
                    });
                }
            });
            
            this.getMaskSelection().autoSuggest(
                this._datalist || this._datalistURL, 
                {
                    asHtmlID: 'tokenizer' + this.getId(),
                    startText: '',
                    selectedItemProp: 'name',
                    selectedValuesProp: 'value',
                    searchObjProps: 'name',
                    minChars: 2,
                    extraParams: '&@=' + IO.Request.getId(),
                    preFill: this.value(),
                    realtimeParams: function(string) {
                        var widget = $$('.fluid-field-tokenizer input:focus').closest('.fluid-field').data('fluid-widget');
                        return string + widget.extraParams();
                    },
                    selectionAdded: function (elem) {
                        var widget = $$(elem).closest('.fluid-field').data('fluid-widget');
                        if (widget._autocompletePreFilled) {
                            widget._syncValue();
                            widget.inactRealtime();
                        }
                    },
                    selectionRemoved: function (elem) {
                        var widget = $$(elem).closest('.fluid-field').data('fluid-widget');
                        if (widget._autocompletePreFilled) {
                            elem.remove();
                            widget._syncValue();
                            widget.inactRealtime();
                        }
                    }
                });
            this._autocompletePreFilled = true;
            this._syncValue();
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);
    },
    

    /** @private */
    _bind: function () {
        
        if (!this.exists()) {
            return;
        }
        
        // Bind children first
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    child._bind();
                }
            }
        }
        
        // Bind object to the
        this.getHTMLSelection().data('fluid-widget', this);
        this.getInputSelection().data('fluid-widget', this);
        this.getMaskSelection().data('fluid-widget', this);
    
        // Bind native events
        for (var fwEventName in this.__frameworkEvents__) {
            if (this.__frameworkEvents__.hasOwnProperty(fwEventName)) {
                var eventName = this.__frameworkEvents__[fwEventName];
                this.getMaskSelection().unbind(fwEventName + '.fluid');
                this.getMaskSelection().bind(fwEventName + '.fluid', 
                    function (fwEvent) {
                        var widget = $$(this).data('fluid-widget');
                        widget.trigger(widget.__frameworkEvents__[fwEvent.type], 
                            function (event) {
                        }, {data: {fwEvent: fwEvent}});
                    });
            }
        }    
    },      
    
    /** @private */
    _bindEvents: function () {
        
        this.bind('onKeyDown.fluid', function (event) {
            // If some hits enter or return and submits the form we need set
            // the fields state to pending because we want to be able to 
            // submit the form with a return key without waiting for the 
            // field to inact the realtime call.
            if (event.data.fwEvent.keyCode == 13) {
                this._syncValue();
                this.state(FLUID.Field.PENDING_STATE);
            }
        });

        this.bind('onFocus.fluid', function (event) {
            this.snapshot();
            this._showBubble();
        });
        
        this.bind('onBlur.fluid', function (event) {
            this._bubble.burst();
            
            if (this.changed() || this.state() === FLUID.Field.PENDING_STATE) {
                this.inactRealtime();
            }
        });
         
        this.bind('onMouseEnter.fluid', function (event) {
            this._showBubble();
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            this._bubble._burst();
        });       
    }
});