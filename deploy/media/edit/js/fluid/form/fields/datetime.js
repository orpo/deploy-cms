/**
 * @fileoverview    Fluid date and time field widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */ 

'use strict';


/**
 * A date field.
 * @class A date field.
 * @constructor.
 */
FLUID.Field.Date = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Text.call(this, options, false);
    this._type.push('Field.Date');
    
    /** @private */
    this._picker = new FLUID.Field.Date.DatePicker({owner: this});

	/** @private */
	this._canBurstPicker = true;

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Field.Date, FLUID.Field.Text);

$$.extend(FLUID.Field.Date.prototype, {
    __className__: 'FLUID.Field.Date',
    __css__: FLUID._cssName('field-date'),
    
	/**
     * Destroy the datetime.
     */
    destroy: function () {
		if (this._picker !== undefined && this._picker !== null) {
			this._picker.destroy();
		}
		FLUID.Widget.prototype.destroy.apply(this, [false]);
	},
	
    /** @private */
    _bindEvents: function () {
        FLUID.Field.Text.prototype._bindEvents.apply(this);
        
		this.bind('onFocus', function (event, data) {
			
            // Determine the orientation & position
            var offset = this.getOffset();
            var height = this.getHTMLSelection().outerHeight(true, false);
            var menuHeight = this._picker.getPopupSize()[1];
            var screenHeight = $$(window).height();
            
            if (offset[1] + height + menuHeight > screenHeight) {
                this._picker.style({left: offset[0], 
                    top: offset[1] - menuHeight});
            } else {
                this._picker.style({left: offset[0], 
                    top: offset[1] + height});
            }

            this._picker.date(UTILS.dateFromString(this.value()));
            this._picker.selectedDate(UTILS.dateFromString(this.value()));
                
			this._canBurstPicker = true;
            this._picker.popup();
		});
		
		this.bind('onBlur.fluid', function (event, data) {
			if (this._canBurstPicker) {
				this._picker.burst();
			}
		});		
    }
});


/**
 * A date selection class used to provide a calendar view for selecting 
 * dates in a bubble next to a date(time) field.
 * @class A calendar date selection class.
 * @constructor.
 */
FLUID.Field.Date.DatePicker = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Popup.call(this, options);
	this._type.push('Field.Date.DatePicker');

    /** @private */
    this._startWeekday = options.startWeekday;
    if (this._startWeekday === undefined) {
        this._startWeekday = 1;
    }
    
    /** @private */
    this._date = options.date || new Date();
	
	/** @private */
	this._selectedDate = options.date;

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Field.Date.DatePicker, FLUID.Widget);
UTILS.copyPrototype(FLUID.Field.Date.DatePicker, FLUID.Popup);

$$.extend(FLUID.Field.Date.DatePicker.prototype, {
    __className__: 'FLUID.Field.Date.DatePicker',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
			'<div class="fluid-title">' + 
                '<span class="fluid-label"></span>' + 
                '<div class="fluid-prev"></div>' + 
                '<div class="fluid-next"></div>' +
            '</div>' +
            '<table class="fluid-calendar">' + 
                '<thead>' + 
                '</thead>' + 
                '<tbody></tbody>' + 
            '</table>' + 
        '</div>',
    __css__: FLUID._cssName('field-date-picker'),
    __frameworkEvents__: {
        'mouseout': 'mouseOut',
        'mouseover': 'mouseOver',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
    },
    __defaultClient__: FLUID_DEFAULT_GROUP,
    __weekdays__: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    __months__: [
        'January', 
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
        ],
    __monthLengths__: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
			         
	/**
	 * Set/Get the start weekday the date picker starts with.
	 * @param {String} value (Optional) If undefined the current start 
	 * weekday for the picker is returned, else the date picker label is set.
	 * @return The starting weekday.
	 * @type Number
	 */
	startWeekday: function (value) {
	    if (value === undefined) {
	        return this._startWeekday;
	    }
	    this.trigger('startWeekday', function () {
	        if (this._startWeekday !== value) {
	            this._startWeekday = value;
	            this._updateCalendarHeader();
	        }
	    }, {data: {'value': value}});
	},

	/**
	 * Set/Get the date for the picker.
	 * @param {String} value (Optional) If undefined the current date for the 
	 * picker is returned, else the date picker date is set.
	 * @return The date.
	 * @type Date
	 */
	date: function (value) {
	    if (value === undefined) {
	        return this._date;
	    }
	    this.trigger('date', function () {
	        if (this._date !== value) {
	            this._date = value;
	            this._updateCalendar();
	        }
	    }, {data: {'value': value}});
	},
	
	/**
	 * Set/Get the selected date for the picker.
	 * @param {String} value (Optional) If undefined the current date for the 
	 * picker is returned, else the selected date picker date is set.
	 * @return The selected date.
	 * @type Date
	 */
	selectedDate: function (value) {
	    if (value === undefined) {
	        return this._date;
	    }
	    this.trigger('selectedDate', function () {
	        if (this._selectedDate !== value) {
	            this._selectedDate = value;
	            this._updateCalendar();
	        }
	    }, {data: {'value': value}});
	},
	
    /**
     * Go to the next page.
     */
	nextMonth: function () {
        var year = this._date.getFullYear();
        var month = this._date.getMonth() + 1;
        if (month > 11) {
            year++;
            month = 0;
        }
        
        this._date = new Date(year, month, 1);
        this._updateCalendar();
	},
	
    /**
     * Go to the previous page.
     */
	previousMonth: function () {
        var year = this._date.getFullYear();
        var month = this._date.getMonth() - 1;
        if (month < 0) {
            year--;
            month = 11;
        }
        
        this._date = new Date(year, month, 1);
        this._updateCalendar();
	},
	
    /**
     * Destroy the widget.
     */
    destroy: function () {
		FLUID.Widget.prototype.destroy.apply(this);
		
		// Tool selections
        var nextSelector = '#' + this._htmlElementId + ' .fluid-next';
        var previousSelector = '#' + this._htmlElementId + ' .fluid-prev';
        var daysSelector = '#' + this._htmlElementId + ' .fluid-calendar td';
	
		// Next
		$$(document).undelegate(nextSelector);
		
		// Prev
		$$(document).undelegate(previousSelector);
		
		// Days
		$$(document).undelegate(daysSelector);
	},
	
	/** @private */
	_updateCalendar: function () {
		if (!this.exists()) {
            return;
        }
        var i = 0;
        var j = 0;
		
		// Update the title
		this.getHTMLSelection().find('.fluid-title > .fluid-label').html(this.__months__[this._date.getMonth()] + ', ' + this._date.getFullYear());
		
		// Update the weekdays
        var header = [];
        for (i = this._startWeekday; i < this.__weekdays__.length; i++) {
            header.push(this.__weekdays__[i]);
        }
        for (i = 0; i < this._startWeekday; i++) {
            header.push(this.__weekdays__[i]);
        }
		var weekdaysHTML = UTILS.template('<tr><% for (var i = 0; i < header.length; i++) { %>' +
				'<th><%= header[i] %></th>' +
			'<% } %></tr>', 
			{'header': header});
		this.getHTMLSelection().find('.fluid-calendar > thead').html(weekdaysHTML);
		
		// Update the dates
        var year = this._date.getFullYear();
        var month = this._date.getMonth();
        var dates = [];
        var offset = new Date(year, month, 1).getDay();
        
        // Pad for start day
        offset -= this._startWeekday;
        if (offset < 0) {
            offset = 6 - offset;
        }
        if (offset > 6) {
            offset = offset - (offset - 6);
        }
        for (i = 0; i < (offset); i++) {
            dates.push(null);
        }
        
        // Real dates
        var monthLength = this._monthLength(this._date.getMonth(), 
            this._date.getFullYear());
        for (i = 0; i < monthLength; i++) {
            dates.push({
                value: i + 1
                });
        }
        
        // Convert dates to rows
		var today = new Date();
		var todayStr = UTILS.dateToString(today);
		var selectedStr = '';
		if (this._selectedDate !== null) {
			selectedStr = UTILS.dateToString(this._selectedDate);
        }
		var rows = [];
        i = 0;
        while (i < dates.length) {
            var row = [];
            for (j = 0; j < 7; j++) {
                if (i >= dates.length || dates[i] === null) {
					row.push(null);
                	i++;
                    continue;
                }
				var date = new Date(year, month, dates[i].value);
				var dateStr = UTILS.dateToString(date);
                row.push({
                    label: dates[i].value,
					past: dateStr < todayStr,
                    today: dateStr === todayStr, 
                    selected: dateStr === selectedStr,
					date: dateStr
                    })
                i++;
            }
            rows.push(row);
        }

		var datesHTML = UTILS.template('<% for (var i = 0; i < rows.length; i++) { %><tr>' +
				'<% for (var j = 0; j < rows[i].length; j++) { %>' + 
					'<% if (rows[i][j] === null) { %>' + 
						'<td class="fluid-empty"></td>' + 
					'<% } else { %>' + 
						'<td data-date="<%= rows[i][j].date %>" ' +
						' class="<% if (rows[i][j].past) { %>fluid-past<% } %><% if (rows[i][j].today) { %>fluid-today<% } %> <% if (rows[i][j].selected) { %>fluid-selected<% } %>">' + 
							'<%= rows[i][j].label %>' +
						'</td>' +
					'<% } %>' +
				'<% } %>' + 
			'</tr><% } %>', 
			{'rows': rows});
		this.getHTMLSelection().find('.fluid-calendar > tbody').html(datesHTML);
	},
	
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
    	FLUID.Widget.prototype._update.apply(this, [false]);

        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  

        this._updateCalendar();

        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },

    /** @private */
    _bindEvents: function () {
		// Tool selections
        var nextSelector = '#' + this._htmlElementId + ' .fluid-next';
        var previousSelector = '#' + this._htmlElementId + ' .fluid-prev';
        var daysSelector = '#' + this._htmlElementId + ' .fluid-calendar td';

		// Previous
        $$(document).delegate(previousSelector, 'mouseenter.fluid', function (fwEvent) {
			$$(this).addClass('fluid-over');
        });
	    $$(document).delegate(previousSelector, 'mouseleave.fluid', function (fwEvent) {
			$$(this).removeClass('fluid-over');
        	$$(this).removeClass('fluid-down');
		});
	    $$(document).delegate(previousSelector, 'mousedown.fluid', function (fwEvent) {
			$$(this).addClass('fluid-down');
        });
	    $$(document).delegate(previousSelector, 'mouseup.fluid', function (fwEvent) {
			$$(this).removeClass('fluid-down');
        });
		$$(document).delegate(previousSelector, 'click.fluid', function (fwEvent) {
			var widget = $$(this).closest('.fluid-field-date-picker').data('fluid-widget');
			widget.previousMonth();
        });

        // Next
        $$(document).delegate(nextSelector, 'mouseenter.fluid', function (fwEvent) {
			$$(this).addClass('fluid-over');
        });
	    $$(document).delegate(nextSelector, 'mouseleave.fluid', function (fwEvent) {
			$$(this).removeClass('fluid-over');
        	$$(this).removeClass('fluid-down');
		});
	    $$(document).delegate(nextSelector, 'mousedown.fluid', function (fwEvent) {
			$$(this).addClass('fluid-down');
        });
	    $$(document).delegate(nextSelector, 'mouseup.fluid', function (fwEvent) {
			$$(this).removeClass('fluid-down');
        });
        $$(document).delegate(nextSelector, 'click.fluid', function (fwEvent) {
			var widget = $$(this).closest('.fluid-field-date-picker').data('fluid-widget');
			widget.nextMonth();
        });

		// Days
        $$(document).delegate(daysSelector, 'mouseenter.fluid', function (fwEvent) {
			if (!$$(this).hasClass('fluid-empty')) {
				$$(this).addClass('fluid-over');
        	}
		});
	    $$(document).delegate(daysSelector, 'mouseleave.fluid', function (fwEvent) {
			if (!$$(this).hasClass('fluid-empty')) {
				$$(this).removeClass('fluid-over');
	        	$$(this).removeClass('fluid-down');
			};
		});
	    $$(document).delegate(daysSelector, 'mousedown.fluid', function (fwEvent) {
			if (!$$(this).hasClass('fluid-empty')) {
				$$(this).addClass('fluid-down');
			}
        });
	    $$(document).delegate(daysSelector, 'mouseup.fluid', function (fwEvent) {
			if (!$$(this).hasClass('fluid-empty')) {
				$$(this).removeClass('fluid-down');
        	}
		});
	    $$(document).delegate(daysSelector, 'click.fluid', function (fwEvent) {
			if (!$$(this).hasClass('fluid-empty')) {
				var widget = $$(this).closest('.fluid-field-date-picker').data('fluid-widget');
				
				// Act as if the associated date field was updated manually so 
				// the sync works.
				widget.owner().getInputSelection().attr('value', $$(this).attr('data-date'));
				widget.owner().getInputSelection().blur();
				
				widget.burst();
			}
		});
		
		this.bind('onMouseDown.fluid', function (event) {
			this.owner()._canBurstPicker = false;
			event.data.fwEvent.preventDefault();
		});
		
		this.bind('onMouseLeave.fluid', function (event) {
		    this.owner()._canBurstPicker = true;
			if (!this.owner().getInputSelection().is(':focus')) {
				this.burst();
			}
		});
	},

    /** @private */
    _monthLength: function (month, year) {
        // Handle February in leap years
        if (month === 1 && this._isLeapYear(year)) {
            return 29;
        }
        return this.__monthLengths__[month];
    },
    
    /** @private */
    _isLeapYear: function (year) {
        return new Date(year, 1, 29).getDate() == 29;
    },
	
    /** @private */
    _popup: function () {
        
        switch (this._transitionState) {
            case FLUID.Popup.POPUP_STATE:
                return;
                                
            case FLUID.Popup.BURST_STATE:
                this.getHTMLSelection().stop(true, true);
                this.hide();
                break;
            
            case FLUID.Popup.DORMANT_STATE:
                if (this._parent === undefined) {
					this.getDesktop().appendChild(this, 'modal');
                }
                this.refresh();
                this.hide();
                break;
            
            default:
                break;
        }
        this._transitionState = FLUID.Popup.POPUP_STATE;
        
        this.getDesktop().startModal(FLUID.Desktop.MODAL);
        
        this.getHTMLSelection().fadeIn(
            FLUID.Popup.POPUP_DURATION,
            function (event) {
                $$(this).data('fluid-widget').trigger('popupComplete', 
                    function (event) {
                        this.show();
                    });
            });
    },
	
    /** @private */
    _burst: function () {
		var localArgs = EVENT.getArgs(arguments);
        var endModal = EVENT.getArg(localArgs, 0);

		if (this._transitionState !== FLUID.Popup.POPUP_STATE) {
		    return;
		}
		this._transitionState = FLUID.Popup.BURST_STATE;
		this.remove();
	},

    /**
     * The default options when creating a new date picker.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
		date: null
    }
});