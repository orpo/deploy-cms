/**
 * @fileoverview    Fluid fieldset widgets. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


/**
 * A form fieldset for the UI.
 * @class A form fieldset class.  
 * @constructor
 */    
FLUID.Fieldset = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('Fieldset');
    
    /** @private */
    this._legend = options.legend;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Fieldset, FLUID.Widget);

$$.extend(FLUID.Fieldset.prototype, {
    __className__: 'FLUID.Fieldset',
    __template__: '<fieldset id="<%=widget.getHTMLElementId()%>">' +
            '<legend><%=widget.legend()%></legend>' +
            '<div class="fluid-group-client">' + 
                '<%=group_client%>' +
            '</div>' + 
        '</fieldset>',
    __css__: FLUID._cssName('fieldset'),

    /**
     * Set/Get the legend for the fieldset.
     * @param {String} value (Optional) If undefined the current legend for
     * the fieldset is returned, else the fieldsets's legend is set.
     * @return The legend.
     * @type String
     */
    legend: function (value) {
        if (value === undefined) {
            return this._legend;
        }
        this.trigger('legend', function () {
            if (this._legend !== value) {
                this._legend = value;
                if (this.exists()) {
                    this.getHTMLSelection().find('legend').first().html(
                        this._legend);
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);
            
        FLUID.Widget.prototype._update.apply(this, [false]);
    
        // Hide the legend if it is not set
        if (this.legend() === '') {
            this.getHTMLSelection().children('legend').hide();
        } else {
            this.getHTMLSelection().children('legend').show();
        }
    
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true,
        legend: ''
    }
});

