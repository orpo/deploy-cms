/**
 * @fileoverview    Fluid panel widget. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ??
 */

/**
 * A panel widget.
 * @class A panel class.
 * @constructor
 */
FLUID.Panel = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('Panel');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Panel, FLUID.Widget);

$$.extend(FLUID.Panel.prototype, {
    __className__: 'FLUID.Panel',
    __css__: FLUID._cssName('panel'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-group-client">' +
                '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +  
            '</div>' + 
        '</div>'
});

/**
 * A group widget.
 * @class A group class.
 * @constructor
 */
FLUID.Group = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('Group');
    
    /* @private */
    this._legend = options.legend;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Group, FLUID.Widget);

$$.extend(FLUID.Group.prototype, {
    __className__: 'FLUID.Group',
    __css__: FLUID._cssName('group'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<span class="fluid-legend"><%=widget._legend%></span>' +
            '<div class="fluid-group-client">' +
                '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +  
            '</div>' + 
        '</div>',

    /**
     * Set/Get the legend for the group.
     * @param {String} value (Optional) If undefined the current legend for
     * the group is returned, else the group's legend is set.
     * @return The legend.
     * @type String
     */
    legend: function (value) {
        if (value === undefined) {
            return this._legend;
        }
        this.trigger('legend', function () {
            if (this._legend !== value) {
                this._legend = value;
                if (this.exists()) {
                    this.getHTMLSelection().children('.fluid-legend').html(value);
                }
            }
        }, {data: {'value': value}});
    }
});

/**
 * A tabber widget.
 * @class A tabber class.
 * @constructor
 */
FLUID.Tabber = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Helpful.call(this, options);
    FLUID.Labelled.call(this, options);
    this._type.push('Tabber');
    
    this._childGroups['tabs'] = [];
    this._childGroups['panels'] = [];

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.Tabber, FLUID.Widget);
UTILS.copyPrototype(FLUID.Tabber, FLUID.Helpful);
UTILS.copyPrototype(FLUID.Tabber, FLUID.Labelled);

$$.extend(FLUID.Tabber.prototype, {
    __className__: 'FLUID.Tabber',
    __css__: FLUID._cssName('tabber'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-tabs fluid-group-tabs">' +
                '<%=group_tabs%>' +  
            '</div>' +
            '<div class="fluid-client fluid-group-panels">' +
                '<%=group_panels%>' +  
            '</div>' + 
        '</div>',

	/**
	 * Select a tab in the tabber.
	 * @param {Number} index The index of the tab to remove.
	 */
	selectTab: function (index) {
		this.getTab(index).select();
	},

	/**
	 * Return the index of the selected tab.
	 * @return The index of the selected tab (if non selected returns -1).
	 * @type Number
	 */
	selectedTab: function () {
		for (var i = 0; i < this.getTabCount(); i++) {
			if (this.getTab(i).selected()) {
				return i;
			}
		}
		return -1;
	},

	/**
	 * Append a tab to the tabber.
	 * @param {String} label (Optional) The label for the tab.
	 * @param {FLUID.Icon} icon (Optional) The icon for the tab.
	 */
	appendTab: function (label, icon) {
		
		// Create the tab and panel
		var panel = new FLUID.Panel();
		var tab = new FLUID.Tabber.Tab({
			panel: panel, 
			label: label, 
			icon: icon
		});
		
		// Add the tab (and associated panel) to the tab
		this.appendChild(tab, 'tabs');
		this.appendChild(panel, 'panels');	
		
		return this.getTabCount() - 1;
	},
	
	/**
	 * Remove a tab from the tabber.
	 * @param {Number} index The index of the tab to remove.
	 */
	removeTab: function (index) {	
		this.getTabPanel(index).remove();
		this.getTab(index).remove();
	},
	
	/**
	 * Set/Get the tab label.
	 * @param {Number} index The index of the tab to set/get the label for.
	 * @param {String} value (Optional) The label to set for the indexed tab, 
	 * if undefined the tabs label is returned.
	 * @return The indexed tabs label.
	 * @type String
	 */
	tabLabel: function (index, value) {
		this.getTab(index).label(value);
	},
	
	/**
	 * Set/Get the tab icon.
	 * @param {Number} index The index of the tab to set/get the icon for.
	 * @param {String} value (Optional) The icon to set for the indexed tab, 
	 * if undefined the tabs icon is returned.
	 * @return The indexed tabs icon.
	 * @type FLUID.Icon
	 */
	tabIcon: function (index, value) {
		this.getTab(index).icon(value);
	},
	
	/**
	 * Get a tab from the tabber.
	 * @param {Number} index The index of the tab to return.
	 * @return The indexed tab.
	 * @type FLUID.Tabber.Tab
	 */
	getTab: function (index) {
		return this._childGroups['tabs'][index];
	},
	
	/**
	 * Get a tab panel from the tabber.
	 * @param {Number} index The index of the tab panel to return.
	 * @return The indexed tab panel.
	 * @type FLUID.Panel
	 */
	getTabPanel: function (index) {
		return this.getTab(index).panel();
	},
	
	/**
	 * Get the number of tabs for the tabber.
	 * @return The number of tabs.
	 * @type Number
	 */
	getTabCount: function () {
		return this._childGroups['tabs'].length;
	}
});


/**
 * A tab for a tabber widget. Typically this class is not created manually, 
 * but by a parent tabber class.
 * @class A tab class.
 * @constructor
 */    
FLUID.Tabber.Tab = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    FLUID.Helpful.call(this, options);
    FLUID.Labelled.call(this, options);
    this._type.push('Tabber.Tab');

	/** private */
	this._panel = options.panel;
	this._panel.hide();
	
	/* private */
	this._selected = false;

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Tabber.Tab, FLUID.Widget);
UTILS.copyPrototype(FLUID.Tabber.Tab, FLUID.Helpful);
UTILS.copyPrototype(FLUID.Tabber.Tab, FLUID.Labelled);

$$.extend(FLUID.Tabber.Tab.prototype, {
    __className__: 'FLUID.Tabber.Tab',
    __css__: FLUID._cssName('tabber-tab'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-click-region"></div>' + 
            '<span class="fluid-icon"></span>' +
            '<span class="fluid-label"><%=widget._label%></span>' +
        '</div>',
    __frameworkEvents__: {
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
    },
    
    /**
     * Set/Get the label for the tab.
     * @param {String} value (Optional) If undefined the current label for
     * the tab is returned, else the tab's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this.getHTMLSelection().children('.fluid-label').html(value);
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the panel associated with the tab.
     * @param {FLUID.Panel} value (Optional) If undefined the current panel
     * for the tab is returned, else the tab's panel is set.
     * @return The panel.
     * @type FLUID.Panel
     */
    panel: function (value) {
        if (value === undefined) {
            return this._panel;
        }
        this.trigger('panel', function () {
            if (this._panel.getId() !== value.getId()) {
                this._panel = value;
            }
        }, {data: {'value': value}});
    },

	/**
     * Select the tab.
     */
    select: EVENT.classMethod(FLUID.Tabber.Tab, function select() {
		if (this.selected() || !this.enabled()) {
			return;
		}
		
		// Unselect any previously selected sibling
		var siblings = this.getSiblings();
		for (var i = 0; i < siblings.length; i++) {
			if (siblings[i].selected()) {
				siblings[i].unselect();
				break;
			}
		}
		
		// Select this tab
		this._selected = true;
		this.addClass('fluid-selected');
		
		// Show the panel associated with this tab
		this.panel().show();
    }, 'select'),

	/**
     * Unselect the tab.
     */
    unselect: EVENT.classMethod(FLUID.Tabber.Tab, function unselect() {
		if (!this.selected()) {
			return;
		}
		
		// Unselect this tab
		this._selected = false;
		this.removeClass('fluid-selected');
		
		// Hide the panel associated with this tab
		this.panel().hide();
    }, 'unselect'),

	/**
	 * Return true if the tab is selected.
	 * @return Whether the tab is selected.
	 * @type Boolean
	 */
	selected: function () {
		return this._selected;
	},

    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        
        if (!this.exists()) {
            return;
        }
        
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Widget.prototype._update.apply(this, [false]);
        FLUID.Helpful.prototype._cUpdate.apply(this);
        
        // Icon
        var iconSelection = this.getHTMLSelection().find(
                '.' + FLUID._cssName('icon'));
        if (this.icon() !== null) {        
            iconSelection.css('background', 
                this.icon().getBackgroundCSS());
            iconSelection.show();
        } else {
            iconSelection.hide();
        }
    
        this.getDesktop().theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        FLUID.Widget.prototype._applyStates.apply(this);
        FLUID.Labelled.prototype._cApplyStates.apply(this);
    },

    /** @private */
    _bindEvents: function () {
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        this.bind('onMouseEnter.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            // Allow text selection again
            document.body.onselectstart = function () {};
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseDown.fluid', function (event) {
            // Prevent text selection
            document.body.onselectstart = function() { return false; }
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.addClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseUp.fluid', function (event) {
            // Allow text selection again
            document.body.onselectstart = function () {};
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });

        this.bind('onClick.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.select();
            }
        });
    },
    
    /**
     * The default options when creating a new tab.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: true,
        enabled: true,
        tooltip: '',
        helpURL: ''
    }
});