/**
 * @fileoverview    Fluid desktop. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widgets) (/js/fluid/widget.js)
 * @requires        FLUID (toast) (/js/fluid/toast.js)
 */

'use strict';

/**
 * A desktop for the UI.
 * @class An desktop class. Desktops act as the top level manager widgets for 
 * other UI widgets. They have no parent widget. 
 * @constructor
 */    
FLUID.Desktop = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    options._bindEvents = false;
    options.name = 'desktop';
    FLUID.Widget.call(this, options, false);
    this._type.push('Desktop');
    
    // Groups
    this._childGroups[this.__defaultClient__] = [];
    this._childGroups['modal'] = [];
    this._childGroups['singleModal'] = [];
    
    // Attributes
    
    /** @private */
    this._state = FLUID.Desktop.MODELESS;
    
    /** @private */
    this._theme = options.theme || new FLUID.Theme({css:[]});
    this._theme.load();
    
    /** @private */
    this._helpContext;
    
    // Create the application bar
    
    /** @private */
    this._applicationBar = new FLUID.ApplicationBar({parent: this});
    
    // Create a notice for the notify method
    this._notice = new FLUID.Notice({owner: this});
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.Desktop, FLUID.Widget);

// Constants
FLUID.Desktop.MODELESS = 1;
FLUID.Desktop.MODAL = 2;
FLUID.Desktop.SINGLE_MODAL = 4;

$$.extend(FLUID.Desktop.prototype, {
    __className__: 'FLUID.Desktop',
    __template__: '<div id="fluid-ns-outer"><div id="fluid-ns-inner"><div id="<%=widget.getHTMLElementId()%>">' + 
            '<div class="<%=FLUID_GROUP_PREFIX%>modeless">' + 
                '<%=group_modeless%>' +
            '</div>' +
            '<div class="<%=FLUID_CSS_PREFIX%>modal-curtain"></div>' + 
            '<div class="<%=FLUID_GROUP_PREFIX%>modal">' + 
                '<%=group_modal%>' +
            '</div>' +
            '<div class="<%=FLUID_CSS_PREFIX%>single-modal-curtain"></div>' + 
            '<div class="<%=FLUID_GROUP_PREFIX%>single-modal">' + 
                '<%=group_singleModal%>' +
            '</div>' +
        '</div></div></div>',
    __defaultClient__: 'modeless',
    __css__: FLUID._cssName('desktop'),
    
    /**
     * Return the desktop (self).
     * @return The desktop (return self).
     * @type FLUID.Desktop
     */
    getDesktop: function () {
        return this;
    },
    
    /**
     * Return the desktop's application bar.
     * @return The desktop's application bar.
     * @type FLUID.ApplicationBar
     */
    getApplicationBar: function () {
        return this._applicationBar;
    },
    
    /**
     * Set/Get the theme for the desktop.
     * @param {String} value (Optional) If undefined the current theme for
     * the desktop is returned, else the desktops widget is set.
     * @return The desktop size.
     * @type Array
     */
    theme: function (value) {
        if (value === undefined) {
            return this._theme;
        }
        this.trigger('theme', function () {
            // Unload the current theme
            this._theme.unload();
            // Set and load the new theme
            this._theme = value;
            this._theme.bind('afterComplete.fluid', 
                function (event, data) {
                    data.desktop.refresh();
                }, {desktop: this});
            
            this._theme.load();
        }, {'value': value});
    },
    
    /**
     * Set/Get the help context for the desktop.
     * @param {String} value (Optional) If undefined the current help context
     * widget for the desktop is returned, else the desktop's help context is
     * set.
     * @return The help context widget.
     * @type FLUID.Widget
     */
    helpContext: function (value) {
        if (value === undefined) {
            return this._helpContext;
        }
        this.trigger('helpContext', function () {
            this._helpContext = value;
        }, {data: {'value': value}});
    },
    
    /**
     * Clear the currently set help context provided it is the same as the
     * specified widget.
     * @param {FLUID.Widget} widget The widget to clear from the current help
     * context.
     */
    clearHelpContext: function (widget) {
        if (this._helpContext !== undefined && this._helpContext.getId() === widget.getId()) {
            this._helpContext = undefined;
        }
    },
    
    /**
     * Return true if the desktop is in a modal or single modal state.
     * @return If the desktop is in a modal state.
     * @type Boolean
     */
    isModal: function () {
        return this._state === FLUID.Desktop.MODAL || 
            this._state === FLUID.Desktop.SINGLE_MODAL;
    },
    
    /**
     * Return the desktops current state.
     * @return The desktops current state.
     * @type Number
     */
    getState: function () {
        return this._state;
    },
    
    /**
     * Start a modal session for the desktop.
     * @param {Number} mode The modal mode to start.
     */
    startModal: function (mode) {
        this.trigger('startModal', function (event) {
            if (!(this._state & mode)) {
                switch (mode) {
    
                case FLUID.Desktop.MODAL:
                    this._state = FLUID.Desktop.MODAL;
                    break;
                    
                case FLUID.Desktop.SINGLE_MODAL:
                    this._state = this._state | FLUID.Desktop.SINGLE_MODAL;
                    break;
                    
                default:
                    break;
                };

                this._update();
            }
        });
    },
    
    /**
     * End modal session for the desktop.
     * @param {Number} mode The modal mode to end.
     */
    endModal: function (mode) {
        this.trigger('endModal', function (event) {
            switch (mode) {

            case FLUID.Desktop.MODAL:
                if ((FLUID.Desktop.MODAL & this._state) 
                        !== FLUID.Desktop.MODAL) {
                    return;
                }
                this._state = FLUID.Desktop.MODELESS;
                this._update();
                break;
                
            case FLUID.Desktop.SINGLE_MODAL:
                if ((FLUID.Desktop.SINGLE_MODAL & this._state) 
                        !== FLUID.Desktop.SINGLE_MODAL) {
                    return;
                }
				
                var children = this._childGroups['singleModal'];
                for (var i = 0; i < children.length; i++) {
					if (children[i].burst !== undefined) {
						children[i].burst(false);
					}
                }
	            this._state = this._state ^ FLUID.Desktop.SINGLE_MODAL;
                this._update();
                break;
                
            default:
                break;
            };
        });
    },
    
    /**
     * Display a notification to the user.
     * @param {String} msg The message to notify the user about.
     */
    notify: function (msg) {
        this._notice.burst();
        this._notice.content(msg);
        this._notice.popup();
    },
    
    /** @private */
    _refresh: function () {
        
        // Remove any existing element from the page and clear the cached
        // version we have
        if (this.exists()) {
            this.getHTMLSelection().unbind().remove();
            this.clearHTMLSelectionCache();
        }
        
        // Generate the HTML for each of the child groups
        var templateVars = {};
        for (var group in this._childGroups) {
            if (this._childGroups.hasOwnProperty(group)) {
                var varName = 'group_' + group;
                templateVars[varName] = '';
                var children = this._childGroups[group];
                for (var i = 0; i < children.length; i++) {
                    var child = children[i];
                    templateVars[varName] += child._refresh();
                }
            }
        }
        var html = this._paint(templateVars);
        
        $$('body').prepend(html);
        this._bind();
        this._update();
    },
    
    /** @private */
    _bindEvents: function () {
        // Support for context sensitive help (EXPERIMENTAL)
        $$(document).data('fluid-desktop', this);
        $$(document).bind('keypress.fluid', function (event) {
            if (event.which === 47 || event.which === 63) { // The question mark key
                var desktop = $$(document).data('fluid-desktop');
                if (desktop.helpContext() && 
                        desktop.helpContext().helpURL() !== '') {
                    window.open(desktop.helpContext().helpURL());
                    event.stopPropagation();
                }
            }
        });
        // Support for single modal click close
        $$(document).delegate('.' + FLUID._cssGroup('singleModal'), 'click.fluid', 
            function(event) {
                var desktop = $$(document).data('fluid-desktop');
                desktop.endModal(FLUID.Desktop.SINGLE_MODAL);
            });
    },
    
    /** @private */
    _applyStates: function () {
        if (!this.exists()) {
            return;
        }
        var modeless = this.getHTMLSelection('modeless');
        var modal = this.getHTMLSelection('modal');
        var modalCurtain = this.getHTMLSelection().children(
            '.' + FLUID._cssName('modal-curtain'));
        var singleModal = this.getHTMLSelection('singleModal');
        var singleModalCurtain = this.getHTMLSelection().children(
            '.' + FLUID._cssName('single-modal-curtain'));
        
        if ((FLUID.Desktop.MODAL & this._state) === FLUID.Desktop.MODAL) {
            modal.show();
            modalCurtain.show();
        } else {
            modal.hide();
            modalCurtain.hide();
        }
        if ((FLUID.Desktop.SINGLE_MODAL & this._state) 
                === FLUID.Desktop.SINGLE_MODAL) {
            singleModal.show();
            singleModalCurtain.show();
        } else {
            singleModal.hide();
            singleModalCurtain.hide();
        }
    }
});


/**
 * An application bar for the UI.
 * @class An application bar class. 
 * @constructor
 */    
FLUID.ApplicationBar = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    options._bindEvents = false;
    FLUID.Widget.call(this, options);
    this._type.push('Application');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.ApplicationBar, FLUID.Widget);

$$.extend(FLUID.ApplicationBar.prototype, {
    __className__: 'FLUID.ApplicationBar',
    __template__: '<div class="fluid-application-bar-shadow"></div>' +
        '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<div class="fluid-group-widget">' + 
                '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +
            '</div>' +  
        '</div>',
    __css__: FLUID._cssName('application-bar')
});