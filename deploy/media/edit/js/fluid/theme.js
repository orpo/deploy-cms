/**
 * @fileoverview    Fluid theme. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 */

'use strict';

/**
 * A theme for the UI. When initialising a Desktop the theme can be
 * specified. Themes can load additional CSS and widget templates from remote
 * locations which can then override the default behaviour for a widget.
 * @class A theme class.
 * @constructor
 * @param {Object} options (Optional) The options for the icon, all of the 
 * options are optional.
 * @param {Array} css (Option) A list of css URLs for the theme.
 * @param {Array} resources (Option) A list of resource (JS) file URLs for 
 * the them.
 * @param {Array} templates (Option) A list of template (JS) file URLs for 
 * the them.
 * @param {Array} extensions (Option) A list of extension (JS) file URLs for 
 * the them.
 */
FLUID.Theme = function (options) {
    EVENT.EventClass.call(this);
    
    /** @private */
    this._unloaded = [];
    
    /** @private */
    this._loading = '';
    
    /** @private */
    this._resources = new FLUID.ResourceManager();
    
    /** @private */
    this._templates = {};
    
    /** @private */
    this._extensions = {};
    
    /** @private */
    this._remoteItems = [];
    
    /** @private */
    this._name = options.name; 
    
    // Build the remote items stack
    var i = 0;
    var css = options.css || [];
    for (i = 0; i < css.length; i++) {
        this._remoteItems.push({url: css[i], type: 'stylesheet'});
    }
    
    var resources = options.resources || [];
    for (i = 0; i < resources.length; i++) {
        this._remoteItems.push({url: resources[i], type: 'resource'});
    } 
    
    var templates = options.templates || [];
    for (i = 0; i < templates.length; i++) {
        this._remoteItems.push({url: templates[i], type: 'template'});
    } 
    
    var extensions = options.extensions || [];
    for (i = 0; i < extensions.length; i++) {
        this._remoteItems.push({url: extensions[i], type: 'extension'});
    }
};
UTILS.copyPrototype(FLUID.Theme, EVENT.EventClass);

$$.extend(FLUID.Theme.prototype, {
    __className__: 'FLUID.Theme',
    
    /**
     * Return the name of the theme.
     * @return The theme's name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    /**
     * Return the resource manager for the them.
     * @return The resource manager for the theme.
     * @type String
     */
    getResources: function () {
        return this._resources;
    },
    
    /**
     * Set a widgets template for the theme.
     * @param {Class|FLUID.Widget|String} The class or an instance of the 
     * class to set the template for.
     * @param {String} The template to set.
     **/
    setTemplate: function (widget, template) {
        if (typeof(widget) === 'string') { // String
            this._templates[widget] = template;
        } else if (widget.prototype === undefined) { // Instance
            this._templates[widget.__className__] = template;
        } else { // Class
            this._templates[widget.prototype.__className__] = template;
        }
    },
    
    /**
     * Return a template for the widget using this theme.
     * @param {Class|FLUID.Widget} widget The class or an instance of the 
     * class to return a template for.
     * @return The template for the widget.
     * @type String
     **/
    getTemplate: function (widget) {
        if (widget.prototype === undefined) { // Instance
            if (this._templates[widget.__className__] === undefined) {
                return widget.__template__;
            }
            return this._templates[widget.__className__];
        } else { // Class
            if (this._templates[widget.prototype.__className__] === 
                    undefined) {
                return widget.__template__;
            }
            return this._templates[widget.prototype.__className__];
        }
    },
    
    /**
     * Set a widgets layout extension for the theme.
     * @param {Class|FLUID.Widget} The class or an instance of the class
     * to set the layout extension for.
     * @param {Function} The layout extension to set.
     **/
    setExtension: function (widget, extension) {
        if (typeof(widget) === 'string') { // String
            this._extensions[widget] = extension;
        } else if (widget.prototype === undefined) { // Instance
            this._extensions[widget.__className__] = extension;
        } else { // Class
            this._extensions[widget.prototype.__className__] = 
                extension;
        }
    },
    
    /**
     * Return a layout extention for the widget using this theme.
     * @param {Class|FLUID.Widget} widget The class or an instance of the 
     * class to return a layout extension for.
     * @return The layout extention for the widget.
     * @type Function
     **/
    getExtension: function (widget) {
        if (widget.prototype === undefined) { // Instance
            if (this._extensions[widget.__className__] !== undefined) {
                return this._extensions[widget.__className__];
            }
            return;
        } else { // Class
            if (this._extensions[widget.prototype.__className__] !== 
                    undefined) {
                return this._extensions[widget.prototype.__className__];
            }
            return;
        }
    },
    
    /**
     * Call an extension for the specified widget.
     * @param {FLUID.Widget} widget The widget to call the extension for.
     * @param {Number} phase The phase (ON or AFTER) the extension is being
     * called at.
     * @param {Boolean} supportExtensions If true (defaults to true) then the
     * extension if it exists is called.
     */
    callExtension: function (widget, phase, supportExtensions) {
        if (supportExtensions === undefined || supportExtensions) {
            var extension = this.getExtension(widget);
            if (extension !== undefined) {
                extension.call(widget, phase); 
            }
        }
    },
    
    /**
     * Load the theme, typically this should only be called internally by the
     * desktop.
     */
    load: EVENT.classMethod(FLUID.Theme, function load() {
        // Reset the unloaded stack
        $$(window).data('fluid-theme', this);
        this._unloaded = this._remoteItems.slice();
        this._next();    
    }, 'load'),
    
    /**
     * Unload the theme, typically this should only be called internally by 
     * the desktop.
     */
    unload: EVENT.classMethod(FLUID.Theme, function unload() {
        for (var i = 0; i < this._remoteItems.length; i++) {
            var remoteItem = this._remoteItems[i];
            if (remoteItem.type === 'stylesheet') {
                $$('link[href="' + remoteItem.url + '"]').remove();
            } else {
                $$('script[src="' + remoteItem.url + '"]').remove();
            }
        }
        this._resources.removeAll();
        this._templates = {};
        this._extensions = {};
    }, 'unload'),
    
    /** @private */
    _next: function () {
        // Check for completed load
        if (this._unloaded.length === 0) {
            this.trigger('complete', function () {
                $$(window).data('fluid-theme', undefined);
            });
            return;
        }
        // Load next url
        var remoteItem = this._unloaded.shift();
        switch (remoteItem.type) {

        case 'stylesheet':
            this._loadStyleSheet(remoteItem.url);
            break;

        case 'resource':
            this._loadResource(remoteItem.url);
            break;

        case 'template':
            this._loadTemplate(remoteItem.url);
            break;

        case 'extension':
            this._loadExtension(remoteItem.url);
            break;

        default:
            break;
        }
    },
    
    /** @private */
    _loadStyleSheet: function (url) {
        this._loading = url;
        $$('head').append(
            '<link media="all" rel="stylesheet" type="text/css" href="' +
            url + 
            '" />');
        this._next();
    },
    
    /** @private */
    _loadResource: function (url) {
        this._loading = url;
        $$.getScript(url, function () {
            var theme = $$(window).data('fluid-theme');
            var url = theme._loading;
            var resources = fluidResources[url];
            for (var key in resources) {
                if (resources.hasOwnProperty(key)) {
                    theme.getResources().set(key, resources[key]);
                }
            }
            theme._next();
        });
    },
    
    /** @private */
    _loadTemplate: function (url) {
        this._loading = url;
        $$.getScript(url, function () {
            var theme = $$(window).data('fluid-theme');
            var url = theme._loading;
            var templates = fluidTemplates[url];
            for (var key in templates) {
                if (templates.hasOwnProperty(key)) {
                    theme.setTemplate(key, templates[key]);
                }
            }
            theme._next();
        });
    },
    
    /** @private */
    _loadExtension: function (url) {
        this._loading = url;
        $$.getScript(url, function () {
            var theme = $$(window).data('fluid-theme');
            var url = theme._loading;
            var extensions = fluidExtensions[url];
            for (var key in extensions) {
                if (extensions.hasOwnProperty(key)) {
                    theme.setExtension(key, extensions[key]);
                }
            }
            theme._next();
        });
    }
});