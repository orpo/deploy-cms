/**
 * @fileoverview    Fluid icon. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 */

'use strict';

/**
 * An icon for the UI.
 * @class An icon class.  
 * @constructor                                                         
 * @param {Object} options (Optional) The options for the icon.
 * @param {String} imageUrl (Option) The URL of the image for the icon.
 * @param {Array} offset (Option) The offset (in multiples of it's size of
 * the icon) (defaults to [0, 0]).
 * @param {Number} size (Option) The size of the icon (defaults
 * to 16).
 */    
FLUID.Icon = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    this._imageUrl = options.imageUrl;
    this._offset = options.offset || [0, 0];
    this._size = options.size;
};

$$.extend(FLUID.Icon.prototype, {
    __className__: 'FLUID.Icon',
    
    /**
     * Return the CSS style need to display the icon as a background image.
     * @return The CSS style for the icon as a background.
     * @type String
     */
    getBackgroundCSS: function () {
        var position = this._getCSSPosition(this._size);
        return UTILS.template(
            'transparent url(' + this._imageUrl + ') no-repeat ' +
            '-<%=position[0]%>px -<%=position[1]%>px',
            {'size': this._size, 'position': position});
    },
    
    /**
     * The default options when creating a new icon.
     * @type Object
     */
    defaultOptions: {
        imageUrl: '',
        size:16
    },
    
    /** @private */
    _getCSSPosition: function () {
        return [this._offset[0] * this._size, this._offset[1] * this._size];
    }
});

/**
 * An class for managing a set of icons for the UI.
 * @class An icon set class.  
 * @constructor                          
 * @param {Object} options (Optional) The options for the icon set.
 * @param {String} imageUrl (Option) The URL of the image for the icon set.
 * @param {Number} size (Option) The size of the icons in the set.
 * @param {Number} cols The number of columns in the icon set.
 * @param {Number} count The total number of icons in the set. 
 */    
FLUID.IconSet = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);

    /** private */
    this._icons = [];

    /** private */
    this._cols = options.cols;

    var col = 0;
    var row = 0;
    for (var i = 0; i < options.count; i++) {
        if (this._cols !== 0 && col >= this._cols) {
            col = 0;
            row++;
        }
        var icon = new FLUID.Icon({imageUrl: options.imageUrl,
            offset: [col, row],
            size: options.size});
        this._icons.push(icon);
        col++;
    }
};

$$.extend(FLUID.IconSet.prototype, {
    __className__: 'FLUID.IconSet',

    /**
     * Return an icon by it's index or grid position (col, row).
     * @param {Number|Array} index The index or grid position (as a two item 
     * array e.g. [col, row]) of the icon to return.
     * @return An icon from the set.
     * @type FLUID.Icon
     */
    getIcon: function (index) {
        if (index instanceof Array) {
            index = index[0] + index[1] * this._cols;
        }
        return this._icons[index];
    }, 

    /**
     * The default options when creating a new icon set.
     * @type Object
     */
    defaultOptions: {
        imageUrl: '',
        size: 16,
        cols: 0,
        count: 0
    }
});