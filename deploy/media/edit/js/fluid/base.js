/**
 * @fileoverview    Fluid base (Part of Fluid UI library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

/**
 * The FLUID namespace.
 * @final
 * @type Namespace
 */  
var FLUID = {__global_ids__: {}, __global_names__: {}};

// Constants
var FLUID_ID_PREFIX = 'fluid-';
var FLUID_CSS_PREFIX = 'fluid-';
var FLUID_GROUP_PREFIX = 'fluid-group-';
var FLUID_DEFAULT_GROUP = 'client';

/**
 * Return a widget with the specified ID.
 * @param {Number} The widgets ID.
 * @return A widget.
 * @type FLUID.Widget
 */
FLUID.widgetById = function (id) {
    return FLUID.__global_ids__[id];
};

/**
 * Return a widget with the specified name.
 * @param {String} The widgets name.
 * @return A widget.
 * @type FLUID.Widget
 */
FLUID.widgetByName = function (name) {
    return FLUID.__global_names__[name];
};

/** @private */
FLUID._cssName = function (name) {
    return FLUID_CSS_PREFIX + UTILS.camelToDash(name);
}

/** @private */
FLUID._cssGroup = function (name) {
    return FLUID_GROUP_PREFIX + UTILS.camelToDash(name);
}

/** @private */
FLUID._toPixels = function (value, range) {
    if (value.constructor === Number) {
        return Math.floor(value);
    } else if (value === '*') {
        return value;
    } else if (value.match(/^\d+%$/)) {
        value = Number(value.replace('%', ''));
        return Math.floor((value / 100.0) * range)
    } else if (value.match(/^\d+(px)*$/)) {
        return Number(value.replace('px', ''));
    } else {
        throw new Error('Unable to convert value to ' + value + 'pixels');
    }
};