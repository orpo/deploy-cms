/**
 * @fileoverview    Fluid data handling classes. (Part of Fluid UI).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (theme) (/js/fluid/theme.js)
 * @requires        FLUID (widget) (/js/fluid/widget.js)
 */

'use strict';

/**
 * A table class for displaying data in a listed format.
 * @class A table class.
 * @constructor
 */
FLUID.DataTable = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('DataTable');
    
    /** @private */
    this._header = options.header || [];
    
    /** @private */
    this._data = options.data || [];
    
    /** @private */
    this._headerCls = options.headerClass || FLUID.DataTableHead;
    
    /** @private */
    this._rowClass = options.rowClass || FLUID.DataTableRow;
    
    /** @private */
    this._cellClass = options.cellClass || FLUID.DataTableCell;
    
    // Groups
    this._childGroups['header'] = [];
    this._childGroups[this.__defaultClient__] = [];
    
    this._buildHeader();
    this._buildBody();
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.DataTable, FLUID.Widget);

$$.extend(FLUID.DataTable.prototype, {
    __className__: 'FLUID.DataTable',
    __template__: '<table id="<%=widget.getHTMLElementId()%>">' +
            '<% if (group_header) { %><thead><tr><%=group_header%></tr></thead><% } %>' +
            '<tbody><%=group_' + FLUID_DEFAULT_GROUP + '%></tbody>' +
        '</table>',
    __css__: FLUID._cssName('dataTable'),
    
    /**
     * Get/Set the table header.
     * @param {Array} value If undefined the current header options for
     * the table are returned, else the current header is set.
     * @return The current header options.
     * @type Array
     */
    header: function (value) {
        if (value === undefined) {
            return this._header;
        }
        this.trigger('header', function () {
            this._header = value;
            this._buildHeader();
        }, {data: {'value': value}});
    },
    
    /**
     * Get/Set the table data.
     * @param {Array} value If undefined the current header options for
     * the table are returned, else the current header is set.
     * @return The current header options.
     * @type Array
     */
    data: function (value) {
        if (value === undefined) {
            return this._data;
        }
        this.trigger('data', function () {
            this._data = value;
            this._buildBody();
        }, {data: {'value': value}});
    },
    
    /**
     * Get/Set the table header class (used to build the table header 
     * columns).
     * @param {FLUID.DataTableHead} value If undefined the current header 
     * class for the table is returned, else the current header class
     * is set.
     * @return The current header class.
     * @type FLUID.DataTableHead
     */
    headerClass: function (value) {
        if (value === undefined) {
            return this._headerClass;
        }
        this.trigger('headerClass', function () {
            this._headerClass = value;
            this._buildHeader();
        }, {data: {'value': value}});
    },
    
    /**
     * Get/Set the table row class (used to build the table rows).
     * @param {FLUID.DataTableRow} value If undefined the current row 
     * class for the table is returned, else the current row class is set.
     * @return The current row class.
     * @type FLUID.DataTableRow
     */
    rowClass: function (value) {
        if (value === undefined) {
            return this._rowClass;
        }
        this.trigger('rowClass', function () {
            this._rowClass = value;
            this._buildBody();
        }, {data: {'value': value}});
    },
    
    /**
     * Get/Set the table cell class (used to build the table cells).
     * @param {FLUID.DataTableCell} value If undefined the current cell 
     * class for the table is returned, else the cell header class is set.
     * @return The current cell class.
     * @type FLUID.DataTableCell
     */
    cellClass: function (value) {
        if (value === undefined) {
            return this._cellClass;
        }
        this.trigger('cellClass', function () {
            this._cellClass = value;
            this._buildBody();
        }, {data: {'value': value}});
    },
    
    /** @private */
    _buildHeader: function () {
        // Clear existing header widgets
        this.removeAllChildren('header');
        
        // Create the header again
        for (var i = 0; i < this._header.length; i++) {
            var headingOptions = this._header[i];
            var headingWidget = new this._headerCls(headingOptions);
            this._appendChild(headingWidget, 'header');
        }
    },
    
    /** @private */
    _buildBody: function () {
        // Clear existing body widgets
        this.removeAllChildren(FLUID_DEFAULT_GROUP);
        
        // Create the header again
        for (var i = 0; i < this._data.length; i++) {
            // Add a row to the body
            var row = new this._rowClass({classes: [['fluid-odd', 'fluid-even'][i % 2]] });
            for (var j = 0; j < this._data[i].length; j++) {
                var cell = new this._cellClass(this._data[i][j]);
                row.appendChild(cell);
                this.trigger('addCell', function (event, data) {}, {data: {cell: cell}});
            }
            this.appendChild(row);
            this.trigger('addRow', function (event, data) {}, {data: {row: row}});
        }
    },
    
    /**
     * The default options when creating a new button.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: true,
        enabled: true,
        tooltip: '',
        helpURL: ''     
    }
});



/**
 * A data table row for holding data table cells.
 * @class A data table row class.
 * @constructor
 */  
FLUID.DataTableRow = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    options._bindEvents = false;
    FLUID.Widget.call(this, options);
    this._type.push('DataTableRow');
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(FLUID.DataTableRow, FLUID.Widget);

$$.extend(FLUID.DataTableRow.prototype, {
    __className__: 'FLUID.DataTableRow',
    __template__: '<tr id="<%=widget.getHTMLElementId()%>">' +
            '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +
        '</tr>',
    __css__: FLUID._cssName('dataTableRow'),
    __frameworkEvents__: {
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter'
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Helpful.prototype._cBindEvents.apply(this);
        
        this.bind('onMouseEnter.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        
        this.bind('onMouseLeave.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
    },
    
    /**
     * The default options when creating a new widget.
     * @type Object
     */
    defaultOptions: {
        visible: true,
        enabled: true
    }
});


/**
 * A data table cell.
 * @class A data table cell class.
 * @constructor
 */
FLUID.DataTableCell = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('DataTableCell');
    
    /** @private */
    this._value = options.value;
    
    /** @private */
    this._data = options.data;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.DataTableCell, FLUID.Widget);

$$.extend(FLUID.DataTableCell.prototype, {
    __className__: 'FLUID.DataTableCell',
    __template__: '<td id="<%=widget.getHTMLElementId()%>">' +
            '<%=widget._value%>' +
        '</td>',
    __frameworkEvents__: {
        'mouseover': 'mouseOver',
        'mouseout': 'mouseOut',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
        },
    __css__: FLUID._cssName('dataTableCell'),

    /**
     * Set/Get the value for the cell.
     * @param {String} value (Optional) If undefined the current value for
     * the cell is returned, else the cell's value is set.
     * @return The value.
     * @type String
     */
    value: function (value) {
        if (value === undefined) {
            return this._value;
        }
        this.trigger('value', function () {
            if (this._value !== value) {
                this._value = value;
                if (this.exists()) {
                    // @@ IMPLEMENT (WHAT SHOULD THIS DO)?? 
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get the data for the cell.
     * @param {String} value (Optional) If undefined the current data for the
     * cell is returned, else the cell's data is set.
     * @return The data.
     * @type *
     */
    data: function (value) {
        if (value === undefined) {
            return this._data;
        }
        this.trigger('data', function () {
            if (this._data !== value) {
                this._data = value;
            }
        }, {data: {'value': value}});
    },
    
    /**
     * The default options when creating a new data table head.
     * @type Object
     */
    defaultOptions: {
        value: '',
        visible: true,
        enabled: true
    }
});


/**
 * A data table column header.
 * @class A data table column head class.
 * @constructor
 */
FLUID.DataTableHead = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('DataTableHead');
    
    /** @private */
    this._label = options.label;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(FLUID.DataTableHead, FLUID.Widget);

$$.extend(FLUID.DataTableHead.prototype, {
    __className__: 'FLUID.DataTableHead',
    __template__: '<th id="<%=widget.getHTMLElementId()%>">' +
            '<%=widget._label%><span class="deploycms-sort"></span>' +
        '</th>',
    __css__: FLUID._cssName('dataTableHead'),
        
    /**
     * Set/Get the label for the widget.
     * @param {String} value (Optional) If undefined the current label for
     * the widget is returned, else the widget's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
                if (this.exists()) {
                    this._update();
                }
            }
        }, {data: {'value': value}});
    },
    
    /**
     * The default options when creating a new data table head.
     * @type Object
     */
    defaultOptions: {
        label: '',
        visible: true,
        enabled: true
    }
});