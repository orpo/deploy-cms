/**
 * @fileoverview    Editable table elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 * @requires        EDITABLE (text) (/js/editable/text.js)
 */

'use strict';

// @@ Todo
// - Add an integrity function to ensure a table is made stable after something is deleted.
// - Navigation support for rowspan

/**
 * An editable table element.
 * @class An editable table element.
 * @param {Object} options The options for the editable table element.
 * @param {String} htmlTag The HTML element type the editable element 
 * represents.
 * @param {String} style The elements style.
 * @constructor
 */    
EDITABLE.Table = function (options) {
    EDITABLE.HTMLCollection.call(this);
    EDITABLE.HTMLElement.call(this);
    this._type.push('Table');
    
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    this.htmlTag(options.htmlTag);
    this.styles(options.styles || []);
};
UTILS.copyPrototype(EDITABLE.Table, EDITABLE.HTMLCollection);
UTILS.copyPrototype(EDITABLE.Table, EDITABLE.HTMLElement);

$$.extend(EDITABLE.Table.prototype, {
    
    __className__: 'EDITABLE.Table',
    
    /*
    insertColumn:
    
    removeColumn:
    */
    
    /**
     * Return the specified section from the table.
     * @param {String} section The section to return.
     * @return A section from the table.
     * @type EDITABLE.TableSection
     */
    getSection: function (section) {
        for (var i = 0; i < this._children.length; i++) {
            var child = this._children[i];
            if (child.typeOf('TableSection') && child.htmlTag() === section) {
                return child;
            }
        }
    },
    
    /**
     * Add the editable table to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this.htmlTag(),
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                styles: this.styles() ? this.styles().join(' ') : ''
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' + 
                '<%=cls%> <%=styles%>"></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this.getParent().getHTMLElement()).prepend(html);
            }
            
            // Render the children
            for (var i = 0; i < this._children.length; i++) {
                this._children[i].addHTML();
            }
                
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
            
            // Bind events to the HTML
            this._bindLayoutEvents();
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        // Generate the HTML for the children first
        var children = '';
        for (var i = 0; i < this._children.length; i++) {
            children += '\n' + EDITABLE_TAB + this._children[i].toHTML();
        }
        // Generate the HTML for the list
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : ''
        };
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%><%=styles%>">',
            templateVars);
        
        html += children + '\n</' + this._htmlTag + '>';
            
        return html;
    },
    
    /**
     * Return a copy of the Table element.
     * @return A copy of the Table element.
     * @type EDITABLE.Table
     */
    copy: function () {
        var tableCopy = new EDITABLE.Table({htmlTag: this.htmlTag(),
            styles: this.styles().slice(0)
            });
        for (var i = 0; i < this._children.length; i++) {
            tableCopy.appendChild(this._children[i].copy());
        }
        return tableCopy;
    },
    
    /**
     * The HTML element types that editable table elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['table'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'table'
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());
    
        // Dragging
        htmlElement.bind('mousedown.editable', this, function (event) {
                
            // Ignore right mouse clicks
            if (event.button === 2) {
                return;
            }
            
            event.data._hasMouse = true;
            var mousedown_event = event;
                
            /*
            This avoids a potential FireFox and Safari issue caused by 
            selected text not reporting mouse[over/out] events. By 
            removing the selection we ensure this situation can't 
            arise.
            */            
            TEXT_SELECTION.clear();
            
            $$(this).oneTime(EDITABLE_DRAG_DOUBLE_HOLD_DURATION, 
                    'drag.editable', function (event) {

                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }

                // Prepare the element for dragging
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 
                    'selected');
                
                // Calculate the cursor offset to start dragging from
                var margin = $$(this).margin();
                var cursor_left = -Math.max(0, margin.left);
                var cursor_top = -Math.max(0, margin.top);
                
                $$(this).draggable({
                    revert: 'valid',
                    revertDuration: 0,
                    cursor: 'move',
                    cursorAt: {left: cursor_left, top: cursor_top},
                    helper: function () {
                        if ($$(this).data('editableElement')) {
                            return $$(this).data(
                                'editableElement')._dropperHTML();
                        }
                    },
                    drag: function (event, ui) {
                        if ($$(this).data('editableElement')) {
                            $$(this).data(
                                'editableElement')._draggableDrag(
                                    this, event, ui);
                        }
                    },
                    stop: function (event, ui) {
                        if ($$(this).data('editableElement')) {
                            $$(this).data(
                                'editableElement')._draggableStop(
                                    this, event, ui);
                        }
                    }
                });
                
                $$(this).data('editableElement')._hasDragged = false;
                EDITABLE.HTMLElement._triggerDrag(this, mousedown_event);
            });
        });        
        
        // Mouse up
        htmlElement.bind('mouseup.editable', this, function (event) {
            event.data._hasMouse = false;
            $$(this).stopTime('drag.editable');
            var dragElement = $$(this).data('editableElement');
            if ($$(this).data('draggable') && !dragElement._hasDragged) {
                $$(this).draggable({destroy: true});
                event.data.updateHTML();
            }
            event.data._prevMouseX = null;
            event.data._prevMouseY = null;
        });

        // Mouse out
        htmlElement.bind('mouseout.editable', this, function (event) {
            if (event.data._hasMouse) {
                $$(this).stopTime('drag.editable');
            }
        });

        // Mouse move
        htmlElement.bind('mousemove.editable', this, function (event) {
            if (event.data._hasMouse) {
                if ((event.data._prevMouseX !== null && 
                    event.data._prevMouseY !== null) && 
                    (event.data._prevMouseX !== event.pageX || 
                    event.data._prevMouseY !== event.pageY)) {
                    
                    $$(this).stopTime('drag.editable');
                }
                event.data._prevMouseX = event.pageX;
                event.data._prevMouseY = event.pageY;
            }
        });
                    
        // Dropping
        htmlElement.droppable({
            greedy: true,
            drop: function (event, ui) {
                $$(this).data('editableElement')._droppableDrop(this, 
                    event, ui);
            },
            over: function (event, ui) {
                $$(this).data('editableElement')._droppableOver(this, 
                    event, ui);
            },
            out: function (event, ui) {
                $$(this).data('editableElement')._droppableOut(this, 
                    event, ui);
            }
        });
    },
    
    /** @private */
    _dropperHTML: function () {
        var content = UTILS.abbreviate($$(this.getHTMLElement()).text(), 
            EDITABLE_HELPER_CONTENT_LENGTH);
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + 'Table',
            content: content
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' +
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            '<%=content%></div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' + 
            'dropper-overlay"></div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _defaultChildType: 'TableSection',
     
    /** @private */
    _supportedChildTypes: ['Caption', 'TableSection'],
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @ignore */
    align: undefined,
    
    /** @ignore */
    _align: undefined
});

/**
 * Convert a HTML element into a Table editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An Table editable element.
 * @type EDITABLE.Table
 */
EDITABLE.Table.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var styles = EDITABLE._getStylesFromCSS(css);
    var options = {
        htmlTag: htmlElement.nodeName,
        styles: styles
    };
    var table = new EDITABLE.Table(options);
    var htmlChildren = $$(htmlElement).children();
    for (var i = 0; i < htmlChildren.length; i++) {
        var htmlChild = htmlChildren[i];
        
        // Children
        var childCss = $$(htmlChild).attr('class');
        var className = EDITABLE._getSettingFromCSS(
            EDITABLE_CSS_CLASS_PREFIX, childCss);
        
        if (className.length > 0) {
            if (EDITABLE[className[0]] === undefined) {
                throw new Error('EDITABLE class \'' + className + 
                    '\' not found');
            }
            var child = EDITABLE[className[0]].fromHTML(this, htmlChild);
            table.appendChild(child);
        } else {
            throw new Error('No EDITABLE class name found for HTML element');
        }
    }
    return table;
};

// Define which element classes can hold Table elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'Table');

// Define that the Table element can contain Caption elements
EDITABLE.defineSupportedChildType(EDITABLE.Table, 'Caption');

// Define droppers
EDITABLE.defineDropper(
    EDITABLE.Table, 
    [
        EDITABLE.Text, 
        EDITABLE.List, 
        EDITABLE.Rule,
        EDITABLE.Table
    ],  
    EDITABLE.HTMLElement.DRAG_VERTICAL[0],
    EDITABLE.HTMLElement.DRAG_VERTICAL[1]
);

EDITABLE.defineDropper(
    [
        EDITABLE.Text, 
        EDITABLE.List, 
        EDITABLE.Rule
    ],  
    EDITABLE.Table, 
    EDITABLE.HTMLElement.DRAG_VERTICAL[0],
    EDITABLE.HTMLElement.DRAG_VERTICAL[1]
);


/**
 * An editable table section element.
 * @class An editable table section element.
 * @param {Object} options The options for the editable table section element.
 * @param {String} htmlTag The HTML element type the editable section element 
 * represents.
 * @param {String} style The elements style.
 * @constructor
 */    
EDITABLE.TableSection = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLCollection.call(this);
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('TableSection');
    
    this.htmlTag(options.htmlTag);
    this.styles(options.styles || []);
};
UTILS.copyPrototype(EDITABLE.TableSection, EDITABLE.HTMLCollection);
UTILS.copyPrototype(EDITABLE.TableSection, EDITABLE.HTMLElement);

$$.extend(EDITABLE.TableSection.prototype, {
    
    __className__: 'EDITABLE.TableSection',
    
    /**
     * Insert a row in to the table section.
     * @param {Number} index The index at which to insert the row.
     */
    insertRow: function (index) {
        this.trigger('insertRow', function (event) {
            var newRow = null;
            var i = 0;
            
            // Are there any other rows in the table section
             
                // Will this be the first row?
                    
                    // Use the
                    
                // Else
                    
                    // Use the previous row
                    
            // Are there any other table sections
            
                // Use them to determine the number of columns and col spans
            
            // Else
             
             // Default
            
        });
    },
    
    // @@ Do we need remove row
    
    /**
     * Add the editable table section to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this.htmlTag(),
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                styles: this.styles() ? this.styles().join(' ') : ''
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' + 
                '<%=cls%> <%=styles%>"></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this.getParent().getHTMLElement()).prepend(html);
            }
            
            // Render the children
            for (var i = 0; i < this._children.length; i++) {
                this._children[i].addHTML();
            }
                
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
            
            // Bind events to the HTML
            this._bindLayoutEvents();
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        // Generate the HTML for the children first
        var children = '';
        for (var i = 0; i < this._children.length; i++) {
            children += '\n' + UTILS.mulStr(EDITABLE_TAB, 2) + this._children[i].toHTML();
        }
        // Generate the HTML for the list
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
            'children': children
        };
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%><%=styles%>">',
            templateVars);
        
        html += children + '\n' + EDITABLE_TAB + '</' + this._htmlTag + '>';
            
        return html;
    },
    
    /**
     * Return a copy of the TableSection element.
     * @return A copy of the TableSection element.
     * @type EDITABLE.Table
     */
    copy: function () {
        var tableSectionCopy = new EDITABLE.TableSection({htmlTag: this.htmlTag(),
            styles: this.styles().slice(0)
            });
        for (var i = 0; i < this._children.length; i++) {
            tableSectionCopy.appendChild(this._children[i].copy());
        }
        return tableSectionCopy;
    },
    
    /**
     * The HTML element types that editable table elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['thead', 'tbody', 'tfoot'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'tbody'
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        
    }, 
    
    /** @private */
    _defaultChildType: 'TableRow',
     
    /** @private */
    _supportedChildTypes: ['TableRow'],
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @ignore */
    align: undefined,
    
    /** @ignore */
    _align: undefined
});

/**
 * Convert a HTML element into a TableSection editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An TableSection editable element.
 * @type EDITABLE.TableSection
 */
EDITABLE.TableSection.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var styles = EDITABLE._getStylesFromCSS(css);
    var options = {
        htmlTag: htmlElement.nodeName,
        styles: styles
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    var tableSection = new EDITABLE.TableSection(options);
    var htmlChildren = $$(htmlElement).children();
    for (var i = 0; i < htmlChildren.length; i++) {
        var htmlChild = htmlChildren[i];
        
        // Children
        var childCss = $$(htmlChild).attr('class');
        var className = EDITABLE._getSettingFromCSS(
            EDITABLE_CSS_CLASS_PREFIX, childCss);
        
        if (className.length > 0) {
            if (EDITABLE[className[0]] === undefined) {
                throw new Error('EDITABLE class \'' + className + 
                    '\' not found');
            }
            var child = EDITABLE[className[0]].fromHTML(this, htmlChild);
            tableSection.appendChild(child);
        } else {
            throw new Error('No EDITABLE class name found for HTML element');
        }
    }
    return tableSection;
};

// Define which element classes can hold TableSection elements
EDITABLE.defineSupportedChildType(EDITABLE.Table, 'TableSection');


/**
 * An editable table row element.
 * @class An editable table row element.
 * @param {Object} options The options for the editable table row element.
 * @param {String} htmlTag The HTML element type the editable row element 
 * represents.
 * @param {String} style The elements style.
 * @constructor
 */    
EDITABLE.TableRow = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLCollection.call(this);
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('TableRow');
    
    this.htmlTag(options.htmlTag);
    this.styles(options.styles || []);

    /** @private */
    this._clickedOuter = false;
};
UTILS.copyPrototype(EDITABLE.TableRow, EDITABLE.HTMLCollection);
UTILS.copyPrototype(EDITABLE.TableRow, EDITABLE.HTMLElement);

$$.extend(EDITABLE.TableRow.prototype, {
    
    __className__: 'EDITABLE.TableRow',
    
    /**
     * Return the column count for the row, taking into account colspans.
     * @return The column count for the row.
     * @type Number
     */
    getColumnCount: function () {
        /*
        WARNING! This could get very slow on large tables.
        */
        
        var columnCount = 0;
        
        // For every row before this row check for columns with rowspans 
        // greater than 1.
        var rowIndex = this.getParent().indexOfChild(this);
        for (var i = 0; i < rowIndex; i++) {
            var row = this.getParent().getChildren()[i];
            for (var j = 0; j < row.getChildCount(); j++) {
                var col = row.getChildren()[j];
                // If a rowspan is greater than 1 determine if it spans this row
                if (col.rowspan() > 1) {
                    // If so increase the column count by 1
                    if ((col.rowspan() + j) >= rowIndex) {
                        columnCount++;
                    }
                }
            }
        }
        
        // Check for column spans
        var children = this.getChildren();
        for (var k = 0; k < children.length; k++) {
            columnCount += children[k].colspan();
        }
        
        return columnCount;
    },
    
    /**
     * Get the index of the specified table cell within the row.
     * @param {EDITABLE.TableCell} cell The cell to return the position of. 
     * @return The index of the specified table cell in the row.
     * @type Number
     */
    getCellPosition: function (cell) {
        // @@
    },
    
    /**
     * Get a table cell by it's position.
     * @param {Number} position The position of the table cell to return.
     * @return The table cell at the specified position.
     * @type EDITABLE.TableCell
     */
    getCellByPosition: function (position) {
        // @@
    },
    
    /**
     * Return true if the table row accepts the specified type of element being
     * dropped on to it.
     * @param {Class|EDITABLE.Element} The class or an instance of the class
     * to check.
     * @return Whether the element accepts the specified type of element 
     * being dropped on to it.
     * @type Boolean
     **/
    acceptsDrop: function (dropperClass) {
        if (EDITABLE.HTMLElement.prototype.acceptsDrop.call(this, 
                dropperClass)) {
            
            // If the dropper is a Row check they share the same parent table
            // section.
            if (dropperClass.typeOf('TableRow')) {
                return dropperClass.getParent().getId() === 
                    this.getParent().getId();
            } else {
                return true;
            }
        }
        return false;
    },
    
    /**
     * Add the editable table row to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this.htmlTag(),
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                styles: this.styles() ? this.styles().join() : ''
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' + 
                '<%=cls%> <%=styles%>"></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this.getParent().getHTMLElement()).prepend(html);
            }
            
            // Render the children
            for (var i = 0; i < this._children.length; i++) {
                this._children[i].addHTML();
            }
                
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
            
            // Bind events to the HTML
            this._bindLayoutEvents();
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        // Generate the HTML for the children first
        var children = '';
        for (var i = 0; i < this._children.length; i++) {
            children += '\n' + UTILS.mulStr(EDITABLE_TAB, 3) + this._children[i].toHTML();
        }
        // Generate the HTML for the list
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
            'children': children
        };
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%><%=styles%>">',
            templateVars);
            
        html += children + '\n' + UTILS.mulStr(EDITABLE_TAB, 2) + '</' + this._htmlTag + '>';
        
        return html;
    },
    
    /**
     * Return a copy of the TableRow element.
     * @return A copy of the TableRow element.
     * @type EDITABLE.TableRow
     */
    copy: function () {
        var tableRowCopy = new EDITABLE.TableRow({htmlTag: this.htmlTag(),
            styles: this.styles().slice(0)
            });
        for (var i = 0; i < this._children.length; i++) {
            tableRowCopy.appendChild(this._children[i].copy());
        }
        return tableRowCopy;
    },
    
    /**
     * The HTML element types that editable table elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['tr'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'tr'
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());

        // Mouse down
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data._hasMouse = true;
            /*
            Table rows support a dual drag mode, they allow for either the 
            row, or the top level table to be dragged. Which is dragged is 
            determined by the duration the user clicks and holds over the item. 
            */
            event.data._inDragTimeframe = false;
            $$(this).oneTime(EDITABLE_DRAG_HOLD_DURATION, 'dragOn.editable', 
                    function (event) {
                
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                /*
                By setting the '_inDragTimeframe' flag should the user move 
                their mouse the list item will begin dragging.
                */
                $$(this).data('editableElement')._inDragTimeframe = true;
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this).attr('contentEditable', 'False');
                TEXT_SELECTION.clear();
            });
            $$(this).oneTime(EDITABLE_DRAG_DOUBLE_HOLD_DURATION, 
                    'dragOff.editable', function (event) {
                
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                /*
                The user has held the mouse down over the element long enough
                to trigger the drag event for its top level list.
                */
                $$(this).data('editableElement')._inDragTimeframe = false;
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this).attr('contentEditable', 'False');
                TEXT_SELECTION.clear();
            });
        });    
        
        // Mouse up
        htmlElement.bind('mouseup.editable', this, function (event) {
            event.data._hasMouse = false;
            $$(this).stopTime('dragOn.editable');
            $$(this).stopTime('dragOff.editable');
            var dragElement = $$(this).data('editableElement');
            if (dragElement._inDragTimeframe) {
                $$(this).draggable({destroy: true});
                event.data.updateHTML(true);
            }
            event.data._prevMouseX = null;
            event.data._prevMouseY = null;
        });

        // Mouse out
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).stopTime('dragOn.editable');
            $$(this).stopTime('dragOff.editable');
            if (event.data._hasMouse) {
                if (event.data._inDragTimeframe) {
                    event.data._inDragTimeframe = false;
                    event.data._initDraggable(event);
                }
            }
        });

        // Mouse move
        htmlElement.bind('mousemove.editable', this, function (event) {
            if (event.data._hasMouse) {
                if ((event.data._prevMouseX !== null && 
                    event.data._prevMouseY !== null) && 
                    (event.data._prevMouseX !== event.pageX || 
                    event.data._prevMouseY !== event.pageY)) {
                    
                    $$(this).stopTime('dragOn.editable');
                    $$(this).stopTime('dragOff.editable');
                    if (event.data._inDragTimeframe) {
                        event.data._inDragTimeframe = false;
                        event.data._initDraggable(event);
                    }
                }
                event.data._prevMouseX = event.pageX;
                event.data._prevMouseY = event.pageY;
            }
        });    
           
        // Dropping
        htmlElement.droppable({
            drop: function (event, ui) {
                $$(this).data('editableElement')._droppableDrop(this, 
                    event, ui);
            },
            over: function (event, ui) {
                $$(this).data('editableElement')._droppableOver(this, 
                    event, ui);
            },
            out: function (event, ui) {
                $$(this).data('editableElement')._droppableOut(this, 
                    event, ui);
            }
        });
    }, 
    
    /** Private */
    _initDraggable: function (event) {
        /*
        Due to the dual drag functionality of the table row a separate 
        method exists for setting up the rows drag mode.
        */
        this._inDragTimeframe = false;
            
        // Calculate the cursor offset to start dragging from
        var mousedown_event = event;
        var margin = $$(this.getHTMLElement()).margin();
        var cursor_left = -Math.max(0, margin.left);
        var cursor_top = -Math.max(0, margin.top);
        
        $$(this.getInnerHTMLElement()).draggable({
            revert: 'valid',
            revertDuration: 0,
            cursor: 'move',
            cursorAt: {left: cursor_left, top: cursor_top},
            helper: function () {
                if ($$(this).data('editableElement')) {
                    // IE can't append the draggable to the TR so instead we 
                    // append it to the body 
                    var html = $$(this).data('editableElement')._dropperHTML();
                    $$('body').append(html);
                    return $$('body').children('div.' + EDITABLE_CSS_TMP_PREFIX + 'dropper:last');
                }
            },            
            start: function (event, ui) {
                /*
                This is required to fix a text selection issue when dragging 
                in Safari.
                */
                TEXT_SELECTION.clear();
            },
            drag: function (event, ui) {
                if ($$(this).data('editableElement')) {
                    $$(this).data('editableElement')._draggableDrag(this, 
                        event, ui);
                }
            },
            stop: function (event, ui) {
                if ($$(this).data('editableElement')) {
                    $$(this).data('editableElement')._draggableStop(this, 
                        event, ui);
                }
            }
        });
        
        this._hasDragged = false;
        EDITABLE.HTMLElement._triggerDrag(this.getInnerHTMLElement(), 
            mousedown_event);
    },
    
    /** @private */
    _dropperHTML: function () {
        var content = UTILS.abbreviate($$(this.getHTMLElement()).text(), 
            EDITABLE_HELPER_CONTENT_LENGTH);
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + 'TableRow',
            content: content
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' +
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            '<%=content%></div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' + 
            'dropper-overlay"></div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _defaultChildType: 'TableCell',
     
    /** @private */
    _supportedChildTypes: ['TableCell'],
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @ignore */
    align: undefined,
    
    /** @ignore */
    _align: undefined
});

/**
 * Convert a HTML element into a TableRow editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An TableRow editable element.
 * @type EDITABLE.TableRow
 */
EDITABLE.TableRow.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var styles = EDITABLE._getStylesFromCSS(css);
    var options = {
        htmlTag: htmlElement.nodeName,
        styles: styles
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    var tableRow = new EDITABLE.TableRow(options);
    var htmlChildren = $$(htmlElement).children();
    for (var i = 0; i < htmlChildren.length; i++) {
        var htmlChild = htmlChildren[i];
        
        // Children
        var childCss = $$(htmlChild).attr('class');
        var className = EDITABLE._getSettingFromCSS(
            EDITABLE_CSS_CLASS_PREFIX, childCss);
        
        if (className.length > 0) {
            if (EDITABLE[className[0]] === undefined) {
                throw new Error('EDITABLE class \'' + className + 
                    '\' not found');
            }
            var child = EDITABLE[className[0]].fromHTML(this, htmlChild);
            tableRow.appendChild(child);
        } else {
            throw new Error('No EDITABLE class name found for HTML element');
        }
    }
    return tableRow;
};

// Define which element classes can hold TableRow elements
EDITABLE.defineSupportedChildType(EDITABLE.TableSection, 'TableRow');

// Define droppers
EDITABLE.defineDropper(
    EDITABLE.TableRow, 
    EDITABLE.TableRow,  
    EDITABLE.HTMLElement.DRAG_VERTICAL[0],
    EDITABLE.HTMLElement.DRAG_VERTICAL[1]
);


/**
 * An editable table cell element. 
 * @class An editable table cell element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable table cell element.
 * @param {String} htmlTag The HTML element type the editable element 
 * represents.
 * @param {String} align The alignment of the elements content.
 * @param {String} style The elements style.
 * @param {String|IXMO.Soup} content The content of the element.
 * @constructor
 */    
EDITABLE.TableCell = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.Text.call(this, options.id);
    this._type.push('TableCell');
    
    /** @private */
    this._colspan = options.colspan;
    
    /** @private */
    this._rowspan = options.rowspan;
    
    this.htmlTag(options.htmlTag);
    this.align(options.align);
    this.styles(options.styles || []);
    this.htmlSoup(options.content);
};
UTILS.copyPrototype(EDITABLE.TableCell, EDITABLE.Text);

$$.extend(EDITABLE.TableCell.prototype, {
    
    __className__: 'EDITABLE.TableCell',
    
    /**
     * Set/Get the colspan of the column.
     * @param {String} type (Optional) If undefined the current colspan is 
     * returned, else the colspan type is set.
     * @return The colspan.
     * @type Number
     */
    colspan: function (colspan) {
        if (colspan === undefined) {
            return this._colspan;
        }
        if (this._colspan !== colspan) {
            this.trigger('colspanChange', function () {
                this._colspan = colspan;
                if (this.getHTMLElement()) {
                    this.updateHTML();
                    this.taint();
                }
            }, {'value': colspan});
        }
    },
    
    /**
     * Set/Get the rowspan of the column.
     * @param {String} type (Optional) If undefined the current rowspan is 
     * returned, else the rowspan type is set.
     * @return The rowspan.
     * @type Number
     */
    rowspan: function (rowspan) {
        if (rowspan === undefined) {
            return this._rowspan;
        }
        if (this._rowspan !== rowspan) {
            this.trigger('rowspanChange', function () {
                this._rowspan = rowspan;
                if (this.getHTMLElement()) {
                    this.updateHTML();
                    this.taint();
                }
            }, {'value': rowspan});
        }
    },
    
    /**
     * Return the inner (editable component) associated HTML element.
     * @return The HTML element.
     * @type String
     */
    getInnerHTMLElement: function () { 
        // * See note in addHTML method.
        var htmlElement = $$('#' + this._htmlElementId).find(
            '.' + EDITABLE_CSS_BASE_INNER);
        if (htmlElement.length > 0) {
            htmlElement = htmlElement[0];
        } else {
            htmlElement = null;
        }
        return htmlElement;
    },

    /**
     * Select the element.
     */
    select: function () {
        if (this.isSelected()) {
            return;
        }
        this.trigger('select', function (event) {
            if (this.getRoot()._selectedElement) {
                this.getRoot()._selectedElement.unselect();
            }
            this._setContentEditable();
            this._selected = true;
            this.getRoot().selectedElement(this);
        });
    },
    
    /**
     * Unselect the element.
     * @param {Boolean} snapshotTextSelection If true then a snapshot of the 
     * current text selection is taken before unselecting the element (true 
     * by default).
     */
    unselect: function (snapshotTextSelection) {
        if (!this.isSelected()) {
            return;
        }
            
        this.trigger('unselect', function (event) {
            this._selected = false;
            // Snapshot the current text selection
            if (snapshotTextSelection !== false) {
                this._snapshotTextSelection();
            }
            TEXT_SELECTION.clear();
            /*
            Important! HTML elements that have had the contentEditable 
            attribute set must be re-added because the IE render engine 
            makes such elements behave as if the style overlay:auto; is 
            in effect (even if the attribute is removed).
            */
            
            this.updateHTML();
            $$(this.getInnerHTMLElement()).removeClass(
                EDITABLE_CSS_TMP_PREFIX + 'edit');
            this.getRoot().clearSelectedElement(this);
        });
    },
    
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this._htmlTag,
                id: this.getHTMLElementId(),
                rowspan: this.rowspan(),
                colspan: this.colspan(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                align: this.align() ? 
                    EDITABLE_CSS_ALIGN_PREFIX + this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
                /*
                Important! We don't use 'htmlSoup' call here to prevent a
                sync.
                */
                content: this._htmlSoup.toString({optimize: false})
            };
            
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" ' + 
                '<% if (rowspan !== 1) { %>rowspan="<%=rowspan%>"<% } %> ' +
                '<% if (colspan !== 1) { %>colspan="<%=colspan%>"<% } %> ' +
                'class="<%=EDITABLE_CSS_BASE%> <%=cls%> <%=align%> ' + 
                '<%=styles%>">' + 
                '<span class="<%=EDITABLE_CSS_BASE_INNER%>">' +
                '<%=content%></span></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML element
            this._bindMouseEvents();
            this._bindKeyEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this._selected) {
                this._setContentEditable();
            }
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        var content = this._htmlSoup.toString();
        if (content === EDITABLE_DEFAULT_CONTENT) {
            content = '';
        }
        
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            rowspan: this.rowspan(),
            colspan: this.colspan(),
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            align: this.align() ? ' ' + EDITABLE_CSS_ALIGN_PREFIX + 
                this.align() : '',
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
            content: content
        };
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %>' +
            '<% if (rowspan !== 1) { %> rowspan="<%=rowspan%>"<% } %>' + 
            '<% if (colspan !== 1) { %> colspan="<%=colspan%>"<% } %>' + 
            ' class="<%=cls%><%=align%><%=styles%>">' + 
            '<%=content%></<%=type%>>',
            templateVars);
        return html;
    },
    
    /**
     * Return a copy of the TableCell element.
     * @return A copy of the TableCell element.
     * @type EDITABLE.Text
     */
    copy: function () {
        return new EDITABLE.TableCell({
            htmlTag: this.htmlTag(),
            align: this.align(),
            styles: this.styles().slice(0),
            content: this.htmlSoup()
        });
    },
    
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['td', 'th'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'td',
        colspan: 1,
        rowspan: 1,
        align: 'left',
        content: ''
    },
    
    /** @private */
    _setContentEditable: function () {
        /*
        Important! Here we set the HTML element as editable, IE7 requires
        that the attribute name has a capital 'E' as in 'contentEditable'
        otherwise the setting is ignored.
        */
        $$(this.getInnerHTMLElement()).attr('contentEditable', 'true');
        $$(this.getInnerHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
            'selected');
        $$(this.getInnerHTMLElement()).removeClass(EDITABLE_CSS_TMP_PREFIX + 
            'over');
        // Set the selection
        $$(this.getInnerHTMLElement()).focus();
        TEXT_SELECTION.select(this._textSelection,
            this.getInnerHTMLElement());
    },
    
    /** @private */
    _bindMouseEvents: function () {
        var innerHtmlElement = $$(this.getInnerHTMLElement());
        var outerHtmlElement = $$(this.getHTMLElement());

        // Blur @@ Experimenting with not blurring just because focus is lost (might need to to show selected and focused)
        /*innerHtmlElement.bind('blur.editable', this, function (event) {
            if (!event.data._clickedOuter) {
                event.data.unselect();
            }
        });*/
       
        // Hover
        innerHtmlElement.bind('mouseover.editable', this, function (event) {
            if (!event.data._selected) {
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        innerHtmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'over');
        });

        // Click
        innerHtmlElement.bind('mousedown.editable', this, function (event) {
            event.data._clickedOuter = true;
            event.data.select();
            
        });
        
        innerHtmlElement.bind('mouseup.editable', this, function (event) {
            event.data._clickedOuter = false;
            $$(event.data.getInnerHTMLElement()).focus();
        });
        
        // DblClick
        innerHtmlElement.bind('dblclick.editable', this, function (event) {
            event.data.trigger('properties', function () {
                // Double click is the standard event for providing external
                // applications quick link to the items properties.    
            }, {});            
        });
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        
    },
    
    /** @private */
    _inputArrowUp: function (event) {
        if (this.textSelection()[0] === 0 && 
                this.textSelection().isCollapsed()) {
            
            var tableRow = this.getParent();
            var tableSection = tableRow.getParent();
            var columnIndex = tableRow.indexOfChild(this);
            var siblingSection = tableSection.prevSibling();
            var siblingRow = null;
            var siblingColumn = null;
            var len = 0;
            
            if (tableSection.indexOfChild(tableRow) > 0) {
                // Move up a row
                siblingRow = tableRow.prevSibling();
                columnIndex = Math.min(columnIndex, 
                    siblingRow.getChildCount() - 1);
                siblingColumn = siblingRow.getChildren()[columnIndex];
                len = siblingColumn.htmlSoup().getLength();
                siblingColumn.textSelection([len, len]);
                siblingColumn.select();
                
            } else if (siblingSection !== undefined && 
                        siblingSection.isA('TableSection')) {
                            
                // Move up a section
                siblingRow = siblingSection.getChildren()[
                    siblingSection.getChildCount() - 1];
                columnIndex = Math.min(columnIndex, 
                    siblingRow.getChildCount() - 1);
                siblingColumn = siblingRow.getChildren()[columnIndex];
                len = siblingColumn.htmlSoup().getLength();
                siblingColumn.textSelection([len, len]);
                siblingColumn.select();
                
            } else {
                return this._inputArrowLeft(event);
            }
            
            return false;
        }
    },

    /** @private */
    _inputArrowDown: function (event) { 
        var len = this._htmlSoup.getLength();
        if (this.textSelection()[1] === len && 
                this.textSelection().isCollapsed()) {
            
            var tableRow = this.getParent();
            var tableSection = tableRow.getParent();
            var columnIndex = tableRow.indexOfChild(this);
            var siblingSection = tableSection.nextSibling();
            var siblingRow = null;
            var siblingColumn = null;
            
            if (tableSection.indexOfChild(tableRow) < 
                    tableSection.getChildCount() - 1) {
                
                // Move down a row
                siblingRow = tableRow.nextSibling();
                columnIndex = Math.min(columnIndex, 
                    siblingRow.getChildCount() - 1);
                siblingColumn = siblingRow.getChildren()[columnIndex];
                siblingColumn.textSelection([0, 0]);
                siblingColumn.select();
                
            } else if (siblingSection !== undefined && 
                        siblingSection.isA('TableSection')) {
                
                // Move down a section
                siblingRow = siblingSection.getChildren()[0];
                columnIndex = Math.min(columnIndex, 
                    siblingRow.getChildCount() - 1);
                siblingColumn = siblingRow.getChildren()[columnIndex];
                siblingColumn.textSelection([0, 0]);
                siblingColumn.select();
                
            } else {
                return this._inputArrowRight(event);
            }
            
            return false;
        }
    },

    /*
    Both input for the back and delete key are overridden to handle an issue 
    in FireFox that prevents a completely selected list items content being 
    deleted. To reslove this we check for and handle the interaction 
    manually, then relay the rest of the process back to the EDITABLE.Text 
    class.  
    */
    
    /** @private */
    _inputBack: function (event) {
        var soupLength = this._htmlSoup.getLength();
        if (soupLength > 0 && this.textSelection()[0] === 0 && 
            this.textSelection()[1] === soupLength) {
            
            this.htmlSoup('');
            return false;
        }
    },
    
    /** @private */
    _inputDelete: function (event) {
        var soupLength = this._htmlSoup.getLength();
        if (soupLength > 0 && this.textSelection()[0] === 0 && 
            this.textSelection()[1] === soupLength) {
            
            this.htmlSoup('');
            return false;
        }
    },
    
    /** @private */
    _inputReturn: function (event) {
        var row = this.getParent();
        if (row.isLast() && this.isLast()) {
            event.data.trigger('newRow', function () {
                // New row requests events should be handled externally from the library
            }, {});
        } else {
            var next = this.nextRelative({typeOf: 'Text'});
            if (next) {
                next.textSelection([0, 0]);
                next.select();
            }
        }
        return false;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
});

/**
 * Convert a HTML element (and its contents) into a TableCell editable 
 * element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return A TableCell editable element.
 * @type EDITABLE.TableCell
 */
EDITABLE.TableCell.fromHTML = function (parent, htmlElement) {
    var rowspan = $$(htmlElement).attr('rowspan');
    var colspan = $$(htmlElement).attr('colspan');
    var css = $$(htmlElement).attr('class');
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, css);
    var styles = EDITABLE._getStylesFromCSS(css);
    var content = $$(htmlElement).html();
    
    var options = {
        htmlTag: htmlElement.nodeName,
        rowspan: rowspan,
        colspan: colspan,
        align: align.length > 0 ? align[0] : undefined,
        styles: styles,
        content: content.replace(' ', '') !== '' ? content : undefined
    };

    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    
    return new EDITABLE.TableCell(options);
};

// Define which element classes can hold TableCell elements
EDITABLE.defineSupportedChildType(EDITABLE.TableRow, 'TableCell');
