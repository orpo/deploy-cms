/**
 * @fileoverview    Editable image elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 */

'use strict';

/**
 * An editable image element.
 * @class An editable image element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable text element.
 * @param {String} alt The alternative text for the image.
 * @param {String} align The alignment of the image relative to other 
 * elements.
 * @param {String} longdesc The long description of for the image.
 * @param {Array} constraints The min/max size of the image in pixels as an 
 * Array [min, max].
 * @param {Array} size The size of the image in pixels as an Array [width, 
 * height].
 * @param {String} src The source for the image.
 * @param {String} style The images style class.
 * @constructor
 */    
EDITABLE.Image = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('Image');
    
    /** @private */
    this._src = '';

    /** @private */
    this._alt = '';
    
    /** @private */
    this._longdesc = '';
    
    /** @private */
    this._size = options.size;
    
    /** @private */
    this._constraints = options.constraints || [50, 500];
    
    /** @private */
    this._href = '';
    
    /* @private */
    this._dropAlign = 'center';
    
    this.htmlTag('img');
    this.src(options.src);
    this.alt(options.alt);
    this.longdesc(options.longdesc);
    this.size(options.size);
    this.constraints(options.constraints);
    this.href(options.href);
    this.align(options.align);
    this.styles(options.styles || []);
};
UTILS.copyPrototype(EDITABLE.Image, EDITABLE.HTMLElement);

$$.extend(EDITABLE.Image.prototype, {
    
    __className__: 'EDITABLE.Image',
    
    /**
     * Return the inner (editable component) associated HTML element.
     * @return The HTML element.
     * @type String
     */
    getInnerHTMLElement: function () { 
        var htmlElement = $$('#' + this._htmlElementId).find('.' + 
            EDITABLE_CSS_BASE_INNER);
        if (htmlElement.length > 0) {
            htmlElement = htmlElement[0];
        } else {
            htmlElement = null;
        }
        return htmlElement;
    },
    
    /**
     * Return the aspect ratio for the image.
     * @return The aspect ratio for the image.
     * @type Number
     */
    getAspectRatio: function () {
        return this._size[0] / this._size[1];
    },
    
    /**
     * Set/Get the source for the image.
     * @param {String} value (Optional) If undefined the current source for
     * the image is returned, else the images src is set. A src tag must be 
     * present for an image element but an empty string value is acceptable 
     * if not advisable.
     * @return The images source.
     * @type String
     */
    src: function (value) {
        if (value === undefined) {
            return this._src;
        }
        if (this._src !== value) {
            this.trigger('srcChange', function () {
                this._src = value;
                $$(this.getInnerHTMLElement()).attr('src', this._src);
                this.taint();
            }, {'value': value});
        }
    },    
    
    /**
     * Set/Get the size of the image.
     * @param {Array} value (Optional) If undefined the current images size
     * is returned, else the images size is set based on a two item Array 
     * [width, height]. The size will be constrained to the current min/max
     * sizes for the image.
     * @return The images source.
     * @type Array
     */
    size: function (value) {
        if (value === undefined) {
            return this._size;
        }
        value = this._constrainSize(value);
        if (this._size[0] !== value[0] && this._size[1] !== value[1]) {
            this.trigger('sizeChange', function () {
                this._size = [parseInt(value[0], 10), 
                    parseInt(value[1], 10)];
                $$(this.getHTMLElement()).css(
                    {'width': parseInt(value[0], 10).toString() + 'px',
                    'height': parseInt(value[1], 10).toString() + 'px'});
                this.taint();
            }, {'value': value});
        }
    },     
    
    /**
     * Set/Get the alternative text for the image.
     * @param {String} value (Optional) If undefined the current alternative 
     * text for the image is returned, else the images alternative text is 
     * set. An alt attribute must be present for an image element but an 
     * empty string value is acceptable if not advisable.
     * @return The images alternative text.
     * @type String
     */
    alt: function (value) {
        if (value === undefined) {
            return this._alt;
        }
        if (this._alt !== value) {
            this.trigger('altChange', function () {
                this._alt = value;
                $$(this.getInnerHTMLElement()).attr('alt', this._alt);
                this.taint();
            }, {'value': value});
        }
    },
    
    /**
     * Set/Get the long description for the image.
     * @param {String} value (Optional) If undefined the current long 
     * description for the image is returned, else the images long 
     * description is set. To remove the long description use an empty string
     * '';
     * @return The images long description.
     * @type String
     */
    longdesc: function (value) {
        if (value === undefined) {
            return this._longdesc;
        }
        if (this._longdesc !== value) {
            this.trigger('longdescChange', function () {
                this._longdesc = value;
                if (this._longdesc === '') {
                    $$(this.getInnerHTMLElement()).removeAttr('longdesc');
                } else {
                    $$(this.getInnerHTMLElement()).attr('longdesc', 
                        this._longdesc);
                }
                this.taint();
            }, {'value': value});
        }
    },
    
    /**
     * Set/Get the href for the image.
     * @param {String} value (Optional) If undefined the current href for
     * the image is returned, else the href the image links to is set. Images
     * with a href attribute are wrapped in an anchor 
     * (e.g. <a href="..."><img ... /></a>) when output to HTML via the 
     * toHTML method.
     * @return The images href.
     * @type String
     */
    href: function (value) {
        if (value === undefined) {
            return this._href;
        }
        if (this._href !== value) {
            this.trigger('hrefChange', function () {
                this._href = value;
                this.taint();
            }, {'value': value});
        }
    },
    
    /**
     * Set/Get the size constraints (min/max) for the image.
     * @param {Array} value (Optional) If undefined the current constraints
     * of the image are returned, else the image constraints are set.
     * @return The images size constraints.
     * @type Array
     */
    constraints: function (value) {
        if (value === undefined) {
            return this._constraints;
        }
        if (this._constraints[0] !== value[0] && this._constraints[1] !== value[1]) {
            this.trigger('constraintsChange', function () {
                this._constraints = value;
                this.size(this._constrainSize(this._size));
            }, {'value': value});
        }
    },

    /**
     * Select the element.
     */
    select: function () {
        if (this.isSelected()) {
            return;
        }
        this.trigger('select', function (event) {
            if (this.getRoot()._selectedElement) {
                this.getRoot()._selectedElement.unselect();
            }
            $$(this.getHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
                'selected');
            $$(this.getHTMLElement()).removeClass(EDITABLE_CSS_TMP_PREFIX +
                'over');
            this._selected = true;
            this.getRoot().selectedElement(this);
        });
    },
    
    /**
     * Unselect the element.
     */
    unselect: function () {
        if (!this.isSelected()) {
            return;
        }
        this.trigger('unselect', function (event) {
            $$(this.getHTMLElement()).removeClass(
                EDITABLE_CSS_TMP_PREFIX + 'selected');
            // Hide the helper
            $$(this.getHelperHTMLElement()).hide();
            this._selected = false;
            this.getRoot().clearSelectedElement(this);       
        });
    },
    
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                src: this.src(),
                alt: this.alt(),
                longdesc: this.longdesc(),
                size: this.size(),
                align: this.align() ? 
                    EDITABLE_CSS_ALIGN_PREFIX + this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
            };
            var html = UTILS.template(
                '<div id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' + 
                '<%=cls%> <%=align%> <%=styles%>" ' + 
                'style="width:<%=size[0]%>px; ' + 
                'height:<%=size[1]%>px;"><img src="<%=src%>" ' + 
                'alt="<%=alt%>" ' + 
                '<%if (longdesc) {%>longdesc="<%=longdesc%>"<%}%> ' + 
                'class="<%=EDITABLE_CSS_BASE_INNER%>" /></div>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            // Store the element against the HTML element        
            $$(this.getHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML
            this._bindMouseEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this.isSelected()) {
                $$(this.getHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX +
                    'selected');
                $$(this.getHTMLElement()).removeClass(
                    EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        var outerId = null;
        var innerId = null;
        if (this._hasOwnId === true) {
            innerId = this._id;
            outerId = innerId;
        }
        var outerCls = EDITABLE_CSS_CLASS_PREFIX + this.getType();
        var innerCls = EDITABLE_CSS_CLASS_PREFIX + this.getType() + ' ';
        var outerAlign = this.align() ? EDITABLE_CSS_ALIGN_PREFIX + 
            this.align() : '';
        var innerAlign = this.align() ? EDITABLE_CSS_ALIGN_PREFIX + 
            this.align() + ' ' : '';
        var outerStyles = this.styles().length > 0 ? ' ' + this.styles().join(' ') : '';
        var innerStyles = this.styles().length > 0 ? ' ' + this.styles().join(' ') : '';
        if (this.href()) {
            innerId = '';
            innerCls = '';
            innerAlign = '';
            innerStyles = '';
        }
        
        var templateVars = {
            id: innerId,
            cls: innerCls,
            src: this.src(),
            alt: this.alt(),
            longdesc: this.longdesc(),
            size: this.size(),
            constraints: this.constraints(),
            align: innerAlign,
            styles: innerStyles
        };
        
        var html = UTILS.template(
            '<img<% if (id !== null) { %> id="<%=id%>"<% } %> src="<%=src%>" alt="<%=alt%>"' + 
            '<%if (longdesc) { %> longdesc="<%=longdesc%>"<%}%> ' + 
            ' class="<%=cls%><%=align%><%=styles%>" ' + 
            'style="width:<%=size[0]%>px; height:<%=size[1]%>px; min-width:<%=constraints[0]%>px; max-width:<%=constraints[1]%>px;" /> ',
            templateVars);
        if (this.href()) {
            html = UTILS.template(
                '<a<% if (id !== null) { %> id="<%=id%>"<% } %> href="<%=href%>" class="<%=cls%> <%=align%>' + 
                '<%=styles%>" style="width:<%=size[0]%>px; ' + 
                'height:<%=size[1]%>px;"><%=inner%></a>\n',
                {id: outerId,
                cls: outerCls, 
                align: outerAlign, 
                styles: outerStyles, 
                size: this.size(), 
                href: this.href(), 
                inner: html});
        }
        return html;
    },
    
    /**
     * Return a copy of the Image element.
     * @return A copy of the Image element.
     * @type EDITABLE.Image
     */
    copy: function () {
        return new EDITABLE.Image({src: this.src(),
            size: this.size(),
            alt: this.alt(),
            constraints: this.constraints().slice(0),
            align: this.align(),
            styles: this.styles().slice(0),
            longdesc: this.longdesc(),
            href: this.href()
            });
    },
        
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['img'],
    
    /**
     * The default options when creating a new editable Image element.
     * @type Object
     */
    defaultOptions: {
        src: '',
        size: [1, 1],
        alt: '',
        align: 'center',
        longdesc: '',
        href: ''
    },
    
    /** @private */
    _constrainSize: function (size) {
        /*
        Constrain the (min/max) size of the image to within the specified 
        limits (respects the images aspect ratio).
        */
        var newSize = size.slice();
        var aspectRatio = 1.0;
        if (newSize[0] > 0) {
            aspectRatio = newSize[0] / newSize[1];
        }
        if (newSize[0] >= newSize[1]) {
            newSize[0] = Math.max(this._constraints[0], newSize[0]);
            newSize[0] = Math.min(this._constraints[1], newSize[0]);
            newSize[1] = parseInt(newSize[0] / aspectRatio, 10);
        } else {
            newSize[1] = Math.max(this._constraints[0], newSize[1]);
            newSize[1] = Math.min(this._constraints[1], newSize[1]);
            newSize[0] = parseInt(newSize[1] * aspectRatio, 10);
        }
        return newSize;
    },

    /*
    The minimum/maximum size of the image are specified as single values. The
    private '_minSize' and '_maxSize' methods return minimum/maximum image 
    dimensions based on the images aspect ratio - required by JQuery.UIs 
    resizable plug-in.
    */

    /** @private */
    _minSize: function () {
        // Return the images minimum size
        return [this._constraints[0], 
            this._constraints[0] / this.getAspectRatio()];
    },
    
    /** @private */
    _maxSize: function () {
        return [this._constraints[1], 
            this._constraints[1] / this.getAspectRatio()];
    },
    
    /** @private */
    _bindMouseEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
        
        // Hover
        htmlElement.bind('mouseover.editable', this, function (event) {
            if (!event.data._selected) {
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'over');
        });
        
        // Click
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data.select();
        });

        // DblClick
        htmlElement.bind('dblclick.editable', this, function (event) {
            event.data.trigger('properties', function () {
                // Double click is the standard event for providing external 
                // applications quick link to the items properties.    
            }, {});            
        });
    },

    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
                
        // Dragging
    
        // Calculate the cursor offset to start dragging from
        var margin = htmlElement.margin();
        var cursor_left = -Math.max(0, margin.left);
        var cursor_top = -Math.max(0, margin.top);

        htmlElement.draggable({
            revert: 'valid',
            revertDuration: 0,
            cursor: 'move',
            cursorAt: {top: cursor_top, left: cursor_left},
            distance: 5,
            helper: function () {
                return $$(this).data('editableElement')._dropperHTML();
            },
            start: function (event, ui) {
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
            },
            drag: function (event, ui) {
                $$(this).data('editableElement')._draggableDrag(this, event, 
                    ui);
            },
            stop: function (event, ui) {
                $$(this).data('editableElement')._draggableStop(this, event, 
                    ui);
            }
        });
        
        // Resizing
        htmlElement.resizable({
            //aspectRatio: this.getAspectRatio(),
            handles: 'ne, se, sw, nw',
            minWidth: this._minSize()[0],
            minHeight: this._minSize()[1],
            maxWidth: this._maxSize()[0],
            maxHeight: this._maxSize()[1],
            start: function (event, ui) {
                
                // Prevent image jump
                var height = $$('body').height();
                $$(this).css('position', 'relative');
                
                /* 
                - 
                After upgrade to later version of JQuery and JQueryUI this 
                fix seems to no longer apply and infact cause issues.
                -
                var scroll = $$(window).scrollTop();
                $$(window).scrollTop(scroll + $$('body').height() - height);
                */ 
                
                // Add the size information helper
                var size_info_html = UTILS.template(
                    '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>size-info">' + 
                    '</div>', 
                    {});
                $$(this).append(size_info_html);
            },
            resize: function (event, ui) {
                $$(this).css({top: '0', left: '0'});
                
                // Update the size information helper
                var size_info = $$(this).find('.' + EDITABLE_CSS_TMP_PREFIX +
                    'size-info');
                
                // This ensures the aspect ratio remains integral
                var width = parseInt(Math.ceil(ui.size.width));
                var height = parseInt(Math.round(width / $$(this).data('editableElement').getAspectRatio()));
                
                $$(this).width(width);
                $$(this).height(height);
                
                $$(size_info).html(UTILS.template(
                    'w&nbsp;<%=width%> h&nbsp;<%=height%>', 
                    {width: width, height: height}));
            },
            stop: function (event, ui) {
                var elm = $$(this).data('editableElement');
                elm.size([$$(this).width(), $$(this).height()]);
                    
                // Remove the size information helper
                var size_info = $$(this).find('.' + EDITABLE_CSS_TMP_PREFIX + 
                    'size-info');
                $$(size_info).remove();
            }
        });
    },
    
    /** @private */
    _dropperHTML: function () {
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            src: this.src()
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' + 
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content" ' + 
            'style="background-image:url(\'<%=src%>\');"></div>' + 
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-overlay">' + 
            '</div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {}
    
});

/**
 * Convert a HTML element into a Image editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An Image editable element.
 * @type EDITABLE.Image
 */
EDITABLE.Image.fromHTML = function (parent, htmlElement) {
    
    // Check for a link around the image
    var outer = $$(htmlElement);
    var inner = $$(htmlElement).find('img');
    if (inner.length === 0) {
        inner = outer;
    }
    
    // Size
    var size = null;
    var width = $$(inner).css('width').replace(/(\d+)px/i, '$1');
    var height = $$(inner).css('height').replace(/(\d+)px/i, '$1');
    if (width && Number(width) > 0 && height && Number(height)) {
        size = [Number(width), Number(height)];
    }
    
    // Constraints
    var constraints = [50, 500];
    var minSize = $$(inner).css('min-width').replace(/(\d+)px/i, '$1');
    var maxSize = $$(inner).css('max-width').replace(/(\d+)px/i, '$1');
    if (minSize && Number(minSize) > 0) {
        constraints[0] = Number(minSize);
    }
    if (maxSize && Number(maxSize) > 0) {
        constraints[1] = Number(maxSize);
    }
    
    var outerCSS = $$(htmlElement).attr('class');         
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, 
        outerCSS);
    var styles = EDITABLE._getStylesFromCSS(outerCSS);
    var options = {
        src: $$(inner).attr('src'),
        size: size,
        constraints: constraints,
        alt: $$(inner).attr('alt'),
        longdesc: $$(inner).attr('longdesc'),
        href: $$(outer).attr('href'),
        align: align.length > 0 ? align[0] : undefined,
        styles: styles
    };
    if (outer.attr('id') !== '') {
        options['id'] = outer.attr('id');
    };
    return new EDITABLE.Image(options);
};

// Define which element classes can hold Image elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'Image');

// Define droppers
EDITABLE.defineDropper(EDITABLE.Image, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[1] === -1) {
            this.align('left');
        } else if (region[1] === 1) {
            this.align('right');
        } else if (region[0] === 1) {
            this.align('center');
            insertIndex += 1;
        } else {
            this.align('center');
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });

EDITABLE.defineDropper(EDITABLE.Image, 
    [EDITABLE.Rule, EDITABLE.Table],
    EDITABLE.HTMLElement.DRAG_VERTICAL[0],
    EDITABLE.HTMLElement.DRAG_VERTICAL[1]
    );

EDITABLE.defineDropper(EDITABLE.Image, EDITABLE.List, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[1] === -1) {
            this.align('left');
        } else if (region[1] === 1) {
            this.align('right');
        } else if (region[0] === 1) {
            this.align('center');
            insertIndex += 1;
        } else {
            this.align('center');
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });