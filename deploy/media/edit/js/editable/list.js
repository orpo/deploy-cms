/**
 * @fileoverview    Editable list elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 * @requires        EDITABLE (text) (/js/editable/text.js)
 */

'use strict';

/**
 * An editable list element.
 * @class An editable list element.
 * @param {Object} options The options for the editable list element.
 * @param {String} htmlTag The HTML element type the editable element 
 * represents.
 * @param {String} style The elements style.
 * @constructor
 */    
EDITABLE.List = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLCollection.call(this);
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('List');
    
    
    this.htmlTag(options.htmlTag);
    this.styles(options.styles || []);
};
UTILS.copyPrototype(EDITABLE.List, EDITABLE.HTMLCollection);
UTILS.copyPrototype(EDITABLE.List, EDITABLE.HTMLElement);

$$.extend(EDITABLE.List.prototype, {
    
    __className__: 'EDITABLE.List',
    
    /**
     * Return the depth of the list.
     * @return The depth of the list.
     */
    getDepth: function () {
        var depth = 0;
        var parent = this.getParent();
        while (parent.isA('ListItem')) {
            depth += 1;
            parent = parent.getGrandParent();
        }
        return depth;
    },
    
    /**
     * Remove an element from the editable list. Unlike other collections an 
     * empty list automatically removes itself.
     * @param {EDITABLE.HTMLelement} child The element to be remove.
     */
    removeChild: function (child) {
        this.trigger('removeChild', function (event) {
            var index = this.indexOfChild(child);
            if (index === -1) {
                throw new Error('Child is not part of the collection');
            }
            if (child.isSelected()) {
                /*
                Don't snapshot the text selection when unselecting a child
                text element to remove it.
                */ 
                if (child.typeOf('Text')) { 
                    child.unselect(false);
                } else {
                    child.unselect(false);
                }
            }
            var after = this._children.slice(index + 1);
            this._children.length = index;
            this._children.push.apply(this._children, after);
            child.removeHTML();
            child._parent = null;
            this.taint();
            // Auto remove list if it has no children
            if (this.getParent() && this._children.length === 0) {
                this.getParent().removeChild(this);
            }
        }, {'child': child});
    },

    /**
     * Add the editable list to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this.htmlTag(),
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                styles: this.styles() ? this.styles().join(' ') : ''
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' + 
                '<%=cls%> <%=styles%>"></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                if (this.getParent().isA('ListItem')) {
                    $$(this.getParent().getHTMLElement()).append(html);
                } else {
                    $$(this.getParent().getHTMLElement()).prepend(html);
                }
            }
            
            // Render the children
            for (var i = 0; i < this._children.length; i++) {
                this._children[i].addHTML();
            }
                
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
            
            // Bind events to the HTML
            this._bindLayoutEvents();
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        
        // Generate the HTML for the children first
        var children = '';
        var depth = this.getDepth();
        var padding = UTILS.mulStr(EDITABLE_TAB, depth * 2);
        
        for (var i = 0; i < this._children.length; i++) {
            children += '\n' + this._children[i].toHTML();
        }
        // Generate the HTML for the list
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : ''
        };
        
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%><%=styles%>">', 
            templateVars);
        html = padding + html + children + '\n' + padding + '</' + this._htmlTag + '>';
        return html;
    },
    
    /**
     * Return a copy of the List element.
     * @return A copy of the List element.
     * @type EDITABLE.List
     */
    copy: function () {
        var listCopy = new EDITABLE.List({htmlTag: this.htmlTag(),
            styles: this.styles().slice()
            });
        for (var i = 0; i < this._children.length; i++) {
            listCopy.appendChild(this._children[i].copy());
        }
        return listCopy;
    },
    
    /**
     * The HTML element types that editable lists elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['ol', 'ul'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'ul'
    },

    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());
        
        /*
        List can be dragged to a new position as a whole, because of this we
        only add the draggable ability to top level lists.
        */
        if (!this.getParent().isA('ListItem')) {
            
            // Dragging
            htmlElement.bind('mousedown.editable', this, function (event) {
                    
                // Ignore right mouse clicks
                if (event.button === 2) {
                    return;
                }
                
                
                event.data._hasMouse = true;
                var mousedown_event = event;
                    
                /*
                This avoids a potential FireFox and Safari issue caused by 
                selected text not reporting mouse[over/out] events. By 
                removing the selection we ensure this situation can't 
                arise.
                */            
                TEXT_SELECTION.clear();
                
                $$(this).oneTime(EDITABLE_DRAG_DOUBLE_HOLD_DURATION, 
                    'drag.editable', function (event) {
                    
                    // Check we're allowed to move items within the collection
                    if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                        return false;
                    }
                    
                    // Prepare the element for dragging
                    $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                    $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 
                        'selected');
                    
                    // Calculate the cursor offset to start dragging from
                    var margin = $$(this).margin();
                    var cursor_left = -Math.max(0, margin.left);
                    var cursor_top = -Math.max(0, margin.top);
                    
                    $$(this).draggable({
                        revert: 'valid',
                        revertDuration: 0,
                        cursor: 'move',
                        cursorAt: {left: cursor_left, top: cursor_top},
                        helper: function () {
                            if ($$(this).data('editableElement')) {
                                return $$(this).data(
                                    'editableElement')._dropperHTML();
                            }
                        },
                        drag: function (event, ui) {
                            if ($$(this).data('editableElement')) {
                                $$(this).data(
                                    'editableElement')._draggableDrag(
                                        this, event, ui);
                            }
                        },
                        stop: function (event, ui) {
                            if ($$(this).data('editableElement')) {
                                $$(this).data(
                                    'editableElement')._draggableStop(
                                        this, event, ui);
                            }
                        }
                    });
                    
                    $$(this).data('editableElement')._hasDragged = false;
                    EDITABLE.HTMLElement._triggerDrag(this, mousedown_event);
                });
            });        
            
            // Mouse up
            htmlElement.bind('mouseup.editable', this, function (event) {
                event.data._hasMouse = false;
                $$(this).stopTime('drag.editable');
                var dragElement = $$(this).data('editableElement');
                if ($$(this).data('draggable') && !dragElement._hasDragged) {
                    $$(this).draggable({destroy: true});
                    event.data.updateHTML();
                }
                event.data._prevMouseX = null;
                event.data._prevMouseY = null;
            });
    
            // Mouse out
            htmlElement.bind('mouseout.editable', this, function (event) {
                if (event.data._hasMouse) {
                    $$(this).stopTime('drag.editable');
                }
            });
    
            // Mouse move
            htmlElement.bind('mousemove.editable', this, function (event) {
                if (event.data._hasMouse) {
                    if ((event.data._prevMouseX !== null && 
                        event.data._prevMouseY !== null) && 
                        (event.data._prevMouseX !== event.pageX || 
                        event.data._prevMouseY !== event.pageY)) {
                        
                        $$(this).stopTime('drag.editable');
                    }
                    event.data._prevMouseX = event.pageX;
                    event.data._prevMouseY = event.pageY;
                }
            });
                        
            // Dropping
            htmlElement.droppable({
                greedy: true,
                drop: function (event, ui) {
                    $$(this).data('editableElement')._droppableDrop(this, 
                        event, ui);
                },
                over: function (event, ui) {
                    $$(this).data('editableElement')._droppableOver(this, 
                        event, ui);
                },
                out: function (event, ui) {
                    $$(this).data('editableElement')._droppableOut(this, 
                        event, ui);
                }
            });
        }
    },
    
    /** @private */
    _dropperHTML: function () {
        var content = UTILS.abbreviate($$(this.getHTMLElement()).text(), 
            EDITABLE_HELPER_CONTENT_LENGTH);
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + 'List',
            content: content
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' +
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            '<%=content%></div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' + 
            'dropper-overlay"></div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _defaultChildType: 'ListItem',
     
    /** @private */
    _supportedChildTypes: ['ListItem'],
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @ignore */
    align: undefined,
    
    /** @ignore */
    _align: undefined
});

/**
 * Convert a HTML element into a List editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An List editable element.
 * @type EDITABLE.List
 */
EDITABLE.List.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var styles = EDITABLE._getStylesFromCSS(css);
    var options = {
        htmlTag: htmlElement.nodeName,
        styles: styles
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    
    var list = new EDITABLE.List(options);
    var htmlChildren = $$(htmlElement).children();
    for (var i = 0; i < htmlChildren.length; i++) {
        var htmlChild = htmlChildren[i];
        var childCss = $$(htmlChild).attr('class');
        var className = EDITABLE._getSettingFromCSS(
            EDITABLE_CSS_CLASS_PREFIX, childCss);
        
        if (className.length > 0) {
            if (EDITABLE[className[0]] === undefined) {
                throw new Error('EDITABLE class \'' + className + 
                    '\' not found');
            }
            var child = EDITABLE[className[0]].fromHTML(this, htmlChild);
            list.appendChild(child);
        } else {
            throw new Error('No EDITABLE class name found for HTML element');
        }
    }
    return list;
};

// Define which element classes can hold List elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'List');

// Define droppers
EDITABLE.defineDropper(EDITABLE.List, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
    });

EDITABLE.defineDropper(EDITABLE.List, EDITABLE.List, 
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
    });
    
EDITABLE.defineDropper(EDITABLE.Rule, EDITABLE.List, 
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
    });
    
EDITABLE.defineDropper(EDITABLE.List, EDITABLE.Rule, 
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
    });
    
/**
 * An editable list item element.
 * @class An editable list item element.
 * @param {Object} options The options for the editable list item element.
 * @param {String} style The elements style.
 * @constructor
 */    
EDITABLE.ListItem = function (options) {
    EDITABLE.List.call(this, options);
    EDITABLE.Text.call(this, options);
    this._type.push('ListItem');
};
UTILS.copyPrototype(EDITABLE.ListItem, EDITABLE.List);
UTILS.copyPrototype(EDITABLE.ListItem, EDITABLE.Text);

$$.extend(EDITABLE.ListItem.prototype, {
    
    __className__: 'EDITABLE.ListItem',

    /**
     * Return the inner (editable component) associated HTML element.
     * @return The HTML element.
     * @type String
     */
    getInnerHTMLElement: function () { 
        // * See note in addHTML method.
        var htmlElement = $$('#' + this._htmlElementId).find(
            '.' + EDITABLE_CSS_BASE_INNER);
        if (htmlElement.length > 0) {
            htmlElement = htmlElement[0];
        } else {
            htmlElement = null;
        }
        return htmlElement;
    },
    
    /**
     * Support get/setting the ID of an element. Supersedes getHTMLElementId.
     */ 
    changeId: function (value) {
        if (value === undefined) {
            return this._id;
        }
        if (this._id !== value) {
            this.trigger('changeId', function () {
                
                // Unregister
                delete EDITABLE.__global__[this._id];
                
                if (value === null || value === '') {
                    // Clear
                    this._hasOwnId = false;
                    this._id = UTILS.createId();
                } else {
                    // Set
                    this._hasOwnId = true;
                    this._id = value;
                }    
                
                // Register
                EDITABLE.__global__[this._id] = this;
                
                this.updateHTML(true);
                this.taint();
            }, {'value': value});
        }
    },
    
    /**
     * Remove an element from the collection.
     * @param {EDITABLE.HTMLelement} child The element to be remove.
     */
    removeChild: function (child) {
        EDITABLE.HTMLCollection.prototype.removeChild.call(this, child);
    },
    
    /**
     * Unselect the element.
     */
    unselect: function () {
        if (!this.isSelected()) {
            return;
        }
        this.trigger('unselect', function (event) {
            this._selected = false;
            this.getRoot().clearSelectedElement(this);
            if (this.htmlSoup().getLength() > 0) {
                /*
                Important! HTML elements that have had the contentEditable 
                attribute set must be re-added because the IE render engine 
                makes such elements behave as if the style overlay:auto; is 
                in effect (even if the attribute is removed).
                */
                $$(this.getInnerHTMLElement()).removeClass(
                    EDITABLE_CSS_TMP_PREFIX + 'selected');
                this.updateHTML(true);
                TEXT_SELECTION.clear();
            } else {
                // Element will be automatically removed so we need to 
                // unindent any children first.
                if (this.firstChild() && 
                    this.firstChild().getChildCount() > 0) {
                    
                    var index = this.getParent().indexOfChild(this);
                    var children = this.firstChild().getChildren();
                    var child = children[0];
                    var i = 0;
                    while (child) {
                        child.getParent().removeChild(child);
                        this.getParent().insertChild(index + i, child);
                        child = children[0];
                        i++;
                    }
                }
                // Auto-remove if empty
                if (this.getParent()) {
                    this.getParent().removeChild(this);
                }                
            }
        });
    },    
    
    /**
     * Indent the list item.
     */
    indent: function () {
        // Check for first item (which can't be indented)
        if (this === this.getParent().firstChild()) {
            return;
        }
            
        /*
        If the items previous sibling already has children then we simply 
        append on to that list, otherwise we create a new list under the 
        previous sibling and append on to that. 
        */
        var newParent = null;
        var prevSibling = this.prevSibling();
        if (prevSibling.firstChild()) {
            newParent = prevSibling.firstChild();
        } else {
            newParent = new EDITABLE.List(
                {htmlTag: this.getParent().htmlTag()});
            prevSibling.appendChild(newParent);
        }
        this.getParent().removeChild(this);
        newParent.appendChild(this);
    },
    
    /**
     * Unindent the list item.
     */
    unindent: function () {
        var i = 0;
        var postSibling = null;
        
        // Get a list of all the siblings for the item that come after it
        var nextSibling = this.nextSibling();
        var postSiblingList = [];
        while (nextSibling) {
            postSiblingList.push(nextSibling);
            nextSibling = nextSibling.nextSibling();
        } 

        // Unindent the item
        var parent = this.getParent();
        var grandParent = this.getGrandParent();
        nextSibling = this.nextSibling();
        if (grandParent.isA('ListItem')) {
            // Since the grand parent of this item is another list item we 
            // just move the item into it's parent.
            
            // Move the item...
            parent.removeChild(this);
            var newParent = grandParent.getParent();
            newParent.insertChild(newParent.indexOfChild(grandParent) + 1, 
                this);
                
            if (postSiblingList.length > 0) {
                /*
                ... and it's post siblings. The post siblings now become 
                children of the unindented item.
                */
                var firstChild = this.firstChild();
                if (!firstChild) {
                    // Ensure the unindented item has a child list to insert its 
                    // (previously) post siblings into.
                    firstChild = new EDITABLE.List(
                        {htmlTag: parent.htmlTag()});
                    this.appendChild(firstChild);
                }
                for (i = 0; i < postSiblingList.length; i++) {
                    postSibling = postSiblingList[i];
                    postSibling.getParent().removeChild(postSibling);
                    firstChild.appendChild(postSibling);
                }
            }
            this.select();
            
        } else if (grandParent.isA('HTMLCollection')) {
            /*
            The indented ListItem becomes a Text element in the 
            HTMLCollection.
            */
            
            // Extract any child list from the item
            var childList = this.firstChild();
            if (childList) {
                this.removeChild(childList);
            }
            
            /* 
            Remove the item from it's current parent and convert and insert 
            it into the HTMLCollection.
            
            NOTE: We determine the new index before we remove the list item 
            since removing a list item from it's parent can cause the parent
            to be automatically deleted (if empty), which then prevents a 
            valid index from being queried.
            */
            var newIndex = grandParent.indexOfChild(parent);
            if (parent.getChildCount() > 1) {
                newIndex++;
            }
            parent.removeChild(this);
            var newChild = this.as(EDITABLE.Text);
            grandParent.insertChild(newIndex, newChild);
            newChild.select();
            
            // Unindent the items child list
            if (childList) {
                grandParent.insertChild(
                    grandParent.indexOfChild(newChild) + 1,
                    childList);
            }
            
            // Move any post siblings 
            if (nextSibling) {
                // Create a new list in which to place any post siblings
                var newList = new EDITABLE.List(
                    {htmlTag: parent.htmlTag()});
                newChild.getParent().insertChild(
                    newChild.getParent().indexOfChild(newChild) + 1,
                    newList);
                // Move the post siblings into the new list
                for (i = 0; i < postSiblingList.length; i++) {
                    postSibling = postSiblingList[i];
                    postSibling.getParent().removeChild(postSibling);
                    newList.appendChild(postSibling);
                }
            }
        } 
    },
    
    /**
     * Add the element to the page HTML.
     * @param {Boolean} inner If true (default: false) then only the inner 
     * HTML element is added.
     */
    addHTML: function (inner) {
        /*
        Lists are a both a collection and text editable element (List/Text)
        their composition is more complext to support both collection and 
        content editable functionality.
        */
        this.trigger('addHTHML', function (event) {
            // Render the HTML
            var templateVars = {
                type: this._htmlTag,
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                align: this.align() ? 
                    EDITABLE_CSS_ALIGN_PREFIX + this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
                /*
                Important! We don't use 'htmlSoup' call here to prevent a
                sync.
                */
                content: this._htmlSoup.toString({optimize: false})
            };
            var html = '';
            if (inner === true) {
                /* 
                Only add the inner content HTML (typically only the inner 
                will have been removed)
                */
                html = UTILS.template(
                    '<span class="<%=EDITABLE_CSS_BASE_INNER%>">' + 
                    '<%=content%></span>',
                    templateVars);
                if (this.getChildCount() > 0) {
                    $$(this.getChildren()[0].getHTMLElement()).before(html);
                } else {
                    $$(this.getHTMLElement()).html(html);
                }
            } else {
                html = UTILS.template(
                    '<<%=type%> id="<%=id%>" class="' +
                    '<%=EDITABLE_CSS_BASE%> <%=cls%> <%=align%> ' + 
                    '<%=styles%>">' + 
                    '<span class="<%=EDITABLE_CSS_BASE_INNER%>">' +
                    '<%=content%></span></<%=type%>>',
                    templateVars);
                if (this.prevSibling()) {
                    $$(this.prevSibling().getHTMLElement()).after(html);
                } else {
                    $$(this._parent.getHTMLElement()).prepend(html);
                }
            }
            
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
            
            // Bind events to the HTML element
            this._bindMouseEvents();
            this._bindKeyEvents();
            this._bindLayoutEvents();
            
            if (inner !== true) {
                // Render any child elements
                for (var i = 0; i < this._children.length; i++) {
                    this._children[i].addHTML();
                }
            }

            // Re-select if required
            if (this._selected) {
                this._setContentEditable();
            }
        });
    },
    
    /**
     * Remove the element from the page HTML.
     * @param {Boolean} inner If true (default: false) then only the inner 
     * HTML element is removed.
     */
    removeHTML: function (inner) {
        this.trigger('removeHTHML', function (event) {
            if (inner) {
                $$(this.getInnerHTMLElement()).draggable('destroy');
                $$(this.getInnerHTMLElement()).droppable('destroy');
                $$(this.getInnerHTMLElement()).resizable('destroy');
                $$(this.getInnerHTMLElement()).remove();
            } else {
                $$(this.getHTMLElement()).draggable('destroy');
                $$(this.getHTMLElement()).droppable('destroy');
                $$(this.getHTMLElement()).resizable('destroy');
                $$(this.getHTMLElement()).remove(); 
            }
        });
    },
    
    /**
     * Update the element in the page HTML.
     * @param {Boolean} inner If true (default: false) then only the content 
     * HTML of the list item is updated, instead of the list item and all 
     * it's child items.
     */
    updateHTML: function (inner) {
        this.removeHTML(inner);
        this.addHTML(inner);
    },    
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        // Generate the HTML for the children first
        var childHTML = '';    
        for (var i = 0; i < this._children.length; i++) {
            childHTML += this._children[i].toHTML();
        }
        
        // Generate the HTML for the ListItem
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            align: this.align() ? EDITABLE_CSS_ALIGN_PREFIX + 
                this.align() : '',
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
        };
        
        var depth = this.getParent().getDepth();
        var padding = UTILS.mulStr(EDITABLE_TAB, depth * 2) + EDITABLE_TAB;
        
        var content = '';
        var html = UTILS.template(
                '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%> <%=align%><%=styles%>">',
                templateVars);
        if (this._children.length === 0) {
            content = this._htmlSoup.toString();
            if (content === EDITABLE_DEFAULT_CONTENT) {
                content = '';
            }
            content += childHTML
            
            html = padding + html + content + '</' + this._htmlTag + '>';
        } else {
            content = this._htmlSoup.toString();
            if (content === EDITABLE_DEFAULT_CONTENT) {
                content = '';
            }
            content += '\n' + childHTML;
            
            html = padding + html + content + '\n' + padding + '</' + this._htmlTag + '>';
        }
        return html;
    },    
    
    /**
     * Return a copy of the ListItem element.
     * @return A copy of the ListItem element.
     * @type EDITABLE.ListItem
     */
    copy: function () {
        return new EDITABLE.ListItem({htmlTag: this.htmlTag(),
            align: this.align(),
            styles: this.styles().slice(0),
            content: this.htmlSoup()
            });
    },
    
    /**
     * The HTML element types that editable list item elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['li'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'li',
        align: 'left',
        content: EDITABLE_DEFAULT_CONTENT
    },
    
    /*
    Both input for the back and delete key are overridden to handle an issue 
    in FireFox that prevents a completely selected list items content being 
    deleted. To reslove this we check for and handle the interaction 
    manually, then relay the rest of the process back to the EDITABLE.Text 
    class.  
    */
    
    /** @private */
    _inputBack: function (event) {
        var soupLength = this._htmlSoup.getLength();
        if (soupLength > 0 && this.textSelection()[0] === 0 && 
            this.textSelection()[1] === soupLength) {
            
            this.htmlSoup('');
            return false;
        }
        return EDITABLE.Text.prototype._inputBack.call(this, event);
    },
    
    /** @private */
    _inputDelete: function (event) {
        var soupLength = this._htmlSoup.getLength();
        if (soupLength > 0 && this.textSelection()[0] === 0 && 
            this.textSelection()[1] === soupLength) {
            
            this.htmlSoup('');
            return false;
        }
        return EDITABLE.Text.prototype._inputDelete.call(this, event);
    },
    
    /** @private */
    _inputReturn: function (event) {
        var htmlSoups = this.htmlSoup().split(this.textSelection());
        var first = htmlSoups[0];
        var firstEmpty = first.allWhitespace();
        var last = htmlSoups[1];
        var lastEmpty = last.allWhitespace();
        if (firstEmpty && lastEmpty) {
            /*
            If the user has selected all the list items content then we 
            unindent it. This is the behaviour of a number of mainstream word
            processors and so we follow their lead here.
            */
            if (!this.textSelection().isCollapsed()) {
                this.unindent();
            }
        } else {
            // This will create a new list item
            EDITABLE.Text.prototype._inputReturn.call(this, event);
            if (this.firstChild()) {
                /* 
                Transfer the list items children to the newly created 
                list item (which is the nextSibling).
                */
                var firstChild = this.firstChild();
                this.removeChild(firstChild);
                this.nextSibling().appendChild(firstChild);
            }
        }
        return false;
    },
    
    /** Private */
    _initDraggable: function (event) {
        /*
        Due to the dual drag functionality of the list item a separate 
        method exists for setting up the items drag mode.
        */
        this._inDragTimeframe = false;
            
        // Calculate the cursor offset to start dragging from
        var mousedown_event = event;
        var margin = $$(this.getHTMLElement()).margin();
        var cursor_left = -Math.max(0, margin.left);
        var cursor_top = -Math.max(0, margin.top);
        
        $$(this.getInnerHTMLElement()).draggable({
            revert: 'valid',
            revertDuration: 0,
            cursor: 'move',
            cursorAt: {left: cursor_left, top: cursor_top},
            helper: function () {
                if ($$(this).data('editableElement')) {
                    return $$(this).data('editableElement')._dropperHTML();
                }
            },            
            start: function (event, ui) {
                /*
                This is required to fix a text selection issue when dragging 
                in Safari.
                */
                TEXT_SELECTION.clear();
            },
            drag: function (event, ui) {
                if ($$(this).data('editableElement')) {
                    $$(this).data('editableElement')._draggableDrag(this, 
                        event, ui);
                }
            },
            stop: function (event, ui) {
                if ($$(this).data('editableElement')) {
                    $$(this).data('editableElement')._draggableStop(this, 
                        event, ui);
                }
            }
        });
        
        this._hasDragged = false;
        EDITABLE.HTMLElement._triggerDrag(this.getInnerHTMLElement(), 
            mousedown_event);
    },
    
    /** @private */
    _bindMouseEvents: function () {
        EDITABLE.Text.prototype._bindMouseEvents.call(this);  
        
        /*
        Lists can be dragged on to by other elements, so their children (list
        items) should not highlight if their top level list is being dropped
        on to.
        */
        var htmlElement = $$(this.getInnerHTMLElement());
        htmlElement.unbind('mouseover.editable');
        htmlElement.bind('mouseover.editable', this, function (event) {
            var list = event.data.lastAncestor({isA: 'List'});
            if (!event.data._selected && 
                !$$(list.getInnerHTMLElement()).hasClass(
                    EDITABLE_CSS_TMP_PREFIX + 'dragged-over')) {

                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        /*
        Dual drag support means that we need to ensure the elements text
        selection is not updated when the item is in the drag timeframe or
        its top level list is being dragged.
        */
        htmlElement.unbind('mouseup.editable');
        htmlElement.bind('mouseup.editable', this, function (event) {
            var list = event.data.lastAncestor({isA: 'List'});
            if (event.data.isSelected() && !event.data._inDragTimeframe &&
                !$$(list.getInnerHTMLElement()).data('draggable')) {
                
                event.data.textSelection(TEXT_SELECTION.query(this));
                event.data._syncHTMLSoup();
            }
        });
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());

        // Mouse down
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data._hasMouse = true;
            /*
            List items support a dual drag mode, they allow for either the 
            list item, or the top level list for an item to be dragged. Which
            is dragged is determined by the duration the user clicks and 
            holds over the item. 
            */
            event.data._inDragTimeframe = false;
            $$(this).oneTime(EDITABLE_DRAG_HOLD_DURATION, 'dragOn.editable', 
                    function (event) {
                
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
            
                /*
                By setting the '_inDragTimeframe' flag should the user move 
                their mouse the list item will begin dragging.
                */
                $$(this).data('editableElement')._inDragTimeframe = true;
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this).attr('contentEditable', 'False');
                TEXT_SELECTION.clear();
            });
            $$(this).oneTime(EDITABLE_DRAG_DOUBLE_HOLD_DURATION, 
                    'dragOff.editable', function (event) {
                
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                /*
                The user has held the mouse down over the element long enough
                to trigger the drag event for its top level list.
                */
                $$(this).data('editableElement')._inDragTimeframe = false;
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this).attr('contentEditable', 'False');
                TEXT_SELECTION.clear();
            });
        });        
        
        // Mouse up
        htmlElement.bind('mouseup.editable', this, function (event) {
            event.data._hasMouse = false;
            $$(this).stopTime('dragOn.editable');
            $$(this).stopTime('dragOff.editable');
            var dragElement = $$(this).data('editableElement');
            if (dragElement._inDragTimeframe) {
                $$(this).draggable({destroy: true});
                event.data.updateHTML(true);
            }
            event.data._prevMouseX = null;
            event.data._prevMouseY = null;
        });

        // Mouse out
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).stopTime('dragOn.editable');
            $$(this).stopTime('dragOff.editable');
            if (event.data._hasMouse) {
                if (event.data._inDragTimeframe) {
                    event.data._inDragTimeframe = false;
                    event.data._initDraggable(event);
                }
            }
        });

        // Mouse move
        htmlElement.bind('mousemove.editable', this, function (event) {
            if (event.data._hasMouse) {
                if ((event.data._prevMouseX !== null && 
                    event.data._prevMouseY !== null) && 
                    (event.data._prevMouseX !== event.pageX || 
                    event.data._prevMouseY !== event.pageY)) {
                    
                    $$(this).stopTime('dragOn.editable');
                    $$(this).stopTime('dragOff.editable');
                    if (event.data._inDragTimeframe) {
                        event.data._inDragTimeframe = false;
                        event.data._initDraggable(event);
                    }
                }
                event.data._prevMouseX = event.pageX;
                event.data._prevMouseY = event.pageY;
            }
        });

        // Dropping
        htmlElement.droppable({
            greedy: false, // Must not be greedy of dual drag support
            drop: function (event, ui) {
                $$(this).data('editableElement')._droppableDrop(this, event, 
                    ui);
            },
            over: function (event, ui) {
                var dropper = $$(this).data('editableElement');
                var dragger = $$(ui.draggable).data('editableElement');
                if (dragger !== undefined && dragger.acceptsDrop(dropper)) {
                    $$(ui.draggable).data('editableDropOver', dropper);
                }
            },
            out: function (event, ui) {
                $$(this).data('editableElement')._droppableOut(this, event, 
                    ui);
            }
        });
    },

    /** @private */
    _supportedChildTypes: ['List'],

    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
    
    /** @private */
    _inDragTimeframe: false
});

EDITABLE.ListItem.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, css);
    var styles = EDITABLE._getStylesFromCSS(css);
    var childList = $$(htmlElement).find('li').parent().clone();
    $$(htmlElement).find('li').parent().remove();
    var content = $$(htmlElement).html();
    
    var options = {
        htmlTag: htmlElement.nodeName,
        align: align.length > 0 ? align[0] : undefined,
        styles: styles,
        content: content.replace(' ', '') !== '' ? content : undefined
    };
    
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    
    var listItem = new EDITABLE.ListItem(options);
    
    // Cater for a sub-list
    if (childList.length > 0) {
        var list = EDITABLE.List.fromHTML(listItem, childList[0]);
        listItem.appendChild(list);
    }
    return listItem;
};

// Define interaction handlers

// Converters
EDITABLE.defineConverter(EDITABLE.ListItem, EDITABLE.Text, 
    function () {
        var element = new EDITABLE.Text({content: this._htmlSoup});
        element.textSelection(this.textSelection());
        return element;
    });

EDITABLE.defineConverter(EDITABLE.Text, EDITABLE.ListItem, 
    function () {
        var element = new EDITABLE.ListItem({content: this._htmlSoup});
        element.textSelection(this.textSelection());
        return element;
    });

// Mergers
EDITABLE.defineMerger(EDITABLE.ListItem, EDITABLE.Text,
    function (element) {
        var index = this.htmlSoup().getLength();
        if (!element.isEmpty()) {
            // Paste content on to the end of the merged into item
            this._htmlSoup.paste(index, element.htmlSoup());
            this.refreshHTML();
        }
        // Set the selection at the end of the merged into item
        this.textSelection([index, index]);
        this.select();
        // If the merged in element had content in it remove it from its 
        // parent (auto remove will deal with it otherwise).
        if (element._htmlSoup.getLength() > 0) {
            element.getParent().removeChild(element);        
        }
    });
    
EDITABLE.defineMerger(EDITABLE.ListItem, EDITABLE.ListItem, 
    function (element) {
        // Move any children on to the end of the prev siblings child list
        if (element.firstChild() && 
            element.firstChild().getChildCount() > 0) {
            
            var newList = null;
            var children = null;
            var child = null;
            if (element.prevSibling()) {
                /*
                Merge any children the merging list item has on to the 
                previous sibling being merged with.
                */
                var prevListItem = element.prevSibling();
                newList = prevListItem.firstChild();
                if (!newList) {
                    newList = new EDITABLE.List(
                        {htmlTag: this.getParent().htmlTag()});
                    prevListItem.appendChild(newList);
                }
                children = element.firstChild().getChildren();
                child = children[0];
                while (child) {
                    child.getParent().removeChild(child);
                    newList.appendChild(child);
                    child = children[0];
                }
            } else {
                /*
                Merge any children the merging list item has on to the 
                parent being merged with.
                */
                if (this.getParent() !== element.getParent()) {
                    newList = this.firstChild();
                    if (!newList) {
                        newList = new EDITABLE.List(
                            {htmlTag: this.getParent().htmlTag()});
                        this.appendChild(newList);
                    }
                    children = element.firstChild().getChildren();
                    child = children[0];
                    while (child) {
                        child.getParent().removeChild(child);
                        newList.appendChild(child);
                        child = children[0];
                    }
                }
            }
        }
        // Merge the contents of the list items
        var index = this.htmlSoup().getLength();
        if (!element.isEmpty()) {
            // Paste content on to the end of the merged into item
            this._htmlSoup.paste(index, element.htmlSoup());
            this.refreshHTML();
        }
        // Set the selection at the end of the merged into item
        this.textSelection([index, index]);
        this.select();
        // If the merged in element had content in it remove it from its 
        // parent (auto remove will deal with it otherwise).
        if (element._htmlSoup.getLength() > 0) {
            element.getParent().removeChild(element);
        }
    });

EDITABLE.defineMerger(EDITABLE.Text, EDITABLE.ListItem,
    function (element) {
        if (element.firstChild() && 
            element.firstChild().getChildCount() > 0) {
            
            // Unindent any of the list items children
            var children = element.firstChild().getChildren();
            var child = children[0];
            var i = 0;
            while (child) {
                child.getParent().removeChild(child);
                element.getParent().insertChild(i, child);
                child = children[0];
                i++;
            }           
        }
        // Merge the contents of the list item
        var index = this.htmlSoup().getLength();
        if (!element.isEmpty()) {
            // Paste content on to the end of the merged into item
            this._htmlSoup.paste(index, element.htmlSoup());
            this.refreshHTML();
        }
        // Set the selection at the end of the merged into item
        this.textSelection([index, index]);
        this.select();
        // If the merged in element had content in it remove it from its 
        // parent (auto remove will deal with it otherwise).
        if (element._htmlSoup.getLength() > 0) {
            element.getParent().removeChild(element);
        }       
    });


// Droppers
EDITABLE.defineDropper(EDITABLE.ListItem, EDITABLE.ListItem, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        if (this.parentOf(element)) {
            /*
            Safely remove the parent element from the list by unindenting
            any of its children,
            */
            var children = this.firstChild().getChildren();
            var child = children[0];
            var i = 0;
            while (child) {
                child.getParent().removeChild(child);
                this.getParent().insertChild(i, child);
                child = children[0];
                i++;
            }
        } 
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        this.getParent().removeChild(this);
        element.getParent().insertChild(insertIndex, this);
        this.select();    
    });

EDITABLE.defineDropper(EDITABLE.Text, EDITABLE.ListItem, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        var converted_element = this.as(EDITABLE.ListItem);
        element.getParent().insertChild(insertIndex, converted_element);
        converted_element.select();
    });

EDITABLE.defineDropper(EDITABLE.ListItem, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        if (this.getChildCount()) {
            /*
            Unindent children in reverse otherwise after the first child is 
            unindented all the children will have moved to the first child 
            and will remain indented.
            */
            var children = this.getChildren()[0].getChildren();
            for (var i = children.length; i > 0 ; i--) {
                children[i - 1].unindent();
            }
        }
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        var converted_element = this.as(EDITABLE.Text);
        element.getParent().insertChild(insertIndex, converted_element);
        converted_element.select();
    });