/**
 * @fileoverview    Editable text elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 */

'use strict';

/**
 * An editable text element.
 * @class An editable text element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable text element.
 * @param {String} htmlTag The HTML element type the editable element 
 * represents.
 * @param {String} align The alignment of the elements content.
 * @param {String} style The elements style.
 * @param {String|IXMO.Soup} content The content of the element.
 * @constructor
 */    
EDITABLE.Text = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('Text');
    
    /*
    A snapshot of the HTML for the element is used to check for changes in 
    the HTML and prevent unnessecary (and time consuming) parses with IXMO.
    */
    
    /** @private */
    this._htmlSnapshot = null;
    
    /** @private */
    this._htmlSoup = null;
    
    /** @private */
    this._textSelection = new TEXT_SELECTION.Range(0, 0);
    
    this.htmlTag(options.htmlTag);
    this.align(options.align);
    this.styles(options.styles || []);
    this.htmlSoup(options.content);
};
UTILS.copyPrototype(EDITABLE.Text, EDITABLE.HTMLElement);

$$.extend(EDITABLE.Text.prototype, {
    
    __className__: 'EDITABLE.Text',
    
    /**
     * Return true if the elements content has been modified.
     * @return 
     * @type Boolean
     */
    isModified: function () { 
        this._syncHTMLSoup();
        return this._modified; 
    },
    
    /**
     * Return true if the elements is empty of content.
     * @return 
     * @type Boolean
     */
    isEmpty: function () { 
        return this.htmlSoup().allWhitespace(); 
    },
    
    /**
     * Set/Get the HTML soup for the element.
     * @param {String|IXMO.Soup} source (Optional) If undefined the current
     * HTML soup is returned, else the HTML soup type is set.
     * @param {Boolean} refresh If true and returning the current HTML soup,
     * the HTML contents is synchronised from the page HTML (true by default).
     * @return The HTML soup.
     * @type IXMO.Soup
     */
    htmlSoup: function (source, refresh) {
        if (source === undefined) {
            if (refresh === undefined || refresh === true) {
                this._syncHTMLSoup();
            }
            return this._htmlSoup;
        }
        var soup = new IXMO.Soup(source);
        this.trigger('htmlSoupChange', function () {
            this._htmlSoup = soup;
            this.refreshHTML();
        }, {'source': source});
    },
    
    /**
     * Refresh the HTML content of the element from the soup. 
     * 
     * Tip: This is useful when the htmlSoup has been modified directly 
     * rather than via the htmlSoup method.
     */
    refreshHTML: function () {
        if (this.getInnerHTMLElement()) {
            $$(this.getInnerHTMLElement()).html(
                this._htmlSoup.toString({optimize: false}));
            this._syncHTMLSoup();
        }
    },
    
    /**
     * Set/Get the text selection for the element.
     * @param {Array|TEXT_SELECTION.Range} range (Optional) If undefined the
     * current text selection is returned, else the selection is set.
     * @return The text selection.
     * @type TEXT_SELECTION.Range
     */
    textSelection: function (range) {
        if (range === undefined) {
            if (this.isSelected()) {
                // Return the real-time current selection
                try {
                    return TEXT_SELECTION.query(this.getInnerHTMLElement());
                } catch (ie_error) {
                    /*
                    @@ Internet explorer has problems occassionally because 
                    it has to use IXMO for selection, and if this method is 
                    called during a drag problems occur due to the inner 
                    helper div.
                    */
                    return this._textSelection;
                }
            } else {
                // Return last known selection
                return this._textSelection;
            }
        }
        if (!this._textSelection.eq(range)) {
            this.trigger('textSelectionChange', function () {
                this._textSelection = new TEXT_SELECTION.Range(range[0], 
                    range[1]);
            }, {'range': range});
        }
        if (this.isSelected() && !this.isEmpty()) {
            /*
            When reselecting after resetting the element to content editable 
            the current text selection may be incorrect (out of date). To 
            resolve this, if we fail to set the selection (typically due to 
            an out of range error), then we query again and reselect.
            */
            try {
                TEXT_SELECTION.select(this._textSelection,
                    this.getInnerHTMLElement());
            } catch (error) {
                TEXT_SELECTION.select(TEXT_SELECTION.query(
                    this.getInnerHTMLElement()),
                    this.getInnerHTMLElement());
            }
        }
    },    
    
    /**
     * Select the element.
     */
    select: function () {
        if (this.isSelected()) {
            return;
        }
        this.trigger('select', function (event) {
            if (this.getRoot()._selectedElement) {
                this.getRoot()._selectedElement.unselect();
            }
            this._setContentEditable();
            this._selected = true;
            this.getRoot().selectedElement(this);
        });
    },
    
    /**
     * Unselect the element.
     * @param {Boolean} snapshotTextSelection If true then a snapshot of the 
     * current text selection is taken before unselecting the element (true 
     * by default).
     */
    unselect: function (snapshotTextSelection) {
        if (!this.isSelected()) {
            return;
        }
            
        this.trigger('unselect', function (event) {
            this._selected = false;
            if (this.htmlSoup().getLength() > 0) {
                // Snapshot the current text selection
                if (snapshotTextSelection !== false) {
                    this._snapshotTextSelection();
                }
                TEXT_SELECTION.clear();
                /*
                Important! HTML elements that have had the contentEditable 
                attribute set must be re-added because the IE render engine 
                makes such elements behave as if the style overlay:auto; is 
                in effect (even if the attribute is removed).
                */
                this.updateHTML();
                $$(this.getInnerHTMLElement()).removeClass(
                    EDITABLE_CSS_TMP_PREFIX + 'edit');
                this.getRoot().clearSelectedElement(this);
            } else {
                // Auto-remove empty children
                this.getParent().removeChild(this);
            }
        });
    },
    
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this._htmlTag,
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                align: this.align() ? 
                    EDITABLE_CSS_ALIGN_PREFIX + this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
                /*
                Important! We don't use 'htmlSoup' call here to prevent a
                sync.
                */
                content: this._htmlSoup.toString({optimize: false})
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> ' +
                '<%=cls%> <%=align%> <%=styles%> dropper"><%=content%>' + 
                '</<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML element
            this._bindMouseEvents();
            this._bindKeyEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this._selected) {
                this._setContentEditable();
            }
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        var content = this._htmlSoup.toString();
        if (content === EDITABLE_DEFAULT_CONTENT) {
            content = '';
        }
        
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            type: this._htmlTag,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            align: this.align() ? ' ' + EDITABLE_CSS_ALIGN_PREFIX + 
                this.align() : '',
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
            content: content
        };
        
        var html = UTILS.template(
            '<<%=type%><% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=cls%><%=align%><%=styles%>">' + 
            '<%=content%></<%=type%>>',
            templateVars);
        return html;
    },
    
    /**
     * Split the text element into two.
     * @param {Array} selection The selection to split on.
     */
    split: function (selection) {
        // Split on the return
        var htmlSoups = this.htmlSoup().split(selection);
        var first = htmlSoups[0];
        var firstEmpty = first.allWhitespace();
        var last = htmlSoups[1];
        var lastEmpty = last.allWhitespace();
        
        // If after the split either side is empty then remove it
        if (firstEmpty) {
            first = new IXMO.Soup(this.defaultOptions.content);
        }
        if (lastEmpty) {
            last = new IXMO.Soup(this.defaultOptions.content);
        }
        if (firstEmpty && lastEmpty) {
            return false;
        }
        this._htmlSoup = first;
        this.refreshHTML();
        var newElement = new EDITABLE[this.getType()]({
            align: this.align(),
            styles: this.styles().slice(0),
            content: last
        });
        var index = this._parent.indexOfChild(this) + 1;
        this._parent.insertChild(index, newElement);
    },
    
    
    /**
     * Return a copy of the Text element.
     * @return A copy of the Text element.
     * @type EDITABLE.Text
     */
    copy: function () {
        return new EDITABLE.Text({
            htmlTag: this.htmlTag(),
            align: this.align(),
            styles: this.styles().slice(0),
            content: this.htmlSoup()
        });
    },
    
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['address', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p',
        'pre'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'p',
        align: 'left',
        content: EDITABLE_DEFAULT_CONTENT
    },
    
    /** @private */
    _syncHTMLSoup: function () {
        var newHTML = $$(this.getInnerHTMLElement()).html() || '';
        if (this._htmlSnapshot !== newHTML) {
            var first_sync = (this._htmlSnapshot === null);
            this._htmlSnapshot = newHTML;
            this._htmlSoup = new IXMO.Soup(this._htmlSnapshot);

            // If the last character is a <br /> without a class remove it as
            // it is most likely a place holder used by some browsers to keep 
            // whitespace valid.
            var len = this._htmlSoup.getLength();
            if (len) {
                var lastChar = this._htmlSoup.charAt(len - 1);
                if (lastChar.isTag('br') && 
                        !lastChar.getTags('br')[0].getAttribute('class')) {
                    this._htmlSoup.chop();
                }
            }
            
            /*
            Don't report a taint event if the HTML snapshot for the text 
            element hasn't yet been set.
            */
            if (!first_sync) {
                this.taint();
            }
        }
    },
    
    /** @private */
    _snapshotTextSelection: function () {
        var range;
        try {
            range = TEXT_SELECTION.query(this.getInnerHTMLElement());
        } catch (error) {
            /* 
            Ignore errors failure to capture snapshot, typically the element
            no longer has the current selection.
            */
        }
        if (range !== undefined) {
            this.textSelection(range);
        }    
    },
    
    /** @private */
    _setContentEditable: function () {
        /* 
        Important! Here we set the HTML element as editable, IE7 requires
        that the attribute name has a capital 'E' as in 'contentEditable' 
        otherwise the setting is ignored.
        */
        $$(this.getInnerHTMLElement()).attr('contentEditable', 'true');
        $$(this.getInnerHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
            'selected');
        $$(this.getInnerHTMLElement()).removeClass(EDITABLE_CSS_TMP_PREFIX + 
            'over');
        // Set the selection
        $$(this.getInnerHTMLElement()).focus();
        if (!this.isEmpty()) {
            TEXT_SELECTION.select(this._textSelection,
                this.getInnerHTMLElement());
        }
    },
    
    /** @private */
    _bindMouseEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());

        htmlElement.bind('blur.editable', this, function (event) {
            if (event.data.htmlSoup().toString() == '') {
                event.data.htmlSoup(EDITABLE_DEFAULT_CONTENT);
                event.data.refreshHTML();
            }
        });
       
        // Hover
        htmlElement.bind('mouseover.editable', this, function (event) {
            if (!event.data._selected) {
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'over');
        });

        // Click
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data.select();
        });
        
        // DblClick
        htmlElement.bind('dblclick.editable', this, function (event) {
            event.data.trigger('properties', function () {
                // Double click is the standard event for providing external
                // applications quick link to the items properties.    
            }, {});            
        });
    },
    
    /** @private */
    _bindKeyEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());
        
        // Key press
        htmlElement.bind('keydown.editable', this, function (event) {
            event.data._syncHTMLSoup();
            
            var allowEvent = true;
            switch (event.which) {
        
            // Merging
            case 8:
                allowEvent = event.data._inputBack(event);
                break;
            
            case 46:
                allowEvent = event.data._inputDelete(event);
                break;
                
            // Split
            case 13:
                allowEvent = event.data._inputReturn(event);
                break;                            

            // Navigation
            case 37:
                allowEvent = event.data._inputArrowLeft(event);
                break;
                
            case 39:
                allowEvent = event.data._inputArrowRight(event);
                break;

            case 38:
                allowEvent = event.data._inputArrowUp(event);
                break;                
                
            case 40:
                allowEvent = event.data._inputArrowDown(event);
                break;                
                
            case 33:
                allowEvent = event.data._inputPageUp(event);
                break;                  

            case 34:
                allowEvent = event.data._inputPageDown(event);
                break;
            
            case 9:
                allowEvent = event.data._inputTab(event);
                break;
                
            default:
                break;
            }
            event.data._syncHTMLSoup();
            return allowEvent;
        });
        
        htmlElement.bind('keyup.editable', this, function (event) {
            event.data._syncHTMLSoup();
        });
        
        // Paste event
        htmlElement.bind('paste', this, function (event) {
            event.data.trigger('paste', function () {
                // Paste events should be handled externally from the library
            }, {});
            return false;
        });   
    },

    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getInnerHTMLElement());

        // Mouse down
        htmlElement.bind('mousedown.editable', this, function (event) {
            
            // Ignore right mouse clicks
            if (event.button === 2) {
                return;
            }
            
            event.data._hasMouse = true;
            var mousedown_event = event;
            
            /*
            This avoids a potential FireFox and Safari issue caused by 
            selected text not reporting mouse[over/out] events. By removing 
            the selection we ensure this situation can't arise.
            */                  
            event.data._snapshotTextSelection();
            TEXT_SELECTION.clear();
                
            // Dragging
            $$(this).oneTime(EDITABLE_DRAG_HOLD_DURATION, 'drag.editable', 
                    function (event) {
                
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                // Prepare the element for dragging
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
                $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this).attr('contentEditable', 'False');
                TEXT_SELECTION.clear();
                    
                // Calculate the cursor offset to start dragging from
                var margin = $$(this).margin();
                var cursor_left = -Math.max(0, margin.left);
                var cursor_top = -Math.max(0, margin.top);
                
                /*
                We have to ensure that each dragger function checks for an 
                editable element with text elements because occasionally drag
                is initialised incorrectly, problably due to the hack we have
                in place to allow us to initialise dragging after a click and
                hold.
                */
                $$(this).draggable({
                    revert: 'valid',
                    revertDuration: 0,
                    cursor: 'move',
                    cursorAt: {left: cursor_left, top: cursor_top},
                    helper: function () {
                        if ($$(this).data('editableElement')) {
                            return $$(this).data(
                                'editableElement')._dropperHTML();
                        }
                    },
                    drag: function (event, ui) {
                        if ($$(this).data('editableElement')) {
                            $$(this).data('editableElement')._draggableDrag(
                                this, event, ui);
                        }
                    },
                    stop: function (event, ui) {
                        if ($$(this).data('editableElement')) {
                            $$(this).data('editableElement')._draggableStop(
                                this, event, ui);
                        }
                    }
                });
                
                $$(this).data('editableElement')._hasDragged = false;
                EDITABLE.HTMLElement._triggerDrag(this, mousedown_event);
            });
        });
        
        // Mouse up
        htmlElement.bind('mouseup.editable', this, function (event) {
            event.data._hasMouse = false;
            $$(this).stopTime('drag.editable');
            
            var dragElement = $$(this).data('editableElement');
            if ($$(this).data('draggable') && !dragElement._hasDragged) {
                $$(this).draggable({destroy: true});
                event.data.updateHTML();
            } else {
                event.data._syncHTMLSoup();
            }
            event.data._prevMouseX = null;
            event.data._prevMouseY = null;
        });
        
        // Mouse out
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).stopTime('drag.editable');
        });
        
        // Mouse move
        htmlElement.bind('mousemove.editable', this, function (event) {
            if (event.data._hasMouse) {
                if ((event.data._prevMouseX !== null && 
                    event.data._prevMouseY !== null) &&
                    (event.data._prevMouseX !== event.pageX || 
                    event.data._prevMouseY !== event.pageY)) {
                        
                    $$(this).stopTime('drag.editable');
                }
                event.data._prevMouseX = event.pageX;
                event.data._prevMouseY = event.pageY;
            }
        });
        
        // Dropping
        htmlElement.droppable({
            greedy: true,
            drop: function (event, ui) {
                $$(this).data('editableElement')._droppableDrop(
                    this, event, ui);
            },
            over: function (event, ui) {
                $$(this).data('editableElement')._droppableOver(
                    this, event, ui);
            },
            out: function (event, ui) {
                $$(this).data('editableElement')._droppableOut(
                    this, event, ui);
            }
        });
    },

    /** @private */
    _inputBack: function (event) {
        if (this.textSelection()[0] === 0 && 
                this.textSelection().isCollapsed()) {
            
            // Check the parent collection allows for self remove
            if (!this.getHTMLCollection().can('remove')) {
                this.htmlSoup(EDITABLE_DEFAULT_CONTENT);
                this.refreshHTML();
                return false;
            }
            
            // Merge
            var prev = this.prevRelative({typeOf: 'Text'});
            if (prev && !prev.typeOf('TableCell')) {
                prev.merge(this);
            }
            return false;
        }
    },
    
    /** @private */
    _inputDelete: function (event) {
        var index = this.htmlSoup().getLength();
        if (this.textSelection()[0] === index && 
                this.textSelection().isCollapsed()) {
            
            // Check the parent collection allows for self remove
            if (!this.getHTMLCollection().can('remove')) {
                this.htmlSoup(EDITABLE_DEFAULT_CONTENT);
                this.refreshHTML();
                return false;
            }
            
            // Merge
            var next = this.nextRelative({typeOf: 'Text'});
            if (next) {
                this.merge(next);
            }
            return false;
        }
    },

    /** @private */
    _inputReturn: function (event) {
        // Check the parent collection allows for self add
        if (!this.getHTMLCollection().can('add')) {
            return false;
        }
        
        // Split on the return
        var htmlSoups = this.htmlSoup().split(this.textSelection());
        var first = htmlSoups[0];
        var firstEmpty = first.allWhitespace();
        var last = htmlSoups[1];
        var lastEmpty = last.allWhitespace();
        
        // If after the split either side is empty then remove it
        if (firstEmpty) {
            first = new IXMO.Soup(this.defaultOptions.content);
        }
        if (lastEmpty) {
            last = new IXMO.Soup(this.defaultOptions.content);
        }
        if (firstEmpty && lastEmpty) {
            return false;
        }
        this._htmlSoup = first;
        this.refreshHTML();
        var newElement = new EDITABLE[this.getType()]({
            align: this.align(),
            styles: this.styles().slice(0),
            content: last
        });
        var index = this._parent.indexOfChild(this) + 1;
        this._parent.insertChild(index, newElement);     
        if (firstEmpty) {
            this.textSelection([0, first.getLength()]);
        } else {
            this.textSelection([0, 0]);
            if (lastEmpty) {
                newElement.textSelection([0, last.getLength()]);
            }
            newElement.select();
        }
        
        if (firstEmpty) {
            this.textSelection([0, first.getLength()]);
        } else {
            this.textSelection([0, 0]);
            if (lastEmpty) {
                newElement.textSelection([0, last.getLength()]);
            }
            newElement.select();
        }
        
        return false;
    },
    
    /** @private */
    _inputArrowLeft: function (event) {
        if (this.textSelection()[0] === 0 && 
                this.textSelection().isCollapsed()) {
            var prev = this.prevRelative({typeOf: 'Text'});
            if (prev) {
                var len = prev.htmlSoup().getLength();
                prev.textSelection([len, len]);
                prev.select();
            }
            return false;
        }
    },
    
    /** @private */
    _inputArrowRight: function (event) {
        var len = this._htmlSoup.getLength();
        if (this.textSelection()[1] === len && 
                this.textSelection().isCollapsed()) {
            var next = this.nextRelative({typeOf: 'Text'});
            if (next) {
                next.textSelection([0, 0]);
                next.select();
            }
            return false;
        }
    },    
    
    /** @private */
    _inputArrowUp: function (event) { 
        return this._inputArrowLeft(event);
    },

    /** @private */
    _inputArrowDown: function (event) { 
        return this._inputArrowRight(event);
    },
    
    /** @private */
    _inputPageUp: function (event) {
        TEXT_SELECTION.select([0, 0], this.getInnerHTMLElement());
        this.textSelection([0, 0]);
        return false;
    },
    
    /** @private */
    _inputPageDown: function (event) {
        var len = this._htmlSoup.getLength();
        TEXT_SELECTION.select([len, len], this.getInnerHTMLElement());
        this.textSelection([len, len]);
        return false;
    },
    
    /** @private */
    _inputTab: function (event) {
        var element = null;
        if (event.shiftKey) {
            element = this.prevRelative({typeOf: 'Text'});
        } else {
            element = this.nextRelative({typeOf: 'Text'});
        }
        if (element) {
            element.select();
        }
        return false;
    },
    
    /** @private */
    _dropperHTML: function () {
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            content: UTILS.abbreviate($$(this.getHTMLElement()).text(), 
                EDITABLE_HELPER_CONTENT_LENGTH)
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' + 
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            '<%=content%></div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' +
            'dropper-overlay"></div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
});

/**
 * Convert a HTML element (and its contents) into a Text editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return A Text editable element.
 * @type EDITABLE.Text
 */
EDITABLE.Text.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, css);
    var styles = EDITABLE._getStylesFromCSS(css);
    var content = $$(htmlElement).html();
    var options = {
        htmlTag: htmlElement.nodeName,
        align: align.length > 0 ? align[0] : undefined,
        styles: styles,
        content: content.replace(' ', '') !== '' ? content : undefined
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    return new EDITABLE.Text(options);
};

// Define which element classes can hold Text elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'Text', true);

// Define mergers
EDITABLE.defineMerger(EDITABLE.Text, EDITABLE.Text, 
    function (element) {
        var index = this.htmlSoup().getLength();
        if (!element.isEmpty()) {
            this._htmlSoup.paste(index, element.htmlSoup());
            this.refreshHTML();
        }
        this.textSelection([index, index]);
        this.select();
        if (element._htmlSoup.getLength() > 0) {
            element.getParent().removeChild(element);
        }        
    });

// Define droppers
EDITABLE.defineDropper(EDITABLE.Text, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });
    
/**
 * An editable caption element. A caption element is similar to a text 
 * element but cannot be dragged and does not support for the creation of new
 * elements using the return key. Captions must sit within block elements 
 * such as tables.
 * @class An editable caption element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable caption element.
 * @param {String} htmlTag The HTML element type the editable element 
 * represents.
 * @param {String} align The alignment of the elements content.
 * @param {String} style The elements style.
 * @param {String|IXMO.Soup} content The content of the element.
 * @constructor
 */    
EDITABLE.Caption = function (options) {
    EDITABLE.Text.call(this, options);
    this._type.push('Caption');
    
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    this.htmlTag(options.htmlTag);
    this.align(options.align);
    this.styles(options.styles || []);
    this.htmlSoup(options.content);
};
UTILS.copyPrototype(EDITABLE.Caption, EDITABLE.Text);

$$.extend(EDITABLE.Caption.prototype, {
    
    __className__: 'EDITABLE.Caption',
    
    /**
     * Return the inner (editable component) associated HTML element.
     * @return The HTML element.
     * @type String
     */
    getInnerHTMLElement: function () { 
        // * See note in addHTML method.
        var htmlElement = $$('#' + this._htmlElementId).find(
            '.' + EDITABLE_CSS_BASE_INNER);
        if (htmlElement.length > 0) {
            htmlElement = htmlElement[0];
        } else {
            htmlElement = null;
        }
        return htmlElement;
    },
    
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                type: this._htmlTag,
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                align: this.align() ? 
                    EDITABLE_CSS_ALIGN_PREFIX + this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
                /*
                Important! We don't use 'htmlSoup' call here to prevent a
                sync.
                */
                content: this._htmlSoup.toString({optimize: false})
            };
            var html = UTILS.template(
                '<<%=type%> id="<%=id%>" class="' +
                '<%=EDITABLE_CSS_BASE%> <%=cls%> <%=align%> ' + 
                '<%=styles%>">' + 
                '<span class="<%=EDITABLE_CSS_BASE_INNER%>">' +
                '<%=content%></span></<%=type%>>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            // Store the element against the HTML element        
            $$(this.getInnerHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML element
            this._bindMouseEvents();
            this._bindKeyEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this._selected) {
                this._setContentEditable();
            }
        });
    },
    
    /**
     * Return a copy of the Caption element.
     * @return A copy of the Caption element.
     * @type EDITABLE.Text
     */
    copy: function () {
        return new EDITABLE.Caption({
            htmlTag: this.htmlTag(),
            align: this.align(),
            styles: this.styles().slice(0),
            content: this.htmlSoup()
        });
    },
    
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['caption'],
    
    /**
     * The default options when creating a new editable text element.
     * @type Object
     */
    defaultOptions: {
        htmlTag: 'caption',
        align: 'left',
        content: EDITABLE_DEFAULT_CONTENT
    },
    
    /** @private */
    _bindLayoutEvents: function () {
        
    },

    /** @private */
    _inputBack: function (event) {
        // @@ Delete captions when they are empty???
    },
    
    /** @private */
    _inputDelete: function (event) {
        // @@ Delete captions when they are empty???
    },

    /** @private */
    _inputReturn: function (event) {
        return false;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
});


/**
 * Convert a HTML element (and its contents) into a Caption editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return A Caption editable element.
 * @type EDITABLE.Caption
 */
EDITABLE.Caption.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, css);
    var styles = EDITABLE._getStylesFromCSS(css);
    var content = $$(htmlElement).html();
    var options = {
        htmlTag: htmlElement.nodeName,
        align: align.length > 0 ? align[0] : undefined,
        styles: styles,
        content: content.replace(' ', '') !== '' ? content : undefined
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    return new EDITABLE.Caption(options);
};