/**
 * @fileoverview    Editable video elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 * @requires        EDITABLE (image) (/js/editable/image.js)
 */

'use strict';

/**
 * An editable video element.
 * @class An editable video element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable text element.
 * @param {String} alt The alternative text for the video.
 * @param {String} align The alignment of the video relative to other 
 * elements.
 * @param {Array} constraints The min/max size of the video in pixels as an 
 * Array [min, max].
 * @param {Array} size The size of the video in pixels as an Array [width, 
 * height].
 * @param {String} src The source for the video.
 * @param {String} style The videos style class.
 * @constructor
 */    
EDITABLE.Video = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('Image');
    this._type.push('Video');
    
    /** @private */
    this._title = '';
    
    /** @private */
    this._size = options.size;
    
    /** @private */
    this._constraints = options.constraints || [50, 500];
    
    /** @private */
    this._href = '';
    
    /* @private */
    this._dropAlign = 'center';
    
    this.htmlTag('a');
    this.href(options.href);
    this.title(options.title);
    this.size(options.size);
    this.constraints(options.constraints);
    this.align(options.align);
    this.styles(options.styles || []);
};
UTILS.copyPrototype(EDITABLE.Video, EDITABLE.Image);

$$.extend(EDITABLE.Video.prototype, {
    
    __className__: 'EDITABLE.Video',
    
    /**
     * Return the inner (editable component) associated HTML element.
     * @return The HTML element.
     * @type String
     */
    getInnerHTMLElement: function () { 
        return this.getHTMLElement();
    },
    
    /**
     * Set/Get the title for the video.
     * @param {String} value (Optional) If undefined the current title for
     * the video is returned, else the videos title is set. An title 
     * attribute must be present for a video element but an empty string 
     * value is acceptable if not advisable.
     * @return The videos title text.
     * @type String
     */
    title: function (value) {
        if (value === undefined) {
            return this._title;
        }
        
        if (this._title !== value) {
            this.trigger('titleChange', function () {
                this._title = value;
                $$(this.getInnerHTMLElement()).attr('title', this._title);
                this.taint();
            }, {'value': value});
        }
    },
    
    /**
     * Select the element.
     */
    select: function () {
        if (this.isSelected()) {
            return;
        }
        
        this.trigger('select', function (event) {
            if (this.getRoot()._selectedElement) {
                this.getRoot()._selectedElement.unselect();
            }
            $$(this.getHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
                'selected');
            $$(this.getHTMLElement()).removeClass(EDITABLE_CSS_TMP_PREFIX + 
                'over');
            this._selected = true;
            this.getRoot().selectedElement(this);
        });
    },
    
    /**
     * Unselect the element.
     */
    unselect: function () {
        if (!this.isSelected()) {
            return;
        }
        
        this.trigger('unselect', function (event) {
            $$(this.getHTMLElement()).removeClass(
                EDITABLE_CSS_TMP_PREFIX + 'selected');
            // Hide the helper
            $$(this.getHelperHTMLElement()).hide();
            this._selected = false;
            this.getRoot().clearSelectedElement(this);       
        });
    },
    
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                href: this.href(),
                title: this.title(),
                size: this.size(),
                align: this.align() ? EDITABLE_CSS_ALIGN_PREFIX + 
                    this.align() : '',
                styles: this.styles() ? this.styles().join(' ') : '',
            };
            var html = UTILS.template(
                '<a id="<%=id%>" href="<%=href%>" ' + 
                'class="<%=EDITABLE_CSS_BASE%> <%=cls%> <%=align%> ' +
                '<%=styles%>" style="width:<%=size[0]%>px; ' + 
                'height:<%=size[1]%>px;"><%=title%></a>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            
            // Store the element against the HTML element        
            $$(this.getHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML
            this._bindMouseEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this.isSelected()) {
                $$(this.getHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
                    'selected');
                $$(this.getHTMLElement()).removeClass(
                    EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            href: this.href(),
            title: this.title(),
            size: this.size(),
            constraints: this.constraints(),
            align: this.align() ? ' ' + EDITABLE_CSS_ALIGN_PREFIX + 
                this.align() : '',
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
        };
        var html = UTILS.template(
            '<a<% if (id !== null) { %> id="<%=id%>"<% } %> href="<%=href%>" class="<%=cls%><%=align%><%=styles%>" ' +
            'style="width:<%=size[0]%>px; height:<%=size[1]%>px; min-width:<%=constraints[0]%>px; max-width:<%=constraints[1]%>px;">' + 
            '<%=title%></a>',
            templateVars);
        return html;
    },
    
    /**
     * Return a copy of the Video element.
     * @return A copy of the Video element.
     * @type EDITABLE.Video
     */
    copy: function () {
        return new EDITABLE.Video({href: this.href(),
            size: this.size(),
            title: this.title(),
            constraints: this.constraints(),
            align: this.align(),
            styles: this.styles().slice(0)
            });
    },
        
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['a'],
    
    /**
     * The default options when creating a new editable Video element.
     * @type Object
     */
    defaultOptions: {
        size: [1, 1],
        href: '',
        title: '',
        constraints: [50, 500],
        align: 'center'
    },
    
    /** @private */
    _bindMouseEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
        
        // Hover
        htmlElement.bind('mouseover.editable', this, function (event) {
            if (!event.data._selected) {
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'over');
        });
        
        // Click
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data.select();
        });
        htmlElement.bind('click.editable', this, function (event) {
            // Prevent the video (which is a link) from activating.
            return false;
        });

        // DblClick
        htmlElement.bind('dblclick.editable', this, function (event) {
            event.data.trigger('properties', function () {
                // Double click is the standard event for providing external 
                // applications quick link to the items properties.    
            }, {});            
            return false;
        });
    },

    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
                
        // Dragging
    
        // Calculate the cursor offset to start dragging from
        var margin = htmlElement.margin();
        var cursor_left = -Math.max(0, margin.left);
        var cursor_top = -Math.max(0, margin.top);

        htmlElement.draggable({
            revert: 'valid',
            revertDuration: 0,
            cursor: 'move',
            cursorAt: {top: cursor_top, left: cursor_left},
            distance: 5,
            helper: function () {
                return $$(this).data('editableElement')._dropperHTML();
            },
            start: function (event, ui) {
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
            },
            drag: function (event, ui) {
                $$(this).data('editableElement')._draggableDrag(this, event, 
                    ui);
            },
            stop: function (event, ui) {
                $$(this).data('editableElement')._draggableStop(this, event, 
                    ui);
            }
        });
        
        // Resizing
        htmlElement.resizable({
            aspectRatio: this.getAspectRatio(),
            handles: 'ne, se, sw, nw',
            minWidth: this._minSize()[0],
            minHeight: this._minSize()[1],
            maxWidth: this._maxSize()[0],
            maxHeight: this._maxSize()[1],
            start: function (event, ui) {
                $$(this).css('position', 'relative');
                
                // Add the size information helper
                var size_info_html = UTILS.template(
                    '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>size-info">' + 
                    '</div>', 
                    {});
                $$(this).append(size_info_html);
            },
            resize: function (event, ui) {
                $$(this).css({top: '0', left: '0'});
                
                // Update the size information helper
                var size_info = $$(this).find('.' + EDITABLE_CSS_TMP_PREFIX +
                    'size-info');
                $$(size_info).html(UTILS.template(
                    'w&nbsp;<%=width%> h&nbsp;<%=height%>', 
                    {width: $$(this).width(), height: $$(this).height()}));
            },
            stop: function (event, ui) {
                $$(this).data('editableElement').size(
                    [ui.size.width, ui.size.height]);
                
                // Remove the size information helper
                var size_info = $$(this).find('.' + 
                    EDITABLE_CSS_TMP_PREFIX + 'size-info');
                $$(size_info).remove();
            }
        });
    },
    
    /** @private */
    _dropperHTML: function () {
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            title: UTILS.abbreviate(this.title(), 
                EDITABLE_HELPER_CONTENT_LENGTH)
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' + 
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            '<%=title%></div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' + 
            'dropper-overlay"></div></div>',
            templateVars);    
        return html;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
    
    /** @ignore */
    src: undefined,
    
    /** @ignore */
    alt: undefined,
    
    /** @ignore */
    longdesc: undefined    
});

/**
 * Convert a HTML element into a Video editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return An Video editable element.
 * @type EDITABLE.Video
 */
EDITABLE.Video.fromHTML = function (parent, htmlElement) {
    // Size
    var size = null;
    var width = $$(htmlElement).css('width').replace(/(\d+)px/i, '$1');
    var height = $$(htmlElement).css('height').replace(/(\d+)px/i, '$1');
    if (width && Number(width) > 0 && height && Number(height)) {
        size = [Number(width), Number(height)];
    }
    
    // Constraints
    var constraints = [50, 500];
    var minSize = $$(htmlElement).css('min-width').replace(/(\d+)px/i, '$1');
    var maxSize = $$(htmlElement).css('max-width').replace(/(\d+)px/i, '$1');
    if (minSize && Number(minSize) > 0) {
        constraints[0] = Number(minSize);
    }
    if (maxSize && Number(maxSize) > 0) {
        constraints[1] = Number(maxSize);
    }
    
    var css = $$(htmlElement).attr('class');
    var align = EDITABLE._getSettingFromCSS(EDITABLE_CSS_ALIGN_PREFIX, css);
    var styles = EDITABLE._getStylesFromCSS(css);
    var title = $$(htmlElement).html();
    var options = {
        href: $$(htmlElement).attr('href'),
        size: size,
        constraints: constraints,
        align: align.length > 0 ? align[0] : undefined,
        style: styles,
        title: title.replace(' ', '') !== '' ? title : undefined
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    return new EDITABLE.Video(options);
};

// Define which element classes can hold Video elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'Video');

// Define droppers
EDITABLE.defineDropper(EDITABLE.Video, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        if (region[1] === -1) {
            this.align('left');
        } else if (region[1] === 1) {
            this.align('right');
        } else if (region[0] === 1) {
            this.align('center');
            insertIndex += 1;
        } else {
            this.align('center');
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });

EDITABLE.defineDropper(EDITABLE.Video, EDITABLE.Rule, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });

EDITABLE.defineDropper(EDITABLE.Video, EDITABLE.List, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = $$(element.getInnerHTMLElement());
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        if (region[1] === -1) {
            this.align('left');
        } else if (region[1] === 1) {
            this.align('right');
        } else if (region[0] === 1) {
            this.align('center');
            insertIndex += 1;
        } else {
            this.align('center');
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });