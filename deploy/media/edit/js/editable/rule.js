/**
 * @fileoverview    Editable rule elements. (Part of editable web elements).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        $$ (/js/framework.js)
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        IXMO (/js/core/ixmo.js)
 * @requires        TEXT_SELECTION (/js/core/text-selection.js)
 * @requires        EDITABLE (elements) (/js/editable/elements.js)
 * @requires        EDITABLE (text) (/js/editable/text.js)
 */

'use strict';

/**
 * An editable rule element.
 * @class An editable rule element.
 * @param {EDITABLE.Element} parent The parent editable element.
 * @param {Object} options The options for the editable text element.
 * @param {String} style The movies style class.
 * @constructor
 */    
EDITABLE.Rule = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    EDITABLE.HTMLElement.call(this, options.id);
    this._type.push('Rule');
    
    this.htmlTag('hr');
    this.styles(options.styles ||[]);
};
UTILS.copyPrototype(EDITABLE.Rule, EDITABLE.HTMLElement);

$$.extend(EDITABLE.Rule.prototype, {
    
    __className__: 'EDITABLE.Rule',

    /**
     * Select the element.
     */
    select: function () {
        if (this.isSelected()) {
            return;
        }
        this.trigger('select', function (event) {
            if (this.getRoot()._selectedElement) {
                this.getRoot()._selectedElement.unselect();
            }
            $$(this.getHTMLElement()).addClass(EDITABLE_CSS_TMP_PREFIX + 
                'selected');
            $$(this.getHTMLElement()).removeClass(EDITABLE_CSS_TMP_PREFIX + 
                'over');
            this._selected = true;
            this.getRoot().selectedElement(this);
        });
    },
    
    /**
     * Unselect the element.
     */
    unselect: function () {
        if (!this.isSelected()) {
            return;
        }
        this.trigger('unselect', function (event) {
            $$(this.getHTMLElement()).removeClass(
                EDITABLE_CSS_TMP_PREFIX + 'selected');
            this._selected = false;
            this.getRoot().clearSelectedElement(this);       
        });
    },
        
    /**
     * Add the element to the page HTML.
     */
    addHTML: function () {
        this.trigger('addHTHML', function (event) {
            
            // Render the HTML
            var templateVars = {
                id: this.getHTMLElementId(),
                cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
                styles: this.styles() ? this.styles().join(' ') : '',
            };
            var html = UTILS.template(
                '<div id="<%=id%>" class="<%=EDITABLE_CSS_BASE%> <%=cls%> ' +
                '<%=styles%>"><hr /></div>',
                templateVars);
            if (this.prevSibling()) {
                $$(this.prevSibling().getHTMLElement()).after(html);
            } else {
                $$(this._parent.getHTMLElement()).prepend(html);
            }
            // Store the element against the HTML element        
            $$(this.getHTMLElement()).data('editableElement', this);
    
            // Bind events to the HTML
            this._bindMouseEvents();
            this._bindLayoutEvents();
            
            // Re-select if required
            if (this.isSelected()) {
                $$(this.getHTMLElement()).addClass(
                    EDITABLE_CSS_TMP_PREFIX + 'selected');
                $$(this.getHTMLElement()).removeClass(
                    EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
    },
    
    /**
     * Return the HTML for this element.
     * @return The elements HTML.
     * @type String
     */
    toHTML: function () {
        var templateVars = {
            id: this._hasOwnId ? this._id : null,
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType(),
            styles: this.styles().length > 0 ? ' ' + this.styles().join(' ') : '',
        };
        var html = UTILS.template(
            '<div<% if (id !== null) { %> id="<%=id%>"<% } %> class="<%=EDITABLE_CSS_BASE%> <%=cls%><%=styles%>">' +
            '<hr /></div>',
            templateVars);
        return html;
    },
    
    /**
     * Return a copy of the Rule element.
     * @return A copy of the Rule element.
     * @type EDITABLE.Rule
     */
    copy: function () {
        return new EDITABLE.Rule({
            src: this.src(),
            size: this.size(),
            alt: this.alt(),
            constraints: this.constraints(),
            align: this.align(),
            styles: this.styles(),
            longdesc: this.longdesc(),
            href: this.href()
        });
    },
    
    /**
     * The HTML element types that editable text elements can represent.
     * @type Array
     */
    supportedHTMLTags: ['hr'],
    
    /**
     * The default options when creating a new editable Rule element.
     * @type Object
     */
    defaultOptions: {
    },

    /** @private */
    _bindMouseEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
        
        // Hover
        htmlElement.bind('mouseover.editable', this, function (event) {
            if (!event.data._selected) {
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'over');
            }
        });
        htmlElement.bind('mouseout.editable', this, function (event) {
            $$(this).removeClass(EDITABLE_CSS_TMP_PREFIX + 'over');
        });
        
        // Click
        htmlElement.bind('mousedown.editable', this, function (event) {
            event.data.select();
        });

        // DblClick
        htmlElement.bind('dblclick.editable', this, function (event) {
            event.data.trigger('properties', function () {
                // Double click is the standard event for providing external 
                // applications quick link to the items properties.    
            }, {});            
        });
    },

    /** @private */
    _bindLayoutEvents: function () {
        var htmlElement = $$(this.getHTMLElement());
        
        // Dragging
    
        // Calculate the cursor offset to start dragging from
        var margin = htmlElement.margin();
        var cursor_left = -Math.max(0, margin.left);
        var cursor_top = -Math.max(0, margin.top);

        htmlElement.draggable({
            revert: 'valid',
            revertDuration: 0,
            cursor: 'move',
            cursorAt: {top: cursor_top, left: cursor_left},
            distance: 5,
            helper: function () {
                return $$(this).data('editableElement')._dropperHTML();
            },
            start: function (event, ui) {
                // Check we're allowed to move items within the collection
                if (!$$(this).data('editableElement').getHTMLCollection().can('move')) {
                    return false;
                }
                
                $$(this).addClass(EDITABLE_CSS_TMP_PREFIX + 'dragging');
            },
            drag: function (event, ui) {
                $$(this).data('editableElement')._draggableDrag(this, event, 
                    ui);
            },
            stop: function (event, ui) {
                $$(this).data('editableElement')._draggableStop(this, event, 
                    ui);
            }
        });
        
        // Dropping
        htmlElement.droppable({
            greedy: true,
            drop: function (event, ui) {
                $$(this).data('editableElement')._droppableDrop(this, event, 
                    ui);
            },
            over: function (event, ui) {
                $$(this).data('editableElement')._droppableOver(this, event, 
                    ui);
            },
            out: function (event, ui) {
                $$(this).data('editableElement')._droppableOut(this, event, 
                    ui);
            }
        });
    },
    
    /** @private */
    _dropperHTML: function () {
        var templateVars = {
            cls: EDITABLE_CSS_CLASS_PREFIX + this.getType()
        };
        var html = UTILS.template(
            '<div class="<%=cls%> <%=EDITABLE_CSS_TMP_PREFIX%>dropper">' + 
            '<div class="<%=EDITABLE_CSS_TMP_PREFIX%>dropper-content">' + 
            'Rule</div><div class="<%=EDITABLE_CSS_TMP_PREFIX%>' + 
            'dropper-overlay"></div></div>',
            templateVars);
        return html;
    },
    
    /** @private */
    _converters: {},
    
    /** @private */
    _draggers: {},
    
    /** @private */
    _droppers: {},
    
    /** @private */
    _mergers: {},
    
    /** ignore */
    align: undefined,
    
    /** ignore */
    _align: undefined
});

/**
 * Convert a HTML element into a Rule editable element.
 * @param {EDITABLE.Element} parent The parent editable Element.
 * @param {Object} htmlElement The HTML element to convert.
 * @return A Rule editable element.
 * @type EDITABLE.Movie
 */
EDITABLE.Rule.fromHTML = function (parent, htmlElement) {
    var css = $$(htmlElement).attr('class');
    var styles = EDITABLE._getStylesFromCSS(css);
    var options = {
        styles: styles
    };
    if ($$(htmlElement).attr('id') !== '') {
        options['id'] = $$(htmlElement).attr('id');
    }
    return new EDITABLE.Rule(options);
};

// Define which element classes can hold Movie elements
EDITABLE.defineSupportedChildType(EDITABLE.HTMLCollection, 'Rule');

// Define droppers
EDITABLE.defineDropper(EDITABLE.Rule, EDITABLE.Text, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });

EDITABLE.defineDropper(EDITABLE.Text, EDITABLE.Rule, 
    function (element, position) {
        var htmlElement = element.getInnerHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        region[1] = 0;
        EDITABLE.HTMLElement.highlightDropRegion(htmlElement, region);
    },
    function (element, position) {
        var htmlElement = element.getHTMLElement();
        var region = EDITABLE.HTMLElement.getDropRegion(htmlElement, 
            position);
        this.getParent().removeChild(this);
        var insertIndex = element.getParent().indexOfChild(element);
        if (region[0] === 1) {
            insertIndex += 1;
        }
        element.getParent().insertChild(insertIndex, this);
        this.select();
    });