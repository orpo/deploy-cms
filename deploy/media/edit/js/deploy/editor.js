/**
 * @fileoverview    Deploy editor class to implement in-page editors.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


// Constants
var DEPLOY_STYLEABLE_TAG_NAMES = [
	'p', 
	'h1', 
	'h2', 
	'h3', 
	'img', 
	'ul', 
	'ol', 
	'li',
	'rule',
	'table',
	'caption',
	'tbody',
	'thead',
	'tfoot',
	'tr',
	'th',
	'td'
	];


/**
 * There should only ever be one editable environment class instance, this 
 * function creates a singleton way to access it.
 * @return The editable environment.
 * @type {DEPLOY.EditableEnvironment}
 */ 
DEPLOY.getEditableEnvironment = function () {
    if (DEPLOY._editableEnvironment === null) {
        DEPLOY._editableEnvironment = new DEPLOY.EditableEnvironment();
    }
    
    return DEPLOY._editableEnvironment;
}

/* Editable environments */

DEPLOY.EditableEnvironment = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /* @private */
    this._editing = false;
    
    /* @private */
    this._pageController = null;
    
    /* @private */
    this._pageID = null;
	
    /*
    -- Find all the editable documents on the page. --
    
    An editable document is basically determined by editable collections that
    have unique controllers. So a collection without a custom defined 
    controller is assigned to the master controller, if a controller is defined
    then the collection will be assigned to a document for that controller.
    */
    this._docs = [];
    
    /* @private */
    this._selectedDoc = null;
    
    /*
    -- Create a tool window for the editors to populate --
    
    Different collections use different editors to allow users to update their
    content. The editors all share the same tool window and the environment is
    responsible for switching between them.
    */
    this._toolWin = new FLUID.Window({
	    parent: FLUID.widgetByName('desktop'),
        label: _('Editing'), 
        group: 'modeless',
        name: 'deploy.tool-win.in-page-editor',
        position: [100, 100],
        size: [350, 200],
        flags: FLUID.Window.CAN_DRAG |
            FLUID.Window.CAN_MINIMIZE |
			FLUID.Window.HAS_MINIMUM_BORDER
	});
	
	this._pageEditor = DEPLOY.getEditor('empty');
	this._pageEditor.buildUI(this._toolWin, null);
    this._toolWin.prepare();
	this._toolWin.refresh();
	
    /*
    -- Create a style window for CSS classes (styles) --
    
	The applicable CSS classes and the style window are managed by the 
	editable environment.
    */

    this._stylesWin = new FLUID.Window({
	    parent: FLUID.widgetByName('desktop'),
        label: _('Styles'), 
        group: 'modeless',
		minSize: [250, 200],
		maxSize: [250, 1000],
        name: 'deploy.tool-win.in-page-styles',
        position: [100, 311],
        size: [250, 300],
        flags: FLUID.Window.CAN_CLOSE |
            FLUID.Window.CAN_DRAG |
			FLUID.Window.CAN_MINIMIZE |
			FLUID.Window.CAN_RESIZE |
            FLUID.Window.HAS_MINIMUM_BORDER
	});
	
	var tabber = new FLUID.Tabber({
		parent: this._stylesWin
	});
	this._stylesWin.widgetData('tabber', tabber);
	
	/* Ensure the tabber matches the size of the window */
	tabber.style('height', function (tabber) {
		/* Only update the size if the tabber has been rendered */
		if (!tabber.exists()) {
            return;
        }

		/* How tall is the window's client area? */
		var winElm = tabber.getParent().getHTMLSelection();
		var height = winElm.height();
		
        // Allow for title
        height -= winElm.children('.fluid-title').outerHeight(true);
    
        // Allow for padding on client
        height -= (winElm.children('.fluid-client').outerHeight(true) - winElm.children('.fluid-client').height());
    
		return height;
	});

    this._stylesWin.prepare();
	this._stylesWin.refresh();
	
	// Enable/disable and bind events to the application bar buttons
	var user = DEPLOY.getAdministrator();
	var masterCtr = DEPLOY.getController(DEPLOY.getAdministrator().getMasterController() || '');
    var editPageButton = FLUID.widgetByName('deploy.app.btn.edit-page');
    var pagePropertiesButton = FLUID.widgetByName('deploy.app.btn.page-properties');
    var newPageButton = FLUID.widgetByName('deploy.app.btn.new-page');
	var styles;
	var styleSheet;
	var cssLink;
	var cssRules;
	var cssRule;
	var validRuleRegEx;
	var tagName;
	var heads;
	var head;
	var href;
	var i = 0;
	var j = 0;
	
    /* 
    The app bar buttons can only be enabled by the master controller or in the
    case of the 'Edit page' page button, presense of a globally editable
    collection.
    */
    
    if (masterCtr !== undefined && masterCtr.inpage()) {
        
        // Update the page controller value
        this._pageController = masterCtr;
        
        // Does the user have permission to create new pages?
        if (user.can(masterCtr.getName(), 'add')) {
            newPageButton.enable();
            newPageButton.bind('onClick', function (event, data) {
                /*
                Change the buttons so that the user is directed to the page 
                for any newely created record.
                */
                
                data.ctr.unbind('afterAddReady.inpage.deploy');
        		data.ctr.bind('afterAddReady.inpage.deploy', function (event, data) {
                 
                    // Get the a reference to the add window 
                    var win = FLUID.widgetByName('deploy-window-add-' + this.getName());
                    
                    // Hide the existing buttons
                    win.widgetData('buttons').saveNewBtn.hide();
                    win.widgetData('buttons').saveCloseBtn.hide();
                    
                    // Create a 'Save & View' button
                    var saveViewBtn = new FLUID.Button({
		            	parent: win,
						group: 'actions',
		            	label: _('Save & View'),
		        	});
			        saveViewBtn.bind('onClick', function (event, data) { 
			            if (this.enabled()) {
			                data.form.send();
			            }
			        }, {form: win.widgetData('form')});
                    saveViewBtn.refresh();
                    
                    win.widgetData('form').unbind('afterSendSuccess.deploy');
                    win.widgetData('form').unbind('afterSendSuccess.inpage.deploy');
    				win.widgetData('form').bind('afterSendSuccess.inpage.deploy', function (event, data) {
    				    var ctr = data.ctr;
    				    
    				    // Get the URL for the newely created record
    				    var id = event.data.data.record_id;
    				    
    				    // Attempt to get a URL for the record
                		var urlReq = new IO.Request({
                	    	controller: ctr.getName(),
                			method: 'get-url-for-id',
                			data: {id: id}
                        });

                        urlReq.bind('afterSuccess.inpage.deploy', function (event, data) {
        				    var win = data.win;
                            
                            // Forward to the newly created record
                            window.location = event.data.data.url;

        				}, {ctr: data.ctr, win: this});
    				    urlReq.send();
    				    
    				}, {ctr: masterCtr});

        	    });
                
                data.ctr.add();
                
            }, {ctr: masterCtr});
        }
        
        // Get the ID for the page
        var pageIdReq = new IO.Request({
			controller: masterCtr.getName(),
			method: 'get-id-for-url',
			data: {url: DEPLOY.getAdministrator().getURL()}
		});

        pageIdReq.bind('afterFail', function (event, data) {
            // Failed to get an ID for this page so it it is editable then the
            // ID must be held against the individual collections.
        });
        
        pageIdReq.bind('afterSuccess', function (event, data) {
            var env = data.env;
            var ctr = data.ctr;
            var user = data.user;
            var editPageButton = data.editPageButton;
            var pagePropertiesButton = data.pagePropertiesButton;
            
            // Store the page ID
            env._pageID = event.data.data.id;
            
            // Does the user have permission to update page and it's properties?
            if (user.can(masterCtr.getName(), 'update')) {
                
                // Properties
                if (env._pageID !== null) {
                    pagePropertiesButton.enable();
                    pagePropertiesButton.bind('onClick', function (event, data) {
                        data.ctr.update(data.pageID);
                    }, {ctr: ctr, pageID: env._pageID});
                }
            }
            
        }, {
            env: this, 
            ctr: masterCtr,
            user: user, 
            editPageButton: editPageButton, 
            pagePropertiesButton: pagePropertiesButton
        });
        pageIdReq.send();
    }
    
    /* Check for editable collections */
    if (
            // If globally editable
            $$('.edit-global').length !== 0 || (
            
            // ...OR...
            
            // If locally editable
            masterCtr !== undefined &&
            masterCtr.inpage() &&
            $$('.edit-local').length !== 0)) {
        
        // In-page editing
        if ($$('.edit-local').length !== 0) {
            
            // Set-up edit page button
            editPageButton.enable();
            editPageButton.bind('onClick', function (event, data) {
                data.env.edit();
            }, {env: this});
        
            // Set-up accept & cancel buttons
            var acceptButton = FLUID.widgetByName('deploy.app.btn.edit-page-accept');
            var cancelButton = FLUID.widgetByName('deploy.app.btn.edit-page-cancel');
            
            acceptButton.bind('onClick', function (event, data) {
                if (!this.enabled()) {
                    return;
                }
                data.env.save();
            }, {env: this});
            
            cancelButton.bind('onClick', function (event, data) {
                if (!this.enabled()) {
                    return;
                }
                data.env.revert();
            }, {env: this});
        }
    }

	/* Parse any applicable CSS files for in-page editing */
	
	/*
	A map of applicable in-page CSS classes, classes are stored based on their
	tag namespace (e.g. p.my-class -> this._css['p'] = ['my-class', ''']), or 
	if they have no class they get stored against the '*' key and can be used
	against any element. 
	*/
	styles = {
		'*': [],
		'hr': [],
		'video': []
	};
	
	for (i = 0; i < DEPLOY_STYLEABLE_TAG_NAMES.length; i++) {
		tagName = DEPLOY_STYLEABLE_TAG_NAMES[i];
		styles[tagName] = [];
	}

	/* 
	Find the CSS stylesheets applicable to the editor and parse the styles in 
	them. 
	*/
	cssRules = [];
	if(document.styleSheets) {
	    for (i = 0; i < document.styleSheets.length; i++) {
	        styleSheet = document.styleSheets[i];
			cssLink = $$(styleSheet.ownerNode);
			
			/* Only parse CSS marked for use with the editor */
			if (!cssLink.attr('data-deploycms') || 
				cssLink.attr('data-deploycms').toLowerCase() !== 'css') {
				
				continue;
			}
			
			/* If this is IE, get the rules directly... */
	        if(styleSheet.cssText) { 
	            cssRules.push(styleSheet.cssText);
	
			/* ...otherwise get them individually */
	        } else {
	            j = 0;
				do {
	                cssRule = styleSheet.cssRules[j];
	                if (cssRule) {
						cssRules.push(cssRule.cssText);
					}
	                j++;
	            } while (cssRule);
	        }
	    }
	}
	
	/* Parse each rule and store references to those applicable */
	validRuleRegEx = /^(\.[A-Za-z0-9_\-]+|[A-Za-z0-9_\-]+\.[A-Za-z0-9_\-]+|\.edit-class-Rule\.edit-class-Video\.[A-Za-z0-9_\-]+)$/;
	
	for (i = 0; i < cssRules.length; i++) {
		cssRule = cssRules[i];
		
		/* Extract just the rules */
		heads = cssRule.split('{')[0].split(',');
		
		for (j = 0; j < heads.length; j++) {
			head = $$.trim(heads[j]);
			
			/* 
			The rule must be of the form .class, .video.class, or tag.class, 
			all other rules are ignored. 
			*/
			if (validRuleRegEx.exec(head) === null) {
				continue;
			}
			
			/* 
			Determine if the rule is supported. To be supported it must start 
			with a dot '.' (applicable to all elements '*'), or a one of the
			supported editable tags types.
			*/
			
			if (head.indexOf('.edit-class-Rule') === 0) { // Special case rule for rules
				/* Video */
				styles['rule'].push(head.replace('.edit-class-Rule.', ''));
			
			} else if (head.indexOf('.edit-class-Video') === 0) { // Special case rule for videos
				/* Video */
				styles['video'].push(head.replace('.edit-class-Rule.', ''));

			} else if (head[0] === '.') {
				/* All*/
				styles['*'].push(head.replace('.', ''));
				
			} else {
				/* Other */
				
				/* 
				Check to see if the tag matches any of our styleable tags.
				*/
				tagName = head.slice(0, head.indexOf('.'));
				if (DEPLOY_STYLEABLE_TAG_NAMES.indexOf(tagName) !== -1) {
					styles[tagName].push(head.replace(tagName + '.', ''));
				}
			}
		}
	}
	
	/* Initialize the style manager */
	this._stylesMgr = new DEPLOY.StyleMgr({
		tabber: this._stylesWin.widgetData('tabber'),
		styles: styles
		});

	/* 
	Bind to style change event so we can up the document when styles are 
	changed.
	*/
	this._stylesMgr.bind('onStyleChange.deploy', function (ev, data) {
		var selectedDoc = data.env.getSelectedDoc();
		var selectedElm = null;
		
		if (selectedDoc) {
			selectedElm = selectedDoc.getRoot().selectedElement();
		}
		
		if (selectedElm) {
			if (selectedElm.htmlTag() === ev.data.tag) {
				selectedElm.styles(ev.data.styles);
			} else {
				switch (ev.data.tag) {
					case 'ul':
						selectedElm.getParent().styles(ev.data.styles);
						break;
					case 'ol':
						selectedElm.getParent().styles(ev.data.styles);
						break;
					case 'table':
						selectedElm.firstAncestor({isA: 'Table'}).styles(ev.data.styles);
						break;
					case 'thead':
						selectedElm.getGrandParent().styles(ev.data.styles);
						break;
					case 'tbody':
						selectedElm.getGrandParent().styles(ev.data.styles);
						break;
					case 'tfoot':
						selectedElm.getGrandParent().styles(ev.data.styles);
						break;
					case 'tr':
						selectedElm.getParent().styles(ev.data.styles);
						break;				
				}
			}
		}
		
		// Update the properties box
		data.env.getEditor().showProperties();
		
	}, {env: this});
}
UTILS.copyPrototype(DEPLOY.EditableEnvironment, EVENT.EventClass);

$$.extend(DEPLOY.EditableEnvironment.prototype, {
    __className__: 'DEPLOY.EditableEnvironment',
    
    /**
     * Return the tool window for the editable environment.
     * @return The tool window.
     * @type FLUID.Window
     */
    getToolWindow: function () {
        return this._toolWin;
    },
    
    /**
     * Return the style window for the editable environment.
     * @return The style window.
     * @type FLUID.Window
     */
    getStylesWindow: function () {
        return this._stylesWin;
    },
	
    /**
     * Return the style manager for the editable environment.
     * @return The style manager.
     * @type DEPLOY.StyleMgr
     */
    getStylesMgr: function () {
        return this._stylesMgr;
    },

    /**
     * Return the current editor.
     * @return The current editor.
     * @type DEPLOY.Editor
     */
    getEditor: function () {
        return this._pageEditor;
    },
    
    /**
     * Return the currently selected doc.
     * @return the currently selected doc.
     * @type DEPLOY.EditableDocument
     */
    getSelectedDoc: function () {
        return this._selectedDoc;
    },
    
    /**
     * Start editing. 
     */
 	edit: EVENT.classMethod(DEPLOY.EditableEnvironment,  function edit()  {
        
        try {
            this._editing = true;
            
            /* 
            Prevent the editor from accidentally leaving while editing page
            content.
            */
            $$(document).bind('keydown.deploy.editor.prevent-back', this, function (event) {

                /* Prevent back key firing back event */
                if (event.keyCode === 8 || event.keycode === 46) {
                    
                    /* If nothing has focus cancel the event */
                    var focused = $$(':focus');
                    if (focused.length === 0) {
                        event.preventDefault();
                        
                        var selectedDoc = event.data.getSelectedDoc();
                        if (selectedDoc !== null) {
                            var selectedElm = selectedDoc.getRoot().selectedElement();
                            if (selectedElm !== null) {
                                if (
                                    selectedElm.isA('Image') ||
                                    selectedElm.isA('Rule') ||
                                    selectedElm.isA('Video') 
                                ) {
                                    // Trigger the delete tool for the element
                                    DEPLOY.getEditorTool('delete').use(selectedDoc, selectedElm);
                                }
                            }
                        }
                        
                        return;
                    }
                    
                    /*
                    If what has focus is not a text entry element (of some 
                    sort) cancel the event.
                    */
                    if (focused.attr('contenteditable') === undefined) {
                        var tagName = focused[0].tagName.toLowerCase();
                        if (['input', 'textarea', 'select'].indexOf(tagName) === -1) {
                            event.preventDefault();
                            return;
                        }
                    }
                }
            });
            
            /* Disable links during editing */
            $$('a').bind('click.deploy.editor', null, function(event) { event.preventDefault(); });
            
            /* Disable all app bar buttons we know about */
            FLUID.widgetByName('deploy.app.btn.site').enable(false);
            FLUID.widgetByName('deploy.app.btn.plug-ins').enable(false);
			FLUID.widgetByName('deploy.app.btn.me').enable(false);
        	
			/* We don't disable the tools menu on the desktop */
            FLUID.widgetByName('deploy.app.menu.desktop.themes').enable(false);

			/* Enable editing tools options */
            FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').enable(true);
			
            /* Determine what documents need to be created for the page */
            var collections = $$('.edit-local');
            var controllers = {};
            var doc = null;
            var controllerName = null;
            var collection = null;
            var collectionID = null;
            var controller = null;
            var recordID = null;
            var element = null;
            var i = 0;
            var scrollTop = 0;
        
            for (i = 0; i < collections.length; i++) {
                var collection = collections[i];
            
                /* Does the controller have a custom controller? */
                controllerName = $$(collection).attr('data-edit-controller');
                
                /* Default to the page controller */
                if (controllerName === undefined && this._pageController !== null) {
                    controllerName = this._pageController.getName();
                }
                
                /* Check the collection has an ID */
                collectionID = $$(collection).attr('id') || '';
                if ($$.trim(collectionID) === '') {
                    throw new Error('Editable collection is missing ID attribute.');
                }
                
                /* Check for duplicate IDs */
                if ($$('#' + collectionID).length > 1) {
                    throw new Error('Duplicate ID on page for collection \'' + collectionID + '\'.');
                }
                
                /* Check the controller exists */
                if (DEPLOY.getController(controllerName) === undefined) {
                    throw new Error('Controller \'' + controllerName + '\') can not be found for editable collection \'' + collectionID + '\'');
                }
                
                /* Validate the data ID */
                recordID = $$(collection).attr('data-edit-id');
                if (recordID == undefined && this._pageID === null) {
                    throw new Error('No record ID (and no default ID for the page) found for editable collection \'' + collectionID + '\'');
                }
                
                if (controllers[controllerName] === undefined) {
                    controllers[controllerName] = [];
                }
                controllers[controllerName].push(collection);
            }
            
            /* 
            Converting the page so that it's editable removes the page content
            which causes the page to shrink in size potentially loosing the
            original scroll position.
            
            To resolve this 'jump' in page position, we remember and reset the
            scroll position after the page has been made editable.
            */
            scrollTop = $$(window).scrollTop();
            
            /* For each controller create a document */
            for (controllerName in controllers) {
                if (controllers.hasOwnProperty(controllerName)) {
                    collections = controllers[controllerName];
                    doc = new DEPLOY.EditableDocument({
                        controller: DEPLOY.getController(controllerName),
        				id: recordID || this._pageID,
        				collections: collections
                        });
                    
                    /* Handle any change of collection/element within the document */
                    doc.getRoot().bind('afterSelectionChange', function (event, data) {
                        data.env._switchCollection(data.doc);
						data.env.getStylesMgr().updateUI(event.target.selectedElement());
                    }, {env: this, doc: doc});
                    doc.capture();
                    
                    this._docs.push(doc);
                }
            }
            
			/* Bind events for opening & closing the styles editor... */
			
			/* ...from the menu... */
			FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').on();
			FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').unbind('afterToggle.deploy');
			FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').bind('afterToggle.deploy', function (ev, data) {
				var env = data.env;
				if (this.getState()) {
					env.getStylesWindow().open();
				} else {
					env.getStylesWindow().close();
				}
			}, {env: this});
			
			/* ...from the window's title bar. */
			this._stylesWin.bind('onClose.deploy', function (ev, data) {
				FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').off();
			});
			
            /* Update the editor (semi real-time) and styles */
            this._toolWin.getHTMLSelection().everyTime(250, 'deploy-page-editor-update', function(event) {
                DEPLOY.getEditableEnvironment().getEditor().updateUI();
				// @@ Update styles
            });
            
            // @@ Check for global collections -> no plugin for this yet
            
            /* Hide the edit page button and show accept and cancel buttons */
            FLUID.widgetByName('deploy.app.btn.edit-page').hide();
            FLUID.widgetByName('deploy.app.btn.edit-page-accept').show();
            FLUID.widgetByName('deploy.app.btn.edit-page-cancel').show();
            FLUID.widgetByName('deploy.app.btn.edit-page-sep').show();
             
            /* Show the tool and styles windows */
            this._toolWin.open();
			this._stylesWin.minimize(); // @@ For the moment the style window is always initially minimized (until we get preferences working)
            this._stylesWin.open();
            
            /* Select the first editable text element on the page */
            for (i = 0; i < this._docs.length; i++) {
                element = this._docs[i].getFirstTextElement();
                if (element !== null) {
                    /* Select the element */
                    element.select();
                    break;
                }
            }
           
            /* Restore page position */
            $$(window).scrollTop(scrollTop);
            
        } catch (error) {
            this._failGracefully(error);
        }
    }, 'edit'),
    
    /**
     * Stop editing. 
     */
 	shutdown: EVENT.classMethod(DEPLOY.EditableEnvironment,  function shutdown()  {
 	    
        this._editing = false;
        
        // Cancel editor updates
        this._toolWin.getHTMLSelection().stopTime('deploy-page-editor-update');
        
        // Release all documents
        for (var i = 0; i < this._docs.length; i++) {
            var doc = this._docs[i];
            doc.release();
        }
        
        // Clear the existing docs
        this._docs = [];
        
        // Set the editor to empty and close the window
        this._pageEditor.removeUI();
        this._pageEditor = DEPLOY.getEditor('empty');
    	this._pageEditor.buildUI(this._toolWin);
        this._toolWin.close();
        
		// @@ Clear the CSS styles and close the styles window
		this._stylesWin.close();

        // Show the edit page button and hide accept and cancel buttons
        FLUID.widgetByName('deploy.app.btn.edit-page').show();
        FLUID.widgetByName('deploy.app.btn.edit-page-accept').hide();
        FLUID.widgetByName('deploy.app.btn.edit-page-cancel').hide();
        FLUID.widgetByName('deploy.app.btn.edit-page-sep').hide();
        
        // Re-enable the interface
        FLUID.widgetByName('deploy.app.btn.site').enable();
        FLUID.widgetByName('deploy.app.btn.plug-ins').enable();
        FLUID.widgetByName('deploy.app.btn.desktop').enable();
        FLUID.widgetByName('deploy.app.btn.me').enable();
        
		if (DEPLOY.getAdministrator().can('deploy-site', 'get-themes')) {
			FLUID.widgetByName('deploy.app.menu.desktop.themes').enable(true);
		}	
		
		FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').unbind('afterToggle.deploy');
		FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').off();
		FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').enable(false);
        FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.history').enable(false);

        // Allow links again
        $$('a').unbind('click.deploy.editor');
        
        // Allow back button again
         $$(document).unbind('keydown.deploy.editor.prevent-back');
        
    }, 'shutdown'),
    
    /**
     * Save changes to the environment.
     */
    save: EVENT.classMethod(DEPLOY.EditableEnvironment, function save()  {
        try {
            for (var i=0; i < this._docs.length; i++) {
                this._docs[i].save();
            }    
        } catch (error) {
            this._failGracefully(error);
        }
        this.shutdown();
    }, 'save'),
     
    /**
     * Revert changes to the environment.
     */
     revert: EVENT.classMethod(DEPLOY.EditableEnvironment, function revert()  {
         try {
             for (var i=0; i < this._docs.length; i++) {
                 this._docs[i].revert();
             }
         } catch (error) {
             this._failGracefully(error);
         }
         this.shutdown();
     }, 'revert'),

     /* @private */
     _failGracefully: function (errorMsg) {
        
        /*
        If we get a problem when trying to enter the editing mode then we need
        to clean up whatever mess has been created in the process of trying and
        re-enable the interface.
        */
        this._shutdown();
        
        // Throw the exception so the user knows there was a problem
        throw new DEPLOY.Error(errorMsg);
     },
     
    /* @private */
    _switchCollection: function (doc)  {
        
        /*
        -- Collection switching --
        
        Multiple things happen (or at least can happen) when you switch from
        one collection to another. We have to check if the document and/or 
        editor needs to switched.
        */
        
        var editorName = 'default';
        var editor = null;
        var selectedElement = doc.getRoot().selectedElement();
        var prevElm = null;
        var collectionHTML = null;
        var collectionID = null;
        var i = 0;
        
        if (doc !== this._selectedDoc) {
            /*
            Make sure that the other documents don't have any elements selected.
            */
            if (this._selectedDoc !== null) {
                prevElm = this._docs[i].getRoot().selectedElement();
                if (prevElm !== null) {
                    prevElm.unselect();
                }
            }
            
            /*
            Remember the newely selected doc.
            */
            this._selectedDoc = doc;
        }
        
        // Which editor should be used for the newely selected collection
        if (selectedElement !== null) {
            collectionHTML = selectedElement.getHTMLCollection().getHTMLElement();
            collectionID = $$(collectionHTML).attr('id');
            
            if ($$(collectionHTML).attr('data-edit-editor') !== undefined) {
                editorName = $$(collectionHTML).attr('data-edit-editor');
            }
        } else {
            editorName = 'empty';
        }
        
        // Do we need to do anything? (has the editor changed)
        if (editorName === this.getEditor().getName()) {
            return;
        }
        
        // Validate the new editor exists
        var editor = DEPLOY.getEditor(editorName);
        if (editor === undefined) {
            this._failGracefully('Editor \'' + editorName + '\' not found for collection \'' + collectionID + '\'');
        }
        
        /* Switch editor */
        
        // Remove the old editor
        this.getEditor().removeUI();
        
        // Add the new UI
        editor.buildUI(this._toolWin, doc);
        this._pageEditor = editor;
    }
});


/* Editalbe documents*/

DEPLOY.EditableDocument = function (options) {
	EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
	
	/** @private */
	this._controller = options.controller;
	
	/** @private */
	this._id = options.id;
	
	/** @private */
	this._collectionElements = options.collections;
    
	/** @private */
	this._root = new EDITABLE.Root();
	
	/** @private */
	this._collections = []; 
	
	/** @private */
	this._collectionSnapshots = [];
	
	/** @private */
	this._initialState = {};
	
	/** @private */
	this._imageChanges = {
	    'inserted': {},
	    'removed': {},
	    'resized': {}
	};
	
	/** @private */
	this._imageAttachments = {};
	
	/** @private */
	this._io = {};
	
	/*
	Whenever an element is added to the root document, whether as part of
	capturing the HTML elements initially, or as a user action, we have to
	bind events to the new editable elements.
	*/
    this._root.bind('onElementAdded', function (event, data) {
        
        /*
        This inline function recursively applies the paste event to list items
        which due to their tree structure require this approach.
        */
        function applyPasteToList(list, doc) {
            var children = list.getChildren();
            for (i = 0; i < children.length; i++) {   
                var child = children[i];
                child.unbind('onPaste.deploy');
                child.bind('onPaste.deploy', function (ev, data) { data.doc.copyPaste(); }, {doc: doc});
                if (child.getChildren().length > 0) {
                    applyPasteToList(child.getChildren()[0], doc);
                }
            }    
        }
        
        var doc = data.doc;
        var elm = event.data.element;
        var tableSections;
        var tableRows;
        var tableCells;
        var i = 0;
        var j = 0;
        var k = 0;
        
        /*
        Text elements.
        */
        
        // Copy & Paste
        if (elm.typeOf('Text')) {   
            elm.unbind('onPaste.deploy');         
            elm.bind('onPaste.deploy', function (ev, data) { data.doc.copyPaste(); }, {doc: doc});
        }

        if (elm.typeOf('List')) {
            applyPasteToList(elm, doc);
        }
        
        if (elm.typeOf('Table')) {
            var tableSections = elm.getChildren();
            for (i = 0; i < tableSections.length; i++) {
                if (tableSections[i].typeOf('TableSection')) {
                    tableRows = tableSections[i].getChildren();
                    for (j = 0; j < tableRows.length; j++) {
                        tableCells = tableRows[j].getChildren();
                        for (k = 0; k < tableCells.length; k++) {
                            tableCells[k].unbind('onPaste.deploy');
                            tableCells[k].bind('onPaste.deploy', function (ev, data) { data.doc.copyPaste(); }, {doc: doc});
                        }
                    }
                } else if (tableSections[i].typeOf('Caption')) {
                    tableSections[i].unbind('onPaste.deploy');         
                    tableSections[i].bind('onPaste.deploy', function (ev, data) { data.doc.copyPaste(); }, {doc: doc});
                }
            }
        }
        
        /*
        Image elements.
        */
        
        if (elm.isA('Image')) {
        
	        /* Resize */
	    
            /* 
            Remember the original size of the image, this allows us to 
            determine if we need to ask the server to resize an image
            when a user applies their changes.
            */
            
            elm.unbind('afterSizeChange.deploy');
            elm.bind('afterSizeChange.deploy', function (ev, data) {
                
                // Log the image resize event
                data.doc.imageResized(
                    elm.getHTMLCollection().getHTMLElementId(),
                    this.src(), 
                    this.size(), 
                    data.originalSize
                    );
            
            }, {doc: doc, originalSize: elm.size()});
        
			/* Double-click support for meta properties */
            elm.unbind('onProperties.deploy');
            elm.bind('onProperties.deploy', function (ev, data) {
            	DEPLOY.getEditorTool('insert-image').use(data.doc, data.elm);
			}, {doc: doc, elm: elm});
		}
    }, {doc: this});
}
UTILS.copyPrototype(DEPLOY.EditableDocument, EVENT.EventClass);

$$.extend(DEPLOY.EditableDocument.prototype, {
    __className__: 'DEPLOY.EditableDocument',
    
    /**
     * Return the root for the document. 
     * @return The root for the document.
     * @type EDITABLE.Root
     */
    getRoot: function () {
        return this._root;
    },
    
    /**
     * Return the first editable text element in the document.
     * @return First editable text element in the document.
     * @type EDITABLE.Text
     */
    getFirstTextElement: function () {
        var element = null;
        for (var i = 0; i < this._collections.length; i++) {
            var child = this._collections[i].firstChild({typeOf: 'Text'});
            if (child !== undefined) {
                element = child;
                break;
            }
        }
        return element;
    },
    
    /**
     * Return the controller for the document. 
     * @return The controller for the document.
     * @type DEPLOY.Controller
     */
    getController: function () {
        return this._controller;
    },
    
    /**
     * Return the ID of the record to which the collection belongs.
     * @return The ID of the record to which the collection belongs.
     * @type String
     */
    getID: function (collectionID) {
        return $$('#' + collectionID).attr('data-edit-id') || this._id;
    },
    
    /**
     * Return the property content should be stored against server side.
     * @return The property content should be stored against server side.
     * @type String
     */
    getStoreContent: function (collectionID) {
        return $$('#' + collectionID).attr('data-edit-store-content') || 'content';
    },
    
    /**
     * Return the property images should be stored against server side.
     * @return The property images should be stored against server side.
     * @type String
     */
    getStoreImages: function (collectionID) {
        return $$('#' + collectionID).attr('data-edit-store-images') || 'images';
    },
    
    /**
     * Return the view that should be called when syncing the collection.
     * @return The view to call when syncing the collection.
     * @type String
     */
    getView: function (collectionID) {
        return $$('#' + collectionID).attr('data-edit-view') || 'inpage-sync';
    },
    
    /**
     * Catpure the editable collections
     */
    capture: EVENT.classMethod(DEPLOY.EditableDocument,  function capture()  {
        var i = 0;
        var collectionElement = null;
        
        /*
        We store the initial state so that we can revert the document if the
        user cancels their changes.
        */
        this._initialState = {};
        for (i = 0; i < this._collectionElements.length; i++) {
            collectionElement = $$(this._collectionElements[i]);
            this._initialState[collectionElement.attr('id')] = collectionElement.html();
        }

        /*
        Convert the HTML collections to editable collections.
        */
    	for (i = 0; i < this._collectionElements.length; i++) {
    		collectionElement = $$(this._collectionElements[i]);
    		
    		/*
    		To reduce the page layout jumping we set the min height of 
    		elements to a fixed value.
    		*/
    		collectionElement.css('min-height', collectionElement.outerHeight(true, true));
    		
    		// Convert the collections
    		var collection = new EDITABLE.HTMLCollection.fromHTML(
    		    this.getRoot(), // Document root the collection belongs to 
    		    collectionElement[0] // The single (hence [0]) HTML element to convert
    		    );
    		
    		/* Prevent the collection from being made empty */
    		collection.bind('afterRemoveChild', function(event) {
                if(this.getChildCount() === 0) {
                    var emptyText = new EDITABLE.Text();
                    this.appendChild(emptyText);
                }
            });
    		
    		/*
    		Apply any collection rules (these are held against the collection 
    		HTML element as data- attributes).
    		*/
            
            // Rule - Sharing
            if (collectionElement.attr('data-edit-no-share') &&
                collectionElement.attr('data-edit-no-share').toLowerCase() === 'true') {
                collection.can('share', false);
            }
            
            // Rule - Add
            if (collectionElement.attr('data-edit-no-add') && 
                collectionElement.attr('data-edit-no-add').toLowerCase() === 'true') {
                collection.can('add', false);
            }
            
            // Rule - Remove
            if (collectionElement.attr('data-edit-no-remove') &&
                collectionElement.attr('data-edit-no-remove').toLowerCase() === 'true') {
                collection.can('remove', false);
            }
            
            // Rule - Move
            if (collectionElement.attr('data-edit-no-move') &&
                collectionElement.attr('data-edit-no-move').toLowerCase() === 'true') {
                collection.can('move', false);
            }
            
    		/*
    		We store a reference to the collection and a snapshot of it's 
    		contents which is used to determine if the collection has been 
    		changed and therefore needs saving.
    		*/
    		this._collections.push(collection);
    		this._collectionSnapshots.push(collection.toHTML());
    		
    		// Remove fixed min height
    		collectionElement.css('min-height', 0);
    	}
        
    }, 'capture'),

    /**
     * Release the page content held by the editable document.
     */
    release: EVENT.classMethod(DEPLOY.EditableDocument,  function release()  {

        // Clear any element selected within the document
        this.getRoot().clearSelectedElement();
        TEXT_SELECTION.clear();

    }, 'release'),
    
    /**
     * Save the editable document.
     */
    save: EVENT.classMethod(DEPLOY.EditableDocument,  function save()  {
        
        /*
        First we determine which collections in the document have changed so
        that we don't make any unnecessary save requests.
        
        To save changed collections we have to build a content map. A content
        map is a dictionary where values are keyed either by their store 
        property of their ID (depending on whether or not a store content
        value is available).
        
        If a collection maps directly to a field then a store content property
        will be used. If changes are applied to a HTML template held by the
        record (as is the case with pages) then the collection ID's are used
        as keys so that the changed content can be applied.
        
        The fullPageSave is used to indicates the type of content map, a 
        mixture of content map types is not supported in the same document.
        */
        
        var modifiedCollections = {};
    	var fullPageSave = true;
    	var collection = null;
    	var collectionID = null;
    	var child = null;
    	var empty = false;
    	var html = null;
    	var htmlSnapshot = null;
    	var contentMap = null;
    	var images = null;
    	var image = null;
    	var saveReq = null;
    	var imageResizeReq = null;
    	var imageRemoveReq = null;
    	var firstSave = true;
    	var removes = null;
    	var resizes = null;
    	var metaUpdates = null;
    	var src = null;
    	var i = 0;
    	var changed = false;
    	var currentAttachment = null;
    	var originalAttachment = null;
    	
    	for (var i = 0; i < this._collections.length; i++) {
    	
    	    /*
    	    To determine if a collection is changed we compare it's static 
    	    HTML output with the snapshot we took when the collection was
    	    originally captured. We also check for hidden changes such as
    	    changes to the meta data of images.
    	    */
    	    
    		collection = this._collections[i];
    		collectionID = collection.getHTMLElementId();
    		
    		html = collection.toHTML();
    		htmlSnapshot = this._collectionSnapshots[i];
    		
    		/* Check for single empty paragraph case */
    		empty = false;
    		if (collection.getChildCount() === 1) {
		        child = collection.getChildren()[0];
		        if (child.typeOf('Text') && child.isEmpty()) {
	                empty = true;
	            }
		    }
    	    
    	    changed = html.length !== htmlSnapshot.length || html !== htmlSnapshot;
    	
    	    /*
    	    Check if any hidden image meta has been modified.
    	    */
            if (!changed && this._imageAttachments.hasOwnProperty(collectionID)) {
                for (src in this._imageAttachments[collectionID]) {
                    if (this._imageAttachments[collectionID].hasOwnProperty(src)) {
                        currentAttachment = this._imageAttachments[collectionID][src].current;
                        originalAttachment = this._imageAttachments[collectionID][src].original;
                        if (!currentAttachment.equalTo(originalAttachment)) {
                            changed = true;
                            break;
                        }
                    }
                }
            }
            
    		// Has the collections content changed
    		if (changed) {
    		    if (modifiedCollections[collectionID] === undefined) {
		            modifiedCollections[collectionID] = {};
		        }
    		    
    		    // Select the elements key
    		    var key = $$(collection.getHTMLElement()).attr('data-edit-store-content');
    		    if (key === undefined) {
    		        if (fullPageSave) {
    		            key = collection.getHTMLElementId();
    		        } else {
		                // If we find a mixture of content map types throw an error
		                throw new Error('Mixed content map types, you must either indicate which field each collection is saved against, or indicate none for a full page save.');
		            }
    		    }
    		    
    		    // Make sure the same ID hasn't been specified twice for the 
    		    // same collection ID (only possible with property content 
    		    // saves not full page saves).
    		    if (modifiedCollections[collectionID][key] !== undefined) {
		            throw new Error('The data-store-content attribute value \'' + key + '\' has already been applied for another collection with the same ID.');
		        }
    		    
    		    // Set the HTML for storage
    		    if (empty) {
    			    modifiedCollections[collectionID][key] = '';
    			} else {
        			modifiedCollections[collectionID][key] = $$.trim(html);
			    }
			    
    			// Update the snapshot to the current HTML
    			this._collectionSnapshots[i] = html;
    		}
    		
            // Remove editable HTML
            collection.clearHTML();

            // Insert the static HTML
            $$(collection.getHTMLElement()).html(collection.toHTML());
    	}

        /*
        Ask the server to save our content changes.
        
        As we support for updating multiple server-side records via a single
        document we potentially have to fire multiple save requests.
        */
        
        function _recursiveSave(doc, saveStack) {
            if (saveStack.length > 0) {
                /* Bind the next save*/
                var nextStack = saveStack.slice();
                nextStack.shift();
                saveStack[0].bind('afterSuccess', function (event, data) {
                    _recursiveSave(data.doc, data.saveStack);
                }, {'doc': doc, 'saveStack': nextStack});
                
                /* Call the save */
                saveStack[0].send();
            } else {
                /* Clear image change log for add/remove/resize */
                doc._imageChanges['inserted'] = {};
                doc._imageChanges['removed'] = {};
                doc._imageChanges['resize'] = {};
                doc._imageAttachments = {};
            }
        }
        
        var saveStack = [];
        for (collectionID in modifiedCollections) {
            if (modifiedCollections.hasOwnProperty(collectionID)) {
                contentMap = modifiedCollections[collectionID];
                
                saveReq = new IO.Request({
                    controller: this.getController(collectionID).getName(),
                    method: this.getView(collectionID),
            		type: 'POST',
                    data: {
            			id: this.getID(collectionID),
            			content_map: JSON.stringify(contentMap)
            		}
                });
                
                /* Handle image changes after the content has saved */
                removes = [];
                resizes = [];
                metaUpdates = [];
                
                /* Removes */
                if (this._imageChanges['removed'].hasOwnProperty(collectionID)) {
                    images = this._imageChanges['removed'][collectionID];
                    for (i = 0; i < images.length; i++) {
                        image = images[i];

                        // Request that the image is removed
                        var imageRemoveReq = new IO.Request({
                	    	controller: this.getController().getName(),
                			method: 'inpage-remove-image',
                		    type: 'POST',
                			data: {
                			    id: this.getID(collectionID),
                			    store_images: this.getStoreImages(collectionID),
                			    images: image
                			    }
                		});
                		
                		removes.push(imageRemoveReq);
                    }
                }
                
                /* Resizes */
                if (this._imageChanges['resized'].hasOwnProperty(collectionID)) {
                    for (src in this._imageChanges['resized'][collectionID]) {
                        if (this._imageChanges['resized'][collectionID].hasOwnProperty(src)) {
            	            var image = this._imageChanges['resized'][collectionID][src];

                            /*
                            It's possible that the image has been resized but 
                            that at the final resize has modified the image to
                            be the same size in which case we don't need to
                            request a resize.
                            */

            	            var size = image[0].join('|');
            	            var originalSize = image[1].join('|');
            	            if (size === originalSize) {
            	                // Image has been resized back to original 
            	                // size, skip the resize.
            	                continue;
            	            }

                            /*
                            The image may also have been removed in which case 
                            there is no need to apply the update. 
                            */
                            if (this._imageChanges['removed'][collectionID]) {
                                if (this._imageChanges['removed'][collectionID].indexOf(src) > -1) {
                                    continue;
                                }
                            }
                            
                            var imageResizeReq = new IO.Request({
                    	    	controller: this.getController().getName(),
                    			method: 'inpage-resize-image',
                    		    type: 'POST',
                    			data: {
                    			    id: this.getID(collectionID),
                    			    store_images: this.getStoreImages(collectionID),
                    			    images: src,
                    			    width: image[0][0], 
                    			    height: image[0][1]
                    			    }
                    		});
                    		
                    		resizes.push(imageResizeReq);
                        }
                    }
                }
                
                /* Meta updates */
                if (this._imageAttachments.hasOwnProperty(collectionID)) {
                    for (src in this._imageAttachments[collectionID]) {
                        if (this._imageAttachments[collectionID].hasOwnProperty(src)) {
                            
                            /*
                            The image may also have been removed in which case 
                            there is no need to apply the update. 
                            */
                            if (this._imageChanges['removed'][collectionID]) {
                                if (this._imageChanges['removed'][collectionID].indexOf(src) > -1) {
                                    continue;
                                }
                            }
                            
                            /* Only update the meta if it has changed */
                            currentAttachment = this._imageAttachments[collectionID][src].current;
                            originalAttachment = this._imageAttachments[collectionID][src].original;
                            if (!currentAttachment.equalTo(originalAttachment)) {
                                
                                var imageMetaUpdateReq = new IO.Request({
                        	    	controller: this.getController().getName(),
                        			method: 'inpage-update-image',
                        		    type: 'POST',
                        			data: {
                        			    id: this.getID(collectionID),
                        			    store_images: this.getStoreImages(collectionID),
                        			    images: src,
                        			    meta: JSON.stringify(currentAttachment._meta)
                        			    }
                        		});

                        		metaUpdates.push(imageMetaUpdateReq);
                                
                            }
                        }
                    }
                }
                
                saveReq.bind('afterSuccess', function (event, data) {
                    var i = 0;
                    
                    /* Apply image resizes */
                    for (i = 0; i < data.resizes.length; i++) {
                        data.resizes[i].send();
                    }
                    
                    /* Apply image meta updates */
                    for (i = 0; i < data.metaUpdates.length; i++) {
                        data.metaUpdates[i].send();
                    }
                    
                    /* Apply image removes */
                    for (i = 0; i < data.removes.length; i++) {
                        data.removes[i].send();
                    }
                    
                }, {
                    doc: this, 
                    collectionID: collectionID, 
                    removes: removes, 
                    resizes: resizes,
                    metaUpdates: metaUpdates
                });
                
                /* Notify the user of the problem */
                saveReq.bind('afterFail', function (event, data) {
                    
                    /* Revert the contents of that collection */
                    if (data.doc._initialState.hasOwnProperty(data.collectionID)) {
                        $$('#' + data.collectionID).html(data.doc._initialState[data.collectionID]);
                    }
                    
                    /* Notify the user that there was a problem */
                    FLUID.widgetByName('desktop').notify('Save failed on ' + event.data.data.field + '. ' + event.data.data.msg);
                    
                }, {doc: this, collectionID: collectionID});
                
                /* Fire save request */
                saveStack.push(saveReq);
            }
        }
        
        _recursiveSave(this, saveStack);
        
    }, 'save'),
    
    /**
     * Rever changes to the editable document.
     */
    revert: EVENT.classMethod(DEPLOY.EditableDocument,  function revert()  {
        
        var collectionID = null;
        var images = null;
        var i = 0;

        /*
        If the user has inserted images during his latest editing session then
        we must remove those images from the server as part of the revert 
        process.
        */
        
        for (collectionID in this._imageChanges['inserted']) {
            if (this._imageChanges['inserted'].hasOwnProperty(collectionID)) {
                images = this._imageChanges['inserted'][collectionID];
                for (i = 0; i < images.length; i++) {

                    // Request that the image is removed
                    var imageRemoveReq = new IO.Request({
            	    	controller: this.getController().getName(),
            			method: 'inpage-remove-image',
            		    type: 'POST',
            			data: {
            			    id: this.getID(collectionID),
            			    store_images: this.getStoreImages(collectionID),
            			    images: images[i]
            			    }
            		});
                    imageRemoveReq.send();
                    
                }
            }
        }

        // Clear image change log for add/remove
        this._imageChanges['inserted'] = {};
        this._imageChanges['removed'] = {};
        
        /*
        Reset the document HTML to it's original state.
        */
        for (collectionID in this._initialState) {
            if (this._initialState.hasOwnProperty(collectionID)) {
                $$('#' + collectionID).html(this._initialState[collectionID]);
            }
        }

    }, 'revert'),    
    
    /*
    Support for copy & paste events.
    
    Copy & paste cannot be safely supported into edtiable content as the user
    may copy from an application which generates it's own HTML tags (which) 
    maybe unsuitable.

    As a security precaution most of the mainstream browsers don't allow you
    to access the user's clipboard contents. So instead we intercept paste
    events and provide the user with a textarea they can safely paste into.
    
    We then parse the text they've pasted in for line breaks and generate a
    new set of paragraphs of list items based on the content.
    */
    copyPaste: function () {
        var elm = this.getRoot().selectedElement();
        var sel = elm.textSelection();
        
        /*
        Enter modal mode and show the user a copy & paste form.
        */
        FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);
        
        var win = FLUID.widgetByName('deploy-window-copy-paste');
        
    	/*
    	Check if the window is already open, if it is just bring it too the
    	front.
    	*/
    	if (win !== undefined) {
    		FLUID.widgetByName('deploy-window-copy-paste').minimize(false);
    		FLUID.widgetByName('deploy-window-copy-paste').sendToFront();
    		return;
    	}
        
        /*
        No existing window open so create a new window and copy & paste form.
        */
        win = new FLUID.Window({
            name: 'deploy-window-copy-paste',
            group: 'modal',
            label: _('Copy & Paste'),
            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
            flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED | FLUID.Window.HAS_ACTIONS
            });

        //
        win.bind('afterCloseComplete', function(event, data) {
            this.destroy();
            
            /*
            If the user pasted content into the collection then the paste
            process will have already selected the last element generated by
            the paste action.
            
            If the user changed their mind and didn't paste anything then we
            reselect the initial element the paste was performed against.
            */
            if (win.widgetData('pasted') !== true) {
                data.elm.select();
                data.elm.textSelection(data.sel);
            }

            FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);  
        }, {doc: this, elm: elm, sel: sel});

        /*
        Create the copy & paste form elements.
        */
        var cpForm = new FLUID.Form({parent: win});
        var cpFieldset = new FLUID.Fieldset({parent: cpForm});
        var pasteField = new FLUID.Field.Textarea({
            parent: cpFieldset, 
            name: 'content',
            label: _('Content')
        });
    	var pasteBtn = new FLUID.Button({
        	parent: win,
    		group: 'actions',
        	label: _('Paste'),
        	classes: ['fluid-sep']
    	});
        
        // When the window opens we focus on the textarea the user can paste 
        // their content into without having to use the mouse or tab key to
        // select the field.
        win.bind('onOpenComplete', function(event, data) {
            data.pasteField.focus();
        }, {pasteField: pasteField});

        /*
        Handle pasting content into the document.
        */
        pasteBtn.bind('onClick', function (event, data) {
            data.cpForm.puesdoSend();
        }, {cpForm: cpForm});
        
        cpForm.bind('afterSendSuccess', function (event, data) {
            var win = data.win;
            var elm = data.elm;
            var parentElm = elm.getParent();
            var sel = data.sel;
            var content = $$.trim(this.getData()['content']);
            var cursorPos = 0;
            var soups = null;
            var soup = null;
            var lines = null;
            var line = null;
            var textElm = null;
            var insertElmClass = null;
            var insertIndex = 0;
            var i = 0;
            
            // Did the user submit any pasted text?
            if (content === '') {
                // No so just close the window
                win.close();
                return;
            }
            
            // Make the content safe
            content = UTILS.makeTextSafe(content);
            
            /*
            If the user only pastes in one line of text, or if the element 
            they are pasting into doesn't support for accepting a multiple 
            lines we replace the selected content with the pasted content and
            ignore the lines.
            */
            lines = content.split('\n');
            
            if (lines.length == 1 || elm.isA('Caption') || elm.isA('TableCell') || !elm.getHTMLCollection().can('add')) {
                
                /* Single line */
                
                // Split the soup around the current selection
                soups = elm.htmlSoup().split(sel);
                soup = soups[0];
                
                // Paste the new content into the left-hand soup and reappend 
                // the right-hand soup.
                soup.append(content); 
                cursorPos = soup.getLength();
                soup.paste(soup.getLength(), soups[1]);
                
                // Update the element with the new soup
                elm.htmlSoup(soup);
                elm.refreshHTML();
                
                // Move the cursor the end of the pasted content within the 
                // element.
                elm.select();
                elm.textSelection([cursorPos, cursorPos], false);
            
            } else {
                
                /* Multiple lines */
                
                // Which element type does each line in the pasted text
                // represent.
                insertElmClass = EDITABLE.Text;
                if (elm.isA('ListItem')) {
                    insertElmClass = EDITABLE.ListItem;
                }
                
                // At what point in the document are we inserting the new 
                // content?
                insertIndex = parentElm.indexOfChild(elm) + 1;
                
                /*
                Handle splitting the element down at the insertion point. The
                most efficent way to do this currently is to trigger a return
                event on the element. However it the element is completely 
                selected then we need to remove it first.
                */
                if (sel[0] === 0 && sel[1] === elm.htmlSoup().getLength()) {
                    // Remove element being replaced
                    parentElm.removeChild(elm);
                    insertIndex -= 1;
                } else {
                    // Fake a return key input
                    elm.split(sel);
                }
                
                for (i = 0; i < lines.length; i++) {
                    line = $$.trim(lines[i]);
                    
                    // Skip empty lines
                    if (line === '') {
                        continue;
                    }
                    
                    // Escape any potentially illegal characters
                    line = IXMO.Parser.escapeEntity(line);
                    
                    // Create an editable element from the line
                    textElm = new insertElmClass({content: line});
                    parentElm.insertChild(insertIndex, textElm);
                    
                    // Update the insert position
                    insertIndex++;
                }
                
                // Select the last element we inserted and place the cursor at
                // the end.
                textElm.select();
                cursorPos = textElm.htmlSoup().getLength();
                textElm.textSelection([cursorPos, cursorPos], false);
            }
            
            // Done, close the window
            win.widgetData('pasted', true); // Remember that a paste was performed (see close window event)
            data.win.close();
            
        }, {win: win, elm: elm, sel: sel});
        
        // Open the window
    	win.refresh();
    	win.prepare();
    	win.open();
    	
    },
    
    /*
    The following functions are used to handle image changes within the
    document. We have to log image changes so that we can take action to
    clean up changes should the user revert the document changes 
    */
    
    imageRemoved: function (collectionID, src) {
        if (this._imageChanges['removed'][collectionID] === undefined) {
            this._imageChanges['removed'][collectionID] = [];
        }
        
        /* If this image has been marked for attachment changes unmark it */
        this.removeImageAttachment(collectionID, src);
        
        this._imageChanges['removed'][collectionID].push(src);
    },
    
    imageInserted: function (collectionID, src) {
        if (this._imageChanges['inserted'][collectionID] === undefined) {
            this._imageChanges['inserted'][collectionID] = [];
        }
        this._imageChanges['inserted'][collectionID].push(src);
    },

    imageResized: function (collectionID, src, size, original_size) {
        if (this._imageChanges['resized'][collectionID] === undefined) {
            this._imageChanges['resized'][collectionID] = {};
        }
        this._imageChanges['resized'][collectionID][src] =  [size, original_size];
    },
    
    /*
    The following functions are used to handle meta changes within the 
    document. We have to log changes to image meta so that they can either be
    applied or reverted.
    */
    addImageAttachment: function (collectionID, src, attachment) {
        if (this._imageAttachments[collectionID] === undefined) {
            this._imageAttachments[collectionID] = {};
        }
        this._imageAttachments[collectionID][src] = {
            'current': attachment, 
            'original': attachment.copy()
            };
    },
    
    removeImageAttachment: function (collectionID, src) {
        if (this._imageAttachments[collectionID] === undefined) {
            return;
        }
        if (this._imageAttachments[collectionID][src] === undefined) {
            return;
        }
        delete this._imageAttachments[collectionID][src];
    },
    
    getImageAttachment: function (collectionID, src) {
        if (this._imageAttachments[collectionID] === undefined) {
            return null;
        }
        if (this._imageAttachments[collectionID][src] === undefined) {
            return null;
        }
        return this._imageAttachments[collectionID][src];
    }
});


/* Style manager */

/**
 * A style manager class to support setting CSS styles for editable elements.
 * @class A style manager class.
 * @constructor
 */
DEPLOY.StyleMgr = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
	/** @private */
	this._styles = options.styles;
	
	/** @private */
	this._tabber = options.tabber;
	
	/** @private */
	this._tabs = {};
	this._tab_groups = {};

	/* Pre-build the tabs and panels */
	this._buildUI();
}
UTILS.copyPrototype(DEPLOY.StyleMgr, EVENT.EventClass);

$$.extend(DEPLOY.StyleMgr.prototype, {
    __className__: 'DEPLOY.StyleMgr',

    /**
     * Placeholder function that triggers the styleChange event which can then
	 * be captured and used by other elements.
     */
    styleChange: function (tag, styles) {
		this.trigger('styleChange', function () {

        }, {data: {'tag': tag, 'styles': styles}});
	},

	/**
	 * Update the UI based on the specified element.
	 * @param {EDITABLE.Element} elm The editable element.
	 */
	updateUI: function (elm) {
		var i = 0;
		var sectionTag;
		var selectedTab;
		var tag;
		var tab;
		var tabIndex;
		var tabCount = this._tabber.getTabCount();
		
		/* Unselect the current tab (if one is selected) */
		selectedTab = this._tabber.getTab(this._tabber.selectedTab());
		if (selectedTab !== undefined) {
			selectedTab.unselect();
		}
		
		/* Hide all tabs */ 
		for (i = 0; i < tabCount; i++) {
			this._tabber.getTab(i).show(false);
		}
		
		if (elm === undefined || elm === null) {
			return;
		}
		tag = elm.htmlTag();
		
		/* Show the relevant tabs for the element */
		switch (tag) {
			
			/* Special cases */
			case 'li':
				if (elm.getParent().htmlTag() === 'ul') {
				
					/* Update the value of the ul tab */
					tabIndex = this._tabs['ul'];
					tab = this._tabber.getTab(tabIndex);
					this._selectClasses(tab.widgetData('field'), elm.getParent());
				
					/* Show and select the tag */
					this._tabber.getTab(this._tabs['ul']).show(true);
					this._tabber.selectTab(this._tabs['ul']);
					
				} else {
					
					/* Update the value of the ol tab */
					tabIndex = this._tabs['ol'];
					tab = this._tabber.getTab(tabIndex);
					this._selectClasses(tab.widgetData('field'), elm.getParent());
					
					/* Show and select the tag */
					this._tabber.getTab(this._tabs['ol']).show(true);
					this._tabber.selectTab(this._tabs['ol']);
				}
				
				/* Update the value of the li tab */
				tabIndex = this._tabs['li'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm);

				/* Show the li tag */
				this._tabber.getTab(this._tabs['li']).show(true);
				break;
			
			case 'th':
				
				/* Update the value of the ul tab */
				tabIndex = this._tabs['table'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm.firstAncestor({isA: 'Table'}));
				
				/* Show and selet the table tab*/
				this._tabber.getTab(this._tabs['table']).show(true);
				this._tabber.selectTab(this._tabs['table']);
				
				/* Update the value of the table section tab */
				sectionTag = elm.getParent().getParent().htmlTag();
				tabIndex = this._tabs[sectionTag];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm.firstAncestor({isA: 'TableSection'}));
				
				/* Show the table section tab */
				this._tabber.getTab(this._tabs[sectionTag]).show(true);
				
				/* Update the value of the tr tab */
				tabIndex = this._tabs['tr'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm.getParent());
				
				/* Show the tr tab */				
				this._tabber.getTab(this._tabs['tr']).show(true);
				
				/* Update the value of the tr tab */
				tabIndex = this._tabs['th'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm);
				
				/* Show the th tab */
				this._tabber.getTab(this._tabs['th']).show(true);
				break;
			
			case 'td':
			
				/* Update the value of the ul tab */
				tabIndex = this._tabs['table'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm.firstAncestor({isA: 'Table'}));
			
				/* Show and selet the table tab*/
				this._tabber.getTab(this._tabs['table']).show(true);
				this._tabber.selectTab(this._tabs['table']);
			
				/* Update the value of the table section tab */
				sectionTag = elm.getParent().getParent().htmlTag();
				tabIndex = this._tabs[sectionTag];
				tab = this._tabber.getTab(tabIndex);
				
				this._selectClasses(tab.widgetData('field'), elm.firstAncestor({isA: 'TableSection'}));
			
				/* Show the table section tab */
				this._tabber.getTab(this._tabs[sectionTag]).show(true);
			
				/* Update the value of the tr tab */
				tabIndex = this._tabs['tr'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm.getParent());
			
				/* Show the tr tab */				
				this._tabber.getTab(this._tabs['tr']).show(true);
			
				/* Update the value of the tr tab */
				tabIndex = this._tabs['th'];
				tab = this._tabber.getTab(tabIndex);
				this._selectClasses(tab.widgetData('field'), elm);
			
				/* Show the td tab */
				this._tabber.getTab(this._tabs['td']).show(true);
				break;
			
			/* Special case for rules */
			case 'hr':
				tabIndex = this._tabs['rule'];
				tab = this._tabber.getTab(tabIndex);
				
				/* Update the value of active tabs */
				this._selectClasses(tab.widgetData('field'), elm);

				/* Show and select the tab */
				tab.show(true);
				this._tabber.selectTab(tabIndex);
				break;
				
			/* Special case for videos */
			case 'a':
				tabIndex = this._tabs['video'];
				tab = this._tabber.getTab(tabIndex);
			
				/* Update the value of active tabs */
				this._selectClasses(tab.widgetData('field'), elm);

				/* Show and select the tab */
				tab.show(true);
				this._tabber.selectTab(tabIndex);
				break;
				
			/* One tab cases */
			default:
				tabIndex = this._tabs[tag];
				tab = this._tabber.getTab(tabIndex);
				
				/* Update the value of active tabs */
				this._selectClasses(tab.widgetData('field'), elm);

				/* Show and select the tab */
				tab.show(true);
				this._tabber.selectTab(tabIndex);
		}		
	},

	/** @private */
	_buildUI: function () {
		var i = 0;
		var form;
		var fieldset;
		var field;
		var tab;
		var panel;
		var style;
		var tag;
		
		/* Build the tabs */
		this._tabs['p'] = this._tabber.appendTab('Paragraph');
		this._tabs['h1'] = this._tabber.appendTab('Heading 1');
		this._tabs['h2'] = this._tabber.appendTab('Heading 2');
		this._tabs['h3'] = this._tabber.appendTab('Heading 3');
		this._tabs['img'] = this._tabber.appendTab('Image');
		this._tabs['rule'] = this._tabber.appendTab('Rule');
		this._tabs['ul'] = this._tabber.appendTab('List');
		this._tabs['ol'] = this._tabber.appendTab('List');
		this._tabs['li'] = this._tabber.appendTab('List item');
		this._tabs['table'] = this._tabber.appendTab('Table');
		this._tabs['caption'] = this._tabber.appendTab('Caption');
		this._tabs['thead'] = this._tabber.appendTab('THead');
		this._tabs['tbody'] = this._tabber.appendTab('TBody');
		this._tabs['tfoot'] = this._tabber.appendTab('TFoot');
		this._tabs['tr'] = this._tabber.appendTab('Row');
		this._tabs['th'] = this._tabber.appendTab('Cell');
		this._tabs['td'] = this._tabber.appendTab('Cell');
		this._tabs['video'] = this._tabber.appendTab('Video'); 
		this._tabber.refresh();
		
		/* Build the pannels for each tab */
		for (tag in this._tabs) {
			if (this._tabs.hasOwnProperty(tag)) {
				tab = this._tabber.getTab(this._tabs[tag]);
				panel = this._tabber.getTabPanel(this._tabs[tag]);
				
				/* Hide all the tabs */
				tab.show(false);
				
				/* Turn of the minimum width for the tabs */
				tab.style('min-width', 0);
				
				/*
				If there are no applicable styles disable the tab (global or 
				specific to the tag). 
				*/
				if (this._styles[tag].length === 0 && this._styles['*'].length === 0) {
					tab.enable(false);
					continue;
				}
				
				/* Create a form for the panel */
				form = new FLUID.Form({parent: panel});
				fieldset = new FLUID.Fieldset({parent: form});
				
				/* Add a select field for the applicable styles */
				field = new FLUID.Field.Choice({
					parent: fieldset,
					name: 'style',
					label: _(''),
					multiple: true,
					classes: ['fluid-no-label']
					});
				
				/* Global styles */
				for (i = 0; i < this._styles['*'].length; i++) {
					style = this._styles['*'][i];
					new FLUID.FieldOption.Checkbox({
						parent: field,
						label: style,
						value: style
					});
				}
				
				/* Tag styles */
				for (i = 0; i < this._styles[tag].length; i++) {
					style = this._styles[tag][i];
					new FLUID.FieldOption.Checkbox({
						parent: field,
						label: style,
						value: style
					});
				}
				
				/* Sort the options */
				field.sort();
				
				/*
				Associate the tag with the field and the field with the tab 
				for easy lookup later.
				*/
				field.widgetData('tag', tag);
				tab.widgetData('field', field);
				
				/*
				Trigger an event against the style manager when the fields 
				value changes.
				*/
				field.bind('afterSnapshot.deploy', function (ev, data) {
					var styles = this.value();
					if (UTILS.isString(styles)) {
						styles = [styles];
					}
					data.mgr.styleChange(this.widgetData('tag'), styles);
				}, {mgr: this});
				
				form.refresh();
			}
		}
	},
	
	/** @private */
	_selectClasses: function (field, elm) {
		if (!field) {
			return;
		}
		
		var i = 0;
		var options = field.getFieldOptions();
		var option;
		
		/* Select any valid options */
		for (i = 0; i < options.length; i++) {
			option = options[i];
			option.select(elm.hasClass(option.value()));
		}
	}
});


/* Page editors */

/**
 * Register an editor.
 * @param {DEPLOY.Editor} editor The editor to register.
 */ 
DEPLOY.registerEditor = function (editor) {
    DEPLOY._editors[editor.getName().toLowerCase()] = editor;
}

/**
 * Return the registered editor associated with the specified name.
 * @param {String} name The name of the editor being requested.
 * @return A registered editor.
 * @type DEPLOY.Editor
 */ 
DEPLOY.getEditor = function (name) {
    return DEPLOY._editors[name.toLowerCase()];
}

/**
 * An in-page editor class to support in-page editing.
 * @class An in-page editor class.
 * @constructor
 */
DEPLOY.Editor = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._name = options.name;
    
    /** @private */
    this._editableDocument = null;
    
    /** @private */
    this._toolGroups = options.toolGroups;
    
    /** @private */
    this._tabber = null;
    
    /** @private */
    this._toolButtons = [];
    
    /** @private */
    this._flags = null;
    
    // Register the editor
    DEPLOY.registerEditor(this);
}
UTILS.copyPrototype(DEPLOY.Editor, EVENT.EventClass);

$$.extend(DEPLOY.Editor.prototype, {
    __className__: 'DEPLOY.Editor',

    /**
     * Return the name of the editor.
     * @return The editor's name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    /**
     * Return the name of the editor.
     * @return The editor's name.
     * @type String
     */
    getEditableDocument: function () {
        return this._editableDocument;
    },

    /**
     * Prepare the editor and document
	 * @param {DEPLOY.EditableDocument} editableDocument The editable document
	 * being edited.
     */
	prepare: EVENT.classMethod(DEPLOY.Editor, 
        function prepare(editableDocument) {
			var localArgs = EVENT.getArgs(arguments);
            var editableDocument = EVENT.getArg(localArgs, 0);
            this._editableDocument = editableDocument;
        }, 'prepare'),
    
    /**
     * Update the editor's UI elements.
     */
    buildUI: EVENT.classMethod(DEPLOY.Editor, 
        function buildUI(win, doc) {
            var localArgs = EVENT.getArgs(arguments);
            var win = EVENT.getArg(localArgs, 0);
            var doc = EVENT.getArg(localArgs, 1);
            var i = 0;
            var j = 0;
            
            /* @@ NEED TO TIDY EDITABLE DOCS UP*/
            this._editableDocument = doc;
            
            // Clear out existing tab widgets
            if (this._tabber !== null) {
                this._tabber.destroy();
            }
            this._tabber = new FLUID.Tabber();
            win.appendChild(this._tabber);
            this._tabber.style('height', 168)
            
            // Build new tab widgets
            for (i = 0; i < this._toolGroups.length; i++) {
                var toolGroup = this._toolGroups[i];
                
                // Create the tab
                this._tabber.appendTab(toolGroup[0]);
                var tabPanel = this._tabber.getTabPanel(i);
                
                for (j = 0; j < toolGroup[1].length; j++) {
                    var tool = toolGroup[1][j];
                    if (tool === '|') {
                        new FLUID.Separator({parent: tabPanel});             
                    } else if (tool === '-') {
                        new FLUID.Separator({parent: tabPanel, classes: ['fluid-vertical']});
                    } else {
                        var toolBtn = DEPLOY.getEditorTool(tool).buildButton(this);
                        this._toolButtons.push(toolBtn);
                        toolBtn.addClass('fluid-sep');
                        tabPanel.appendChild(toolBtn);
                    }
                }
            }        

            // Add a tab for properties (@@ perhaps make properties an optional tab?)
            this._tabber.appendTab('Properties');
            this._propertiesPanel = this._tabber.getTabPanel(this._toolGroups.length);
            
            this._tabber.getTab(i).bind('onClick', function (event, data) { 
                data.editor.showProperties();
            }, {editor: this});
            
            // Ensure selecting tabs doesn't remove the focus from the elements
            for (i = 0; i < this._tabber.getTabCount(); i++) {
                this._tabber.getTab(i).bind('onMouseDown', function (event, data) { 
                    event.data.fwEvent.preventDefault();
                });
            }
            
            // @@ This section is to be replaced shortly
            if (this._editableDocument) {
                this._editableDocument.getRoot().bind('afterSelectionChange', function (event, data) {
                    try {
                        data.editor.showProperties();
                    } catch (error) {
                        // @@ Currently this is just a race condition issue with the editor window, sort this out once the proper setup is defined.
                    }
                }, {editor: this});
            }
            
            // Select the first tab by default
            this._tabber.selectTab(0);
            
            this._tabber.refresh();
            
        }, 'buildUI'),

    /**
     * Update the editor's UI.
     */
    updateUI: EVENT.classMethod(DEPLOY.Editor, 
        function updateUI() {
			var editableDocument = this.getEditableDocument();
		    
		    if (!editableDocument) {
	            return;
	        }
		    
		    var element = editableDocument.getRoot().selectedElement();
            
            // @@ Only update the current tab
            
            for (var i=0; i < this._toolButtons.length; i++) {
                var toolBtn = this._toolButtons[i];
                var tool = toolBtn.widgetData('deploy-tool');
                toolBtn.enable(tool.canUse(editableDocument, element));

				var inUse = tool.inUse(editableDocument, element);
				if (inUse && !toolBtn.hasClass('deploy-in-use')) {
					toolBtn.addClass('deploy-in-use');
				} else if (!inUse && toolBtn.hasClass('deploy-in-use')) {
					toolBtn.removeClass('deploy-in-use');
				}
            }
            
        }, 'updateUI'),
        
    /**
     * Remove the editor's UI.
     */
    removeUI: EVENT.classMethod(DEPLOY.Editor, function removeUI() {
        this._tabber.destroy();
    }, 'removeUI'),
    
    /**
     * Show the properties tab in the tool window.
     */
    showProperties: EVENT.classMethod(DEPLOY.Editor, 
        function showProperties() {
			var form = null;
			var editableDocument = this.getEditableDocument();
			if (!editableDocument) {
		        return;
		    }
			
		    var element = editableDocument.getRoot().selectedElement();
            var prevElment = this._propertiesPanel.widgetData('element');
            
            // Do we need to build a different properties form or is the
            // existing form suitable.
            if ((element !== undefined && element !== null) && 
                (prevElment === undefined || prevElment === null || prevElment.getType() !== element.getType())
                ) {
                
                // Clear existing form
                if (this._propertiesPanel.widgetData('form') !== undefined) {
                    this._propertiesPanel.widgetData('form').remove();
                }
                
                // Create new form
                if (element.typeOf('Text')) {
                    form = this._buildTextForm(this._propertiesPanel);
                } else if (element.typeOf('Image')) {
                    form = this._buildImageForm(this._propertiesPanel);
                }
            } else {
                form = this._propertiesPanel.widgetData('form');
            }
            
            // Store the element against the panel
            this._propertiesPanel.widgetData('element', element);
            
            // Show/Hide the form
            if (element === undefined || element === null) {
                if (form !== undefined && form !== null) {
                    form.hide();
                }
            } else {
                form.show();

                // Populate form
                if (element.typeOf('Text')) {
                    this._populateTextForm(element, form);
                } else if (element.typeOf('Image')) {
                    this._populateImageForm(element, form);
                }
            }
        }, 'showProperties'),
    
    _buildBaseForm: function (panel) {
        var form = new FLUID.Form({parent: panel});
        panel.widgetData('form', form);
        form.bind('onSend', function (ev, data) {
            ev.stopDefault();
        });

        var fieldset = new FLUID.Fieldset({parent: form, legend: _('Block')});
        
        // ID field
        var idField = new FLUID.Field.Text({
            parent: fieldset, 
            name: 'htmlID',
            label: _('ID#')
        });
        
        idField.unbind('afterChange.deploy');
        idField.bind('afterChange.deploy', function (event, data) {
            var element = this.closest({'isA': 'Form'}).getParent().widgetData('element');
            if (element !== undefined && element !== null) {
                element.changeId(this.value());
            }
        }, {editor: this});
        
		// CSS field
		var cssField = new FLUID.Field.Text({
            parent: fieldset, 
            name: 'htmlCSS',
            label: _('CSS')
        });
		
        cssField.unbind('afterChange.deploy');
        cssField.bind('afterChange.deploy', function (event, data) {
            var element = this.closest({'isA': 'Form'}).getParent().widgetData('element');
            if (element !== undefined && element !== null) {
				var rawClasses = this.value().split(',');
				var classes = [];
				for (var i = 0; i < rawClasses.length; i++) {
					classes.push($$.trim(rawClasses[i]));
				}
                element.styles(classes);
        	}
		});
		
        return form;
    },
    
    _buildTextForm: function (panel) {
        var form = this._buildBaseForm(panel);
        
        form.refresh();
        return form;
    },
    
    _buildImageForm: function (panel) {
        var form = this._buildBaseForm(panel);
        
        form.refresh();
        return form;
    },
    
    _populateBaseForm: function (element, form) {
        var fieldMap = form.getFieldMap();
        var idField = fieldMap['htmlID'];
        var cssField = fieldMap['htmlCSS'];

        // Populate ID field
        if (element.hasOwnId()) {
            idField.value(element.getId());
        } else {
            idField.value('');
        }
		
		// Populate CSS Field
		if (element.styles().length > 0) {
        	cssField.value(element.styles().join(', '));
    	} else {
	        cssField.value('');
		}
	},
    
    _populateTextForm: function (element, form) {
        this._populateBaseForm(element, form);
    },
    
    _populateImageForm: function (element, form) {
        this._populateBaseForm(element, form);
    },
    
    defaultOptions: {}
});


/**
 * Register an editor tool.
 * @param {DEPLOY.Editor} editor The editor tool to register.
 */ 
DEPLOY.registerEditorTool = function (tool) {
    DEPLOY._editorTools[tool.getName().toLowerCase()] = tool;
}

/**
 * Return the registered editor tool associated with the specified name.
 * @param {String} name The name of the editor tool being requested.
 * @return A registered editor tool.
 * @type DEPLOY.Editor
 */ 
DEPLOY.getEditorTool = function (name) {
    return DEPLOY._editorTools[name.toLowerCase()];
}


/**
 * In-page editors have one or more tools associated with them that allow 
 * users to perform various editing tasks. 
 * @class An editor tool class.
 * @constructor
 */
DEPLOY.EditorTool = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._name = options.name;
    
    /** @private */
    this._label = options.label;
    
    /** @private */
    this._icon = options.icon;
    
    /** @private */
    this._canUse = options.canUse;
    
    /** @private */
    this._inUse = options.inUse || null;
	
    /** @private */
    this._action = options.action;
    
    // Build the tool button in advance
    
    // Register the editor tool
    DEPLOY.registerEditorTool(this);
};
UTILS.copyPrototype(DEPLOY.EditorTool, EVENT.EventClass);

$$.extend(DEPLOY.EditorTool.prototype, {
    __className__: 'DEPLOY.EditorTool',
    
    /**
     * Return true if the tool can be user;
     * @return Whether the tool can be used.
     * @type Boolean
     */
    canUse: function (doc, elm) { 
        return this._canUse(doc, elm);
    },
    
    /**
     * Return true if the tool is in use by the user;
     * @return Whether the tool is in use.
     * @type Boolean
     */
    inUse: function (doc, elm) { 
		if (this._inUse === null) {
			return false;
    	}	
        return this._inUse(doc, elm);
	},

    /**
     * Use the tool.
     * @param {FLUID.EditableDocument} doc The editable document to perform 
     * the action on.
     * @param {EDITABLE.Element} element The currently selected editable 
     * element.
     */
    use: EVENT.classMethod(DEPLOY.EditorTool, 
        function use(doc, elm) {
			var localArgs = EVENT.getArgs(arguments);
            var doc = EVENT.getArg(localArgs, 0);
            var elm = EVENT.getArg(localArgs, 1);
            this._action(doc, elm);
        }, 'use'),
    
    /**
     * Return the name of the editor.
     * @return The editor's name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    /**
     * Build a button for the tool.
     * @return A button for executing tool.
     * @tyle FLUID.Button
     */
    buildButton: function (editor) {
        var btn = new FLUID.Button({
            label: this._label,
            icon: this._icon
            });
        
        btn.widgetData('deploy-editor', editor);
        btn.widgetData('deploy-tool', this);

        btn.bind('onMouseDown', function (event, data) { 
            event.data.fwEvent.preventDefault();
        });
        
        btn.bind('onClick', function (event, data) { 
            if (this.enabled()) {
                var editor = this.widgetData('deploy-editor');
                var tool = this.widgetData('deploy-tool');
				var doc = editor.getEditableDocument();
		        var elm = doc.getRoot().selectedElement();
		        tool.use(doc, elm);
            }
        });
            
        return btn;
    }
    
});

/**
 * A utility 'can' function for inline text tools.
 * @param {DEPLOY.EditableDocument} doc The editable document.
 * @param {EDITABLE.Element} doc The element currently selected.
 * @return Whether the tool can be used or not.
 * @type Boolean
 */
DEPLOY.EditorTool.inlineTextCanUse = function (doc, elm) {
    if (elm === null || !elm.typeOf('Text')) {
        return false;
    }
    if (elm.textSelection().isCollapsed()) {
        return false;
    }
    return true;
};

/**
 * A utility 'inUse' function for inline text tools.
 * @param {DEPLOY.EditableDocument} doc The editable document.
 * @param {EDITABLE.Element} doc The element currently selected.
 * @return Whether the tool is in use for the current selection or not.
 * @type Boolean
 */
DEPLOY.EditorTool.inlineTextInUse = function (doc, elm, tagName) {
    if (elm === null || !elm.isA('Text')) {
		return false;
	}
	
	var selection = elm.textSelection();

	/* 
	If the selection if collapsed convert the selection to last character.
	*/
	if (selection.isCollapsed() && selection[0] > 0) {
		selection = [selection[0] - 1, selection[0]];
	}
	
	if (elm.htmlSoup().hasTags(selection, new IXMO.Tag(tagName), true) === -1) {
		return false;
	}
	
    return true;
};

/**
 * A utility 'can' function for block text tools.
 * @param {DEPLOY.EditableDocument} doc The editable document.
 * @param {EDITABLE.Element} doc The element currently selected.
 * @return Whether the tool can be used or not.
 * @type Boolean
 */
DEPLOY.EditorTool.blockTextCanUse = function (doc, elm) {
    if (elm === null || !elm.isA('Text')) {
        return false;
    }
    return true;
};

/**
 * A utility 'can' function for block text type tools.
 * @param {DEPLOY.EditableDocument} doc The editable document.
 * @param {EDITABLE.Element} doc The element currently selected.
 * @return Whether the tool can be used or not.
 * @type Boolean
 */
DEPLOY.EditorTool.blockTextTypeCanUse = function (doc, elm) {
    if (elm === null || !elm.typeOf('Text')) {
        return false;
    }
    return true;
};

/* Default editor tools */

// Bold
new DEPLOY.EditorTool({
    name: 'strong',
    icon: 'deploy.icons.editor.bold',
    canUse: DEPLOY.EditorTool.inlineTextCanUse,
    inUse: function (doc, elm) {
        return DEPLOY.EditorTool.inlineTextInUse(doc, elm, 'strong');
    },
    action: function (doc, elm) {
        var selection = elm.textSelection();
        if (elm.htmlSoup().test(selection, 'strong', [], false)) {
            elm.htmlSoup().unformat(selection, 'strong');
        } else {
            elm.htmlSoup().format(selection, 'strong');
		}
	    elm.refreshHTML();
        elm.textSelection(selection);
    }
});

// Italic
new DEPLOY.EditorTool({
    name: 'em',
    icon: 'deploy.icons.editor.italic',
    canUse: DEPLOY.EditorTool.inlineTextCanUse,
    inUse: function (doc, elm) {
        return DEPLOY.EditorTool.inlineTextInUse(doc, elm, 'em');
    },
    action: function (doc, elm) {
        var selection = elm.textSelection();
        if (elm.htmlSoup().test(selection, 'em', [], false)) {
            elm.htmlSoup().unformat(selection, 'em');
        } else {
            elm.htmlSoup().format(selection, 'em');
		}
	    elm.refreshHTML();
        elm.textSelection(selection);
    }
});

// Insert anchor
new DEPLOY.EditorTool({
    name: 'insert-a',
    icon: 'deploy.icons.editor.insert-a',
    canUse: function (doc, elm) {
        if (elm !== null && elm.isA('Image')) { 
            return true;
        }
        return DEPLOY.EditorTool.inlineTextCanUse(doc, elm);
    },
    action: function (doc, elm) {
        var desktop = FLUID.widgetByName('desktop');
        var selection = null;
        var existingURL = '';
        var offset = [0, 0];
        var tags = [];
        
        if (elm.isA('Image')) {
            existingURL = elm.href();
            offset = $$(elm.getHTMLElement()).offset();
            
        } else if (elm.typeOf('Text')) {
            selection = elm.textSelection();
        
            tags = elm.htmlSoup().getTags(selection, 'a');
            if (tags.length > 0) {
                existingURL = tags[0].getAttribute('href') ? tags[0].getAttribute('href') : '';
            }

            // Add link marker span            
            elm.htmlSoup().format(selection, 'span', {'class': 'deploy-link-marker'});

            // Mark to find the position
            elm.htmlSoup().format([selection[1] - 1, selection[1]], 'span', {'id': 'deploy-link-end-marker'});
    	    elm.refreshHTML();

            offset = $$('#deploy-link-end-marker').offset();

            elm.htmlSoup().unformat([selection[1] - 1, selection[1]], 'span', {'id': 'deploy-link-end-marker'});
    	    elm.refreshHTML();
        }
        
        // Generate the linkable URL
        var linkableURL = IO.Request.buildURL('deploy-page', 'linkable');
        
        // Show a bubble window with a form in for the user to specify a 
        // link with.
        var bubbleWin = new FLUID.BubbleWindow({owner: desktop, flags: FLUID.Bubble.PIN});
        
        var linkForm = new FLUID.Form({parent: bubbleWin});
        var linkFieldset = new FLUID.Fieldset({parent: linkForm});
        var linkField = new FLUID.Field.AutoComplete({
            parent: linkFieldset, 
            name: 'url',
            datalistURL: linkableURL
            });
        linkField.maskValue(existingURL);
        
        bubbleWin.tether(offset.left - $$(document).scrollLeft(), offset.top - $$(document).scrollTop());
        bubbleWin.popup();
        bubbleWin.bind('onPopupComplete', function (ev, data) {
            data.linkField.focus();
        }, {doc: doc, elm: elm, linkField: linkField});
        
        bubbleWin.bind('onBurst', function (ev, data) {
			var form = data.form;
			
            // Get the value 
            var url = form.getData()['url']; // Auto-complete
            if (url === undefined || $$.trim(url) === '') {
                url = form.getFieldMap()['url'].getMaskSelection().val(); // Manual entry
            }

            // Check for emails and convert to mailto:
            var email_exp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if(url && email_exp.test(url)) {
                url = 'mailto:' + url;
            }

            // Apply the link
            if (data.elm.isA('Image')) {
                data.elm.href(url);
            } else if (data.elm.typeOf('Text')) {
                if (url !== undefined && url !== '') {
                    data.elm.htmlSoup().unformat(selection, 'a');
                    data.elm.htmlSoup().format(selection, 'a', {'href': url});
                } else {
                    data.elm.htmlSoup().unformat(selection, 'a');
                }
        	    data.elm.refreshHTML();
            }	
	
            if (elm.typeOf('Text')) {
                // Clear link marker span        
                data.elm.htmlSoup().unformat(data.selection, 'span', {'class': 'deploy-link-marker'});
        	    data.elm.refreshHTML();
            
                // Re-select the element and it's content
                data.elm.select();
                data.elm.textSelection(data.selection);
            }
            
        }, {doc: doc, elm: elm, selection: selection, form: linkForm});
    
        linkForm.bind('onSend', function (ev, data) {
            ev.stopDefault();
            data.win.burst();
        }, {win: bubbleWin, doc: doc, elm: elm, selection: selection});
    }
});

// Remove anchor
new DEPLOY.EditorTool({
    name: 'delete-a',
    icon: 'deploy.icons.editor.delete-a',
    canUse: function (doc, elm) {
        if (elm !== null && elm.isA('Image')) {
            return elm.href() !== '';
        }
        return DEPLOY.EditorTool.inlineTextCanUse(doc, elm);
    },
    action: function (doc, elm) {
        if (elm.isA('Image')) {
            elm.href('');
            return;
        } else if (elm.typeOf('Text')) {
            var selection = elm.textSelection();
            elm.htmlSoup().unformat(selection, 'a');
	        elm.refreshHTML();
            elm.textSelection(selection);
        }
    }
});

// Left align
new DEPLOY.EditorTool({
    name: 'align-left',
    icon: 'deploy.icons.editor.align-left',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
        
        elm.align('left');
    }
});

// Center align
new DEPLOY.EditorTool({
    name: 'align-center',
    icon: 'deploy.icons.editor.align-center',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
        
        elm.align('center');
    }
});

// Right align
new DEPLOY.EditorTool({
    name: 'align-right',
    icon: 'deploy.icons.editor.align-right',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
        
        elm.align('right');
    }
});

// H1
new DEPLOY.EditorTool({
    name: 'h1',
    icon: 'deploy.icons.editor.h1',
    canUse: DEPLOY.EditorTool.blockTextCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
		if (elm.htmlTag() === 'h1') {
			return;
		}

        var selection = elm.textSelection();
		elm.htmlTag('h1');
		elm.refreshHTML();
        elm.textSelection(selection);

		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// H2
new DEPLOY.EditorTool({
    name: 'h2',
    icon: 'deploy.icons.editor.h2',
    canUse: DEPLOY.EditorTool.blockTextCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
		if (elm.htmlTag() === 'h2') {
			return;
		}
        
        var selection = elm.textSelection();
		elm.htmlTag('h2');
		elm.refreshHTML();
        elm.textSelection(selection);

		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// H3
new DEPLOY.EditorTool({
    name: 'h3',
    icon: 'deploy.icons.editor.h3',
    canUse: DEPLOY.EditorTool.blockTextCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
		if (elm.htmlTag() === 'h3') {
			return;
		}
        
        var selection = elm.textSelection();
		elm.htmlTag('h3');
		elm.refreshHTML();
        elm.textSelection(selection); 

		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// Paragraph
new DEPLOY.EditorTool({
    name: 'paragraph',
    icon: 'deploy.icons.editor.p',
    canUse: function (doc, elm) {
        
        if (elm === null) {
            return false;
        }
        
        /* Do we have permission to add an element to this collection */
        if (!elm.isA('Text') && !elm.getHTMLCollection().can('add')) {
            return false;
        }
        
        return true;
    },
    inUse: function () {
        return false;
    },
    action: function (doc, elm) {
        if (elm.isA('Text')) {
			if (elm.htmlTag() === 'p') {
				return;
			}
	
            var selection = elm.textSelection();
			elm.htmlTag('p');
			elm.refreshHTML();
	        elm.textSelection(selection);
        } else {
            /*
	        If the element we are creating the paragraph next to isn't within
	        a HTML collection we need to find the parent element that is.
	        */
	        var ancestor = elm;
	        if (elm.isA('ListItem')) {
	            ancestor = elm.lastAncestor({isA: 'List'});
	        }
	        if (elm.isA('TableCell')) {
	            ancestor = elm.lastAncestor({isA: 'Table'});
	        }
	        var insertIndex = ancestor.getParent().indexOfChild(ancestor);

	        var text = new EDITABLE.Text();
	        ancestor.getParent().insertChild(insertIndex + 1, text);
            text.select();
	        text.textSelection([0, text.htmlSoup().getLength()]);
		}
		
		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// BR
new DEPLOY.EditorTool({
    name: 'br',
    icon: 'deploy.icons.editor.insert-br',
    canUse: DEPLOY.EditorTool.blockTextCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (!elm.isA('Text')) {
            return;
        }
        
        var selection = elm.textSelection();
        var soupLength = elm.htmlSoup().getLength();
        
        /* 
        Don't allow insertions at the start of the elements content or 
        next to another break.
        */
        if (selection[0] === 0 || elm.htmlSoup().test([selection[0] - 1, Math.min(selection[0] + 1, soupLength)], 'br')) {
            return;
        }    
        
        if (selection[0] === soupLength) {
            elm.htmlSoup().paste(selection[0], '<br />&nbsp;');
        } else {
            elm.htmlSoup().paste(selection[0], '<br />');
        }
        elm.refreshHTML();
        
        // Move the selection on one character
        selection[0] += 1;
        selection.collapse();
        elm.textSelection(selection);
    }
});

// HR
new DEPLOY.EditorTool({
    name: 'hr',
    icon: 'deploy.icons.editor.insert-hr',
    canUse: function (doc, elm) {
        if (elm === null) {
            return false;
        }        
        
        /* Do we have permission to add an element to this collection */
        if (!elm.getHTMLCollection().can('add')) {
            return false;
        }
        
        return true;
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        var rule = new EDITABLE.Rule({style: 'section-break'});
        
        /*
        If the element we are creating the rule next to isn't within a 
        HTML collection we need to find the parent element that is.
        */
        var ancestor = elm;
        if (elm.isA('ListItem')) {
            ancestor = elm.lastAncestor({isA: 'List'});
        }
        var insertIndex = ancestor.getParent().indexOfChild(ancestor);
        
        // Insert the rule
        ancestor.getParent().insertChild(insertIndex + 1, rule);
        
        /*
        If the next text relative is not in the same collection then we
        add a text element to aid the user, otherwise it may not be
        immediately obviously to them how they add text after the break. 
        */
        var nextText = ancestor.nextSibling({'isA': 'Text'});
        if (!nextText) {
            var text = new EDITABLE.Text();
            ancestor.getParent().insertChild(insertIndex + 2, text);
            text.textSelection([0, text.htmlSoup().getLength()]);
        }
    }
});

// Ordered list
new DEPLOY.EditorTool({
    name: 'ol',
    icon: 'deploy.icons.editor.ol',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        var selection = elm.textSelection();
        switch (elm.getType()) {
                case 'Text':
                var parent = elm.getParent();
                var list = new EDITABLE.List({htmlTag: 'ol'});
                var listItem = elm.as(EDITABLE.ListItem);
                var listIndex = parent.indexOfChild(elm);
                parent.removeChild(elm);
                parent.insertChild(listIndex, list);
                list.appendChild(listItem);
                elm = listItem;
                elm.select();
                break;
            case 'ListItem':
				if (elm.getParent().htmlTag() === 'ol') {
					return;
				}
				
                elm.getParent().htmlTag('ol');
                break;
        }
		elm.refreshHTML();
        elm.textSelection(selection); 
		
		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// Unordered list
new DEPLOY.EditorTool({
    name: 'ul',
    icon: 'deploy.icons.editor.ul',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        var selection = elm.textSelection();
        switch (elm.getType()) {
                case 'Text':
                var parent = elm.getParent();
                var list = new EDITABLE.List({htmlTag: 'ul'});
                var listItem = elm.as(EDITABLE.ListItem);
                var listIndex = parent.indexOfChild(elm);
                parent.removeChild(elm);
                parent.insertChild(listIndex, list);
                list.appendChild(listItem);
                elm = listItem;
                elm.select();
                break;
            case 'ListItem':
				if (elm.getParent().htmlTag() === 'ol') {
					return;
				}

                elm.getParent().htmlTag('ul');
                break;
        }
		elm.refreshHTML();
        elm.textSelection(selection);
		
		// Remove any exist styles
		elm.styles([]);

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// Indent
new DEPLOY.EditorTool({
    name: 'indent',
    icon: 'deploy.icons.editor.indent',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (elm.getParent().indexOfChild(elm) != 0) {
            var selection = elm.textSelection();
            elm.indent();
            elm.select();
            elm.textSelection(selection);
        }

      	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// Unindent
new DEPLOY.EditorTool({
    name: 'unindent',
    icon: 'deploy.icons.editor.unindent',
    canUse: DEPLOY.EditorTool.blockTextTypeCanUse,
    inUse: function () { return false; },
    action: function (doc, elm) {
        elm.unindent();

	  	// Update the styles UI
		DEPLOY.getEditableEnvironment().getStylesMgr().updateUI(elm);
    }
});

// Insert/Update table
new DEPLOY.EditorTool({
    name: 'insert-table',
    icon: 'deploy.icons.editor.insert-table',
    canUse: function (doc, elm) {
        if (elm === null) {
            return false;
        }
        
        /* Do we have permission to add an element to this collection */
        if (!elm.isA('Caption') || !elm.isA('TableCell')) {
            return elm.getHTMLCollection().can('add');
        }
    
        return DEPLOY.EditorTool.blockTextTypeCanUse();
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        
        var selection = elm.textSelection();
        var children;
        var child;
        var i;
        
        /* Enter modal mode */
        FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);
        
        /* Create a window to show the table options */
        var win = new FLUID.Window({
            name: 'deploy-window-table-insert',
            group: 'modal',
            label: _('Insert table'),
            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
            flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED | FLUID.Window.HAS_ACTIONS
            });        
        
        /* Handle the user closing the window*/
        win.bind('afterCloseComplete', function(event, data) {
            this.destroy();

            /*
            Re-select the element and it's content (if it is still part of the
            document.
            */
            if (data.elm.getRoot() !== null) {
                data.elm.select();
                data.elm.textSelection(data.selection);
            }
            
            FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);  
        }, {doc: doc, elm: elm, selection: selection});

        /* Create a form for the table options */
        var tableForm = new FLUID.Form({parent: win});
        var tableFieldset = new FLUID.Fieldset({parent: tableForm});
        
        var captionField = new FLUID.Field.Boolean({
            parent: tableFieldset, 
            name: 'caption',
            label: _('Caption'),
        	classes: ['fluid-split', 'fluid-last']
        });
    	
        var headerField = new FLUID.Field.Boolean({
            parent: tableFieldset, 
            name: 'header',
            label: _('Header'),
        	classes: ['fluid-split']
        });    	
    	
        var footerField = new FLUID.Field.Boolean({
            parent: tableFieldset, 
            name: 'footer',
            label: _('Footer'),
        	classes: ['fluid-split', 'fluid-last']
        });    	
    	
    	var columnsField = new FLUID.Field.Number({
            parent: tableFieldset, 
            name: 'columns',
            label: _('Columns'),
        	classes: ['fluid-field-eighth', 'fluid-split'],
        	required: true,
        	value: 2
        });
    	
    	var rowsField = new FLUID.Field.Number({
            parent: tableFieldset, 
            name: 'rows',
            label: _('Rows'),
        	classes: ['fluid-field-eighth', 'fluid-split', 'fluid-last'],
        	required: true,
        	value: 2
        });
                
        /* Add an insert/update button to the window */
		var insertBtn = new FLUID.Button({
        	parent: win,
			group: 'actions',
        	label: _('Insert')
    	});

        /* Set the form to always use a puesdoSend */
        insertBtn.bind('onClick', function (ev, data) {
            data.form.puesdoSend();
        }, {form: tableForm});
        
        tableForm.bind('onSend', function (ev, data) {
            ev.stopDefault();
            this.puesdoSend();
        });
        
        /* When a valid form is submitted insert the table */
        tableForm.bind('afterSendSuccess', function (ev, data) {
            var formData = this.getData();
            var rowCount = parseInt(formData['rows']);
            var columnCount = parseInt(formData['columns']);
            var table = null;
            var caption = null;
            var thead = null;
            var tbody = null;
            var tfoot = null;
            var row = null;
            var column = null;
            var children;
            var child;
            var childCount = 0;
            var i = 0;
            var j = 0;
            
            /* Validate the number of rows & columns */
            if (rowCount < 1 || rowCount > 50) {
                FLUID.widgetByName('desktop').notify('Please specify between 1 and 50 rows');
                return;
            }
            
            if (columnCount < 1 || columnCount > 20) {
                FLUID.widgetByName('desktop').notify('Please specify between 1 and 20 columns');
                return;
            }
            
            if (elm.isA('Caption') || elm.isA('TableCell')) {
                
                /* Update */
                table = elm.firstAncestor({isA: 'Table'});
                
                // Find existing table sections
                var children = elm.firstAncestor({isA: 'Table'}).getChildren();
                for (i = 0; i < children.length; i++) {
                    child = children[i];
                    if (child.isA('Caption')) {
                        caption = child;
                    } else if (child.isA('TableSection')) {
                        if (child.htmlTag() === 'thead') {
                            thead = child;
                        } else if (child.htmlTag() === 'tfoot') {
                            tfoot = child;
                        } else if (child.htmlTag() === 'tbody') {
                            tbody = child;
                        }
                    }
                }
                
                /* Caption */
                if (formData['caption'] && caption === null) {
                    // Add
                    table.appendChild(new EDITABLE.Caption());
                } else if (!formData['caption'] && caption !== null) {
                    // Remove
                    table.removeChild(caption);
                }
                
                /* Table header */
                if (formData['header'] && thead === null) {
                    // Add
                    thead = new EDITABLE.TableSection({htmlTag: 'thead'});
                    row = new EDITABLE.TableRow();
                    thead.appendChild(row);
                    for (i = 0; i < columnCount; i++) {
                        row.appendChild(new EDITABLE.TableCell({
                            'htmlTag': 'th',
                            'content': 'Heading ' + (i + 1)
                            }));
                    }
                    table.appendChild(thead);
                    
                } else if (!formData['header'] && thead !== null) {
                    // Remove
                    table.removeChild(thead);
                
                } else {
                    /* 
                    Update - Do we need to increase the number of columns in 
                    the header?
                    */
                    
                    if (thead) {
                        row = thead.getChildren()[0];
                        childCount = row.getChildCount();
                    
                        if (childCount < columnCount) {
                            /* Add columns */
                            for (i = 0; i < (columnCount - childCount); i++) {
                                row.appendChild(new EDITABLE.TableCell({
                                    'htmlTag': 'th',
                                    'content': 'Heading ' + (i + childCount + 1)
                                    }));
                            }
                        
                        } else if (childCount > columnCount) {
                            /* Remove columns */
                            for (i = 0; i < (childCount - columnCount); i++) {
                                child = row.lastChild();
                                row.removeChild(child);
                            }

                        }
                    }
                }    
                
                /* Table footer */
                if (formData['footer'] && tfoot === null) {
                    // Add
                    tfoot = new EDITABLE.TableSection({htmlTag: 'tfoot'});
                    row = new EDITABLE.TableRow();
                    tfoot.appendChild(row);
                    for (i = 0; i < columnCount; i++) {
                        row.appendChild(new EDITABLE.TableCell({
                            'htmlTag': 'td',
                            'content': 'Footer ' + (i + 1)
                            }));
                    }
                    table.appendChild(tfoot);
                    
                } else if (!formData['footer'] && tfoot !== null) {
                    // Remove
                    table.removeChild(tfoot);
                
                } else {
                    /* 
                    Update - Do we need to increase the number of columns in 
                    the footer?
                    */
                    
                    if (tfoot) {
                        row = tfoot.getChildren()[0];
                        childCount = row.getChildCount();
                    
                        if (childCount < columnCount) {
                            /* Add columns */
                            for (i = 0; i < (columnCount - childCount); i++) {
                                row.appendChild(new EDITABLE.TableCell({
                                    'htmlTag': 'td',
                                    'content': 'Footer ' + (i + childCount + 1)
                                    }));
                            }
                        
                        } else if (childCount > columnCount) {
                            /* Remove columns */
                            for (i = 0; i < (childCount - columnCount); i++) {
                                child = row.lastChild();
                                row.removeChild(child);
                            }

                        }
                    }
                }
                
                /* Table body */
                
                /* Update the number of columns */
                row = tbody.getChildren()[0];
                childCount = row.getChildCount();
                
                if (childCount < columnCount) {
                    /* Add columns */
                    children = tbody.getChildren();
                    for (i = 0; i < children.length; i++) {
                        row = children[i];
                        for (j = 0; j < (columnCount - childCount); j++) {
                            row.appendChild(new EDITABLE.TableCell({
                                'htmlTag': 'td',
                                'content': '-'
                                }));
                        }
                    }
                    
                } else if (childCount > columnCount) {
                    /* Remove columns */
                    children = tbody.getChildren();
                    for (i = 0; i < children.length; i++) {
                        row = children[i];
                        for (j = 0; j < (childCount - columnCount); j++) {
                            child = row.lastChild();
                            row.removeChild(child);
                        }
                    }
                }
                
                /* Update the number of rows */
                childCount = tbody.getChildCount();
                
                if (childCount < rowCount) {
                    /* Add rows */
                    for (i = 0; i < (rowCount - childCount); i++) {
                        row = new EDITABLE.TableRow();
                        tbody.appendChild(row);
                        for (j = 0; j < columnCount; j++) {
                            row.appendChild(new EDITABLE.TableCell({
                                'htmlTag': 'td',
                                'content': '-'
                                }));
                        }
                    }
                    
                } else if (childCount > rowCount) {
                    /* Remove rows */
                    for (i = 0; i < (childCount - rowCount); i++) {
                        child = tbody.lastChild();
                        tbody.removeChild(child);
                    }
                    
                }
                
                
            } else {
                /* Insert */
                var table = new EDITABLE.Table();

                // Caption
                if (formData['caption']) {
                    table.appendChild(new EDITABLE.Caption());
                }

                /* Table head */
                if (formData['header']) {
                    thead = new EDITABLE.TableSection({htmlTag: 'thead'});
                    row = new EDITABLE.TableRow();
                    thead.appendChild(row);
                    for (i = 0; i < columnCount; i++) {
                        row.appendChild(new EDITABLE.TableCell({
                            'htmlTag': 'th',
                            'content': 'Heading ' + (i + 1)
                            }));
                    }
                    table.appendChild(thead);
                }

                /* Table body */
                tbody = new EDITABLE.TableSection({htmlTag: 'tbody'});
                for (i = 0; i < rowCount; i++) {
                    row = new EDITABLE.TableRow();
                    tbody.appendChild(row);
                    for (j = 0; j < columnCount; j++) {
                        row.appendChild(new EDITABLE.TableCell({
                            'htmlTag': 'td',
                            'content': '-'
                            }));
                    }
                }
                table.appendChild(tbody);

                /* Table foot */
                if (formData['footer']) {
                    tfoot = new EDITABLE.TableSection({htmlTag: 'tfoot'});
                    row = new EDITABLE.TableRow();
                    tfoot.appendChild(row);
                    for (i = 0; i < columnCount; i++) {
                        row.appendChild(new EDITABLE.TableCell({
                            'htmlTag': 'td',
                            'content': 'Footer ' + (i + 1)
                            }));
                    }
                    table.appendChild(tfoot);
                }

                /*
                If the element we are creating the table next to isn't within a 
                HTML collection we need to find the parent element that is.
                */
                var ancestor = elm;
                if (elm.isA('ListItem')) {
                    ancestor = elm.lastAncestor({isA: 'List'});
                }
                
                var insertIndex = ancestor.getParent().indexOfChild(ancestor);
                ancestor.getParent().insertChild(insertIndex + 1, table);
                
            }
            
            /* Close the window */
            win.close();
            
        }, {win: win, doc: doc, elm: elm});
        
        /* 
        Change labels and pre-populate the form if we are updating an existing
        table element.
        */
        if (elm.isA('Caption') || elm.isA('TableCell')) {
            /* Update labels */
            win.label(_('Update table'));
            insertBtn.label(_('Update'));
        
            /* Pre-populate the form */
            var children = elm.firstAncestor({isA: 'Table'}).getChildren();
            for (i = 0; i < children.length; i++) {
                child = children[i];
                if (child.isA('Caption')) {
                    captionField.value(true);
                } else if (child.isA('TableSection')) {
                    if (child.htmlTag() === 'thead') {
                        headerField.value(true);
                    } else if (child.htmlTag() === 'tfoot') {
                        footerField.value(true);
                    } else if (child.htmlTag() === 'tbody') {
                        rowsField.value(child.getChildCount());
                        columnsField.value(child.getChildren()[0].getChildCount());
                    }
                }
            }
        }
        
        /* Open the table insert/update window */
        win.refresh();
        win.prepare();
        win.open();
    }
});

// Toggle table heading
new DEPLOY.EditorTool({
    name: 'th',
    icon: 'deploy.icons.editor.th',
    canUse: function (doc, elm) {
        return elm !== null && elm.isA('TableCell');
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (elm.htmlTag() === 'th') {
            elm.htmlTag('td');
        } else {
            elm.htmlTag('th');
        }
    }
});

// Insert image
new DEPLOY.EditorTool({
    name: 'insert-image',
    icon: 'deploy.icons.editor.insert-image',
    canUse: function (doc, elm) {
        if (elm === null) {
            return false;
        }
        
        if (elm.isA('Image')) { 
            return true;
        }
        
        if(!elm.getHTMLCollection().can('add')) {
            return false;
        }
        
        return DEPLOY.EditorTool.blockTextTypeCanUse(doc, elm);
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        // Determine the controller and field to use?
        var collectionID = elm.getHTMLCollection().getHTMLElementId();
        var controller = doc.getController(collectionID);
        var storeImages = doc.getStoreImages(collectionID);
        var recordID = doc.getID(collectionID);
        var metaWin = null;
        
        if (elm.isA('Image')) {
            
            /* Update image */
            var attachment = doc.getImageAttachment(collectionID, elm.src());
            if (attachment !== null) {
                
                /* Manage the images meta */
                FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);
                DEPLOY.Attachment.manageMeta(attachment.current);
                
                /* Apply any changes to the image */
                metaWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
                metaWin.bind('onClose', function (event, data) {
                    var attachment = data.attachment.current;

                    /* Alternative text update */
                    elm.alt(attachment.meta('alt'));
                    
                    /* Long description update */
                    elm.longdesc(attachment.meta('longdesc'));
                    
                }, {elm: elm, attachment: attachment});
                
            } else {
                
                /* Haven't got an attachment for the image so get one */
                var imageGetReq = new IO.Request({
    		    	controller: controller.getName(),
        			method: 'inpage-get-image',
    			    type: 'GET',
        			data: {
        			    id: recordID, 
        			    store_images: storeImages,
        		        images: elm.src()
        			}
        		});
            
                imageGetReq.bind('afterSuccess', function (event, data) {
                    /* 
                    Convert the attachment map into a list of images, we only want
                    the first image.
                    */
                    var images = [];
                    for (var imagePath in event.data.data.attachments) {
                        if (event.data.data.attachments.hasOwnProperty(imagePath)) {
                            images.push(event.data.data.attachments[imagePath]);
                        }
                    }                
                
                    /* Create an attachment for the image */
                    var attachment = new DEPLOY.Attachment({
                        path: images[0].url, 
                        meta: images[0].meta, 
                        expectedMeta: images[0].expected_meta
                    });
                    
                    /* Add the attachment to the doc */
                    doc.addImageAttachment(data.collectionID, data.elm.src(), attachment);
                
                    /* Manage the images meta */
                    FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);
                    DEPLOY.Attachment.manageMeta(attachment);
                    
                    /* Apply any changes to the image */
                    metaWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
                    metaWin.bind('onClose', function (event, data) {
                        var attachment = data.attachment;

                        /* Alternative text update */
                        elm.alt(attachment.meta('alt'));
                        
                        /* Long description update */
                        elm.longdesc(attachment.meta('longdesc'));
                        
                    }, {elm: elm, attachment: attachment});

                }, {win: win, doc: doc, elm: elm, collectionID: collectionID});
        
                imageGetReq.send();            
            }
            
        } else {
            /* Insert image */
            var selection = elm.textSelection();

            FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);

    		/* 
    		If the window is already open then don't allow it to be opened
    		again, instead bring it to the front.
    		*/
    		if (FLUID.widgetByName('deploy-window-image-insert')) {
    			FLUID.widgetByName('deploy-window-image-insert').minimize(false);
    			FLUID.widgetByName('deploy-window-image-insert').sendToFront();
    			return;
    		}

            var win = new FLUID.Window({
                name: 'deploy-window-image-insert',
                group: 'modal',
                label: _('Insert image'),
                size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
                minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
                maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
                flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED | FLUID.Window.HAS_ACTIONS
                });
    	
            var imageForm = new FLUID.Form({parent: win});
            var imageFieldset = new FLUID.Fieldset({parent: imageForm});
            var imageField = new DEPLOY.SingleAttachmentField({
                parent: imageFieldset, 
                name: storeImages,
                controller: controller
                });
        
    		var insertBtn = new FLUID.Button({
            	parent: win,
    			group: 'actions',
            	label: _('Insert'),
            	enabled: false,
            	classes: ['fluid-sep']
        	});
    	
            imageField.bind('afterAttachment', function (ev, data) {
                if (this.attachment() !== null) {
                    data.insertBtn.enable();
                } else {
                    data.insertBtn.enable(false);
                }
            }, {insertBtn: insertBtn});
        
            insertBtn.bind('onClick', function (ev, data) {
                var attachment = data.imageField.attachment();
            
                var imageInsertReq = new IO.Request({
    		    	controller: data.controller.getName(),
        			method: 'inpage-insert-image',
    			    type: 'POST',
        			data: {
        			    id: data.recordID, 
        			    store_images: data.storeImages,
        			    images: JSON.stringify([attachment])
        			}
        		});
            
                imageInsertReq.bind('afterSuccess', function (event, data) {
                
                    // Get the image variation
                    var images = [];
                    for (var imagePath in event.data.data.attachments) {
                        if (event.data.data.attachments.hasOwnProperty(imagePath)) {
                            images.push(event.data.data.attachments[imagePath]);
                        }
                    }
                
                    // No support for inserting multiple images just yet
                    var image = new EDITABLE.Image({src: images[0].variation_path, 
                        alt: images[0].meta.alt || '', 
                        size: [images[0].meta.width || 400, images[0].meta.height || 400],
                        constraints: [50, Math.max(images[0].meta.original_height, images[0].meta.original_width)]
                        });
                
                    /*
                    If the element we are creating the rule next to isn't within a 
                    HTML collection we need to find the parent element that is.
                    */
                    var ancestor = data.elm;
                    if (elm.isA('ListItem'))
                        ancestor = elm.lastAncestor({isA: 'List'});
                    var insertIndex = ancestor.getParent().indexOfChild(ancestor);
                    ancestor.getParent().insertChild(insertIndex + 1, image);
                    
                    // Mark the image as inserted incase the user reverts
                    data.doc.imageInserted(data.collectionID, image.src());
                
                    /* Create an attachment for the image */
                    var attachment = new DEPLOY.Attachment({
                        path: images[0].path, 
                        meta: images[0].meta, 
                        expectedMeta: images[0].expected_meta
                    });
                
                    /* Add the attachment to the doc */
                    doc.addImageAttachment(data.collectionID, images[0].variation_path, attachment);
                    
                    // Close the window
                    data.win.close();
                
                }, {win: win, doc: doc, elm: elm, collectionID: collectionID});
            
                imageInsertReq.send();
            
            }, {
                win: win, 
                imageForm: imageForm, 
                imageField: imageField, 
                doc: doc, 
                elm: elm, 
                collectionID: collectionID, 
                controller: controller, 
                storeImages: storeImages,
                recordID: recordID
            });
        
            win.bind('afterCloseComplete', function(event, data) {
                this.destroy();
            
                // Re-select the element and it's content
                data.elm.select();
                data.elm.textSelection(data.selection);
            
                FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);  
            }, {doc: doc, elm: elm, selection: selection});
        
        	win.refresh();
        	win.prepare();
    		win.open(); 
		}
    }
});

// Insert video
new DEPLOY.EditorTool({
    name: 'insert-video',
    icon: 'deploy.icons.editor.insert-video',
    canUse: function (doc, elm) {
        if (elm === null) {
            return false;
        }
        
        if (elm.isA('Video')) { 
            return true;
        }
        
        if(!elm.getHTMLCollection().can('add')) {
            return false;
        }
        
        return DEPLOY.EditorTool.blockTextTypeCanUse(doc, elm);
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        
        var desktop = FLUID.widgetByName('desktop');
        
        desktop.startModal(FLUID.Desktop.MODAL);
        
        var selection = null;
        var existingURL = '';
        var existingTitle = '';
        
        if (elm.isA('Video')) {
            existingURL = elm.href();
            existingTitle = elm.title();
        } else if (elm.typeOf('Text')) {
            selection = elm.textSelection();
        }
        
		// If the window is already open then don't allow it to be opened
		// again, instead bring it to the front.
		if (FLUID.widgetByName('deploy-window-video-insert')) {
			FLUID.widgetByName('deploy-window-video-insert').minimize(false);
			FLUID.widgetByName('deploy-window-video-insert').sendToFront();
			return;
		}

        var win = new FLUID.Window({
            name: 'deploy-window-video-insert',
            group: 'modal',
            label: _('Insert video'),
            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
            flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED | FLUID.Window.HAS_ACTIONS
            });
	
        var videoForm = new FLUID.Form({parent: win});
        var videoFieldset = new FLUID.Fieldset({parent: videoForm});
        var titleField = new FLUID.Field.Text({
            parent: videoFieldset, 
            name: 'title',
            label: _('Title'),
        	value: existingTitle,
        	required: true
        });
        var urlField = new FLUID.Field.Text({
            parent: videoFieldset, 
            name: 'url',
            label: _('URL'),
        	value: existingURL,
        	required: true
        });
    
		var completeBtn = new FLUID.Button({
        	parent: win,
			group: 'actions',
        	label: _('Insert'),
        	classes: ['fluid-sep']
    	});
    	
    	if (existingURL != '') {
    	    win.label(_('Update video'));
    	    completeBtn.label(_('Update'));
	    }
	            
        completeBtn.bind('onClick', function (ev, data) {
            data.form.puesdoSend();
        }, {win: win, form: videoForm});
        
        videoForm.bind('onSend', function (ev, data) {
            ev.stopDefault();
            this.puesdoSend();
        });
        
        // Handle submissions with empty required fields
        videoForm.bind('afterSendNotReady', function (event, data) {
            var field = event.data.field;
    		var msg = '';

    		// Notify the user that they've missed out a field
    		switch (field.state()) {
    			case FLUID.Field.INVALID_STATE:
    				msg = _('Not all your fields are valid - ') + event.data.field.label();
    				break;

    			case FLUID.Field.CHECKING_STATE:
    				msg = _('Not all your fields have finished validating - ') + event.data.field.label();
    				break;

    			default:
    			    if (event.data.field.label === undefined) {
    				    msg = _('You need to specify - ') + event.data.field.getName();
    		        } else {
    	                msg = _('You need to specify - ') + event.data.field.label();
    	            }
    		};

    		FLUID.widgetByName('desktop').notify(msg);
    	});        
        
        videoForm.bind('afterSendSuccess', function (ev, data) {
            var formData = this.getData();
            
            if (data.elm.isA('Video')) { 
                // Update
                data.elm.title(formData.title);
                data.elm.href(formData.url);
                data.elm.updateHTML();
            } else {
                // Insert
                var video = new EDITABLE.Video({href: formData.url, title: formData.title, size:[400, 300]}); // @@ NEED TO MAKE THESE SETTINGS
                var ancestor = data.elm;
                if (data.elm.isA('ListItem')) {
                    ancestor = data.elm.lastAncestor({isA: 'List'});
                }
                var insertIndex = ancestor.getParent().indexOfChild(ancestor);
                ancestor.getParent().insertChild(insertIndex + 1, video);
            }
            win.close();
            
        }, {win: win, doc: doc, elm: elm});
        
        win.bind('afterCloseComplete', function(event, data) {
            var desktop = FLUID.widgetByName('desktop');
        
            this.destroy();
        
            // Re-select the element and it's content
            if (data.elm.typeOf('Text')) {
                data.elm.select();
                data.elm.textSelection(data.selection);
            }
            
            desktop.endModal(FLUID.Desktop.MODAL);  
        }, {doc: doc, elm: elm, selection: selection});
    	
    	win.refresh();
    	win.prepare();
		win.open();
    }
});

new DEPLOY.EditorTool({
    name: 'delete',
    icon: 'deploy.icons.editor.delete',
    canUse: function (doc, elm) {
        if (elm == null) {
            return false;
        }
        if (!elm.getHTMLCollection().can('remove')) {
            return false;
        }  
        return true;       
    },
    inUse: function () { return false; },
    action: function (doc, elm) {
        if (elm) {
            /*
            Handle deletes of different element types.
            */
            if (elm.isA('Image')) {
                /* Mark the image as removed for Save event */
                var collectionID = elm.getHTMLCollection().getHTMLElementId();
                doc.imageRemoved(collectionID, elm.src());
                doc.removeImageAttachment(collectionID, elm.src());
                
                /* Remove the element */
                elm.getParent().removeChild(elm);
            
            } else if (elm.isA('TableCell')) {
                /*
                If the user is deleting from a table we need to know if they
                are deleting a table, table row, or table column.
                */
                
                /* Enter modal mode */
                FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);

                /* Create a window to show the table options */
                var win = new FLUID.Window({
                    name: 'deploy-window-table-delete',
                    group: 'modal',
                    label: _('Delete table, row or column?'),
                    size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
                    minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
                    maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
                    flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED | FLUID.Window.HAS_ACTIONS
                    });        

                /* Handle the user closing the window*/
                win.bind('afterCloseComplete', function(event, data) {
                    this.destroy();

                    /*
                    Re-select the element and it's content (if it is still part of the
                    document.
                    */
                    if (data.elm.getRoot() !== null) {
                        data.elm.select();
                        data.elm.textSelection(data.selection);
                    }

                    FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);  
                }, {doc: doc, elm: elm, selection: elm.textSelection()});

                /*  */
                
                /* Create a form for the delete options */
                var tableForm = new FLUID.Form({parent: win});
                var tableFieldset = new FLUID.Fieldset({parent: tableForm});

                var deleteTypeField = new FLUID.Field.Choice({
                    parent: tableFieldset, 
                    name: 'deleteType',
                    label: _('Delete the...'),
                    multiple: false,
                });
                new FLUID.FieldOption.Radio({parent: deleteTypeField, label: _('Table'), value: 'table', selected: true});
                new FLUID.FieldOption.Radio({parent: deleteTypeField, label: _('Row'), value: 'row'});
                new FLUID.FieldOption.Radio({parent: deleteTypeField, label: _('Column'), value: 'column'});

                /* Add a delete button to the window */
        		var deleteBtn = new FLUID.Button({
                	parent: win,
        			group: 'actions',
                	label: _('Delete')
            	});

                /* Set the form to always use a puesdoSend */
                tableForm.bind('onSend', function (ev, data) {
                    ev.stopDefault();
                    this.puesdoSend();
                });
                
                deleteBtn.bind('onClick', function (ev, data) {
                    data.form.puesdoSend();
                }, {form: tableForm});

                /* When a valid form is submitted delete the table/row/column */
                tableForm.bind('afterSendSuccess', function (ev, data) {
                    var elm = data.elm;
                    var deleteType = this.getData()['deleteType'];
                    var parent;
                    var table;
                    var sections;
                    var section;
                    var rows;
                    var row;
                    var column;
                    var columnIndex = 0;
                    var i = 0;
                    var j = 0;
                    
                    /* Remove the table, row, or column */
                    switch (deleteType) {
                    
                    case 'table':
                        /* Remove the whole table */
                        table = elm.firstAncestor({isA: 'Table'});
                        table.getParent().removeChild(table);
                        break;
                        
                    case 'row':
                        /* Remove the row */
                        row = elm.firstAncestor({isA: 'TableRow'});
                        section = row.firstAncestor({isA: 'TableSection'});
                        table = section.firstAncestor({isA: 'Table'});
                        
                        /* What if this is the last row? */
                        if (section.getChildCount() === 1) {
                            if (section.htmlTag() === 'tbody') {
                                /* Remove the table */
                                table.getParent().removeChild(table);
                            } else {
                                /* Remove the table header/footer */
                                section.getParent().removeChild(section);
                            }
                        } else {
                            /* Just remove the row */
                            row.getParent().removeChild(row);
                        }
                        break;
                        
                    case 'column':
                        /* Remove the column */
                        row = elm.firstAncestor({isA: 'TableRow'});
                        table = row.firstAncestor({isA: 'Table'});
                        
                        /* 
                        Columns are always even between sections, so it it's 
                        the last column then remove the table.
                        */
                        if (row.getChildCount() === 1) {
                            /* Remove the table */
                            table.getParent().removeChild(table);
                        } else {
                            /* Find the index of the column in the row */
                            columnIndex = row.indexOfChild(elm);
                            
                            /* Remove the column (the cell at that index from each row) */
                            sections = table.getChildren();
                            for (i = 0; i < sections.length; i++) {
                                if (!sections[i].isA('TableSection')) {
                                    continue;
                                }
                                rows = sections[i].getChildren();
                                for (j = 0; j < rows.length; j++) {
                                    column = rows[j].getChildren()[columnIndex];
                                    column.getParent().removeChild(column);
                                }
                            }
                        }
                        
                        break;
                    }
                    
                    /* Close the window */
                    win.close();

                }, {win: win, doc: doc, elm: elm});
                
                /* Open the table delete window */
                win.refresh();
                win.prepare();
                win.open();
                
            } else {
                /* By default just remove the element */
                elm.getParent().removeChild(elm);
            }
        }
    }
});


/* Default editor */

new DEPLOY.Editor({
    name: 'default',
    toolGroups: [
        ['Tools', [
            'strong', 'em', '|', 'insert-a', 'delete-a', '|', 'insert-image', 'insert-video',
            '-',
            'align-left', 'align-center', 'align-right', '|', 'h1', 'h2', 'h3', 'paragraph', '|', 'hr', 'br',
            '-',
            'ol', 'ul', 'indent', 'unindent', '|', 'insert-table', 'th', '|', 'delete'
            ]]
    ]
});

new DEPLOY.Editor({
    name: 'basic',
    toolGroups: [
        ['Tools', [
            'strong', 'em', '|', 'insert-a', 'delete-a',
            '-', 
            'delete'
            ]]
    ]
});

/*
The empty editor is a special case editor used when no collection is selected
by the user.
*/
new DEPLOY.Editor({name: 'empty', toolGroups: [['Tools', []]]});


