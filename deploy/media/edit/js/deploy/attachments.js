/**
 * @fileoverview    Deploy attachment management. Attachments are a mechanism 
 *                  for attaching and managing files.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

/*
 Attachments are attached (associated) using form fields. The fields 
 themselves are an extension of the standard FLUID.Field.EntryField class.

 Single attachments are handled through a field that appears within the form
 where as Multiple attachments are handled in a separate tab although they 
 still store their value in a hidden field as part of the main form.

 This file defines the Single/Multiple attachment classes, an attachment class 
 for holding an attachments data, a multiple attachment manager class and a 
 properties manager class.

 Creation of attachment fields is handled via the deploy form creation 
 function.

 NOTE: Single attachments need to be surrounded by fieldsets, this is 
 currently achieved by the form definition on the server. 
*/

/**
 * An attachment.
 * @class An attachment class.
 * @constructor
 */
DEPLOY.Attachment = function (options) {
    options = UTILS.optionsArg(options, {path: '', thumbnail_path: null, order: 0, meta: {}, expectedMeta: []});
    EVENT.EventClass.call(this);
    
    /** @private */
    this._path = options.path;
    
    /** @private */
    this._order = options.order;
    
    /** @private */
    this._thumbnail_path = options.thumbnail_path;
    
    /** @private */
    this._meta = options.meta || {};
    
    /** @private */
    this._expectedMeta = options.expectedMeta;
}
UTILS.copyPrototype(DEPLOY.Attachment, EVENT.EventClass);

$$.extend(DEPLOY.Attachment.prototype, {
    __className__: 'DEPLOY.Attachment',
    
    /**
     * Return an identity for the attachment, although path is technically an
     * the true identity, it's often long so this method returns a shorter 
     * more readble version.
     * @return An identity for the attachment.
     * @type String
     */
    getIdentity: function () {
        
        // Remove the path and extension
        var identity = this._path.split('/');
        identity = identity[identity.length - 1].split('.')[0];
        
        // Remove special characters and handle white spaces
        identity = identity.replace(/[^a-zA-Z 0-9 ]+/g, ' ').replace(/\s+/g, ' ');
        return UTILS.abbreviate($$.trim(identity), 30);
    },
    
    /**
     * Return the path for the attachment.
     * @return The unique path for the attachment.
     * @type String
     */
    getPath: function () {
        return this._path;
    },

    /**
     * Return the thumbnail path for the attachment.
     * @return The thumbnail path for the attachment.
     * @type String
     */
    getThumbnailPath: function () {
        return this._thumbnail_path;
    },

    /**
     * Return the expected meta for the attachment.
     * @return A list of meta fields expected for the attachment.
     * @type Array
     */
    getExpectedMeta: function () {
        return this._expectedMeta;
    },
    
    /**
     * Set/Get the order for the attachment.
     * @param {Number} value (Optional) If undefined the current order value
     * for the attachment is returned, else the attachment's order value is 
     * set. Attachments are displayed in the order specified by their order
     * vale.
     * @return The order value.
     * @type Number
     */
    order: function (value) {
        if (value === undefined) {
            return this._order;
        }
        this.trigger('order', function () {
            this._order = value;
        }, {data: {'value': value}});
    },
    
    /**
     * Set/Get a meta value for the attachment.
     * @param {String} name The name of the meta attribute to return.
     * @param {Boolean|Number|String} value (Optional) If undefined the value
     * for the named attribute is returned, else the value is set.
     * @return The named meta attributes value.
     * @type Boolean|Number|String
     */
    meta: function (name, value) {
        if (value === undefined) {
            return this._meta[name];
        }
        this.trigger('meta', function () {
            if (value === null) {
                delete this._meta[name];
            } else {
                this._meta[name] = value;
            }
        }, {data: {'value': value}});       
    },
    
    /**
     * Get the names defined in the attachments meta data, the names are
     * sorted before being returned.
     * @return The names (keys) for the attachments meta data.
     * @type Object
     */
    getMetaNames: function () {  
        var user = DEPLOY.getAdministrator();
        var names = [];
        var hiddenNames = [];
        
        if (user.can('deploy-template', 'add')) {
            /* 
            Show extra meta information for people who can create templates, 
            such as DEVELOPERS 
            */
            hiddenNames = DEPLOY.Attachment.LESS_HIDDEN_META_NAMES;
        } else {
            /* Everyone else */
            hiddenNames = DEPLOY.Attachment.HIDDEN_META_NAMES;
        }
                
        for (var name in this._meta) {
            if (this._meta.hasOwnProperty(name)) {
                // Don't include hidden or names
                if (hiddenNames.indexOf(name) === -1) {
                    names.push(name);
                }
            }
        }
        var expectedMetaNames = this.getExpectedMetaNames();
        for (var i = 0; i < expectedMetaNames.length; i++) {
            var name = expectedMetaNames[i];
            
            // Don't add duplicate names
            if (names.indexOf(name) === -1) {
                names.push(name);
            }
        }
        names.sort();
        return names;
    },
    
    /**
     * Get the names defined in the expected meta data, the names are
     * sorted before being returned.
     * @return The names (keys) for the attachments expected meta data.
     * @type Object
     */
    getExpectedMetaNames: function () { 
        var names = [];
        if (this._expectedMeta !== undefined && this._expectedMeta !== null) {
            for (var i = 0; i < this._expectedMeta.length; i++) {
                names.push(this._expectedMeta[i][1]);
            }
        }
        names.sort();
        return names;
    },
    
    /**
     * Return true if the 'other' attachment is equal to this attachment.
     * @param {DEPLOY.Attachment} other The attachment to compare with.
     * @return Whether the 'other' attachment is the same as this attachment.
     * @type Boolean
     */
    equalTo: function (other) {
        
        /* Are the paths the same? */
        if (this._path !== other._path) {
            return false;
        }
        
        /* Are the orders the same? */
        if (this._order !== other._order) {
            return false;
        }        
        
        /* Are the thumbnail paths the same? */
        if (this._thumbnail_path !== other._thumbnail_path) {
            return false;
        }
        
        /* Are the meta values the same? */
        if (this.getMetaNames().length !== other.getMetaNames().length) {
            return false;
        }
        
        var metaNames = this.getMetaNames();
        for (var i = 0; i < metaNames.length; i++) {
            var metaName = metaNames[i];
            if (this._meta[metaName] !== other._meta[metaName]) {
                return false;
            }
        }
        
        /* NOTE: We don't care if expected meta is the same or not. */
        return true;
    },
    
    /**
     * Return a version of the attachment suitable for serializing as JSON.
     * @return An object representing the attachment suitable for serializing
     *         as JSON.
     * @type Object
     */
    toJSON: function () {
        return {path: this._path, thumbnail_path: this._thumbnail_path, meta: this._meta};
    },
    
    /**
     * Return a copy of the attachment.
     * @return A copy of the attachment.
     * @type DEPLOY.Attachment
     */
    copy: function () {
        return new DEPLOY.Attachment({
            path: this._path, 
            meta: $$.extend(true, {}, this._meta), 
            expectedMeta: this._expectedMeta.slice(0)
        });
    }
});


// Constants
DEPLOY.Attachment.HIDDEN_META_NAMES = ['height', 'inpage_size', 'size', 'variations', 'width'];
DEPLOY.Attachment.LESS_HIDDEN_META_NAMES = ['height', 'inpage_size', 'size', 'width'];
DEPLOY.Attachment.FIXED_META_NAMES = [
    'encoding', 
    'original_filename', 
    'original_height', 
    'original_width', 
    'size_mb', 
    'type',
    'variations'
    ];


/**
 * Default view for managing an attachments meta information. The attachment 
 * fields should bind to the attachments 'meta' value to be aware of changes
 * and perform saves.
 */
DEPLOY.Attachment.manageMeta = function (attachment) {
    var user = DEPLOY.getAdministrator();
    
    // Check if the meta manager is already open
    var existingWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
	if (existingWin) {
        // If the attachment is the same just bring it to the front
        if (existingWin.widgetData('attachment').getPath() === attachment.getPath()) {
			existingWin.minimize(false);
			existingWin.sendToFront();
			return;
		}
        // If the attachment is different then close the window now
		existingWin.destroy();
	}

    // Create the meta attachment window
    var win = new FLUID.Window({
        name: 'deploy-window-attachment-meta-manage',
        group: 'modal',
        label: 'Meta information for: ' + attachment.getIdentity(),
        size: [500, 400],
        minSize: [500, 400],
        maxSize: [500, 0],
        flags: 
            FLUID.Window.CAN_CLOSE |
            FLUID.Window.CAN_MINIMIZE |
            FLUID.Window.CAN_RESIZE |
            FLUID.Window.CAN_DRAG |
            FLUID.Window.HAS_ACTIONS
    });

    // Handle window events
    win.bind('afterCloseComplete', function(event, data) {
        var desktop = FLUID.widgetByName('desktop');
    
        // Remove the window
        this.destroy();
    
        // If there are still other windows then we should remain in modal
        // mode...
        var children = desktop.getChildren('modal');
        for (var i = 0; i < children.length; i++) {
            if (children[i].typeOf('Window')) {
                return;
            }
        }
        
        // ...else we can exit modal mode
        desktop.endModal(FLUID.Desktop.MODAL);
    });
    
    // Store a reference to the attachment against the window
    win.widgetData('attachment', attachment);
    
    // Create a popup menu we can use to provide editing options for the 
    // meta attribues.
    var attributeMenu = new FLUID.Menu();
    var changeItem = new FLUID.MenuItem({parent: attributeMenu, label: 'Change...'});
    var removeItem = new FLUID.MenuItem({parent: attributeMenu, label: 'Remove'});
    
    // Create a table and populate it with the current meta data
	var panel = new FLUID.Panel({parent: win});
	var dataTable = new FLUID.DataTable({
	    parent: panel,
	    classes: ['deploycms-meta-data']
	});
    
    // Store a reference to the attachment against the window
    win.widgetData('dataTable', dataTable);
    
    // Set-up the events for each cell 
    dataTable.bind('afterAddCell', function(event, data) {
        var cell = event.data.cell;
        
        if (DEPLOY.Attachment.FIXED_META_NAMES.indexOf(cell.data()) > -1) {
            cell.enable(false);
        }
        
        // Hover
        cell.getParent().unbind('onMouseEnter.fluid');
        cell.bind('onMouseEnter.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.getParent().addClass(FLUID._cssName('over'));
            }
        });
        cell.getParent().unbind('onMouseLeave.fluid');
        cell.bind('onMouseLeave.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.getParent().removeClass(FLUID._cssName('over'));
            }
        });        
        
        // Click
        cell.bind('onClick.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                var menu = data.attributeMenu;
                menu.owner(this);
                
                // Reset the events for each item in the menu
                
                // Update
                menu.getChildren()[0].unbind('onClick.deploy');
                menu.getChildren()[0].bind('onClick.deploy', function (event, data) {
                    DEPLOY.Attachment.updateMeta(data.attachment, data.name);
                }, {attachment: attachment, name: this.data()});
                
                // Remove
                menu.getChildren()[1].unbind('onClick.deploy');
                menu.getChildren()[1].bind('onClick.deploy', function (event, data) {
                    data.attachment.meta(data.name, null);
                    DEPLOY.Attachment.manageMeta._refresh(data.attachment);
                }, {attachment: attachment, name: this.data()});
                
                // Enable/Disable remove based on expected attribute 
                menu.getChildren()[1].enable(true);
                if (attachment.getExpectedMetaNames().indexOf(this.data()) > -1) {
                    menu.getChildren()[1].enable(false);
                }
                
                menu.style({
                    left: event.data.fwEvent.pageX - $$(document).scrollLeft(),
                    top: event.data.fwEvent.pageY - $$(document).scrollTop()
                    });
                                
                menu.popup();
            }
        }, {attributeMenu: data.attributeMenu, attachment: attachment});        
                  
    }, {attributeMenu: attributeMenu, attachment: attachment});

    // Add the header
    dataTable.header([
        {label: 'Name'},
        {label: 'Value'}
    ]);
    
    // Add the meta data
    var rows = [];
    var names = attachment.getMetaNames();
    for (var i = 0; i < names.length; i++) {
        var name = names[i];
        var value = attachment.meta(name);
        
        /* Special case variables */
        if (name === 'variations') {
            var variations = [];
            for (var property in value) {
                if (value.hasOwnProperty(property)) {
                    variations.push(property + '[' + value[property].args.join(', ') + ']');
                }
            }
            if (variations.length > 0) {
                value = variations.join('; ') + ';';
            } else {
                value = '';
            }
        }
        
        rows.push([
            {value: name, data: name},
            {value: value, data: name} 
        ]);
    }
    dataTable.data(rows);
    
    // Add the 'Add attribute' button
    var addBtn = new FLUID.Button({
        parent: win,
        group: 'actions',
        icon: 'deploy.icons.views.add',
        label: _('Add attribute')
    });
    addBtn.bind('onClick', function (event, data) { 
        DEPLOY.Attachment.addMeta(data.attachment);
    }, {attachment: attachment});
    
    // Prepare and open the window
    win.refresh();
    win.prepare(function (win) { 
        win.center();
    	win.sendToFront();
		win.getChildren()[0].style({
            height: function (body) {
                if (!body.exists()) {
                    return;
                }
                var win = body.getParent();
                var winHTML = win.getHTMLSelection();
                var height = winHTML.height();

                // Allow for title
                height -= winHTML.children('.fluid-title').outerHeight(true);

                // Allow for info & action areas
                if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
                    height -= winHTML.children('.fluid-info').outerHeight(true);
                }
                if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
                    height -= winHTML.children('.fluid-actions').outerHeight(true);
                }

                // Allow for padding on client
                height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());

                return height;
            }
        });	
    });
    win.open();    
}

/** @private */
DEPLOY.Attachment.manageMeta._refresh = function (attachment) {
    var existingWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
    if (existingWin !== undefined) {
        var dataTable = existingWin.widgetData('dataTable');
        var rows = [];
        var names = attachment.getMetaNames();
        for (var i = 0; i < names.length; i++) {
            var name = names[i];
            var value = attachment.meta(name);
            rows.push([
                {value: name, data: name},
                {value: value, data: name} 
            ]);
        }
        dataTable.data(rows);
    }
    dataTable.refresh();
}

/**
 * Default view for adding a meta value to an attachment.
 */
DEPLOY.Attachment.addMeta = function (attachment) {
	var desktop = FLUID.widgetByName('desktop');
    
    // Enter *single* model state
    desktop.startModal(FLUID.Desktop.SINGLE_MODAL);
    desktop.bind('onEndModal.deploy', function (event) {
		event.stopDefault();
	});
	desktop.addClass('fluid-frozen');

    // Create the add window
    var win = new FLUID.Window({
        name: 'deploy-window-attachment-meta-add',
        group: 'singleModal',
        label: _('Add meta attribute'),
        size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(),
        flags: 
			FLUID.Window.HAS_ACTIONS |
            FLUID.Window.CAN_CLOSE |
            FLUID.Window.CENTERED
    });

    // Handle closing the window
    win.unbind('afterCloseComplete');
    win.bind('afterCloseComplete', function(event, data) {
		this.destroy();
		FLUID.widgetByName('desktop').unbind('onEndModal.deploy');
		FLUID.widgetByName('desktop').endModal(FLUID.Desktop.SINGLE_MODAL);
		FLUID.widgetByName('desktop').removeClass('fluid-frozen');
    });

    // Store a reference to the attachment against the window
    win.widgetData('attachment', attachment);    
    
    // Create the add form
    
    // Add the add button
    var form = new FLUID.Form({parent: win}); 
    var fieldset = new FLUID.Fieldset({parent: form})
    var nameField = new FLUID.Field.Text({
        parent: fieldset, 
        name: 'name',
        label: 'Name', 
        realtime: false,
        required: true,
        classes: ['fluid-split']
        })
    var valueField = new FLUID.Field.Text({
        parent: fieldset, 
        name: 'value', 
        label: 'Value', 
        realtime: false,
        required: false,
        classes: ['fluid-split', 'fluid-last']
        })
    
	// Add 'Add' button
	var addBtn = new FLUID.Button({
    	parent: win,
		group: 'actions',
    	label: _('Add'),
    	classes: ['fluid-sep']
	});
    addBtn.bind('onClick', function (event, data) { 
        if (this.enabled()) {
            data.form.send();
        }
    }, {form: form});

    // Handle add event on form
    form.bind('onSend.deploy', function (event, data) {
        event.stopDefault();
        
        var user = DEPLOY.getAdministrator();
        var formData = this.getData();
        var hiddenNames = [];
        
        if (user.can('deploy-template', 'add')) {
            /* 
            Show extra meta information for people who can create templates, 
            such as DEVELOPERS 
            */
            hiddenNames = DEPLOY.Attachment.LESS_HIDDEN_META_NAMES;
        } else {
            /* Everyone else */
            hiddenNames = DEPLOY.Attachment.HIDDEN_META_NAMES;
        }
        
        // Validate value has been specified
        if (formData['name'] === undefined) {
            return;
        }
        
        // Validate name does not already exist
        if (
            data.attachment.getMetaNames().indexOf(formData['name']) > -1 ||
            hiddenNames.indexOf(formData['name']) > -1
        ) {
            this.getFieldMap()['name'].stateMessage('This name is either already taken or resevered.');
            this.getFieldMap()['name'].state(FLUID.Field.INVALID_STATE);
            return;
        }
        
        // Validate value has been specified
        if (formData['value'] === undefined) {
            return;
        }
        
        // Close the window
        win.close();
        
        // Add the attribute and close the window
        attachment.meta(formData['name'], formData['value']);
        
        // Update the meta information
        DEPLOY.Attachment.manageMeta._refresh(data.attachment);
        
    }, {attachment: attachment});

    win.refresh();
	win.prepare();
    win.open();
}


/**
 * Default view for updating a meta value for an attachment.
 */
DEPLOY.Attachment.updateMeta = function (attachment, name) {
	var desktop = FLUID.widgetByName('desktop');
    
    // Enter *single* model state
    desktop.startModal(FLUID.Desktop.SINGLE_MODAL);
    desktop.bind('onEndModal.deploy', function (event) {
		event.stopDefault();
	});
	desktop.addClass('fluid-frozen');

    // Create the add window
    var win = new FLUID.Window({
        name: 'deploy-window-attachment-meta-update',
        group: 'singleModal',
        label: _('Update meta attribute'),
        size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(),
        flags: 
			FLUID.Window.HAS_ACTIONS |
            FLUID.Window.CAN_CLOSE |
            FLUID.Window.CENTERED
    });

    // Handle closing the window
    win.unbind('afterCloseComplete');
    win.bind('afterCloseComplete', function(event, data) {
		this.destroy();
		FLUID.widgetByName('desktop').unbind('onEndModal.deploy');
		FLUID.widgetByName('desktop').endModal(FLUID.Desktop.SINGLE_MODAL);
		FLUID.widgetByName('desktop').removeClass('fluid-frozen');
    });

    // Store a reference to the attachment against the window
    win.widgetData('attachment', attachment);    
    
    // Create the add form
    
    // Add the add button
    var form = new FLUID.Form({parent: win}); 
    var fieldset = new FLUID.Fieldset({parent: form})
    var nameField = new FLUID.Field.DisplayOnly({
        parent: fieldset, 
        name: 'name', 
        value: name,
        label: 'Name', 
        realtime: false,
        required: true,
        classes: ['fluid-split']
        })
    var valueField = new FLUID.Field.Text({
        parent: fieldset, 
        name: 'value',
        value: attachment.meta(name),
        label: 'Value', 
        realtime: false,
        required: false,
        classes: ['fluid-split', 'fluid-last']
        })
    
	// Add 'Update' button
	var addBtn = new FLUID.Button({
    	parent: win,
		group: 'actions',
    	label: _('Update'),
    	classes: ['fluid-sep']
	});
    addBtn.bind('onClick', function (event, data) { 
        if (this.enabled()) {
            data.form.send();
        }
    }, {form: form});

    // Handle update event on form
    form.bind('onSend.deploy', function (event, data) {
        event.stopDefault();
        
        var formData = this.getData();
        
        // Validate value has been specified
        if (formData['value'] === undefined) {
            return;
        }
        
        // Close the window
        win.close();
        
        // Add the attribute and close the window
        attachment.meta(data.name, formData['value']);
        
        // Update the meta information
        DEPLOY.Attachment.manageMeta._refresh(data.attachment);
        
    }, {attachment: attachment, name: name});

    win.refresh();
	win.prepare();
    win.open();
}


/**
 * A mixin class to provide custom realtime interaction for attachments.
 * @class A mixin for attachment fields to provide realtime support.
 * @constructor.
 */
DEPLOY.AttachmentRealtime = function () {};

$$.extend(DEPLOY.AttachmentRealtime.prototype, {

    /** @private */
    _inactRealtime: function () {

        // Set the field status to checking        
        this.state(FLUID.Field.CHECKING_STATE);
    
        // Perform the simple required tests first
    
        if (this._required && this.empty()) {
    
            // Required but no value provided
            this.state(FLUID.Field.INVALID_STATE);
            this.stateMessage('This field is required.');
            this.trigger('inactRealtimeComplete', function (event) { 
            
            });
    
        } else if (!this._required && this.empty() && (!this.realtime() || this.getForm().realtimeData()._form !== 'UPDATE' || this.getForm().realtimeData()._action !== 'SAVE')) {
    	
            // Optional condition met
            this.state(FLUID.Field.VALID_STATE);
            this.stateMessage('');
            this.trigger('inactRealtimeComplete', function (event) {
            
            });
    
        } else if (this.realtime()) {
        
            // Real-time validation
        
            // Build the request
            var requestUrl = this.getForm().realtimeURL();
            var requestData = $$.extend({}, 
                this.getForm().realtimeData());
            requestData['_name'] = this._name;
    		if (this.value() === undefined || this.value() === null) {
            	requestData[this._name] = '';
    		} else {
    			requestData[this._name] = this.value();    
    		}
            
            // Cater for linked fields
            var linkedFields = this.getLinkedFields();
            for (var i = 0; i < linkedFields.length; i++) {
                var linkedField = linkedFields[i];
            
                // If the linked field is not a hidden field then it must be 
                // ready or we have to set the results to pending.
                if (
                    !linkedField.typeOf('Field.Hidden') && 
                    (
                        linkedField.state() === FLUID.Field.INITIAL_STATE &&
                        linkedField.value() === ''
                    )
                ) {
                    this.state(FLUID.Field.PENDING_STATE);
                    return;
                }
            
    			if (linkedField.value() === undefined || linkedField.value() === null) {
                	requestData[linkedField.getName()] = '';
    			} else {
    				requestData[linkedField.getName()] = linkedField.value();   
    			}
            }
        
            var request = new IO.Request({type: 'POST', 
                url: requestUrl,
                data: requestData});
        
            // Handle responses
            request.bind('afterSuccess', function (event, data) {
            
                var field = data.field;
                var code = event.data.data.code;
                var msg = event.data.data.msg;
            
                // Validated
                if (code === 'VALID' || code === 'SAVED') {
                    field.state(FLUID.Field.VALID_STATE);
               
                    // Mark any linked fields as valid
                    var linkedFields = field.getLinkedFields();
                    for (var i = 0; i < linkedFields.length; i++) {
                        var linkedField = linkedFields[i];
                        linkedField.state(FLUID.Field.VALID_STATE);
                        linkedField.stateMessage('');
                    }
                
                    // Update a saved value
                    if (code === 'SAVED') {
                        field._syncRealtime(event.data.data.clean[field.getName()]);
                    }
                    
                    // Clear the msg
                    msg = '';
				
                } else if (code === 'PENDING') {
                   field.state(FLUID.Field.PENDING_STATE);
                }
            
                field.stateMessage(msg);
                field.trigger('inactRealtimeComplete', function (event) {
                
                });
    			if (code === 'SAVED') {
    				field.getForm().trigger('realtimeSendSuccess', field.getForm().realtimeSendSuccess, event);
    			}
            
            }, {field: this} );
        
            request.bind('afterFail', function (event, data) {
            
                var field = data.field;
                var status = event.data.data.status;
                var msg = event.data.data.msg;
            
                // Failed to validate
                field.state(FLUID.Field.INVALID_STATE);
                field.stateMessage(msg);
            
               // Mark any linked fields as pending
               var linkedFields = field.getLinkedFields();
               for (var i = 0; i < linkedFields.length; i++) {
                   var linkedField = linkedFields[i];
                   linkedField.state(FLUID.Field.PENDING_STATE);
               }                
            
                field.trigger('inactRealtimeComplete', function (event) {
                
                });
                        
            }, {field: this} );
        
            // Validate
            request.send();
        
        } else {
        
            // Without real-time validation the best we can establish is that
            // the field is required to contain some content and does. It is 
            // valid pending server-side validation.
        
            this.state(FLUID.Field.PENDING_STATE);
            this.stateMessage('');
            this.trigger('inactRealtimeComplete', function (ev) { });
        }
    }
});


/**
 * A field for attaching a file.
 * @class A single attachment field.
 * @constructor.
 */
DEPLOY.SingleAttachmentField = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Field.Entry.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('DEPLOY.SingleAttachmentField');
    
    // Groups
    this._childGroups[this.__defaultClient__] = [];

    /** @private */
    this._controller = options.controller;

    /** @private */
    this._help = options.help;
    
    /** @private */
    this._attachment = options.attachment;
    if (this._attachment !== null) {
        this.value(JSON.stringify([this._attachment]));
        this._attachment.bind('afterMeta.deploy', function (event, data) {
            data.field.value(JSON.stringify([this]));
            data.field.inactRealtime();
        }, {field: this});    
    }
    
    /** @private */
    this._initialAttachment = null;
    if (options.attachment !== null) {
        //this._initialAttachment = $$.extend(true, {}, options.attachment); // @@ I don't think this is safe
    }
    
    /** @private */
    this._uploader = null;

    /** @private */
    this._uploading = false;

    /**
     * The buttons used to manage the attachment.
     */
    this.buttons = {
        'cancel': new FLUID.Button({parent: this, icon: 'deploy.icons.views.cancel', visible: false, enbaled: false}),
        'manage': new FLUID.Button({parent: this, label: _('Manage'), classes: ['fluid-sep'], visible: false, enbaled: false}),
        'remove': new FLUID.Button({parent: this, label: _('Remove'), visible: false, enbaled: false})
    };

    // Manage
    this.buttons['manage'].bind('onClick', function (event, data) { 
        if (this.enabled() && data.field.attachment() !== null) { 
            DEPLOY.Attachment.manageMeta(data.field.attachment());
        }
    }, {field: this});
    
    // Remove
    this.buttons['remove'].bind('onClick', function (event, data) { 
        if (this.enabled() && data.field.attachment() !== null) {
            var dlg = new DEPLOY.ConfirmDialog({
                name: 'deploy-window-attachment-remove',
                label: _('Are you sure you wish to delete this attachment?'),
                message: _(
                    'Please confirm you would like to delete - <strong>#ATTACHMENT#</strong> - by selecting <strong>Confirm</strong> below.'
                    ).replace('#ATTACHMENT#', data.field.attachment().getPath())
                });

            dlg.bind('onConfirm.deploy', function (event, data) { 
                data.field.state(FLUID.Field.INITIAL_STATE);
                data.field.attachment(null);
            }, {field: data.field});
            
            dlg.launch();
        }
    }, {field: this});
    
    // Bind events for the widget
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(DEPLOY.SingleAttachmentField, FLUID.Field.Entry);
UTILS.copyPrototype(DEPLOY.SingleAttachmentField, FLUID.Field.Realtime);
UTILS.copyPrototype(DEPLOY.SingleAttachmentField, DEPLOY.AttachmentRealtime);

$$.extend(DEPLOY.SingleAttachmentField.prototype, {
    __className__: 'DEPLOY.SingleAttachmentField',
    __css__: FLUID._cssName('field-single-attachment'),
    __defaultClient__: 'tools',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' + 
            '<input type="hidden" name="<%=widget.getName()%>" value="<%=widget.value()%>" />' +
            '<div class="fluid-attachment-area">' + 
                '<div class="fluid-attachment">' +  
                    '<div class="fluid-progress-bar">' +
                        '<div class="fluid-progress"></div>' + 
                    '</div>' +
                    '<div class="fluid-thumbnail"></div>' +
                    '<div class="fluid-preview">' +
                    '</div>' + 
                    '<div class="fluid-info">' +
                        '<span class="fluid-name fluid-empty"></span>' +
                        '<span class="fluid-file-info"></span>' +
                    '</div>' + 
                    '<div class="fluid-tools <%=FLUID_GROUP_PREFIX%>tools">' + 
                        '<%=group_tools%>' +
                    '</div>' +
                '</div>' + 
            '</div>' +  
            '<div class="fluid-attachment-note">' + 
                '<div class="fluid-help"><%=widget._help%></div>' +
                '<div class="fluid-status"></div>' +
            '</div>' +
        '</div>',

    /**
     * Set/Get the attachment for the field.
     * @param {Number} value (Optional) If undefined the current attachment
     * for the field is returned, else the field's attachment is set.
     * @return The attachment.
     * @type DEPLOY.Attachment
     */
    attachment: function (value) {
        if (value === undefined) {
            return this._attachment;
        }
        this.trigger('attachment', function () {
            this._attachment = value;
            if (this._attachment !== null) {
                this._attachment.unbind('afterMeta.deploy');
                this._attachment.bind('afterMeta.deploy', function (event, data) {
                    data.field.value(JSON.stringify([this]));
                    data.field.inactRealtime();
                }, {field: this});
                this.value(JSON.stringify([this._attachment]));
            } else {
                this.value('');
            }
            this._update();
        }, {data: {'value': value}});
    },
    
    /** @private */
    _createUploader: function () {
        this._uploader = new qq.FileUploader({
            // Keep a reference to this field
            field: this,
           
            // Drag 'n' Drop is done against the fields preview area
            element: this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-preview')[0],
           
            // Setup the URL
            action: IO.Request.buildURL(this._controller.getName(), 'upload'),
           
            // Add the @ param (ideally this should change each time but it's better than nothing)
            params: {'@':  IO.Request.getId(), '_name': this.getName()},
        
            /**
             * Handle a file submission.
             */
            onSubmit: function(id, fileName) {
        
                // Set the field as state uploading
                this.field._uploading = true;

                // Let the user know which file we're uploading
                var attachmentInfoSelection = this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-info');
                attachmentInfoSelection.children('.fluid-name').html('Uploading ' + fileName);
                attachmentInfoSelection.children('.fluid-name').removeClass('fluid-empty');

                // Reset and show the progress bar
                this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-progress-bar').show();
                var progressSelection = this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-progress-bar > .fluid-progress');
                progressSelection.css('width', '0%');
                progressSelection.attr('title', '0%');

                // Hide the uploader to ensure the user can't attempt to upload another file
                this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-preview > .fluid-uploader').hide();

                // Give the user the option to cancel their upload
                this.field.buttons['cancel'].unbind('onClick.deploy');
                this.field.buttons['cancel'].bind('onClick.deploy', function (event, data) { 
                    if (this.enabled()) {
                        data.uploader.handler.cancel(data.id);
                        data.uploader.onComplete(data.id, data.fileName, {status: 'error', data: {msg: _('Your upload has been cancelled')}});
                    }
                }, {field: this.field, uploader: this, id: id, fileName: fileName});
                this.field.buttons['cancel'].enable();
                this.field.buttons['cancel'].show();
            },
            
            /**
             * Handle the file upload progress.
             */
            onProgress: function(id, loaded, total) {
                // Update the progress bar
                var progressSelection = this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-progress-bar > .fluid-progress');
                var progress = String(parseInt(loaded / total * 100)) + '%';
                progressSelection.css('width', progress);
                progressSelection.attr('title', progress);
            },
        
            /**
             * Handle a completed upload.
             */
            onComplete: function(id, fileName, response) {
                
                // Set the state to no longer be marked as uploading
                this.field._uploading = false;
        
                // Hide the progress bar
                this.field.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-progress-bar').hide();
                
                // Remove the cancel button
                this.field.buttons['cancel'].enable(false);
                this.field.buttons['cancel'].hide();
            
                // Handle the response
                if (response.status === 'success') {
                    
                    // Success
                    
                    // Create the attachment
                    var attachment = new DEPLOY.Attachment({
                        path: response.data.path,
                        thumbnail_path: response.data.thumbnail_path, 
                        meta: response.data.meta,
                        expectedMeta: response.data.expected_meta
                        });
                
                    // Set the attachment for the field
                    this.field.attachment(attachment);
                    
                    // Update the field to reflect the new attachment
                    this.field._update();
                } else {
                    
                    // Fail/Error
                    
                    // Notify the user of the issue
                    FLUID.widgetByName('desktop').notify(response.data.msg);
                
                    // Set the error for the field
                    this.field.stateMessage(response.data.msg);
                    this.field.state(FLUID.Field.INVALID_STATE);
                    
                    // Update the field to reflect the failed upload
                    this.field._update();
                }
            }
        });   
    },
   
    /** @private */
   _reset: function () {
        this.attachment(this._initialAttachment);
        this.state(FLUID.Field.INITIAL_STATE);
    },

    /** @private */
    _refresh: function () {
        return FLUID.Field.Entry.prototype._refresh.apply(this);
        
        // Remove any previous references to the fields uploader
        this._uploader = null;
    },
   
    _update: function () {
        var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        var desktop = FLUID.widgetByName('desktop'); 
        if (!this.exists()) {
            return;
        }
        
        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Field.Entry.prototype._update.apply(this, [false]);
        
        // Create the fields uploader if one doesn't exist
    
        if (this._uploader == null) {    
            this._createUploader();
        }
    
        // Update the field to reflect any associated attachment activity
        if (!this._uploading) {
        
            var attachmentInfoSelection = this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-info');
            
			// Remove any mimetype classes
			this.removeClass('fluid-no-thumb');
			for (var i = 0; i < this._classes.length; i++) {
				var cssClass = this._classes[i];
				if (cssClass.indexOf('fluid-mimetype-') === 0) {
					this.removeClass(cssClass);
				}
			}

            if (this.attachment() === null || this.attachment() === undefined) {
                
                // No attachment
                
    			// Show the uploader to allow the user to upload another file
    			this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-preview > .fluid-uploader').show();
			
    			// Remove any thumbnail
    			this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-thumbnail').css('background-image', 'none');
			    
    			// Clear the info
    			attachmentInfoSelection.children('.fluid-name').html(_('No file attached'));
    			attachmentInfoSelection.children('.fluid-name').addClass('fluid-empty');
    			attachmentInfoSelection.children('.fluid-file-info').html('');
	
    			// Hide the manage and remove tools
    			this.buttons['manage'].enable(false);
    			this.buttons['manage'].hide();
    			this.buttons['remove'].enable(false);
    			this.buttons['remove'].hide();        
    			
            } else {
            
                // Has attachment
                
                // Hide the uploader to ensure the user can't attempt to upload another file
				this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-preview > .fluid-uploader').hide();
				
				if (this.attachment().getThumbnailPath() === undefined || this.attachment().getThumbnailPath() === null) {
				
					// Show a mimetype icon
    				this.addClass('fluid-no-thumb');
    				if (this.attachment().meta('type') !== undefined && this.attachment().meta('type') !== null) {
    				    this.addClass('fluid-mimetype-' + this.attachment().meta('type').replace('/', '-'));
    			    }
					this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-thumbnail').css('background-image', 'none');
					
				} else {
				    
					// Show thumbnail
					this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-thumbnail').css('background-image', 'url(' + this.attachment().getThumbnailPath() + ')');
				}
				
				// Add info
				attachmentInfoSelection.children('.fluid-name').html(this.attachment().meta('original_filename') || this.attachment().getPath());
				attachmentInfoSelection.children('.fluid-name').removeClass('fluid-empty');
				attachmentInfoSelection.children('.fluid-file-info').html(this.attachment().meta('size_mb'));
			
				// Show the remove and manage buttons
				this.buttons['manage'].enable();
				this.buttons['manage'].show();
				this.buttons['remove'].enable();
				this.buttons['remove'].show();
			}
        }

        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);          
    },
   
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
        this.bind('afterAttachment.fluid', function(event) {
            // Attempt to save/validate whenever the attachment is set
            this.inactRealtime();
        });
    },
    
    /** @private */
    _syncValue: function () {
        // Pass
    },
    
    /** @private */
    _syncRealtime: function (attachments_data) {
        this._attachment = null;
        if (attachments_data.length > 0) {
            var attachment = new DEPLOY.Attachment({
    			path: attachments_data[0].path,
    			thumbnail_path: attachments_data[0].thumbnail_path,
    			meta: attachments_data[0].meta,
    			expectedMeta: attachments_data[0].expected_meta
    			});
            this._attachment = attachment;
            this._attachment.unbind('afterMeta.deploy');
            this._attachment.bind('afterMeta.deploy', function (event, data) {
                data.field.value(JSON.stringify([this]));
                data.field.inactRealtime();
            }, {field: this});
            this._setValue(JSON.stringify([this._attachment]));
        } else {
            this._setValue('');
        }
        this._update();
    },
    
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: '',
        attachment: null
    } 
});


/*
 ------------------------------------------------------------------------------
 Multiple attachments, how not to spend 6 months of your life and various other
 insights.
 ------------------------------------------------------------------------------
 
 Unlike Single attachments, Multiple attachments are managaed by 3 separate
 classes. 
 
 The field class is responsible for the storage of attachment data and sending 
 and receiving to/from the server. The feild is a hidden field and has no 
 visible output.
 
 The manager class is responsible for managing the various handler classes 
 which get spawned to handle individual attachments. The manager class also
 provides validation feedback, the help, browse, drag'n'drop and attachment
 viewport.
 
 Handlers are spawned by the manager to handle individual attachments. They are
 responsible for displaying the attachment information and allowing the user to
 manage attachment meta data and remove attachments.
 
*/

/**
 * A field for attaching multiple files.
 * @class A multiple attachment field.
 * @constructor.
 */
DEPLOY.MultipleAttachmentField = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    FLUID.Field.Hidden.call(this, options, false);
    FLUID.Field.Realtime.call(this, options);
    this._type.push('DEPLOY.MultipleAttachmentField');

	/** @private */
	this._controller = options.controller;

	/** @private */
	this._label = options.label;
    
	/** @private */
	this._help = options.help;

	/** @private */
	this._manager = null; // Auto-assigned by the manager instance
	
	/** @private */
	this._attachments = options.attachments || []; 
	if (this._attachments.length !== 0) {
        this.value(JSON.stringify(this._attachments));
        for (var i = 0; i < this._attachments.length; i++) {
            this._attachments[i].unbind('afterMeta.deploy');
            this._attachments[i].bind('afterMeta.deploy', function (event, data) {
                data.field.value(JSON.stringify(data.field.attachments()));
                data.field.inactRealtime();
            }, {field: this});
        }
    }

    /** @private */
    this._attachment_max = options.attachment_max || 0;

	/** @private */
	this._initialAttachments = [];
	if (options.attachments !== null) {
		this._initialAttachments = []; // @@ Deep copy here? Probably always needs to hard reset
	}

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(DEPLOY.MultipleAttachmentField, FLUID.Field.Hidden);
UTILS.copyPrototype(DEPLOY.MultipleAttachmentField, FLUID.Field.Realtime);
UTILS.copyPrototype(DEPLOY.MultipleAttachmentField, DEPLOY.AttachmentRealtime);

$$.extend(DEPLOY.MultipleAttachmentField.prototype, {
    __className__: 'DEPLOY.MultipleAttachmentField',
	__css__: FLUID._cssName('field-multiple-attachment'),
	
    /**
     * Set/Get the label for the field.
     * @param {String} value (Optional) If undefined the current label for
     * the field is returned, else the fields's label is set.
     * @return The label.
     * @type String
     */
    label: function (value) {
        if (value === undefined) {
            return this._label;
        }
        this.trigger('label', function () {
            if (this._label !== value) {
                this._label = value;
            }
        }, {data: {'value': value}});
    },

	/**
     * Set/Get the help for the field.
     * @param {String} value (Optional) If undefined the current help for the
 	 * field is returned, else the field's help is set.
     * @return The fields help.
     * @type String
     */
    help: function (value) {
        if (value === undefined) {
            return this._help;
        }
        this.trigger('help', function () {
            this._help = value;
        }, {data: {'value': value}});
    },

    /**
     * Set/Get the maximum attachments for the field.
     * @param {Number} value (Optional) If undefined the current maximum for
     * the field is returned, else the fields's maximum is set.
     * @return The maximum number of attachments.
     * @type Number
     */
    attachment_max: function (value) {
        if (value === undefined) {
            return this._attachment_max;
        }
        this.trigger('label', function () {
            if (this._attachment_max !== value) {
                this._attachment_max = value;
            }
        }, {data: {'value': value}});
    },

    /**
     * Set/Get the attachments for the field.
     * @param {Number} value (Optional) If undefined the current attachments
     * for the field are returned, else the field's attachments are set.
     * @return The attachments.
     * @type Array
     */
    attachments: function (value) {
        if (value === undefined) {
            return this._attachments;
        }
        this.trigger('attachments', function () {
            this._attachments = value;

            for (var i = 0; i < this._attachments.length; i++) {
                var attachment = this._attachments[i];
                
                // Handle meta updates
                attachment.unbind('afterMeta.deploy');
                attachment.bind('afterMeta.deploy', function (event, data) {
                    data.field.value(JSON.stringify(data.field.attachments()));
                    data.field.inactRealtime();
                }, {field: this});
            }

            this.value(JSON.stringify(this._attachments));
        }, {data: {'value': value}});
    },
    
     /** @private */
    _reset: function () {
         this.attachments(this._initialAttachments);
         this.state(FLUID.Field.INITIAL_STATE);
     },    
    
    /** @private */
    _syncValue: function () {
        // Pass
    },
    
    /** @private */
    _bindEvents: function () {
        FLUID.Widget.prototype._bindEvents.apply(this);
        
        this.bind('afterAttachments.fluid', function(event) {
            // Attempt to save/validate whenever the attachments are set
            this.inactRealtime();
        });
    },
    
    /** @private */
    _syncRealtime: function (attachments_data) {
        var attachments = [];
        var attachment = null;
        for (var i = 0; i < attachments_data.length; i ++) {
            attachment = new DEPLOY.Attachment({
    			path: attachments_data[i].path,
    			thumbnail_path: attachments_data[i].thumbnail_path,
    			meta: attachments_data[i].meta,
    			expectedMeta: attachments_data[i].expected_meta
    			});
    		attachments.push(attachment);
        }
        this._attachments = attachments;
        var handlers = this._manager.getChildren('attachments');

        for (var j = 0; j < this._attachments.length; j++) {
            attachment = this._attachments[j];
    		handlers[j].attachment(attachment);
            
            // Handle meta updates
            attachment.unbind('afterMeta.deploy');
            attachment.bind('afterMeta.deploy', function (event, data) {
                data.field.value(JSON.stringify(data.field.attachments()));
                data.field.inactRealtime();
            }, {field: this});
        }

        this._setValue(JSON.stringify(this._attachments));
    },    
    
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: ''
    }
});


/**
 * A class for managing a multiple attachments.
 * @class A multiple attachments manager.
 * @constructor.
 */
DEPLOY.MultipleAttachmentManager = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('DEPLOY.MultipleAttachmentManager');
	
	// Define attachments as the child group
    this._childGroups['attachments'] = [];    
	
    // Add the additional CSS class to child classes
    this._classes.push(FLUID._cssName('field'));

	/** @private */ 
	this._field = options.field;
	
	/* Link the field */
	this._field._manager = this;
	
	/** @private */
	this._handlerPool = {};
	
	// Create any existing attachment items
	var existingAttachments = this._field.attachments();
	for (var i = 0; i < existingAttachments.length; i++) {
		new DEPLOY.MultipleAttachmentHandler({
			parent: this,
			group: 'attachments',
			attachment: this._field._attachments[i]
			});
	}
	
	// Capture any state changes that occur to the field and relay them to
	// the managers status.
	this._field.bind('afterState', function (event, data) {
        data.manager._setState(this.state());
    }, {manager: this});
	
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(DEPLOY.MultipleAttachmentManager, FLUID.Widget);

$$.extend(DEPLOY.MultipleAttachmentManager.prototype, {
    __className__: 'DEPLOY.MultipleAttachmentManager',
	__css__: FLUID._cssName('field-multiple-attachment'),
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
            '<div class="fluid-attachment-note">' + 
                '<div class="fluid-help"><%=widget.getField().help()%></div>' +                        
                '<div class="fluid-tools"></div>' +
                '<div class="fluid-status"></div>' +
            '</div>' +
			'<div class="fluid-attachment-area fluid-attachments-client">' +
                '<%=group_attachments%>' + 
            '</div>' + 
        '</div>',

	/**
	 * Get a list of the attachments the manager is responsible for.
	 * @return The manager's attachments.
	 * @type Array
	 */
	getAttachments: function () {
		var attachments = [];
		var handlers = this.getChildren('attachments');
		for (var i = 0; i < handlers.length; i++) {
			if (handlers[i].attachment() !== null) {
				attachments.push(handlers[i].attachment());
			}
		}
		return attachments;
	},

    /**
     * Reset the manager.
     */
    reset: function () {
        var children = this.getChildren('attachments');
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            child.destroy();
        }
    },

	/** 
	 * Sync the manager with the field.
	 */
	fieldSync: function () {
	    // Only sync the field if there are no other attachments currently
	    //loading.
	    for (var id in this._handlerPool) {
            if (this._handlerPool.hasOwnProperty(id)) {
                return;
            }
        }
		this._field.attachments(this.getAttachments());
	},

	/** @private */
	_spawnHandler: function (id, fileName) {
		var handler = new DEPLOY.MultipleAttachmentHandler({
			parent: this,
			group: 'attachments',
			fileId: id,
			fileName: fileName
			});
		this._handlerPool[id] = handler;
		this.refresh();
	},

	/** @private */
	_refresh: function () {
		this._uploader = null;
		return FLUID.Widget.prototype._refresh.apply(this);
	},

	/** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        var desktop = FLUID.widgetByName('desktop'); 
		
        if (!this.exists()) {
            return;
        }

		if (this._uploader == null) {
			this._uploader = new qq.FileUploader({
				field: this,
			    element: this.getHTMLSelection().find('.fluid-tools')[0],
				action: IO.Request.buildURL(this.getField()._controller.getName(), 'upload'), // @@ getController method required for fields
				params: {'@':  IO.Request.getId(), '_name': this.getField().getName()},
		        onSubmit: function(id, fileName) {
		            // Has the maximum already been reached?
		            if (this.field.getAttachments().length >= this.field.getField().attachment_max()) {
	                    FLUID.widgetByName('desktop').notify(_('The maximum number of attachments has already been uploaded'));
	                    return;
	                }
		            
					// Create a multiple attachment widget to handle each of the uploaded files
					this.field._spawnHandler(id, fileName);
					
                    // Give the user the option to cancel their upload
                    var handler = this.field._handlerPool[id];
                    handler.buttons['cancel'].unbind('onClick.deploy');
                    handler.buttons['cancel'].bind('onClick.deploy', function (event, data) { 
                        if (this.enabled()) {
                            data.uploader.handler.cancel(data.id);
                            data.handler.destroy();
                            data.field.fieldSync();
                            data.uploader.onComplete(data.id, data.fileName, {status: 'error', data: {msg: _('Your upload has been cancelled')}});
                        }
                    }, {field: this.field, handler: handler, uploader: this, id: id, fileName: fileName});
                    
				},
		        onProgress: function(id, loaded, total) {
	                // Can be undefined can if the upload was prevented by the maximum rule
		            if (this.field._handlerPool[id] !== undefined) {
					    this.field._handlerPool[id].updateProgress(loaded, total);
			        }
				},
		        onComplete: function(id, fileName, response) {
					var handler = this.field._handlerPool[id];
					
    	            // Can be undefined can if the upload was prevented by the maximum rule
					if (handler === undefined) {
				        return;
				    }
					
					// Associate an attachment with the handler
					this.field._uploading = false;
				
				    // Remove the handler from the pool
				    delete this.field._handlerPool[id];
				    handler._fileId = null;
				    
					// Handle the response
					if (response.status === 'success') {
						// Create the attachment
						var attachment = new DEPLOY.Attachment({
							path: response.data.path,
                            thumbnail_path: response.data.thumbnail_path,
							meta: response.data.meta,
							expectedMeta: response.data.expected_meta
							});
						
						// Set the attachment for the field
						handler.attachment(attachment);
	                	this.field.fieldSync();
	
					} else {
						// Notify the user of the error
					    FLUID.widgetByName('desktop').notify(response.data.msg);
						
						// Set the error for the field
		                this.field.stateMessage(response.data.msg);
		                this.field.state(FLUID.Field.INVALID_STATE);
	                	this.field.fieldSync();
						
						handler.destroy();
					}			
				}
			});
		}

        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Widget.prototype._update.apply(this, [false]);
		
        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },

    /** @private */
    _setState: function (value) {
        
        // There is no set/get state public interface as the manager gleans 
        // this information directly from the class.
        
        // Remove existing state styles...
        this.removeClass('fluid-state-checking');
        this.removeClass('fluid-state-pending');
        this.removeClass('fluid-state-valid');
        this.removeClass('fluid-state-invalid');
        
        // Set the state
        
        // Update the state style
        switch (value) {
        
        case FLUID.Field.CHECKING_STATE:
            this.addClass('fluid-state-checking');
            break;
            
        case FLUID.Field.PENDING_STATE:
            this.addClass('fluid-state-pending');
            break;
            
        case FLUID.Field.VALID_STATE:
            this.addClass('fluid-state-valid');
            break;
            
        case FLUID.Field.INVALID_STATE:
            this.addClass('fluid-state-invalid');
            break;
        }
    },

	/**
	 * Get the field associated with the manager.
	 * @return The field associated with the manager.
	 * @type DEPLOY.MultipleAttachmentField
	 */
	getField: function () {
		return this._field;
	}
});


/**
 * A class for handling one attachment for a multiple attachment manager.
 * @class A multiple attachment handler.
 * @constructor.
 */
DEPLOY.MultipleAttachmentHandler = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.Widget.call(this, options, false);
    this._type.push('DEPLOY.MultipleAttachmentHandler');
	
    // Add the additional CSS class to child classes
    this._classes.push(FLUID._cssName('field'));

	/** @private */
	this._fileId = options.fileId;
	
	/** @private */
	this._uploading = options.attachment === null;

	/** @private */
	this._dragNDropSet = false;

	/** @private */
	this._fileName = options.fileName || null;
	
	/** @private */
	this._attachment = options.attachment;
	
    /**
     * The buttons used to manage the attachment.
     */
    this.buttons = {
        'cancel': new FLUID.Button({parent: this, icon: 'deploy.icons.views.cancel', visible: false, enbaled: false}),
        'manage': new FLUID.Button({parent: this, icon: 'deploy.icons.views.properties', classes: ['fluid-sep'], visible: false, enbaled: false}),
        'remove': new FLUID.Button({parent: this, icon: 'deploy.icons.views.delete', visible: false, enbaled: false})
    };
    
    // Manage
    this.buttons['manage'].bind('onClick', function (event, data) { 
        if (this.enabled() && data.field.attachment() !== null) { 
            DEPLOY.Attachment.manageMeta(data.field.attachment());
        }
    }, {field: this});
    
    // Remove
    this.buttons['remove'].bind('onClick', function (event, data) { 
        if (this.enabled() && data.field.attachment() !== null) {
            var dlg = new DEPLOY.ConfirmDialog({
                name: 'deploy-window-attachment-remove',
                label: _('Are you sure you wish to delete this attachment?'),
                message: _(
                    'Please confirm you would like to delete - <strong>#ATTACHMENT#</strong> - by selecting <strong>Confirm</strong> below.'
                    ).replace('#ATTACHMENT#', data.field.attachment().getPath())
                });

            dlg.bind('onConfirm.deploy', function (event, data) { 
                var parent = data.field.getParent();
                data.field.destroy();
                parent.fieldSync();
            }, {field: data.field});
            
            dlg.launch();
        }
    }, {field: this});
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(DEPLOY.MultipleAttachmentHandler, FLUID.Widget);

$$.extend(DEPLOY.MultipleAttachmentHandler.prototype, {
    __className__: 'DEPLOY.MultipleAttachmentHandler',
    __css__: FLUID._cssName('field-multiple-attachment-handler'),
	__defaultClient__: 'tools',
    __template__: '<div id="<%=widget.getHTMLElementId()%>">' +
		'<div class="fluid-progress-bar">' +
			'<div class="fluid-progress"></div>' + 
		'</div>' +
		'<div class="fluid-thumbnail"></div>' +
	    '<div class="fluid-preview">' +
		'</div>' + 
	    '<div class="fluid-info">' +
	        '<span class="fluid-name"></span>' +
	        '<span class="fluid-file-info"></span>' +
	    '</div>' +	
        '<div class="fluid-tools <%=FLUID_GROUP_PREFIX%>tools">' + 
            '<%=group_tools%>' +
        '</div>' +
	'</div>',

    /**
     * Set/Get the attachment for the field.
     * @param {String} value (Optional) If undefined the attachment for
     * the field is returned, else the fields's attachment is set.
     * @return The attachment.
     * @type DEPLOY.Attachment
     */
    attachment: function (value) {
        if (value === undefined) {
            return this._attachment;
        }
        this.trigger('attachment', function () {
			this._attachment = value;
			this._uploading = false;
			this._update();
        }, {data: {'value': value}});
    },

	/**
	 * Update the progress of the file handlers upload.
     * @param {Number} loaded The number of bytes loaded so far.
	 * @param {Number} total The number of bytes to be loaded.
	 */
	updateProgress: function (loaded, total) {
        this.trigger('updateProgress', function () {
			if (this.exists()) {
				var progressSelection = this.getHTMLSelection().find('.fluid-progress-bar > .fluid-progress');
				var progress = String(parseInt(loaded / total * 100)) + '%';
				progressSelection.css('width', progress);
				progressSelection.attr('title', progress);
			}
        }, {data: {'loaded': loaded, 'total': total}});		
	},
	
	/** @private */
	_refresh: function () {
	    this._dragNDropSet = false;
		this._uploader = null;
		return FLUID.Field.Entry.prototype._refresh.apply(this);
	},

    // @@ Custom destroy required to remove drag'n'frop events

    /** @private */
    _update: function () {
		var localArgs = EVENT.getArgs(arguments);
        var supportExtensions = EVENT.getArg(localArgs, 0);
        var desktop = FLUID.widgetByName('desktop'); 
    
        if (!this.exists()) {
            return;
        }
        
        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_ON, supportExtensions);  
            
        FLUID.Field.Entry.prototype._update.apply(this, [false]);
        
        // If uploading or if the handler has an attachment already the allow
        // the attachment to be dragged and to recieve drops.
        
        this.getHTMLSelection().draggable({
            revert: true,
            zIndex: 99
        });
        this.getHTMLSelection().droppable({
			drop: function(event, ui) {
			    var droppedWidget = $$(this).data('fluid-widget');
                var draggedWidget = $$(ui.draggable).data('fluid-widget');
                
                if (draggedWidget.getParent() === droppedWidget.getParent()) {
                    var parent = droppedWidget.getParent();
                    
                    var moveToIndex = parent.indexOfChild(droppedWidget);
                    parent.moveChild(moveToIndex, draggedWidget);
                    parent.refresh();
                
                    parent.fieldSync();
                }
			}
		});
        
        // Update the attachment information and tools
		if (this._uploading) {
			// The handler is waiting to receive an attachment so we provide
			// information on the file name and the ability to cancel the 
			// upload.
			
			// Show the progress bar
			this.getHTMLSelection().children('.fluid-progress-bar').show();
			
			// Set the filename
			this.getHTMLSelection().find('.fluid-info > .fluid-name').html(this._fileName);
			
			// Enable the canel button
			this.buttons['cancel'].enable();
			this.buttons['cancel'].show();
			
        } else {
			// An attachment must be associated with the handler so we need
			// to ensure it's manageable.
			
			// Hide the progress bar
			this.getHTMLSelection().children('.fluid-progress-bar').hide();

			// Remove any mimetype classes
			this.removeClass('fluid-no-thumb');
			for (var i = 0; i < this._classes.length; i++) {
				var cssClass = this._classes[i];
				if (cssClass.indexOf('fluid-mimetype-') === 0) {
					this.removeClass(cssClass);
				}
			}

			// Show the thumbnail
			if (this.attachment().getThumbnailPath() === undefined || this.attachment().getThumbnailPath() === null) {
				// Show a mimetype icon
				this.addClass('fluid-no-thumb');
				if (this.attachment().meta('type') !== undefined && this.attachment().meta('type') !== null) {
				    this.addClass('fluid-mimetype-' + this.attachment().meta('type').replace('/', '-'));
			    }
				this.getHTMLSelection().find('.fluid-attachment-area > .fluid-attachment > .fluid-thumbnail').css('background-image', 'none');
			} else {
				// Show thumbnail
				this.getHTMLSelection().children('.fluid-thumbnail').css('background-image', 'url(' + this.attachment().getThumbnailPath() + ')');
			}
				
			// Update the file size info
			this.getHTMLSelection().find('.fluid-info > .fluid-name').html(this.attachment().meta('original_filename') || this.attachment().getPath());
			this.getHTMLSelection().find('.fluid-info > .fluid-file-info').html(this.attachment().meta('size_mb'));
			
			// Disable the canel button
			this.buttons['cancel'].enable(false);
			this.buttons['cancel'].show(false);
			
			// Enable the management tools
			this.buttons['manage'].enable();
			this.buttons['manage'].show();
			this.buttons['remove'].enable();
			this.buttons['remove'].show();
		}
        
        desktop.theme().callExtension(this,
            FLUID.Widget.LAYOUT_PHASE_AFTER, supportExtensions);  
    },
    
    defaultOptions: {
        visible: true,
        enabled: true,
        included: true,
        realtime: false,
        name: '',
        value: '',
        label: '',
		attachment: null
    }
});


/**
 * http://github.com/valums/file-uploader
 * 
 * Multiple file upload component with progress-bar, drag-and-drop. 
 * © 2010 Andrew Valums andrew(at)valums.com 
 * 
 * Licensed under GNU GPL.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */    
    
var qq = qq || {};

/**
 * Class that creates our multiple file upload widget
 */
qq.FileUploader = function(o){
    this._options = {
        field: null,
        // container element DOM node (ex. $$(selector)[0] for jQuery users)
        element: null,
        // url of the server-side upload script, should be on the same domain
        action: '/server/upload',
        // additional data to send, name-value pairs
        params: {},
        // ex. ['jpg', 'jpeg', 'png', 'gif'] or []
        allowedExtensions: [],        
        // size limit in bytes, 0 - no limit
        // this option isn't supported in all browsers
        sizeLimit: 0,
        onSubmit: function(id, fileName){},
        onProgress: function(id, loaded, total){},
        onComplete: function(id, fileName, responseJSON){},

        //
        // UI customizations

        template: '<div class="fluid-uploader">' + 
                '<div class="fluid-upload-drop-area"><span></span></div>' +
                '<div class="fluid-upload-button">Browse...</div>' +
                '<ul class="fluid-upload-list"></ul>' + 
             '</div>',

        // template for one item in file list
        fileTemplate: '<li>' +
                '<span class="fluid-upload-file"></span>' +
                '<span class="fluid-upload-spinner"></span>' +
                '<span class="fluid-upload-size"></span>' +
                '<a class="fluid-upload-cancel" href="#">Cancel</a>' +
                '<span class="fluid-upload-failed-text">Failed</span>' +
            '</li>',

        classes: {
            // used to get elements from templates
            button: 'fluid-upload-button',
            drop: 'fluid-upload-drop-area',
            dropActive: 'fluid-upload-drop-area-active',
            list: 'fluid-upload-list',
                        
            file: 'fluid-upload-file',
            spinner: 'fluid-upload-spinner',
            size: 'fluid-upload-size',
            cancel: 'fluid-upload-cancel',

            // added to list item when upload completes
            // used in css to hide progress spinner
            success: 'fluid-upload-success',
            fail: 'fluid-upload-fail'
        },
        messages: {
            //serverError: "Some files were not uploaded, please contact support and/or try again.",
            typeError: "{file} has invalid extension. Only {extensions} are allowed.",
            sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
            emptyError: "{file} is empty, please select files again without it."            
        },
        showMessage: function(message){
            alert(message);
        }
    };

    qq.extend(this._options, o);   

    this._field = this._options.field;
    this._element = this._options.element;

    if (this._element.nodeType != 1){
        throw new Error('element param of FileUploader should be dom node');
    }
    
    this._element.innerHTML = this._options.template;
    
    // number of files being uploaded
    this._filesInProgress = 0;
    
    // easier access
    this._classes = this._options.classes;
    
    this._handler = this._createUploadHandler(); 
    this._options.handler = this._handler; 
    this._bindCancelEvent();
    
    var self = this;
    this._button = new qq.UploadButton({
        element: this._getElement('button'),
        multiple: qq.UploadHandlerXhr.isSupported(),
        onChange: function(input){
            self._onInputChange(input);
        }        
    });        
    
    this._setupDragDrop();
};

qq.FileUploader.prototype = {
    setParams: function(params){
        this._options.params = params;
    },
    /**
     * Returns true if some files are being uploaded, false otherwise
     */
    isUploading: function(){
        return !!this._filesInProgress;
    },  
    /**
     * Gets one of the elements listed in this._options.classes
     * 
     * First optional element is root for search,
     * this._element is default value.
     *
     * Usage
     *  1. this._getElement('button');
     *  2. this._getElement(item, 'file'); 
     **/
    _getElement: function(parent, type){                        
        if (typeof parent == 'string'){
            // parent was not passed
            type = parent;
            parent = this._element;                   
        }
        
        var element = qq.getByClass(parent, this._options.classes[type])[0];
        
        if (!element){
            throw new Error('element not found ' + type);
        }
        
        return element;
    },
    _error: function(code, fileName){
        var message = this._options.messages[code];
        message = message.replace('{file}', this._formatFileName(fileName));
        message = message.replace('{extensions}', this._options.allowedExtensions.join(', '));
        message = message.replace('{sizeLimit}', this._formatSize(this._options.sizeLimit));
        this._options.showMessage(message);                
    },
    _formatFileName: function(name){
        if (name.length > 33){
            name = name.slice(0, 19) + '...' + name.slice(-13);    
        }
        return name;
    },
    _isAllowedExtension: function(fileName){
        var ext = (-1 !== fileName.indexOf('.')) ? fileName.replace(/.*[.]/, '').toLowerCase() : '';
        var allowed = this._options.allowedExtensions;
        
        if (!allowed.length){return true;}        
        
        for (var i=0; i<allowed.length; i++){
            if (allowed[i].toLowerCase() == ext){
                return true;
            }    
        }
        
        return false;
    },
    _setupDragDrop: function(){
        function isValidDrag(e){            
            var dt = e.dataTransfer,
                // do not check dt.types.contains in webkit, because it crashes safari 4            
                isWebkit = navigator.userAgent.indexOf("AppleWebKit") > -1;                        

            // dt.effectAllowed is none in Safari 5
            // dt.types.contains check is for firefox            
            return dt && dt.effectAllowed != 'none' && 
                (dt.files || (!isWebkit && dt.types.contains && dt.types.contains('Files')));
        }
        
        var self = this,
            dropArea = this._getElement('drop');                        
        
        dropArea.style.display = 'none';
        
        var hideTimeout;        
        qq.attach(document, 'dragenter', function(e){            
            e.preventDefault(); 
        });        

        qq.attach(document, 'dragover', function(e){
            if (isValidDrag(e)){
                         
                if (hideTimeout){
                    clearTimeout(hideTimeout);
                }
                
                if (dropArea == e.target || qq.contains(dropArea,e.target)){
                    var effect = e.dataTransfer.effectAllowed;
                    if (effect == 'move' || effect == 'linkMove'){
                        e.dataTransfer.dropEffect = 'move'; // for FF (only move allowed)    
                    } else {                    
                        e.dataTransfer.dropEffect = 'copy'; // for Chrome
                    }                                                                                    
                    qq.addClass(dropArea, self._classes.dropActive);     
                    e.stopPropagation();                                                           
                } else {
                    dropArea.style.display = 'block';
                    e.dataTransfer.dropEffect = 'none';    
                }
                                
                e.preventDefault();                
            }            
        });         
        
        qq.attach(document, 'dragleave', function(e){  
            if (isValidDrag(e)){
                                
                if (dropArea == e.target || qq.contains(dropArea,e.target)){                                        
                    qq.removeClass(dropArea, self._classes.dropActive);      
                    e.stopPropagation();                                       
                } else {
                                        
                    if (hideTimeout){
                        clearTimeout(hideTimeout);
                    }
                    
                    hideTimeout = setTimeout(function(){                                                
                        dropArea.style.display = 'none';                            
                    }, 77);
                }   
            }            
        });
        
        qq.attach(dropArea, 'drop', function(e){            
            dropArea.style.display = 'none';
            self._uploadFileList(e.dataTransfer.files);            
            e.preventDefault();
        });                      
    },
    _createUploadHandler: function(){
        var self = this,
            handlerClass;        
        
        if(qq.UploadHandlerXhr.isSupported()){           
            handlerClass = 'UploadHandlerXhr';                        
        } else {
            handlerClass = 'UploadHandlerForm';
        }

        var handler = new qq[handlerClass]({
            action: this._options.action,            
            onProgress: function(id, fileName, loaded, total){
                // is only called for xhr upload
                self._updateProgress(id, loaded, total);      
                self._options.onProgress(id, loaded, total);              
            },
            onComplete: function(id, fileName, result){
                self._filesInProgress--;

                // mark completed
                var item = self._getItemByFileId(id);                
                qq.remove(self._getElement(item, 'cancel'));
                qq.remove(self._getElement(item, 'spinner'));
                
                if (result.status === 'success'){
                    qq.addClass(item, self._classes.success);    
                } else {
                    qq.addClass(item, self._classes.fail);
                    
                    if (result.error){
                       self._options.showMessage(result.error); 
                    }
                }
                    
                self._options.onComplete(id, fileName, result);                                
            }
        });

        return handler;
    },
    _onInputChange: function(input){

        if (this._handler instanceof qq.UploadHandlerXhr){     
            
            this._uploadFileList(input.files);
            
        } else {
             
            if (this._validateFile(input)){                
                this._uploadFile(input);                                    
            }
                      
        }        
        
        this._button.reset();   
    },  
    _uploadFileList: function(files){
        var valid = true;

        var i = files.length;
        while (i--){         
            if (!this._validateFile(files[i])){
                valid = false;
                break;
            }
        }  
        
        if (valid){                                      
            var i = files.length;
            while (i--){ this._uploadFile(files[i]); }  
        }
    },
    _uploadFile: function(fileContainer){            
        var id = this._handler.add(fileContainer);
        var name = this._handler.getName(id);        
        this._options.onSubmit(id, name);        
        this._addToList(id, name);
        this._handler.upload(id, this._options.params);        
    },      
    _validateFile: function(file){
        var name,size;
 
        if (file.value){
            // it is a file input            
            // get input value and remove path to normalize
            name = file.value.replace(/.*(\/|\\)/, "");
        } else {
            // fix missing properties in Safari
            name = file.fileName != null ? file.fileName : file.name;
            size = file.fileSize != null ? file.fileSize : file.size;
        }
                    
        if (! this._isAllowedExtension(name)){            
            this._error('typeError',name);
            return false;
            
        } else if (size === 0){            
            this._error('emptyError',name);
            return false;
                                                     
        } else if (size && this._options.sizeLimit && size > this._options.sizeLimit){            
            this._error('sizeError',name);
            return false;            
        }
        
        return true;                
    },
    _addToList: function(id, fileName){
        var item = qq.toElement(this._options.fileTemplate);                
        item.qqFileId = id;

        var fileElement = this._getElement(item, 'file');        
        qq.setText(fileElement, this._formatFileName(fileName));
        this._getElement(item, 'size').style.display = 'none';        

        this._getElement('list').appendChild(item);

        this._filesInProgress++;
    },
    _updateProgress: function(id, loaded, total){
        var item = this._getItemByFileId(id);
        var size = this._getElement(item, 'size');
        size.style.display = 'inline';
        
        var text; 
        if (loaded != total){
            text = Math.round(loaded / total * 100) + '% from ' + this._formatSize(total);
        } else {                                   
            text = this._formatSize(total);
        }          
        
        qq.setText(size, text);
    },
    _formatSize: function(bytes){
        var i = -1;                                    
        do {
            bytes = bytes / 1024;
            i++;  
        } while (bytes > 99);
        
        return Math.max(bytes, 0.1).toFixed(1) + ['kB', 'MB', 'GB', 'TB', 'PB', 'EB'][i];          
    },
    _getItemByFileId: function(id){
        var item = this._getElement('list').firstChild;
        
        // there can't be text nodes in our dynamically created list
        // because of that we can safely use nextSibling
        while (item){            
            if (item.qqFileId == id){
                return item;
            }
            
            item = item.nextSibling;
        }          
    },
    /**
     * delegate click event for cancel link 
     **/
    _bindCancelEvent: function(){
        var self = this,
            list = this._getElement('list');            
            
        qq.attach(list, 'click', function(e){
            e = e || window.event;
            var target = e.target || e.srcElement;
            
            if (qq.hasClass(target, self._classes.cancel)){
                qq.preventDefault(e);

                var item = target.parentNode;
                self._handler.cancel(item.qqFileId);
                qq.remove(item);
            }
        });

    }    
};

qq.UploadButton = function(o){
    this._options = {
        element: null,  
        // if set to true adds multiple attribute to file input      
        multiple: false,
        // name attribute of file input
        name: 'file',
        onChange: function(input){},
        hoverClass: 'fluid-over',
        focusClass: 'fluid-focus',
        downClass: 'fluid-down'
    };
    
    qq.extend(this._options, o);
        
    this._element = this._options.element;
    
    // make button suitable container for input
    qq.css(this._element, {
        position: 'relative',
        overflow: 'hidden',
        // Make sure browse button is in the right side
        // in Internet Explorer
        direction: 'ltr'
    });   
    
    this._input = this._createInput();
};

qq.UploadButton.prototype = {
    /* returns file input element */    
    getInput: function(){
        return this._input;
    },
    /* cleans/recreates the file input */
    reset: function(){
        if (this._input.parentNode){
            qq.remove(this._input);    
        }                
        
        qq.removeClass(this._element, this._options.focusClass);
        this._input = this._createInput();
    },    
    _createInput: function(){                
        var input = document.createElement("input");
        
        if (this._options.multiple){
            input.setAttribute("multiple", "multiple");
        }
                
        input.setAttribute("type", "file");
        input.setAttribute("name", this._options.name);
        
        qq.css(input, {
            position: 'absolute',
            // in Opera only 'browse' button
            // is clickable and it is located at
            // the right side of the input
            right: 0,
            top: 0,
            zIndex: 1,
            fontSize: '460px',
            margin: 0,
            padding: 0,
            cursor: 'pointer',
            opacity: 0
        });
        
        this._element.appendChild(input);

        var self = this;
        qq.attach(input, 'change', function(){
            self._options.onChange(input);
        });
        
        qq.attach(input, 'mouseover', function(){
            qq.addClass(self._element, self._options.hoverClass);
        });
        qq.attach(input, 'mouseout', function(){
            qq.removeClass(self._element, self._options.hoverClass);
            qq.removeClass(self._element, self._options.downClass);
        });     
        qq.attach(input, 'mousedown', function(){
            qq.addClass(self._element, self._options.downClass);
        });     
        qq.attach(input, 'mouseup', function(){
            qq.removeClass(self._element, self._options.downClass);
        });
        qq.attach(input, 'focus', function(){
            qq.addClass(self._element, self._options.focusClass);
        });
        qq.attach(input, 'blur', function(){
            qq.removeClass(self._element, self._options.focusClass);
        });

        // IE and Opera, unfortunately have 2 tab stops on file input
        // which is unacceptable in our case, disable keyboard access
        if (window.attachEvent){
            // it is IE or Opera
            input.setAttribute('tabIndex', "-1");
        }

        return input;            
    }        
};

/**
 * Class for uploading files using form and iframe
 */
qq.UploadHandlerForm = function(o){
    this._options = {
        // URL of the server-side upload script,
        // should be on the same domain to get response
        action: '/upload',
        // fires for each file, when iframe finishes loading
        onComplete: function(id, fileName, response){}
    };
    qq.extend(this._options, o);
       
    this._inputs = {};
};
qq.UploadHandlerForm.prototype = {
    /**
     * Adds file input to the queue
     * Returns id to use with upload, cancel
     **/    
    add: function(fileInput){
        fileInput.setAttribute('name', 'qqfile');
        var id = 'fluid-upload-handler-iframe' + UTILS.createId();       
        
        this._inputs[id] = fileInput;
        
        // remove file input from DOM
        if (fileInput.parentNode){
            qq.remove(fileInput);
        }
                
        return id;
    },
    /**
     * Sends the file identified by id and additional query params to the server
     * @param {Object} params name-value string pairs
     */
    upload: function(id, params){                        
        var input = this._inputs[id];
        
        if (!input){
            throw new Error('file with passed id was not added, or already uploaded or cancelled');
        }                
        
        var fileName = this.getName(id);
                
        var iframe = this._createIframe(id);
        var form = this._createForm(iframe, params);
        form.appendChild(input);

        var self = this;
        this._attachLoadEvent(iframe, function(){            
            self._options.onComplete(id, fileName, self._getIframeContentJSON(iframe));
            
            delete self._inputs[id];
            // timeout added to fix busy state in FF3.6
            setTimeout(function(){
                qq.remove(iframe);
            }, 1);
        });

        form.submit();        
        qq.remove(form);        
        
        return id;
    },
    cancel: function(id){        
        if (id in this._inputs){
            delete this._inputs[id];
        }        

        var iframe = document.getElementById(id);
        if (iframe){
            // to cancel request set src to something else
            // we use src="javascript:false;" because it doesn't
            // trigger ie6 prompt on https
            iframe.setAttribute('src', 'javascript:false;');

            qq.remove(iframe);
        }
    },
    getName: function(id){
        // get input value and remove path to normalize
        return this._inputs[id].value.replace(/.*(\/|\\)/, "");
    },  
    _attachLoadEvent: function(iframe, callback){
        qq.attach(iframe, 'load', function(){
            // when we remove iframe from dom
            // the request stops, but in IE load
            // event fires
            if (!iframe.parentNode){
                return;
            }

            // fixing Opera 10.53
            if (iframe.contentDocument &&
                iframe.contentDocument.body &&
                iframe.contentDocument.body.innerHTML == "false"){
                // In Opera event is fired second time
                // when body.innerHTML changed from false
                // to server response approx. after 1 sec
                // when we upload file with iframe
                return;
            }

            callback();
        });
    },
    /**
     * Returns json object received by iframe from server.
     */
    _getIframeContentJSON: function(iframe){
        // iframe.contentWindow.document - for IE<7
        var doc = iframe.contentDocument ? iframe.contentDocument: iframe.contentWindow.document,
            response;

        try{
            response = eval("(" + doc.body.innerHTML + ")");
        } catch(err){
            response = {};
        }

        return response;
    },
    /**
     * Creates iframe with unique name
     */
    _createIframe: function(id){
        // We can't use following code as the name attribute
        // won't be properly registered in IE6, and new window
        // on form submit will open
        // var iframe = document.createElement('iframe');
        // iframe.setAttribute('name', id);

        var iframe = qq.toElement('<iframe src="javascript:false;" name="' + id + '" />');
        // src="javascript:false;" removes ie6 prompt on https

        iframe.setAttribute('id', id);

        iframe.style.display = 'none';
        document.body.appendChild(iframe);

        return iframe;
    },
    /**
     * Creates form, that will be submitted to iframe
     */
    _createForm: function(iframe, params){
        // We can't use the following code in IE6
        // var form = document.createElement('form');
        // form.setAttribute('method', 'post');
        // form.setAttribute('enctype', 'multipart/form-data');
        // Because in this case file won't be attached to request
        var form = qq.toElement('<form method="post" enctype="multipart/form-data"></form>');

        var queryString = '?';
        for (var key in params){
            queryString += '&' + key + '=' + encodeURIComponent(params[key]);
        }

        form.setAttribute('action', this._options.action + queryString);
        form.setAttribute('target', iframe.name);
        form.style.display = 'none';
        document.body.appendChild(form);

        return form;
    }
};

/**
 * Class for uploading files using xhr
 */
qq.UploadHandlerXhr = function(o){
    this._options = {
        // url of the server-side upload script,
        // should be on the same domain
        action: '/upload',
        onProgress: function(id, fileName, loaded, total){},
        onComplete: function(id, fileName, response){}
    };
    qq.extend(this._options, o);

    this._files = {};
    this._xhrs = {};
};

// static method
qq.UploadHandlerXhr.isSupported = function(){
    return typeof File != "undefined" &&
        typeof (new XMLHttpRequest()).upload != "undefined";    
};

qq.UploadHandlerXhr.prototype = {
    /**
     * Adds file to the queue
     * Returns id to use with upload, cancel
     **/    
    add: function(file){
        var id = UTILS.createId();
        this._files[id] = file;
        return id;         
    },
    /**
     * Sends the file identified by id and additional query params to the server
     * @param {Object} params name-value string pairs
     */    
    upload: function(id, params){
        var file = this._files[id],
            name = this.getName(id),
            size = this.getSize(id);
        
        if (!file){
            throw new Error('file with passed id was not added, or already uploaded or cancelled');   
        }
                        
        var xhr = this._xhrs[id] = new XMLHttpRequest();
        var self = this;
                                        
        xhr.upload.onprogress = function(e){
            if (e.lengthComputable){
                self._options.onProgress(id, name, e.loaded, e.total);
            }
        };

        xhr.onreadystatechange = function(){
            // the request was aborted/cancelled
            if (!self._files[id]){
                return;
            }
            
            if (xhr.readyState == 4){
                                
                self._options.onProgress(id, name, size, size);
                
                if (xhr.status == 200){
                    var response;
                    
                    try {
                        response = eval("(" + xhr.responseText + ")");
                    } catch(err){
                        response = {};
                    }
                    
                    self._options.onComplete(id, name, response);
                        
                } else {                   
                    self._options.onComplete(id, name, {});
                }
                
                self._files[id] = null;
                self._xhrs[id] = null;                
            }
        };

        // build query string
        var queryString = '?qqfile=' + encodeURIComponent(name);
        for (var key in params){
            queryString += '&' + key + '=' + encodeURIComponent(params[key]);
        }

        xhr.open("POST", this._options.action + queryString, true);
        xhr.send(file);        
    },
    cancel: function(id){
        this._files[id] = null;
        
        if (this._xhrs[id]){
            this._xhrs[id].abort();
            this._xhrs[id] = null;                                   
        }
    },
    getName: function(id){
        // fix missing name in Safari 4
        var file = this._files[id];
        return file.fileName != null ? file.fileName : file.name;       
    },
    getSize: function(id){
        // fix missing size in Safari 4
        var file = this._files[id];
        return file.fileSize != null ? file.fileSize : file.size;
    }
};

//
// Helper functions
//

var qq = qq || {};

//
// Useful generic functions

/**
 * Adds all missing properties from obj2 to obj1
 */
qq.extend = function(obj1, obj2){
    for (var prop in obj2){
        obj1[prop] = obj2[prop];
    }
};

//
// Events

qq.attach = function(element, type, fn){
    if (element.addEventListener){
        element.addEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.attachEvent('on' + type, fn);
    }
};
qq.detach = function(element, type, fn){
    if (element.removeEventListener){
        element.removeEventListener(type, fn, false);
    } else if (element.attachEvent){
        element.detachEvent('on' + type, fn);
    }
};

qq.preventDefault = function(e){
    if (e.preventDefault){
        e.preventDefault();
    } else{
        e.returnValue = false;
    }
};
//
// Node manipulations

/**
 * Insert node a before node b.
 */
qq.insertBefore = function(a, b){
    b.parentNode.insertBefore(a, b);
};
qq.remove = function(element){
    element.parentNode.removeChild(element);
};

qq.contains = function(parent, descendant){
    if (parent.contains){
        return parent.contains(descendant);
    } else {
        return !!(descendant.compareDocumentPosition(parent) & 8);
    }
};

/**
 * Creates and returns element from html string
 * Uses innerHTML to create an element
 */
qq.toElement = (function(){
    var div = document.createElement('div');
    return function(html){
        div.innerHTML = html;
        var element = div.firstChild;
        div.removeChild(element);
        return element;
    };
})();

//
// Node properties and attributes

/**
 * Sets styles for an element.
 * Fixes opacity in IE6-8.
 */
qq.css = function(element, styles){
    if (styles.opacity != null){
        if (typeof element.style.opacity != 'string' && typeof(element.filters) != 'undefined'){
            styles.filter = 'alpha(opacity=' + Math.round(100 * styles.opacity) + ')';
        }
    }
    qq.extend(element.style, styles);
};
qq.hasClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    return re.test(element.className);
};
qq.addClass = function(element, name){
    if (!qq.hasClass(element, name)){
        element.className += ' ' + name;
    }
};
qq.removeClass = function(element, name){
    var re = new RegExp('(^| )' + name + '( |$)');
    element.className = element.className.replace(re, ' ').replace(/^\s+|\s+$/g, "");
};
qq.setText = function(element, text){
    element.innerText = text;
    element.textContent = text;
};

//
// Selecting elements

qq.children = function(element){
    var children = [],
    child = element.firstChild;

    while (child){
        if (child.nodeType == 1){
            children.push(child);
        }
        child = child.nextSibling;
    }

    return children;
};

qq.getByClass = function(element, className){
    if (element.querySelectorAll){
        return element.querySelectorAll('.' + className);
    }

    var result = [];
    var candidates = element.getElementsByTagName("*");
    var len = candidates.length;

    for (var i = 0; i < len; i++){
        if (qq.hasClass(candidates[i], className)){
            result.push(candidates[i]);
        }
    }
    return result;
};
