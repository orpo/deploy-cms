/**
 * @fileoverview    Deploy base (part of the DeployCMS client-side library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';

/**
 * The DEPLOY namespace.
 * @final
 * @type Namespace
 */  
var DEPLOY = {
    
    /** @private */
    _site: null,
    
    /** @private */
    _administrator: null,
    
    /** @private */
    _controllers: {},
    
    /** @private */
    _editors: {},
    
    /** @private */
    _editableEnvironment: null,
    
    /** @private */
    _editorTools: {},
    
    /** @private */
    _themes: {}

};


/**
 * The DEPLOY PLUGINS namespace. None core DeployCMS plugins should use this 
 * namespace.
 * @final
 * @type Namespace
 */  
DEPLOY.PLUGINS = {};


/**
 * An error class 
 * @class An administrator class.
 * @constructor
 */ 
DEPLOY.Error = function (msg) {

    /**
     * The error message to display/log.
     * @type String
     */ 
    this.msg = msg;
   
    // Notify the user a problem has occurred
	var desktop = FLUID.widgetByName('desktop');

    // Enter *single* model state
    desktop.startModal(FLUID.Desktop.MODAL);
    desktop.startModal(FLUID.Desktop.SINGLE_MODAL);
    desktop.bind('onEndModal.deploy', function (event) {
		event.stopDefault();
	});
	desktop.addClass('fluid-frozen');   

    // Create a window
	var win = new FLUID.Window({
		name: 'deploy-window-error',
		group: 'singleModal',
		label: _('An error has occurred...'),
		size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['alert'].slice(0),
		flags: 
			FLUID.Window.HAS_ACTIONS |
			FLUID.Window.CAN_CLOSE |
			FLUID.Window.CENTERED
	});

	win.unbind('afterCloseComplete');
	win.bind('afterCloseComplete', function(event, data) {
		var desktop = FLUID.widgetByName('desktop');
		this.destroy();
		desktop.unbind('onEndModal.deploy');
		desktop.endModal(FLUID.Desktop.SINGLE_MODAL);
		desktop.removeClass('fluid-frozen');
		
		// Make sure we shut down any zombie windows hiding in the modal layer
		var zombies = desktop.getChildren('modal');
		for (var i = 0; i < zombies.length; i++) {
			if (zombies[i].getState() !== FLUID.Window.OPEN_STATE) { // Is it really a zombie?
				zombies[i].destroy();
			}
		}
		
		// If there is no windows left then exit modal mode
		if (desktop.getChildren('modal').length === 0) {
			desktop.endModal(FLUID.Desktop.MODAL);
		}
	});

    // Error message
    var msg = new FLUID.Text({
        parent: win,
        content: this.msg
        });

	// OK button
	var okBtn = new FLUID.Button({
       	parent: win,
		group: 'actions',
       	label: _('OK'),
		classes: ['fluid-center']
   	});
	okBtn.bind('onClick', function (event, data) { 
		win.close();
	}, {win: win});

    // Render, Prepare & Open the window
    win.refresh();
    win.prepare();
    win.open();
};

$$.extend(DEPLOY.Error.prototype, {
    __className__: 'DEPLOY.Error',
    
    toString: function () {
        return this.message();
    }
});

// Hook IO errors into using the DEPLOY.Error class
IO.logger.bind('afterlog', function (event) {
	if (event.data.level === IO.Logger.ERROR) {
		// OK the server fell over or something equally bad happened... send in the hero of the hour
		var msg = _('We\'re not sure what happened, most likely the issue is server-side. Please report this to your web administrator.');
		try {
			msg = event.data.response.error.data.msg;
		} catch (error) { /* Ignore */ }
		new DEPLOY.Error(msg);
	}
});