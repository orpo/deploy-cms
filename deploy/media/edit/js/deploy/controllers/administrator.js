/**
 * @fileoverview    DeployCMS administrator (part of the DeployCMS client-side library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        IO (/js/core/io.js)
 */

'use strict';


/**
 * There should only ever be one Administrator class instance, this function
 * creates a singleton way to access it.
 * @return The administrator.
 * @type {DEPLOY.DeployAdministrator}
 */ 
DEPLOY.getAdministrator = function () {
    if (DEPLOY._administrator === null) {
        DEPLOY._administrator = new DEPLOY.DeployAdministrator();
    }
    return DEPLOY._administrator;
}


/**
 * A class to handle an administrator using the DeployCMS administration
 * environment.
 * @class An administrator class.
 * @constructor
 */
DEPLOY.DeployAdministrator = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._id = '';
    
    /** @private */
    this._email = '';
    
    /** @private */
    this._firstName = '';
    
    /** @private */
    this._lastName = '';
    
    /** @private */
    this._preferences = {};
    
    /** @private */
    this._controllers = {};
    
    /** @private */
    this._masterController = {};

	/** @private */
	this._url = '';
}
UTILS.copyPrototype(DEPLOY.DeployAdministrator, EVENT.EventClass);

$$.extend(DEPLOY.DeployAdministrator.prototype, {
    __className__: 'DEPLOY.DeployAdministrator',
    
    /**
     * Get the administrators ID.
     * @return The administrators ID.
     * @type String
     */
    getID: function () {
        return this._id;
    },
        
    /**
     * Get the administrators email address.
     * @return The administrators email address.
     * @type String
     */
    getEmail: function () {
        return this._email;
    },
    
    /**
     * Get the administrators first name.
     * @return The administrators first name.
     * @type String
     */
    getFirstName: function () {
        return this._firstName;
    },
    
    /**
     * Get the administrators last name.
     * @return The administrators last name.
     * @type String
     */
    getLastName: function () {
        return this._lastName;
    },
    
    /**
     * Get a list of the administrators preference names.
     * @return The administrators preferences.
     * @type Object
     */
    getPreferences: function () {
        var preferences = [];
        for (var property in this._preferences) {
            preferences.push(property);
        }
        return preferences;
    },
    
    /**
     * Get an administrators preference.
     * @param {String} name The name of the preference to return.
     * @param {*} default (optional) The default value to return if a 
     * preference hasn't been 
     * @return The value of the preference.
     * @type *
     */
    getPreference: function (name, defaultValue) {
        var value = defaultValue;
        if (this._preferences !== null) {
            if (this._preferences[name] !== undefined) {
                value = this._preferences[name];
            }
        }
        return value;
    },
    
    /**
     * Get a list of controller names available to the user, optionally by group.
     * @param {String} group The name of the group to return controllers for.
     * @return A list of controller names.
     * @type Object
     */
    getControllers: function (group) {
        var controllers = [];
        for (var name in this._controllers) {
            if (this._controllers.hasOwnProperty(name)) {
                var controller = this._controllers[name];
                if (group === undefined || 
                    group === controller.gui_grouping) {
                    
                    controllers.push(name); 
                }
            }
        }
        return controllers;
    },
    
    /**
     * Return a controller by it's name.
     * @param {String} name The name of the controller to return.
     * @return The controller associated with the specifed name.
     * @type Object
     */
    getController: function (name) {
        if (this._controllers[name] !== undefined) {
            return $$.extend({}, this._controllers[name]);
        }
    },
    
    /**
     * Get the available view names for a controller.
     * @param {String} controller The name of the controller to return the views
     * views for.
     * @return A list of available view names for the controller.
     * @type Array
     */
    getControllerViews: function (controller) {
        var views = [];
        if (this._controllers[controller] !== undefined) {
            views = this._controllers[controller].views.slice();
        }
        return views;
    },
    
    /**
     * Get the groupings for controllers.
     * @return A list of group names for controllers.
     * @type Array
     */
    getControllerGroups: function () {
        var groups = [];
        for (var name in this._controllers) {
            if (this._controllers.hasOwnProperty(name)) {
                var groupName = this._controllers[name]['gui_grouping'];
                if (groups.indexOf(groupName) === -1) {
                    groups.push(groupName);
                }
            }
        }
        return groups;
    },
    
    /**
     * Check if the administrator can call a view against a controller.
     * @param {String} controller The name of the controller to check.
     * @param {String} view The name of the view to to check.
     * @return Whether the administrator has permission to call the specified
     * view.
     * @type Boolean
     */
    can: function(controller, view) {
        var views = this.getControllerViews(controller);
        if (views.indexOf(view) > -1) {
            return true;
        }
        return false;
    },
    
    /**
     * Get the current master controller (the controller responsible for 
     * generating the current page).
     * @return The name of the current master controller.
     * @type String
     */
    getMasterController: function () {
        return this._masterController;
    },
    
    /**
     * Get the URL for the current page.
     * @return The current URL.
     * @type String
     */
    getURL: function () {
        return this._url;
    },
	
    /**
     * Attempt to get the administrators account details from the remote 
     * server.
     */
    remoteGet: EVENT.classMethod(DEPLOY.DeployAdministrator, function remoteGet() {
        // IO Request to get administrator details
        var admReq = new IO.Request({
            type: 'GET',
            controller: 'deploy-administrator',
            method: 'get-configuration'
        });
        
        // Handle the outcome of the request
        admReq.bind('afterSuccess.deploy', function (event, data) {
            data.adm.trigger('sessionFound', function (event) {
                
            }, event);
        }, {adm: this});
        
        admReq.bind('afterFail.deploy', function (event, data) {
            data.adm.trigger('sessionExpired', function (event) {
                
            }, event);
        }, {adm: this});
        
        admReq.send();
    }, 'remoteGet'),
    
    configure: EVENT.classMethod(DEPLOY.DeployAdministrator, function configure() {
		var localArgs = EVENT.getArgs(arguments);
        var configuration = EVENT.getArg(localArgs, 0);
        
        this._id = configuration['id'];
        this._email = configuration['email'];
        this._firstName = configuration['first_name'];
        this._lastName = configuration['last_name'];
        this._role = configuration['role'];
        this._preferences = configuration['preferences'] || {}; 
        this._controllers = configuration['controllers'];
        
        /* 
        Convert any controller views to dash separated (instead of 
        underscore). 
        
        @@ It would probably better if this was done server side in the same
        way controller names are.
        */
		for (var name in configuration['controllers']) {
            if (configuration['controllers'].hasOwnProperty(name)) {
                var views = configuration['controllers'][name]['views'];
                var new_views = [];
                for (var i = 0; i < views.length; i++) {
					var new_view = views[i].replace(/_/g, '-');
                    new_views.push(new_view);
                }
                configuration['controllers'][name]['views'] = new_views;
            }
        }

        this._masterController = configuration['master_controller'];
        this._url = configuration['url'];
        
    }, 'configure'),
    
    initGUI: EVENT.classMethod(DEPLOY.DeployAdministrator, function initGUI() {
        
    }, 'initGUI'),
    
    updateGUI: EVENT.classMethod(DEPLOY.DeployAdministrator, function updateGUI() {
        
    }, 'updateGUI')
});

// @@ Comment
DEPLOY.DeployAdministratorController = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    DEPLOY.Controller.call(this, {
        name: 'deploy-administrator',
        verbose_name: 'administrator',
        verbose_name_plural: 'administrators'
    });
};
UTILS.copyPrototype(DEPLOY.DeployAdministratorController, DEPLOY.Controller);

$$.extend(DEPLOY.DeployAdministratorController.prototype, {
    __className__: 'DEPLOY.DeployAdministratorController',

    /**
     * Ask the user to sign-in as an administrator.
     */
    signIn: function (username) {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
        // Create the view window
        var win = new FLUID.Window({
            name: 'deploy-window-sign-in-' + this.getName(),
            group: 'modal',
            label: _('Sign-in'),
            minSize: [0, 0],
            size: [DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0][0], 'auto'],
            flags:
                FLUID.Window.HAS_ACTIONS |
                FLUID.Window.CENTERED
            });

        // Load the form
        var formReq = new IO.Request({
            controller: this.getName(),     
            method: 'sign-in'
            });
        
        // Handle the form load request
        formReq.bind('afterSuccess', function (event, data) {
            var ctr = data.ctr;
            var win = data.win;
            var username = data.username;
            
            // Create a form from the JSON returned            
            var form = DEPLOY.createForm({
				controller: ctr,
				view: 'PASSIVE',
				json: event.data.data
			});
            win.appendChild(form);
            
            // Pre-populate username if specified
            if (username !== undefined) {
                form.getFieldMap()['username'].value(username);
            }
                    
            // Add the sign-in & reset password buttons
            var signInBtn = new FLUID.Button({
                parent: win,
                group: 'actions',
                label: _('Sign-in'),
                icon: 'deploy.icons.views.sign'
            });
            signInBtn.bind('onClick', function (event, data) { 
                data.form.send();
            }, {form: form});

            var resetBtn = new FLUID.Button({
                parent: win,
                group: 'actions',
                label: _('Forgotten?'),
                classes: ['fluid-right']
            });
            resetBtn.bind('onClick', function (event, data) { 
                var ctr = data.ctr;
                var win = data.win;
                
                win.bind('afterCloseComplete', function(event, data) {
                    // Close the window
                    this.remove();
                    
                    // Call reset password
                    ctr.forgottenPassword();
                    
                }, {ctr: ctr});
                
                // Close the window
                win.close();
                
            }, {ctr: ctr, win: win});            
            
            // Handle form submissions
            form.bind('afterSendSuccess', function (event) {
                var win = data.win;
                
                win.bind('afterCloseComplete', function(event, data) {
                    // Close the window
                    this.remove();
                    
                    // Update the GUI
                    DEPLOY.getAdministrator().bind('afterUpdateGUI.sign-in', function (event, data) {
                        DEPLOY.getAdministrator().unbind('afterUpdateGUI.sign-in');
                        FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);
                        FLUID.widgetByName('desktop').notify(data.msg);
                    }, {msg: data.msg});
                    
                    DEPLOY.getAdministrator().remoteGet();
                }, {msg: event.data.data.msg});
                
                // Close the window
                win.close();
                
            }, {win: win});
            
            // Render, Prepare & Open the window
            win.refresh();
            win.prepare();
            win.open();
            
        }, {ctr: this, win: win, username: username});
            
        // Get the sign-in form
        formReq.send();
    },
    
    /**
     * Reset an administrators password.
     */
    forgottenPassword: function () {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
        // Create the view window
        var win = new FLUID.Window({
            name: 'deploy-window-reset-password-' + this.getName(),
            group: 'modal',
            label: _('Reset your password'),
            minSize: [0, 0],
            size: [DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0][0], 'auto'],
            flags:
                FLUID.Window.HAS_ACTIONS |
                FLUID.Window.CENTERED
            });

        // Load the form
        var formReq = new IO.Request({
            controller: this.getName(),     
            method: 'forgotten-password'
            });
    
        // Handle the form load request
        formReq.bind('afterSuccess', function (event, data) {
            var ctr = data.ctr;
            var win = data.win;
            
            // Create a form from the JSON returned            
            var form = DEPLOY.createForm({
				controller: ctr,
				view: 'PASSIVE',
				json: event.data.data
			});
            win.appendChild(form);
            
            // Add the sign-in & reset password buttons
            var resetBtn = new FLUID.Button({
                parent: win,
                group: 'actions',
                label: _('Reset password'),
                icon: 'deploy.icons.views.password'
            });
            resetBtn.bind('onClick', function (event, data) { 
                data.form.send();
            }, {form: form});

            var signInBtn = new FLUID.Button({
                parent: win,
                group: 'actions',
                label: _('Back to Sign-in'),
                classes: ['fluid-right']
            });
            signInBtn.bind('onClick', function (event, data) { 
                var ctr = data.ctr;
                var win = data.win;
                
                win.bind('afterCloseComplete', function(event, data) {
                    // Close the window
                    this.remove();
                    
                    // Call sign in
                    ctr.signIn();
                    
                }, {ctr: ctr});
                
                // Close the window
                win.close();
                
            }, {ctr: ctr, win: win});                    
            
            // Handle form submissions
            form.bind('afterSendSuccess', function (event) {
                var win = data.win;
                
                win.bind('afterCloseComplete', function(event, data) {
                    var username = data.username;
                    
                    // Close the window
                    this.remove();
                                    
                    // Call sign in
                    ctr.signIn(username);
                    
                    // Notify the user that their password has been reset
                    FLUID.widgetByName('desktop').notify(data.msg);

                }, {msg: event.data.data.msg, username: this.getData()['username']});
                
                // Close the window
                win.close();
                
            }, {win: win});
            
            // Render, Prepare & Open the window
            win.refresh();
            win.prepare();
            win.open();
            
        }, {ctr: this, win: win});
            
        // Get the sign-in form
        formReq.send();        
    },
    
    /**
     * Sign out the administrator.
     */
    signOut: function () {
        
        // Ask the server to sign out the administrator
        var signOutReq = new IO.Request({
            type: 'POST',
            controller: this.getName(),
            method: 'sign-out'
            });
        
        // Handle the server response
        signOutReq.bind('afterSuccess', function (event, data) {
            window.location = event.data.data.redirect;
        });

        // Send the sign-out request
        signOutReq.send(); 
    },
    
    defaultOptions: {}
    
}); new DEPLOY.DeployAdministratorController();