/**
 * @fileoverview    DeployCMS site (part of the DeployCMS client-side library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 */

'use strict';


/**
 * A controller class for documents.
 * @class A document controller class.
 * @constructor
 */
DEPLOY.DeployDocumentController = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    DEPLOY.Controller.call(this, {
        name: 'deploy-document',
        verbose_name: 'document',
        verbose_name_plural: 'documents',
        visit_page_label: 'Download document',
        visit_page_new_window: true
    });
};
UTILS.copyPrototype(DEPLOY.DeployDocumentController, DEPLOY.Controller);

$$.extend(DEPLOY.DeployDocumentController.prototype, {
    __className__: 'DEPLOY.DeployDocumentController',
    
}); new DEPLOY.DeployDocumentController();


/**
 * A controller class to support site features.
 * @class A site controller class.
 * @constructor
 */
DEPLOY.DeploySiteController = function (options) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    DEPLOY.Controller.call(this, {
        name: 'deploy-site',
        verbose_name: 'site',
        verbose_name_plural: 'sites'
    });
};
UTILS.copyPrototype(DEPLOY.DeploySiteController, DEPLOY.Controller);

$$.extend(DEPLOY.DeploySiteController.prototype, {
    __className__: 'DEPLOY.DeploySiteController',

    systemInfo: function () {
        
        // Enter model state
        FLUID.widgetByName('desktop').startModal(FLUID.Desktop.MODAL);

        // Create the view window
        var win = new FLUID.Window({
            name: 'deploy-window-system-info-' + this.getName(),
            group: 'modal',
            label: _('System Info'),
            size: [DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0][0], 'auto'],
            flags:
                FLUID.Window.CAN_CLOSE |
                FLUID.Window.HAS_ACTIONS |
                FLUID.Window.CENTERED
            });

        // Handle the window being closed
        win.bind('afterCloseComplete', function(event, data) {
            // Remove the window
            this.remove();

            // Exit modal mode
            FLUID.widgetByName('desktop').endModal(FLUID.Desktop.MODAL);
        });

        // Load the form
        var infoReq = new IO.Request({
            controller: this.getName(),     
            method: 'system-info'
            });
        
        infoReq.bind('afterSuccess', function (event, data) {
            
            var i = 0;
            var info = event.data.data;
            var infoString = '';
            var accountInfo = [];
            var property;
            var systemInfo = [];
            var user = DEPLOY.getAdministrator();
            var preferenceNames = user.getPreferences();
            preferenceNames.sort();
            var controllerNames = user.getControllers();
            controllerNames.sort();
            
            // Build a form in which to show the information
            var infoForm = new FLUID.Form({parent: win});
            var infoFieldset = new FLUID.Fieldset({parent: infoForm});
            
            // System info
            var systemField = new FLUID.Field.DisplayOnly({
                parent: infoFieldset, 
                name: 'system',
                label: _('System'),
                classes: ['fluid-info'] 
            });
            
            // Format the framework info to display
            var systemInfo = [];
            for (property in info) {
                if (info.hasOwnProperty(property) && property !== 'msg') {
                    systemInfo.push(property + ': ' + info[property]);
                }
            }
            systemInfo.sort();
            systemField.value(systemInfo.join('\n'));
            
            // Account information
            var accountField = new FLUID.Field.DisplayOnly({
                parent: infoFieldset, 
                name: 'account',
                label: _('Account'),
                classes: ['fluid-info']
            });
            accountInfo.push('ID: ' + user.getFirstName() + ' ' + user.getLastName() + ' [' + user.getEmail() + ']');
            accountInfo.push('');
            
            // Permissions
            accountInfo.push('** Permissions');
            for (i = 0; i < controllerNames.length; i++) {
                accountInfo.push('* ' + controllerNames[i] + ' \n- ' + user.getControllerViews(controllerNames[i]).join('\n- '));
                accountInfo.push('');
            }
            
            // Preferences
            accountInfo.push('** Preferences');
            for (i = 0; i < preferenceNames.length; i++) {
                accountInfo.push(preferenceNames[i] + ': ' + user.getPreferences(preferenceNames[i]));
            }
            accountField.value(accountInfo.join('\n'));

            var infoString = '**** System\n' + systemInfo.join('\n') + '\n\n**** Account\n' + accountInfo.join('\n');
            
            // Copy and Email buttons
            var emailBtn = new FLUID.Button({
                parent: win,
                group: 'actions',
                label: _('Email')
            });
            
            emailBtn.bind('onClick.deploy', function (ev, data) {
                window.location = 'mailto:' + DEPLOY.getAdministrator().getEmail() + '?subject=System Info...&body=' + escape(data.infoString);
                
            }, {infoString: infoString});
            
            // Render, Prepare & Open the window
            win.refresh();
            win.prepare();
            win.open();
        });
        
        infoReq.send();
    },

    defaultOptions: {}
}); new DEPLOY.DeploySiteController();