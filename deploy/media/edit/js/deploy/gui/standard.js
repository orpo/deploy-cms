/**
 * @fileoverview    Standard GUI setup.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

/* 
IMPORTANT! Do not alter this file to customise the GUI, instead add an 
additional file to the resource stack in the '/deploy/gui/' folder, and bind
to the initGUI/updateGUI events for the administrator.
*/

'use strict';


// App menus
DEPLOY.getAdministrator().bind('afterInitGUI', function (event, data) {
    
    var i;
    var j;
    var controller;
    var controllerNames;
    var controllerName;
    var controllerInfo;
    var groups;
    var group;
    var groupName;
    var menuItem;
    var parentMenu;
    var siteController;
    
    $$(document).bind('keydown.global.deploy', this, function (event) {
        
        var desktop = FLUID.widgetByName('desktop');
            
        // Support for escape key downloading the top level window in modal
        if (event.keyCode == 27) {
            
            // Check that we are in modal mode, else ignore the event
            if (desktop.getState() !== FLUID.Desktop.MODAL) {
                return;
            }
            
            // Find the top window
            var win = null;
            var children = desktop.getChildren('modal');
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                if (!child.isA('Window')) {
                    continue;
                }
                // Select the top window
                if (win === null || (win.style('z-index') || 0) < (child.style('z-index') || 0)) {
                    win = child;
                }
            }
            
            if (win !== null) {
                win.close();
            }
        }
        
        // Prevent back key leaving the page if in modal
        if (event.keyCode === 8) {
            // Are we in modal mode
            if (desktop.isModal()) {
                var focused = desktop.getHTMLSelection().find(':focus');
                if (focused.length === 0) {
                    event.preventDefault();
                    return;
                }
                var tagName = focused[0].tagName.toLowerCase();
                if (['input', 'textarea', 'select'].indexOf(tagName) === -1) {
                    event.preventDefault();
                    return;
                }
            }
        }
    });

    // Auto CRUD set-up
    controllerNames = this.getControllers();
    for (var i=0; i < controllerNames.length; i++) {
        var controllerName = controllerNames[i];
        
        // Check for pre-registered bespoke controller
        if (DEPLOY.getController(controllerName) === undefined) {
            controllerInfo = this.getController(controllerName); 
   
            // Create and register a default controller
            new DEPLOY.Controller({
                name: controllerName,
                verbose_name: controllerInfo['gui_verbose_name'],
                verbose_name_plural: controllerInfo['gui_verbose_name_plural'],
                inpage_editing: controllerInfo['gui_inpage_editing'],
                views: this.getControllerViews(controllerName)
                });
        }
    }
    
    // Application bar
    var desktop = FLUID.widgetByName('desktop');
    
    new FLUID.Button({
        parent: desktop.getApplicationBar(), 
        name: 'deploy.app.btn.edit-page',
        label: _('Edit page'),
        icon: 'deploy.icons.appbar.edit',
        tooltip: _('Edit the page'),
        enabled: false,
        classes: ['fluid-sep']
    });
    
    new FLUID.Button({
        parent: desktop.getApplicationBar(), 
        name: 'deploy.app.btn.edit-page-accept',
        label: _('Accept'),
        icon: 'deploy.icons.appbar.accept',
        tooltip: _('Accept page changes'),
        visible: false,
        classes: ['fluid-sep', 'fluid-green']
    });
    
    new FLUID.Button({
        parent: desktop.getApplicationBar(), 
        name: 'deploy.app.btn.edit-page-cancel',
        icon: 'deploy.icons.appbar.cancel',
        tooltip: _('Cancel page changes'),
        visible: false,
        classes: ['fluid-sep', 'fluid-red']
    });
    new FLUID.Separator({
        name: 'deploy.app.btn.edit-page-sep',
        parent: desktop.getApplicationBar(),
        visible: false
        });
        
    new FLUID.Button({
        parent: desktop.getApplicationBar(), 
        name: 'deploy.app.btn.page-properties',
        icon: 'deploy.icons.appbar.properties',
        tooltip: _('Manage page properties'),
        enabled: false,
        classes: ['fluid-sep']
    });
    new FLUID.Separator({parent: desktop.getApplicationBar()});
    new FLUID.Button({
        parent: desktop.getApplicationBar(),
        name: 'deploy.app.btn.new-page',
        icon: 'deploy.icons.appbar.new',
        tooltip: _('Create a new page'),
        enabled: false,
        classes: ['fluid-sep']
    });
    new FLUID.Separator({parent: desktop.getApplicationBar()});
    new FLUID.MenuButton({
        parent: desktop.getApplicationBar(),
        name: 'deploy.app.btn.site',
        label: 'Site',
        icon: 'deploy.icons.appbar.site',
        tooltip: _('Manage the site'),
        classes: ['fluid-sep']
    });
    new FLUID.MenuButton({
        parent: desktop.getApplicationBar(),
        name: 'deploy.app.btn.plug-ins',
        label: _('Plug-ins'),
        icon: 'deploy.icons.appbar.plug-ins',
        tooltip: _('Manage plug-ins'),
        enabled: false,
        classes: ['fluid-sep']
    });
    new FLUID.Separator({parent: desktop.getApplicationBar()});
    new FLUID.MenuButton({
        parent: desktop.getApplicationBar(),
        name: 'deploy.app.btn.desktop',
        label: _('Desktop'),
        icon: 'deploy.icons.appbar.desktop',
        tooltip: _('Manage the user interface'),
        classes: ['fluid-sep']
    });
    new FLUID.Separator({parent: desktop.getApplicationBar()});
    new FLUID.MenuButton({
        parent: desktop.getApplicationBar(),
        name: 'deploy.app.btn.me',
        label: DEPLOY.getAdministrator().getFirstName(),
        icon: 'deploy.icons.appbar.me',
        tooltip: _('Manage your account')
    });
    
    // Application bar menus
    
    // Site
    var siteMenu = new FLUID.Menu();
    FLUID.widgetByName('deploy.app.btn.site').menu(siteMenu);
    new FLUID.MenuItem({
        parent: siteMenu,
        name: 'deploy.app.menu.site.content', 
        label: _('Content')
    });
    new FLUID.MenuItem({
        parent: siteMenu,
        name: 'deploy.app.menu.site.layout', 
        label: _('Layout')
    });
    new FLUID.Separator({parent: siteMenu});
    new FLUID.MenuItem({
        parent: siteMenu,
        name: 'deploy.app.menu.site.administrators', 
        label: _('Administrators')
    });    
    new FLUID.Separator({parent: siteMenu});
    new FLUID.MenuItem({
        parent: siteMenu,
        name: 'deploy.app.menu.site.development', 
        label: _('Development')
    });    
    new FLUID.Separator({parent: siteMenu});
    new FLUID.MenuItem({
        parent: siteMenu,
        name: 'deploy.app.menu.site.stats', 
        label: _('Stats')
    });
    
    // Site > Content
    var contentSubMenu = new FLUID.Menu({name: 'deploy.app.menu.site.content.sub-menu'});
    FLUID.widgetByName('deploy.app.menu.site.content').menu(contentSubMenu);
    new FLUID.MenuItem({
        parent: contentSubMenu,
        name: 'deploy.app.menu.site.content.sub-menu.pages', 
        label: _('Pages')
    });
    new FLUID.MenuItem({
        parent: contentSubMenu,
        name: 'deploy.app.menu.site.content.sub-menu.documents', 
        label: _('Documents')
    });
    
    // Site > Layout    
    var layoutSubMenu = new FLUID.Menu({name: 'deploy.app.menu.site.layout.sub-menu'});
    FLUID.widgetByName('deploy.app.menu.site.layout').menu(layoutSubMenu);
    new FLUID.MenuItem({
        parent: layoutSubMenu,
        name: 'deploy.app.menu.site.layout.sub-menu.templates', 
        label: _('Templates')
    });
        
    // Site > Administrators
    var administratorsSubMenu = new FLUID.Menu({name: 'deploy.app.menu.site.administrators.sub-menu'});
    FLUID.widgetByName('deploy.app.menu.site.administrators').menu(administratorsSubMenu);
    new FLUID.MenuItem({
        parent: administratorsSubMenu,
        name: 'deploy.app.menu.site.administrators.sub-menu.manage-accounts', 
        label: _('Manage accounts')
    });
    new FLUID.MenuItem({
        parent: administratorsSubMenu,
        name: 'deploy.app.menu.site.administrators.sub-menu.view-activity', 
        label: _('View activity')
    });    
    
    // Site > Development
    var developmentSubMenu = new FLUID.Menu({name: 'deploy.app.menu.site.development.sub-menu'});
    FLUID.widgetByName('deploy.app.menu.site.development').menu(developmentSubMenu);
    new FLUID.MenuItem({
        parent: developmentSubMenu,
        name: 'deploy.app.menu.site.development.sub-menu.settings', 
        label: _('Settings')
    });
    new FLUID.Separator({parent: developmentSubMenu});
    new FLUID.MenuItem({
        parent: developmentSubMenu,
        name: 'deploy.app.menu.site.development.sub-menu.refresh-site', 
        label: _('Refresh site')
    });
    new FLUID.Separator({parent: developmentSubMenu});       
    new FLUID.MenuItem({
        parent: developmentSubMenu,
        name: 'deploy.app.menu.site.development.sub-menu.system-info', 
        label: _('System info')
    });     
    new FLUID.MenuItem({
        parent: developmentSubMenu,
        name: 'deploy.app.menu.site.development.sub-menu.logs', 
        label: _('Logs')
    });  
    
    // Plug-ins
    var plugInsMenu = new FLUID.Menu({name: 'deploy.app.menu.plug-ins'});
    FLUID.widgetByName('deploy.app.btn.plug-ins').menu(plugInsMenu);
    
	// Sort the groups
    groups = this.getControllerGroups();
	groups.sort();

    for (i=0; i < groups.length; i++) {
        group = groups[i];
        
		// Sort controllers by name
		controllerNames = this.getControllers(group);
        controllerNames.sort();
		
        for (j=0; j < controllerNames.length; j++) {
			
            controllerName = controllerNames[j];
            controllerInfo = this.getController(controllerName);
            controller = DEPLOY.getController(controllerName);
    
            if (controllerInfo['gui'] !== true || (
                    !this.can(controllerName, 'list') && 
                    !this.can(controllerName, 'search')   
                )) {
                continue;
            }
            
            // Support grouping
            parentMenu = plugInsMenu;
            if (controllerInfo['gui_grouping'] !== null) {
            
                // Format the group as a name
                groupName = UTILS.slugify(group);
                
                // Check a menu item exists for the grouping
                menuItem = FLUID.widgetByName('deploy.app.menu.plug-ins.' + groupName);
                
                if (menuItem === undefined) {
                    // Create the menu item and sub menu to add it too
                    parentMenu = new FLUID.Menu({
                        name: 'deploy.app.menu.plug-ins.' + groupName + '.sub-menu'
                        }); 
                    
                    new FLUID.MenuItem({
                        parent: plugInsMenu,
                        name: 'deploy.app.menu.plug-ins.' + groupName, 
                        label: _(UTILS.title(group)),
                        menu: parentMenu      
                    });
                } else {
                    parentMenu = menuItem.menu();
                }
            }
            
            // Add null group controllers at the top level
            menuItem = new FLUID.MenuItem({
                parent: parentMenu,
                name: parentMenu.getName() + '.' + controller.getName(), 
                label: _(UTILS.title(controller.getVerboseName()))
            });
            
            // Bind the list view to the menu item
            if (this.can(controllerName, 'search')) {
                menuItem.bind('onClick', function (event, data) {
                    data.controller.search();
                }, {controller: controller});
            } else {
                menuItem.bind('onClick', function (event, data) {
                    data.controller.list();
                }, {controller: controller});
            }
        }
    }
    
    if (plugInsMenu.getChildCount() > 0) {
        FLUID.widgetByName('deploy.app.btn.plug-ins').enable();
    }
    
    // Desktop
    var desktopMenu = new FLUID.Menu({name: 'deploy.app.menu.desktop'});
    FLUID.widgetByName('deploy.app.btn.desktop').menu(desktopMenu);
    new FLUID.MenuItem({
        parent: desktopMenu,
        name: 'deploy.app.menu.desktop.tools', 
        label: _('Tools')
    });
    new FLUID.Separator({parent: desktopMenu});
    new FLUID.MenuItem({
        parent: desktopMenu,
        name: 'deploy.app.menu.desktop.themes', 
        label: _('Themes')
    });    
    
    // Desktop > Tools
    var toolsSubMenu = new FLUID.Menu({name: 'deploy.app.menu.desktop.tools.sub-menu'});
    FLUID.widgetByName('deploy.app.menu.desktop.tools').menu(toolsSubMenu);
    new FLUID.MenuToggleItem({
        parent: toolsSubMenu,
        name: 'deploy.app.menu.desktop.tools.sub-menu.styles', 
        label: _('Styles')
    });
    new FLUID.Separator({parent: toolsSubMenu});
    new FLUID.MenuToggleItem({
        parent: toolsSubMenu,
        name: 'deploy.app.menu.desktop.tools.sub-menu.history', 
        label: _('History')
    });
    
    // Users
    var meMenu = new FLUID.Menu({name: 'deploy.app.menu.me'});
    FLUID.widgetByName('deploy.app.btn.me').menu(meMenu);
    
    new FLUID.MenuItem({
        parent: meMenu,
        name: 'deploy.app.menu.me.manage-my-account', 
        label: _('Manage my account')
    });
    new FLUID.MenuItem({
        parent: meMenu,
        name: 'deploy.app.menu.me.manage-my-preferences',
        label: _('Manage my preferences')
    });
    new FLUID.MenuItem({
        parent: meMenu,
        name: 'deploy.app.menu.me.view-my-activity',
        label: _('View my activity')
    });
    new FLUID.Separator({parent: meMenu});
    new FLUID.MenuItem({
        parent: meMenu,
        name: 'deploy.app.menu.me.help',
        label: _('Help')
    });
    new FLUID.Separator({parent: meMenu});
    new FLUID.MenuItem({
        parent: meMenu,
        name: 'deploy.app.menu.me.sign-out',
        label: _('Sign out')
    });    
    
    // Hard coded events
    
    // Content
    if (this.can('deploy-page', 'list')) {
        FLUID.widgetByName('deploy.app.menu.site.content.sub-menu.pages').bind('onClick',
            function (event, data) {
                DEPLOY.getController('deploy-page').list();
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.content.sub-menu.pages').enable(false);
    }
    
    if (this.can('deploy-document', 'list')) {
        FLUID.widgetByName('deploy.app.menu.site.content.sub-menu.documents').bind('onClick',
            function (event, data) {
                DEPLOY.getController('deploy-document').list();
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.content.sub-menu.documents').enable(false);
    }
        
    // Layout
    if (this.can('deploy-template', 'list')) {
        FLUID.widgetByName('deploy.app.menu.site.layout.sub-menu.templates').bind('onClick',
            function (event, data) {
                DEPLOY.getController('deploy-template').list();
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.layout.sub-menu.templates').enable(false);
    }
    
    // Site
    siteController = DEPLOY.getController('deploy-site');
    
    if (this.can('deploy-site', 'development-settings')) {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.settings').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Settings - not implemented yet!');
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.settings').enable(false);
    }

    if (this.can('deploy-site', 'development-refresh')) {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.refresh-site').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Refresh site - not implemented yet!');
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.refresh-site').enable(false);
    }
    if (this.can('deploy-site', 'system-info')) {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.system-info').bind('onClick',
            function (event, data) {
                siteController.systemInfo();
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.system-info').enable(false);
    }
    
    if (this.can('deploy-site', 'development-logs')) {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.logs').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Logs - not implemented yet!');
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.site.development.sub-menu.logs').enable(false);
    }

    if (this.can('deploy-site', 'stats')) {
        FLUID.widgetByName('deploy.app.menu.site.stats').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Stats - not implemented yet!');
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.site.stats').enable(false);
    }

    // Desktop
    FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').enable(false);
    FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.history').enable(false);
    if (this.can('deploy-site', 'get-themes')) {
        FLUID.widgetByName('deploy.app.menu.desktop.themes').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Themes - not implemented yet!');
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.desktop.themes').enable(false);
    }
    
    if (this.can('deploy-page', 'get-styles')) {
        FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Styles tool - not implemented yet!');
            });
        FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.styles').enable();
    }

    if (this.can('deploy-page', 'get-history')) {
        FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.history').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Styles tool - not implemented yet!');
            });
        FLUID.widgetByName('deploy.app.menu.desktop.tools.sub-menu.history').enable();
    }
    
    // Help
    FLUID.widgetByName('deploy.app.menu.me.help').bind('onClick',
        function (event, data) {
            // Open help URL in new window
            window.open('http://support.deploycms.com/');
        });    
            
    desktop.refresh();
    
    // Add Google Map support
    function loadScript() {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'http://maps.googleapis.com/maps/api/js?sensor=false&callback=DEPLOY_InitGoogleMaps';
        document.body.appendChild(script);
    }
    loadScript();
    
    // Update the interface (based on the users preferences)
    this.updateGUI();

    // Setup the editable environment
    var masterController = DEPLOY.getAdministrator().getMasterController();
    DEPLOY.getEditableEnvironment(); // Initial get will setup the environment
}); 

DEPLOY.getAdministrator().bind('afterUpdateGUI', function (event, data) {
    
});

function DEPLOY_InitGoogleMaps() {
    // A callback function is required for google maps API to initialize 
    // properly.
}