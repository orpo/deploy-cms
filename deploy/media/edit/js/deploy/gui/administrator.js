/**
 * @fileoverview    Administrator GUI setup.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


// App menus
DEPLOY.getAdministrator().bind('afterInitGUI', function (event, data) {
    
    // Managing the administrators account

    // Managing my account
    FLUID.widgetByName('deploy.app.menu.me.manage-my-account').bind('onClick',
        function (event, data) {
            DEPLOY.getController('deploy-administrator').update(DEPLOY.getAdministrator().getID());
        });
    
    // Managing my preferences
    if (this.can('deploy-administrator', 'update-my-preferences')) {
        FLUID.widgetByName('deploy.app.menu.me.manage-my-preferences').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('Manage my preferences - not implemented yet!');
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.me.manage-my-preferences').enable(false);
    }
        
    // View my activity
    if (this.can('deploy-administrator', 'view-my-activity')) {
        FLUID.widgetByName('deploy.app.menu.me.view-my-activity').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('View my activity - not implemented yet!');
            });
    } else {
        FLUID.widgetByName('deploy.app.menu.me.view-my-activity').enable(false);
    }
        
    // Sign-out
    FLUID.widgetByName('deploy.app.menu.me.sign-out').bind('onClick',
        function (event, data) {
            DEPLOY.getController('deploy-administrator').signOut();
        });
    
    // Managing other administrator accounts
    
    // Manage user accounts
    if (this.can('deploy-administrator', 'list')) {
        FLUID.widgetByName('deploy.app.menu.site.administrators.sub-menu.manage-accounts').bind('onClick',
            function (event, data) {
                DEPLOY.getController('deploy-administrator').list();
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.administrators.sub-menu.manage-accounts').enable(false);
    }
    
    // View user activity
    if (this.can('deploy-administrator', 'view-activity')) {
        FLUID.widgetByName('deploy.app.menu.site.administrators.sub-menu.view-activity').bind('onClick',
            function (event, data) {
                throw new DEPLOY.Error('View activity - not implemented yet!');
            });    
    } else {
        FLUID.widgetByName('deploy.app.menu.site.administrators.sub-menu.view-activity').enable(false);
    }
    
}); 

DEPLOY.getAdministrator().bind('afterUpdateGUI', function (event, data) {
    
});
