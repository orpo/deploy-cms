/**
 * @fileoverview    Deploy pagination class.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        IO (/js/core/io.js)
 */

'use strict';

/**
 * A class for paging records.
 * @class A paginator class.
 * @constructor
 */
DEPLOY.Paginator = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._page = null;
    
    /** @private */
    this._sortColumn = null;
    
    /** @private */
    this._sortDirection = 'ASC';
    
    /** @private */
    this._searchOptions = {};
    
    /** @private */
    this._controllerName = options.controllerName;
    
    /** @private */
    this._view = options.view;
}
UTILS.copyPrototype(DEPLOY.Paginator, EVENT.EventClass);

$$.extend(DEPLOY.Paginator.prototype, {
    __className__: 'DEPLOY.Paginator',

    /**
     * Return the current page object.
     * @return The page object
     * @type Page
     */
    getPage: function () {
        return this._page;
    },

    // Search
    
    /**
     * Set the search options for results.
     * @param {Object} options The search options applied to any request for
     * results.
     */
    searchOptions: function (options) {
        if (options === undefined) {
            return this._searchOptions;
        } else { 
            this._searchOptions = options;
        }
    },
    
    /**
     * Set the sort for results.
     * @param {String} column The name of the column to sort by.
     * @param {String} direction The direction to sort the results in, either 
     * 'ASC' (ascending) or 'DESC' (descending).
     */
    setSort: function (column, direction) {
        this._sortColumn = column;
        this._sortDirection = direction;
    },
    
    // Navigation
    
    /**
     * Go to page.
     * @param {Number} pageNumber The number of the page to get.
     */
    goToPage: EVENT.classMethod(DEPLOY.Paginator, 
        function goToPage() {        
			var localArgs = EVENT.getArgs(arguments);
            var pageNumber = EVENT.getArg(localArgs, 0);
            
            // Build the paging data
            var pagingData = {page: pageNumber};
            if (this._sortColumn !== null) {
                pagingData['page_order'] = this._sortColumn;
            }
            if (this._sortColumn !== null) {
                pagingData['page_order_direction'] = this._sortDirection;
            }
            for (var option_name in this._searchOptions) {
                if (this._searchOptions.hasOwnProperty(option_name)) {
                    pagingData[option_name] = this._searchOptions[option_name];
                }
            }
            
            // Request the page from the server
            var pageReq = new IO.Request({
                controller: this._controllerName,
                method: this._view,
                data: pagingData
                });
            
            // Handle return of the form JSON
            pageReq.bind('afterSuccess', function (event, data) {
                var paginator = data.paginator;
                
                // Update the page object
                paginator._page = event.data.data.page;

                // Trigger the page load event
                paginator.trigger('goToPageComplete', 
                    function (event) {}, 
                    {data: {search_form: event.data.data.search_form}
                });
                
            }, {paginator: this});
            
            // Handle failure to get the page     
            pageReq.bind('afterFail', function (event, data) {
                data.paginator._page = undefined;
                data.paginator.trigger('goToPageComplete', function (event) {});
                
            }, {paginator: this});
            
            pageReq.send();
        }, 'goToPage'),
    
    /**
     * Go to the next page.
     */
    nextPage: function () {
        this.goToPage(this._page.next_page_number);
    },
     
    /**
     * Go to the previous page.
     */
    previousPage: function () {
        this.goToPage(this._page.previous_page_number);
    } 
});


/**
 * A table header for pagination.
 * @class A data table header class.
 * @constructor
 */
DEPLOY.Paginator.DataHeader = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.DataTableHead.call(this, options, false);
    this._type.push('Paginator.DataHeader');
    
    /** @private */
    this._paginator = options.paginator;
    
    /** @private */
    this._name = options.name;
    
    /** @private */
    this._direction = options.direction;
    
    /** @private */
    this._selected = options.selected;
    
    /** @private */
    this._sortable = options.sortable;
    
    if (this._sortable) {
        if (this._selected) {
            this.addClass('fluid-selected');
        }
        if (this._direction == 'ASC') {
            this.addClass('deploycms-asc');
        } else {
            this.addClass('deploycms-desc');
        }
        this.addClass('deploycms-sortable');
    } else {
    }
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(DEPLOY.Paginator.DataHeader, FLUID.DataTableHead);

$$.extend(DEPLOY.Paginator.DataHeader.prototype, {
    __className__: 'FLUID.Paginator.DataHeader',
    __css__: 'fluid-paginator-data-header',
    __frameworkEvents__: {
        'mouseover': 'mouseOver',
        'mouseout': 'mouseOut',
        'mouseleave': 'mouseLeave',
        'mouseenter': 'mouseEnter',
        'mousemove': 'mouseMove',
        'mousedown': 'mouseDown',
        'mouseup': 'mouseUp',
        'click': 'click',
        'dblclick': 'doubleClick'
        },
        
    /** 
     * Select the specified page.
     */
    select: EVENT.classMethod(DEPLOY.Paginator.DataHeader, function select() {  
        if (!this._sortable) {
            return;
        }
    
        // Sort the paginators output and reset the page to 1
        if (!this._selected) {
            this._selected = true;
            this._direction = 'ASC';
        } else {
            if (this._direction === 'ASC') {
                this._direction = 'DESC';
            } else {
                this._direction = 'ASC';
            }
        }
        this._paginator.setSort(this._name, this._direction);
        this._paginator.goToPage(1);
        
    }, 'select'),
    
    /** @private */
    _bindEvents: function () {
        this.bind('onMouseEnter.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        this.bind('onMouseLeave.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        this.bind('onMouseDown.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.removeClass(FLUID._cssName('over'));
                this.addClass(FLUID._cssName('down'));
            }
        });
        this.bind('onMouseUp.fluid', function (event) {
            if (this.exists() && this.enabled()) {
                this.addClass(FLUID._cssName('over'));
                this.removeClass(FLUID._cssName('down'));
            }
        });
        this.bind('onClick.deploycms', function(event) {
            this._select();
        });
    },
    
    /**
     * The default options when creating a new pagination data header.
     * @type Object
     */
    defaultOptions: {
        value: '',
        visible: true,
        enabled: true,
        name: '',
        direction: 'ASC',
        selected: false,
        sortable: false
    }
});

/**
 * A table cell for pagination.
 * @class A data table cell class.
 * @constructor
 */
DEPLOY.Paginator.DataCell = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    FLUID.DataTableCell.call(this, options, false);
    this._type.push('Paginator.DataCell');
    
    /** @private */
    this._itemId = options.itemId;
    
    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
};
UTILS.copyPrototype(DEPLOY.Paginator.DataCell, FLUID.DataTableCell);

$$.extend(DEPLOY.Paginator.DataCell.prototype, {
    __className__: 'fluid-paginator-data-cell',
    __css__: FLUID._cssName('PaginatorDataCell'),
    
    /**
     * Return the item ID associate with the cell.
     * @return The cells item ID.
     * @type Number|String
     */ 
    getItemID: function () {
        return this._itemId;
    },
    
    /** @private */
    _bindEvents: function () {
        this.bind('onMouseEnter.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.getParent().addClass(FLUID._cssName('over'));
            }
        });
        this.bind('onMouseLeave.fluid', function(event) {
            if (this.exists() && this.enabled()) {
                this.getParent().removeClass(FLUID._cssName('over'));
            }
        });
    },
    
    /**
     * The default options when creating a new data table head.
     * @type Object
     */
    defaultOptions: {
        value: '',
        visible: true,
        enabled: true
    }
});