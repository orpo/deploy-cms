/**
 * @fileoverview    Deploy controller class to implement CRUD (part of the 
 * DeployCMS client-side library).
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        IO (/js/core/io.js)
 * @requires        PAGINATION (/js/core/pagination.js)
 */

'use strict';

/**
 * Register a controller.
 * @param {DEPLOY.Controller} controller The controller to register.
 */ 
DEPLOY.registerController = function (controller) {
    DEPLOY._controllers[controller.getName().toLowerCase()] = controller;
}

/**
 * Return the registered controller associated with the specified name.
 * @param {String} name The name of the controller being requested.
 * @return A registered controller.
 * @type DEPLOY.Controller
 */ 
DEPLOY.getController = function (name) {
    return DEPLOY._controllers[name.toLowerCase()];
}


/**
 * A controller class to support CRUD for the DeployCMS administration
 * environment.
 * @class An administrator class.
 * @constructor
 */
DEPLOY.Controller = function (options) {
    EVENT.EventClass.call(this);
    options = UTILS.optionsArg(options, this.defaultOptions);
    
    /** @private */
    this._name = options.name;
    
    /** @private */
    this._verbose_name = options.verbose_name || this._name.toLowerCase();
    
    /** @private */
    this._verbose_name_plural = options.verbose_name_plural || 
        this._verbose_name + 's';

    /** @private */
    this._uc_verbose_name = this._verbose_name.slice(0,1).toUpperCase() + this._verbose_name.slice(1);

    /** @private */
    this._uc_verbose_name_plural = this._verbose_name_plural.slice(0,1).toUpperCase() + this._verbose_name_plural.slice(1);

    /* @private */
    this._visit_page_label = options.visit_page_label;
    
    /* @private */
    this._visit_page_new_window = options.visit_page_new_window;

    /* @private */
    this._inpage_editing = options.inpage_editing || false;

    /** @private */
    this._views = options.views || [];

    // Register the controller
    DEPLOY.registerController(this);
}
UTILS.copyPrototype(DEPLOY.Controller, EVENT.EventClass);

// Default window sizes
DEPLOY.Controller.DEFAULT_WINDOW_SIZES = {
	'alert': [500, 'auto'],
    'form': [[644, 'auto'], [644, 100], [644, 0]],
    'list': [[800, 600], [644, 400], [0, 0]]
}

$$.extend(DEPLOY.Controller.prototype, {
    __className__: 'DEPLOY.Controller',

    /**
     * Return the name of the controller.
     * @return The controller's name.
     * @type String
     */
    getName: function () {
        return this._name;
    },
    
    /**
     * Return the verbose name of the controller.
     * @return The controller's verbose name.
     * @type String
     */
    getVerboseName: function () {
        return this._verbose_name;
    },
    
    /**
     * Return the verbose ploral name of the controller.
     * @return The controller's verbose name.
     * @type String
     */
    getVerboseNamePloral: function () {
        return this._verbose_name_plural;
    },
    
    /**
     * Return true if the controller supports in page editing.
     * @return Whether the controller supports in-page editing.
     * @type Boolean
     */
    inpage: function () {
        return this._inpage_editing;
    },
    
    /**
     * Default search view.
     */
    search: function () {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
		// If the window is already open then don't allow it to be opened
		// again, instead bring it to the front.
		if (FLUID.widgetByName('deploy-window-search-' + this.getName())) {
			FLUID.widgetByName('deploy-window-search-' + this.getName()).minimize(false);
			FLUID.widgetByName('deploy-window-search-' + this.getName()).sendToFront();
			return;
		}
		
        // Create the paginator
        var paginator = new DEPLOY.Paginator({
            controllerName: this.getName(), 
            view: 'search'
        });
        
        this.trigger('searchReady', function (event, data) {
			var paginator = event.data.paginator;
			
	        // Create the search window
	        var win = new FLUID.Window({
	            name: 'deploy-window-search-' + this.getName(),
	            group: 'modal',
	            label: this._uc_verbose_name_plural,
	            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][0].slice(0),
	            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][1].slice(0),
	            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][2].slice(0),
	            flags: 
	                FLUID.Window.CAN_CLOSE |
	                FLUID.Window.CAN_MINIMIZE |
	                FLUID.Window.CAN_MAXIMIZE |
	                FLUID.Window.CAN_RESIZE |
	                FLUID.Window.CAN_DRAG
	            });
        
	        // Handle window events
	        win.bind('afterCloseComplete', function(event, data) {
	            data.ctr.trigger('searchClosed', function(event) {
	                var desktop = FLUID.widgetByName('desktop');
	                var win = event.data.win;
                
	                // Remove the window
	                win.destroy();
                
	                // If there are still other windows then we should remain in modal
	                // mode...
	                var children = desktop.getChildren('modal');
	                for (var i = 0; i < children.length; i++) {
	                    if (children[i].typeOf('Window')) {
	                        return;
	                    }
	                }
                
	                // ...else we can exit modal mode
	                desktop.endModal(FLUID.Desktop.MODAL);
	            }, {data: {win: this}});
	        }, {ctr: this});
        
	        // Set up the panels/groups for the view
	        var panels = {
	            'search': new FLUID.Panel({
	                parent: win,
	                classes: ['deploycms-list-search']
	            }),
	            'body': new FLUID.Panel({
	                parent: win, 
	                classes: ['deploycms-list-body'],
	                style: {
	                    height: function (body) {
	                        if (!body.exists()) {
	                            return;
	                        }
	                        var win = body.getParent();
	                        var winHTML = win.getHTMLSelection();
	                        var height = winHTML.height();
                        
	                        // Allow for title
	                        height -= winHTML.children('.fluid-title').outerHeight(true);
                        
	                        // Allow for info & action areas
	                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
	                            height -= winHTML.children('.fluid-info').outerHeight(true);
	                        }
	                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
	                            height -= winHTML.children('.fluid-actions').outerHeight(true);
	                        }
                        
	                        // Allow for search form
	                        var searchForm = body.prevSibling();
	                        height -= searchForm.getHTMLSelection().outerHeight(true);
                        
	                        // Allow for padding on client
	                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());
                        
	                        return height;
	                    }
	                }
	            })
	        };

	        panels.data = new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-data-view'],
	            style: {
	                height: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var height = body.style('height');
                    
	                    // Allow for navigation
	                    var navigation = data.nextSibling().nextSibling();
	                    height -= navigation.getHTMLSelection().outerHeight(true);
                    
	                    return height;
	                },
	                width: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var width = body.getHTMLSelection().width();
                    
	                    // Allow for actions
	                    var actions = data.nextSibling();
	                    if (actions.visible()) {
							width -= actions.getHTMLSelection().outerWidth(true);
                    	}
	                    return width;
	                }
	            }
	        });
        
	        panels.actions = new FLUID.Group({
	            parent: panels.body,
	            legend: 'Actions',
	            classes: ['deploycms-list-actions']
	        });

	        panels.navigation =  new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-navigation'],
	            style: {
	                width: function (navigation) {
	                    if (!navigation.exists()) {
	                        return;
	                    }
	                    var data = navigation.prevSibling().prevSibling();
	                    return data.style('width');
	                }
	            }
	        });      
        
	        // Set up the data table
	        var dataTable = new FLUID.DataTable({
	            parent: panels.data, 
	            headerClass: DEPLOY.Paginator.DataHeader, 
	            cellClass: DEPLOY.Paginator.DataCell,
	            classes: ['deploycms-list-data']
	            });

	        // Set up update or view action    
	        function updateRecord(event, data) {
	            data.ctr.update(data.itemId);
	        };   

		    if (!user.can(this.getName(), 'update')) {
                updateRecord = function () {
                    FLUID.widgetByName('desktop').notify(_('Permission denied'));
                }
	        }		
		
            dataTable.bind('afterAddCell', function(event, data) {
                event.data.cell.bind('onClick', data.func, {
                    ctr: data.ctr,
                    itemId: event.data.cell.getItemID()
                });          
            }, {ctr: this, func: updateRecord});
    
	        // Set up standard actions
	        if (user.can(this.getName(), 'add')) {
            
	            // Add a record
	            var addBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.add',
	                label: _('Add') + ' ' + this.getVerboseName()
	            });
	            addBtn.bind('onClick', function (event, data) { 
	                data.ctr.add();
	            }, {ctr: this});
            
	            // Add a separator if other actions are supported
	            if (user.can(this.getName(), 'import-csv') ||
	                    user.can(this.getName(), 'export-csv')) {
	                new FLUID.Separator({parent: panels.actions});
	            }
	        }
        
	        // Import/Export records
	        if (user.can(this.getName(), 'import-csv')) {
	            var importBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.import',
	                label: _('Import CSV')
	            });
	            importBtn.bind('onClick', function (event, data) { 
	                data.ctr.import_csv();
	            }, {ctr: this});
	        }
	        if (user.can(this.getName(), 'export-csv')) {
	            var exportBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.export',
	                label: _('Export CSV')
	            });
	            exportBtn.bind('onClick', function (event, data) { 
	                data.ctr.export_csv();
	            }, {ctr: this});
	        }
        
			// If there are no standard actions we hide the actions panel
			if (panels.actions.getChildCount() === 0) {
				panels.actions.hide();
			}
			
	        // Set up naviagtion
        
	        // Go form
	        var navigationForm = new FLUID.Form({parent: panels.navigation}); 
	        navigationForm.bind('onSend', function (ev) {
	            ev.stopDefault();
	            var data = this.getData();
	            var page = parseInt(data.page);
            
	            if (page >= 1 && page <= paginator.getPage().page_count) {
	                paginator.goToPage(page);
	            } else {
	                FLUID.widgetByName('desktop').notify(_('You have entered an invalid page number'));
	            }
	        }, {paginator: paginator});
	        var navigationFieldset = new FLUID.Fieldset({
	            parent: navigationForm
	        });
	        var goField = new FLUID.Field.Number({
	            parent: navigationFieldset,
	            name: 'page',
	            label: 'Go to page',
	            min: 1,
	            max: 1,
	            value: 1
	        });
	        var goBtn = new FLUID.Button({
	            parent: navigationFieldset,
	            icon: 'deploy.icons.views.go',
	            label: _('Go'),
	            classes: ['fluid-icon-right']
	        });
	        goBtn.bind('onClick', function (event, data) { 
	            data.form.send();
	        }, {form: navigationForm});
        
	        // Next & Previous buttons
	        var nextBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.next',
	            label: _('Next'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-icon-right']
	        });
	        nextBtn.bind('onClick', function (event, data) { 
	            event.stopDefault();
	            if (this.enabled()) {
	                data.paginator.nextPage();
	            }
	        }, {paginator: paginator});
            
	        var prevBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.prev',
	            label: _('Prev'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-sep']
	        });
	        prevBtn.bind('onClick', function (event, data) { 
	            if (this.enabled()) {
	                data.paginator.previousPage();
	            }
	        }, {paginator: paginator});
        
	        // Handle page loads
	        paginator.bind('afterGoToPageComplete', function (event, data) {
	            var desktop = FLUID.widgetByName('desktop');
	            var ctr = data.ctr;
	            var win = data.win;
	            var dataTable = data.dataTable;
	            var navigationForm = data.navigationForm;
	            var prevBtn = data.prevBtn;
	            var nextBtn = data.nextBtn;
	            var page = this.getPage();
            
	            // Search form
	            var searchForm = null;
	            if (panels.search.getChildren().length === 0) {
	                if (event.data !== undefined && event.data.search_form !== null) {
                    
	                    // Create the form
	                    var searchForm = DEPLOY.createForm({
							controller: ctr,
							view: 'PASSIVE',
							json: {form: event.data.search_form}
						});
        
	                    searchForm.bind('onSend', function (event, data) {
	                        event.stopDefault();
                        
	                        paginator.searchOptions(this.getData());
	                        paginator.goToPage(1);
                        
	                    }, {paginator: this});
                    
	                    // Add the apply button to the form
	                    var applyBtn = new FLUID.Button({
	                        parent: searchForm.getChildren()[0],
	                        label: _('Apply'),
	                        classes: ['fluid-right']
	                    });
	                    applyBtn.bind('onClick', function (event, data) { 
							data.form.send();
	                    }, {form: searchForm});
                    
	                    // Add the form to the Window
	                    panels.search.appendChild(searchForm);
	                }
	            } else {
	                searchForm = panels.search.getChildren()[0];
	            }
            
	            // Check for empty page
	            if (page !== undefined) {
	                win.label(ctr._uc_verbose_name_plural + ' (Page ' + page.number + ' of ' + page.page_count + ')');
                
	                // Update the table's header
	                for (var i = 0; i < page.columns.length; i++) {
	                    page.columns[i]['paginator'] = paginator;
	                    page.columns[i]['selected'] = false;
	                    if (page.columns[i]['name'] === this._sortColumn) {
	                        page.columns[i]['selected'] = true;
	                        page.columns[i]['direction'] = this._sortDirection;
	                    }
	                }
	                dataTable.header(page.columns);
                
	                // Update the table's data
	                var rows = [];
	                for (var i = 0; i < page.items.length; i++) {
	                    var item = page.items[i];
	                    var row = [];
	                    for (var j = 0; j < page.columns.length; j++) {
	                        var column = page.columns[j];
	                        row.push({
	                            controller: ctr,
	                            itemId: item['_id'],
	                            value: item[column.name]
	                            });
	                    }
	                    rows.push(row);
	                }
	                dataTable.data(rows);
                
	                // Update navigation
	                navigationForm.getFieldMap()['page'].max(page.page_count);
	                navigationForm.getFieldMap()['page'].value(page.number);
	                prevBtn.enable(page.has_previous_page);
	                nextBtn.enable(page.has_next_page); 
	            } else { 
	                win.label(ctr._verbose_name_plural + ' (Page 1 of 1)');
	                navigationForm.getFieldMap()['page'].max(1);
	                navigationForm.getFieldMap()['page'].value(1);
	                dataTable.header([]);
	                dataTable.data([]);
                
	                FLUID.widgetByName('desktop').notify(_('No records found'));
	            }
            
	            this.trigger('searchGoToPage', 
	                function (event) {
	                    var win = event.data.win;
	                    var dataTable = event.data.dataTable;
	                    if (!win.exists()) {
	                        // If this is the first page load then we need to open the 
	                        // window.
	                        win.refresh();
	                        win.prepare(function (win) { 
	                            win.center();
								win.sendToFront();
	                        });
	                        win.open();
	                    } else {
	                        dataTable.refresh();
	                    }     
	                }, {
	                data: {
	                    win: win, 
	                    paginator: paginator, 
	                    dataTable: dataTable, 
	                    searchForm: searchForm
	                }
	            });
	        }, {
	            ctr: this,
	            win: win,
	            dataTable: dataTable,
	            navigationForm: navigationForm,
	            prevBtn: prevBtn,
	            nextBtn: nextBtn
	        });
        
			// Store useful references against the window
			win.widgetData('paginator', paginator);
			win.widgetData('panels', panels);
			win.widgetData('navigationForm', navigationForm);
			
		}, {data: {paginator: paginator}});
		
        // Get the first page
        this.trigger('searchInit', 
            function (event) {
                event.data.paginator.goToPage(1);
            },
            {data: {paginator: paginator}}
        );
    },

    /**
     * Default search support in tabs.
     */
    searchInTab: function (tab) {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        var paginator = tab.widgetData('paginator');
        var tabPanel = tab.panel();
        
        // Has the tab been initialised before?
        if (paginator !== undefined) {
            // Reload the current data and return
            if (paginator.getPage()) {
            	paginator.goToPage(paginator.getPage().number);
            } else {
            	paginator.goToPage(1);
            }
            return;
        }
        
        /* First time call to the tab */
        tabPanel.rename('deploy-tab-search-' + this.getName());
 
        // Create the paginator
        var paginator = new DEPLOY.Paginator({
            controllerName: this.getName(), 
            view: 'search'
        });
        tabPanel.widgetData('paginator', paginator);
        
        this.trigger('searchInTabReady', function (event, data) {
			var paginator = event.data.paginator;
			var tab = event.data.tab;
			var tabPanel = event.data.tabPanel;
			var tabber = tabPanel.getParent();
			var win = tabber.getParent();
		
		    // Set up the panels/groups for the view
	        var panels = {
	            'search': new FLUID.Panel({
	                parent: tabPanel,
	                classes: ['deploycms-list-search']
	            }),
	            'body': new FLUID.Panel({
	                parent: tabPanel, 
	                classes: ['deploycms-list-body'],
	                style: {
	                    height: function (body) {
	                        if (!body.exists()) {
	                            return;
	                        }
	                        var tabber = body.getParent().getParent();
	                        var tabberHTML = tabber.getHTMLSelection();
	                        var win = body.getParent().getParent().getParent();
	                        var winHTML = win.getHTMLSelection();
	                        var height = winHTML.height();
                        
	                        // Allow for title
	                        height -= winHTML.children('.fluid-title').outerHeight(true);
                        
	                        // Allow for info & action areas
	                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
	                            height -= winHTML.children('.fluid-info').outerHeight(true);
	                        }
	                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
	                            height -= winHTML.children('.fluid-actions').outerHeight(true);
	                        }
                            
                            // Need to account for the height of the tabs and padding/margin on the tabber
                            height -= tabber.getTab(0).getHTMLSelection().outerHeight(true);
                            height -= (tabberHTML.children('.fluid-client').outerHeight(true) - tabberHTML.children('.fluid-client').height());
                            
	                        // Allow for search form
	                        var searchForm = body.prevSibling();
	                        height -= searchForm.getHTMLSelection().outerHeight(true);
	                        
	                        // Allow for padding on client
	                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());
                        
	                        return height;
	                    }
	                }
	            })
	        };
            
	        panels.data = new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-data-view'],
	            style: {
	                height: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var height = body.style('height');
                    
	                    // Allow for navigation
	                    var navigation = data.nextSibling().nextSibling();
	                    height -= navigation.getHTMLSelection().outerHeight(true);
                    
	                    return height;
	                },
	                width: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var width = body.getHTMLSelection().width();
                    
	                    // Allow for actions
	                    var actions = data.nextSibling();
	                    if (actions.visible()) {
							width -= actions.getHTMLSelection().outerWidth(true);
                    	}
                    	
	                    return width;
	                }
	            }
	        });
        
	        panels.actions = new FLUID.Group({
	            parent: panels.body,
	            legend: 'Actions',
	            classes: ['deploycms-list-actions']
	        });            
            
	        panels.navigation =  new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-navigation'],
	            style: {
	                width: function (navigation) {
	                    if (!navigation.exists()) {
	                        return;
	                    }
	                    var data = navigation.prevSibling().prevSibling();
	                    return data.style('width');
	                }
	            }
	        });      
        
	        // Set up the data table
	        var dataTable = new FLUID.DataTable({
	            parent: panels.data, 
	            headerClass: DEPLOY.Paginator.DataHeader, 
	            cellClass: DEPLOY.Paginator.DataCell,
	            classes: ['deploycms-list-data']
	            });

	        // Set up update or view action    
	        function updateRecord(event, data) {
	            data.ctr.update(data.itemId);
	        };   
		
    		if (!user.can(this.getName(), 'update')) {
                updateRecord = function () {
                    FLUID.widgetByName('desktop').notify(_('Permission denied'));
                }
	        }
	        
            dataTable.bind('afterAddCell', function(event, data) {
                event.data.cell.bind('onClick', data.func, {
                    ctr: data.ctr,
                    itemId: event.data.cell.getItemID()
                });          
            }, {ctr: this, func: updateRecord});
        
	        // Set up standard actions
	        if (user.can(this.getName(), 'add')) {
            
	            // Add a record
	            var addBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.add',
	                label: _('Add') + ' ' + this.getVerboseName()
	            });
	            addBtn.bind('onClick', function (event, data) { 
	                data.ctr.add();
	            }, {ctr: this});
            
	            // Add a separator if other actions are supported
	            if (user.can(this.getName(), 'import-csv') ||
	                    user.can(this.getName(), 'export-csv')) {
	                new FLUID.Separator({parent: panels.actions});
	            }
	        }
        
	        // Import/Export records
	        if (user.can(this.getName(), 'import-csv')) {
	            var importBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.import',
	                label: _('Import CSV')
	            });
	            importBtn.bind('onClick', function (event, data) { 
	                data.ctr.import_csv();
	            }, {ctr: this});
	        }
	        if (user.can(this.getName(), 'export-csv')) {
	            var exportBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.export',
	                label: _('Export CSV')
	            });
	            exportBtn.bind('onClick', function (event, data) { 
	                data.ctr.export_csv();
	            }, {ctr: this});
	        }
        
			// If there are no standard actions we hide the actions panel
			if (panels.actions.getChildCount() === 0) {
				panels.actions.hide();
			}
			
	        // Set up naviagtion
        
	        // Go form
	        var navigationForm = new FLUID.Form({parent: panels.navigation}); 
	        navigationForm.bind('onSend', function (ev) {
	            ev.stopDefault();
	            var data = this.getData();
	            var page = parseInt(data.page);
            
	            if (page >= 1 && page <= paginator.getPage().page_count) {
	                paginator.goToPage(page);
	            } else {
	                FLUID.widgetByName('desktop').notify(_('You have entered an invalid page number'));
	            }
	        }, {paginator: paginator});
	        var navigationFieldset = new FLUID.Fieldset({
	            parent: navigationForm
	        });
	        var goField = new FLUID.Field.Number({
	            parent: navigationFieldset,
	            name: 'page',
	            label: 'Go to page',
	            min: 1,
	            max: 1,
	            value: 1
	        });
	        var goBtn = new FLUID.Button({
	            parent: navigationFieldset,
	            icon: 'deploy.icons.views.go',
	            label: _('Go'),
	            classes: ['fluid-icon-right']
	        });
	        goBtn.bind('onClick', function (event, data) { 
	            data.form.send();
	        }, {form: navigationForm});
        
	        // Next & Previous buttons
	        var nextBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.next',
	            label: _('Next'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-icon-right']
	        });
	        nextBtn.bind('onClick', function (event, data) { 
	            event.stopDefault();
	            if (this.enabled()) {
	                data.paginator.nextPage();
	            }
	        }, {paginator: paginator});
            
	        var prevBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.prev',
	            label: _('Prev'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-sep']
	        });
	        prevBtn.bind('onClick', function (event, data) { 
	            if (this.enabled()) {
	                data.paginator.previousPage();
	            }
	        }, {paginator: paginator});
        

	        // Handle page loads
	        paginator.bind('afterGoToPageComplete', function (event, data) {
	            var desktop = FLUID.widgetByName('desktop');
	            var ctr = data.ctr;
	            var tab = data.tab;
	            var tabPanel = data.tabPanel;
	            var dataTable = data.dataTable;
	            var navigationForm = data.navigationForm;
	            var prevBtn = data.prevBtn;
	            var nextBtn = data.nextBtn;
	            var page = this.getPage();
            
	            // Search form
	            var searchForm = null;
	            if (panels.search.getChildren().length === 0) {
	                if (event.data !== undefined && event.data.search_form !== null) {
                    
	                    // Create the form
	                    var searchForm = DEPLOY.createForm({
							controller: ctr,
							view: 'PASSIVE',
							json: {form: event.data.search_form}
						});
        
	                    searchForm.bind('onSend', function (event, data) {
	                        event.stopDefault();
                        
	                        paginator.searchOptions(this.getData());
	                        paginator.goToPage(1);
                        
	                    }, {paginator: this});
                    
	                    // Add the apply button to the form
	                    var applyBtn = new FLUID.Button({
	                        parent: searchForm.getChildren()[0],
	                        label: _('Apply'),
	                        classes: ['fluid-right']
	                    });
	                    applyBtn.bind('onClick', function (event, data) { 
							data.form.send();
	                    }, {form: searchForm});
                    
	                    // Add the form to the tab
	                    panels.search.appendChild(searchForm);
	                }
	            } else {
	                searchForm = panels.search.getChildren()[0];
	            }
            
	            // Check for empty page
	            if (page !== undefined) {
	                tab.label(ctr._uc_verbose_name_plural + ' (Page ' + page.number + ' of ' + page.page_count + ')');
	                
	                // Update the table's header
	                for (var i = 0; i < page.columns.length; i++) {
	                    page.columns[i]['paginator'] = paginator;
	                    page.columns[i]['selected'] = false;
	                    if (page.columns[i]['name'] === this._sortColumn) {
	                        page.columns[i]['selected'] = true;
	                        page.columns[i]['direction'] = this._sortDirection;
	                    }
	                }
	                dataTable.header(page.columns);
                
	                // Update the table's data
	                var rows = [];
	                for (var i = 0; i < page.items.length; i++) {
	                    var item = page.items[i];
	                    var row = [];
	                    for (var j = 0; j < page.columns.length; j++) {
	                        var column = page.columns[j];
	                        row.push({
	                            controller: ctr,
	                            itemId: item['_id'],
	                            value: item[column.name]
	                            });
	                    }
	                    rows.push(row);
	                }
	                dataTable.data(rows);
                
	                // Update navigation
	                navigationForm.getFieldMap()['page'].max(page.page_count);
	                navigationForm.getFieldMap()['page'].value(page.number);
	                prevBtn.enable(page.has_previous_page);
	                nextBtn.enable(page.has_next_page); 
	            } else { 
	                tab.label(ctr._verbose_name_plural + ' (Page 1 of 1)');
	                navigationForm.getFieldMap()['page'].max(1);
	                navigationForm.getFieldMap()['page'].value(1);
	                dataTable.header([]);
	                dataTable.data([]);
                
	                FLUID.widgetByName('desktop').notify(_('No records found'));
	            }
            
	            this.trigger('searchGoToPage', 
	                function (event) {
	                    var tabPanel = event.data.tabPanel;
	                    var dataTable = event.data.dataTable;
	                    var panels = event.data.panels;
	                    var nextBtn = event.data.nextBtn;
	                    if (!dataTable.exists()) {
	                        
	                        tabPanel.refresh();
	                        
	                        // Update the window
	                        tabPanel.getParent().getParent().update();
	                        
	                        // Odd fix required for webkit
	                        var children = panels.navigation.getChildren();
	                        for (var i = 0; i < children.length; i++) {
                                children[i].refresh();
                            }
	                    } else {
	                        dataTable.refresh();
	                    }     
	                }, {
	                data: {
	                    tab: tab, 
	                    tabPanel: tabPanel, 
	                    paginator: paginator, 
	                    dataTable: dataTable, 
	                    searchForm: searchForm,
	                    panels: panels,
	                    prevBtn: prevBtn,
	                    nextBtn: nextBtn
	                }
	            });
	        }, {
	            ctr: this,
	            tab: tab,
	            tabPanel: tabPanel,
	            dataTable: dataTable,
	            navigationForm: navigationForm,
	            prevBtn: prevBtn,
	            nextBtn: nextBtn
	        });        
            
			// Store useful references against the window
			tab.widgetData('paginator', paginator);
			tab.widgetData('panels', panels);
			tab.widgetData('navigationForm', navigationForm);
			
		}, {data: {paginator: paginator, tab: tab, tabPanel: tabPanel}});
		
        // Get the first page
        this.trigger('searchInTabInit', 
            function (event) {
                event.data.paginator.goToPage(1);
            },
            {data: {paginator: paginator}}
        );
    },
    
    /**
     * Default list method.
     */
    list: function () {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
		// If the window is already open then don't allow it to be opened
		// again, instead bring it to the front.
		if (FLUID.widgetByName('deploy-window-list-' + this.getName())) {
			FLUID.widgetByName('deploy-window-list-' + this.getName()).minimize(false);
			FLUID.widgetByName('deploy-window-list-' + this.getName()).sendToFront();
			return;
		}
		
        // Create the paginator
        var paginator = new DEPLOY.Paginator({
            controllerName: this.getName(), 
            view: 'list'
        });
        
        this.trigger('listReady', function (event, data) {
			var paginator = event.data.paginator;
		
	        // Create the list window
	        var win = new FLUID.Window({
	            name: 'deploy-window-list-' + this.getName(),
	            group: 'modal',
	            label: this._uc_verbose_name_plural,
	            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][0].slice(0),
	            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][1].slice(0),
	            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['list'][2].slice(0),
	            flags: 
	                FLUID.Window.CAN_CLOSE |
	                FLUID.Window.CAN_MINIMIZE |
	                FLUID.Window.CAN_MAXIMIZE |
	                FLUID.Window.CAN_RESIZE |
	                FLUID.Window.CAN_DRAG
	            });
       
			// Add paginator to the win data

	        // Handle window events
	        win.bind('afterCloseComplete', function(event, data) {
	            data.ctr.trigger('listClosed', function(event) {
	                var desktop = FLUID.widgetByName('desktop');
	                var win = event.data.win;
                
	                // Remove the window
	                win.destroy();
                
	                // If there are still other windows then we should remain in modal
	                // mode...
	                var children = desktop.getChildren('modal');
	                for (var i = 0; i < children.length; i++) {
	                    if (children[i].typeOf('Window')) {
	                        return;
	                    }
	                }
                
	                // ...else we can exit modal mode
	                desktop.endModal(FLUID.Desktop.MODAL);
	            }, {data: {win: this}});
	        }, {ctr: this});
        
	        // Set up the panels/groups for the view
	        var panels = {
	            'body': new FLUID.Panel({
	                parent: win, 
	                classes: ['deploycms-list-body'],
	                style: {
	                    height: function (body) {
	                        if (!body.exists()) {
	                            return;
	                        }
	                        var win = body.getParent();
	                        var winHTML = win.getHTMLSelection();
	                        var height = winHTML.height();
                        
	                        // Allow for title
	                        height -= winHTML.children('.fluid-title').outerHeight(true);
                        
	                        // Allow for info & action areas
	                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
	                            height -= winHTML.children('.fluid-info').outerHeight(true);
	                        }
	                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
	                            height -= winHTML.children('.fluid-actions').outerHeight(true);
	                        }
                        
	                        // Allow for padding on client
	                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());
                        
	                        return height;
	                    }
	                }
	            })
	        };

	        panels.data = new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-data-view'],
	            style: {
	                height: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var height = body.style('height');
                    
	                    // Allow for navigation
	                    var navigation = data.nextSibling().nextSibling();
	                    height -= navigation.getHTMLSelection().outerHeight(true);
                    
	                    return height;
	                },
	                width: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var width = body.getHTMLSelection().width();
                    
	                    // Allow for actions
	                    var actions = data.nextSibling();
						if (actions.visible()) {
	                    	width -= actions.getHTMLSelection().outerWidth(true);
                    	}
	                    return width;
	                }
	            }
	        });
        
	        panels.actions = new FLUID.Group({
	            parent: panels.body,
	            legend: 'Actions',
	            classes: ['deploycms-list-actions']
	        });

	        panels.navigation =  new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-navigation'],
	            style: {
	                width: function (navigation) {
	                    if (!navigation.exists()) {
	                        return;
	                    }
	                    var data = navigation.prevSibling().prevSibling();
	                    return data.style('width');
	                }
	            }
	        });      
        
	        // Set up the data table
	        var dataTable = new FLUID.DataTable({
	            parent: panels.data, 
	            headerClass: DEPLOY.Paginator.DataHeader, 
	            cellClass: DEPLOY.Paginator.DataCell,
	            classes: ['deploycms-list-data']
	            });

	        // Set up update/view action    
	        function updateRecord(event, data) {
	            data.ctr.update(data.itemId);
	        };   	
		
		    if (!user.can(this.getName(), 'update')) {
                updateRecord = function () {
                    FLUID.widgetByName('desktop').notify(_('Permission denied'));
                }
	        }
		
	        dataTable.bind('afterAddCell', function(event, data) {
	            event.data.cell.bind('onClick', data.func, {
                    ctr: data.ctr,
                    itemId: event.data.cell.getItemID()
                });       
            }, {ctr: this, func: updateRecord});
    
	        // Set up standard actions
	        if (user.can(this.getName(), 'add')) {
            
	            // Add a record
	            var addBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.add',
	                label: _('Add') + ' ' + this.getVerboseName()
	            });
	            addBtn.bind('onClick', function (event, data) { 
	                data.ctr.add();
	            }, {ctr: this});
            
	            // Add a separator if other actions are supported
	            if (user.can(this.getName(), 'import-csv') ||
	                    user.can(this.getName(), 'export-csv')) {
	                new FLUID.Separator({parent: panels.actions});
	            }
	        }
        
	        // Import/Export records
	        if (user.can(this.getName(), 'import-csv')) {
	            var importBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.import',
	                label: _('Import CSV')
	            });
	            importBtn.bind('onClick', function (event, data) { 
	                data.ctr.import_csv();
	            }, {ctr: this});
	        }
	        if (user.can(this.getName(), 'export-csv')) {
	            var exportBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.export',
	                label: _('Export CSV')
	            });
	            exportBtn.bind('onClick', function (event, data) { 
	                data.ctr.export_csv();
	            }, {ctr: this});
	        }
			
			// If there are no standard actions we hide the actions panel
			if (panels.actions.getChildCount() === 0) {
				panels.actions.hide();
			}
	
	        // Set up naviagtion
        
	        // Go form
	        var navigationForm = new FLUID.Form({parent: panels.navigation}); 
	        navigationForm.bind('onSend', function (ev) {
	            ev.stopDefault();
	            var data = this.getData();
	            var page = parseInt(data.page);
            
	            if (page >= 1 && page <= paginator.getPage().page_count) {
	                paginator.goToPage(page);
	            } else {
	                FLUID.widgetByName('desktop').notify(_('You have entered an invalid page number'));
	            }
	        }, {paginator: paginator});
	        var navigationFieldset = new FLUID.Fieldset({
	            parent: navigationForm
	        });
	        var goField = new FLUID.Field.Number({
	            parent: navigationFieldset,
	            name: 'page',
	            label: 'Go to page',
	            min: 1,
	            max: 1,
	            value: 1
	        });
	        var goBtn = new FLUID.Button({
	            parent: navigationFieldset,
	            icon: 'deploy.icons.views.go',
	            label: _('Go'),
	            classes: ['fluid-icon-right']
	        });
	        goBtn.bind('onClick', function (event, data) { 
	            data.form.send();
	        }, {form: navigationForm});
        
	        // Next & Previous buttons
	        var nextBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.next',
	            label: _('Next'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-icon-right']
	        });
	        nextBtn.bind('onClick', function (event, data) { 
	            event.stopDefault();
	            if (this.enabled()) {
	                data.paginator.nextPage();
	            }
	        }, {paginator: paginator});
            
	        var prevBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.prev',
	            label: _('Prev'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-sep']
	        });
	        prevBtn.bind('onClick', function (event, data) { 
	            if (this.enabled()) {
	                data.paginator.previousPage();
	            }
	        }, {paginator: paginator});
        
	        // Handle page loads
	        paginator.bind('afterGoToPageComplete.deploy', function (event, data) {
	            var desktop = FLUID.widgetByName('desktop');
	            var ctr = data.ctr;
	            var win = data.win;
	            var dataTable = data.dataTable;
	            var navigationForm = data.navigationForm;
	            var prevBtn = data.prevBtn;
	            var nextBtn = data.nextBtn;
	            var page = this.getPage();
            
	            // Check for empty page
	            if (page !== undefined) {
	                win.label(ctr._uc_verbose_name_plural + ' (Page ' + page.number + ' of ' + page.page_count + ')');
                
	                // Update the table's header
	                for (var i = 0; i < page.columns.length; i++) {
	                    page.columns[i]['paginator'] = paginator;
	                    page.columns[i]['selected'] = false;
	                    if (page.columns[i]['name'] === this._sortColumn) {
	                        page.columns[i]['selected'] = true;
	                        page.columns[i]['direction'] = this._sortDirection;
	                    }
	                }
	                dataTable.header(page.columns);
                
	                // Update the table's data
	                var rows = [];
	                for (var i = 0; i < page.items.length; i++) {
	                    var item = page.items[i];
	                    var row = [];
	                    for (var j = 0; j < page.columns.length; j++) {
	                        var column = page.columns[j];
	                        row.push({
	                            controller: ctr,
	                            itemId: item['_id'],
	                            value: item[column.name]
	                            });
	                    }
	                    rows.push(row);
	                }
	                dataTable.data(rows);
                
	                // Update navigation
	                navigationForm.getFieldMap()['page'].max(page.page_count);
	                navigationForm.getFieldMap()['page'].value(page.number);
	                prevBtn.enable(page.has_previous_page);
	                nextBtn.enable(page.has_next_page); 
	            } else { 
	                win.label(ctr._verbose_name_plural + ' (Page 1 of 1)');
	                navigationForm.getFieldMap()['page'].max(1);
	                navigationForm.getFieldMap()['page'].value(1);
	                dataTable.header([]);
	                dataTable.data([]);
                
	                FLUID.widgetByName('desktop').notify(_('No records found'));
	            }
            
	            this.trigger('listGoToPage', 
	                function (event) {
	                    var win = event.data.win;
	                    var dataTable = event.data.dataTable;
	                    if (!win.exists()) {
	                        // If this is the first page load then we need to open the 
	                        // window.
	                        win.refresh();
	                        win.prepare(function (win) { 
	                            win.center();
								win.sendToFront();
	                        });
	                        win.open();
	                    } else {
	                        dataTable.refresh();
	                    }     
	                }, {
	                data: {
	                    win: win, 
	                    paginator: paginator, 
	                    dataTable: dataTable
	                }
	            });
	        }, {
	            ctr: this,
	            win: win,
	            dataTable: dataTable,
	            navigationForm: navigationForm,
	            prevBtn: prevBtn,
	            nextBtn: nextBtn
	        });
        
			// Store useful references against the window
			win.widgetData('paginator', paginator);
			win.widgetData('panels', panels);
			win.widgetData('navigationForm', navigationForm);
			
		}, {data: {paginator: paginator}});
		
        // Get the first page
        this.trigger('listInit', 
            function (event) {
                event.data.paginator.goToPage(1);
            },
            {data: {paginator: paginator}}
        );
    },

    /**
     * Default list support in tabs.
     */
    listInTab: function (tab) {
        var desktop = FLUID.widgetByName('desktop');
        var user = DEPLOY.getAdministrator();
        var paginator = tab.widgetData('paginator');
        var tabPanel = tab.panel();
        
        // Has the tab been initialised before?
        if (paginator !== undefined) {
            // Reload the current data and return
            if (paginator.getPage()) {
            	paginator.goToPage(paginator.getPage().number);
            } else {
            	paginator.goToPage(1);
            }
            return;
        }
        
        /* First time call to the tab */
        tabPanel.rename('deploy-tab-list-' + this.getName());
 
        // Create the paginator
        var paginator = new DEPLOY.Paginator({
            controllerName: this.getName(), 
            view: 'list'
        });
        tabPanel.widgetData('paginator', paginator);
        
        this.trigger('listInTabReady', function (event, data) {
			var paginator = event.data.paginator;
			var tab = event.data.tab;
			var tabPanel = event.data.tabPanel;
			var tabber = tabPanel.getParent();
			var win = tabber.getParent();
		
		    // Set up the panels/groups for the view
	        var panels = {
	            'body': new FLUID.Panel({
	                parent: tabPanel,
	                classes: ['deploycms-list-body'],
	                style: {
	                    height: function (body) {
	                        if (!body.exists()) {
	                            return;
	                        }
	                        var tabber = body.getParent().getParent();
	                        var tabberHTML = tabber.getHTMLSelection();
	                        var win = body.getParent().getParent().getParent();
	                        var winHTML = win.getHTMLSelection();
	                        var height = winHTML.height();
                        
	                        // Allow for title
	                        height -= winHTML.children('.fluid-title').outerHeight(true);
                        
	                        // Allow for info & action areas
	                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
	                            height -= winHTML.children('.fluid-info').outerHeight(true);
	                        }
	                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
	                            height -= winHTML.children('.fluid-actions').outerHeight(true);
	                        }
                            
                            // Need to account for the height of the tabs and padding/margin on the tabber
                            height -= tabber.getTab(0).getHTMLSelection().outerHeight(true);
                            height -= (tabberHTML.children('.fluid-client').outerHeight(true) - tabberHTML.children('.fluid-client').height());
                            
	                        // Allow for padding on client
	                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());
                        
	                        return height;
	                    }
	                }
	            })
	        };
            
	        panels.data = new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-data-view'],
	            style: {
	                height: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var height = body.style('height');
                    
	                    // Allow for navigation
	                    var navigation = data.nextSibling().nextSibling();
	                    height -= navigation.getHTMLSelection().outerHeight(true);
                    
	                    return height;
	                },
	                width: function (data) {
	                    if (!data.exists()) {
	                        return;
	                    }
	                    var body = data.getParent();
	                    var width = body.getHTMLSelection().width();
                    
	                    // Allow for actions
	                    var actions = data.nextSibling();
	                    if (actions.visible()) {
							width -= actions.getHTMLSelection().outerWidth(true);
                    	}
                    	
	                    return width;
	                }
	            }
	        });
        
	        panels.actions = new FLUID.Group({
	            parent: panels.body,
	            legend: 'Actions',
	            classes: ['deploycms-list-actions']
	        });            
            
            
	        panels.navigation =  new FLUID.Panel({
	            parent: panels.body, 
	            classes: ['deploycms-list-navigation'],
	            style: {
	                width: function (navigation) {
	                    if (!navigation.exists()) {
	                        return;
	                    }
	                    var data = navigation.prevSibling().prevSibling();
	                    return data.style('width');
	                }
	            }
	        });      
        
	        // Set up the data table
	        var dataTable = new FLUID.DataTable({
	            parent: panels.data, 
	            headerClass: DEPLOY.Paginator.DataHeader, 
	            cellClass: DEPLOY.Paginator.DataCell,
	            classes: ['deploycms-list-data']
	            });

	        // Set up update or view action    
	        function updateRecord(event, data) {
	            data.ctr.update(data.itemId);
	        };   
		
    		if (!user.can(this.getName(), 'update')) {
                updateRecord = function () {
                    FLUID.widgetByName('desktop').notify(_('Permission denied'));
                }
	        }
	        
            dataTable.bind('afterAddCell', function(event, data) {
                event.data.cell.bind('onClick', data.func, {
                    ctr: data.ctr,
                    itemId: event.data.cell.getItemID()
                });          
            }, {ctr: this, func: updateRecord});
            
	        // Set up standard actions
	        if (user.can(this.getName(), 'add')) {
            
	            // Add a record
	            var addBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.add',
	                label: _('Add') + ' ' + this.getVerboseName()
	            });
	            addBtn.bind('onClick', function (event, data) { 
	                data.ctr.add();
	            }, {ctr: this});
            
	            // Add a separator if other actions are supported
	            if (user.can(this.getName(), 'import-csv') ||
	                    user.can(this.getName(), 'export-csv')) {
	                new FLUID.Separator({parent: panels.actions});
	            }
	        }
        
	        // Import/Export records
	        if (user.can(this.getName(), 'import-csv')) {
	            var importBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.import',
	                label: _('Import CSV')
	            });
	            importBtn.bind('onClick', function (event, data) { 
	                data.ctr.import_csv();
	            }, {ctr: this});
	        }
	        if (user.can(this.getName(), 'export-csv')) {
	            var exportBtn = new FLUID.Button({
	                parent: panels.actions,
	                icon: 'deploy.icons.views.export',
	                label: _('Export CSV')
	            });
	            exportBtn.bind('onClick', function (event, data) { 
	                data.ctr.export_csv();
	            }, {ctr: this});
	        }
        
			// If there are no standard actions we hide the actions panel
			if (panels.actions.getChildCount() === 0) {
				panels.actions.hide();
			}
			
	        // Set up naviagtion
        
	        // Go form
	        var navigationForm = new FLUID.Form({parent: panels.navigation}); 
	        navigationForm.bind('onSend', function (ev) {
	            ev.stopDefault();
	            var data = this.getData();
	            var page = parseInt(data.page);
            
	            if (page >= 1 && page <= paginator.getPage().page_count) {
	                paginator.goToPage(page);
	            } else {
	                FLUID.widgetByName('desktop').notify(_('You have entered an invalid page number'));
	            }
	        }, {paginator: paginator});
	        var navigationFieldset = new FLUID.Fieldset({
	            parent: navigationForm
	        });
	        var goField = new FLUID.Field.Number({
	            parent: navigationFieldset,
	            name: 'page',
	            label: 'Go to page',
	            min: 1,
	            max: 1,
	            value: 1
	        });
	        var goBtn = new FLUID.Button({
	            parent: navigationFieldset,
	            icon: 'deploy.icons.views.go',
	            label: _('Go'),
	            classes: ['fluid-icon-right']
	        });
	        goBtn.bind('onClick', function (event, data) { 
	            data.form.send();
	        }, {form: navigationForm});
        
	        // Next & Previous buttons
	        var nextBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.next',
	            label: _('Next'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-icon-right']
	        });
	        nextBtn.bind('onClick', function (event, data) { 
	            event.stopDefault();
	            if (this.enabled()) {
	                data.paginator.nextPage();
	            }
	        }, {paginator: paginator});
            
	        var prevBtn = new FLUID.Button({
	            parent: panels.navigation,
	            icon: 'deploy.icons.views.prev',
	            label: _('Prev'),
	            enabled: false,
	            classes: ['fluid-right', 'fluid-sep']
	        });
	        prevBtn.bind('onClick', function (event, data) { 
	            if (this.enabled()) {
	                data.paginator.previousPage();
	            }
	        }, {paginator: paginator});
        

	        // Handle page loads
	        paginator.bind('afterGoToPageComplete', function (event, data) {
	            var desktop = FLUID.widgetByName('desktop');
	            var ctr = data.ctr;
	            var tab = data.tab;
	            var tabPanel = data.tabPanel;
	            var dataTable = data.dataTable;
	            var navigationForm = data.navigationForm;
	            var prevBtn = data.prevBtn;
	            var nextBtn = data.nextBtn;
	            var page = this.getPage();
            
	            // Check for empty page
	            if (page !== undefined) {
	                tab.label(ctr._uc_verbose_name_plural + ' (Page ' + page.number + ' of ' + page.page_count + ')');
	                
	                // Update the table's header
	                for (var i = 0; i < page.columns.length; i++) {
	                    page.columns[i]['paginator'] = paginator;
	                    page.columns[i]['selected'] = false;
	                    if (page.columns[i]['name'] === this._sortColumn) {
	                        page.columns[i]['selected'] = true;
	                        page.columns[i]['direction'] = this._sortDirection;
	                    }
	                }
	                dataTable.header(page.columns);
                
	                // Update the table's data
	                var rows = [];
	                for (var i = 0; i < page.items.length; i++) {
	                    var item = page.items[i];
	                    var row = [];
	                    for (var j = 0; j < page.columns.length; j++) {
	                        var column = page.columns[j];
	                        row.push({
	                            controller: ctr,
	                            itemId: item['_id'],
	                            value: item[column.name]
	                            });
	                    }
	                    rows.push(row);
	                }
	                dataTable.data(rows);
                
	                // Update navigation
	                navigationForm.getFieldMap()['page'].max(page.page_count);
	                navigationForm.getFieldMap()['page'].value(page.number);
	                prevBtn.enable(page.has_previous_page);
	                nextBtn.enable(page.has_next_page); 
	            } else { 
	                tab.label(ctr._verbose_name_plural + ' (Page 1 of 1)');
	                navigationForm.getFieldMap()['page'].max(1);
	                navigationForm.getFieldMap()['page'].value(1);
	                dataTable.header([]);
	                dataTable.data([]);
                
	                FLUID.widgetByName('desktop').notify(_('No records found'));
	            }
            
	            this.trigger('listGoToPage', 
	                function (event) {
	                    var tabPanel = event.data.tabPanel;
	                    var dataTable = event.data.dataTable;
	                    var panels = event.data.panels;
	                    var nextBtn = event.data.nextBtn;
	                    if (!dataTable.exists()) {
	                        
	                        tabPanel.refresh();
	                        
	                        // Update the window
	                        tabPanel.getParent().getParent().update();
	                        
	                        // Odd fix required for webkit
	                        var children = panels.navigation.getChildren();
	                        for (var i = 0; i < children.length; i++) {
                                children[i].refresh();
                            }
	                    } else {
	                        dataTable.refresh();
	                    }     
	                }, {
	                data: {
	                    tab: tab, 
	                    tabPanel: tabPanel, 
	                    paginator: paginator, 
	                    dataTable: dataTable, 
	                    panels: panels,
	                    prevBtn: prevBtn,
	                    nextBtn: nextBtn
	                }
	            });
	        }, {
	            ctr: this,
	            tab: tab,
	            tabPanel: tabPanel,
	            dataTable: dataTable,
	            navigationForm: navigationForm,
	            prevBtn: prevBtn,
	            nextBtn: nextBtn
	        });        
            
			// Store useful references against the window
			tab.widgetData('paginator', paginator);
			tab.widgetData('panels', panels);
			tab.widgetData('navigationForm', navigationForm);
			
		}, {data: {paginator: paginator, tab: tab, tabPanel: tabPanel}});
		
        // Get the first page
        this.trigger('listInTabInit', 
            function (event) {
                event.data.paginator.goToPage(1);
            },
            {data: {paginator: paginator}}
        );
    },
    
    /**
     * Default add method.
     */
    add: function () {
        var desktop = FLUID.widgetByName('desktop');
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
		// If the window is already open then don't allow it to be opened
		// again, instead bring it to the front.
		if (FLUID.widgetByName('deploy-window-add-' + this.getName())) {
			FLUID.widgetByName('deploy-window-add-' + this.getName()).minimize(false);
			FLUID.widgetByName('deploy-window-add-' + this.getName()).sendToFront();
			return;
		}
		
        // Request the add form from the server
        var formReq = new IO.Request({
			controller: this.getName(),
			method: 'add'
		});
		
		// Handle the form load
        formReq.bind('afterSuccess', function (event, data) {
			var ctr = data.ctr;
        	var formData = event.data.data;
			
            ctr.trigger('addReady', function (event) {
				var ctr = event.data.ctr;
				var formData = event.data.formData;
				
		        // Create the add window
		        var win = new FLUID.Window({
		            name: 'deploy-window-add-' + this.getName(),
		            group: 'modal',
		            label: _('Add ') + this._verbose_name,
		            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
		            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
		            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
		            flags: 
						FLUID.Window.HAS_ACTIONS |
		                FLUID.Window.CAN_CLOSE |
		                FLUID.Window.CAN_MINIMIZE |
		                FLUID.Window.CAN_RESIZE |
		                FLUID.Window.CAN_DRAG
		            });
                
                win.bind('onClose', function (event, data) {
                    if (this.widgetData('closeConfirmed')) {
                        this.widgetData('closeConfirmed', false);
                    } else {
                        event.stopDefault();
                    
                        // Make the user confirm they want to close the window
                        var dlg = new DEPLOY.ConfirmDialog({
                            name: 'deploy-window-confirm-close-' + data.ctr.getName(),
                            label: _('Cancel add ' + data.ctr.verbose_name + '?'),
                            message: _('Please <strong>confirm</strong> you wish to close the add window.') + 
                                _('<strong>You will loose all the data you have entered.</strong>')
                            });

                        dlg.bind('onConfirm', function (event, data) { 
                            this.bind('afterCloseComplete', function (event, data) {
                                data.win.widgetData('closeConfirmed', true);
                                data.win.close();
                            }, {win: data.win})
                        }, {ctr: data.ctr, win: this});

                        dlg.launch();
                    }
                }, {ctr: this});
                
		        // Handle window events
		        win.bind('afterCloseComplete', function (event, data) {
		            data.ctr.trigger('addClosed', function (event) {
		                var desktop = FLUID.widgetByName('desktop');
		                var win = event.data.win;
                        var i = 0;
                        
                        // Are there any attachments in the the form and if so 
                        // are they associated with the meta update window.
                        var metaWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
                        if (metaWin !== undefined) {
                            var currentPath = metaWin.widgetData('attachment').getPath();
                            var fields = win.widgetData('form').getFields();
                            for (i = 0; i < fields.length; i++) {
                                var field = fields[i];
                                if (field.isA('DEPLOY.SingleAttachmentField')) {
                                    if (currentPath == field.attachment().getPath()) {
                                        metaWin.destroy();
                                    }
                                }
                            } 
                        }

		                // Remove the window
		                win.destroy();

		                // If there are still other windows then we should remain in modal
		                // mode...
		                var children = desktop.getChildren('modal');
		                for (i = 0; i < children.length; i++) {
		                    if (children[i].typeOf('Window')) {
		                        return;
		                    }
		                }

		                // ...else we can exit modal mode
		                desktop.endModal(FLUID.Desktop.MODAL);
		            }, {data: {win: this}});
		        }, {ctr: this});

				// Build the form
				var form = DEPLOY.createForm({
					controller: ctr,
					view: 'ADD',
					json: formData
				});
				
				// Create the tabber for the form
				var tabber = new FLUID.Tabber({
					parent: win
		        });
				
				// Add the tabs
				tabber.appendTab('Details');
				
				// Check for multiple attachments and create tabs if found
				var fields = form.getFields();
				var tabCount = 0;
				for (var i = 0; i < fields.length; i++) {
					var field = fields[i];
					if (field.typeOf('DEPLOY.MultipleAttachmentField')) {
						tabber.appendTab(field.label());
						tabCount++;
						var attachmentMgr = new DEPLOY.MultipleAttachmentManager({
							field: field,
							parent: tabber.getTabPanel(tabCount)
						});
						tabber.getTabPanel(tabCount).widgetData('attachmentMgr', attachmentMgr);
					}
				}
				
				tabber.selectTab(0);
				
				// Handle form submissions
				form.bind('afterSendSuccess.add.deploy', function (event, data) {
					// Check if the list or search window is open
					var listWin = FLUID.widgetByName('deploy-window-search-' + data.ctr.getName());
					if (listWin === undefined) {
						listWin = FLUID.widgetByName('deploy-window-list-' + data.ctr.getName());
					}
					if (listWin !== undefined) {
						// Reload the current list view
						var paginator = listWin.widgetData('paginator');
						if (paginator.getPage()) {
							paginator.goToPage(paginator.getPage().number);
						} else {
							paginator.goToPage(1);
						}
					}
					var listTab = FLUID.widgetByName('deploy-tab-search-' + data.ctr.getName());
					if (listTab === undefined) {
						listTab = FLUID.widgetByName('deploy-tab-list-' + data.ctr.getName());
					}					
					if (listTab !== undefined) {
						// Reload the current list view
						var paginator = listTab.widgetData('paginator');
						if (paginator.getPage()) {
							paginator.goToPage(paginator.getPage().number);
						} else {
							paginator.goToPage(1);
						}
					}
				}, {ctr: ctr, win: win});
				
            	form.bind('afterSendSuccess.add.deploy', function (event, data) {
					data.ctr.trigger('addSave', function (event, data) {
						
					}, {data: {saveEvent: event}});
				}, {ctr: ctr});
	
            	form.bind('afterSendSuccess.deploy', function (event, data) {
                	var win = data.win;

					data.win.unbind('afterCloseComplete.deploy');
	                data.win.bind('afterCloseComplete.deploy', function(event, data) {
						
						// Notify the admin that the record has been added
						FLUID.widgetByName('desktop').notify(data.msg);
						
						// Remove the window
	                    this.destroy();
	
	                }, {ctr: ctr, msg: event.data.data.msg});
	                
					// Close the window
					data.win.widgetData('closeConfirmed', true);
					data.win.close();
						
				}, {win: win});
        		
				// Save & New
				var saveNewBtn = new FLUID.Button({
	            	parent: win,
					group: 'actions',
	            	label: _('Save & New'),
	            	classes: ['fluid-sep']
	        	});
		        saveNewBtn.bind('onClick', function (event, data) { 
		            if (this.enabled()) {
						data.form.unbind('afterSendSuccess.deploy');
						data.form.bind('afterSendSuccess.deploy', function (event, data) {

							// Notify the admin that the record has been added
							FLUID.widgetByName('desktop').notify(event.data.data.msg);
							
							// Reset the form
							this.reset();
							
							// Check for multiple attachment managers
							var tabCount = data.tabber.getTabCount();
							for (var i = 0; i < tabCount; i++) {
						        var tablPanel = data.tabber.getTabPanel(i);
						        if (tablPanel.widgetData('attachmentMgr') !== undefined) {
					                tablPanel.widgetData('attachmentMgr').reset();
					            }
						    }
							
							// Select the form tab and scroll to the top of the form
							data.tabber.selectTab(0);
							data.win.getHTMLSelection('client').scrollTop(0);
						}, {win: win, tabber: tabber});
		                data.form.send();
		            }
		        }, {form: form, win: win, tabber: tabber});
	
				// Save & Close
				var saveCloseBtn = new FLUID.Button({
	            	parent: win,
					group: 'actions',
	            	label: _('Save & Close')
	        	});
				saveCloseBtn.bind('onClick', function (event, data) {
					var form = data.form;
					var win = data.win;
					
	            	if (this.enabled()) {
						data.form.unbind('afterSendSuccess.deploy');
						data.form.bind('afterSendSuccess.deploy', function (event, data) {

							data.win.unbind('afterCloseComplete');
			                data.win.bind('afterCloseComplete', function(event, data) {

								// Notify the admin that the record has been added
								FLUID.widgetByName('desktop').notify(data.msg);

								// Remove the window
			                    this.destroy();

			                }, {msg: event.data.data.msg});

							// Close the window
							data.win.widgetData('closeConfirmed', true);
							data.win.close();
						
						}, {win: win}); 
		                data.form.send();
		            }
		        }, {form: form, win: win});
				
				// Add the form to the first tab
				tabber.getTabPanel(0).appendChild(form);
				
				// Store useful references against the window
				win.widgetData('form', form);
				win.widgetData('tabber', tabber);
				win.widgetData('buttons', {
				    'saveNewBtn': saveNewBtn,
				    'saveCloseBtn': saveCloseBtn
				});
							
				// Render, Prepare & Open the window
				ctr.trigger('addOpen', 
					function (event) {
						var win = event.data.win;
						win.refresh();
		                win.prepare(function (win) { 
		                    win.center();
							win.sendToFront();
					
							win.getChildren()[0].style({
			                    height: function (body) {
			                        if (!body.exists()) {
			                            return;
			                        }
			                        var win = body.getParent();
			                        var winHTML = win.getHTMLSelection();
			                        var height = winHTML.height();

			                        // Allow for title
			                        height -= winHTML.children('.fluid-title').outerHeight(true);

			                        // Allow for info & action areas
			                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
			                            height -= winHTML.children('.fluid-info').outerHeight(true);
			                        }
			                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
			                            height -= winHTML.children('.fluid-actions').outerHeight(true);
			                        }

			                        // Allow for padding on client
			                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());

			                        return height;
			                    }
			                });					
		                });						
						win.open();
					}, 
					{data: {ctr: ctr, win: win}}
				);
				
			}, {data: {ctr: ctr, formData: formData}});
			
		}, {ctr: this});

        // Load the add form
        this.trigger('addInit', 
            function (event) {
				event.data.formReq.send();
            },
            {data: {formReq: formReq}}
		);
    },
    
    /**
     * Default update method.
     */
    update: function (id) {
        var desktop = FLUID.widgetByName('desktop');
        
        // Enter model state
        desktop.startModal(FLUID.Desktop.MODAL);
        
		// If the window is already open then don't allow it to be opened
		// again, instead bring it to the front.
		var existingWin = FLUID.widgetByName('deploy-window-update-' + this.getName());
		if (existingWin) {
			if (existingWin.widgetData('id') === id) {
				existingWin.minimize(false);
				existingWin.sendToFront();
				return;
			}
			existingWin.destroy();
		}
		
        // Request the update form from the server
        var formReq = new IO.Request({
			controller: this.getName(),
			method: 'update',
			data: {id: id}
		});

		// Handle the form load
        formReq.bind('afterSuccess', function (event, data) {
			var ctr = data.ctr;
			var id = data.id;
			var formData = event.data.data;

            ctr.trigger('updateReady', function (event) {
				var ctr = event.data.ctr;
				var formData = event.data.formData;
				var identity = formData._identity;
				var id = event.data.id;
				var user = DEPLOY.getAdministrator();
                
		        // Create the update window
		        var win = new FLUID.Window({
		            name: 'deploy-window-update-' + this.getName(),
		            group: 'modal',
		            label: identity, //_('Update ') + this._verbose_name,
		            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][0].slice(0),
		            minSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][1].slice(0),
		            maxSize: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['form'][2].slice(0),
		            flags: 
		                FLUID.Window.CAN_CLOSE |
		                FLUID.Window.CAN_MINIMIZE |
		                FLUID.Window.CAN_RESIZE |
		                FLUID.Window.CAN_DRAG
		            });

                // Do we have an ID for the record?
                
        		// Attempt to get a URL for the record
        		var urlReq = new IO.Request({
        	    	controller: this.getName(),
        			method: 'get-url-for-id',
        			data: {id: id}
                });

                urlReq.bind('afterSuccess.deploy', function (event, data) {
				    var win = data.win;
				    
                    /*
                    If the user is currently viewing this page then highlight it to them.
                    */
                    win.flags(win.flags() | FLUID.Window.HAS_INFO);
                    if (event.data.data.url === DEPLOY.getAdministrator().getURL()) {
						var message = new FLUID.Text({
							parent: win,
                            group: 'info',
                            content: _('You are currently viewing this page'),
                            classes: ['fluid-center', 'fluid-strong']
							});
						message.refresh();
						
                        win.widgetData('buttons').removeBtn.widgetData('refresh', true);

                    } else {
                        /* If we got a URL add a 'Visit page' button */
                        var visitBtn = new FLUID.Button({
                            parent: win,
                            group: 'info',
                            label: _(data.ctr._visit_page_label),
                            classes: ['fluid-right']
                        });

                        visitBtn.bind('onClick', function (event, data) {
                            /* 
                            Do we open the models URL in a new window or in
                            the current view?
                            */
                            if (data.ctr._visit_page_new_window) {
                                window.open(data.url);
                            } else {
                                window.location = data.url;
                            }
                        }, {ctr: data.ctr, url: event.data.data.url});
                        
                        visitBtn.refresh();
                    }
                    
				}, {ctr: ctr, win: win});
                
				if (user.can(this.getName(), 'get-url-for-id')) {
					urlReq.send();
				}
        		
		        // Handle window events
		        win.bind('afterCloseComplete', function(event, data) {
		            data.ctr.trigger('updateClosed', function(event) {
		                var desktop = FLUID.widgetByName('desktop');
		                var win = event.data.win;
                        var i = 0;
                        
                        // Are there any attachments in the the form and if so 
                        // are they associated with the meta update window.
                        var metaWin = FLUID.widgetByName('deploy-window-attachment-meta-manage');
                        if (metaWin !== undefined) {
                            var currentPath = metaWin.widgetData('attachment').getPath();
                            var fields = win.widgetData('form').getFields();
                            for (i = 0; i < fields.length; i++) {
                                var field = fields[i];
                                if (field.isA('DEPLOY.SingleAttachmentField')) {
                                    if (currentPath == field.attachment().getPath()) {
                                        metaWin.destroy();
                                    }
                                }
                            } 
                        }

		                // Remove the window
		                win.destroy();

		                // If there are still other windows then we should remain in modal
		                // mode...
		                var children = desktop.getChildren('modal');
		                for (i = 0; i < children.length; i++) {
		                    if (children[i].typeOf('Window')) {
		                        return;
		                    }
		                }

		                // ...else we can exit modal mode
		                desktop.endModal(FLUID.Desktop.MODAL);
		            }, {data: {win: this}});
		        }, {ctr: this});
				
				// Build the form
				var form = DEPLOY.createForm({
					controller: ctr,
					id: id,
					view: 'UPDATE',
					autosave: user.getPreference('auto_save', true),
					json: formData
				});
				
				// Create the tabber for the form
				var tabber = new FLUID.Tabber({
					parent: win
		        });
				
				// Add the tabs
				tabber.appendTab('Details');
				
				// Check for multiple attachments and create tabs if found
				var fields = form.getFields();
				var tabCount = 0;
				for (var i = 0; i < fields.length; i++) {
					var field = fields[i];
					if (field.typeOf('DEPLOY.MultipleAttachmentField')) {
						tabber.appendTab(field.label());
						tabCount++;
						var attachmentMgr = new DEPLOY.MultipleAttachmentManager({
							field: field,
							parent: tabber.getTabPanel(tabCount)
						});
					}
				}
				
				tabber.selectTab(0);
				
				// Use the status bar to inform user of auto-saves
				
				// Handle form submissions
				form.bind('afterSendSuccess.list.deploy afterRealtimeSendSuccess.list.deploy', function (event, data) {
					// Check if the list or search window is open
					var listWin = FLUID.widgetByName('deploy-window-search-' + data.ctr.getName());
					if (listWin === undefined) {
						listWin = FLUID.widgetByName('deploy-window-list-' + data.ctr.getName());
					}
					if (listWin !== undefined) {
						// Reload the current list view
						var paginator = listWin.widgetData('paginator');
						if (paginator.getPage()) {
							paginator.goToPage(paginator.getPage().number);
						} else {
							paginator.goToPage(1);
						}
					}
					var listTab = FLUID.widgetByName('deploy-tab-search-' + data.ctr.getName());
					if (listTab === undefined) {
						listTab = FLUID.widgetByName('deploy-tab-list-' + data.ctr.getName());
					}					
					if (listTab !== undefined) {
						// Reload the current list view
						var paginator = listTab.widgetData('paginator');
						if (paginator.getPage()) {
							paginator.goToPage(paginator.getPage().number);
						} else {
							paginator.goToPage(1);
						}
					}
				}, {ctr: ctr, win: win});
				
            	form.bind('afterSendSuccess.update.deploy', function (event, data) {
					data.ctr.trigger('updateSave', function (event, data) {
						
					}, {data: {saveEvent: event}});
				}, {ctr: ctr});
				
            	form.bind('afterSendSuccess.deploy', function (event, data) {
                	var win = data.win;

					data.win.unbind('afterCloseComplete.deploy');
	                data.win.bind('afterCloseComplete.deploy', function(event, data) {
						
						// Notify the admin that the record has been added
						FLUID.widgetByName('desktop').notify(data.msg);
						
	                }, {ctr: ctr, msg: event.data.data.msg});
	                
					// Close the window
					data.win.close();
						
				}, {win: win});
        		
				// If auto-save is not enabled add a save button
				var saveBtn = null;
				if (!user.getPreference('auto_save', true) || form.realtimeData()['_action'] !== 'SAVE') {
					win.flags(win.flags() | FLUID.Window.HAS_ACTIONS);
				    saveBtn = new FLUID.Button({
		            	parent: win,
						group: 'actions',
		            	label: _('Save changes')
		        	});
			        saveBtn.bind('onClick', function (event, data) { 
			            if (this.enabled()) {
							data.form.send();
			            }
			        }, {ctr: ctr, form: form});
				}

				// Remove button
				var removeBtn = null;
				if (user.can(ctr.getName(), 'remove')) {
					win.flags(win.flags() | FLUID.Window.HAS_ACTIONS);
					removeBtn = new FLUID.Button({
		            	parent: win,
						group: 'actions',
		            	label: _('Remove'),
		            	classes: ['fluid-right']
		        	});
			        removeBtn.bind('onClick', function (event, data) { 
			            if (this.enabled()) {
							data.ctr.remove(data.id, this.widgetData('refresh'));
			            }
			        }, {ctr: ctr, id: id});
				}
		
				// Add the form to the first tab
				tabber.getTabPanel(0).appendChild(form);
				
				// Store useful references against the window
				win.widgetData('id', id);
				win.widgetData('identity', identity);
				win.widgetData('form', form);
				win.widgetData('tabber', tabber);			
				win.widgetData('buttons', {
				    'saveBtn': saveBtn,
				    'removeBtn': removeBtn
				});
				
				// Render, Prepare & Open the window
				ctr.trigger('updateOpen', 
					function (event) {
						var win = event.data.win;
						win.refresh();
		                win.prepare(function (win) { 
		                    win.center();
							win.sendToFront();
					
							win.getChildren()[0].style({
			                    height: function (body) {
			                        if (!body.exists()) {
			                            return;
			                        }
			                        var win = body.getParent();
			                        var winHTML = win.getHTMLSelection();
			                        var height = winHTML.height();

			                        // Allow for title
			                        height -= winHTML.children('.fluid-title').outerHeight(true);

			                        // Allow for info & action areas
			                        if ((FLUID.Window.HAS_INFO & win.flags()) === FLUID.Window.HAS_INFO) {
			                            height -= winHTML.children('.fluid-info').outerHeight(true);
			                        }
			                        if ((FLUID.Window.HAS_ACTIONS & win.flags()) === FLUID.Window.HAS_ACTIONS) {
			                            height -= winHTML.children('.fluid-actions').outerHeight(true);
			                        }

			                        // Allow for padding on client
			                        height -= (winHTML.children('.fluid-client').outerHeight(true) - winHTML.children('.fluid-client').height());

			                        return height;
			                    }
			                });					
		                });						
						win.open();
					}, 
					{data: {ctr: ctr, win: win}}
				);
			}, {data: {ctr: ctr,  id: id, formData: formData}});
			
		}, {ctr: this, id: id});

        // Load the update form
        this.trigger('updateInit', 
            function (event) {
				event.data.formReq.send();	
            },
            {data: {formReq: formReq}}
		);
    },

    /**
     * Default remove method.
     */
    remove: function (id, refresh) {
        var desktop = FLUID.widgetByName('desktop');
        
        // Enter *single* model state
        desktop.startModal(FLUID.Desktop.SINGLE_MODAL);
        desktop.bind('onEndModal.deploy', function (event) {
			event.stopDefault();
		});
		desktop.addClass('fluid-frozen');
		
		// Call remove as get, then as a post
		var removeGetReq = new IO.Request({
		    controller: this.getName(),
		    method: 'remove',
		    type: 'GET',
		    data: {id: id}
		    });        

		// Handle return of the form JSON
		removeGetReq.bind('afterSuccess', function (event, data) {
			var ctr = data.ctr;
			var id = data.id;
			var refresh = data.refresh;
			var identity = event.data.data._identity;
			
            ctr.trigger('removeReady', function (event) {
				var ctr = event.data.ctr;
				var id = event.data.id;
				var identity = event.data.identity;
			
		        // Create the remove window
		        var win = new FLUID.Window({
		            name: 'deploy-window-remove-' + this.getName(),
		            group: 'singleModal',
		            label: _('Are you sure you wish to delete this #TABLE#?').replace('#TABLE#', this._verbose_name),
		            size: DEPLOY.Controller.DEFAULT_WINDOW_SIZES['alert'].slice(0),
		            flags: 
						FLUID.Window.HAS_ACTIONS |
		                FLUID.Window.CAN_CLOSE |
		                FLUID.Window.CENTERED
		            });

		        win.unbind('afterCloseComplete');
		        win.bind('afterCloseComplete', function(event, data) {
					this.destroy();
					FLUID.widgetByName('desktop').unbind('onEndModal.deploy');
					FLUID.widgetByName('desktop').endModal(FLUID.Desktop.SINGLE_MODAL);
					FLUID.widgetByName('desktop').removeClass('fluid-frozen');
		        });
			
				// Message
				var msg = new FLUID.Text({ 
					parent: win,
					content: _(
						'Please confirm you would like to delete - <strong>#RECORD#</strong> - by selecting <strong>Confirm</strong> below.'
						).replace('#RECORD#', identity)
				});
        
				// Confirm
				var confirmBtn = new FLUID.Button({
		        	parent: win,
					group: 'actions',
					icon: 'deploy.icons.views.accept',
		        	label: _('Confirm')
		    	});
		        confirmBtn.bind('onClick', function (event, data) { 
					var ctr = data.ctr;
					var win = data.win;
					var id = data.id;
					var refresh = data.refresh;
			
					// Remove the record
			        var removePostReq = new IO.Request({
			            controller: ctr.getName(),
			            method: 'remove',
			            type: 'POST',
			            data: {id: id, confirm: 'confirm'}
			            });
					
	            	removePostReq.bind('afterSuccess.remove.deploy', function (event, data) {
						data.ctr.trigger('removeSave', function (event, data) {

						}, {data: {saveEvent: event}});
					}, {ctr: ctr});
					
			        removePostReq.bind('afterSuccess', function (event, data) {
						var ctr = data.ctr;
						var win = data.win;
						var refresh = data.refresh;
					
			            win.unbind('afterCloseComplete');
						
						if (refresh) {
							window.location.reload(true);
							return;
						}
					
			            win.bind('afterCloseComplete', function(event, data) {
		                	
							// Check if the list or search window is open
							var listWin = FLUID.widgetByName('deploy-window-search-' + data.ctr.getName());
							if (listWin === undefined) {
								listWin = FLUID.widgetByName('deploy-window-list-' + data.ctr.getName());
							}
							if (listWin !== undefined) {
								// Reload the current list view
								var paginator = listWin.widgetData('paginator');
								if (paginator.getPage()) {
									paginator.goToPage(paginator.getPage().number);
								} else {
									paginator.goToPage(1);
								}
							}
							var listTab = FLUID.widgetByName('deploy-tab-search-' + data.ctr.getName());
        					if (listTab === undefined) {
        						listTab = FLUID.widgetByName('deploy-tab-list-' + data.ctr.getName());
        					}					
        					if (listTab !== undefined) {
        						// Reload the current list view
        						var paginator = listTab.widgetData('paginator');
        						if (paginator.getPage()) {
        							paginator.goToPage(paginator.getPage().number);
        						} else {
        							paginator.goToPage(1);
        						}
        					}
							// Close any related update windows
							var viewWin = FLUID.widgetByName('deploy-window-update-' + data.ctr.getName());						
							if (viewWin !== undefined) {
								// Close the window if it is displaying the current window
								if (viewWin.widgetData('id') === data.id) {
									viewWin.close();
								}
							}
						
							this.destroy();
							FLUID.widgetByName('desktop').removeClass('fluid-frozen');
							FLUID.widgetByName('desktop').unbind('onEndModal.deploy');
							FLUID.widgetByName('desktop').notify(data.msg);
						
			            }, {ctr: ctr, id: id, msg: event.data.data.msg});
			            win.close();
						
			        }, {ctr: ctr, win: win, refresh: refresh});

			        removePostReq.send();

		        }, {ctr: ctr, win: win, id: id, refresh: refresh});
			
				// Cancel
				var cancelBtn = new FLUID.Button({
		        	parent: win,
					group: 'actions',
		        	label: _('Cancel'),
					icon: 'deploy.icons.views.cancel',
					classes: ['fluid-right']
		    	});
		        cancelBtn.bind('onClick', function (event, data) { 
		            data.win.close();
		        }, {win: win});
			
				// Store useful references against the window
				win.widgetData('id', id);		
				win.widgetData('buttons', {
				    'confirmBtn': confirmBtn,
				    'cancelBtn': cancelBtn
				});
				
			    // Render, Prepare & Open the window
			    win.refresh();
			    win.prepare();
			    win.open();
				
			}, {data: {ctr: ctr,  id: id, refresh: refresh, identity: identity}});
			
		}, {ctr: this, id: id, refresh: refresh});
		
		// Load the remove info
        this.trigger('removeInit', 
            function (event) {
				event.data.removeGetReq.send();
            },
            {data: {removeGetReq: removeGetReq}}
		);
    },
	
    /**
     * Default export method.
     */
	export_csv: function () {
		window.location = IO.Request.buildURL(this.getName(), 'export-csv') + '?@=' + IO.Request.getId();
	},
    
    defaultOptions: {
        visit_page_label: 'Visit page', 
        visit_page_new_window: false
        }
});


/**
 * Utitlity window for asking the user for confirmation.
 */
DEPLOY.ConfirmDialog = function (options, bindEvents) {
    options = UTILS.optionsArg(options, this.defaultOptions);
    options.group = 'singleModal';
	options.size = DEPLOY.Controller.DEFAULT_WINDOW_SIZES['alert'].slice(0);
	options.flags = FLUID.Window.HAS_ACTIONS | FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED;
	
    FLUID.Window.call(this, options, false);
    this._type.push('DEPLOY.ConfirmDialog');
    
	// Message
	var msg = new FLUID.Text({ 
		parent: this,
		content: options.message
	});
    
	// Confirm
	this.confirmBtn = new FLUID.Button({
       	parent: this,
		group: 'actions',
		icon: 'deploy.icons.views.accept',
       	label: _('Confirm')
   	});
	this.confirmBtn.bind('onClick', function (event, data) { 
		data.win.trigger('confirm', function (event) {
	    	event.data.win.close();
		}, {data: {win: data.win}});
	}, {win: this});

	// Cancel
	this.cancelBtn = new FLUID.Button({
       	parent: this,
		group: 'actions',
       	label: _('Cancel'),
		icon: 'deploy.icons.views.cancel',
		classes: ['fluid-right']
   	});
	this.cancelBtn.bind('onClick', function (event, data) { 
		data.win.trigger('cancel', function (event) {
	    	event.data.win.close();
		}, {data: {win: data.win}});
	}, {win: this});

	// Close event
	this.bind('afterCloseComplete', function(event, data) {
		this.destroy();
		FLUID.widgetByName('desktop').unbind('onEndModal.deploy');
		FLUID.widgetByName('desktop').endModal(FLUID.Desktop.SINGLE_MODAL);
		FLUID.widgetByName('desktop').removeClass('fluid-frozen');
	});

    if (bindEvents === undefined || bindEvents) {
        this._bindEvents();
    }
}
UTILS.copyPrototype(DEPLOY.ConfirmDialog, FLUID.Window);

$$.extend(DEPLOY.ConfirmDialog.prototype, {
    __className__: 'DEPLOY.ConfirmDialog',

	launch: function () {
		var desktop = FLUID.widgetByName('desktop');
        
        // Enter *single* model state
        desktop.startModal(FLUID.Desktop.SINGLE_MODAL);
        desktop.bind('onEndModal.deploy', function (event) {
			event.stopDefault();
		});
		desktop.addClass('fluid-frozen');
		
		// Render, Prepare & Open the window
	    this.refresh();
	    this.prepare();
	    this.open();
	}
});

// @@ Multiple attachments within views
// @@ Can you close a window if auto saving is on? Does it auto save on close, refuse to close and ask the user, or just cancel the changes and close
// @@ Tell nick system needs to email admins on create/update/remove
