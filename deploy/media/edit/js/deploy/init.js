/**
 * @fileoverview    Initialisation script for the deploy client framework.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        IO (/js/core/io.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @requires        FLUID (resource) (/js/deploy/theme.js)
 */

'use strict';


/**
 * Initialise the DeployCMS interface.
 */
DEPLOY.init = function () {
    
    // Setup the desktop
    var desktop = new FLUID.Desktop();
    
    DEPLOY.getAdministrator().bind('afterSessionFound.init', 
        function (event, data) {
            
            // Configure the administrator
            this.configure(event.data.data);
            
            // Get the administrators theme
            var themeReq = new IO.Request({
                controller: 'deploy-administrator',
                method: 'get-theme'
                });
        
            themeReq.bind('afterSuccess.init', function (event, data) {
                
                var themeName = event.data.data.theme;
                var theme = DEPLOY.getTheme(themeName);
                
                theme.bind('afterComplete.init', function (event) {
                
                    // Get the administrators language string table
                    var languageReq = new IO.Request({
                        controller: 'deploy-administrator',
                        method: 'get-language-table'
                        });
                     
                    languageReq.bind('afterSuccess.init', function (event) {
                        
                        // Update the string table
                        LOCALIZATION.stringTable = event.data.data.string_table;
                        
                        // Initialise the GUI
                        DEPLOY.getAdministrator().initGUI();
                    });
                    
                    languageReq.send(); 
                    
                    theme.unbind('afterComplete.init');     
                });        
                
                // Load the theme
                FLUID.widgetByName('desktop').theme(theme);
            });
            
            themeReq.send();
        });

    DEPLOY.getAdministrator().bind('afterSessionExpired.init', 
        function (event, data) {
            
            // Get the default site theme
            var themeReq = new IO.Request({
                controller: 'deploy-site',
                method: 'get-theme'
                });
            
            themeReq.bind('afterSuccess.init', function (event, data) {
                var themeName = event.data.data.theme;
                var theme = DEPLOY.getTheme(themeName);
                
                theme.bind('afterComplete.init', function (event) {
                
                    // Get the default site string table
                    var languageReq = new IO.Request({
                        controller: 'deploy-site',
                        method: 'get-language-table'
                        });
                                
                    languageReq.bind('afterSuccess.init', function (event) {
                        
                        // Update the string table
                        LOCALIZATION.stringTable = event.data.data.string_table;
                        
                        // Ask the administrator to login
                        DEPLOY.getController('deploy-administrator').signIn();
                    });
                    
                    languageReq.send();
                    
                    theme.unbind('afterComplete.init');
                });        
                
                // Load the theme
                FLUID.widgetByName('desktop').theme(theme);
            });
            
            themeReq.send();
        });

    // Attempt to get an administrator by session
    DEPLOY.getAdministrator().remoteGet();
}

$$(document).ready(function (ev) {
    window.onload = DEPLOY.init();
});