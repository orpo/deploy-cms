/**
 * @fileoverview    Library to support creating and managing FLUID forms 
 *                  from JSON output by Deploy.  
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        ???
 */

'use strict';


/**
 * Create a Form from JSON.
 */ 
DEPLOY.createForm = function (options) {
    options = UTILS.optionsArg(options,
		{controller: null, id: null, view: null, autosave: false, json: null});
	
    var i = 0;
    var j = 0;
    var form;
    var formData;
    var fieldMap;
    var fieldClass;
    var fieldData;
    var fields;
    var fieldset;
    var fieldName;
    var field;
    var sourceField;
    var idField;
    var json;
    var option;
    var optionClass;
    var attributeName;
    var fieldDataMap;
    var fieldsetData;
    var defaultFieldset;
    var realtimeURL;
    var realtimeData;

    // If the JSON is still a string convert it to an object
	json = options.json;
    if (json.constuctor === String) {
        json = $$.parseJSON(json);
    }
    
	// Build the realtime URL and data
	realtimeURL = IO.Request.buildURL(options.controller.getName(), 'realtime');
	realtimeData = {
		_action: 'VALIDATE',
		_form: options.view
	};
	if (options.autosave && options.view === 'UPDATE') {
		realtimeData['_action'] = 'SAVE';
	}
	if (options.view === 'UPDATE') {
		realtimeData['_id'] = options.id;
	}
    if (json.prevent_realtime_save === true) {
        realtimeData['_action'] = 'VALIDATE';
    }
	
    // Form
    formData = json.form;
    form = new FLUID.Form({
        action: formData.action,
        method: formData.method,
        realtimeURL: realtimeURL,
        realtimeData: realtimeData
        });     
    	
	if (options.autosave && options.view === 'UPDATE' && json.prevent_realtime_save !== true) {
		// As the form will be updating via the realtime feature we prevent
		// standard submits.
		form.bind('onSend', function (event) {
			event.stopDefault();
		});
	}
	
    // Handle submissions with empty required fields
    form.bind('afterSendNotReady.deploy', function (event, data) {
        var field = event.data.field;
	    var msg = '';
		
		// Notify the user that they've missed out a field
		switch (field.state()) {
			case FLUID.Field.INVALID_STATE:
				msg = _('Not all your fields are valid - ') + event.data.field.label();
				break;
				
			case FLUID.Field.CHECKING_STATE:
				msg = _('Not all your fields have finished validating - ') + event.data.field.label();
				break;
				
			default:
			    if (event.data.field.label === undefined) {
				    msg = _('You need to specify - ') + event.data.field.getName();
		        } else {
	                msg = _('You need to specify - ') + event.data.field.label();
	            }
		};
		
		FLUID.widgetByName('desktop').notify(msg);
	});
   
    // Handle submissions with invalid fields
    form.bind('afterSendFail.deploy', function (event, data) {
        // Find all the fields containing errors
        var invalidFields = [];
        var invalidFieldNames = [];
        var fieldMap = this.getFieldMap();
        
        for (var field_name in event.data.data.errors) {
            if (event.data.data.errors.hasOwnProperty(field_name)) {
                field = fieldMap[field_name];
                field.stateMessage(event.data.data.errors[field_name]);
                field.state(FLUID.Field.INVALID_STATE);
                invalidFields.push(field);
                invalidFieldNames.push(field.label());
            }
        }
        
        if (invalidFields.length > 0) {
            // Notify the user of the fields that have errors
            FLUID.widgetByName('desktop').notify(_('Not all your fields are valid - ') + invalidFieldNames.join(', '));
        } else {
            FLUID.widgetByName('desktop').notify(event.data.data.msg);
        }
    });
    
    // Fieldsets
    
    // If no fieldsets exists create a default one to encompass all fields
    if (formData.fieldsets === undefined || formData.fieldsets.length === 0) {
        defaultFieldset = new FLUID.Fieldset({parent: form, classes: ['fluid-last']});
        formData.fieldsets = [];
    }
    
    var fieldsets = {};
	var lastFieldset = defaultFieldset;
    for (i = 0; i < formData.fieldsets.length; i++) {
        fieldsetData = formData.fieldsets[i];
        
        fieldset = new FLUID.Fieldset({
            parent: form,
            legend: fieldsetData.legend
            });
            
        if (i == formData.fieldsets.length - 1) {
            fieldset.addClass('fluid-last');
			lastFieldset = fieldset;
        }
        
        for (j = 0; j < fieldsetData.fields.length; j++) {
            fieldsets[fieldsetData.fields[j]] = fieldset;
        }
    }
    
    // Build a field data map
    fieldDataMap = {};
    for (i = 0; i < formData.fields.length; i++) {
        fieldDataMap[formData.fields[i].name] = formData.fields[i];
    }
    
    fields = [];
    if (formData.fieldsets.length === 0) {
         fields = formData.fields;
    } else {
        for (i = 0; i < formData.fieldsets.length; i++) {
            fieldsetData = formData.fieldsets[i];
            for (j = 0; j < fieldsetData.fields.length; j++) {
                fields.push(fieldDataMap[fieldsetData.fields[j]]);
            }
        }
    }
    
    // Fields
    for (i = 0; i < fields.length; i++) {
        fieldData = formData.fields[i];
        fieldClass = FLUID.Field[fieldData.type];
        
        // Check the field is defined
        if (fieldClass === undefined) {
	
			// Check for special case fields
			switch (fieldData.type) {
				
			case 'Attachment':
				if (fieldData['attachment_max'] === 1) {
					fieldClass = DEPLOY.SingleAttachmentField;
					if (fieldData['value'].length > 0) {
						fieldData['attachment'] = new DEPLOY.Attachment({
							path: fieldData['value'][0].path,
							thumbnail_path: fieldData['value'][0].thumbnail_path,
							meta: fieldData['value'][0].meta,
							expectedMeta: fieldData['value'][0].expected_meta
							});
					}
				} else {
					fieldClass = DEPLOY.MultipleAttachmentField;
					fieldData['attachments'] = [];
					for (j = 0; j < fieldData['value'].length; j++) {
						fieldData['attachments'].push(
							new DEPLOY.Attachment({
								path: fieldData['value'][j].path,
							    thumbnail_path: fieldData['value'][j].thumbnail_path,
								meta: fieldData['value'][j].meta,
								expectedMeta: fieldData['value'][j].expected_meta
								})
						);
					}
				}
				fieldData['controller'] = options.controller;
				break;
				
			default:
				fieldClass = FLUID.Field.Entry;
				
			}
        }	
        
        // Check for special attribute names
        fieldDataMap = {
            'allow_create': 'allowCreate',
            'datalist_url': 'datalistURL',
            'mask_value': 'maskValue'
        };
        for (attributeName in fieldDataMap) {
            if (fieldDataMap.hasOwnProperty(attributeName)) {
                if (fieldData[attributeName] !== undefined) {
                    fieldData[fieldDataMap[attributeName]] = fieldData[attributeName];
                }
            }
        }
        
        // Assign the field to a fieldset
        if (fieldsets[fieldData.name] === undefined && 
                defaultFieldset !== null) { // @@ Need to look at this tests
            fieldData.parent = defaultFieldset;
        } else if (fieldsets[fieldData.name] !== undefined) {
            fieldData.parent = fieldsets[fieldData.name];
        } else {
            throw new Error('Field has no field set: ' + fieldData.name);
        }
        		
		// Handle help
		if (fieldData.help !== null) {
			switch (fieldData.type) {

			case 'Attachment':
				break;

			default:
				new FLUID.Text({
					parent: fieldData.parent,
					content: fieldData.help,
					classes: ['fluid-help']
				});
			}
		}

        // Create the field
        field = new fieldClass(fieldData);
        
        // Field options 
        if (fieldData.options !== undefined) {
            for (j = 0; j < fieldData.options.length; j++) {
                option = fieldData.options[j];
                option.parent = field;
                
                optionClass = field.getOptionClass();
                new optionClass(option);
            }
            field.value(fieldData.value);
        } 
    }
    
    // Now we've added all the fields to the form we can map any links
    // between linked fields.
    for (fieldName in formData.linked) {
        if (formData.linked.hasOwnProperty(fieldName)) {
            form.linkField(fieldName, formData.linked[fieldName]);
        }
    }

	// If an ID has been provided add this as a hidden field
	if (options.id !== null) {
		idField = new FLUID.Field.Hidden({
			parent: lastFieldset,
			name: 'id',
			value: options.id			
		});
	}
    
    /* Look for special attributes */
    fieldMap = form.getFieldMap();
    for (i = 0; i < fields.length; i++) {
        fieldData = formData.fields[i];
        
        /* Slugify (build the fields value as a slug from another field) */
        if (fieldData['attribs'] && fieldData['attribs']['slugify'] !== undefined) {
            
            /* Check the source field exists */
            field = fieldMap[fieldData['name']];
            sourceField = fieldMap[fieldData['attribs']['slugify']];
            if (sourceField === undefined) {
                continue;
            }
            
            /* Add an event to the source so that we capture changes */
            sourceField.bind('onKeyup.deploy, onChange.deploy', function (event, data) {
                data.field.value('/' + UTILS.slugify($$.trim(this.getInputSelection().val())) + '/');
                data.field.inactRealtime();
            }, {field: field});
        }
    }
    
    return form;
}