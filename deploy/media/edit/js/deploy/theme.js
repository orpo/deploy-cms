/**
 * @fileoverview    Deploy theme management.
 * @version         1.0
 * @author          Anthony Blackshaw <ant@getme.co.uk>
 * @requires        UTILS (/js/core/utils.js)
 * @requires        EVENT (/js/core/event.js)
 * @requires        LOCALIZATION (/js/core/localization.js)
 * @requires        IO (/js/core/io.js)
 * @requires        FLUID (base) (/js/fluid/base.js)
 * @requires        FLUID (resource) (/js/fluid/resource.js)
 * @
 */

'use strict';


/**
 * Register a theme.
 * @param {DEPLOY.Theme} theme The theme to register.
 */ 
DEPLOY.registerTheme = function (theme) {
    DEPLOY._themes[theme.getName().toLowerCase()] = theme;
}

/**
 * Return the registered theme associated with the specified name.
 * @param {String} name The name of the theme being requested.
 * @return A registered theme.
 * @type DEPLOY.Theme
 */ 
DEPLOY.getTheme = function (name) {
    if (DEPLOY._themes[name.toLowerCase()] === undefined) {
        throw new Error('No theme registered by the name: ' + name);
    }
    return DEPLOY._themes[name.toLowerCase()];
}

// Register the default theme
DEPLOY.registerTheme(new FLUID.Theme({
    name: 'sw-theme',
    css: [
        '/media/edit/themes/sw-theme/ui.css', 
        '/media/edit/themes/sw-theme/ui.forms.css',
        '/media/edit/themes/sw-theme/deploycms.css',
        '/media/edit/themes/sw-theme/editable.css',
        ],
    resources: ['/media/edit/themes/sw-theme/resources.js'],
    templates: ['/media/edit/themes/sw-theme/templates.js'],
    extensions: ['/media/edit/themes/sw-theme/extensions.js']
}));