"""Default Settings for Deploy"""

import os.path
import datetime
import logging

from deploy.utils._time import GMT0

LOCALE = ''
ENCODING = 'utf-8'

#### Applications ###############################################################################
APPLICATION = 'deploy.config.defaultapp'	# Location of the site application
APPLICATION_NAME = None						# Name of the application
APPLICATION_NAMESPACE = 'com.example'		# Application WSGI namespace
APPLICATION_PATHS = []						# Paths to application support libs
APPLICATION_SETUP_ARGS = {}					# Arguments to be sent to the application on startup

#### Site Settings ##############################################################################
SITE_URL = ''								# The domain the site application runs on
SITE_EDIT_URL = ''							# The domain that the edit application runs on
SITE_NAME = ''								# Site name
SITE_EMAIL = 'root@localhost'				# Default email address the site will send from
SITE_SKIN = 'default'						#
SITE_MEDIA_PREFIX = '/media'				#

# Site admins
SITE_ADMINISTRATORS = ()					# List of site administrators ('NAME', 'EMAIL')

# Directories
SITE_ROOT = ''								# The filesystem location of the site
SITE_WWW_ROOT = ''							# The filesystem location of the sites www root
SITE_DATA_ROOT = ''							# The filesystem location of the sites data dir

# Cryptography
PASSWORD_SALT = ''							# Random hash used for encryping information

#### SERVER #####################################################################################
SERVER_IP = '127.0.0.1'						# The IP address the server runs on
SERVER_ROOT = SITE_ROOT						# The filesystem root the server will run from
SERVER_PORT = 8888
SERVER_USER = 'nobody'
SERVER_GROUP = 'nogroup'
SERVER_PID_FILE = 'deployserver.pid'
SERVER_THREADS = 10
SERVER_TIMEOUT = 60
SERVER_SHUTDOWN_TIMEOUT = 5
SERVER_REQUEST_QUEUE_SIZE = 5
SERVER_RUN_DIR = ''
SERVER_DEFAULT_CHMOD = 0644
SERVER_UMASK = 0022

SERVER_SSL_CERTIFICATE = None
SERVER_SSL_PRIVATE_KEY = None

#### URLs #######################################################################################
URL_PROTECTED_PATH_LIST = ['edit/', 'img/', 'js/', 'css/']
URL_PROTECTED_LIST = ['/robots.txt', '/favicon.ico']
URL_ADD_TRAILING_SLASH = True

#### Templating #################################################################################
# Specify a templating engine for deploy: (mako)
TEMPLATE_ENGINES = ('mako', 'jinja')
TEMPLATE_LOCATIONS = []
TEMPLATE_CONFIG = {}

#### Sessions ###################################################################################
SESSION_NAME = 'deploy_session'
SESSION_DURATION = 5*24*60*60 # In seconds (default 5 days)

#### Files ####################################################################
FILES_SUPPORTED_EXTENSION_LIST = ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt',
								'pptx', 'txt']

#### Imagery ##################################################################
IMAGE_QUALITY = 90
IMAGE_MAX_SIZE = [1024, 1024]
IMAGE_MAX_FILE_SIZE = 1024 * 1024 * 10
IMAGE_SUPPORTED_FORMAT_LIST = ['GIF', 'JPEG', 'PNG']
IMAGE_SUPPORTED_EXTENSION_LIST = ['gif', 'png', 'jpg', 'jpeg']
IMAGE_DEFAULT_INSERT_SIZE = [400,0]
THUMBNAIL_SIZE = [100, 100]
THUMBNAIL_QUALITY = 50
THUMBNAIL_MAX_FILE_SIZE = 1024 * 250

#### Documents ################################################################
DOCUMENTS_MAX_FILE_SIZE = 1024 * 1024 * 5

#### Email ####################################################################
EMAIL_HOST = 'localhost'
EMAIL_USERNAME = None
EMAIL_PASSWORD = None
EMAIL_PORT = 25

#### Error Management/Logging/Debugging ########################################
DEBUG = False
DEBUG_LOG = os.path.join(SITE_ROOT, 'debug.log')
DEBUG_LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'
DEBUG_LOG_LEVEL = logging.INFO
HTTP_LOG = ''
ERROR_500_EMAIL = True
ERROR_404_EMAIL = False
ERROR_EMAIL_OVERIDE = None
ERROR_HANDLERS = {
	500: 'deploy.core.views.error_500',
	404: 'deploy.core.views.error_404',
}

#### Database #################################################################
DATABASE_URI = ''
DATABASE_DEBUG = False

#### Cache ####################################################################
CACHE_ENABLED = False
CACHE_INTERFACE = 'flatfile'
CACHE_ROOT = ''
CACHE_DEFAULT_EXTENSION = '.txt'
CACHE_OPTIONS = {
	# Items that may reside in the cache but are not to be removed by a call to
	# clear()
	'EXCEPTIONS': ('img/', 'js/', 'css/'),
	'ALLOWED_FILE_EXTENSIONS': ('.html', '.htm', '.xml', '.json'),
	'DEFAULT_MIMETYPE': 'text/plain'
}

##### i18n ####################################################################
I18N_ENABLED = False

##### Plugins #################################################################

# A list of available plugins
PLUGINS = []

# Allow plugin settings to modify local & default settings
PLUGIN_SETTINGS_OVERRIDE = False

# Flag to determine if loading a plugin should halt a system init if it's
# settings file can not be loaded
PLUGIN_FAIL_QUIETLY = False

# Manually specificy the location of the plugins
PLUGIN_LOCATION = None

##### Resources ###############################################################

# A list of global resources to be outputted with each view
RESOURCE_MAP = {
	'css': [],
	'js': [],
}

RESOURCES_CACHE = 'media/cache'

##### Snippets ################################################################
SNIPPETS_ENABLED = False

#### Deploy Settings ##########################################################

DEFAULT_TIMEZONE_INFO = GMT0

HTML_SERIALIZER_OPTIONS = {
	'quote_attr_values': True,
	'strip_whitespace': True,
	'use_trailing_solidus': True,
	'omit_optional_tags': False,
	'minimize_boolean_attributes': False
}

# These settings do not normally need to be overwritten
USER_AGENT = 'Deploy CMS 1.0'
SERVER_AGENT = 'Deploy Server/1.0'

### Private Settings used for testing etc...
MEDIA_EXPIRES = {
	'text/js': datetime.timedelta(days=5),
	'text/css': datetime.timedelta(days=5),
}
