import os.path

from deploy.config import settings
from deploy.applications import DeployApp, DeployStaticFileApp
from deploy.applications.db import DeployDatabaseApp
from deploy.applications.exception import DeployExceptionApp
from deploy.applications.log import DeployLoggerApp
from deploy.applications.routing import DeployCouplingDispatcher

from deploy.cms.applications import DeployCMSApp

# Default WSGI application stack

def application(args):
	"""Tool to build"""

	# Build the application pipeline for serving dynamic pages
	deploy_app = DeployApp()
	deploy_app = DeployCMSApp(deploy_app)
	deploy_app = DeployDatabaseApp(deploy_app, settings.DATABASE_URI, debug=settings.DATABASE_DEBUG)

	# Put it all together
	app = DeployCouplingDispatcher({'/': deploy_app})
	app = DeployExceptionApp(app)
	app = DeployLoggerApp(app, log=settings.HTTP_LOG, console_log=args['VERBOSE'])

	return app