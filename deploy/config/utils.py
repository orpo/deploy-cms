"""Utilities used for configuring a deploy application"""

import os
import os.path
import mimetypes
import sys
import logging

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError
from deploy.core.urls import __resolver__
from deploy.plugins import __plugins__
from deploy.utils.importing import import_module

__all__ = ('get_application_name', 'setup_environ', 'get_application_pipeline',
	'bootstrap')

def get_application_name(root):
	"""Utility to get the deploy application name in a virtualenv environment."""

	if os.path.exists(os.path.join(root, '.deployapp')):
		return os.path.basename(root)

	# Get the name of the application from the application path
	application_name = os.path.basename(os.path.abspath(root))
	application_name = application_name.replace('.', '_').replace('-', '_').lower()

	# Does that application exist?
	if os.path.exists(os.path.join(root, 'lib', application_name)):
		return application_name

	# We no longer name library folders after the website, just return local if found within lib
	if os.path.exists(os.path.join(root, 'lib', 'local')):
		return 'local'

	# Application is ambiguous, try to find it
	dirs = os.listdir(root)

	for d in dirs:
		if os.path.exists(os.path.join(root, d, '.deployapp')):
			return os.path.basename(d)

	return None

def setup_environ(root):
	"""Utility to configure the deploy environment"""

	# Try to get the application name
	application_name = get_application_name(root)

	if not application_name:
		raise ConfigurationError(
			'Unable to find Application! Check it is either located inside '
			'lib OR your app contains a ".deployapp" file.'
		)

	# Add system paths we will be using
	sys.path.insert(0, os.path.join(root))

	# Test where the application is, and add a path to it
	if os.path.exists(os.path.join(root, 'lib', application_name)):
		sys.path.insert(1, os.path.join(root, 'lib'))
		sys.path.insert(2, os.path.join(root, 'lib', application_name))
	else:
		sys.path.insert(1, os.path.join(root, application_name))

	# If settings is specified, have a guess what they may be (default PROJECT.setting)
	if not os.environ.has_key('DEPLOY_SETTINGS'):
		os.environ['DEPLOY_SETTINGS'] = '%s.settings' % application_name

	# Setup the Egg Cache
	egg_cache = os.path.join(root, 'tmp/.python-eggs')

	if not os.path.exists(egg_cache):
		os.makedirs(egg_cache)

	os.environ['PYTHON_EGG_CACHE'] = egg_cache

	# Add the mimetypes we need in deploy
	if sys.version_info < (2, 6):
		mimetypes.add_type('application/json', '.json')

	return application_name

def bootstrap(settings_path=None):
	"""Utility to bootstrap deploy environment - should only be used if the DeployApp
	is not being used, typically this is because something like a cron is running Deploy"""

	if not os.environ['VIRTUAL_ENV']:
		# @@ TODO Work out where we are when not in a virtual env
		raise NotImplementedError('Looks like your running from a non virtual env - havent got this working yet, sorry :(')

	else:
		root = os.path.abspath(os.environ['VIRTUAL_ENV'])

	# Setup the environment and get our application name
	application_name = setup_environ(root)

	if settings_path is not None:
		os.environ['DEPLOY_SETTINGS'] = settings_path

	# Load our local settings
	settings.load(os.environ['DEPLOY_SETTINGS'], force=True)
	settings.load({'APPLICATION_NAME': application_name}, force=True)

	# Setup the log...
	logging.basicConfig(
		level=logging.DEBUG,
		format=settings.DEBUG_LOG_FORMAT,
		filename=settings.DEBUG_LOG,
		filemode='a+'
	)

	log = logging.getLogger('deployapp')

	# Start building the application
	application = import_module(settings.APPLICATION_NAME, fromlist=['urls'])

	__resolver__.add(application.urls)

	DEPLOY_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

	# Append the default deploy template path
	if isinstance(settings.TEMPLATE_LOCATIONS, (list, tuple)):
		settings.TEMPLATE_LOCATIONS.append(os.path.join(DEPLOY_ROOT, 'templates'))

	# Install local plugin path if it exists
	if os.path.exists(os.path.join(settings.SITE_ROOT, 'plugins')):
		sys.path.insert(0, os.path.join(settings.SITE_ROOT, 'plugins'))

	# Setup any other specified paths for the application
	if settings.APPLICATION_PATHS:
		counter = 1
		for app_path in settings.APPLICATION_PATHS:
			sys.path.insert(counter, app_path)
			counter += 1

	# Install plugins
	__plugins__.install(settings.PLUGINS)

def get_application_pipeline(args=None):
	"""Tool to load the WSGI application pipeline"""

	application_module = import_module(settings.APPLICATION, fromlist=['application'])

	if not hasattr(application_module, 'application'):
		raise ConfigurationError('Unable to import Application from "%s"' % settings.APPLICATION)

	return application_module.application(args)
