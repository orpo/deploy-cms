"""Settings module

This provides a settings object that can be used globally throughout
deploy applications.
"""

import os
import datetime
import inspect
import logging
import types

__all__ = ('settings',)

PROTECTED_SETTINGS = 'DATABASE_URI', 'EMAIL_PASSWORD', 'PASSWORD_SALT'

log = logging.getLogger('deployapp.settings')

class SettingsManager(object):
	"""Settings manager provides a single 'registry' for the Web application which
	contains configuration and state information"""

	_instance = None
	_settings = None

	def __new__(cls, *args, **kwargs):
		# Create with a Singleton Pattern
		if not cls._instance:
			cls._instance = super(SettingsManager, cls).__new__(cls, *args, **kwargs)

		return cls._instance

	def __init__(self, load_defaults=True):
		# Setup the settings object to store all of the actual settings
		self.__dict__['_settings'] = self.Settings()

		if load_defaults:
			# Load the default system settings
			self.load('deploy.config.defaults')

		log.debug('Settings singleton initialised')

	def __getattr__(self, name):
		if hasattr(self._settings, name):
			return getattr(self._settings, name)

		raise NameError('settings has no field "%s"' % name)

	def __setattr__(self, attr, value):
		if hasattr(self, attr):
			super(SettingsManager, self).__setattr__(attr, value)
		else:
			setattr(self._settings, attr, value)

	def __unicode__(self):
		return '<DeploySettings for %s>' % self._settings.SITE_URL

	def __str__(self):
		return self.__unicode__()

	def __repr__(self):
		return '<DeploySettings %s>' % dir(self._settings)

	def __dir__(self):
		return dir(self._settings)

	def __getstate__(self):
		return self._settings.__dict__.copy()

	def __setstate__(self, dic):
		self._settings.__dict__ = dic

	def get_current_settings(self):
		"""Returns a *safe* list of the current settings. Safe refers to the fact that
		some settings are considered private and are blanked out - such as th databse
		uri or email password. (a full list is contained in
		deploy.config.PROTECTED_SETTINGS)"""

		settings = self.__getstate__()

		for protected in PROTECTED_SETTINGS:
			if protected in settings:
				settings[protected] = '*' * len(str(settings[protected]))

		return settings

	def load(self, settings, force=True):
		"""Function to load settings into the current settings object. 'settings' argument
		can be specified as a string/unicode module path, in which case the function will
		attempt to import the module. It can also be specified as a module directly or as
		a dictionary."""

		if isinstance(settings, (str, unicode)) or isinstance(settings, types.ModuleType):
			if isinstance(settings, (str, unicode)):
				log.debug('Loading Settings: "%r"' % settings)

				# Attempt to load the specified settings module
				try:
					settings = __import__(settings, {}, {}, [''])
				except ImportError, e:
					log.error('Unable to load Settings: "%r" because %s' % (settings, e))

			else:
				log.debug('Loading Settings: "%r"' % settings.__name__)

			setting_values = inspect.getmembers(settings)

			for name, value in setting_values:
				if not name.startswith('_') and not inspect.ismodule(value):
					if force or not hasattr(self._settings, name.upper()):
						setattr(self._settings, name.upper(), value)

		elif isinstance(settings, dict):
			log.debug('Loading Settings Dictionary: "%r"' % str(settings))

			for name, value in settings.items():
				if not name.startswith('_'):
					if force or not hasattr(self._settings, name.upper()):
						setattr(self._settings, name.upper(), value)

		return True

	class Settings(object):
		pass

settings = SettingsManager()
