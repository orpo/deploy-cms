"""Deploy WSGI Middleware Application: Exceptions"""

import sys
import logging
import traceback

from deploy.config import settings
from deploy.core.exceptions import DeployBaseException
from deploy.core.http import Response
from deploy.core.http.exceptions import HTTPError, InternalServerError

log = logging.getLogger('deployapp.exceptions')

class DeployExceptionApp(object):
	"""The Deploy WSGI Exception Application is designed to site in front of the
	application stack and catch any errors that may occur. It is then responsible for
	producing either friendly error messages or debugging information."""

	def __init__(self, app):
		self.app = app

	def __call__(self, environ, start_response):
		"""Run the application"""

		response = None

		# We create a start response catcher here, which has the effect of
		# buffering output. We need to be careful we don't release headers etc
		# until we are sure what we are sending is correct. By buffering here
		# we can recover if an error occurs as the response is moving back up
		# the stack.
		def lazy_start_response(status, headers, exc_info=None):
			lazy_start_response.lazy_status = status
			lazy_start_response.lazy_headers = headers
			lazy_start_response.lazy_exc_info = exc_info

			return True

		try:
			# Attempt to call the application..
			response = self.app(environ, lazy_start_response)

			# Response iterator has been returned, all O.K.
			# It's safe to start the response....
			start_response(
				lazy_start_response.lazy_status,
				lazy_start_response.lazy_headers,
				exc_info=lazy_start_response.lazy_exc_info
			)

			return response

		except Exception, e:
			# An error has occured at this point, attempt to recover

			exc_info = sys.exc_info()

			# Log the error
			if settings.DEBUG:
				tb = '\n'.join(traceback.format_exception(*exc_info))
				log.debug("An Error Occurred: %s\n%s" % (e, tb))
			else:
				log.error('An Error Occurred: %s' % e)


			# Catch everything....
			try:
				# Attempt to return either a formatted error page
				# or if settings allow, output debugging information
				response = self.handle_exception(e, environ)

				return response(environ, start_response)

			except Exception, e:
				# Fatal Error.
				# An error has occured from within the exception handling
				# subsystem - we can no longer recover.

				exc_info = sys.exc_info()

				tb = '\n'.join(traceback.format_exception(*exc_info))

				# Log this fatal error
				log.critical("Internal Server Error: %s\n%s" % (e, tb))

				# Create a faux response.
				# This is to stop a traceback being returned to the user.
				response = Response()
				response.status = 500

				# Only thing we can do is show a fatel error
				if settings.DEBUG:
					response.body = '<h1>Deploy CMS: Internal Server Error</h1><p>%s</p>' % str(e)
				else:
					response.body = '<h1>Deploy CMS: Internal Server Error</h1>'

				return response(environ, start_response)

	def handle_exception(self, exception, environ):
		"""An exception has occured, attempt to locate/create a HTTP response from the
		exception object"""

		response = None

		if isinstance(exception, HTTPError):
			# A Deploy HTTP exception has occured, these can return responses
			# of their own. (Could be 404, 500)
			response = exception.get_response(environ)

		elif isinstance(exception, DeployBaseException):
			# A Deploy exception has occurred which may be custom, give it a change to
			# return a response otherwise wrap it
			if hasattr(exception, 'get_response'):
				response = exception.get_response(environ)
			else:
				error = InternalServerError(exception)
				response = error.get_response(environ)
		else:
			# An exception has occured that is not Deploy specifc, wrap the exception
			# with a standard Deploy server error (500 response)
			error = InternalServerError(exception)
			response = error.get_response(environ)

		return response