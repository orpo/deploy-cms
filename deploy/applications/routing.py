"""Deploy WSGI Middleware Application: Routing"""

import types
import urlparse

from deploy.config import settings
from deploy.core.http.exceptions import PageNotFound
from deploy.core.urls import URL
from deploy.utils.importing import import_module

__all__ = ('DeployCouplingDispatcher', 'DeployRoutingApp')

class DeployCouplingDispatcher(object):
	""""""

	# Note: This is modified from the cherrypy WSGIPathInfoDispatch

	def __init__(self, apps):
		try:
			apps = apps.items()
		except AttributeError:
			pass

		# Sort the apps by len(path), descending
		apps.sort()
		apps.reverse()

		# The path_prefix strings must start, but not end, with a slash.
		# Use "" instead of "/".
		self.apps = [(p.rstrip("/"), a) for p, a in apps]

	def __call__(self, environ, start_response):
		# Get the correct path info or default
		if 'PATH_INFO' in environ:
			path = environ['PATH_INFO']
		else:
			path = '/'

		for p, app in self.apps:
			# The apps list should be sorted by length, descending.
			if path.startswith(p + "/") or path == p:

				# Copy the environment
				environ = environ.copy()

				# Attempt to get the
				if 'SCRIPT_NAME' in environ:
					script_name = environ['SCRIPT_NAME']
				else:
					script_name = '/'

				# Set the script name to the full request path
				# @@ FIXME: Should this be setting SCRIPT_NAME to the full request path??
				environ['SCRIPT_NAME'] = script_name + p

				environ['com.deploycms.routing.path_info'] = path[len(p):]

				return app(environ, start_response)

		raise PageNotFound(environ['PATH_INFO'])

class DeployRoutingApp(object):

	def __init__(self, app):
		self.app = app
		self.map = []
		self.cache = {}

		# Add the application URL's
		application = import_module(settings.APPLICATION_NAME, fromlist=['urls'])
		self.add(application.urls)

	def __call__(self, environ, start_response):

		path = environ['PATH_INFO']

		endpoint = self.resolve(path)

		if endpoint is None:
			raise PageNotFound(path, available_urls=self.get_all_urls())

		environ['com.deploycms.routing.view'] = endpoint

		return self.app(environ, start_response)

	def add(self, url_arg):
		"""Add a URL (or a list/tuple of URL's) to the resolvers
		internal Map"""

		if isinstance(url_arg, types.ModuleType):
			# Support for adding url lists from modules

			# Check it has the correct information in the module..
			if hasattr(url_arg, 'urls'):
				self.add(url_arg.urls)

		elif isinstance(url_arg, (list, tuple)):
			# Support for adding multiple URL's at once
			for url in url_arg:
				self.map.append(url)

		else:
			self.map.append(url_arg)

	def resolve(self, url):
		"""Resolve a string URL to a specific URL object"""

		parsed_url = urlparse.urlparse(url)
		url_path = parsed_url.path

		# Check the internal cache
		if url_path in self.cache:
			return self.cache[url_path].get_endpoint()

		# Check the internal map (1st match wins!!)
		for site_url in self.map:
			if site_url.match(url_path):
				# Add the positive match to the cache
				self.add_to_cache(url_path, site_url)

				# Return the URL's endpoint
				return site_url.get_endpoint()

		# Unable to resolve the URL
		return None

	def add_to_cache(self, url, url_match):
		"""Add a URL to the cache. This saves lookup time in future"""
		self.cache[url] = url_match

	def flush_cache(self):
		"""Clears the internal resolver cache"""
		self.cache = {}

	def get_all_urls(self):
		"""Return a list of all the current URL's in the resolver map"""
		return [url for url in self.map]
