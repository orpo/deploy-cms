"""Deploy WSGI applications"""

import sys
import os
import os.path
import mimetypes
import datetime
import stat
import time
import logging

from deploy.config import settings
from deploy.core.http import Request
from deploy.core.http.exceptions import InternalServerError, PageNotFound, HTTPForbidden, HTTPMethodNotAllowed
from deploy.core.http.utils import http_date
from deploy.core.urls import __resolver__
from deploy.plugins import __plugins__
from deploy.utils.importing import import_module

__all__ = ('DeployApp', 'DeployStaticFileApp')

log = logging.getLogger('deployapp')

class DeployApp(object):
	"""This application is responsible for providing a 'friendly' interface
	to the WSGI application stack. It should be the final application called
	in the stack and will then attempt to map requests to relevant callable
	'views"""

	def __init__(self):
		# Import the base application URL's
		application = import_module(settings.APPLICATION_NAME, fromlist=['urls'])
		__resolver__.add(application.urls)

		DEPLOY_ROOT = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

		# Append the default deploy template path
		if isinstance(settings.TEMPLATE_LOCATIONS, (list, tuple)):
			settings.TEMPLATE_LOCATIONS.append(os.path.join(DEPLOY_ROOT, 'templates'))

		# App the plugin path to the system
		if settings.PLUGIN_LOCATION is not None:
			if not os.path.existd(PLUGIN_LOCATION):
				raise RuntimeError('Plugin Location: "%s" does not exist' % PLUGIN_LOCATION)

			sys.path.insert(0, settings.PLUGIN_LOCATION)
		else:
			sys.path.insert(0, os.path.join(settings.SITE_ROOT, 'plugins'))

		# Import plugins
		__plugins__.install(settings.PLUGINS)

	def __call__(self, environ, start_response):

		# Create the request/response object
		request = Request(environ)
		response = None

		# Set resolved URL to false, we use this to determine if
		# a URL has been matched to a view and subsequently raised
		# a 404. This can be used to stop redirection of 404's in
		# applications further up the stack - such as the CMS app.
		environ['com.deploycms.url_resolved'] = False

		result = __resolver__.resolve(environ)

		if not result:
			raise PageNotFound(environ['PATH_INFO'])

		# Tell the environ that it matched a URL
		environ['com.deploycms.url_resolved'] = True

		endpoint, args, kwargs = result

		action = self._load_endpoint(endpoint)

		# An responder/view action was found, run it and fetch the response
		if kwargs:
			response = action(request, **kwargs)
		elif args:
			response = action(request, *args)
		else:
			response = action(request)

		# All done, return the response we have generated
		return response(environ, start_response)

	def _load_endpoint(self, path):
		mod_act = path.split('.')
		import_str = '.'.join(mod_act[:-1])
		action_str = mod_act[-1]

		# Check to see if the action has already been imported
		if action_str in globals():
			return globals()[action_str]

		else:
			# @@ switch to import_module
			module = __import__(import_str, globals(), locals(), [action_str], -1)
			return getattr(module, action_str)

class DeployStaticFileApp(object):

	def __init__(self, root):
		"""Initialise the static file handler by storing the file path root
		in which it operates"""

		self.root = root

	def __call__(self, environ, start_response):
		"""Return the file for a given request"""

		def static_file_writer(status, headers, output):
			"""Utility function to return the response output"""
			start_response(status, headers.items())
			return output

		# Setup some output vars
		headers = {
			'Server': settings.SERVER_AGENT,
			'Date': http_date(time.time()),
			'Content-Type': 'text/plain', # Default content type
		}

		# Check HTTP Verbs.....
		request_method = environ['REQUEST_METHOD']

		if not request_method in ('GET', 'HEAD'):
			# We only support GET & HEAD requests on static files
			raise HTTPMethodNotAllowed(('GET', 'HEAD'))

		# Get the path of the file we are looking for, this
		# is the path which is relative to the root
		if 'com.deploycms.routing.path_info' in environ:
			# We check if the path has been modified here, certain deploy
			# middleware may provide an altered path which we use if avaiable
			file_path = environ['com.deploycms.routing.path_info']

			# @@ FIXME: A workaround for favicon problem...
			if file_path == '':
				file_path = environ['PATH_INFO']

		else:
			file_path = environ['PATH_INFO']

		# Strip leading slashes...
		if file_path.startswith('/'):
			file_path = file_path[1:]

		# Create the full filepath
		full_file_path = os.path.join(self.root, file_path)

		# The file can not be found on the file system, return a 404
		# response
		if not os.path.exists(full_file_path):
			raise PageNotFound(os.path.join('/', file_path))

		# The file exists, attempt to read it into memory
		try:
			fp = open(full_file_path, 'rb')
		except IOError:
			# We can't read in the file for whatever reason,
			# we just present an access denied message in the
			# response...
			raise HTTPForbidden()

		# We have the file, get the last time it was modified
		epoch_modified_time = os.stat(full_file_path)[stat.ST_MTIME]
		http_modified_time = http_date(epoch_modified_time)
		headers['Last-Modified'] = http_modified_time

		# Check if the file has been modified since it was last requested
		if environ.get('HTTP_IF_MODIFIED_SINCE', None) == http_modified_time:
			status = '304 Not Modified'
			headers['Content-Length'] = str(0)
			output = []

			return static_file_writer(status, headers, output)

		# We have passed all the checks, return the content
		content = fp.read()
		fp.close()

		status = '200 OK'
		mime_type = mimetypes.guess_type(file_path)[0]

		# Setup the HTTP headers
		headers['Content-Length'] = str(len(content))

		# This is the default as we don't know about expires yet
		headers['Cache-Control'] = 'must-revalidate'

		if mime_type:
			# Add the specific mimetype if we have one, otherwise
			# the default text/plain is used

			# @@ TODO: Static file: should this use the application/octet+stream as default here?
			headers['Content-Type'] = mime_type

			# Attempt to set proper cache control headers. First we see if we have a
			# specification for how long a particular media type should be allowed
			# to live for, if we don't have one we don't set any cache controls
			if mime_type in settings.MEDIA_EXPIRES:
				# Expires
				# This works by specifiying a specific date in which a resource will become
				# invalid.
				modified_datetime = datetime.datetime.fromtimestamp(epoch_modified_time)
				expires = modified_datetime + settings.MEDIA_EXPIRES[mime_type]
				epoch_expires = time.mktime(expires.timetuple())

				headers['Exipres'] = http_date(epoch_expires)

				# Cache-Control

				# @@ TODO: Static File Cache control needs to be looked at (ns)

				# This works by specifiying an amount of time the resources is valid for since the
				# last request was made
				#now = datetime.datetime.now()
				#revalidate = now + settings.MEDIA_EXPIRES[mime_type]
				#cache_control = time.mktime(revalidate.timetuple())

				#headers['Cache-Control'] = 'max-age=%s, must-revalidate' % str(cache_control)

		if request_method == 'GET':
			# Only in GET request do we return the content...
			output = [content,]
		else:
			output = []

		# Return our file
		return static_file_writer(status, headers, output)
