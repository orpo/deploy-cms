"""WSGI middleware for logging Deploy Applications"""

import logging
import logging.handlers
import time
import urllib

class ColorConsoleLogFormatter(logging.Formatter):
	BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)
	RESET_SEQ = "\033[0m"
	COLOR_SEQ = "\033[1;%dm"
	BOLD_SEQ = "\033[1m"

	COLORS = {
		'WARNING': (30 + YELLOW),
		'INFO': (30 + GREEN),
		'DEBUG': (30 + GREEN),
		'CRITICAL': (30 + RED),
		'ERROR': (30 + RED)
	}

	def format(self, record):
		# Get the levelname
		levelname = record.levelname

		# Get the formatted result
		result = logging.Formatter.format(self, record)

		return '%s%s%s' % (self.COLOR_SEQ % self.COLORS[levelname], result, self.RESET_SEQ)

__all__ = ('DeployLoggerApp',)

# Note: The logging application should come before the DeployExceptionApp, otherwise
# logs may appear incorrecly as the DeployExceptionApp buffers output.

class DeployLoggerApp(object):
	"""WSGI application to log WSGI requests to a log file based on Apaches 'combined' format.
	WSGILogger can also be used to print to log output to console/stout"""

	# Apache combined log format
	format = ('%(REMOTE_ADDR)s - %(REMOTE_USER)s [%(time)s] '
			  '"%(REQUEST_METHOD)s %(REQUEST_URI)s %(HTTP_VERSION)s" '
			  '%(status)s %(bytes)s "%(HTTP_REFERER)s" "%(HTTP_USER_AGENT)s"')

	def __init__(self, application, logger=None, log='', format=None, logging_level=logging.INFO, console_log=False,
				logger_name='deploywsgi', set_level=logging.DEBUG):

		self.application = application
		self.log = log
		self.use_console = console_log

		# Log style format
		if format is not None:
			self.format = format

		self.logging_level = logging_level

		if logger:
			self.logger = logger
		else:
			self.logger = logging.getLogger(logger_name)

		# Add the handlers to rotate the log files...
		log_handler = logging.handlers.RotatingFileHandler(self.log, maxBytes=5242880, backupCount=5) # 5mb
		log_handler.setLevel(logging.INFO)
		log_handler.setFormatter(logging.Formatter('%(message)s'))
		self.logger.addHandler(log_handler)

		if self.use_console:
			# If console logging is enabled all requests will print to stout
			console_handler = logging.StreamHandler()
			console_handler.setLevel(logging.DEBUG)
			console_handler.setFormatter(ColorConsoleLogFormatter('%(message)s'))

			self.logger.addHandler(console_handler)
			self.logger.propagate = False

		if set_level:
			self.logger.setLevel(set_level)

	def __call__(self, environ, start_response):
		runtime = time.localtime()
		uri = urllib.quote(environ.get('SCRIPT_NAME', '') + environ.get('PATH_INFO', ''))

		if environ.get('QUERY_STRING'):
			uri = '%s?%s' % (uri, environ['QUERY_STRING'])

		method = environ['REQUEST_METHOD']

		def log_response(status, headers, exc_info=None):
			bytes = None

			for name, value in headers:
				if name.lower() == 'content-length':
					bytes = value

			self.write(environ, method, uri, runtime, status, bytes)

			return start_response(status, headers, exc_info=exc_info)

		return self.application(environ, log_response)

	def write(self, environ, method, uri, start, status, bytes):

		# @@ TODO: The logging app needs to be careful about what it writes
		# as there is no exception catching above here.

		if bytes is None:
			bytes = '-'

		if time.daylight:
			offset = time.altzone / 60 / 60 * -100
		else:
			offset = time.timezone / 60 / 60 * -100

		if offset >= 0:
			offset = "+%0.4d" % (offset)
		elif offset < 0:
			offset = "%0.4d" % (offset)

		status_code = status.split(None, 1)[0]

		d = {
			'REMOTE_ADDR': environ.get('REMOTE_ADDR') or '-',
			'REMOTE_USER': environ.get('REMOTE_USER') or '-',
			'REQUEST_METHOD': method,
			'REQUEST_URI': uri,
			'HTTP_VERSION': environ.get('SERVER_PROTOCOL'),
			'HTTP_REFERER': environ.get('HTTP_REFERER', '-'),
			'HTTP_USER_AGENT': environ.get('HTTP_USER_AGENT', '-'),
			'time': time.strftime('%d/%b/%Y:%H:%M:%S ', start) + offset,
			'status': status_code,
			'bytes': bytes,
		}

		if status_code in ['400', '401','500','502','503','504','509']:
			self.logger.error(self.format % d)

		elif status_code in ['404', '403']:
			self.logger.warning(self.format % d)

		else:
			self.logger.log(self.logging_level, self.format % d)
