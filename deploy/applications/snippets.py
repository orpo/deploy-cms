"""Deploy WSGI Middleware Application: Snippets"""

from deploy.config import settings
from deploy.core.http import Request, Response
from deploy.core.templates.snippets import parse_snippets
from deploy.utils.importing import import_module

# Parse response and process any snippets

__all__ = ('DeploySnippets',)

class DeploySnippets(object):
	"""Deploy Snippet application, creates dynamic hooks from static response objects"""

	def __init__(self, application, enabled=True, process_statuses=[200,]):
		self.application = application
		self.enabled = enabled
		self.process_statuses = process_statuses

		# Init the snippets
		if self.enabled:
			try:
				base_snippet_module = import_module('%s.snippets' % settings.APPLICATION_NAME)
			except ImportError:
				raise RuntimeError(
					'Snippets is enabled, but snippets.py does not exist in "%s"!' % settings.APPLICATION_NAME
				)

	def __call__(self, environ, start_response):
		# Are snippets turned on?
		if not self.enabled:
			return self.application(environ, start_response)

		# Get our request and response objects
		request = Request(environ)
		response = request.get_response(self.application) # This runs the pipeline up to this point

		# Run the snippet processing if it is safe to do so
		if response.content_type == 'text/html' and (response.status_int in self.process_statuses):
			altered_html = parse_snippets(response.body, request=request, debug=settings.DEBUG)

			if altered_html:
				# Alter the reponse object
				response.content_length = len(altered_html)
				response.body = altered_html

		return response(environ, start_response)
