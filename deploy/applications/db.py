"""Deploy WSGI Middleware Application: Database"""

import sys
from threading import local

from storm.database import create_database
from storm.store import Store
from storm.tracer import debug as storm_debug

__all__ = ('DeployDatabaseApp',)

class DeployDatabaseApp(object):

	def __init__(self, app, database_uri, debug=False):
		self.app = app
		self.key = 'com.deploycms.db.conn'
		self.database = create_database(database_uri)
		self.local = local()
		self.database_uri = database_uri

		if debug:
			storm_debug(True, stream=sys.stdout)

	def __call__(self, environ, start_response):
		# Add the store to the environment
		try:
			environ[self.key] = self.local.store
			# Make sure the connection is clean
			environ[self.key].flush()
			environ[self.key].rollback()
		except AttributeError:
			environ[self.key] = self.local.__dict__.setdefault('store', Store(self.database))

			# Special case for mysql and storm
			# This is to allow for MySQL connections to be automatically restarted
			if self.database_uri.startswith('mysql'):
				environ[self.key]._connection._raw_connection.ping(True)

		try:
			# Run the application
			return self.app(environ, start_response)
		finally:
			# Clean up our connection for the next transactions
			environ[self.key].rollback()