"""Shortcut functions for common Deploy idioms"""

import urlparse
import json

from deploy.config import settings
from deploy.core.http import Response
from deploy.core.mail import Email
from deploy.core.templates import TemplateManager
from deploy.utils.encoding import ExtendedJSONEncoder

__all__ = ('render_page', 'render_json_page', 'redirect_to', 'mail_admins', 'mail_template')

def _set_cookies(response, cookies):
	if cookies:
		domain = urlparse.urlparse(settings.SITE_URL).netloc

		for cookie in cookies:
			if cookie['value'] is None:
				response.delete_cookie(cookie['key'])
			else:
				response.set_cookie(
					cookie['key'],
					cookie['value'],
					domain=cookie['domain'] if cookie.has_key('domain') else domain,
					max_age=cookie['max_age'] if cookie.has_key('max_age') else None,
					expires=cookie['expires'] if cookie.has_key('expires') else None,
					path=cookie['path'] if cookie.has_key('path') else '/',
					secure=cookie['secure'] if cookie.has_key('secure') else False
				)

	return response

def render_page(request, template, template_args=None, template_engine=None, status=200,
				content_type='text/html', resources=None, cookies=None):
	"""Shortcut to render a template for a view"""

	# Sanity checks....
	assert template is not None, 'You must specifiy a template to render_page!'

	if template_args is None:
		template_args = {}

	if resources:
		template_args['resources'] = resources

	# Setup the template manager
	template_manager = TemplateManager(request, engine=template_engine)

	source = template_manager.render(template, data=template_args)

	# Setup the response
	response = Response(content_type=content_type, request=request)
	response.status = status
	response.body = str(source)

	# Are we setting any cookies?
	response = _set_cookies(response, cookies)

	return response

def render_json_page(request, args, status=200, content_type='application/json',
					cookies=None, encoder=ExtendedJSONEncoder):
	"""Shortcut to render a template for a view"""

	# Sanity checks....
	assert args is not None, 'You must specifiy a arguments to render_json_page!'

	# Setup the response
	response = Response(content_type=content_type, request=request)
	response.status = status
	response.body = json.dumps(args, cls=encoder)

	# Are we setting any cookies?
	response = _set_cookies(response, cookies)

	return response

def redirect_to(url, status=302, cookies=None, ssl=False):
	"""Shortcut to redirect a request to another URL"""

	# Sanity check....
	assert (status in (301, 302, 303, 307)), 'You must specify a valid redirect status!'

	if url.startswith('/'):
		# Add in the full site url
		if ssl:
			url = urlparse.urljoin('https://%s' % settings.SITE_URL, url)
		else:
			url = urlparse.urljoin('http://%s' % settings.SITE_URL, url)

	response = Response()
	response.status = status
	response.location = url
	response.body = '%s Redirect' % status

	# Are we setting any cookies?
	response = _set_cookies(response, cookies)

	return response

def mail_admins(subject, content, fail_silently=False):
	"""Utility function to quickly email site administrators"""

	if not settings.SITE_ADMINISTRATORS:
		return False

	try:
		email = Email(settings.SITE_EMAIL, subject, content)
		email.send(
			[email for name, email in settings.SITE_ADMINISTRATORS],
			host=settings.EMAIL_HOST,
			username=settings.EMAIL_USERNAME,
			password=settings.EMAIL_PASSWORD
		)
	except Exception, e:
		if not fail_silently:
			raise e

	return True

def mail_template(to, subject, template, template_args=None, template_manager_args=None, silent=False):
	"""Send an email template"""

	template_args = template_args if template_args is not None else {}
	template_manager_args = template_manager_args if template_manager_args is not None else {}

	template_manager = TemplateManager(**template_manager_args)
	msg = template_manager.render(template, data=template_args)

	try:
		email = Email(settings.SITE_EMAIL, subject, msg)
		email.send(
			to,
			host=settings.EMAIL_HOST,
			username=settings.EMAIL_USERNAME,
			password=settings.EMAIL_PASSWORD
		)
	except Exception, e:
		if not silent:
			raise e
