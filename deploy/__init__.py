"""Deploy Web Framework"""

__version__ = '0.3'
__author__ = 'Anthony Blackshaw & Nick Snell'
__build__ = '0.3-beta'

# This seems to be absent sometimes from Python installs
import mimetypes
mimetypes.add_type('application/json', '.json')

SNAILWHALE = """

          ........   ....     ....
        ............  ....   ....
       ....      ....   .......
       ....      ....    ..^..
        .....................
           ................

"""
