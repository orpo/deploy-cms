"""Tools for working with users"""

import os
import string
import hashlib
import datetime
from random import choice, randint, seed

from storm.locals import *

from deploy.config import settings
from deploy.core.models import Model

__all__ = ('UserModel', 'encrypt_password', 'generate_password')

class UserModel(Model):

	__storm_table__ = 'deploy_users'

	id = Int(primary=True)
	active = Bool(default=True)
	created = DateTime()
	last_modified = DateTime()
	login_last_attempt = DateTime()
	login_attempt_count = Int(default=0)
	last_reset_attempt = DateTime()
	first_name = Unicode()
	last_name = Unicode()
	email = Unicode()
	password = Unicode()

	def authenticate(self, password):
		return encrypt_password(password) == self.password

def encrypt_password(password):
	"""Encrypt a password field. If no password is sent, an empty string is returned"""
	if not password:
		return u''

	passgen = hashlib.md5()
	passgen.update(settings.PASSWORD_SALT)
	passgen.update(password)
	return unicode(passgen.hexdigest())

def generate_password(choices=string.letters + string.digits, size=randint(6, 8)):
	"""Generate a random password"""
	seed(os.urandom(64))
	return ''.join([choice(choices) for i in range(size)]).upper()
