"""Tools for working with Client Sessions"""

import os
import hashlib
import time
import datetime

from storm.locals import *

from deploy.config import settings
from deploy.core.models import Model

__all__ = ('SessionModel', 'SessionStore', 'generate_id')

class SessionModel(Model):

	__storm_table__ = 'deploy_session'

	id = Int(primary=True)
	created = DateTime()
	last_modified = DateTime()
	session_id = Unicode()
	data = Pickle(default={})

	def __init__(self, *args, **kwargs):
		self.created = self.last_modified = datetime.datetime.now()
		self.session_id = generate_id()
		self.data = SessionStore()

		super(SessionModel, self).__init__(*args, **kwargs)

class SessionStore(dict):
	pass

def generate_id():
	"""Generate a random ID suitable for use as a session ID"""
	keygen = hashlib.md5()
	keygen.update(os.urandom(64))
	keygen.update(time.ctime())
	return unicode(keygen.hexdigest())