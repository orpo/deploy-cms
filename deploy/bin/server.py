#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The Deploy Web Server"""

import os
import os.path
import time
import logging
import signal
import sys
import urlparse
from optparse import OptionParser

from deploy import SNAILWHALE
from deploy.config import settings
from deploy.config.utils import setup_environ, get_application_pipeline
from deploy.server import wsgiserver
from deploy.utils._os import switch_uid_gid, get_uid_gid

VALID_COMMANDS = 'START', 'STOP', 'RESTART', 'STATUS'

DEFAULT_CONFIG = {
	'APPLICATION_PATHS': 			[],
	'APPLICATION_SETUP_ARGS': 		{},
	'LOG': 							None,
	'QUIET': 						False,
	'SERVER_NAME': 					'DeployServer',
	'SERVER_RUN_DIR': 				os.getcwd(),
	'SERVER_IP': 					'127.0.0.1',
	'SERVER_PORT': 					8888,
	'SERVER_PID_FILE': 				'deployserver.pid',
	'SERVER_USER': 					'nobody',
	'SERVER_GROUP': 				'nogroup',
	'SERVER_REQUEST_QUEUE_SIZE': 	5,
	'SERVER_TIMEOUT':  				60,
	'SERVER_SHUTDOWN_TIMEOUT':		5,
	'SERVER_THREADS': 				20,
	'SERVER_MAX': 					-1,
	'SERVER_DAEMONIZE': 			True,
	'SERVER_UMASK':					0002,
	'SSL_CERTIFICATE': 				None,
	'SSL_PRIVATE_KEY': 				None,
	'SNAIL':						False,
	'WHALE':						False
}

__all__ = ('DeployServer', 'VALID_COMMANDS', 'DEFAULT_CONFIG')

class DeployServer(object):

	def __init__(self, conf):
		"""Setup the Server object with a given configuration"""

		server_conf = DEFAULT_CONFIG
		server_conf.update(conf)

		# @@ TODO: Sanity Check the config

		self.conf = server_conf
		self.log = conf['LOG']

		self._server = None

	def run(self, command):
		"""Execute the command on the server"""

		command = command.upper()

		assert command in VALID_COMMANDS, 'You must provide a valid command!'

		if command == 'START':
			self.start()
		elif command == 'STOP':
			self.stop()
		elif command == 'RESTART':
			self.restart()
		elif command == 'STATUS':
			self.status()
		else:
			raise RuntimeError('Unknown command!')

	def start(self):
		"""Start the Deploy Web Server"""

		# Check the server isn't already running
		if self._is_running():
			self.log.critical('Unable to start server! Is Server Running? (PID file exists)')
			sys.exit(1)

		# Setup our application paths
		sys.path.insert(0, self.conf['SERVER_RUN_DIR'])

		if 'APPLICATION_PATHS' in self.conf and self.conf['APPLICATION_PATHS']:
			for path in self.conf['APPLICATION_PATHS']:
				sys.path.insert(0, path)

		pid = None

		if self.conf['SERVER_DAEMONIZE']:
			# Daemon server

			# Start to detach the server process
			try:
				pid = os.fork()

				if pid > 0:
					# Exit first parent
					sys.exit(0)

			except OSError, e:
				self.log.critical('Unable to start server - Can not fork parent process: %s' % e)
				sys.exit(1)

			# Setup our Daemon environment
			os.chdir(self.conf['SERVER_RUN_DIR'])
			os.setsid()

			# The Second fork....
			try:
				pid = os.fork()

				if pid > 0:
					# Exit from second parent, save the PID
					open(self.conf['SERVER_PID_FILE'], 'w').write('%d' % pid)

					# Set the PID file and change permissions
					uid, gid = get_uid_gid(self.conf['SERVER_USER'], self.conf['SERVER_GROUP'])

					# Set the permissions on the PID file - this is important as if we don't
					# we can't stop the server using the same user
					if os.path.exists(self.conf['SERVER_PID_FILE']):
						os.chown(self.conf['SERVER_PID_FILE'], uid, gid)

					sys.exit(0)

			except OSError, e:
				self.log.critical('Unable to start server - Can not double fork: %s' % e)
				sys.exit(1)

			# Read back our PID
			attempts = 0

			while attempts < 4:
				try:
					f = open(self.conf['SERVER_PID_FILE'], 'r')
					pid = f.read()
					f.close()
				except IOError:
					# Need to wait as the child may not of finished writing to the
					# PID file...
					attempts += 1
					time.sleep(1)
				else:
					break

			# Redirect our standard file descriptors
			sys.stdout.flush()
			sys.stderr.flush()

			std_in = file('/dev/null', 'r')
			std_out = file(self.conf['LOG_FILE'], 'a+')
			std_err = file(self.conf['LOG_FILE'], 'a+', 0)

			os.dup2(std_in.fileno(), sys.stdin.fileno())
			os.dup2(std_out.fileno(), sys.stdout.fileno())
			os.dup2(std_err.fileno(), sys.stderr.fileno())

			# Switch the user and group
			switch_uid_gid(self.conf['SERVER_USER'], self.conf['SERVER_GROUP'])

		else:
			# Non-Daemon Server
			pid = os.getpid()
			std_err = sys.stderr

		# Set the umask
		os.umask(self.conf['SERVER_UMASK'])

		application_setup = self.conf['APPLICATION_SETUP_ARGS']
		application_setup['VERBOSE'] = not self.conf['QUIET']

		# Configure the WSGI Application
		deploy_app = get_application_pipeline(application_setup)

		# Setup the Server
		self._server = wsgiserver.CherryPyWSGIServer(
			(self.conf['SERVER_IP'], self.conf['SERVER_PORT']),
			deploy_app,
			server_name=self.conf['SERVER_NAME'],
			numthreads=self.conf['SERVER_THREADS'],
			max=self.conf['SERVER_MAX'],
			request_queue_size=self.conf['SERVER_REQUEST_QUEUE_SIZE'],
			timeout=self.conf['SERVER_TIMEOUT'],
			shutdown_timeout=self.conf['SERVER_SHUTDOWN_TIMEOUT']
		)

		# Setup SSL if it has been requested....
		if self.conf['SSL_CERTIFICATE'] and self.conf['SSL_PRIVATE_KEY']:
			self.server.ssl_certificate = self.conf['SSL_CERTIFICATE']
			self.server.ssl_private_key = self.conf['SSL_PRIVATE_KEY']

		# Init server callback signals
		signal.signal(signal.SIGUSR1, self._signal_handler)
		signal.signal(signal.SIGHUP, self._signal_handler)
		signal.signal(signal.SIGTERM, self._signal_handler)

		# Start the server...
		try:
			self.log.info('Server is running. %s:%s / PID %s' % (self.conf['SERVER_IP'], self.conf['SERVER_PORT'], pid))

			if not self.conf['QUIET']:
				if self.conf['SNAIL'] or self.conf['WHALE']:
					print SNAILWHALE
					print

				print 'Deploy Server is running. %s:%s / PID %s' % (self.conf['SERVER_IP'], self.conf['SERVER_PORT'], pid)
				print

			self._server.start()

		except KeyboardInterrupt:
			if not self.conf['QUIET']:
				print 'Server received keyboard interrupt - shutting down.... ',

			self.log.info('Server received keyboard interrupt - shutting down')

			self._server.stop()
			self._clean()

			if not self.conf['QUIET']:
				print 'Done.'

		except Exception, e:
			self.log.error('Server Failed %s' % e)

			if not self.conf['QUIET']:
				print 'Server Failed %s' % e

			self._clean()

	def stop(self):
		"""Stop the Deploy Web Server - This is only used in daemon mode"""

		# See if there is a valid process ID
		if not os.path.exists(self.conf['SERVER_PID_FILE']):
			# Server is already stopped
			self.log.critical('Trying to stop a server that does not exist! (PID file %s)' % self.conf['SERVER_PID_FILE'])

			if not self.conf['QUIET']:
				print 'Trying to stop a server that does not exist!'

			sys.exit(1)

		try:
			pid = open(self.conf['SERVER_PID_FILE'], 'r').read()
		except OSError:
			self.log.error('Unable to open PID file: %s' % self.conf['SERVER_PID_FILE'])

		try:
			pid = int(pid)
		except ValueError, e:
			self.log.critical('PID file does not contain a valid Process ID!')
			sys.exit(1)

		try:
			os.kill(pid, signal.SIGTERM)
		except OSError, err:
			self.log.critical('Unable to kill process')
			sys.exit(1)

		if not self.conf['QUIET']:
			self.log.debug('Server Server has stopped.')

	def restart(self):
		"""Restart the Deploy Web Server"""

		self.stop()
		self.start()

	def status(self):
		"""Returns status of the Deploy Web Server"""

		# @@ TODO: Status functionality on Web Server

		raise NotImplementedError('Not Implemented Yet :(')

	def _is_running(self):
		"""Utility to tell is a daemonized server is running"""

		if self.conf['SERVER_DAEMONIZE']:

			if os.path.exists(self.conf['SERVER_PID_FILE']):

				# @@ TODO: Should we need to actually check the active processes

				return True

		return False

	def _clean(self):
		"""Attempt to clean up the runtime environment"""

		if self.conf['SERVER_DAEMONIZE']:
			if os.path.exists(self.conf['SERVER_PID_FILE']):
				try:
					os.remove(self.conf['SERVER_PID_FILE'])
				except IOError:
					self.log.error('Unable to remove PID file at: %s' % self.conf['SERVER_PID_FILE'])

	def _signal_handler(self, sig, stack):
		"""Handle OS signals sent to the server"""

		if sig == signal.SIGUSR1:
			pass

		elif sig == signal.SIGHUP:
			self.log.info('Server recieved SIGHUP - restarting....')

			self._server.stop()
			self._clean()

			self.start()

		elif sig == signal.SIGTERM:
			self.log.info('Server recieved SIGTERM - shutting down....')

			self._server.stop()
			self._clean()

			sys.exit(0)

		else:
			log.warning('Server recieved unknown signal "%s" - ignoring.' % sig)

		# @@ TODO Any of these other signals needed for the server?
		# TERM, INT
		# QUIT
		# HUP
		# USR1
		# USR2
		# WINCH

def main():
	"""Server Command Line Startup"""

	# Sanity Check....
	if sys.version_info[:3] < (2, 6, 0):
		raise RuntimeError('Deploy requires Python 2.6 or later!')

	snail = False
	if '--snail' in sys.argv:
		sys.argv.pop(sys.argv.index('--snail'))
		snail = True

	whale = False
	if '--whale' in sys.argv:
		sys.argv.pop(sys.argv.index('--whale'))
		whale = True

	# Setup our command line options
	parser = OptionParser()
	parser.add_option('-d', '--no-daemon', action='store_false',
		dest='daemonize', default=True, help='Do not Daemonize the webserver')
	parser.add_option('-q', '--quiet', dest='quiet', action='store_true',
		default=False, help='Stop console output')
	parser.add_option('-s', '--settings', dest='settings', help='Custom settings file', default=None)
	parser.add_option('-r', '--root', dest='root', help='Application root', default=None)

	options, args = parser.parse_args()

	if not len(args) > 0:
		parser.error('incorrect number of arguments! Command Expected!')

	command = args[0]

	# Check the command is valid
	if not command.upper() in VALID_COMMANDS:
		parser.error('invalid command!')

	# Do we need to overide default settings?
	if options.settings is not None:
		os.environ['DEPLOY_SETTINGS'] = options.settings

	# Setup the deploy environment...
	root = None

	if options.root is not None:
		root = options.root
	elif 'VIRTUAL_ENV' in os.environ:
		root = os.path.abspath(os.environ['VIRTUAL_ENV'])
	else:
		raise NotImplementedError('Looks like your running from a non virtual env - havent got this working yet, sorry :(')

	# Setup the environment and get our application name
	application_name = setup_environ(root)

	# Load our local settings
	settings.load(os.environ['DEPLOY_SETTINGS'], force=True)
	settings.load({'APPLICATION_NAME': application_name}, force=True)

	# Setup the log...
	if not os.path.exists(os.path.dirname(settings.DEBUG_LOG)):
		os.makedirs(os.path.dirname(settings.DEBUG_LOG))

	logging.basicConfig(
		level=logging.DEBUG,
		format=settings.DEBUG_LOG_FORMAT,
		filename=settings.DEBUG_LOG,
		filemode='a+'
	)

	log = logging.getLogger('deployapp')

	# Console logging - This is only used up until the point it
	# daemonizes itself. At that point stderr is redirected to
	# the log file
	console = logging.StreamHandler()
	console.setLevel(logging.WARNING)
	console.setFormatter(logging.Formatter('Deploy Server: %(levelname)-8s %(message)s'))

	log.addHandler(console)

	# Get the domain for the site
	site_url_bits = urlparse.urlparse(settings.SITE_URL)
	domain = site_url_bits.netloc

	# Setup the server config
	conf = {
		'APPLICATION_PATHS': 			settings.APPLICATION_PATHS,
		'APPLICATION_SETUP_ARGS':		settings.APPLICATION_SETUP_ARGS,
		'SERVER_NAME': 					'%s %s' % (settings.SERVER_AGENT, domain),
		'SERVER_RUN_DIR': 				settings.SERVER_RUN_DIR,
		'SERVER_IP': 					settings.SERVER_IP,
		'SERVER_PORT': 					settings.SERVER_PORT,
		'SERVER_PID_FILE': 				settings.SERVER_PID_FILE,
		'SERVER_USER': 					settings.SERVER_USER,
		'SERVER_GROUP': 				settings.SERVER_GROUP,
		'SERVER_REQUEST_QUEUE_SIZE': 	settings.SERVER_REQUEST_QUEUE_SIZE,
		'SERVER_TIMEOUT': 				settings.SERVER_TIMEOUT,
		'SERVER_SHUTDOWN_TIMEOUT':		settings.SERVER_SHUTDOWN_TIMEOUT,
		'SERVER_THREADS': 				settings.SERVER_THREADS,
		'SERVER_UMASK':					settings.SERVER_UMASK,
		'SERVER_DAEMONIZE':				options.daemonize,
		'SSL_CERTIFICATE': 				settings.SERVER_SSL_CERTIFICATE,
		'SSL_PRIVATE_KEY': 				settings.SERVER_SSL_PRIVATE_KEY,

		'LOG': 							log,
		'LOG_FILE':						settings.DEBUG_LOG,
		'QUIET': 						options.quiet,
		'SNAIL':						snail,
		'WHALE':						whale
	}

	# Start the server
	server = DeployServer(conf)
	server.run(command)

if __name__ == '__main__':
	# Run the server....
	main()
