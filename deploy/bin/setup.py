#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Setup utility for Deploy CMS Sites"""

import os
import os.path
import string
import sys
from random import choice, randint, seed

from deploy import SNAILWHALE
from deploy.config.utils import get_application_name

# Some assumptions:
#
# - Deploy is already installed in a virtualenv
# - No site specific data currently exists

BASE_STRUCTURE = (
	# Folder name, children, placeholder files
	(
		'etc', None, ('pip.conf',)
	),
	(
		'lib', None, ('__init__.py',)
	),
	(
		'media', (
			('css', None, ('base.css')),
			('js', None, None),
			('img', None, None),
		), None
	),
	(
		'plugins', None, None
	),
	(
		'settings', None, ('__init__.py', 'development.py', 'staging.py', 'production.py')
	),
	(
		'static', None, ('50x.html')
	),
	(
		'templates', None, None
	),
)

CONFIGURATION_OPTIONS = (
	# Question, Validation
	('')

)

BASE_SETTINGS_FILE = """
import sys
import os.path

PROJECT_ROOT = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

sys.path.insert(0, PROJECT_ROOT)
sys.path.insert(1, os.path.join(PROJECT_ROOT, 'lib'))

"""

def generate_random_salt(choices=string.letters + string.digits + '{}/.@$%\\^*()"<>!', size=randint(20, 25)):
	"""Generate a random salt for hashing passwords etc..."""
	seed(os.urandom(64))
	return ''.join([choice(choices) for i in range(size)]).lower()

def main():

	# Sanity Check....
	if sys.version_info[:3] < (2, 6, 0):
		raise RuntimeError('Deploy requires Python 2.6 or later!')

	if not os.environ['VIRTUAL_ENV']:
		# @@ TODO Work out where we are when not in a virtual env
		raise NotImplementedError(
			'Looks like your running from a non virtual env - '
			'havent got this working yet, sorry :('
		)
	else:
		root = os.path.abspath(os.environ['VIRTUAL_ENV'])

	# Welcome user...
	print SNAILWHALE
	print
	print 'Welcome to the Deploy CMS setup utility...'
	print

	# Attempt to lookup an application - if we get something back here it
	# would indicate the application has already been setup
	application_name = get_application_name(root)

	# Check site isn't already setup
	if application_name is not None:
		raise Exception('Application looks to already be setup - "%s" was found.' % application_name)

	# Ask the user for the application name
	while application_name is None:
		application_name = raw_input('Application name: ')

		# Check application name is safe
		if ('.', '\\', '/', '?', '>', '<', '$', '*', '%') in application_name:
			application_name = None

	## SETUP
	application_root = os.path.join(root, application_name)

	# Write out base site root
	os.mkdir(application_root)

	# Recursivly write out the default directories and placeholder files
	def build_structure(folder, children, files):
		# Make the folder
		os.mkdir(folder)

		# @@ Add in placeholder files - touch??

		# Build the children
		for f, c, fi in children:
			build_structure(os.path.join(folder, f), c ,fi)

	# Build the base site
	for f, c , fi in BASE_STRUCTURE:
		build_structure(os.path.join(application_root, f), c ,fi)

	## CONFIG

	# Generate defaults
	configuration_options = {
		# Hard set
		'DEBUG': True,
		'LOCALE': 'en',
		'SITE_ROOT': root,
		'APPLICATION': '%s.application' % application_name,
		'PASSWORD_SALT': generate_random_salt(),
		'TEMPLATE_LOCATIONS': "[\nos.path.join(SITE_ROOT, '%s/templates/'),\n]" % application_name,
		'TEMPLATE_ENGINES': "('jinja',)",
		'PLUGINS': "[\n'deploy.cms',\n]",
		'SITE_EDIT_URL': None, 			# Default: SITE_URL with www prefix switched (or added) to edit
		'APPLICATION_NAMESPACE': None, 	# Default: the reverse of SITE_URL

		# Dynamically populated
		'SITE_NAME': None,
		'SITE_URL': None,
		'SITE_EDIT_URL': None,
		'DATABASE_URI': None,
	}

	# Get the site name
	while configuration_options['SITE_NAME'] is None:
		configuration_options['SITE_NAME'] = raw_input('Enter Site name: ')

	# Get the site URL
	while configuration_options['SITE_URL'] is None:
		configuration_options['SITE_URL'] = raw_input('Enter Site URL: ')

		# Create the edit URL dynamically
		configuration_options['SITE_EDIT_URL'] = configuration_options['SITE_URL']

	while configuration_options['DATABASE_URI'] is None:
		configuration_options['DATABASE_URI'] = raw_input('Enter database URI: ')

	# @@ Sanity check values - can connect to database etc...

	# Write out settings file
	f = open(os.path.join(application_root, 'settings/__init__.py'), 'wb')
	f.write(BASE_SETTINGS_FILE)
	f.write('\n'.join(['%s = %s' % (k, v) for k, v in configuration_options.items()]))
	f.close()

	## FINISH UP

	# @@ Remove the hooks for the setup tool

if __name__ == '__main__':
	# Run the setup facility....
	main()
