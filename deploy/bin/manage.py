#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The Deploy Management tool"""

import os
import os.path
import sys
from optparse import OptionParser

from deploy.config import settings
from deploy.config.utils import setup_environ, bootstrap

class ManageController(object):
	"""Utility for controlling manage commands"""

	def __init__(self):
		# Container for controllers
		self._controllers = []

	def load(self):
		""""Load the manage controllers"""
		pass


def main():
	"""Run a manage command"""

	# Sanity Check....
	if sys.version_info[:3] < (2, 6, 0):
		raise RuntimeError('Deploy requires Python 2.6 or later!')

	# Setup the deploy environment...
	root = None

	if not os.environ['VIRTUAL_ENV']:
		# @@ TODO Work out where we are when not in a virtual env
		raise NotImplementedError(
			'Looks like your running from a non virtual env - '
			'havent got this working yet, sorry :('
		)
	else:
		root = os.path.abspath(os.environ['VIRTUAL_ENV'])

	# Setup the option parser
	parser = OptionParser()
	parser.add_option('-s', '--settings', dest='settings', default=None, action='store',
		help='custom settings PROJECT.settings is the default')

	(options, args) = parser.parse_args()

	# Setup the environment
	application_name = setup_environ(root)

	# Bootstrap the application
	bootstrap()

	# Get the management controllers
	master_controller = ManageController()
	master_controller.load()

	# Attempt to map the command to a controller

	# Run the command

if __name__ == '__main__':
	# Run the manage facility....
	main()