"""The Email class and it supporting Attachment classes simplify the process of
sending emails."""

import smtplib
import mimetypes
from email import Encoders
from email.Message import Message
from email.MIMEAudio import MIMEAudio
from email.MIMEBase import MIMEBase
from email.MIMEImage import MIMEImage
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Header import Header

from deploy.config import settings
from deploy.utils.encoding import to_unicode

__all__ = ('Email', 'Attachment')

class Email(object):
	"""A class for generating and sending emails."""

	def __init__(self, sender, subject, message, mode='plain', attachment_list=None):
		self.sender = sender
		self.subject = to_unicode(subject)
		self.message = to_unicode(message)
		self.mode = mode
		self.attachment_list = attachment_list if attachment_list is not None else []

	def send(self, to, cc=None, bcc=None, host=None, username=None, password=None, port=None):
		"""Send the email"""

		if host is None:
			host = settings.EMAIL_HOST

		if username is None:
			username = settings.EMAIL_USERNAME

		if password is None:
			password = settings.EMAIL_PASSWORD

		if port is None:
			port = settings.EMAIL_PORT

		# Build the email
		email_script = None

		# Check if we have any mail attachments
		if len(self.attachment_list) > 0:
			email_script = MIMEMultipart()
			email_script.attach(MIMEText(self.message.encode('utf8', 'ignore'), self.mode, 'utf8'))

			# Add each attachment
			for attachment in self.attachment_list:
				email_script.attach(attachment.as_script())
		else:
			email_script = MIMEText(self.message.encode('utf8', 'ignore'), self.mode, 'utf8')
			email_script['content-type'] = 'text/%s; charset=%s' % (self.mode, settings.ENCODING)

		email_script['subject'] = Header(self.subject, 'utf8')
		email_script['from'] = self.sender
		to_list = []

		if isinstance(to, basestring):
			email_script['to'] = to
			to_list.append(to)
		else:
			email_script['to'] = ', '.join(to)
			to_list.extend(to)

		if isinstance(cc, basestring):
			email_script['cc'] = cc
			to_list.append(cc)
		elif cc:
			email_script['cc'] = ', '.join(cc)
			to_list.extend(cc)

		if isinstance(bcc, basestring):
			email_script['bcc'] = bcc
			to_list.append(bcc)
		elif bcc:
			email_script['bcc'] = ', '.join(bcc)
			to_list.extend(bcc)

		# Connect to the server and send the email
		smtp_inst = smtplib.SMTP(host, port)

		if username and password:
			smtp_inst.login(username, password)

		smtp_inst.sendmail(self.sender, to_list, email_script.as_string())
		smtp_inst.close()

class Attachment(object):
	"""A class for generating email attachements."""

	def __init__(self, fp, filename, mimetype=None):
		self.fp = fp
		self.filename = filename
		self.mimetype = mimetype

		if self.mimetype is None:
			# Best guess at mime-type
			self.mimetype, self.encoding = mimetypes.guess_type(filename)

			if self.mimetype is None:
				self.mimetype = 'application/octet-stream'

	def as_script(self):
		"""Return the MIME attachment script"""
		attachment_script = None

		main_type, sub_type = self.mimetype.split('/', 1)
		if main_type == 'text':
			attachment_script = MIMEText(self.fp.read(), _subtype=sub_type)

		elif main_type == 'image':
			attachment_script = MIMEImage(self.fp.read(), _subtype=sub_type)

		elif main_type == 'audio':
			attachment_script = MIMEAudio(self.fp.read(), _subtype=sub_type)

		else:
			attachment_script = MIMEBase(main_type, sub_type)
			attachment_script.set_payload(self.fp.read())
			Encoders.encode_base64(attachment_script)

		attachment_script.add_header('Content-Disposition', 'attachment', filename=self.filename)

		return attachment_script
