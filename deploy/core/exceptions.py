"""Deploy exceptions"""

import sys
import datetime
import traceback

__all__ = ('DeployBaseException', 'ApplicationException', 'ConfigurationError')

class DeployBaseException(Exception):
	"""The Base Deploy Exception, all other Deploy exceptions should inherit from this. This
	should generally not be thrown, use ApplicationException instead"""

	def __init__(self, error_value=None):
		""" """
		self.error_value = error_value
		self.created = datetime.datetime.now()
		self._exc_info = None

		super(DeployBaseException, self).__init__(error_value)

	def __repr__(self):
		return '<DeployException: %s: %r>' % (self.__class__.__name__.capitalize(), self.error_value)

	def __unicode__(self):
		return repr(self.error_value)

	def __str__(self):
		return str(self.__unicode__())

	def get_exc_info(self):
		return self._exc_info

	def set_exc_info(self, v):
		self._exc_info = v

	exc_info = property(get_exc_info, set_exc_info)

	def get_traceback(self, exc_info=None):
		"""Get the last traceback as a string"""
		return '\n'.join(traceback.format_exception(*(exc_info or self.exc_info or sys.exc_info())))

	def get_msg(self, more=None):
		"""Utility function for returning an error in a readable message format,
		primarly used for emailing errors"""

		msg = []
		msg.append('An error has occured:\n')
		msg.append(self.get_traceback())

		if more:
			msg.append(more)

		return '\n'.join(msg)

class ApplicationException(DeployBaseException):
	"""This is a basic wrapper around the base exception, which can be used throught the
	Deploy application as the most general form of an Exception"""

	pass

class ConfigurationError(ApplicationException):
	"""This exception is to be used by Deploy for errors that occur during configiration/startup"""

	pass