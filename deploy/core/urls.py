"""Tools for working with URL's"""

import re
import types
import urlparse
import unicodedata

from deploy.core.http.utils import HTTP_VERBS

__all__ = ('URL_CHANGE_FREQUENCY', 'URL', '__resolver__', 'safe_url')

# Sitemap specification for valid URL change frequency
URL_CHANGE_FREQUENCY = (
	'ALWAYS',
	'HOURLY',
	'DAILY',
	'WEEKLY',
	'MONTHLY',
	'YEARLY',
	'NEVER',
)

URL_WORD_STRIP_LIST = [
	'a', 'an', 'as', 'at', 'before', 'but', 'by', 'for', 'from', 'is', 'in',
	'into', 'like', 'of', 'off', 'on', 'onto', 'per', 'since', 'than', 'the',
	'this', 'that', 'to', 'up', 'via', 'with'
]

class URL(object):

	def __init__(self, url, endpoint, last_modified=None, priority=0.5, changefreq=None, sitemap=True,
				host='*', allowed_verbs=None, options=None):
		"""Initialise the URL"""

		if changefreq is not None:
			assert changefreq in URL_CHANGE_FREQUENCY, '"%s" is an invalid "changefreq" for URL "%s"!' % (changefreq, url)

		if priority is not None:
			assert priority >= 0.0 and priority <= 1.0, '"%s" invalid priority for URL "%s"!' % (priority, url)

		if last_modified is not None:
			# @@ TODO: Check the URL last modified date is a sane date
			pass

		if allowed_verbs is not None:
			# @@ TODO: Check each allowed URL verb is in allowed HTTP verbs
			pass
		else:
			allowed_verbs = ('GET', 'POST', 'HEAD')

		if host and host != '*':
			host = re.compile(r'^%s$' % host)

		self._url = re.compile(r'^%s$' % url)
		self._host = host
		self._raw_url = url
		self._endpoint = endpoint

		self._meta = {
			'changefreq': changefreq,
			'priority': priority,
			'last_modified': last_modified,
			'allowed_verbs': allowed_verbs,
			'options': options if options is not None else {},
		}

	def __unicode__(self):
		return '%s' % self._raw_url

	def __str__(self):
		return str(self.__unicode__())

	def match(self, method, hostname, query_url):
		"""Attempt to match a given string to the internal URL expression"""

		# Match the method
		if not method:
			# Assume GET
			method = 'GET'

		if not method in self._meta['allowed_verbs']:
			return False

		# Match the hostname
		if self._host and not self._host == '*':
			# We have to perform a hostname match...
			hostname_match = self._host.match(hostname)

			if not hostname_match:
				return False

		# Match the path/query_url...
		query_url_match = self._url.match(query_url)

		# Query URL does not match
		if not query_url_match:
			return False

		# Get any groups that may be in the match
		kwargs = query_url_match.groupdict()

		if kwargs:
			# Check if there are default options to include
			if self._meta['options']:
				default_kwargs = self._meta['options']
				default_kwargs.update(kwargs)
				kwargs = default_kwargs

			return kwargs

		args = query_url_match.groups()

		if args:
			return args

		# Check for default options
		if self._meta['options']:
			return self._meta['options']

		return True

	def get_endpoint(self):
		return self._endpoint

	def set_endpoint(self, v):
		self._endpoint = v

	endpoint = property(get_endpoint, set_endpoint)

	@staticmethod
	def validate(url):
		"""Utility to validate if a URL is in a valid format"""

		# @@ TODO: Add in validation for URL's

		pass

class Resolver(object):

	def __init__(self):
		self.map = []
		self.cache = {}

	def add(self, url_arg):
		"""Add a URL (or a list/tuple of URL's) to the resolvers
		internal Map"""

		if isinstance(url_arg, types.ModuleType):
			# Support for adding url lists from modules

			# Check it has the correct information in the module..
			if hasattr(url_arg, 'urls'):
				self.add(url_arg.urls)

		elif isinstance(url_arg, (list, tuple)):
			# Support for adding multiple URL's at once
			for url in url_arg:
				self.map.append(url)

		else:
			self.map.append(url_arg)

	def resolve(self, environ, allow_cache=True, configuration=False):
		"""Resolve a string URL to a specific URL object"""

		url = environ['PATH_INFO']
		hostname = environ['HTTP_HOST'] if environ.has_key('HTTP_HOST') else environ['SERVER_NAME']
		method = environ['REQUEST_METHOD']

		parsed_url = urlparse.urlparse(url)
		url_path = parsed_url.path

		if allow_cache:
			cache_key = '%s:%s%s' % (method.upper(), hostname, url_path)

			# Check the internal cache
			if cache_key in self.cache:
				url, args, kwargs = self.cache[cache_key]
				return url.get_endpoint(), args, kwargs

		# Check the internal map (1st match wins!!)
		for site_url in self.map:
			result = site_url.match(method, hostname, url_path)

			if result:
				# Get the URL's endpoint
				endpoint = site_url.get_endpoint()

				args = None
				kwargs = None

				if isinstance(result, dict):
					kwargs = result
				elif isinstance(result, (tuple, list)):
					args = result

				# Add the positive match to the cache
				if allow_cache:
					self.add_to_cache(cache_key, site_url, args, kwargs)

				if configuration:
					return site_url

				else:
					return (endpoint, args, kwargs)

		# Unable to resolve the URL...
		return None

	def add_to_cache(self, key, url, args=None, kwargs=None):
		"""Add a URL to the cache. This saves lookup time in future"""
		self.cache[key] = url, args, kwargs

	def flush_cache(self):
		"""Clears the internal resolver cache"""
		self.cache = {}

	def get_all_urls(self):
		"""Return a list of all the current URL's in the resolver map"""
		return [unicode(url) for url in self.map]

__resolver__ = Resolver()

def reverse_url(name, *args, **kw):
    """Return a named URL by it's name"""
    
    # Default to the base application resolver
    if 'resolver' in kw:
        resolver = kw['resolver']
    
    else:
        resolver = __resolver__
    
    # Can we find the named URL?
    if name not in resolver.named_map:
        raise KeyError("%s not found in named urls" % name)
    
    pattern = resolver.named_map[name]._raw_url
    regex =  resolver.named_map[name]._url
    
    # Does the number of arguments match that of the URL's?
    if regex.groups != len(args):
        raise RuntimeError('Incorrect number of arguments for named URL: %s' % name)
        
    # Can we reverse the URL pattern (we can't cope with brackets basically)
    if regex.groups != pattern.count('('):
        raise RuntimeError('Cannot reverse URL, pattern is too complicated for named URL: %s' % name)

    # Convert the regex to a URL
    pieces = []
    
    for fragment in pattern.split('('):
        if ')' in fragment:
            paren_loc = fragment.index(')')
            if paren_loc >= 0:
                pieces.append('%s' + fragment[paren_loc + 1:])
        else:
            pieces.append(fragment)

    safe_args = []

    for arg in args:
        if isinstance(arg, basestring):
            safe_args.append(urllib.quote_plus(arg))
        else:
            safe_args.append(arg)

    return ''.join(pieces) % tuple(safe_args)

def safe_url(text):
	"""Convert a string into a safe URL and return it. Adapted from code listed
	on 'http://www.djangosnippets.org/snippets/29/' by santuri."""

	url = ''

	# Remove the reserved word list
	for word in URL_WORD_STRIP_LIST:
		url = re.sub(r'\b' + word + r'\b', '', text)

	# Remove any non-latin characters
	url = unicodedata.normalize('NFKD', url).encode('ASCII', 'ignore')

	# Remove anything else we don't like the look of, and replace spaces
	url = re.sub('[^\w\s-]', '', url).strip().lower()
	url = re.sub('\s+', '-', url)

	return url