"""Deploy handler for WSGI servers"""

import os
import os.path
import sys
import logging

from deploy.config import settings
from deploy.config.utils import setup_environ, get_application_pipeline
from deploy.utils.importing import import_module

class WSGIHandler(object):

	def __init__(self):
		"""Setup the deploy environment"""

		# Check we have settings first...
		if not os.environ.has_key('DEPLOY_SETTINGS'):
			raise RuntimeError('Unable to find DEPLOY_SETTINGS - is it specified in the environment?')

		# Load our local settings
		settings.load(os.environ['DEPLOY_SETTINGS'], force=True)

		# Find the root of the application
		if os.environ.has_key('VIRTUAL_ENV'):
			# Use the virtual env if specified as this will be more accurate
			root = os.path.abspath(os.environ['VIRTUAL_ENV'])
		else:
			# Use the specified site root
			root = os.path.abspath(settings.SITE_ROOT)

		# Setup the environment and get our application name
		application_name = setup_environ(root)

		# Set the application name to the settings
		settings.load({'APPLICATION_NAME': application_name}, force=True)

		# Setup the log...

		# Check the log file exists
		if not os.path.exists(settings.DEBUG_LOG):
			# Create a blank file
			open(settings.DEBUG_LOG, 'w')

		logging.basicConfig(
			level=logging.DEBUG,
			format=settings.DEBUG_LOG_FORMAT,
			filename=settings.DEBUG_LOG,
			filemode='a+'
		)

		log = logging.getLogger('deployapp')

		# Get the setup args, turn off any verbosity settings
		# (not used here in uWSGI)
		setup_args = settings.APPLICATION_SETUP_ARGS

		if not setup_args:
			setup_args = {'VERBOSE': False}
		else:
			setup_args['VERBOSE'] = False

		# Setup config for application
		self.deploy = get_application_pipeline(setup_args)

	def __call__(self, environ, start_response):
		"""Run the deploy application"""

		def wsgi_handler(status, headers, exc_info=None):

			# @@ TODO: ..

			return start_response(status, headers)

		# Run the application
		return self.deploy(environ, wsgi_handler)
