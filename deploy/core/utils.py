"""Utility functions for working with Deploy models"""

import re

__all__ = ('get_model_table_name',)

def get_model_table_name(name):
	"""Create the table name from the model name"""
	return str(re.sub(r'([a-z]*)([A-Z])', r'\1_\2', name)).lstrip('_').lower()

