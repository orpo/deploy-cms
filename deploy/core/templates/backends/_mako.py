"""Template interface for 'Mako' language"""

import os.path
import markupsafe

from mako.template import Template
from mako.lookup import TemplateLookup

from deploy.config import settings
from deploy.core.templates import get_template
from deploy.core.templates.backends import BaseTemplateInterface
from deploy.utils.strings import SafeData

__all__ = ('TemplateInterface', 'safe_strings')

def safe_strings(arg):
	"""Check if the string is marked as safe - otherwise it is escaped"""

	# See if it is a safe string/unicode object
	if isinstance(arg, SafeData):
		return arg

	return markupsafe.escape(arg)

class TemplateInterface(BaseTemplateInterface):
	"""Template interface for 'Mako' language"""

	engine = 'mako'
	template_lookup = None

	def __init__(self, request=None, data=None, *args, **kwargs):

		if settings.TEMPLATE_CONFIG.has_key('cache'):
			self.template_cache = os.path.join(settings.TEMPLATE_CONFIG)
		else:
			self.template_cache = os.path.join(settings.SITE_ROOT, 'tmp/mako_cache')

		# Template lookup should only ever be initialised once, if it's not been
		# initialised we create it and add it to the parent class for future use.
		# if self.template_lookup is None:
		TemplateInterface.template_lookup = TemplateLookup(
			directories=settings.TEMPLATE_LOCATIONS
		)

		super(TemplateInterface, self).__init__(request=request, data=data)

	def get_value(self, attr):
		if not attr in self._data:
			raise NameError('"%s" does not exist in template args' % attr)

		return self._data[attr]

	def set_value(self, attr, value):
		self._data[attr] = value

	def render(self, template=None, data=None):

		# Sanity Checking....
		assert (template or self._source), 'You must specify a template file or template source to render!'

		if self._source:
			the_template = Template(
				self._source,
				lookup=self.template_lookup,
				output_encoding=settings.ENCODING,
				default_filters=['safe_strings'],
				imports=['from deploy.core.templates.backends._mako import safe_strings']
			)
		else:
			template = get_template(template)

			the_template = Template(
				filename=template,
				module_directory=self.template_cache,
				lookup=self.template_lookup,
				output_encoding=settings.ENCODING,
				default_filters=['safe_strings'],
				imports=['from deploy.core.templates.backends._mako import safe_strings']
			)

		if self._request:
			self.set_value('request', self._request)

		if data is not None:
			self._data.update(data)

		result = the_template.render(**self._data)

		return result

	def parse(self, template_code):
		self._source = template_code
