"""Template interface for 'jinja' language"""

import os.path

from jinja2 import Environment, Template, FileSystemLoader, PackageLoader

from deploy.config import settings
from deploy.core.templates.backends import BaseTemplateInterface

__all__ = ('TemplateInterface',)

class TemplateInterface(BaseTemplateInterface):
	"""Template interface for 'jinja' language"""

	engine = 'jinja'

	def __init__(self, request=None, data=None, *args, **kwargs):
		# Setup the loader
		loader = FileSystemLoader(settings.TEMPLATE_LOCATIONS)

		autoescape = True

		if settings.TEMPLATE_CONFIG.has_key('autoescape'):
			autoescape = autoescape

		# Setup the environment
		self.environment = Environment(loader=loader, autoescape=autoescape)

		# Check if we have any custom filters or globals, if so add them to the environment
		if settings.TEMPLATE_CONFIG.has_key('globals'):
			for key, value in settings.TEMPLATE_CONFIG['globals'].items():
				self.environment.globals[key] = value

		if settings.TEMPLATE_CONFIG.has_key('filters'):
			for key, value in settings.TEMPLATE_CONFIG['filters'].items():
				self.environment.filters[key] = value

		super(TemplateInterface, self).__init__(request=request, data=data)

	def get_value(self, attr):
		if not attr in self._data:
			raise NameError('"%s" does not exist in template args' % attr)

		return self._data[attr]

	def set_value(self, attr, value):
		self._data[attr] = value

	def render(self, template=None, data=None):

		# Sanity Checking....
		assert (template or self._source), 'You must specify a template file or template source to render!'

		if self._source:
			the_template = self.environment.from_string(self._source)
		else:
			the_template = self.environment.get_template(template)

		if data is not None:
			self._data.update(data)

		if self._request and not 'request' in self._data:
			self.set_value('request', self._request)

		result = the_template.render(**self._data)

		return result

	def parse(self, template_code):
		self._source = template_code
