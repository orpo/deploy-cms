"""Template interface for 'Clearsilver' language"""

import decimal
from datetime import datetime
from xml.sax.saxutils import escape as xmlescape

import neo_cgi; neo_cgi.update()
import neo_util
import neo_cs

from deploy.config import settings
from deploy.core.templates import get_template
from deploy.core.templates.backends import BaseTemplateInterface
from deploy.utils.strings import SafeData
from deploy.utils.encoding import to_unicode

__all__ = ('TemplateInterface',)

class TemplateInterface(BaseTemplateInterface):

	engine = 'clearsilver'

	# Internal Clearsilver managers
	CS = neo_cs.CS
	HDF = neo_util.HDF

	def __init__(self, request=None, data=None, *args, **kwargs):
		self._hdf = self.HDF()

		super(TemplateInterface, self).__init__(request=request, data=data)

	def set_globals(self):
		"""Set the default"""

		# Global args
		self.set_value('site_url', settings.SITE_URL)
		self.set_value('site_edit_url', settings.SITE_EDIT_URL)
		self.set_value('site_media_prefix', settings.SITE_MEDIA_PREFIX)
		self.set_value('site_name', settings.SITE_NAME)
		self.set_value('date', str(datetime.now().date()))
		self.set_value('time', str(datetime.now().time()))

		# Include global resources
		css_resources = []
		for css_resource_list in settings.RESOURCE_MAP['css']:
			css_resources.append([css.as_dict() for css in css_resource_list])

		js_resources = []
		for js_resource_list in settings.RESOURCE_MAP['js']:
			js_resources.append([js.as_dict() for js in js_resource_list])

		resources = {
			'css': css_resources,
			'js': js_resources,
		}

		self.set_value('resources', resources)

		if self._request:
			# Set the request dictionary
			self.set_value('request', self._request.as_dict())

	def get_value(self, attr):
		raise NotImplementedError

	def set_value(self, attr, value):
		self._hdf = TemplateInterface.to_hdf(self._hdf, attr, value)

	def render(self, template=None, data=None):

		# Sanity Checking....
		assert (self._source or template), 'You must specify a template file or template source to render!'

		# Include any args sent at render time....
		if data is not None:
			self.set_value('Page', data)

		# Add the default global values to the template args
		self.set_globals()

		# Setup the HDF specific args
		hdf_args = {
			'loadpaths': settings.TEMPLATE_LOCATIONS
		}

		self.set_value('hdf', hdf_args)

		#if template != 'debug.html':
		#	raise Exception(self._hdf.dump())

		# Get the CS parser object
		cs = self.CS(self._hdf)

		if self._source:
			cs.parseStr(self._source)
		else:
			cs.parseFile(get_template(template))

		result = cs.render()

		return result

	def parse(self, template_code):
		self._source = template_code

	@staticmethod
	def to_hdf(hdf, key, value, escape=True):
		"""Recursive facility for building HDF outputs"""

		# NOTE: Be careful with large datasets.....

		if not value:
			hdf.setValue(key.encode('utf-8'), '')

		# Python Types
		if isinstance(value, (list, tuple)):
			for count, item in enumerate(value):
				TemplateInterface.to_hdf(hdf, '%s.%s' % (key, count), item, escape=escape)

		elif isinstance(value, dict):
			for dkey, dvalue in value.items():
				TemplateInterface.to_hdf(hdf, '%s.%s' % (key, dkey), dvalue, escape=escape)

		elif isinstance(value, bool):
			hdf.setValue(key.encode('utf-8'), '1' if value else '')

		elif isinstance(value, (int, long)):
			hdf.setValue(key.encode('utf-8'), '%d' % value)

		elif isinstance(value, float):
			hdf.setValue(key.encode('utf-8'), '%.2f' % value)

		elif isinstance(value, decimal.Decimal):
			hdf.setValue(key.encode('utf-8'), '%.2f' % value)

		elif isinstance(value, str):
			if escape and not isinstance(value, SafeData):
				value = xmlescape(to_unicode(value))
				hdf.setValue(key.encode('utf-8'), value.encode('utf-8'))
			else:
				hdf.setValue(key.encode('utf-8'), str(value))

		elif isinstance(value, unicode):
			if escape and not isinstance(value, SafeData):
				value = xmlescape(value)
				hdf.setValue(key.encode('utf-8'), value.encode('utf-8'))
			else:
				hdf.setValue(key.encode('utf-8'), value.encode('utf-8'))

		# SQLObject types
		#elif isinstance(value, SelectResults):
		#	TemplateInterface.to_hdf(hdf, key, list(value), escape=escape)

		# Deploy Types (All deploy types which can be used for template output should have a as_dict callable)
		elif hasattr(value, 'as_dict'):
			TemplateInterface.to_hdf(hdf, key, value.as_dict(), escape=escape)

		else:
			hdf.setValue(key.encode('utf-8'), repr(value))

		return hdf