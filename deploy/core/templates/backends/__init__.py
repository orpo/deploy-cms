"""Wrapper library for template interfaces"""

from datetime import datetime

from deploy.config import settings

__all__ = ('BaseTemplateInterface',)

class BaseTemplateInterface(object):
	"""Base Interface for Template Adaptors"""

	engine = 'base'

	def __init__(self, request=None, data=None, *args, **kwargs):
		self._source = None
		self._request = request
		self._data = data if data is not None else {}

	def __repr__(self):
		return '<DeployTemplateInterface: %s>' % self.engine

	def get_value(self, attr):
		"""Return a template argment value for a key"""
		raise NotImplementedError

	def set_value(self, attr, value):
		"""Set a template argment value for a key"""
		raise NotImplementedError

	values = property(get_value, set_value)

	def render(self, template, data=None):
		"""Render the template"""
		raise NotImplementedError

	def parse(self, template_code):
		"""Parse a template"""
		raise NotImplementedError
