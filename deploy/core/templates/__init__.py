"""Template manager"""

from datetime import datetime

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError, ApplicationException
from deploy.utils._os import get_file_location
from deploy.utils.importing import import_module

__all__ = ('TemplateManager', 'get_template')

class TemplateManager(object):
	"""Template manager acts as a dispatcher for template interfaces"""

	def __new__(cls, *args, **kwargs):
		# Check if a specific template interface 'engine' has been specified

		if 'engine' in kwargs and kwargs['engine'] is not None:
			engine = kwargs['engine']

			# For security template engines *must* be specified in the settings. This is
			# to stop code be arbitrarily imported
			assert engine in settings.TEMPLATE_ENGINES, 'Invalid template engine "%s"! You must specify template engines in settings!' % engine

			del kwargs['engine']

		else:
			assert isinstance(settings.TEMPLATE_ENGINES, (list, tuple)), 'TEMPLATE_ENGINES must be a list or tuple!'

			# Get the default template engine
			engine = settings.TEMPLATE_ENGINES[0]

		engine_path = 'deploy.core.templates.backends._%s' % engine

		# Attempt to load the template interface
		try:
			engine_interface = import_module(engine_path)
		except ImportError, e:
			raise ConfigurationError('Unable to find a backend for template engine "%s"!' % engine)

		return engine_interface.TemplateInterface(*args, **kwargs)

def get_template(template_name, template_locations=None, strict=True):
	"""Utility function to return the most applicable template for a given
	name. This function will search backwards through the template list,
	first match wins."""

	if not template_locations:
		template_locations = settings.TEMPLATE_LOCATIONS

	try:
		template = get_file_location(template_name, template_locations)
	except Exception, e:
		raise ApplicationException('Error locating template: %s' % e)

	if not template and strict:
		raise ApplicationException('Template "%s" does not exist!' % template_name)

	return template