"""Deploy Snippets

Snippets are small blocks of HTML that can be injected into a HTML
document.

Snippet HTML format:

	<div id="examplesnippet" deploy:snippet="example|foo|spam">
		Place Holder...
	<div>
"""

import re

import html5lib
import lxml.html
from lxml.html import builder

from deploy.config import settings

__all__ = ('__snippets__', 'parse_snippets', 'parse_snippet_info')

SNIPPET_ATTRIBUTE = 'data-deploysnippet'

class SnippetManager(object):
	"""Tool for managing snippets"""

	def __init__(self):
		self._snippets = {}

	def get_snippet(self, name):
		return self._snippets[name]

	def set_snippet(self, snippet, name=None):

		if name is not None:
			self._snippets[name] = snippet

		elif hasattr(snippet, '__fragment_name__'):
			self._snippets[snippet.__fragment_name__] = snippet

		else:
			self._snippets[snippet.__name__] = snippet

	def has_snippet(self, name):
		return name in self._snippets

	def register(self, snippet_function, name=None):
		"""Wrapper around set_snippet"""
		self.set_snippet(snippet_function, name=name)

	def run(self, name, args, request=None):
		if not self.has_snippet(name):
			raise KeyError(u'No snippet called "%s"' % name)

		snippet = self.get_snippet(name)

		return snippet(request, *args)

__snippets__ = SnippetManager()

def parse_snippets(html, request=None, debug=False, strict=False):
	"""Parse a HTML string and process any deploy snippet tags"""

	assert html is not None, u'Invalid template sent to parse snippets!'

	# The aim here is to parse out snippet attributes and create a map of
	# snippet functions to call and the arguments they except.

	parser = html5lib.HTMLParser(tree=html5lib.treebuilders.getTreeBuilder('lxml'), namespaceHTMLElements=False)

	# Parse the html
	try:
		tree = parser.parse(html, encoding=settings.ENCODING)
	except Exception:
		if strict:
			# Re-rasie the error
			raise e

		return None

	snippet_elements = tree.findall('.//*[@%s]' % SNIPPET_ATTRIBUTE)

	for snippet_node in snippet_elements:
		# Store the snippet information...
		snippet_info = snippet_node.attrib[SNIPPET_ATTRIBUTE]

		# Remove the attribute
		del snippet_node.attrib[SNIPPET_ATTRIBUTE]

		# Do we have anything to work with?
		if not snippet_info:
			continue

		info = None

		try:
			info = parse_snippet_info(snippet_info)
		except Exception, e:
			# Snippet was malformed or incorrect, if we are in debug mode
			# we add in the debug information to the snippet.
			if debug:
				error_node = builder.P(builder.CLASS('snippet-error'), u'Snippet Error: %s' % unicode(e))
				snippet_node.append(error_node)

			continue

		# Attempt to run the snippet
		try:
			snippet_html = __snippets__.run(info['name'], info['args'], request=request)
			snippet_html_node = lxml.html.fromstring(snippet_html)
			snippet_node.append(snippet_html_node)
		except Exception, e:
			# If snippet failed to execute correctly and we are in debug mode,
			# display the relevent information
			if debug:
				error_node = builder.P(builder.CLASS('snippet-error'), u'Snippet Error: %s' % unicode(e))
				snippet_node.append(error_node)

			continue

	walker = html5lib.treewalkers.getTreeWalker('lxml')
	stream = walker(tree)

	s = html5lib.serializer.htmlserializer.HTMLSerializer(**settings.HTML_SERIALIZER_OPTIONS)
	output_generator = s.serialize(stream, encoding=settings.ENCODING)

	return ''.join([chunk for chunk in output_generator])

def parse_snippet_info(snippet_info):
	"""Parse snippet info into relevent segments"""

	if not '|' in snippet_info:
		return {'name': snippet_info, 'args': []}

	bits = snippet_info.split('|')

	return {'name': bits[0], 'args': bits[1:]}
