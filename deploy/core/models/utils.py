"""Utilities for working with Models"""

import re
import json
import inspect

from storm.expr import Column

from deploy.config import settings
from deploy.utils.importing import import_module, get_class_list
from deploy.utils.encoding import ExtendedJSONEncoder

__all__ = ('get_model_slug', 'get_model_from_slug','get_model_table_name', 'get_table_model_name',
			'get_model_columns', 'get_model_column_names', 'find_models' 'get_all_models',
			'serialize_to_json')

UPPERCASE_REGEX = re.compile(r'([A-Z])')
UNDERSCORE_REGEX = re.compile(r'(_(\w))')
DASH_REGEX = re.compile(r'(-(\w))')

def get_model_slug(name):
	"""Format a Model Class name as a 'slug'. E.g. MyModelClass == my-model-class"""
	return UPPERCASE_REGEX.sub('-\g<1>', name).lower().lstrip('-')

def get_model_from_slug(name):
	"""Format a 'slug' as Model Class name. E.g. my-model-class == MyModelClass"""

	def dash_replace(match):
		return match.group(2).upper()

	return DASH_REGEX.sub(dash_replace, name.capitalize())

def get_model_table_name(name):
	"""Format a Model Class name as a database table name. E.g. MyModelClass == my_model_class"""
	return UPPERCASE_REGEX.sub('_\g<1>', name).lower().lstrip('_')

def get_table_model_name(name):
	"""Format a table name as a Model Class name. E.g my_model_class == MyModelClass"""

	def uppercase_replace(match):
		return match.group(2).upper()

	return UNDERSCORE_REGEX.sub(uppercase_replace, name.capitalize())

def get_model_columns(model):
	"""Get a list of tuples containing the column name and type"""

	pairs = []

	for attr in dir(model):
		if not attr.startswith('__'):
			column = getattr(model, attr, None)

			if isinstance(column, Column):
				pairs.append((attr, column))

	pairs.sort()

	return pairs

def get_model_column_names(model):
	"""Get a list of column names the model has"""

	columns = []

	for attr in dir(model):
		if not attr.startswith('__'):
			column = getattr(model, attr, None)

			if isinstance(column, Column):
				columns.append(attr)

	columns.sort()

	return columns

def find_models(module):
	"""Recursive function to find all the models in a python module structure"""

	models = {}

	# Get all the child members of the module, to check
	for attribute_name in dir(module):

		attribute = getattr(module, attribute_name)

		if not attribute_name in module.__all__:
			continue

		# Ignore specials
		if not attribute_name.startswith('__'):

			# Is the attribute a Model?
			if inspect.isclass(attribute) and issubclass(attribute, Model) and not attribute.__name__ == 'Model':
				models['%s.%s' % (module.__name__, attribute_name)] = attribute

			# Is it another module we can look inside?
			elif inspect.ismodule(attribute):
				# Round again...
				models.update(find_models(attribute))

	return models

def get_all_models():
	"""Return a list of all models configured in the system. This does no
	validation on the models to check the exist in the database or have the
	correct setup, it's purely a list of what the deploy application currently
	believes to be active models"""

	from deploy.plugins import __plugins__

	# Models live here:
	# - Application root
	# - Inside active plugins

	models = {}
	plugin_models = {}

	# Find the application models (if they exist)
	try:
		application = import_module(Settings.APPLICATION_NAME, fromlist=['models'])
		application_models = application.models
	except (ImportError, AttributeError):
		# If we can't import it we continue anyway as deploy would not
		# of been able to import it earlier
		pass
	else:
		# There is a Model file - lets have a look inside the models file for
		# Model subclasses
		models.update(find_models(application_models))

	# Find the plugin models
	installed_plugins = __plugins__.all()

	for plugin in installed_plugins:
		try:
			plugin_app = import_module(plugin, fromlist=['models'])
			plugin_app_models = plugin_app.models
		except (ImportError, AttributeError):
			# Doesn't matter, may not exist, move on...
			pass
		else:
			# There is a Model file - lets have a look inside the models file for
			# Model subclasses
			plugin_models.update(find_models(plugin_app_models))

	return models, plugin_models

def serialize_to_json(arg):
	"""Serialize the argument to JSON"""
	return json.dumps(arg, cls=ExtendedJSONEncoder)