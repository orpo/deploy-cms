"""Custom Deploy Storm ORM Properties"""

import os.path

from storm.properties import Unicode
from storm.expr import Column
from storm.variables import VariableFactory

from deploy.config import settings

class EmailField(Unicode):
	"""Wrapper for Email Fields"""
	pass

class PasswordField(Unicode):
	"""Wrapper for Password Fields"""
	pass

class FileField(unicode):
	"""Wrapper for File Fields"""

	def __init__(self, prop, cls, attr, name, primary,
				 variable_class, variable_kwargs, location=''):

		# Internal file handler
		self._file = None
		self._filename = ''

		# @@ TODO: Securty check the path isnt outside of the base dir
		self._location = os.path.join(settings.SITE_DATA_ROOT, 'files', location.lstrip('/'))

		super(FileField, self).__init__(prop, cls, attr, name, primary,
					 variable_class, variable_kwargs)

	def save(self):
		pass

	def delete(self):
		pass

	def open(self):
		pass

	def close(self):
		pass
