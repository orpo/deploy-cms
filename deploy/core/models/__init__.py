"""Deploy Data Modeling"""

import warnings

from storm.base import Storm
from storm.expr import Column
from storm.properties import Int, PropertyPublisherMeta
from storm.info import get_cls_info, get_obj_info
from pydispatch import dispatcher

from deploy.config import settings
from deploy.core.models.utils import get_model_table_name, get_model_columns,\
	get_model_column_names, serialize_to_json
from deploy.utils.encoding import to_unicode

__all__ = ('Model')

ALLOWED_SERIALIZE_FORMATS = {
	'json': serialize_to_json,
	'xml': None,
	'csv': None,
}

DEFAULT_MODEL_META = {
	'verbose_name': None,
	'verbose_name_plural': None,
	'serialize_keys': [],
}

ADD_FLAG = 1
REMOVE_FLAG = 2
UPDATE_FLAG = 3

class ModelBuilder(type):

	def __new__(cls, name, bases, attrs):
		# Add in the special storm attributes....

		# Table name - we do this here as we are not interested in base
		# classes, table names are specific to the class
		if not '__storm_table__' in attrs:
			attrs['__storm_table__'] = get_model_table_name(name)

		# Check for a primary key, otherwise add one
		# has_primary = False
		#
		# for name, attr in attrs.items():
		# 	if isinstance(attr, Column):
		# 		if attr.primary != 0:
		# 			has_primary = True
		#
		# if not has_primary:
		# 	attrs['id'] = Int(primary=True)

		# Check if model meta data has been specified
		attr_meta = attrs.pop('Meta', None)

		if attr_meta is not None:
			# Model has meta detail specified, merge this with the default options
			base_meta = DEFAULT_MODEL_META.copy()
			base_meta.update(attr_meta.__dict__)

			attrs['Meta'] = type('Meta', (object,), base_meta)

		else:
			attrs['Meta'] = type('Meta', (object,), DEFAULT_MODEL_META)

		new_class = super(ModelBuilder, cls).__new__(cls, name, bases, attrs)

		return new_class

class Model(object):
	"""Model is used as a basic template for data models within deploy"""

	__metaclass__ = ModelBuilder

	def __init__(self, *args, **kwargs):

		# Shortcut to set the model attributes from the kwargs
		if kwargs:
			for key, value in kwargs.items():
				setattr(self, key, value)

		# Setup the meta information
		if self.Meta.verbose_name is None:
			self.Meta.verbose_name = self.__class__.__name__.lower()

		if self.Meta.verbose_name_plural is None:
			self.Meta.verbose_name_plural = '%ss' % self.__class__.__name__.lower()

	def __repr__(self):
		return '<DeployModel: %s>' % self.__class__.__name__.capitalize()

	def __str__(self):
		return str(self.__unicode__())

	def __unicode__(self):
		return u'<%s Model>' % self.__class__.__name__.capitalize()

	def __storm_pre_flush__(self):
		"""Hook to bind pre flush storm events to dispatcher events"""
		obj_info = get_obj_info(self)

		# Get the pending flag - this tells us what event is about to happen
		pending = obj_info.get('pending', None)

		signal_namespace = 'Deploy.%s' % self.__class__.__name__

		# Work out what is going to happen to an object
		if pending == ADD_FLAG:
			signal_key = '%s.on_add' % signal_namespace
			obj_info['DEPLOY_STATE'] = ADD_FLAG

		elif pending == REMOVE_FLAG:
			signal_key = '%s.on_remove' % signal_namespace
			obj_info['DEPLOY_STATE'] = REMOVE_FLAG

		else:
			signal_key = '%s.on_update' % signal_namespace
			obj_info['DEPLOY_STATE'] = UPDATE_FLAG

		# Send out the signal
		dispatcher.send(signal=signal_key, sender=self)

	def __storm_flushed__(self):
		"""Hook to bind post flush storm events to dispatcher events"""

		obj_info = get_obj_info(self)

		# Attempt to get the state of the flushed object
		obj_state = obj_info.pop('DEPLOY_STATE', None)

		if obj_state is not None:
			signal_namespace = 'Deploy.%s' % self.__class__.__name__
			signal_key = None

			# Try to send a signal for the state
			if obj_state == ADD_FLAG:
				signal_key = '%s.after_add' % signal_namespace

			elif obj_state == REMOVE_FLAG:
				signal_key = '%s.after_remove' % signal_namespace

			elif obj_state == UPDATE_FLAG:
				signal_key = '%s.after_update' % signal_namespace

			# Send out the signal
			dispatcher.send(signal=signal_key, sender=self)

	def to_dict(self):
		"""Return the Form object as a JSON object"""
		warnings.warn('Using to_dict is a depeciated API, use the models serialize function', DeprecationWarning)
		return self.__dict__

	def get_model_name(self):
		return self.verbose_name

	def get_unique_identifier(self):
		"""Return a unique identifier for the model"""
		return self.id

	def update(self, args):
		"""Update a model with values from a dictionary"""

		if args is None:
			return False

		if not hasattr(args, 'items'):
			return False

		for k, v in args.items():
			if hasattr(self, k):
				setattr(self, k, v)

		return True

	@staticmethod
	def serialize(obj_arg, keys=None, format=lambda d: d, recursive=False):
		"""Serialize an object in a given format"""

		def to_dict(obj, keys):
			if keys is None:
				columns = get_model_columns(obj.__class__)
				keys = [name for name, col in columns]

			values = {}

			for key in keys:
				key_value = getattr(obj, key)

				if isinstance(key_value, Model) and recursive:
					values[key] = key_value.__class__.serialize(key_value, format=format, recursive=recursive)

				elif isinstance(key_value, Model):
					values[key] = key_value.id

				else:
					values[key] = key_value

			return values

		# Check for an overide for keys
		if keys is None and obj_arg.Meta.serialize_keys:
			keys = get_model_column_names(obj_arg.__class__)
			keys.extend(obj_arg.Meta.serialize_keys)

		if callable(format):
			formatter = format
		else:
			assert format in ALLOWED_SERIALIZE_FORMATS, 'Unkown serialize format!'
			formatter = ALLOWED_SERIALIZE_FORMATS[format]

		if hasattr(obj_arg, '__iter__'):
			# There are a series of instances
			results = []

			for obj in obj_arg:
				results.append(formatter(to_dict(obj, keys)))

			# @@ TODO: Serialize only supports JSON for lists of objects

			return '[%s]' % ','.join(results)

		# Standard old instance...
		return formatter(to_dict(obj_arg, keys))
