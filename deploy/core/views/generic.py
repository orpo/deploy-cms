"""Generic views for the deploy system"""

from deploy.config import settings
from deploy.shortcuts import render_page, redirect_to

__all__ = ('template', 'redirect')

def template(request, template=None, template_engine=None, content_type='text/html', **kwargs):
	"""Generic view: 'template' Renders a specified template with a given template
	engine (otherwise uses the default engine) and passes in any additional arguments"""

	assert template is not None, u'You must specify a template to template generic view!'

	if not template_engine:
		template_engine = settings.TEMPLATE_ENGINES[0]

	return render_page(
		request,
		template,
		kwargs,
		template_engine=template_engine,
		content_type=content_type
	)

def redirect(request, url=None, status=302):
	"""Generic view 'redirect' redirects a view to another URL"""

	assert url is not None, u'You must specify a URL to redirect to!'

	return redirect_to(url, status=status)