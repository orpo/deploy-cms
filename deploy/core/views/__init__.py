from operator import itemgetter

from deploy.config import settings
from deploy.core.http import Request
from deploy.core.mail import Email
from deploy.shortcuts import mail_admins, render_page

__all__ = ('error_404', 'error_500')

def error_404(environ, exception):
	"""View used to display 404 error responses"""

	if environ is None:
		environ = {}

	if not settings.DEBUG and settings.ERROR_404_EMAIL:

		subject = 'Page Not Found: %s' % settings.SITE_NAME
		msg = exception.get_msg()

		if settings.ERROR_EMAIL_OVERIDE is not None:
			try:
				email = Email(settings.SITE_EMAIL, subject, msg)
				email.send(
					[email for name, email in settings.ERROR_EMAIL_OVERIDE],
					host=settings.EMAIL_HOST,
					username=settings.EMAIL_USERNAME,
					password=settings.EMAIL_PASSWORD
				)
			except Exception:
				# See note below in error_500 view...
				pass
		else:
			mail_admins(subject, msg)

	request = Request(environ)

	return render_page(request, '404.html', {}, status=404)

def error_500(environ, exception):
	"""View used to display 500 error responses"""

	if environ is None:
		environ = {}

	request = Request(environ)

	if not settings.DEBUG and settings.ERROR_500_EMAIL:

		subject = 'Website Error: %s' % settings.SITE_NAME

		msg = 'Website Error: %s\n' % settings.SITE_NAME
		msg += '-' * 50
		msg += '\n'
		msg += exception.get_msg()
		msg += '\n'
		msg += '-' * 50

		try:
			msg += '\nEnviron:\n%s' % '\n'.join(
				['%s: %s' % (k, unicode(v)) for k, v in sorted(environ.items(), key=itemgetter(1))]
			)
		except Exception, e:
			msg += '\nEnviron: Unable to display. %s' % e

		msg += '-' * 50
		msg += '\n'

		try:
			msg += '\GET:\n%s' % '\n'.join(
				['%s: %s' % (k, unicode(v)) for k, v in sorted(request.GET.items(), key=itemgetter(1))]
			)
		except Exception, e:
			msg += '\nGET: Unable to display. %s' % e

		try:
			msg += '\nPOST:\n%s' % '\n'.join(
				['%s: %s' % (k, unicode(v)) for k, v in sorted(request.POST.items(), key=itemgetter(1))]
			)
		except Exception, e:
			msg += '\nPOST: Unable to display. %s' % e

		if settings.ERROR_EMAIL_OVERIDE is not None:
			try:
				email = Email(settings.SITE_EMAIL, subject, msg)
				email.send(
					[email for name, email in settings.ERROR_EMAIL_OVERIDE],
					host=settings.EMAIL_HOST,
					username=settings.EMAIL_USERNAME,
					password=settings.EMAIL_PASSWORD
				)
			except Exception:
				# This exception has been added as on occuasion a 500 error which
				# causes an email to go out will then fail to send which causes an
				# unchecked internal server error. The view is its better to show the
				# user a formatted 500 than the internal error.
				# See ticket #17
				pass
		else:
			mail_admins(subject, msg)

	return render_page(request, '500.html', {}, status=500)

