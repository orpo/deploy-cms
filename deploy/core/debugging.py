"""Tools for debugging Deploy Applications"""

try:
	from webob.request import DisconnectionError
except ImportError:
	# This is a work around for older versions of webob
	# which do not have the DisconnectionError. In this
	# case we just replace with a bare exception to catch
	# any faults from GET/POST output - it's important
	# we don't let anything fall through at this stage.
	DisconnectionError = Exception

from deploy import __version__
from deploy.config import settings
from deploy.core.http import Request, Response
from deploy.core.templates import TemplateManager
from deploy.utils.encoding import to_unicode

def debug(environ, exception, additional_args=None):
	"""View used to display debug responses"""

	# Sanity check
	assert settings.DEBUG, 'Attempt to display debug information when debug is not set!'

	request = Request(environ)

	# Order settings for output
	debug_settings = settings.get_current_settings()
	keys = debug_settings.keys()
	keys.sort()
	ordered_settings = [(key, '%s' % str(debug_settings[key])) for key in keys]

	# Get params safely
	try:
		get_params = request.GET
		post_params = request.POST
	except DisconnectionError:
		get_params = {}
		post_params = {}

	template_args = {
		'version': __version__,
		'time': exception.created.strftime('%H:%M:%S %d/%m/%Y'),
		'msg': exception.error_value,
		'settings': ordered_settings,
		'error_request': {
			'url': environ['PATH_INFO'] if 'PATH_INFO' in environ else '',
			'complete_url': environ['HTTP_HOST'] if 'HTTP_HOST' in environ else '',
			'method': environ['REQUEST_METHOD'] if 'REQUEST_METHOD' in environ else '',
			'status': exception.status,
			'GET': get_params,
			'POST': post_params,
			'HEADERS': request.headers,
		}
	}

	if additional_args is not None:
		template_args.update(additional_args)

	if isinstance(exception.error_value, Exception):
		template_args['value'] = unicode(exception.error_value)
		template_args['type'] = exception.error_value.__class__.__name__

	else:
		template_args['value'] = repr(exception.error_value)
		template_args['type'] = exception.__class__.__name__

	# Setup the template manager
	template_manager = TemplateManager(request, engine='jinja')
	template_manager.parse(DEBUG_TEMPLATE)
	source = template_manager.render(data=template_args)

	# Setup the response
	response = Response(content_type='text/html', request=request)
	response.status = exception.status
	response.unicode_body = to_unicode(source)

	return response

# Template is here so we can always get it.
DEBUG_TEMPLATE = """<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<title>DeployCMS Debug</title>
	<style>
		body {
			color: #4a4741;
			font: 110% Helvetica, sans-serif;
			margin: 0;
			padding: 0;
		}
		h1 {
			font-size: 36px;
			margin-bottom: 2px;
		}
		h2 {
			color: #515258;
			font-size: 24px;
			font-weight: normal;
			margin: 0 0 15px;
		}
		h6 {
			border-bottom: 1px solid #e8e8e8;
			font-size: 14px;
			margin-bottom: 6px;
			padding-bottom: 3px;
			text-indent: 4px;
		}
		blockquote {}
		#overview, #traceback, #environment, #note {
			padding: 10px 45px;
		}
		#overview {
			background: #f7ddf3;
			border-bottom: 1px solid #e4e4e4;
			padding: 10px 45px 30px;
		}
		#overview p {
			font-size: 14px;
			margin: 0 0 8px;
		}
		#traceback {
			background: #f6f6f7;
			border-bottom: 1px solid #e4e4e4;
			max-width: 100%;
			/*overflow: scroll;*/
			overflow-y: hidden;
		}
		#environment {
			background: #f3f3f3;
			border-bottom: 1px solid #e4e4e4;
			max-width: 100%;
			padding-bottom: 45px;
			overflow-y: hidden;
		}
		#note {
			font-size: 12px;
		}
		.toggle {
			color: #d17bc3;
			font-size: 15px;
			font-weight: bold;
			text-decoration: none;
		}
		.show { display: block }
		.noshow { display: none }
	</style>
	<script>
		function init_toggle() {
			var ids = ['get', 'post', 'headers', 'settings'];
			for(var i=0;i < ids.length;i++) {
				var id_ = ids[i];
				var el = document.getElementById(id_);
				el.childNodes[3].className = 'noshow';
			}
		}
		function toggle(id_) {
			var el = document.getElementById(id_);
			if(el.childNodes[3].className == 'show') {
				el.childNodes[3].className = 'noshow';
			}
			else if(el.childNodes[3].className == 'noshow') {
				el.childNodes[3].className = 'show';
			}
		}
	</script>
</head>
<body onload="init_toggle()">
	<div id="overview">
		<h1>{{ type }} at {{ error_request['url'] }}</h1>
		<h2>{{ value }}</h2>
		<p><strong>Request Method:</strong> {{ error_request['method'] }}</p>
		<p><strong>Request URL:</strong> {{ error_request['complete_url'] }}</p>
		<p><strong>Exception Value:</strong> {{ value }}</p>
		<p><strong>Server Time:</strong> {{ time }}</p>
	</div>
	{% if error_request['status'] == 500 %}
	<div id="traceback">
<blockquote><code><pre>{{ traceback }}</pre></code></blockquote>
	</div>
	<div id="environment">
		<div id="get">
			<h6><a href="#" class="toggle" onclick="toggle('get');return false;">+</a> GET</h6>
			{% if error_request['GET'] %}
<blockquote><code><pre>{% for k, v in error_request['GET'].items() %}
{{ k }}: {{ v }},
{% endfor %}</pre></code></blockquote>
			{% else %}
			<p>{}<p>
			{% endif %}
		</div>
		<div id="post">
			<h6><a href="#" class="toggle" onclick="toggle('post');return false;">+</a> POST</h6>
			{% if error_request['POST'] %}
<blockquote><code><pre>{% for k, v in error_request['POST'].items() %}
{{ k }}: {{ v }},
{% endfor %}</pre></code></blockquote>
			{% else %}
			<p>{}<p>
			{% endif %}
		</div>
		<div id="headers">
			<h6><a href="#" class="toggle" onclick="toggle('headers');return false;">+</a> HTTP Headers</h6>
			{% if error_request['HEADERS'] %}
<blockquote><code><pre>{% for k, v in error_request['HEADERS'].items() %}
{{ k }}: {{ v }},
{% endfor %}</pre></code></blockquote>
			{% else %}
			<p>{}<p>
			{% endif %}
		</div>
		<div id="settings">
			<h6><a href="#" class="toggle" onclick="toggle('settings');return false;">+</a> Settings</h6>
<blockquote><code><pre>{% for setting_name, setting_value in settings %}
{{ setting_name }}: <em>"{{ setting_value }}"</em>,
{% endfor %}</pre></code></blockquote>
		</div>
	</div>
			{% endif %}

{% if error_request['status'] == 404 %}
	<div id="environment">
		<div id="urls">
			<h6>URLS</h6>
			{% if urls %}
			<blockquote>
<code><pre>
{% for url in urls %}
{{ url }}
{% endfor %}
</pre></code></blockquote>
			{% else %}
			<p>()<p>
			{% endif %}
		</div>
	</div>
{% endif %}

	<p id="note"><strong>DeployCMS v{{ version }}</strong> - Debugging is currently set to
	"True" in site settings.</p>
</body>
</html>"""
