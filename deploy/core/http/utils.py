"""Utilities functions relating to the HTTP protocol"""

from email.Utils import formatdate

__all__ = ('STATUS_CODE_TEXT', 'http_date')

HTTP_VERBS = (
	'GET',
	'POST',
	'HEAD',
	# @@ support for these verbs?
	#'PUT',
	#'DELETE',
	#'TRACE',
	#'CONNECT',
	#'OPTIONS',
)

# See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
STATUS_CODE_TEXT = {
	100: 'CONTINUE',
	101: 'SWITCHING PROTOCOLS',
	200: 'OK',
	201: 'CREATED',
	202: 'ACCEPTED',
	203: 'NON-AUTHORITATIVE INFORMATION',
	204: 'NO CONTENT',
	205: 'RESET CONTENT',
	206: 'PARTIAL CONTENT',
	300: 'MULTIPLE CHOICES',
	301: 'MOVED PERMANENTLY',
	302: 'FOUND',
	303: 'SEE OTHER',
	304: 'NOT MODIFIED',
	305: 'USE PROXY',
	306: 'RESERVED',
	307: 'TEMPORARY REDIRECT',
	400: 'BAD REQUEST',
	401: 'UNAUTHORIZED',
	402: 'PAYMENT REQUIRED',
	403: 'FORBIDDEN',
	404: 'NOT FOUND',
	405: 'METHOD NOT ALLOWED',
	406: 'NOT ACCEPTABLE',
	407: 'PROXY AUTHENTICATION REQUIRED',
	408: 'REQUEST TIMEOUT',
	409: 'CONFLICT',
	410: 'GONE',
	411: 'LENGTH REQUIRED',
	412: 'PRECONDITION FAILED',
	413: 'REQUEST ENTITY TOO LARGE',
	414: 'REQUEST-URI TOO LONG',
	415: 'UNSUPPORTED MEDIA TYPE',
	416: 'REQUESTED RANGE NOT SATISFIABLE',
	417: 'EXPECTATION FAILED',
	500: 'INTERNAL SERVER ERROR',
	501: 'NOT IMPLEMENTED',
	502: 'BAD GATEWAY',
	503: 'SERVICE UNAVAILABLE',
	504: 'GATEWAY TIMEOUT',
	505: 'HTTP VERSION NOT SUPPORTED',
}

def http_date(epoch_seconds=None):
	"""
	Formats the time to match the RFC1123 date format as specified by HTTP
	RFC2616 section 3.3.1.

	Accepts a floating point number expressed in seconds since the epoch, in
	UTC - such as that outputted by time.time(). If set to None, defaults to
	the current time.

	Outputs a string in the format 'Wdy, DD Mon YYYY HH:MM:SS GMT'.
	"""
	# This is taken from v1.1 (django.utils.http.httd_date) (c) Django Foundation
	rfcdate = formatdate(epoch_seconds)
	return '%s GMT' % rfcdate[:25]