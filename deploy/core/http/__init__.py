"""A 'Friendly' interface to HTTP transactions"""

from webob import Request as woRequest, Response as woResponse

from deploy.config import settings

__all__ = ('Request', 'Response')

class Request(woRequest):

	# Mode is used internally to determine if the
	# request is made in LIVE or EDIT mode
	MODE = None

	def __init__(self, environ, *args, **kwargs):

		charset = settings.ENCODING

		if 'charset' in kwargs:
			charset = kwargs['charset']
			del kwargs['charset']

		super(Request, self).__init__(environ, charset=charset, *args, **kwargs)

		self.mode = 'LIVE'
		self.cache = False

		if 'com.deploycms.cms.mode' in environ:
			self.mode = environ['com.deploycms.cms.mode']

		if 'com.deploycms.db.conn' in environ:
			self.db = environ['com.deploycms.db.conn']
		else:
			self.db = None

	def as_dict(self):
		return {}

class Response(woResponse):

	def __init__(self, *args, **kwargs):

		charset = settings.ENCODING

		if 'charset' in kwargs:
			charset = kwargs['charset']
			del kwargs['charset']

		super(Response, self).__init__(charset=charset, *args, **kwargs)

	def as_dict(self):
		return {}
