"""HTTP Exceptions"""

from deploy.config import settings
from deploy.core.debugging import debug as debugger
from deploy.core.exceptions import DeployBaseException
from deploy.core.urls import __resolver__
from deploy.utils.importing import import_object

class HTTPError(DeployBaseException):
	"""Standard Exception Class for HTTP Errors"""

	def __init__(self, error_value, status=None):
		self.status = status
		super(HTTPError, self).__init__(error_value)

	def get_response(self, environ=None):
		"""General response to HTTP error"""

		if settings.DEBUG:
			return debugger(environ, self)

		response = Response(content_type='text/html')
		response.status = self.status or 500
		response.unicode_body = '<h1>%s</h1>\n<p>%r</p>' % (self.__class__.__name__, self.error_value)

		return response

	def get_real_error(self):
		# Very often HTTP errors are wrappers for other types of errors,
		# This utility will get the actual error

		if isinstance(self.error_value, DeployBaseException):
			return self.error_value

		return self

class HTTPForbidden(HTTPError):
	def __init__(self, error_value='Access to the requested resource is forbidden by server configuration', status=403):
		super(HTTPForbidden, self).__init__(error_value, status=status)

class PageNotFound(HTTPError):

	def __init__(self, path, error_value='The requested page could not be found.', status=404, available_urls=None):
		self.path = path
		self.available_urls = available_urls
		super(PageNotFound, self).__init__(error_value, status=status)

	def get_msg(self, more=None):
		"""Override the default exception mail message"""

		msg = []
		msg.append('404 The following URL was not found on the server: "%s"' % self.path)

		if more:
			msg.append(more)

		return '\n'.join(msg)

	def get_response(self, environ=None):
		"""General response to HTTP error"""

		if settings.DEBUG:
			return debugger(environ, self, additional_args={'urls': __resolver__.get_all_urls()})

		if self.status in settings.ERROR_HANDLERS:
			error_handler = import_object(settings.ERROR_HANDLERS[self.status])
			return error_handler(environ, self)

		return super(PageNotFound, self).get_response(environ)

class HTTPMethodNotAllowed(HTTPError):
	def __init__(self, allowed_methods, error_value='The requested HTTP Method is not allowed.', status=405):
		self.allowed_methods = allowed_methods
		error_value = '%s: %s' % unicode(self.allowed_methods)
		super(HTTPMethodNotAllowed, self).__init__(error_value, status=status)

	def get_response(self, environ=None):
		"""General response to HTTP error"""

		if settings.DEBUG:
			return debugger(environ, self)

		response = Response(content_type='text/html')
		response.headers['Allow'] = ', '.join(self.allowed_methods)
		response.status = self.status
		response.unicode_body = '<h1>%s</h1>\n<p>%r</p>' % (self.__class__.__name__, self.error_value)

		return response

class InternalServerError(HTTPError):

	def __init__(self, error_value='An unexpected error has occured.', status=500):
		super(InternalServerError, self).__init__(error_value, status=status)

	def get_response(self, environ=None):
		"""General response to HTTP error"""

		if settings.DEBUG:
			real_error = self.get_real_error()
			return debugger(environ, self, additional_args={'traceback': real_error.get_traceback()})

		if self.status in settings.ERROR_HANDLERS:
			error_handler = import_object(settings.ERROR_HANDLERS[self.status])
			return error_handler(environ, self)

		return super(InternalServerError, self).get_response(environ)
