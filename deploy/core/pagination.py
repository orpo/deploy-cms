"""Tools for Paginating objects"""

from deploy.core.exceptions import ApplicationException

__all__ = ('PageNumberNotAnInteger', 'InvalidPage', 'EmptyPage', 'Paginator', 'Page')

class PaginatorCountError(ApplicationException): pass
class PageNumberNotAnInteger(ApplicationException): pass
class InvalidPage(ApplicationException): pass
class EmptyPage(ApplicationException): pass

class Paginator(object):
	"""Simple pagination class for Deploy"""

	def __init__(self, objects, items_per_page=10, orphans=0, columns=None, count=None):
		self.objects = objects
		self.items_per_page = items_per_page
		self.orphans = orphans
		self.columns = columns if columns is not None else []
		self._count = count

	@property
	def item_count(self):
		"""Return the number of rows to paginate"""

		if self._count is not None:
			return self._count

		elif isinstance(self.objects, (list, tuple)):
			self._count = len(self.objects)
			return self._count

		elif hasattr(self.objects, 'count'):
			# This is used typically for Storm resultsets
			self._count = self.objects.count()
			return self._count

		elif hasattr(self.objects, 'rowcount'):
			self._count = self.objects.rowcount
			return self._count

		raise PaginatorCountError('Paginator can not work out list count!')

	@property
	def page_count(self):
		"""Based on the number rows per page, return the number of pages."""

		# Check there are items
		if not self.item_count > 0:
			return 0

		# Check that the number of items isn't equal to the number of orphans as
		# this would cause the first page to be lost
		if (self.item_count - self.orphans) == 0:
			return 1

		page_count = (self.item_count - self.orphans) / self.items_per_page

		if (self.item_count - self.orphans) % self.items_per_page:
			page_count = max(1, page_count + 1)

		return page_count

	@property
	def page_range(self):
		"""Return a range of pages"""

		return range(1, self.page_count + 1)

	def page(self, page_number):
		"""Return a page of paginated data"""

		try:
			page_number = int(page_number)
		except ValueError:
			raise PageNumberNotAnInteger('The page number "%s" is not an valid integer' % page_number)

		if page_number < 1:
			raise InvalidPage('Page number specified is less than 1')

		if page_number > self.page_count:
			raise InvalidPage('Page number specified is greater than page count (%s)' % self.page_count)

		page_number = max(page_number, 1)
		page_number = min(page_number, self.page_count)

		first = (page_number - 1) * self.items_per_page
		last = first + self.items_per_page

		if page_number == self.page_count:
			last += self.orphans

		items = self.objects[first:last]

		if self.item_count == 0:
			raise EmptyPage()

		return Page(page_number, self.page_count, first, last, self.item_count, items, self.columns)

class Page(object):
	"""A object representation of a 'page' of objects."""

	def __init__(self, number, page_count, first, last, total_item_count, items, columns):
		self.number = number
		self.first = first
		self.last = last
		self.page_count = page_count
		self.total_item_count = total_item_count
		self.items = items
		self.columns = columns

	@property
	def next_page_number(self):
		"""Return the number of the next page"""
		return min(self.number + 1, self.page_count)

	@property
	def previous_page_number(self):
		"""Return the number of the next page"""
		return max(self.number - 1, 1)

	def has_next_page(self):
		"""Return True if there is a next page"""
		return self.number < self.page_count

	def has_previous_page(self):
		"""Return True if there is a previous page"""
		return self.number > 1

	def has_other_pages(self):
		"""Return True if there is a next or previous page"""
		return self.has_next_page() or self.has_previous_page()

	def to_dict(self):
		"""Return the page information as a dictionary"""
		page = {
			'number': self.number,
			'numbers': range(1, self.page_count + 1),
			'next_page_number': self.next_page_number,
			'previous_page_number': self.previous_page_number,
			'page_count': self.page_count,
			'has_next_page': self.has_next_page(),
			'has_previous_page': self.has_previous_page(),
			'has_other_pages': self.has_other_pages(),
			'total_item_count': self.total_item_count,
			'columns': self.columns,
		}

		items = []

		for item in self.items:
			if hasattr(item, 'to_dict'):
				items.append(item.to_dict())
			else:
				items.append(dict(zip(self.columns, item)))

		page['items'] = items

		return page
