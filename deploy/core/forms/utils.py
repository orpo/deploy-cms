from storm.variables import BoolVariable, IntVariable, FloatVariable, DecimalVariable, \
	RawStrVariable, UnicodeVariable, DateTimeVariable, DateVariable, TimeVariable, \
	TimeDeltaVariable, EnumVariable, UUIDVariable, PickleVariable, ListVariable

from deploy.core.forms.fields import TextField, HiddenField, BooleanField, \
	TextareaField, PasswordField

__all__ = ('get_field_for_property', 'get_pretty_field_name')

def get_field_for_property(prop):
	"""Match a ORM Property type to a deploy form Field type"""

	field_property_map = {
		BoolVariable: 		BooleanField,
		IntVariable:		TextField,
		FloatVariable:		TextField,
		DecimalVariable:	TextField,
		RawStrVariable:		TextareaField,
		UnicodeVariable:	TextField,

		#DateTime:
		#Date:
		#Time:
		#TimeDelta:
		#UUID:
		#Enum:
		#Pickle:
		#List:
	}

	cls_type = prop.variable_factory()
	valid_types = tuple(field_property_map.keys())

	#if [var for var in field_property_map.keys() if isinstance(cls_type, var)]:
	if not isinstance(cls_type, valid_types):
		return None

	return field_property_map[cls_type.__class__]

def get_pretty_field_name(field_name):
	"""Return a 'pretty' field name. e.g a_field_name == A Field Name"""
	return field_name.replace('_', ' ').capitalize()
