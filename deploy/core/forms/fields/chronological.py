import datetime

from deploy.core.forms.fields import Field

class DateField(Field):
	"""A date input Field."""

	def __init__(self, label='', value='', format='%Y-%m-%d', *args, **kwargs):
		self._format = format
		super(DateField, self).__init__(label=label, value=value, **kwargs)

	def populate(self, value, form=None):
		# Check if it's a date
		if isinstance(value, (datetime.datetime, datetime.date)):
			self._value = value

		else:
			# Not a date,
			try:
				self._value = datetime.datetime.strptime(value, self._format).date()
			except (TypeError, ValueError):
				self._value = None

class TimeField(Field):
	"""A time input Field."""
	pass

class DateTimeField(Field):
	"""A date time input Field."""
	pass
