from deploy.config import settings
from deploy.core.forms.fields import Field
from deploy.core.forms.validators import RequiredJSONValidator, MaxLengthValidator
from deploy.utils.encoding import to_unicode

__all__ = ('ChoiceField', 'ChoiceOptionField', 'ModelChoiceField', 'TokenizerField')

class ChoiceField(Field):
	"""A choice field"""

	def __init__(self, label='', options=None, multiple=False, default=None, **kwargs):
		self._options = []

		self._multiple = multiple

		if options is not None:
			for option in options:
				self.add_option(option)

		if default:
			self.set_value(default)

		super(ChoiceField, self).__init__(label=label, **kwargs)

	def __iter__(self):
		"""Generator for Form, allows iteration over each option in the select field"""
		for option in self._options:
			yield option

	def get_multiple(self):
		return self._multiple

	def get_options(self):
		return self._options

	def add_option(self, option):
		"""Add a new option to the select field"""
		if isinstance(option, ChoiceOptionField):
			self._options.append(option)

		elif isinstance(option, (list, tuple)):
			new_option = ChoiceOptionField(option[1], option[0])
			self._options.append(new_option)

		else:
			raise TypeError('Unknown Option Type "%r" sent to ChoiceField!' % option)

	def get_value(self, default=''):
		values = []

		for option in self._options:
			if option.is_selected() and self._multiple:
				values.append(option.get_value())

			elif option.is_selected():
				return option.get_value()

		if not self._multiple:
			if len(values) == 0:
				return ''
			return values[0]

		return values or default

	def set_value(self, v):
		self._value = v

		for option in self._options:
			if self._multiple:
				vs = [to_unicode(val) for val in v]
				if option.get_value() in vs:
					option.toggle_selected(True)

			elif not self._multiple:
				if option.get_value() == to_unicode(v):
					option.toggle_selected(True)

	value = property(get_value, set_value)

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = super(ChoiceField, self).to_dict()
		field['options'] = [option.to_dict() for option in self.get_options()]
		field['multiple'] = self._multiple

		return field

class ChoiceOptionField(Field):
	"""Choice Option Field"""

	def __init__(self, label, value, selected=False):
		self._label = label
		self._value = value
		self._selected = selected

	def get_label(self):
		return self._label

	def get_value(self):
		return self._value

	def is_selected(self):
		return self._selected

	def toggle_selected(self, value=True):
		self._selected = value

	def to_dict(self):
		return {
			'label': self._label,
			'value': self._value,
			'selected': self._selected
		}

class ModelChoiceField(ChoiceField):

	def __init__(self, label='', default=None, model=None, model_filter=None, model_order=None,
				key_value=('id', 'name'), multiple=False, *args, **kwargs):
		"""Select Field that is dynamically created from the records of a specified Model type.
		Note: You must call build, passing a request/db handle, to generate the options."""

		self._model = model
		self._filter = model_filter
		self._order_by = model_order
		self._key_value = key_value

		self._delayed_default = default
		self._options = []
		self._multiple = multiple

		# As relations are specified by an id by default, we must convert to an
		# integer when populating the value
		if not kwargs.has_key('prepare'):
			kwargs['prepare'] = int

		super(ModelChoiceField, self).__init__(label=label, options=self._options,
												multiple=multiple, *args, **kwargs)

		# Overload the default type (ModelChoice)
		self._type = 'Choice'

	def __deepcopy__(self, memo):
		# @@ TODO: Add explanation regarding deepcopy here (http://bugs.python.org/issue1515)

		args = {
			'type': self._type,
			'id': self._id,
			'value': self._value,
			'name': self._name,
			'label': self.label,
			'help': self.help,
			'default': self._delayed_default,
			'attribs': self.attribs,
			'placeholder': self.placeholder,
			'validators':  self.validators,
			'realtime': self.realtime,
			'classes': self.classes,
			'autofocus': self.autofocus,
			'autocomplete': self.autocomplete,
			'prepare': self._prepare,
			'populate': self._populate,
			'multiple': self._multiple,

			'model': self._model,
			'model_filter': self._filter,
			'model_order': self._order_by,
			'key_value': self._key_value,
		}

		return self.__class__(**args)

	def build(self, request=None):
		"""Build the model select options"""

		records = []

		# Build the dynamic select options
		if self._filter:
			records = request.db.find(self._model, self._filter)
		else:
			records = request.db.find(self._model)

		if self._order_by:
			records = records.order_by(self._order_by)

		for arg in records:
			key = getattr(arg, self._key_value[0]) if not callable(getattr(arg, self._key_value[0])) else getattr(arg, self._key_value[0])()
			value = getattr(arg, self._key_value[1]) if not callable(getattr(arg, self._key_value[1])) else getattr(arg, self._key_value[1])()

			self.add_option((unicode(key), unicode(value)))

		return True

class TokenizerField(Field):
	"""An tokenizer field"""
	def __init__(self, label='', value=None, datalist=None, datalist_url=None, validators=[RequiredJSONValidator(),], allow_create=False, *args, **kwargs):
		self.datalist = datalist
		self.datalist_url = datalist_url
		self.allow_create = allow_create

		if value is None:
			value = []

		super(TokenizerField, self).__init__(label=label, value=value, validators=validators, **kwargs)

		self._type = 'Tokenizer'

	def get_value(self, default=None):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		# Neeed to look at this, but currently this provides a way to specify
		# the mask using the standard controllers (albeit through a custom
		# property). It also matches the autocomplete JS's output format which
		# divides value|mask the same way.
		if '|' in v:
			self._value = v.split('|')[1]
			self.mask_value = v.split('|')[0]
		else:
			self._value = v
			self.mask_value = v

	value = property(get_value, set_value)

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = super(TokenizerField, self).to_dict()
		field['datalist'] = self.datalist
		field['datalist_url'] = self.datalist_url
		field['allow_create'] = self.allow_create

		return field