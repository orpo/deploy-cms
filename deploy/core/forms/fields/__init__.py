"""Deploy Base Form fields"""

import re
from flaskext.htmlbuilder import html, render

from deploy.core.forms.validators import RequiredValidator
from deploy.config import settings
from deploy.utils.strings import mark_safe

__all__ = (
	# Base
	'Fieldset', 'Field',

	# Text
	'TextField', 'TextareaField', 'PasswordField', 'EmailField', 'HiddenField', 'SlugField',

	# Numerical
	'IntField', 'FloatField', 'CurrencyField',

	# Select
	'SelectField', 'OptionField', 'ModelSelectField',

	# Choice
	'ChoiceField', 'ChoiceOptionField', 'ModelChoiceField',

	# Datetime
	'DateField', 'TimeField', 'DateTimeField',

	# Other
	'DisplayOnlyField', 'BooleanField'
)

class Fieldset(object):

	def __init__(self, legend, fields, classes=None, attribs=None):
		self.legend = legend
		self.fields = fields
		self.attribs = attribs

		self.classes = classes if classes is not None else []
		self.attribs = attribs if attribs is not None else {}

	def __iter__(self):
		for field in self.fields:
			yield field

	def has_field(self, name):
		return name in self.fields

	def to_dict(self):
		fieldset = {
			'legend': self.legend,
			'fields': self.fields,
			'attribs': self.attribs,
		}

		return fieldset

class Field(object):

	_counter = 0

	def __init__(self, label='', value=None, placeholder='', validators=[RequiredValidator(),],
				realtime=True, id=None, attribs=None, classes=None, autofocus=False,
				autocomplete=True, default=None, prepare=None, populate=None, help=None, *args, **kwargs):

		self._type = unicode(self.__class__.__name__.replace('Field', ''))
		self._id = id
		self._value = value
		self._name = kwargs['name'] if 'name' in kwargs else None
		self._error = None
		self._default = default

		if prepare is not None:
			assert callable(prepare), 'Prepare function must be callable!'
		self._prepare = prepare

		if populate is not None:
			assert callable(populate), 'Populate function must be callable!'
		self._populate = populate

		self.label = args[0] if len(args) > 1 else label
		self.help = help
		self.placeholder = placeholder
		self.validators = validators
		self.realtime = realtime

		self.autofocus = autofocus
		self.autocomplete = autocomplete

		self.classes = classes if classes is not None else []
		self.attribs = attribs if attribs is not None else {}

		# Counter is used to track the order that fields are created in the Form
		self._counter = Field._counter
		Field._counter += 1

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = {
			'type': self._type,
			'label': self.label,
			'name': self.get_name(),
			'value': self.get_value(),
			'placeholder': self.placeholder,
			'required': self.is_required(),
			'realtime': self.realtime,
			'attribs': self.attribs,
			'classes': self.classes,
			'autofocus': self.autofocus,
			'autocomplete': self.autocomplete,
			'help': self.help,
		}

		return field

	def is_required(self):
		"""Utility to check if field is required"""

		for validator in self.validators:
			if isinstance(validator, RequiredValidator) or issubclass(validator, RequiredValidator):
				return True

		return False

	def get_id(self):
		_id = self._id
		if not _id:
			_id = self.name.lower()
			_id = _id.replace(' ', '-')
			_id = re.sub(r'\W', '', _id)
		return _id

	def set_id(self, v):
		self._id = v

	id = property(get_id, set_id)

	def get_name(self):
		return self._name

	def set_name(self, v):
		self._name = v

	name = property(get_name, set_name)

	def get_value(self, default=None):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		if v is not None:
			self._value = unicode(v)

	value = property(get_value, set_value)

	def get_error(self):
		return self._error

	def has_error(self):
		return True if self._error else False

	def set_error(self, v):
		self._error = v

	error = property(get_error, set_error)

	def build(self, request=None):
		"""Build the field. This method allows for fields to be built dynamically at
		runtime."""
		return True

	def clear(self):
		"""Clear the Fields value"""
		self._value = None
		self.error = None

	def prepare(self, form=None):
		"""Prepares the field value. This is called by the form validate method to
		populate the validated data dictionary. By default it does nothing, but a hook
		'prepare' is available to change this behaviour when initialising the field."""

		if self._prepare is not None:
			return self._prepare(self._value)

		# No hook or overload, the default is to return the value
		return self._value

	def populate(self, value, form=None):
		"""Populate is a special method for populating the internal field value, it allows
		for custom hooks to format the field value."""

		# Values may come through as a list - even single values as getall may of been called
		# by the form.populate method. We normalize the value here.
		if isinstance(value, (list, tuple)) and (hasattr(self, '_multiple') and not self._multiple):
			value = value[0]

		if self._populate is not None:
			self._value = self._populate(value)
		else:
			# No hook or overload, the default is to call set_value
			self.value = value

	def validate(self, form=None, request=None):
		"""Validate the Fields value"""
		valid = True

		for validator in self.validators:
			result = validator.validate(self, self.value, form=form, request=request)

			if not result.success:
				self.set_error(result.error_message)
				valid = False
				break

		return valid

	def is_required(self):
		"""Utility function to test if field is required"""

		for validator in self.validators:
			if isinstance(validator, RequiredValidator):
				return True

		return False

	def to_html(self):
		# Build the default field input node
		input_node = html.input(
			id=self.get_id(),
			type='text',
			name=self.name,
			placeholder=self.placeholder,
			value=self.value or '',
			class_=' '.join(self.classes)
		)

		if self.autofocus:
			input_node._attributes['autofocus'] = 'autofocus'

		if not self.autocomplete:
			input_node._attributes['autocomplete'] = 'off'

		if self.is_required():
			input_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.div()(
			html.label(for_=self.get_id())(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))

# Import the fields to give a cleaner import interface for developers
from deploy.core.forms.fields.choice import ChoiceField, ChoiceOptionField, ModelChoiceField, TokenizerField
from deploy.core.forms.fields.chronological import DateField, TimeField, DateTimeField
from deploy.core.forms.fields.numerical import IntField, FloatField, CurrencyField
from deploy.core.forms.fields.other import BooleanField, DisplayOnlyField, MapField
from deploy.core.forms.fields.select import SelectField, OptionField, ModelSelectField
from deploy.core.forms.fields.text import AutoCompleteField, TextField, TextareaField, PasswordField, \
	EmailField, HiddenField, SlugField
