from flaskext.htmlbuilder import html, render

from deploy.config import settings
from deploy.core.forms.fields import Field
from deploy.core.forms.fields.text import TextField
from deploy.utils.strings import mark_safe

class BooleanField(Field):
	"""A boolean input Field."""

	def __init__(self, label='', validators=[], default=False, **kwargs):
		super(BooleanField, self).__init__(label=label, validators=validators, default=default, **kwargs)
		self._value = None

	def get_value(self, default=None):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		self._value = bool(v)

	value = property(get_value, set_value)

	def prepare(self, form=None):
		""" """

		value = self._value if self._value is not None else False

		if self._prepare is not None:
			return self._prepare(value)

		# No hook or overload, the default is to return the value
		return value

	def to_html(self):
		input_node = html.input(
			id=self.get_id(),
			type='checkbox',
			name=self.name,
			value='1',
			class_=' '.join(self.classes)
		)

		if self.value:
			input_node._attributes['checked'] = 'checked'

		if self.is_required():
			input_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.p()(
			html.label(for_=self.get_id())(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))

class DisplayOnlyField(Field):
	"""A static field used for displaying fixed data values in a form"""

	def validator_dict(self):
		return {}

	def is_required(self):
		return False

	def get_value(self, default=None):

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		self._value = v

	value = property(get_value, set_value)

	def get_error(self):
		return None

	def has_error(self):
		return False

	def set_error(self, v):
		raise NotImplementedError

	error = property(get_error, set_error)

	def build(self, request=None):
		return True

	def clear(self):
		pass

	def validate(self, form=None, request=None):
		return True

	def is_required(self):
		return False

	def to_html(self):
		# Build the default field input node
		input_node = html.p(
			class_=' '.join(self.classes)
		)(self.value or '')

		# Build the output row for the form
		html_snippet = html.div()(
			html.label(for_=self.name)(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))

class MapField(TextField):
	pass

