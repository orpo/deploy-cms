from flaskext.htmlbuilder import html, render

from deploy.config import settings
from deploy.core.forms.fields import Field
from deploy.core.forms.validators import MaxLengthValidator
from deploy.utils.encoding import to_unicode
from deploy.utils.strings import mark_safe

__all__ = ('SelectField', 'OptionField', 'ModelSelectField')

class SelectField(Field):
	"""A Select Field."""

	def __init__(self, label='', options=None, multiple=False, default=None, *args, **kwargs):
		"""Initialise the Select field. This method overides the base init to accept an
		additional argument. It still accepts the label argument as the first positional
		argument"""

		self._options = []

		self._multiple = multiple

		if options is not None:
			for option in options:
				self.add_option(option)

		if default:
			self.set_value(default)

		super(SelectField, self).__init__(label=label, *args, **kwargs)

	def __iter__(self):
		"""Generator for Form, allows iteration over each option in the select field"""
		for option in self._options:
			yield option

	def get_multiple(self):
		return self._multiple

	def get_options(self):
		return self._options

	def add_option(self, option):
		"""Add a new option to the select field"""
		if isinstance(option, OptionField):
			self._options.append(option)

		elif isinstance(option, (list, tuple)):
			new_option = OptionField(option[1], option[0])
			self._options.append(new_option)

		else:
			raise TypeError('Unknown Option Type "%r" sent to SelectField!' % option)

	def _get_value(self):
		values = []

		for option in self._options:
			if option.is_selected() and self._multiple:
				values.append(option.get_value())

			elif option.is_selected():
				return option.get_value()

		if not self._multiple:
			if len(values) == 0:
				return u''
			return values[0]

		return values

	def get_value(self, default=u''):
		return self._get_value() or default

	def set_value(self, v):
		for option in self._options:
			if option.get_value() == to_unicode(v):
				option.toggle_selected(True)

			elif not self._multiple:
				option.toggle_selected(False)

	value = property(get_value, set_value)

	def prepare(self, form=None):
		""" """

		if self._prepare is not None:
			return self._prepare(self._get_value())

		# No hook or overload, the default is to return the value
		return self._get_value()

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = super(SelectField, self).to_dict()
		field['options'] = [option.to_dict() for option in self.get_options()]

		return field

	def to_html(self):
		# Get the option nodes first
		option_nodes = []

		for option in self._options:
			option_nodes.append(option.to_html())

		select_node = html.select(
			name=self.name,
			id=self.get_id(),
			class_=' '.join(self.classes)
		)(*option_nodes)

		if self._multiple:
			select_node._attributes['multiple'] = 'multiple'

		if self.autofocus:
			select_node._attributes['autofocus'] = 'autofocus'

		if self.is_required():
			select_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.p()(
			html.label(for_=self.get_id())(self.label),
			select_node
		)

		return mark_safe(render(html_snippet))

class OptionField(object):

	def __init__(self, label, value, selected=False):
		self._label = label
		self._value = value
		self._selected = selected

	def get_label(self):
		return self._label

	def get_value(self):
		return self._value

	def is_selected(self):
		return self._selected

	def toggle_selected(self, value=True):
		self._selected = value

	def to_dict(self):
		return {
			'label': self._label,
			'value': self._value,
			'selected': self._selected,
		}

	def to_html(self, return_as_string=False):
		option_node = html.option(
			value=self._value
		)(self._label)

		if self.is_selected():
			option_node._attributes['selected'] = 'selected'

		if not return_as_string:
			return option_node

		return render(option_node)

class ModelSelectField(SelectField):

	def __init__(self, label='', default=None, model=None, model_key=None, model_filter=None,
				model_order=None, key_value=('id', 'name'), *args, **kwargs):
		"""Select Field that is dynamically created from the records of a specified Model type.
		Note: You must call build, passing a request/db handle, to generate the options."""

		self._model = model
		self._key = model_key
		self._filter = model_filter
		self._order_by = model_order
		self._key_value = key_value

		self._delayed_default = default
		self._options = []

		# As relations are specified by an id by default, we must convert to an
		# integer when populating the value
		if not kwargs.has_key('prepare'):
			kwargs['prepare'] = int

		super(ModelSelectField, self).__init__(label=label, options=self._options, *args, **kwargs)

		# Overload the default type (ModelSelect)
		self._type = 'Select'

	def __deepcopy__(self, memo):
		# @@ TODO: Add explanation regarding deepcopy here (http://bugs.python.org/issue1515)

		args = {
			'type': self._type,
			'id': self._id,
			'value': self._value,
			'name': self._name,
			'label': self.label,
			'help': self.help,
			'default': self._delayed_default,
			'attribs': self.attribs,
			'placeholder': self.placeholder,
			'validators':  self.validators,
			'realtime': self.realtime,
			'classes': self.classes,
			'autofocus': self.autofocus,
			'autocomplete': self.autocomplete,
			'prepare': self._prepare,
			'populate': self._populate,
			'multiple': self._multiple,

			'model': self._model,
			'model_key': self._key,
			'model_filter': self._filter,
			'model_order': self._order_by,
			'key_value': self._key_value,
		}

		return self.__class__(**args)

	def build(self, request=None):
		"""Build the model select options"""

		records = []

		# Build the dynamic select options
		if self._filter:
			records = request.db.find(self._model, self._filter)
		else:
			records = request.db.find(self._model)

		if self._order_by:
			records = records.order_by(self._order_by)

		for arg in records:
			key = getattr(arg, self._key_value[0]) if not callable(getattr(arg, self._key_value[0])) else getattr(arg, self._key_value[0])()
			value = getattr(arg, self._key_value[1]) if not callable(getattr(arg, self._key_value[1])) else getattr(arg, self._key_value[1])()

			self.add_option((unicode(key), unicode(value)))

		return True
