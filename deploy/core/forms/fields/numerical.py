"""Numerical field types"""

import decimal

from deploy.config import settings
from deploy.core.forms.fields import Field
from deploy.core.forms.validators import RequiredValidator, IsIntegerValidator, IsFloatValidator

__all__ = ('IntField', 'FloatField', 'CurrencyField')

class IntField(Field):
	"""A text input Field - Expects an Integer"""

	def __init__(self, label='', default=0, validators=[RequiredValidator(), IsIntegerValidator()], *args, **kwargs):
		super(IntField, self).__init__(label=label, validators=validators, **kwargs)
		self._type = 'Number'

	def get_value(self, default=0):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		elif default is not None:
			return default

		return self._default

	def set_value(self, v):
		try:
			self._value = int(v)
		except (ValueError, TypeError), e:
			self._value = None

	value = property(get_value, set_value)

class FloatField(Field):
	"""A text input Field - Expects an Float"""

	def __init__(self, label='', default=0.0, validators=[RequiredValidator(), IsFloatValidator()], *args, **kwargs):
		super(FloatField, self).__init__(label=label, validators=validators, **kwargs)
		self._type = 'Number'

	def get_value(self, default=0.0):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		elif default is not None:
			return default

		return self._default

	def set_value(self, v):
		"""Set the value of the field"""

		try:
			self._value = float(v)
		except (ValueError, TypeError), e:
			self._value = None

	value = property(get_value, set_value)

class CurrencyField(FloatField):
	"""Utility field type for use with Currency values"""

	def prepare(self, form=None):
		if self._value:
			return int(decimal.Decimal(self._value) * 100)

		return None

	def populate(self, value, form=None):
		if isinstance(value, int):
			self._value = u'%.2f' % (decimal.Decimal(value) / 100)
		else:
			self._value = value
