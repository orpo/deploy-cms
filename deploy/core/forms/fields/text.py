from flaskext.htmlbuilder import html, render

from deploy.config import settings
from deploy.utils.formatting import slugify

from deploy.core.exceptions import ApplicationException
from deploy.core.forms.fields import Field
from deploy.core.forms.validators import MaxLengthValidator, RequiredValidator
from deploy.utils.strings import mark_safe

__all__ = ('TextField', 'TextareaField', 'PasswordField',
	'EmailField', 'HiddenField', 'SlugField')


class TextField(Field):
	"""A text input Field."""

	def __init__(self, label='', value=u'', *args, **kwargs):
		super(TextField, self).__init__(label=label, value=value, **kwargs)

	@property
	def max_length(self):
		"""Special property on text fields to access the allowed max length"""

		for validator in self.validators:
			if isinstance(validator, MaxLengthValidator):
				return validator.length

		return None


class TextareaField(TextField):
	"""A textarea Field."""

	def to_html(self):
		input_node = html.textarea(
			id=self.get_id(),
			name=self.name,
			placeholder=self.placeholder,
			class_=' '.join(self.classes)
		)(self.value or '')

		if self.autofocus:
			input_node._attributes['autofocus'] = 'autofocus'

		if not self.autocomplete:
			input_node._attributes['autocomplete'] = 'off'

		if self.is_required():
			input_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.p()(
			html.label(for_=self.get_id())(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))


class PasswordField(TextField):
	"""A password input Field."""

	def to_html(self):
		input_node = html.input(
			id=self.get_id(),
			type='password',
			name=self.name,
			placeholder=self.placeholder,
			class_=' '.join(self.classes)
		)

		if self.autofocus:
			input_node._attributes['autofocus'] = 'autofocus'

		if not self.autocomplete:
			input_node._attributes['autocomplete'] = 'off'

		if self.is_required():
			input_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.p()(
			html.label(for_=self.get_id())(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))


class EmailField(TextField):

	def to_html(self):
		# Build the default field input node
		input_node = html.input(
			id=self.get_id(),
			type='email',
			name=self.name,
			placeholder=self.placeholder,
			value=self.value or '',
			class_=' '.join(self.classes)
		)

		if self.autofocus:
			input_node._attributes['autofocus'] = 'autofocus'

		if not self.autocomplete:
			input_node._attributes['autocomplete'] = 'off'

		if self.is_required():
			input_node._attributes['required'] = 'required'

		# Build the output row for the form
		html_snippet = html.p()(
			html.label(for_=self.get_id())(self.label),
			input_node
		)

		return mark_safe(render(html_snippet))


class HiddenField(Field):
	"""A hidden input Field."""

	def __init__(self, label='', datatype=unicode, validators=[], **kwargs):
		self._datatype = datatype
		super(HiddenField, self).__init__(label=label, validators=validators, **kwargs)

	def get_value(self, default=''):
		"""Get the value of the field in it's native Python format"""
		return self._value or default or self._default

	def set_value(self, v):
		self._value = self._datatype(v)

	value = property(get_value, set_value)

	def to_html(self):
		input_node = html.input(
			id=self.get_id(),
			type='hidden',
			name=self.name,
			value=self.value or '',
			class_=' '.join(self.classes)
		)

		return mark_safe(render(input_node))


class SlugField(HiddenField):
	"""A 'slug' Field"""

	def __init__(self, label='', value=u'', populate_from=None, *args, **kwargs):
		super(SlugField, self).__init__(label=label, value=value, **kwargs)

		self.populate_from = populate_from
		self._type = 'Hidden'

		# @@ TODO: We need to check that slug fields are specified after the field they are populating

	def populate(self, value, form=None):
		if self.populate_from is not None and form is not None:
			if not form.has_field(self.populate_from):
				raise ApplicationException('SlugField "%s" can not pre-populate from "%s"' % (
					self.label,
					self.populate_from
				))

			value = form.get_field(self.populate_from).get_value()

			if value:
				self.value = slugify(value)

		elif value:
			super(SlugField, self).populate(value, form=form)


class AutoCompleteField(Field):
	"""An auto-complete field"""
	def __init__(self, label='', value=u'', mask_value=u'', datalist=None, datalist_url=None, *args, **kwargs):
		self.mask_value = mask_value
		self.datalist = datalist
		self.datalist_url = datalist_url
		super(AutoCompleteField, self).__init__(label=label, value=value, **kwargs)

		self._type = 'AutoComplete'

	def get_value(self, default=None):
		"""Get the value of the field in it's native Python format"""

		if self._value is not None:
			return self._value

		if default is not None:
			return default

		return self._default

	def set_value(self, v):
		# Neeed to look at this, but currently this provides a way to specify
		# the mask using the standard controllers (albeit through a custom
		# property). It also matches the autocomplete JS's output format which
		# divides value|mask the same way.
		if '|' in v:
			self._value = v.split('|')[1]
			self.mask_value = v.split('|')[0]
		else:
			self._value = v
			self.mask_value = v

	value = property(get_value, set_value)

	def to_dict(self):
		"""Return the field as a dictionary"""

		field = super(AutoCompleteField, self).to_dict()
		field['mask_value'] = self.mask_value
		field['datalist'] = self.datalist
		field['datalist_url'] = self.datalist_url

		return field