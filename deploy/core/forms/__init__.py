"""Deploy Forms"""

import json
from copy import copy, deepcopy

from deploy.core.exceptions import ConfigurationError
from deploy.core.forms.fields import Field, Fieldset
from deploy.core.forms.utils import get_field_for_property, get_pretty_field_name
from deploy.core.models import Model
from deploy.core.models.utils import get_model_columns
from deploy.utils.datastructures import SortedDict

__all__ = ('Form', 'ModelFormFactory')

DEFAULT_FORM_META = {
	'name': None,
	'classes': [],
	'attribs': {},
	'linked': {},
}

def get_base_fields(bases, attrs):
	fields = [(field_name, attrs.pop(field_name)) for field_name, obj in attrs.items() if isinstance(obj, Field)]
	fields.sort(lambda x, y: cmp(x[1]._counter, y[1]._counter))

	for base in bases[::-1]: # Reverse order!!!
		if hasattr(base, 'base_fields'):
			fields = base.base_fields.items() + fields

	return SortedDict(fields)

def get_base_fieldsets(bases, attrs):
	fieldsets = []

	if 'fieldsets' in attrs:
		fieldsets = attrs['fieldsets']

	for base in bases[::-1]: # Reverse order!!!
		if hasattr(base, 'base_fieldsets'):
			fieldsets = base.base_fieldsets + fieldsets

	return fieldsets

class DeclarativeForm(type):

	def __new__(cls, name, bases, attrs):
		attrs['base_fields'] = get_base_fields(bases, attrs)
		attrs['base_fieldsets'] = get_base_fieldsets(bases, attrs)

		# Check if model meta data has been specified
		attr_meta = attrs.pop('Meta', None)

		if attr_meta is not None:
			# Model has meta detail specified, merge this with the default options
			base_meta = DEFAULT_FORM_META.copy()
			base_meta.update(attr_meta.__dict__)

			attrs['Meta'] = type('Meta', (object,), base_meta)

		else:
			attrs['Meta'] = type('Meta', (object,), DEFAULT_FORM_META)

		new_cls = super(DeclarativeForm, cls).__new__(cls, name, bases, attrs)

		fields = {}

		for name, attrib in attrs['base_fields'].items():
			if isinstance(attrib, Field):
				attrib.name = name
				fields[name] = attrib

		fieldsets = []

		for fieldset in attrs['base_fieldsets']:
			fieldsets.append(fieldset)

		new_cls.fields = fields
		new_cls.fieldsets = fieldsets

		return new_cls

class Form(object):

	__metaclass__ = DeclarativeForm

	def __init__(self, action=None, method='GET', enctype='application/x-www-form-urlencoded', id_=None):

		assert self.base_fields is not None, 'Form class contains no fields!'

		self.fields = deepcopy(self.base_fields)

		if not hasattr(self, 'action') or self.action is None:
			self.action = action

		assert self.action is not None, 'You must specify a form action!'

		if not hasattr(self, 'method') or self.method is None:
			self.method = method

		assert self.method in ('GET', 'POST'), 'Invalid form method! "%s"' % self.method

		if not hasattr(self, 'enctype') or self.enctype is None:
			self.enctype = enctype

		assert self.enctype in ('multipart/form-data', 'application/x-www-form-urlencoded',
								'text/plain'), 'Invalid form method! "%s"' % self.enctype

		if self.fieldsets:
			# Check fields in fieldset exist in form
			for fieldset in self.fieldsets:
				for fieldname in fieldset:
					assert self.has_field(fieldname), 'Fieldset error - Field "%s" does not exist in Form!' % fieldname

			# Check all fields are part of a fieldset
			for field in self:
				for fieldset in self.fieldsets:
					if fieldset.has_field(field.get_name()):
						break
				else:
					raise ConfigurationError('Fieldset error - Field "%s" is not specified as a member of any fieldset!' % field.get_name())

		self._id = id_ or self.__class__.__name__.lower()
		self._form_errors = []

		self.linked = self.Meta.linked
		self.validated_data = {}

	def __repr__(self):
		return '<DeployForm: %s>' % self.id

	def __iter__(self):
		"""Generator for Form, allows iteration over each fieldset in form"""
		for name, field in self.fields.items():
			yield field

	def __getattr__(self, name):
		try:
			field = self.fields[name]
		except KeyError:
			raise KeyError('Form does not contain a field called "%s"' % name)

		return field

	def to_dict(self):
		"""Return the Form object as a JSON object"""

		form = {
			'name': self.get_name(),
			'action': self.action,
			'method': self.method,
			'classes': self.Meta.classes,
			'attribs': self.Meta.attribs,
			'fields': [field.to_dict() for field in self],
			'linked': self.linked,
		}

		if self.fieldsets:
			form['fieldsets'] = [fieldset.to_dict() for fieldset in self.fieldsets]

		return form

	def get_name(self):
		"""Get the form name. Form names are either set in their Meta attribute or it
		is created automatically from the form class name"""

		if hasattr(self.Meta, 'name') and self.Meta.name:
			return unicode(self.Meta.name)

		return '%s-form' % self.__class__.__name__.lower()

	def has_field(self, name):
		"""Check if a form has a field"""
		return name in self.fields

	def get_field(self, name):
		"""Get a field from the form"""
		try:
			return self.__getattr__(name)
		except KeyError:
			return None

	def set_field(self, field, strict=False):
		"""Set a field for the form"""

		# Sanity checks....
		if strict and self.has_field(field.get_name()):
			raise RuntimeError('Form already has a field "%s"' % field.get_name())

		assert isinstance(field, Field), 'You must provide a Field argument to set_field()!'

		self.fields[field.get_name()] = field

	def is_linked_field(self, fieldname):
		return fieldname in self.linked

	def get_linked_fields(self, fieldname):
		if self.is_linked_field(fieldname):
			return self.linked[fieldname]

		return []

	def link_fields(self, fieldname, fields):
		"""'Link' fields of a form together"""

		if self.linked.has_key(fieldname):
			self.linked[fieldname].extend(fields)
		else:
			self.linked[fieldname] = fields

	def unlink_fields(self, fieldname):
		"""'Unlink' fields of a form"""

		if self.linked.has_key(fieldname):
			del self.linked[fieldname]

	def get_fieldsets(self):
		return self.fieldsets

	def build(self, request):
		"""Utility to build dynamic form values"""

		for field in self:
			field.build(request)

	def populate(self, data):
		"""Populate the Fields in the Form"""

		if data:

			# Attempt to populate a field one at a time
			for field in self:

				# See if the data dictionary has a key
				if data.has_key(field.name):

					# We need to be aware that field values may expect multiple values,
					# in order to support this any dictionary object sent to populate
					# must have a 'getall' method
					if hasattr(data, 'getall') and (hasattr(field, '_multiple') and getattr(field, '_multiple')):
						field.populate(data.getall(field.name), form=self)

					else:
						# Use internal populate method on field
						field.populate(data[field.name], form=self)

	def clear(self):
		"""Clear the Form Fields"""

		# Iterate through the fields and clear the field value
		for field in self:
			field.clear()

	def validate(self, request=None):
		"""Validate the Fields in the Form"""
		valid = True

		for field in self:
			valid = field.validate(form=self, request=request) and valid

			if valid:
				self.validated_data[field.get_name()] = field.prepare(form=self)

		return valid

	def get_field_errors(self):
		"""Return a dictionary containing any error for each Field in the Form."""
		error_data = {}

		for field in self:
			if field.error:
				error_data[field.name] = field.error

		return error_data or None

	field_errors = property(get_field_errors)

	def get_form_errors(self):
		return self._form_errors

	def set_form_error(self, err):
		self._form_errors.append(err)

	form_errors = property(get_form_errors, set_form_error)

	def get_data(self):
		"""Return a dictionary containing the value of each Field in the Form."""
		form_data = {}

		for field in self:
			form_data[field.name] = field.get_value()

		return form_data

	data = property(get_data)

	def get_validated_data(self):
		return self.validated_data

	def get_id(self):
		return self._id

	id = property(get_id)

class ModelFormFactory(object):

	def __new__(self, *args, **kwargs):

		# Look for the model for the form factory
		if 'model' in kwargs and kwargs['model'] is not None:
			model = kwargs['model']
			del kwargs['model']

			assert issubclass(model, Model), '"model" argument must be a subclass of Model! is "%r"' % model
		else:
			raise TypeError('Model argument is required!')

		field_overrides = {}

		# Look if any field overides are being specified
		if 'field_overides' in kwargs and kwargs['field_overides'] is not None:
			field_overrides = kwargs['field_overides']
			del kwargs['field_overides']

		new_form = Form(*args, **kwargs)

		# Transfer a 'Meta' class to the Form Class
		if hasattr(self, 'Meta'):
			# Model has meta detail specified, merge this with the default options
			base_meta = DEFAULT_FORM_META.copy()
			base_meta.update(self.Meta.__dict__)

			new_form.Meta = type('Meta', (object,), base_meta)

		else:
			new_form.Meta = type('Meta', (object,), DEFAULT_FORM_META)

		# Get the properties for the Model
		properties = get_model_columns(model)

		if properties:

			# Get the field for the model property
			for name, prop in properties:

				# Never add the id field to the factory forms
				if name == 'id':
					continue

				field_args = {}

				# See if an overide has been specified, this should either be
				# a Field instance or addiitonal args to be passed to the default
				# constructor.
				if name in field_overrides:
					if isinstance(field_overrides[name], Field):
						new_form.set_field(field_overrides[name])
						continue

					field_args = field_overrides[name]

				field_cls = get_field_for_property(prop)

				# Only add it to the form field list if we know the type,
				# If the type is unknown, this should be logged but NOT fatal
				if field_cls:
					field = field_cls(label=get_pretty_field_name(name), **field_args)
					field.name = name

					# Add the field to the form
					new_form.set_field(field)
				else:
					# @@ TODO: Log the failure to find a field type in a Model factory.
					pass

		return new_form
