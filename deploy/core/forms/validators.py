"""Validators for form field data"""

import cgi
import re
import json

__all__ = (
	'Validator', 'RequiredValidator', 'EmailValidator',
	'MinLengthValidator', 'MaxLengthValidator', 'IsIntegerValidator',
	'ModelUniqueValidator',
	'URLValidator'
)

VALID_EMAIL_REGEX = re.compile(r'^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$')

class ValidatorResult(object):
	"""When a Field is validated with a Validator class a ValidatorResult
	instance is returned."""

	def __init__(self, success, error_message=''):
		self.success = success
		self.error_message = error_message

class Validator(object):

	def __init__(self, custom_error_msg=None):
		self.custom_error_msg = custom_error_msg

	def _get_error(self, msg):
		return self.custom_error_msg or msg

	def validate(self, field, value, form=None, request=None):
		"""Validate a fields value"""
		return ValidatorResult(True)

class RequiredValidator(Validator):
	"""A Validator that requires a value."""

	def validate(self, field, value, form=None, request=None):
		"""Validate the field has a value"""
		if isinstance(value, cgi.FieldStorage):
			value = value.filename

		if value is None or value == '':
			return ValidatorResult(False, self._get_error('This field is required.'))

		return ValidatorResult(True)

class RequiredJSONValidator(RequiredValidator):
	"""A Validator that requires a JSON value."""

	def validate(self, field, value, form=None, request=None):
		"""Validate the field has a value"""
		if isinstance(value, cgi.FieldStorage):
			value = value.filename

		value = json.loads(value)

		if value is None or value == '':
			return ValidatorResult(False, self._get_error('This field is required.'))

		return ValidatorResult(True)

class RequiredOptionValidator(Validator):
	"""A Validator that requires a valid option."""

	def validate(self, field, value, form=None, request=None):
		"""Validate the field has a value"""
		if isinstance(value, cgi.FieldStorage):
			value = value.filename

		if value is None:
			return ValidatorResult(False, self._get_error('This field is required.'))

		return ValidatorResult(True)

class EmailValidator(Validator):
	"""A validator that requires a value is in correct email format"""

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is in a valid Email format"""

		if value and not VALID_EMAIL_REGEX.match(value):
			return ValidatorResult(False, self._get_error('This email address is not valid.'))

		return ValidatorResult(True)

class MinLengthValidator(Validator):
	"""A validator that validates the length of a given value
	is greater than a minimum amount"""

	def __init__(self, length=255, custom_error_msg=None):
		self.length = length
		self.custom_error_msg = custom_error_msg

	def validate(self, field, value, form=None, request=None):
		"""Validate the values length"""

		# Note this is intended for string lengths, however it doesn't enforce this
		# so other objects that can generate a length may also be applicable
		if len(value) > 0 and len(value) < self.length:
			return ValidatorResult(False, self._get_error('This field must have a length of %s or more' % self.length))

		return ValidatorResult(True)

class MaxLengthValidator(Validator):
	"""A validator that validates the length of a given value
	is less than a maximum amount"""

	def __init__(self, length=255, custom_error_msg=None):
		self.length = length
		self.custom_error_msg = custom_error_msg

	def validate(self, field, value, form=None, request=None):
		"""Validate the values length"""

		# Note this is intended for string lengths, however it doesn't enforce this
		# so other objects that can generate a length may also be applicable
		if len(value) > self.length:
			return ValidatorResult(False, self._get_error('This field must have a length of %s or less' % self.length))

		return ValidatorResult(True)

class IsIntegerValidator(Validator):

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is an int"""

		if isinstance(value, int):
			return ValidatorResult(True)

		try:
			int(value)
		except Exception:
			return ValidatorResult(False, self._get_error('This field must be a valid integer.'))

		return ValidatorResult(True)

class URLValidator(Validator):

	regex = re.compile(
		r'^https?://' # http:// or https://
		r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|' # domain...
		r'localhost|' #localhost...
		r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
		r'(?::\d+)?' # optional port
		r'(?:/?|[/?]\S+)$', re.IGNORECASE)

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is a valid URL"""

		if value and not self.regex.search(value):
			return ValidatorResult(False, self._get_error('This field must be a valid URL!'))

		return ValidatorResult(True)

class IsFloatValidator(Validator):

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is an int"""

		if isinstance(value, float):
			return ValidatorResult(True)

		try:
			float(value)
		except Exception:
			return ValidatorResult(False, self._get_error('This field must be a valid float.'))

		return ValidatorResult(True)

class ModelUniqueValidator(Validator):
	"""A validator that requires a value is unique within the database records"""

	def __init__(self, model, fieldname=None, custom_error_msg=None, additional_filter=None):
		self.model = model
		self.custom_fieldname = fieldname
		self.custom_error_msg = custom_error_msg
		self.additional_filter = additional_filter

	def validate(self, field, value, form=None, request=None):
		"""Validate the field is in a valid Email format"""

		field_name = self.custom_fieldname or field.get_name()

		if not request:
			return ValidatorResult(False, 'Unable to validate field "%s" without a request!' % field_name)

		if not self.model:
			return ValidatorResult(False, 'Unable to validate field "%s" without a model reference!' % field_name)

		if not hasattr(self.model, field_name):
			return ValidatorResult(False, 'Unable to validate: Model "%s" has no field "%s"' % (self.model.__class__.__name__, field_name))

		filter_args = []

		# Exclude own field
		if form and form.has_field('id'):

			try:
				id_value = int(form.get_field('id').value)
			except ValueError:
				return ValidatorResult(False, 'Unable to validate field "%s" with ID that is not an Integer!' % field_name)

			results = request.db.find(self.model, (
				getattr(self.model, field_name) == value,
				getattr(self.model, 'id') != id_value,
			)).one()

		else:
			results = request.db.find(self.model, getattr(self.model, field_name) == value).one()

		if results:
			return ValidatorResult(False, self._get_error('The "%s" value must be unique!' % field_name))

		return ValidatorResult(True)
