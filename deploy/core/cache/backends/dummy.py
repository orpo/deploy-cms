"""Dummy cache interface"""

from deploy.core.cache.backends import BaseCacheInterface

__all__ = ('CacheInterface',)

class CacheInterface(BaseCacheInterface):

	def get(self, url):
		return None

	def set(self, url, content):
		return True

	def remove(self, url):
		return True

	def clear(self):
		return True