"""Structured Cache backend

This creates a cache in a directory/file structure, one that would be
suitable to run the site statically, e.g:

URL: /
Content-Type: text/html
=> index.html

URL: /about/
Content-Type: text/html
=> /about/index.html

"""

import os.path
import datetime
import mimetypes
import hashlib
import glob

from deploy.config import settings
from deploy.core.cache.backends import BaseCacheInterface
from deploy.core.cache.exceptions import CacheBaseException, CacheCantGetItem, CacheCantSetItem, CacheCantRemoveItem, CacheCantClear
from deploy.core.http import Response
from deploy.utils._os import last_modified

__all__ = ('CacheInterface',)

class CacheInterface(BaseCacheInterface):

	def __init__(self, *args, **kwargs):
		super(CacheInterface, self).__init__(*args, **kwargs)

		# Check we have a cache root, otherwise we make a default root
		if not settings.CACHE_ROOT:
			self.cache_root = os.path.join(settings.SITE_ROOT, 'cache')

			# Does it exist?
			if not os.path.exists(self.cache_root):
				try:
					os.mkdir(self.cache_root)
				except OSError, e:
					raise CacheBaseException('Unable to create a cache root! %s' % self.cache_root)
		else:
			self.cache_root = settings.CACHE_ROOT

		self.options = {
			'default_html_filename': 'index.html'
		}

		self.options.update(settings.CACHE_OPTIONS)

	def get(self, request):
		"""Attempt to fetch a cache object from the cache, on success return
		a response object"""

		filename = self._generate_filename(request.url)
		filepath = os.path.join(self.cache_root, filename)

		if not os.path.exists(filepath):
			return None

		# Read the cache file in....
		try:
			cache_file = open(filepath, 'rb')
			contents = cache_file.read()
			cache_file.close()
		except IOError, e:
			raise CacheCantGetItem(e)

		cache_file_last_modified = last_modified(filepath)
		mimetype = self._get_mimetype_for_url(request.url)

		# Create our response..
		response = Response(content_type=mimetype)
		response.status = 200
		response.body = contents

		return response

	def set(self, request, response):
		"""Attempt to write a response to the cache"""

		if not response.status == 200:
			return False

		contents = response.body

		# @@ Todo: Add facility for capturing more than just response body...

		filename = self._generate_filename(request.url)
		filepath = os.path.join(self.cache_root, filename)

		try:
			cache_file = open(filepath, 'wb')
			cache_file.write(contents.encode('utf8'))
			cache_file.close()
		except IOError, e:
			raise CacheCantSetItem(e)

		return True

	def delete(self, url):
		"""Attempt to remove an item from the cache"""

		filename = self._generate_filename(url)
		filepath = os.path.join(self.cache_root, filename)

		try:
			os.remove(filepath)
		except OSError, e:
			raise CacheCantRemoveItem(e)

		return True

	def clear(self):
		"""Attempt to remove all the items from the cache"""

		for the_file in os.listdir(self.cache_root):
			filepath = os.path.join(self.cache_root, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
			except OSError, e:
				raise CacheCantClear(e)

		return True

	def _get_mimetype_for_url(self, url):
		mimetype, encoding = mimetypes.guess_type(url, strict=False)

		if not mimetype:
			# Assume it is html...
			mimetype = 'text/html'

		return mimetype

	def _generate_filename(self, url):
		"""Create a filename from the request"""

		url = url.lstrip('/')

		if not url.endswith('/'):
			url = '%s/' % url

		filename = '%s%s' % (url, self.options['default_html_filename'])

		return filename
