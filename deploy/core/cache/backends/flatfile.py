"""Flatfile Cache backend

This creates a cache in a flat file structure - each filename is a
MD5 of the view name/module, file extension is created from the content
type of the request

"""

import os
import os.path
import datetime
import mimetypes
import hashlib
import glob

from deploy.config import settings
from deploy.core.cache.backends import BaseCacheInterface, BaseCacheFragmentResponse
from deploy.core.cache.exceptions import CacheBaseException, CacheCantGetItem, \
	CacheCantSetItem, CacheCantRemoveItem, CacheCantClear, CacheUnableToReadMeta, \
	CacheMultipleItemsReturned, CacheCantGetFragment, CacheCantSetFragment
from deploy.core.http import Response
from deploy.utils._os import last_modified

__all__ = ('CacheInterface',)

class CacheInterface(BaseCacheInterface):

	def __init__(self, *args, **kwargs):
		super(CacheInterface, self).__init__(*args, **kwargs)

		# Check we have a cache root, otherwise we make a default root
		if not settings.CACHE_ROOT:
			self.cache_root = os.path.join(settings.SITE_ROOT, 'cache')

			# Does it exist?
			if not os.path.exists(self.cache_root):
				try:
					os.mkdir(self.cache_root)
				except OSError, e:
					raise CacheBaseException('Unable to create a cache root! %s' % self.cache_root)
		else:
			self.cache_root = settings.CACHE_ROOT

		self.default_mimetype = 'text/plain'

		if 'DEFAULT_MIMETYPE' in settings.CACHE_OPTIONS:
			self.default_mimetype = settings.CACHE_OPTIONS['DEFAULT_MIMETYPE']

	def get(self, view, request):
		"""Attempt to fetch a cache object from the cache, on success return
		a response object"""

		filepath = self._get_filepath_for_view(view)

		if not filepath:
			return None

		# Read the cache file in....
		try:
			cache_file = open(filepath, 'rb')
			contents = cache_file.readlines()
			cache_file.close()
		except IOError, e:
			raise CacheCantGetItem(e)

		# Generate meta information from the cache file
		cache_file_last_modified = last_modified(filepath)
		cache_file_mimetype, cache_file_encoding = mimetypes.guess_type(filepath, strict=False)

		if not cache_file_mimetype:
			cache_file_mimetype = self.default_mimetype

		# Create our response..
		response = Response(content_type=cache_file_mimetype, request=request)
		response.status = 200
		response.body = contents
		response.last_modified = cache_file_last_modified

		return response

	def get_fragment(self, key, extra_path=None):

		# Get the path to the fragment
		if extra_path is not None:
			fragment_path = os.path.join(self.cache_root, extra_path, key)
		else:
			fragment_path = os.path.join(self.cache_root, key)

		fragment = ''

		if not os.path.exists(fragment_path):
			return fragment

		try:
			fragment_file = open(fragment_path, 'rb')
			fragment_contents = fragment_file.read()
			fragment_file.close()
		except IOError, e:
			raise CacheCantGetFragment(e)

		# Generate meta information from the cache fragment file
		cache_file_last_modified = last_modified(fragment_path)
		cache_file_mimetype, cache_file_encoding = mimetypes.guess_type(fragment_path, strict=False)

		if not cache_file_mimetype:
			cache_file_mimetype = self.default_mimetype

		fragment = BaseCacheFragmentResponse()
		fragment.content = fragment_contents
		fragment.last_modified = cache_file_last_modified

		return fragment

	def set(self, view, request, response):
		"""Attempt to write a response to the cache"""

		assert response.status_int == 200, 'Only responses with a HTTP 200 status code can be cached!'

		contents = response.body

		filename = self._generate_cache_key(view)
		filepath = os.path.join(self.cache_root, filename)

		cache_file_extension = mimetypes.guess_extension(response.content_type, strict=False)

		if cache_file_extension:
			filepath += cache_file_extension

		try:
			cache_file = open(filepath, 'wb')
			cache_file.write(contents.encode('utf8'))
			cache_file.close()
		except IOError, e:
			raise CacheCantSetItem(e)

		return True

	def set_fragment(self, key, value, extra_path=None):

		# Get the path to the fragment
		if extra_path is not None:
			extra_path = os.path.join(self.cache_root, extra_path)

			# Allow for the fact extra path may need to be created dynamically
			if not os.path.exists(extra_path):
				os.makedirs(extra_path)

			# Get the full path
			fragment_path = os.path.join(extra_path, key)

		else:
			# Build standard fragment path
			fragment_path = os.path.join(self.cache_root, key)

		try:
			fragment_file = open(fragment_path, 'wb')
			fragment_file.write(value.encode('utf8'))
			fragment_file.close()
		except IOError, e:
			raise CacheCantSetFragment(e)

		return True

	def delete(self, view):
		"""Attempt to remove an item from the cache"""

		filepath = self._get_filepath_for_view(view)

		if not filepath:
			return False

		try:
			os.unlink(filepath)
		except OSError, e:
			raise CacheCantRemoveItem(e)

		return True

	def delete_fragment(self, key, extra_path=None):
		if extra_path is not None:
			extra_path = os.path.join(self.cache_root, extra_path)

			# Allow for the fact extra path may need to be created dynamically
			if not os.path.exists(extra_path):
				os.makedirs(extra_path)

			# Get the full path
			fragment_path = os.path.join(extra_path, key)

		else:
			# Build standard fragment path
			fragment_path = os.path.join(self.cache_root, key)

		try:
			os.unlink(fragment_path)
		except OSError, e:
			raise CacheCantRemoveItem(e)

		return True

	def clear(self):
		"""Attempt to remove all the items from the cache"""

		for the_file in os.listdir(self.cache_root):
			filepath = os.path.join(self.cache_root, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
			except OSError, e:
				raise CacheCantClear(e)

		return True

	def read(self, view):
		"""Geat a copy of a cache entry if it exists"""

		filepath = self._generate_filename(view)

		if not filepath:
			raise CacheCantGetItem('Can not read file for view "%s.%s"' % (view.__module__, view.__name__))

		try:
			cache_file = open(filepath, 'rb')
			contents = cache_file.read()
			cache_file.close()
		except IOError, e:
			raise CacheCantGetItem(e)

		return contents

	def _generate_cache_key(self, view):
		return hashlib.md5('%s.%s' % (view.__module__, view.__name__)).hexdigest()

	def _get_filepath_for_view(self, view):
		"""Create a filepath for a view"""

		# Generate the key from the view module/name
		cache_hash_key = self._generate_cache_key(view)

		# Build a filesystem path
		filepath = os.path.join(self.cache_root, cache_hash_key)

		# Attempt to match the caceh filename to the filesystem. We
		# don't necessarily know the mimetype at this point so we
		# do a wildcard match.
		results = glob.glob('%s.*' % filepath)

		# Check our results, we only one 1 matching result, any more
		# is an error, less means there is no cache file.
		if len(results) == 0:
			return None
		elif len(results) > 1:
			raise CacheMultipleItemsReturned()

		return results[0]
