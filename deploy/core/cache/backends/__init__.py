"""Base Interface for all cache backends.

Overview of the cache:
All cache backends are intended to work in the same way, with only the storage
mechanisms differing.
"""

import datetime
import sys
import mimetypes

from deploy.config import settings

__all__ = ('BaseCacheInterface', 'BaseCacheFragmentResponse')

class BaseCacheInterface(object):

	def __init__(self):
		# This is the time the 'cache' was last rebuilt
		self._created = datetime.datetime.now()

	def get(self, view, request):
		"""GET: A Request object is sent to to the 'get' method and is used to lookup the required
		cache item - if it exists. If it does the 'get' method MUST return a response object."""
		raise NotImplementedError

	def get_fragment(key, extra_path=None):
		"""Get Fragment is a additional utility, allowing for partial content to be stored. It requires
		that a key is pre generated and sent through for retrieval."""
		raise NotImplementedError

	def set(self, view, request, response):
		"""SET: The original request AND the completed response are sent to set the cache. These together
		are used to create the cache record."""
		raise NotImplementedError

	def set_fragment(key, value, extra_path=None):
		"""Set Fragment is a additional utility, allowing for partial content to be saved to the cache"""
		raise NotImplementedError

	def delete(self, view):
		"""DELETE: A Request object is sent to to the 'remove' method and is used to lookup the required
		cache item - if it exists. If it does the 'remove' method MUST remove the cache record and ONLY
		return a 'True' response if this is successful."""
		raise NotImplementedError

	def delete_fragment(key, value, extra_path=None):
		"""DELETE: Fragment"""
		raise NotImplementedError

	def clear(self):
		"""CLEAR: Clear is a special method that allows for a cache to be truncated. It MUST return 'True'
		if all records have been successfully removed. It MUST also reset the '_created' time on success"""
		raise NotImplementedError

	def read(self, view):
		"""READ: Return a cache entry for a given URL, if it exists in the cache"""
		raise NotImplementedError

class BaseCacheFragmentResponse(object):
	"""Object interface for cache fragment responses"""

	__slots__ = ('content', 'last_modified')
