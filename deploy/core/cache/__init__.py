"""Tools for working with the Deploy Cache interface"""

import datetime
import hashlib

from deploy.config import settings
from deploy.core.exceptions import ConfigurationError
from deploy.utils.importing import import_object

__all__ = ('get_cache_interface', 'cache')

def get_cache_interface(interface=None):
	"""Get a handle to a cache interface"""

	assert interface is not None, 'You must specify a cache interface to get_cache_interface!'

	module = 'deploy.core.cache.backends.%s.CacheInterface' % interface

	try:
		interface_cls = import_object(module)
	except ImportError, e:
		raise ConfigurationError('Unable to load cache interface "%s": %s' % (interface, e))

	return interface_cls()

def cache(expires=None, allowed_methods=None, watch=None, validate=None):
	"""Utility to cache view responses"""

	if allowed_methods is None:
		allowed_methods = ['GET']

	def cache_request(fn, *args, **kwargs):

		def run_cache(request, *args, **kwargs):

			cache_response = None

			# Get the cache interface
			interface = get_cache_interface(interface=settings.CACHE_INTERFACE)

			if request.method in allowed_methods:
				# Check if the view exists in the cache
				cache_response = interface.get(fn, request)

			# If there is a cache response we return this directly
			if cache_response is not None and (not validate or (validate and validate(request, cache_response))):
				# Trigger is the value at which the cache will be invalidated
				trigger = None
				now = datetime.datetime.now(settings.DEFAULT_TIMEZONE_INFO())

				if isinstance(expires, datetime.datetime):
					# Exipres is an explicit date
					trigger = expires

				elif isinstance(expires, datetime.timedelta):
					# Expires is an offset from current date
					trigger = cache_response.last_modified + expires

				if trigger and now > trigger:
					# Cache is invalid - remove the record
					interface.delete(fn)
					cache_response = None

				else:
					# Cache is valid
					return cache_response

			# No cache response - run the view...
			response = fn(request, *args, **kwargs)

			# We ONLY cache 200 responses
			if response.status_int == 200 and request.method in allowed_methods and not cache_response:
				# Write to the cache interface. Ideally this should be a non
				# blocking call.
				interface.set(fn, request, response)

			return response

		return run_cache

	return cache_request

def cache_fragment(expires=None, watch=None, extra_path=None, key=None):
	"""Utility to cache string fragments"""

	def cache_fragment_request(fn, *args, **kwargs):

		def run_fragment_cache(request, *args, **kwargs):

			# Get the cache interface
			interface = get_cache_interface(interface=settings.CACHE_INTERFACE)

			if key is not None:
				fragment_key = key
			else:
				fragment_key = hashlib.md5(fn.__name__).hexdigest()

			# Check if the view exists in the cache
			cache_response = interface.get_fragment(fragment_key, extra_path=extra_path)

			# If there is a cache response we return this directly
			if cache_response is not None:
				# Trigger is the value at which the cache will be invalidated
				trigger = None
				now = datetime.datetime.now(settings.DEFAULT_TIMEZONE_INFO())

				if isinstance(expires, datetime.datetime):
					# Exipres is an explicit date
					trigger = expires

				elif isinstance(expires, datetime.timedelta):
					# Expires is an offset from current date
					trigger = cache_response.last_modified + expires

				if trigger and now > trigger:
					# Cache is invalid - remove the record
					interface.delete(fn)
					cache_response = None

				else:
					# Cache is valid
					return cache_response.content

			# No cache fragment - run the fragment function...
			fragment = fn(request, *args, **kwargs)

			if fragment is not None and not fragment == '':
				interface.set_fragment(fragment_key, fragment, extra_path=extra_path)

			return fragment

		run_fragment_cache.__fragment_name__ = fn.__name__

		return run_fragment_cache

	return cache_fragment_request
