"""Exceptions classes for cache operations"""

from deploy.core.exceptions import DeployBaseException

__all__ = ('CacheCantGetItem', 'CacheCantSetItem', 'CacheCantRemoveItem',
'CacheCantClear', 'CacheUnableToReadMeta', 'CacheMultipleItemsReturned')

class CacheBaseException(DeployBaseException): pass
class CacheCantGetItem(CacheBaseException): pass
class CacheCantSetItem(CacheBaseException): pass
class CacheCantRemoveItem(CacheBaseException): pass
class CacheCantClear(CacheBaseException): pass
class CacheUnableToReadMeta(CacheBaseException): pass
class CacheMultipleItemsReturned(CacheBaseException): pass
class CacheCantGetFragment(CacheBaseException): pass
class CacheCantSetFragment(CacheBaseException): pass