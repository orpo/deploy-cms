"""Deploy Resource Type Objects"""

import os.path
import warnings

from deploy.config import settings
from deploy.core.resources.utils import minified_filename
from deploy.utils import jsmin
from deploy.utils._os import last_modified

__all__ = ('Resource', 'CSSResource', 'JSResource')

class Resource(object):
	"""A base resource class."""

	HTML_TEMPLATE = ''

	def __init__(self, uri, mimetype, minify=False):

		assert uri is not None, 'You must specify a URL for the resource!'

		self.uri = self.original_uri = uri
		self.mimetype = mimetype

		# Minification Settings
		self.can_minify = minify
		self.is_minified = False

	def __repr__(self):
		return '<DeployResource: %s %s/%s %s>' % (self.__class__.__name__, self.mimetype, self.uri)

	def __unicode__(self):
		return unicode(self.__repr__())

	def __str__(self):
		return str(self.__unicode__())

	def get_filename(self):
		"""Lookup the full filename of the resource"""

		if self.is_minified:
			return self.get_minified_filename()

		return os.path.join(settings.SITE_ROOT, self.uri.lstrip('/'))

	def get_original_filename(self):
		"""Return the orignal filename of the resource. This is gaurenteed to be the orignal
		resource filename, it does not take into account minifiying or skining"""
		return os.path.join(settings.SITE_ROOT, self.original_uri.lstrip('/'))

	def get_minified_filename(self):
		"""Return a filename for the minified resource"""
		return minified_filename(self.get_original_filename())

	def get_url(self):
		"""Return the URL. This function takes into account if the resource is minified.
		Use Resource.original_url for the original URL used when it was created."""

		if self.is_minified:
			return minified_filename(self.uri)

		return self.uri

	def get_hash(self):
		"""Generate a MD5 hash of the resource URL"""

		hash_val = None
		original_file_modified = last_modified(self.get_filename())

		if original_file_modified:
			# We include the last mondified date in the hash as this gives us
			# a primative form of versioning, which alters the hash and allows
			# us to provide browsers with different cache urls
			hash_val = hashlib.md5('%s-%s' % (self.uri, original_file_modified.strftime('%H:%I-%m-%d-%Y')))
		else:
			hash_val = hashlib.md5(self.uri)

		return hash_val.hexdigest()

	def is_minifyable(self):
		"""Utility to check if it is possible to minify the resource"""

		if settings.DEBUG:
			return False

		if self.can_minify and not self.is_minified:
			return True

		return False

	def minify(self):
		"""Utility to Minify the resource. There is no base minify function
		provided by default, it is intended to be implimented in sub-classes"""

		raise NotImplementedError

	def read(self):
		"""Read the contents of the resource file. NOTE: Files are opened in non-binary mode
		to ensure line-ending compatability. If a subclass requires binary mode this method should
		be overwritten."""

		try:
			contents = open(self.get_filename(), 'r').read()
		except IOError, e:
			raise RuntimeError(e)

		return contents

	def write(self, contents, filename=None):
		"""Write the contents of a resource"""

		if not filename:
			filename = self.get_filename()

		try:
			f = open(filename, 'w')
			f.write(contents)
			f.close()
		except IOError, e:
			raise RuntimeError(e)

	def as_html(self):
		"""Return a link to the resource as would be used in HTML"""

		raise NotImplementedError

class CSSResource(Resource):
	"""A Cascading Style Sheet resource class."""

	HTML_TEMPLATE = '<link rel="stylesheet" href="%s" />'

	def __init__(self, uri, minify=True):
		super(CSSResource, self).__init__(uri, 'text/css', minify=minify)

	def minify(self):
		"""Minify the CSS resource using CSSTidy"""

		assert self.is_minifyable(), 'Can not minify a resource not marked for minification!'

		try:
			import cssutils
		except ImportError:
			warnings.warn(
				'You must install cssutils to minify CSS',
				RuntimeWarning
			)
			return False

		cssutils.ser.prefs.useMinified()

		sheet = cssutils.parseString(self.read())
		minified_contents = sheet.cssText

		self.write(minified_contents, filename=self.get_minified_filename())

		self.is_minified = True

		return True

	def as_html(self):
		return self.HTML_TEMPLATE % self.get_url()

class JSResource(Resource):
	"""A JavaScript resource class."""

	HTML_TEMPLATE = '<script src="%s"></script>'

	def __init__(self, uri, minify=True, defer=False):
		super(JSResource, self).__init__(uri, 'text/javascript', minify=minify)

		# The defer attribute is intended to delay execution of a script until
		# after the page has loaded, however at the time or writing this class
		# (23/09/2009) there are numerous reports of cross browser quirks when
		# using the attribute (so beware).
		self.defer = defer

	def minify(self):
		"""Minify javascript resource using jsmin"""

		assert self.is_minifyable(), 'Can not minify a resource not marked for minification!'

		# Run the JS minification
		minified_contents = jsmin.jsmin(self.read())

		#js = jsmin.JavaScriptMinifier()
		#minified_contents = js.JSMinify(self.read())

		self.write(minified_contents, filename=self.get_minified_filename())

		self.is_minified = True

		return True

	def as_html(self):
		return self.HTML_TEMPLATE % self.get_url()
