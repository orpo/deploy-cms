"""Utility functions for working with Deploy Resources"""

import os.path

__all__ = ('minified_filename',)

def minified_filename(filepath):
	"""Format a filepath to a minified filepath. Typically this just involves
	the replacement of the filetype with '.min.filetype' or the addition of '.min'
	if there isn't a filetype"""

	head, tail = os.path.split(filepath)

	if '.' in tail:
		bits = tail.rpartition('.')
		last = 'min.%s' % bits[-1]
		tail = bits[:-1] + (last,)
		tail = ''.join(tail)
	else:
		tail += '.min'

	return os.path.join(head, tail)