"""Deploy Resource File Management"""

import os.path
import mimetypes

try:
	import cStringIO as StringIO
except ImportError:
	import StringIO

from deploy.config import settings
from deploy.core.resources.types import Resource

__all__ = ('ResourceList',)

class ResourceList(object):
	"""A resource list (manager) class, enables resources to be organised and
	queried."""

	def __init__(self, *args, **kwargs):
		self._resources = []
		self._strict = True

		# Cache/Compile flags
		self.cache_name = None
		self.is_compiled = False

		# Store the resources..
		for arg in args:
			if isinstance(arg, Resource):
				self._add(arg)

		# Get the mimetype or mimetypes for this resource list
		self._mimetype = self.get_mimetype()

	def __repr__(self):
		return '<DeployResourceList: %s Resources>' % len(self)

	def __iter__(self):
		"""Iterate of the internal resources"""
		for resource in self._resources:
			yield resource

	def __len__(self):
		"""Return the length of the resource list"""
		return len(self._resources)

	def __add__(self, rhs):
		"""Support for addition to resource list. Other resource lists, and individual
		resources can be 'added' together"""

		if isinstance(rhs, ResourceList):
			# We add the resources from the resource list to this one
			for resource in rhs:
				self._add(resource)

		elif isinstance(rhs, Resource):
			# Add an individual resource to the list
			self._add(rhs)

		else:
			raise TypeError('Unsupported operand type(s) for: %r' % rhs)

		return self

	def _add(self, resource):
		"""Private function to add a resource to the resource list. Also
		takes care of sanity checking - looking for duplicates."""

		# Check for duplicates if we are in strict mode
		if self._strict:
			# @@ TODO: Check mimetype

			# Always use the orignal filename as the resource may
			# of changed it's url/filename dynamically
			new_resource_filename = resource.get_filename()

			for existing_resource in self:
				if existing_resource.get_filename() == new_resource_filename:
					raise RuntimeError('A resource with the URL "%s" already exists!' % existing_resource.get_url())

		self._resources.append(resource)

	def get_cache_name(self):
		"""Return the cache name of a compiled resource list. The cache name is
		generated from MD5 hashes of the Resource URL's"""

		if self.cache_name:
			# Don't regenerate if we have already created it
			return self.cache_name

		hashes = []

		# Get the hash for each resource
		for resource in self._resources:
			hashes.append(resource.get_hash())

		# Attempt to get the file extension for the cache file. We can only do this if the
		# resource list has a single common mimetype. If it doesn't we use the default cache
		# file extension
		if not isinstance(self._mimetype, (list, tuple)):
			ext = mimetypes.guess_extension(self._mimetype)
		else:
			ext = settings.CACHE_DEFAULT_EXTENSION

		self.cache_name = os.path.join(settings.RESOURCES_CACHE, '%s%s' % (hashlib.md5(''.join(hashes)).hexdigest(), ext))

		return self.cache_name

	def get_mimetype(self):
		"""Attempt to get the mimetypes for the resource list"""

		mimetypes = []

		for resource in self:
			if not resource.mimetype in mimetypes:
				mimetypes.append(resource.mimetype)

		if len(mimetypes) == 1:
			return mimetypes[0]

		return mimetypes

	def minify(self):
		"""Minify the resources in the resource list that are minifyable"""

		# We don't minify in debug mode....
		if settings.DEBUG:
			return False

		for resource in self._resources:
			if resource.is_minifyable():
				resource.minify()

		return True

	def compile(self, cache_name=None, force=False):
		"""Compile resources in the resource list into a single file. This
		is only possible if the resources are of the same type and is only done if
		debugging is turned off"""

		# We don't compile in debug mode....
		if settings.DEBUG:
			return False

		# Sanity checks
		assert not self.is_compiled, 'This resource is already compiled!'
		assert len(self._resources) > 0, 'There are no resources in this list to compile!'
		assert not isinstance(self.get_mimetype(), (list, tuple)), 'You can\'t compile resources with different mimetypes!'

		# See if the cache file already exists
		cache_name = cache_name or self.get_cache_name()

		# If the cache file exists and the compile isn't being forced
		# use the existing file.
		if os.path.exists(cache_name) and not force:
			self.is_compiled = True
			return True

		# Get the contents
		contents = StringIO.StringIO()

		for resource in self._resources:
			contents.write(resource.read())

		# Attempt to write the compiled version to disk
		try:
			f = open(cache_name, 'w')
			f.write(contents.getvalue())
			f.close()
		except IOError, e:
			raise RuntimeError(e)

		self.is_compiled = True

		return True

	def as_html(self):
		output = []

		if not self.is_compiled:
			# Output the HTML as specified by the resources
			for resource in self._resources:
				output.append(resource.as_html())

		else:
			# Use the compiled version of the resources
			# As compiled versions must be of the same mimetype, we can use
			# the resource class HTML template for output
			output.append(self._resources[0].HTML_TEMPLATE % self.get_cache_name())

		return '\n'.join(output)
