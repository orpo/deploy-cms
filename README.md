# DeployCMS

Deploy CMS a python web application framework with a baked in Content Management System.

It is heavily inspired by other Python web frameworks such as Django, Pylons/Pyramid and CherryPy. The main difference being Deploy's primary focus on creating sites with a fully featured content management system.

Whilst functional it is still very much a *work in progress*, so should be considered a Beta at this time. If you are interested in contributing, please get in touch.

More info can be found here [deploycms.com](http://deploycms.com).

Documentation can be found on [Read the Docs](https://deploy-cms.readthedocs.org).

---

Created by [Anthony Blackshaw](mailto:ant@gemte.co.uk) & [Nick Snell](mailto:nick@orpo.co.uk)

Distributed under the terms of the BSD License.

Thanks for checking Deploy CMS out!