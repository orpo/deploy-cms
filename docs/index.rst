.. Deploy CMS documentation master file, created by
   sphinx-quickstart on Thu Dec 13 22:31:09 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Deploy CMS's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   topics/installing
   topics/getting-started
   topics/plugins/news_tutorial
   topics/ui/ui-guide

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

