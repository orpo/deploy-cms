Creating a news plugin with Deploy
==================================

Let's face it, almost every client asks for news on their website, a vast majority then proceed to never use the feature unless coaxed and cajoled frequently, but nevertheless at proposal time they see it as essential (this rule tends to be just as applicable when you're your own client).

Typically you won't be starting from scratch when you create a news plug-in; you might use some existing code you have, or perhaps a third-party plug-in. However, you have to start somewhere and the aim of this walk-through is to act as a good foundation for your first plug-in, whether it be news, a product catalogue, or whatever else your client might want to manage.

.. note::
    This tutorial is designed for developers looking to create their own plug- ins on the Deploy framework. A working knowledge of Python is required.


Defining tables
---------------

Deploy uses the Storm ORM (storm.canonical.com) library for interfacing with the database. Storm doesn't manage the database schema for you, so you have to set up your tables manually. You can use any database back-end you want. The default is SQLite, though personally I always use MySQL as a preference.
The following SQL statement will create our NewsArticle table::

    use databasename;
    CREATE TABLE `NewsArticle` (
      `id` int(10) unsigned PRIMARY KEY,
      `active` tinyint(1) NOT NULL,
      `publish_date` date NOT NULL,
      `headline` varchar(255) NOT NULL UNIQUE,
      `slug` varchar(255) NOT NULL UNIQUE,
      `body` text NOT NULL,
      `image_paths` text NOT NULL
    )

The column definitions should be fairly self explanatory, the only exception might be image_paths. Images are stored against the model as a comma- separated list of paths. The paths are both a reference to the actual location of the original image, and a key to the attachment record in the database. We'll cover images in more detail later on, but for now with our table in place it's time to start coding our plug-in.

.. note::
    It's possible to use a different ORM, such as SQLObject, but that's beyond this tutorial.

.. warning::
    A common oversight when defining a table is to forget to set an integer primary key to auto-increment (such as `id` in our NewsArticle table). If you forget, Storm ORM will fail with a NotImplemented exception when it tries to create a record, which is not hugely helpful. You have been warned!


The plugin structure
--------------------

Though it's possible to store your plug-ins in any directory, we recommend that you create a **plugins** folder at the site root level and place your plug-ins there. For example;

- my_awesome_site_com (site root)
- ...
    - plugins
        - __init__.py
        - news
        - ...

There is no restriction to what you can place in your plugins folder, but it must contain **__init__.py**, as the Deploy application will look for this when initialising the plug-in. For our news plug-in I'm going to use the structure I most commonly use for plug-ins. Create the following files in your **plugins/news** directory:

- __init__.py
- cms.py
- forms.py
- models.py
- settings.py
- urls.py
- views.py

The only file that needs to be in the folder for our plug-in, other than __init__.py, is cms.py. The Deploy application will look for a cms file or folder in a plug-in's directory, the contents of which determines the user interface that is presented in edit mode for managing the plug-in (e.g. when visiting edit.my- awesome-site.com). If the plug-in doesn't need to be managed in edit mode you don't need a cms file or folder.

Now that we have our base plug-in structure in place we need to register the plug-in with Deploy. Edit the settings.py file in your site root directory and add/amend the following::

    PLUGINS = [
      'deploy.cms',
      'plugins.news'
    ]

.. note::
    Restart **deployserver** after making the Python changes.

With our news plug-in registered we can start to write some code. Everyone has their own order in which they write the code for a plug-in, but I always start by creating the models that will manage the database records for the plug-in, in this case the NewsArticle table.


Interfacing with the database via models
----------------------------------------

As we mentioned earlier we use Storm ORM as the underlying library for interfacing with the database. Though Storm allows you to execute SQL directly against the database, its primary purpose is to provide higher level tools that simplify the process of querying and managing records.

Like many CMS applications, Deploy provides a Model class as the primary tool for managing tables. Let's define a model to manage our NewsArticle table. Add the following code to **models.py**::

    from storm.locals import *
    from deploy.cms.attachments import Attachments
    from deploy.core.models import Model

    __all__ = ('NewsArticle',)

    class NewsArticle(Model):
        """News article model."""

        __storm_table__ = 'NewsArticle'

        id = Int(primary=True)
        active = Bool()
        publish_date = Date()
        headline = Unicode()
        slug = Unicode()
        body = Unicode()
        image_paths = Unicode()

        def __unicode__(self):
            return u'News article: %s' % self.headline

        @property
        def complete_url(self):
            return '/news/%d/%02d/%s' % (
                self.publish_date.year,
                self.publish_date.month,
                self.slug
            )

    NewsArticle.images = Attachments(
        NewsArticle,
        NewsArticle.image_paths,
        'media/img/news',
        max=4,
        variations={
            'thumb':
                {'process': 'variations.fit', 'args': [[75, 75]]},
            'large':
                {'process': 'variations.fit', 'args': [[265, 265]]}
        }
    )

Clearly there is quite a bit to take in here. We start by defining our model class as NewsArticle and telling Storm that the table it refers to is also called NewsArticle, using the special class property __storm_table__.

With the table relationship established we then define properties to match each of the columns in the table. Unlike a lot of ORMs, Storm is fairly generic in how it handles different formats, for example all text fields are handled as Unicode properties.

.. note::
    If you define a model property as unicode you can only set it to a unicode value, you can also only compare unicode values against it in queries. Storm does not attempt to decode strings to unicode format automatically and will simply raise an error if you supply a string.

For convenience I've defined a read-only property for the class that allows me to get the news article's relative URL (using the @property decorator). I've also specified a __unicode__ converter; it's good practice to define the __unicode__ definition for all your models as it helps when debugging your application, and the unicode is used by the edit mode controllers when showing information about the instance. For example when asking an editor to confirm they would like to delete a news article, the user will be shown the output of __unicode__ as a way to identify the individual news article, like so:

.. image:: ../../_static/cms-confirm-box.jpg

After our NewsArticle class has been defined we set up the images property. This is a special property that allows multiple attachments, in this case images, to be stored against the model.

The first two arguments to Attachment specify the model to attach the property to and the model property where the attachment paths will be stored. The third argument is an optional path, and allows us to specify a location where the images will be stored, it's optional but it makes good sense to organise them into different directories per plug-in. The forth argument, the max keyword specifies the maximum number of attachments that can be stored against the property. There doesn't have to be a limit but often it's useful to limit it to fit with your forefront design. The final argument, keyword variations, is a dictionary of functions we want performed against each attachment to create different variations of each.

You can define as many variations for your attachments as you want and then access them later using their key. A variation consists of a path to a definition and a list of arguments, in this case our definition is variations.fit and each variation has a single argument which is the image dimensions we want for that variation.

At the moment the variation definition doesn't exist, so in the site root directory create a file named *variations.py* and add the following code::

    import os
    from PIL import Image
    from deploy.config import settings

    def fit(attachment, save_path, size):
        """Create a version of the image that fits into the specified
        size."""

        path = os.path.join(settings.SITE_WWW_ROOT, attachment.path)
        save_path = os.path.join(settings.SITE_WWW_ROOT, save_path)

        if not os.path.exists(os.path.split(save_path)[0]):
            os.makedirs(os.path.split(save_path)[0])

        im = Image.open(path)
        im = im.convert('RGB')
        im.thumbnail(size, Image.ANTIALIAS)
        im.save(save_path, 'JPEG', quality=90)

The fit definition simply creates a new variation of an image attachment that fits within the dimensions specified.

Right, we're done defining our NewArticle model. But before we can start displaying news articles, we need to be able to create them so next we'll look at Deploy's CRUD.


Setting up CRUD (Create, Read, Update, Delete)
----------------------------------------------

Remember the cms.py file we created? Well that's the file which is responsible for setting up our CRUD facilities for the plug-in. Deploy provides a CMSController as a base for setting up a controller for a model. Let's create the controller for our NewsArticle model. Add the following code to the cms.py file::

    from deploy.cms.controllers import CMSController
    from deploy.cms.utils import is_logged_in
    from plugins.news.models import NewsArticle

    class NewsArticleController(CMSController):
        """CMS controller for news articles."""

        class Meta(CMSController.Meta):
            model = NewsArticle
            controller_name = 'plugin-news-article'
            gui = True
            gui_verbose_name = 'News article'
            gui_verbose_name_plural = 'News articles'
            pagination_columns = [
                dict(name='publish_date', label='Published', sortable=True),
                dict(name='headline', label='Headline'),
                dict(name='active', label='Active', sortable=True)
            ]
            pagination_order_by = 'publish_date'
            pagination_order_by_direction = 'DESC'
            search_fields = ['headline', 'body']

CMS controllers usually consist of a Meta class which is responsible for the basic CRUD setup, and additional or overridden CMS views added as methods and decorated with @is_logged_in - we'll cover these later on.

The Python CMSController sits between the database table it's managing and the JavaScript controller on the browser which will provide an interface for users to manage. These relationships are defined first in the Meta class; the model property refers to the model being managed, in this case the NewsArticle; the controller_name provides an identifier that the client-side JavaScript can refer to when calling the remote server. For example a call to list the news articles would be constructed client-side like so;::

    /edit/{controller_name}/search/

When overriding a controller client-side, the controller_name also plays an important role - see the Deploy user interface guide for JavaScript developers.

.. note::
     Unlike most CMS applications, Deploy manages almost all I/O between the server and client using JSON via AJAX.Sending and receiving just the data you need in this fashion is very fast and efficient and allows us to provide a far more responsive user interface.

Back to the CMS controller's Meta class. The gui and 'gui\_' prefixed properties hint at how the client side interface should appear. The most fundamental of these is gui = True. If you don't set this then by default there will be no interface provided for managing your model.

Deploy assigns plug-ins to the Plug-ins menu in the application toolbar (see below).

.. image:: ../../_static/cms-toolbar.jpg


If you have multiple models being managed under a single plug-in, or if you simply want to group certain models together, then you can use the gui_group property. In the example above, Enquiries is a group which you can tell from the sub-menu arrow to the right of the menu item.

The gui_verbose_name and gui_verbose_name_plural properties allow you to set how the model will be referred to in the interface. By default it just uses the model name, so this helps when you have a model called say ProductCategory, in which case you can set the verbose names to::

    gui_verbose_name = 'Product category'
    gui_verbose_name_plural = 'Product categories'

You might even shorten the name to just 'category' if you were grouping the plug-in under Product catalogue.

So we've defined our database – server-side controller – client-side controller relationship, and we've set where we want our plug-in to appear in the interface, so what happens when a user clicks our News articles menu item?

By default Deploy launches the search window for a controller when the user clicks the respective menu item. The Meta class properties prefixed 'pagination\_' determine which columns will be displayed in the search listing and the default order in which records will be returned.

The pagination_columns property is a list of dictionaries, each describing a column to include when showing a list of results in the search window (or list window if search is disabled). There are three values you can set for a column:

- name – required and must be set to the name of the property on the model you want to show in the column.
- label – required and determines the heading for the column.
- sortable – optional and specifies if a user can sort the results by that column.
- display_property – optional and allows you to specify the value of another attribute in place of the property specified by name.
- foo, bar, zee – it's actually possible to set any against the column, these simply get passed on to the JavaScript controller and can be useful for custom listings. The only caveat is they must be safe to serialise in JSON.

The default order of results is set by the pagination_order_by and pagination_order_by_direction properties. The pagination_order_by can be set to the value of any model column property and the direction to either ASC (A to Z,0to9-default)orDESC(Z toA,9to0).

.. note::
    If you want to order on a column in another model that is referenced by the model being listed you can use Storm's Proxy class. For example if you were listing Products and wanted to order by the reference Category models name you could set up the following proxy and set the order by column to category name:

::

    class Category(Model):
        name = Unicode()

    class Product(Model):
        name = Unicode()
        category_id = Int()
        category = Reference(category_id, Category.id)
        category_name = Proxy(category, Category.name)

    class ProductController(CMSController):
        class Meta(CMSController.Meta):
            pagination_columns = [
                dict(name='name', label='Name', sortable=True),
                dict(name='category_name', label='Category',
                    sortable=True)
            ]
            pagination_order_by = 'category_name'
            pagination_order_by_direction = 'DESC'

The last property we've declared in our CMS controller is search_fields. The property has two purposes: if it is not defined then the default listing view will not support a search, and if it is defined then the columns listed will be searched against when a user performs a keyword search.

.. note::
    Like sorts it is possible to add a Proxy column to the listed fields so that the search will include a column from a referenced model.

.. note::
    If you define a Reference and accidentally just provide a class and not a class property as the second argument, for example Reference (category_id, Category), you will receive a fairly cryptic error from Storm when calling the reference. The error is a reference to an unknown class.


With our CMS controller code in place, now when you click on the News articles menu item a window should appear like the one below.

.. image:: ../../_static/plugin-news-list.jpg

.. note::
    At the moment you won't see a search form in the window, because there are no records to search. Once you have one or more records added you'll be able to see the search form the next time you open the window.

If you click the *Add news article* button, top right, a new window will open and display a basic automatically-generated form for adding a news article. Don't use it yet - next we'll look at how to style our forms.


Planning and creating CMS forms
-------------------------------

First a little warning: the auto-generated forms Deploy creates, at the moment at least, are only really functional in the simplest of cases (e.g a model with just a name and description). From a personal standpoint I detest auto-generated forms - poor form planning is just sloppy.

With that rhetoric ringing in your ears, here is a mock up of how our news article form will look when finished:

.. image:: ../../_static/plugin-planning.jpg

Admittedly I don't usually go to the effort of creating a digital mock-up; normally I just do a quick hand-sketch of the fieldsets and fields as a guide – but the above example is a lot clearer than my sketches.

As you can see from the mock-up, the images for the new articles are handled in a separate tab. This is the case whenever you are supporting for multiple attachments within a form. The attachments tab is automatically created for you when you include an attachment with a maximum of more than one. With our mock-up for a guide it's time to define our form. In the cms.py file insert the following code above the NewsArticleController class::

    from deploy.core.forms import Form
    from deploy.core.forms.fields import *
    from deploy.core.forms.validators import *
    from deploy.cms.forms.fields import AttachmentField
    from plugins.news.models import NewsArticle

    class AddUpdateNewsArticleForm(Form):
        """Add/Update form for news articles."""

        publish_date = DateField(
            'Publish date',
            classes=['fluid-split'],
            placeholder='YYYY-MM-DD',
            validators=[RequiredValidator()],
            help='Articles won\'t appear on the website until their publish date.'
        )

        active = BooleanField(
            'Article is active',
            classes=['fluid-split', 'fluid-last']
        )

        headline = TextField(
            'Headline',
            validators=[
                RequiredValidator(),
                MaxLengthValidator(255),
                ModelUniqueValidator(NewsArticle)
            ]
        )

        slug = SlugField(populate_from='headline')

        body = TextareaField(
            'Body',
            validators=[RequiredValidator()]
        )

        images = AttachmentField(
            'Images',
            attachment=NewsArticle.images,
            validators=[],
            help='Images can be uploaded in JPG, PNG, GIF and TIFF formats. At most, 4 images can be uploaded.'
        )

        fieldsets = [
            Fieldset('Article', [
                'publish_date',
                'active',
                'headline',
                'slug',
                'body',
                'images'
            ])
        ]

        class Meta:
            linked = {'headline': ['slug']}


We also need associate the form with the add and update views for the controller - to do this add the following code to the NewsArticle controller meta class::

    forms = {
        'ADD': AddUpdateNewsArticleForm,
        'UPDATE': AddUpdateNewsArticleForm
    }

.. note::
    For cases where there is only one fieldset you don't need to define it as we have done above. In the case we have as an example, if you don't define one then Deploy automatically creates a default fieldset with all the fields included in it.

The first thing you probably noticed and questioned was the form's Meta class and linked property (if you didn't, go back to the top of the code and read the first three lines). This extra markup is a downside of real-time validation and submissions. Unlike traditional validation and form submissions, Deploy only sends field validation and update requests for individual fields (except on an add when it submits all the data, but add validation is still done in real-time). This means that if two or more fields are required to know about each other for the purpose of validation or updates, as is the case with our slug field which needs to know the value of headline to update itself, then you must link them. Links work both ways automatically, so you only need to link your fields once; there's no need to define every possible combination of links.

.. note::
    You can disable the real-time validation and update behavior on individual fields or across your entire website if you want to. You can also disable the behavior for individual users if they prefer to use traditional-style forms.

After the Meta class we define each of the fields for the form. Each field is represented as a property of the form class that is set to a Field class instance, such as - in the case of our first field - publish_date which is set as a DateField.

There are a multitude of fields available as standard and obviously you can create your own should you need to. The next couple of tables provide a reference for field classes, their initialisation options and common validators, you can skip or skim read them if you want as the information isn't crucial to the rest of the article. Below is a brief overview of the standard fields.

=================  ==============
Field class        Description
=================  ==============
AttachmentField    A field for managing attachments. Depending on whether or not the field supports for one or multiple attachments it will either be included in the fieldset (for one) or in a separate tab (for multiple). Returns a list of dicts (in JSON format). Each dict contains the details of an attachment.
AutoCompleteField  A text field that provides a drop-down of existing options that match the current value. The options are provided either by a static list or dynamically by firing requests to a URL as the text field value changes. Returns a string.
BooleanField       Single tick box field that can be set to True or False. Returns a string.
ChoiceField        One or more radio (to set a single value) or check boxes (to set one or more values) each with a unique label and value. Returns a string or a list of strings.
CurrencyField      A text field that requires a valid price to be set. The value is returned in the lowest denomination of the currency. In the UK that would be pence, in the US, cents. Returns a integer.
DateField          A text field that requires a valid date. The default format is to require the date in ISO standard YYYY-MM-DD. A date-picker widget is provided to make date selection easy for the user. Returns a date.
DisplayOnlyField   A read-only field that displays a value. Useful for showing static data, such as an enquiry from a visitor. Does not return a value.
EmailField         A text field that requires a valid email. Returns a string.
FloatField         A text field that requires a valid number, optionally with a fractional part. Returns a float.
HiddenField        A field that can hold a value but doesn't appear in the form. Returns a string.
IntField           A text field that requires a valid number. Returns an integer.
MapField           A field that displays a Google map on which the user can plot a point.
ModelChoiceField   A choice field where the options are automatically generated from an SQL query. Returns a string or a list of strings.
ModelSelectField   A select field where the options are automatically generated from an SQL query. Returns a string or a list of strings.
PasswordField      A password field. Returns a string.
SelectField        A select field where one (or more if set to support multiple) options can be selected, each with a unique label and value. Returns a string or a list of strings.
SlugField          A hidden field that is populated automatically by the value of another field. The value of the other field is converted to a slug (a pretty URL) and stored in the slug field. Returns a string.
TextField          A single line text field. Returns a string.
TextareaField      A multi-line text field. Returns a string.
TokenizerField     A text field that provides a drop-down of existing options that match the current value. The options are provided either by a static list or dynamically by firing requests to a URL as the text field value changes. Unlike the standard auto-complete field, with the tokenizer field you can set multiple values. Returns a list of dicts [{name: '...', value: '...'}, ...] (in JSON format).
=================  ==============

Field classes share a common set of initialisation options, with some exceptions such as hidden fields which don't support labels (though if you send a label it won't cause a problem). Below is a summary of the common field initialisation options (many of the attributes have existing HTML counterparts).

=================  ==============
Option name        Description
=================  ==============
label              The label assigned to the field.
value              The field's initial value.
placeholder        A placeholder string for the field.
validators         A list of validators that will be used to validate the field's submitted value. This defaults to a list containing a single RequiredValidator.
realtime           A flag that determines if the field will realtime validate and save. By default this is true.
id                 The field's ID. If not specified then the field will receive an ID automatically generated on the client-side.
attribs            A dictionary of attributes for the field. There's no rule as to what can be in the dictionary.
classes            A list of classes (specified as strings) that will be applied to the field. There are a number of helper classes defined by default that help with layout (discussed in the next section).  Classes are applied to the parent HTML field (a div in the standard theme) element, not the input element itself.
default            The default value for the field. If no value is submitted for the field the default value is supplied instead.
prepare            If you want to prepare the a field's value after it's been submitted you can set the prepare option to a function that will prepare the value. A common use is for converting the submitted value from a string to an integer, for example prepare=lambda x: int(x) if x.isdigit() else 0
populate           If you need to change the format of a value before it is used to populate the field you can set the field populate option to a function that will perform the change. For example in the CurrencyField the populate option by default converts an integer to a price, so 1299 becomes '12.99' in the field.
help               Help text appears in the form in a highlighted block above (in the default theme) the field.
=================  ==============

To validate the data submitted to your form you must specify how each field should validate its data by supplying a list of validators in the field options. Some fields have default validation, such as the DateField or CurrencyField classes where there is a clear format that must be specified. By default all fields have their validators option set to [RequiredValidator()]. Below is a brief overview of the standard validators.

.. note::
    To make a field optional simply provide a list of validators that does not contain a RequiredValidator class instance.

====================  =============
Validator class       Description
====================  =============
RequiredValidator     Validate that a field has a value.
EmailValidator        Validate that a field value is a valid email.
IsFloatValidator      Validate that a field value is or can safely be converted to a float.
IsIntegerValidator    Validate that a field value is or can safely be converted to an integer. By safely we mean that the conversion won't lose information, so '12' to 12 is fine, but '12.23' to 12 is not safe.
MinLengthValidator    Validate that a field value is not less than the minimum length.
MaxLengthValidator    Validate that a field value does not exceed a maximum length.
ModelUniqueValidator  Validate that the field value is unique for that model. Optionally you can provide a composite value so that two values are checked for uniqueness, such as a date and a headline. Note: For composite checks you need to link the fields in the form Meta class.
URLValidator          Validate that a field value is a valid URL.
====================  =============

The last property in a form is a list of fieldsets, or just one fieldset in this case. You don't have to define a fieldset, by default all fields are put into one automatically, but on longer forms it's often useful to group your fields into relevant sections.

You initialise a fieldset with a title (which can just be an empty string) and a list of field names to appear within the fieldset. If you do use fieldsets and miss out any fields from the form then an error will be raised when you next display the form, telling you which field has not been assigned.

.. note::
    When you have a single attachment field, for example if we added a PDF press release against our NewArticle, I recommend that you put it in its own fieldset. Single attachments are presented best by the default theme in their own fieldset.


Form layout
-----------

The default theme supports for a two column form layout. Fields can either take up both columns (the default), or can take up a single column and be floated next to each other. In our AddUpdateNewsArticleForm the first pair of fields: publish_date and active are displayed in single column format next to each other, whereas the headline field is displayed spanning both columns.

The layout is controlled using a small set of CSS classes assigned to each field via the classes option. The two classes that control column layout are fluid-split and fluid-last. By assigning fluid-split to a field you indicate that you want it to take up only one column, and by assigning fluid-last to a column you indicate that no more fields should be displayed on the same row after this field.

The table below provides an overview of the form layout classes available in the default theme.

=================  ==============
CSS class          Description
=================  ==============
fluid-split        Set the field to display in a single column.
fluid-last         Set the field to be the last in the row.
fluid-field-*      Set the size of a field, where * is (half, quarter, eighth or sixteenth: half size is the same as a single column (fluid-split), every other size is then exactly half of its predecessor.
fluid-field-tall*  Set the height of a text area to be 'tall' (or 'taller'): fluid-field-tall sets it to twice the standard height, fluid-field-taller sets it to twice the height of field-tall.
=================  ==============

Our news article form should now look like the image below.

.. image:: ../../_static/plugin-news-form.jpg

As I alluded to at the start, taking a bit of time to question the order, position, layout, and type of fields you use is crucial to providing administrators with a good experience. As a real world example, last year I sat with an administrator who entered data off customer review cards every day. The existing CMS she used ordered the form fields differently to those on the review card. By providing them with a form with the same field order (and the nifty 'Save and new' button) she saved over an hour a day.


Why the hell doesn't any data show in my form field?
----------------------------------------------------

I'm not sure this deserves its own section, but it's such a common pitfall (one I'm not proud of) that I wanted to make sure people were aware of it.

If you try to use a model attribute that isn't a database Property or Proxy as a form field in the administration, then by default the field will never be populated with a value. Why? Because when we serialize the model to send the data back in JSON format, by default we only serialize model attributes that are instances of the Property or Proxy classes.

The solution is very simple; you need to add your custom property to the list of fields serialized using the model's meta class. Let's say we have a custom attribute full_name, which returns the combined first and last name of a customer. Below is how you would add that to the serialized data for the model;::

    class Customer(Model):
        class Meta:
            serialize_keys = ['full_name']

I'm sure we'll think of a better solution in future!


Showing new articles on the website
-----------------------------------

Now that we can add news article when in edit mode, we need to put in place some forefront views so that visitors to our website can view them. Add a couple of example news articles with different publish dates and then open the urls.py within the plugin folder and add the following code.::

    from deploy.core.urls import URL
    urls = (
        # View the latest news articles
        URL('/news/', 'plugins.news.views.news_articles'),

        # View news articles for a particular year and month
        URL('/news/(?P<yyyy>\d{4})/(?P<mm>\d{2})/', 'plugins.news.views.news_articles'),

        # View an individual news article
        URL('/news/(?P<yyyy>\d{4})/(?P<mm>\d{2})/(?P<slug>[\w-]+)/', 'plugins.news.views.news_article')
    )

Setting up a URL for a view is pretty straightforward, in the simplest of cases you just specify the URL and the path to the view. Dynamic URLs can be captured using regular expressions as is the case with our final two URLs where the year, month, and slug sections of the URL are not known in advance. Next we need to create the views we have referenced in our URLs; copy the following code into the views.py file within the plugin folder::

    from storm.expr import Func
    from storm.locals import *
    from deploy.config import settings
    from deploy.core.http.exceptions import PageNotFound
    from deploy.core.pagination import Paginator, InvalidPage, EmptyPage
    from deploy.shortcuts import render_page
    from plugins.news.models import *

    def news_articles(request, yyyy=None, mm=None):
        """
        Display a list of articles, either recent or for the
        specified year and month.
        """
        articles = None
        template_args = {}

        # Select the articles
        if yyyy and mm:
            # Show a specific month
            year = int(year)
            month = int(month)

            # Validate the month
            if month < 1 or month > 12:
                raise PageNotFound(request)

            articles = request.db.find(NewsArticle, And(
                NewsArticle.active == True,
                NewsArticle.publish_date <= Func('NOW'),
                Func('YEAR', NewsArticle.publish_date) == year,
                Func('MONTH', NewsArticle.publish_date) == month
            )).order_by(Asc('publish_date'))

            template_args.update(dict(month=month, year=year))
        else:

            # Show recent news
            articles = request.db.find(NewsArticle, And(
                NewsArticle.active == True,
                NewsArticle.publish_date <= Func('NOW'),
                NewsArticle.publish_date >=
                Func('DATE_SUB', Func('NOW'), 'INTERVAL 30 DAY'),
            )).order_by(Desc('publish_date'))

            # Paginate the results
            paginator = Paginator(articles, items_per_page=10)
            page = None
            page_no = request.params.get('page', '1')

            try:
                page = paginator.page(page_no)
            except (EmptyPage, InvalidPage), e:
                if page_no != '1':
                    raise PageNotFound(request)

            template_args.update(dict(
                   page=page,
                   paginator=paginator,
                   articles=articles
            ))

            return render_page(
                request,
                'articles.html',
                template_args
            )

    def news_article(request, yyyy, mm, article_slug):
        """Display an article."""

        article = request.db.find(NewsArticle, And(
            NewsArticle.active == True,
            NewsArticle.publish_date <= Func('NOW'),
            Func('YEAR', NewsArticle.publish_date) == year,
            Func('MONTH', NewsArticle.publish_date) == month,
            NewsArticle.slug == unicode(article_slug)
        )).one()

        if not article:
            raise PageNotFound(request)

        return render_page(
            request,
            'article.html',
            {'article': article}
        )

We have two views, one to list articles (news_articles) and one to display an individual article (news_article). The view news_articles has dual functionality depending on whether the year and month are included in the URL calling the view. If they are, it will display a list of news articles in the specified month, else it will show all news articles from the last 30 days, so is called recent news. To enable this behavior two URLs are mapped to news_articles:

- /news/, recent news.
- /news/(?P<yyyy>\d{4})/(?P<yyyy>\d{4})/, news for that month.

The second URL contains two regular expression groups:

- (?P<yyyy>\d{4}), requires 4 digits – this is the year.
- (?P<mm>\d{2}), requires 2 digits – this is the month.

The ?P<...> segment allows us to give the group a name which is then used as a keyword argument when calling the view. It's not necessary to name your groups, just good practice, if you don't name a group it will be sent through as an argument. The order of such arguments is defined by their position with the regular expression.

To obtain a list of news articles, either recent or for a specific month, we query the database using Storm ORM. At the current time the only consistent way to get a handle to the the database is by using the one associated with a request object. A request object is always the first argument sent to a view by a URL mapping. As I write this, work is already underway to rectify this situation and provide a more flexible approach.

Storm ORM is a large module on its own and I can't hope to cover it all here, but we will cover the find method which is the one we use here and the one you'll use the most. The find method lets us query a table on the database using our models to build a query. The first argument must be either:

- a model class (in this case NewsArticle)
- a single model class property (e.g. NewsArticle.headline)
- a list of model class properties (e.g. [NewsArticle.id, NewsArticle.headline])

If you provide a model class the results of the query will be returned as instances of that model, if not then the results are returned as a tuple of tuples.
The second argument to find is optionally a query to filter the results which are returned. Building queries in Storm is for the most part relatively easy; the simplest case is where you set the filter for a single column, such as::

    NewsArticle.slug == 'my-news-article'

or;::

    NewsArticle.publish_date > datetime(2010, 1, 1)

If you want to combine multiple filters together then I recommend you use the And and Or helpers provided by Storm, for example::

    And(
       Product.active == True,
       Product.title.like('%green%'),
       Or(
             Product.category == category,
             Product.on_special == True
    ))

.. note::
    You can also && (And) and || (Or) to add a logic gate, but to grouping is clearer, especially on more complex queries.

If you want to select across multiple tables you'll need to perform either a join or sub-select, for example::

    # Join - Select the supplier for a product
    supplier = request.db.find(
        Supplier,
        Product.supplier_id == Supplier.id,
        Product.code == 'MPMD1818'
    )

    # Sub-select - Select all blogs tagged as 'design' across 3 tables

    # Blog > Blog_Tag > Tag
    blogs = request.db.find(Blog,
        # Sub-select one
        Blog.id.is_in(Blog_Tag.blog_id,
            # Sub-select two
            Blog_Tag.tag_id.is_in(Tag.id, Tag.name == 'design')
        )
    )

Once you've selected your results you can order them using the order_by method and providing a list of column names or instances; you can change the order direction to descending using the Desc method. For example::

    products = request.db.find(Product).order_by(Product.price)
    blogs = request.db.find(Blog).order_by(Desc('publish_date'))
    retailer = request.db.find(Retailer).order_by(Desc('level'), 'distance')

.. note::
    Using column instances ensures that the table's name is used with the column, this can be useful if your column name happens to be a reserved word (so you shouldn't have this problem really), for example::

        # This will error
        banners = request.db.find(Banner).order_by('index')

        # This is safe
        banners = request.db.find(Banner).order_by(Banner.index)

If you want to limit the results returned you can treat the selected results similar to a list, for example::

    # Select the first 10 products
    products = request.db.find(Product).order_by(Product.price)[:10]

Unlike a list though, no error will be thrown if there are less than ten results to return - you simply get less than ten results. Just as with lists, you can also do::

    # Equivalent of LIMIT 10, 10
    blogs = request.db.find(Blog)[10:20]

    # Get the third result - this will fall over if there aren't three results.
    products = request.db.find(Product)[3]

If you're only expecting one result back from your query and instead of result set back you want a model instance you can use the one method, like so::

    product = request.db.find(Product, slug == 'air-filter').one()

If the query returns no results then one returns None, if more than one result is returned an error will be raised.

Right, back to our news plug-in. In the news_articles view, once we have selected news articles from the database, we next add support for paging using the Paginator class. Deploy provides a simple pagination library that works well with models, but you can of course use any library.

The Paginator is initialised with our list of articles (result set) and the number of articles we'd like displayed on each page. Next we check if a page number has been supplied by the visitor via the page request parameter, if not we default to page one, and then query the Paginator for our page of results. Parameters sent in a request are held in the request instance's params property which behaves like a dictionary for the purpose of reading from it.

When we query the Paginator for a page, we wrap our query in a try ... catch statement. The Paginator is designed to raise an error if the page is empty or if an invalid page number was provided. If the first page of our listing is empty then that's acceptable, we'll just show a page with no articles on it, if the visitor tries to access subsequent pages (e.g. 2, 3, 4, 5, etc.) which are empty then we'll return a 404 error by raising the PageNotFound exception. If an invalid page number is provided (e.g. 1.2, z, %$, etc) we always raise PageNotFound.

You might choose to handle these two situations differently, for example if an invalid page number is provided you might choose to return page one. The only hard and fast rule is that you must capture and handle the EmptyPage and InvalidPage exceptions.

With our news articles selected it's time to generate the HTML we'll be sending back to the visitor. Deploy provides a shortcut tool for rendering HTML and returning it as response, imaginatively called render_page. At the end of your views you'll typical call render_page with the path of the template you want rendered, and a dictionary of arguments you want available to the template.


Template engines and templating
-------------------------------

You're not restricted to a particular template language with Deploy, there's a selection of languages available as standard and it's possible to add new ones if you to use want something not included. Currently we support the following template languages out of the box (and the list is growing);

- ClearSilver (clearsilver)
- Jinja2 (jinja)
- Mako (mako)

You can choose which template language to use when you call the render_page function by using the keyword argument template_engine='mako'. However, by default render_page uses the first language in the site setting TEMPLATE_ENGINES, this is Jinja by default.

.. note::
    If you want to, you can have templates in different languages on the same website. However, I would advise against this, life is complicated enough!

Your templates can be located anywhere so long as you add the path to the site setting TEMPLATE_LOCATIONS. It's common to create a data/template directory in root of your site and place templates there. However, if you want to make your plug-in portable to other websites you can create a template directory within the plugin directory and keep related templates there. Deploy always checks for a template directory within the plugin directory so you don't need to add the location to TEMPLATE_LOCATIONS.

It's beyond the scope of this document, and to be honest my interest, to detail each template language. I'm going to choose Jinja2 for our news templates and stress that they are for illustrative purposes only. Examples of good practices for each template language can be found on their relevant websites.

We need to create a templates directory in our news plug-in directory, to it add the following files;

*articles.html*::

    <html>
        <head>
            <title>News</title>
        </head>
        <body>
            {% if page is not None %}
                {% for news_article in page.items %}
                <div class=”article”>
                    {% if len(news_article.image) > 0 %}
                        <div class=”image”>
                            <img src="{{news_article.image[0].variation('thumb')}}" alt="{{news_article.headline}}" />
                        </div>
                    {% endif %}
                    <div class=”text”>
                        <h2>
                            <a href=”{{news_article.complete_url}}”>{{news_article.headline}}</a>
                        </h2>
                        <div class=”date”>{{date|pretty_date}}</div>
                        <div class=”peek”>
                            {{news_article.body|abbreviate(150)|safe}}
                            <a href=”{{news_article.complete_url}}”>
                               read more.
                            </a>
                        </div>
                    </div>
                </div>
                {% endfor %}
            {% endif %}
            {% if page and page.has_other_pages() %}
                <div class="buttons">
                    {% if page.has_previous_page() %}
                        <a href="?page={{page.previous_page_number}}">Previous</a>
                    {% endif %}
                    <span class=”hide”>|</span>
                    {% if page.has_next_page() %}
                        <a href="?page={{page.next_page_number}}">Next</a>
                    {% endif %}
                </div>
            {% endif %}
        </body>
    </html>

*article.html*::

    <html>
    <head>
        <title>{{news_article.headline}}</title>
    </head>
    <body>
        <h1>{{news_article.headline}}</h1>
        {% if len(news_article.image) > 0 %}
            <div class=”image”>
                <img src="{{news_article.image[0].variation('large')}}" alt="{{news_article.headline}}" />
            </div>
        {% endif %}
        <div class=”story”>
            {{news_article.body|paragraphs}}
        </div>
    </body>
    </html>

If you're familiar with template languages the above should seem reasonably straightforward. However, for those who aren't, or those who haven't seen anything like Jinja2 before, I'll cover the basics.

{% ... %} is used to build blocks, for loops, if tests and so on, the syntax within is just Python. {{...}} is used to insert values into the template; the values need to either be in the global template data or in the data sent locally to the template (the template_args sent to render_page). If you want to call one or more functions against the value you're inserting then you use a vertical bar to separate the functions after the value. Each function will be sent the value as the first argument, followed by any additional arguments you specify e.g {{value|function1|function2(arg2, arg3)}}. The safe filter in our example is built into Jinja2, but to use abbreviate, paragraphs and pretty_date we need to change our site settings.py file and include them as filters for the template engine.

To add the filters we need to add the following code to your settings.py file;::

    import deploy.utils.formatting

    TEMPLATE_CONFIG = {
        'filters': {
            'abbreviate': deploy.utils.formatting.abbreviate,
            'paragraphs': deploy.utils.formatting.paragraphs,
            'pretty_date': deploy.utils.formatting.pretty_date
        }
    }

Something that won't be familiar from other CMS frameworks or template languages is the way we get image information. Although it's possible to obtain the original image path for our news article using article.image_path, more often we want the path to one of the variations we've defined; in our two templates we use the thumb variations on the listing page and the large variation on the view page.::

    <img src="{{article.image[0].variation('large')}}" alt="{{article.headline}}">

.. note::
    We recommend you don't access the attachment storage property directly, and certainly you should never try to update it, all this functionality is available via the higher level attachment property and your actions are guaranteed to be safe that way.

If you want to use meta data held against an attachment, such as an image, you can do so using the get_meta method against the attachment database record, as below::

    <img src="{{article.image[0].variation('large')}}" alt="{{article.image.all(request.db)[0].get_meta('alt')}}">

.. note::
    If you're using SQLite, at the current time, the code above will break due to threading issues. If you're using MySQL or PostgreSQL you won't have any issues.

That concludes our look at creating a news plug-in, however if you want to take things into “overtime” there are some shorter sections that look at how you can extend the news plug-in to support some of Deploy's other neat features.


Overtime – Model events
-----------------------

Most (if not all) of the overtime topics we're about to cover use model events to perform actions when a model instance (database record) is added, updated or removed. You can bind to add, update and remove events, at either the on or after phase, using the pydispatch.dispatcher module. We recommend that you use static methods against the model class to manage the code you bind to the events, for example::

    from pydispatch import dispatcher
    from storm.locals import *
    from deploy.core.models import Model

    class MyModel(Model):
        ...properties, methods...

        @staticmethod
        def _on_remove(signal, sender):
            db = Store.of(sender)

            ...some code that handles removing related records...

    # Events for MyModel
    dispatcher.connect(
        MyModel._on_remove,
        signal='Deploy.MyModel.on_remove'
    )

Adding events to models is pretty straightforward and vastly simplifies managing complex interactions. Within the _on_remove static method you'll notice we use the Store.of method to get a handle to the database, the only catch is that you can't guarantee a db handle will be returned, if for example the model instance is not associated with a database (think on_add).


Overtime – Caching
------------------

Caching the output of dynamic pages can offer massive performance benefits. Deploy provides a number of tools for caching pages and fragments within pages (covered in Overtime – snippets), best of all, in its simplest form all you have to do is decorate the view you want to cache. Say we wanted to cache a news article page, we could add the @cache decorator to the view like so::

    @cache
    def news_article(request, yyyy, mm, slug):
        ...code to view a news article...

That would permanently cache the page though, so any changes to our news article wouldn't be reflected, let's change that::

    import datetime

    @cache(expires=datetime.timedelta(hours=1))
    def news_article(request, yyyy, mm, slug):
        ...code to view a news article...

Now our cached news article page will expire every hour, so at the most we'll have to wait an hour for our changes to be visible. You can set the expires keyword value to any timedelta or datetime value (though datetime seems pointless to me; no idea why we added that).

OK, but what if we need the changes to appear instantly? That's where the validate keyword comes in handy, you can specify your own definition to validate if the cached version of the page has expired or not. For example::

    def cache_news_article(request, cache_response):
        # Find the news article
        match = re.match(
            '/news/(?P<year>\d{4})/(?P<month>\d{2})/(?P<slug>[\w-]+)/',
            request.path)

        if match:
            news_article = request.db.find(NewsArticle, And(
                NewsArticle.slug == unicode(match.group('slug')),
                Func('YEAR', NewsArticle.publish_date) == int(match.group('year')),
                Func('MONTH', NewsArticle.publish_date) == int(match.group('month'))
            )).one()

            if not news_article:
                return False

            # Check the last modified dates
            if news_article.last_modified > cache_response.last_modified:
                return False

            return True

    @cache(validate=cache_news_article)
    def news_article(request, yyyy, mm, slug):
        ...code to view a news article...

The most complex part of our validate code (cache_news_article) is the lookup for the news article record based on the calling URL; once we've got our news article we check to see if it has been updated since the last version of the cache was taken, if so we return False and the cached version of the page is said to have expired.

That's it! Go forth and make your sites faster.

Overtime – Snippets
-------------------

A snippet is a definition that returns a fragment of HTML. Deploy allows you to insert these snippets into your HTML pages by marking elements within the page with the attribute data-deploysnippet. Like pages, snippets can be easily cached using the cache decorator, making them ideal for dragging in large dynamically generated navigation.

To enable snippets on your website set the SNIPPETS_ENABLED setting in your site settings.py to True, then create a file named snippets.py in the site root. If it's not already included you will also need to add the snippets application to your pipeline; in applications.py insert (after your DeployCMSApp)::

    deploy_app = DeploySnippets(deploy_app, enabled=settings.SNIPPETS_ENABLED)

Let's stick with the navigation theme and create a snippet that will return the HTML for our website's primary menu and sub-menus; add the following code to your snippets.py file::

    import datetime
    from deploy.core.cache import cache_fragment
    from deploy.core.templates.snippets import __snippets__
    from deploy.core.templates import TemplateManager, get_template
    from plugins.navigation.models import MenuItem

    @cache_fragment(expires=datetime.timedelta(hours=2))
    def main_menu(request):
        # Select the primary menu items
        primary_menu = request.db.find(MenuItem,
            MenuItem.parent == None
        ).order_by(Menu.index)

        # Use the template manager to
        template_manager = TemplateManager(request)
        template = get_template('snippets/primary_menu.html')

        fragment = template_manager.render(template,
            data={'primary_menu': primary_menu}
        )

        return fragment

    __snippets__.register(main_menu)

Ok, so let's look at our snippet. The first line is a decorator cache_fragment designed for caching fragments of code, it works exactly the same as the standard cache decorator except that it is designed for storing only fragments (if you want to learn more about caching see the overtime Overtime – Caching).

Next we define our snippet definition, the name is important as it is used to include the snippet in our page. Our snippet code is straightforward; we select the top level menu items and use the TemplateManager to render the HTML for the primary menu (yes you have to use your imagination a little to see that primary menu but trust me, it's a work of art). Once rendered we return the HTML, snippets must return a unicode value.

After we define our snippet we need to register it using __snippets__.register, and that's it; we're ready to insert it into our pages. To include it in our template we do the following::

    <html>
    <head>
        <title>My template</title>
    </head>
    <body>
        <header>
            <img src=”/media/logo.png” alt=”My compant”>
            <nav id=”primary-nav” deploysnippet="search_trades_snippet"></nav>
        </header>
        <section>
            ...page content...
        </section>
        <footer>
            ...footer content...

        </footer>
    </body>
    </html>

Snippets are a great way to add content into multiple pages across your website where a traditionally static template include isn't appropriate due to the data- driven and possibly dynamic nature of the content being included.


Overtime – Linkable
-------------------

If the model you're creating can be viewed in its own page it's useful to register the URL with the linkable database. Doing so allows users to select the link from the autocomplete results of a search (see below) whilst editing a page.

.. image:: ../../_static/cms-linkable.jpg

To keep the linkable database in sync with the your model's URL you need to use the model's event handlers like so::

    from deploy.cms.linkable import subscribe_linkable, unsubscribe_linkable

    class MyModel(Model):
        ...properties, methods...

        # Linkable properties
        @property
        def __linkable_id__(self):
            # The URL for the model instance
            return '/my-model/%s/' % self.slug

        @property
        def __linkable_title__(self):
            # What the user is shown in the autocomplete
            return self.name

        @property
        def __linkable_description__(self):
            # What we search against
            return self.name

    # Linkable event handlers
    dispatcher.connect(subscribe_linkable, signal='Deploy.MyModel.after_add')
    dispatcher.connect(subscribe_linkable, signal='Deploy.MyModel.after_update')
    dispatcher.connect(unsubscribe_linkable, signal='Deploy.MyModel.after_remove')

With the additional properties and event bindings we're good to go. Future model add, updates and removes will be registered with the linkable database. To test your newly linkable model add a new model or update an existing one; start editing any page; select some text and click the 'create a link' tool; type some text contained in your model's description (in this case the name property) and an option to link to your model should appear.

.. note::
    It's good practice to namespace your link titles, for example if you have a news plug-in then you might return the article's title followed by '(News)', e.g 'New contract with GMC (News)'.


Overtime – In-page editable
---------------------------

One of Deploy's best features is its in-page editing environment; if a model has its own page per instance, chances are you'll want to give your editors the option to edit the model's main content in- page. In-page editing allows your editors to build rich pages that include images, movies, lists and tables in a UI as familiar as a word processor; best of all you don't have to worry about any of the complications, it's all taken care of for you.

Let's use the news plug-in we built earlier as a basis and update it to support in- page editing. We start with the model; update the model.py file as follows::

    from storm.locals import *
    from deploy.cms.attachments import Attachments
    from deploy.core.models import Model

    __all__ = ('NewsArticle',)

    class NewsArticle(Model):
        """News article model."""

        __storm_table__ = 'NewsArticle'

        id = Int(primary=True)
        active = Bool()
        publish_date = DateTime()
        headline = Unicode()
        slug = Unicode()
        body = Unicode()
        image_paths = Unicode()
        body_image_paths = Unicode()

        @property
        def draft(self):
            return self.publish_date > datetime.date.today() or not self.active

        @property
        def complete_url(self):
            return '/news/%d/%02d/%s' % (
                self.publish_date.year,
                self.publish_date.month,
                self.slug
            )

        def __unicode__(self):
            return u'News article: %s' % self.headline

    NewsArticle.images = Attachments(
        NewsArticle,
        NewsArticle.image_paths,
        'media/img/news',
        max=4,
        variations={
            'thumb': {'process': 'variations.fit', 'args': [[75, 75]]},
            'large': {'process': 'variations.fit', 'args': [[265, 265]]}
        }
    )

    NewsArticle.body_images = Attachments(
        NewsArticle,
        NewsArticle.body_image_paths,
        'media/img/news',
        variations={
            'inpage': {'process': 'deploy.cms.utils.variations.inpage_attachment'}
        }
    )

We've added an extra attachment field so that we can store any images inserted into the body of the article, and a draft property that we'll use later in the forefront templates to indicate whether the article is visible on the live version of the website. Next we need to update our cms.py file; add the following form to validate our in-line page changes::

    class InpageNewsArticleForm(Form):
        body = HiddenField()

set the gui_inpage_editing setting for your controller to True;::

    from deploy.cms.utils.mixins import InpageCMSMixin

    class NewsArticleController(CMSController, InpageCMSMixin):
        """CMS controller for news articles."""

        class Meta(CMSController.Meta):
            model = NewsArticle
            controller_name = 'plugin-news-article'
            gui = True
            gui_inpage_editing = True

            forms = {
                'ADD': AddUpdateNewsArticleForm,
                'UPDATE': AddUpdateNewsArticleForm,
                'INPAGE': InpageNewsArticleForm
            }

            ...the rest of your controller code...

add a reverse URL lookup to the controller::

    @is_logged_in(permission_required='EDITOR')
    def get_id_for_url(self, request, administrator):
        fail_response = JSONFailResponse(request, args={
            'msg': 'Object could not be found for URL.',
            'code': 'OBJECT_NOT_FOUND',
        })

        url = request.params.get('url')
        match = re.match('/news/(?P<year>\d{4})/(?P<month>\d{2})/(?P<slug>[\w-]+)/', url)

        if match:
            news_article = request.db.find(NewsArticle, And(
                NewsArticle.slug == unicode(match.group('slug')),
                Func('YEAR', NewsArticle.publish_date) == int(match.group('year')),
                Func('MONTH', NewsArticle.publish_date) == int(match.group('month'))
            )).one()

            if not news_article:
                return fail_response

            return JSONSuccessResponse(request, args={'msg': 'Got id.', 'id': news_article.id})

        return fail_response

and set the NewsArticle controller as the master controller for your URLs (replace your URL file contents with the code below)::

    from deploy.cms.urls import CMSURL
    from plugins.news.cms import NewsArticleController

    __all__ = []

    urls = (
        CMSURL('/news/', 'plugins.news.views.news_articles', controller=NewsArticleController.Meta.controller_name),
        CMSURL('/news/(?P<year>\d{4})/(?P<month>\d{2})/', 'plugins.news.views.news_articles', controller=NewsArticleController.Meta.controller_name),
        CMSURL('/news/(?P<year>\d{4})/(?P<month>\d{2})/(?P<slug>[\w-]+)/', 'plugins.news.views.news_article', controller=NewsArticleController.Meta.controller_name)
    )

That's everything ready from the CMS perspective, however one change we need to make to our forefront views is to allow articles that are not yet live on the website to be viewed in edit mode so we can still make changes to them. In the find(...) calls we make to select our news articles in the forefront views we use the following two conditions::

    NewsArticle.active == True,
    NewsArticle.publish_date <= Func('NOW')

replace these with the following Or condition; this effectively only applies the publish rules if we're not in edit mode::

    Or(
        And(
            NewsArticle.active == True,
            NewsArticle.publish_date <= Func('NOW')
        ),
        request.mode == “EDIT” # Draft mode
    )

.. note::
    The draft property we added to our NewsArticle model earlier can now be used to indicate to editors viewing the website that the article is live or draft.

The final stage of adding in-page editing to our news plug-in is to update the news_article.html template and mark the editable regions; in this case we just have one, the body::

    <html>
        <head>
            <title>{{news_article.headline}}</title>
        </head>
        <body>
            <h1>{{news_article .headline}}</h1>
            {% if len(news_article.image) > 0 %}
                <div class=”image”>
                    <img src="{{news_article.image[0].variation('large')}}" alt="{{news_article.headline}}">
                </div>
            {% endif %}
            <div
                id=”body”
                class=”story edit-local”
                data-edit-store-content="body"
                data-edit-store-images="body_images">
                {% if news_article.body %}
                    {{news_article.body|safe}}
                {% else %}
                    <p class="edit-class-Text">Start your article...</p>
                {% endif %}
            </div>
        </body>
    </html>

We have to tell the page editor which parts of our page are editable; to do that we use the CSS class edit-local, then because we are editing a model and not a page we have to define which property text and image changes will be applied, using the data-edit-store-content and data-edit-store-images attributes.

With our changes in place, visit the website in edit mode and navigate to a news article. You should be able to click the 'Edit page' button left-most on the application bar, click on the body of your article and start editing the page.

.. note::
    If you are converting your news article to be in-page editable then the existing body content won't work as it will just be plain text and not the marked-up HTML in-page editing requires. You will need to convert or delete existing articles beforehand.

    Since you can copy and paste using the in-page editor, saving the news article records from the database into a CSV file, setting all the body values to an empty string, and then repasting the article bodies is an option if you only have a handful of articles.

You can find out more about what's possible with in-page editing in the Deploy user interface guide for developers.

Overtime – Searchable
---------------------

This overtime is really nothing to do with Deploy; there is no set way to make your models searchable in Deploy. However, making it easy for visitors to search for stuff is a common task when building a website and so I wanted to provide a guide that showed at least one way of doing this using one of the Deploy dependencies - Whoosh.

Whoosh is a search server that builds a separate search database/index that is optimised for performing searches. You could actually use it as your database, but it's not really designed for that and though I'm always tempted to try for fun I won't force you to take that journey with me. For more information about Whoosh check out http://packages.python.org/Whoosh/.

We're going to create a search for our news articles; to do this we'll need to update the models.py and views.py file. For the sake of keeping things as simple as possible we're only going to add the title of our news article to the searchable content, but in a more practical example you would strip the HTML from your news article body.

First we need to bind add, update, and remove events for our model to update our Whoosh database. Add the following event bindings to the NewsArticle model (models.py)::

    import os
    from HTMLParser import HTMLParser
    from whoosh.index import create_in, exists_in, open_dir
    from whoosh.fields import *
    from whoosh.writing import AsyncWriter

    def strip_tags(html):
      """Strip HTML tags from a string – credit to http://stackoverflow.com/a/925630"""

      class HTMLStripper(HTMLParser):
        def __init__(self):
          self.reset()
          self.fed = []

        def handle_data(self, d):
          self.fed.append(d)

        def get_data(self):
          return ''.join(self.fed)

      s = HTMLStripper()
      s.feed(html)
      return s.get_data()

    class NewsArticle(Model):
      # Define search database
      schema = Schema(
          id=ID(unique=True, stored=True),
          headline=TEXT(),
          body=TEXT()
      )

      def __init__(self, *args, **kw):
        if not exists_in(os.path.join(settings.SITE_ROOT, '/data/search/'), indexname='news'):

          # Create search database
          if not os.path.exists(db_path):
              os.mkdir(db_path)

              # Create the linkable index
              create_in(
                  db_path,
                  self.schema,
                  indexname='news'
              )

          super(NewsArticle, self).__init__(*args, **kw)

      # ...existing model properties and methods...

      # Event methods for updating search database
      @staticmethod
      def _add_search(signal, sender):
        index = open_dir(
            os.path.join(settings.SITE_ROOT, '/data/search/'),
            indexname='news'
        )

        writer = AsyncWriter(index)

        # Add to search db
        writer.add_document(
            id=unicode(sender.id),
            title=sender.title,
            body=strip_tags(unicode(body))
        )

        writer.commit()

      @staticmethod
      def _update_search(signal, sender):
        index = open_dir(
            os.path.join(settings.SITE_ROOT, '/data/search/'),
            indexname='news'
        )

        writer = AsyncWriter(index)

        # Remove from search db
        writer.delete_by_term('id', unicode(sender.id))

        # (Re)Add to search db
        writer.add_document(
            id=unicode(sender.id),
            title=sender.title,
            body=strip_tags(unicode(body))
        )

        writer.commit()

      @staticmethod
      def _remove_search(signal, sender):
        index = open_dir(
            os.path.join(settings.SITE_ROOT, '/data/search/'),
            indexname='news'
        )

        writer = AsyncWriter(index)

        # Remove from search db
        writer.delete_by_term('id', unicode(sender.id))
        writer.commit()

    # Search event handlers
    dispatcher.connect(NewsArticle._add_search,  signal='Deploy.NewsArticle.after_add')
    dispatcher.connect(NewsArticle._update_search, signal='Deploy.NewsArticle.after_update')
    dispatcher.connect(NewsArticle._remove_search, signal='Deploy.NewsArticle.after_remove')


With your NewsArticle model updated now whenever you add, update or remove an article the search database will also be updated. Time to give your visitors the ability to search your articles, add the following code to your views.py::

    from whoosh.qparser import MultifieldParser

    def news_search(request):
        """Search news articles"""

        # Search against the search database
        db_path = os.path.join(settings.SITE_ROOT, '/data/search/')
        index = open_dir(db_path)
        news_article_ids = []

        with index.searcher() as searcher:
          query_parser = MultifieldParser([
              'headline',
              'body'
              ], NewsArticle.schema())

          # Filter non-alphanumerics out of keyword search
          keywords = request.params.get('keywords', '')
          keywords = re.sub('\W+', ' ', keywords).strip()

          # Perform search
          query = query_parser.parse(keywords)
          results = searcher.search(query, limit=50)
          news_article_ids = [int(hit['id']) for hit in results]

          # Select the news articles
          news_articles = request.db.find(NewsArticle,
              NewsArticle.id.is_in(news_article_ids)
          )

          news_article_index = {}

          for news_article in news_articles:
            news_article_index[news_article.id] = news_article

          # Order the results based on the ids we have
          news_articles = [news_article_index[news_article_id] for news_article_id in news_article_ids]

        return render_page(request, 'employee/employees.html', {
            'news_articles': news_articles,
            'news_article_count': len(news_articles),
            'keywords': request.params.get('keywords', '')
        })

We need to update our URLs to support the new view; add the following to your urls tuple within urls.py::

    CMSURL('/news/search/', 'plugins.news.views.news_search', controller=NewsArticleController.Meta.controller_name),

Finally we need a template to display the results. Create a new file in the plugins template folder named news_search.html and add the following code to it::

    <html>
    <head>
        <title>News search</title>
    </head>
    <body>
        <h1>News search</h1>
        {% if news_articles|len %}
            {% for news_article in news_articles %}
            <div class=”article”>
                {% if len(news_article.image) > 0 %}
                    <div class=”image”>
                        <img src="{{news_article.image[0].variation('thumb')}}" alt="{{news_article.headline}}">
                    </div>
                {% endif %}
                <div class=”text”>
                    <h2>
                        <a href=”{{news_article.complete_url}}”>
                            {{news_article.headline}}
                        </a>
                    </h2>
                    <div class=”date”>{{date|pretty_date}}</div>
                    <div class=”peek”>
                        {{news_article.body|abbreviate(150)|safe}}
                        <a href=”{{news_article.complete_url}}”>
                            read more.
                        </a>
                    </div>
                </div>
            </div>
            {% endfor %}
        {% endif %}
    </body>
    </html>

Now our visitors can search the news; I bet they're thrilled.

**That's the last of our overtimes (boo), but I hope this helped make Deploy a little more familiar and something you can't wait to use for real. I'm always available to chat about anything Deploy related so if you have a question or suggestion email me at ant@getme.co.uk.**
