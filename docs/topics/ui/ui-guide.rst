Deploy user interface guide for JavaScript developers
=====================================================

In this brief guide we look at the user interface provided by Deploy for website administrators. We're going to start with an overview of its make up, then look at some of the common ways that you'll be customising it to add additional functionality and customise the look and feel.

An intermediate knowledge of HTML, CSS and JavaScript is required for the material within this guide. It's a good idea to sign-in to the administration area of your website now so that you can refer to it whilst reading this guide. Typically you'll sign-in using the edit subdomain of your website's domain, e.g. *edit.myawesomesite.com*.

JavaScript libraries
--------------------

There are four JavaScript libraries at the heart of the UI. We'll be getting intimate with them later in the guide, but for now it will suffice to know their names and purpose.

- core – Contains the core classes and functions shared across all other libraries.
- fluid – Provides classes and functions for managing the user interface.
- editable – Provides classes and functions for managing editable content, typically within pages.
- deploy – Provides tools for interacting with Deploy server-side controllers.

.. note::
    The underlying framework is Jquery, however it is namespaced to use double dollars ($$) instead of the default single dollar ($) to avoid conflicts with local website versions.

The interface is split into multiple layers
-------------------------------------------

When you sign-in to the administration area of your Deploy website a welcome notice will appear in the centre of the screen. In my case it says 'Welcome Anthony'. A few seconds later it disappears to reveal the website and a toolbar at the top of the page – this is the administrator's desktop.

The interface is split into 4 different levels, detailed in the table below.

=============  ==============
interface      Description
=============  ==============
The web page   At this level interaction is directly with the website content itself, e.g moving an image somewhere else on the page.
Modeless       The Modeless layer sits above the web page. The primary application bar and other tools that are available when interacting with the web page layer reside in this layer, e.g a tool window for editing text on a page.
Modal          The modal layer sits above the Modeless layer. When the modal layer is displayed, it's no longer possible to interact with the layers below (the web page or Modeless layer). The Modal layer provides an environment for windowed applications such as listing products or accepting an order. Multiple windows, and therefore applications, can be open at once.
Single modal   The Single modal layer sits above all the other layers. We saw it earlier when the welcome message was displayed. It's used for situations where a temporary modal state needs to be achieved such as displaying a notification, drop-down menu, date picker, etc. Like the Modal layer, the Single modal layer prevents interaction with the layers below it. However, as standard, if the user clicks on the Single modal layer it will automatically hide itself and allow interaction with the layers below again.
=============  ==============

At any one time, the user interface is said to be in one of 3 modes; Modeless, Modal or Single modal. Most of the time creating your own application interface won't involve manually controlling these different modes - the classes provided by the Fluid and Deploy JavaScript libraries will generally handle these mode switches automatically.

The interface is made of widgets
--------------------------------

If you haven't already guessed, widgets are what make up the user interface, from the lowly panel or label to the mighty toolbar or window. The Fluid library uses a mixture of approaches for creating more complex widgets. For extendability, widgets can easily be mixed-in, and can also manage one or more groups of their own widgets in a parent > children relationship. Often, instead of using the parent > child relationship for common parts of a widget, like the label in the titlebar of a window, this element is optimised directly into the template for the widget for performance reasons. We'll cover templates next, but the important thing to remember is that this optimisation is there and the child > parent relationship is guaranteed to exist.

As I mentioned, every widget type has a template, which makes sense because at their core each of them is just a segment of HTML, formatted in CSS, and scripted to behave in a certain way by JavaScript. Although a basic template is defined for every type of widget, this template (and the CSS and JavaScript that controls the layout) can be customised through different themes. Through themes it is possible to completely change the look and feel of the interface. That's not to say you should; most of the time 'white labelling' the interface can be achieved purely through simple CSS changes.

Just like HTML elements, widgets each listen out for a set of predetermined events, such as clicking, mouse move, etc. Many of them support additional events too, and unlike HTML they can listen for on and after event phases.

I want to avoid getting too bogged down in the nitty-gritty of widgets; most of the time you'll only interact with a handful of them even when adding entirely new application interfaces. For now, suffice to say that there are a lot of them. But buttons and menus, which we provide examples for later, are your best friends and for anything more complex. The reference documentation for each is held against the code.


The interface is event driven
-----------------------------

One of the most challenging aspects of writing for the user interface is adjusting to the event-driven flow instead of the more typically linear flow of server-side code. I'm going to use querying information as an example to highlight this.
Let's look at the a linear flow first. For our Python example we'll query the database for a list of products::

    def products(request):
        """Display a list of products"""
        products = request.db.find(Product).order_by('price')
        return render_page(request, 'shop/products.html', {
            'products': products
        })

In the above example we do two things when the products function is called. First we select a list of products from the database (well, we generate a query that will do this when it needs to), second we render a template passing the product information to the render function. We can assume that these two events are always called in this order, in linear order. In the browser environment we can't.

Now lets look at a similar scenario in JavaScript, in this case we're loading some product information over an AJAX connection::

    /* Get a list of products */
    var productsReq = new IO.Request({
        controller: 'product',
        method: 'all',
        type: 'GET'
    });

    productsReq.bind('afterSuccess', function (event, data) {
        var products = event.data.data.products;
        console.log(products);
    });

    productsReq.send();

This isn't something you'd typically do in JavaScript but I've used the example because it's similar to its Python counterpart. You can see that the console.log(products); line, which is our equivalent to render_page in the previous example, comes before we send our request to the server. The command to output the products to the browser console is placed within an event binding for the request object. The binding means that when a successful response is received from the server the code within the binding runs. The productsReq object itself doesn't simply return a list of products when send is called, unlike its db.find equivalent, the code execution continues after send is called and at some undetermined point in the future the request resolves and triggers various events which we can listen for.

For single cases like the example above this isn't too difficult to get your head around, however it can get a lot more confusing when multiple events are being chained together. The next example handles two events in a chain, requesting some information and then opening a window to display the information::

    /* Show some product information in a window */
    var win = new FLUID.Window({
        label: 'Unknown at the moment',
        size: [400, 300],
        flags: FLUID.Window.CAN_CLOSE | FLUID.Window.CENTERED
    });

    win.bind('onOpenComplete', function (event, data) {
        /* This time just get one product */
        var productReq = new IO.Request({
            controller: 'product',
            method: 'get',
            type: 'GET',
            data: {'id': 1}
        });

        productReq.bind('afterSuccess', function (event, data) {
            var win = data.win;
            var product = event.data.data.product;
            // Set the title
            win.label(product.name);
            // Create some other widgets to show product data
            // ...
        }, {win: this});

        /* This triggers the second event */
        productReq.send();
    });

    /* This will trigger the first and our series of events */
    win.open();

Again this example is not entirely practical, but for the sake of simplicity it illustrates how chained events stack up, and the challenges of an event-driven environment as opposed to one that is linear.

.. note::
    This is an appropriate time to highlight a frequent error made by novice and expert developers alike, that of incorrect variable scoping. Not only is the error very easy to make, it's often incredibly difficult to pin down, especially in a series of chained events. In complex environments such as as user interfaces, such errors often show themselves as difficult to reproduce and seemingly random issues.

If you aren't aware of the unique scoping features of JavaScript and the importance of using the var keyword then you need to go and read up. If you are then here are some common pitfalls to be aware of when coding and debugging.

**Not declaring variables within the binding function** - Since it's common in JavaScript to have a binding function in a function it's easy to forget that the variables you use within that binding function should be declared at the top. Here's an example of a common issue::

    function foo () {
        var win = > setup the window <;
         foo.bind('onSomeEvent', function (event, data) {
             win.close();
         }, {win: win});
    }

Here the win variable is referenced in the function bound to 'onSomeEvent'. Because it's not declared it shares its scope with the outer function. The problem is we can't guarantee what win references at the point it's used in win.close(); because we don't know when in the future it might be called.

We are passing a reference to win in the event binding using {win: win} as the final argument. This allows us to safely access win now using data.win. So to make the code safe in our event binding we might change it to::

    var win = data.win;
    win.close();

**Incorrect context for this** (this is especially relevant when copy and pasting code to new areas) – The this keyword in JavaScript can actually refer to almost anything when event binding comes into play. However our events system follows the norm, and unless you override that behavior, it will refer to the object that the event is triggered for.

It's easy to miss which context you're in, especially when copy and pasting code from one section to another.
Since all interface widgets are mixed from the same base class they often share functionality, and incorrect calls to this can result in subtle bugs that aren't reported to the console.

When copy and pasting event bindings be sure to check that any data passed in is still relevant, for example::

    foo.bind('onSomeEvent', function (event, data) {
        win.close();
    }, {win: this});

The context of this when passed into the event binding clearly refers to a window. If you copy and paste this code somewhere else you need to be sure it's in the same context, or change the variable passed to the binding.


Up until this point we've concentrated on rather abstract aspects of the user interface (layers, widgets & events). I promise it wasn't just for kicks, understanding these aspects will be paramount when you start modifying the interface's look and behavior. But now it's time to take a step back and look at real interface applications.

Customizing the interface
-------------------------

In this section we're going to look at how we can change the user interface and work through the code for adding a button to download a PDF invoice for a web order.

Deploy's CRUD support (Create, Read, Update & Delete)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As you'd expect from a web framework, Deploy provides you with administration support for the common actions you'd perform against a model, including *;

- List
- Add
- View/Update
- Delete

The Deploy framework allows you to define a controller for any model you define, and by default that controller will have a set of basic views. When the user interface initialises itself the server provides it with information on the website's controllers and supported views, so by default you don't need to do anything. When the interface loads, any controllers you have defined will appear under a grouping in the Plug-ins menu, for example to list the product categories on the website the menu path might be:

*Plug-ins > Catalogue > Categories*

Selecting the Categories menu item will set the interface mode to Modal and open a window displaying a list of the categories currently on the database. Sweet huh?

.. note::
    In edit mode the app bar is always visible; it takes up 45 pixels at the top of the page which can mean it covers up navigation elements. A common issue is member links such as 'Sign-in' which are often located top right. To solve this issue we recommend that you add a small style block that margins the body from the top of the page when in edit mode, like so::

        {% if request.mode == 'EDIT' %}
            <!-- Push page below the app bar -->
            <style type="text/css">body { margin-top: 45px; }</style>
        {% endif %}

.. note::
    * There are also placeholder views for export/import. Whilst you need to override these placeholders server-side, the forefront provides UI buttons for them if they are defined.

Changing interface behaviour
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Despite support for CRUD, there are often cases where you might need to add additional views or change the behaviour of the existing ones. To learn how we might go about doing this we're going to look at adding a button to a web order view that allows us to download a PDF invoice.

**LAYING THE FOUNDATIONS**
Before we can change the behaviour of the interface we need to define our own controller for web orders. The layout of a default JavaScript install will look something like below:

- www
    - media
        - edit
            - js
                - core
                - deploy
                - editable
                - fluid

First we need to add our own folder within the js folder; it doesn't matter what it's called but a good convention is to use the name of the website, which for our example is cool-shoes.co.uk, so we'll name our folder coolshoes. Into our new folder we add base.js. The base.js JavaScript file is where we define our JavaScript namespace which all our plug-ins will use, it's also where we'd define any settings or constants. The contents of base.js should look something like::

    /**
     * The cool-shoes.co.uk namespace.
     * @final
     * @type Namespace
    */
    var COOLSHOES = {};

With our namespace defined, next we add weborder.js to js/coolshoes. The file name again isn't crucial, but convention dictates that we use the name of the plug-in.

At this point you might be wondering how Deploy will know to load these new files when we browse the website in edit mode? Well, you have to tell it. In the Python folder for the website create a resources.py file (if one doesn't already exist), and add the following to it::

    from deploy.core.resources import ResourceList
    from deploy.core.resources.types import JSResource

    CMS_JS_RESOURCES = ResourceList(
        JSResource('/media/edit/js/coolshoes/base.js'),
        JSResource('/media/edit/js/coolshoes/weborder.js')
    )

.. note::
    Restart deployserver after making the Python changes.

The Deploy framework looks for resources.py files in both the top level directory and in plug-in directories, if it finds them it adds the list of resources it loads in edit mode. Defining resources in this way allows Deploy to do some cool things for you like automatically minimising and concatenating JavaScript files, but that's beyond the scope of this guide so for now let's get back to our PDF invoice button.


**DEFINING A CONTROLLER**
OK, so now when we visit the website in edit mode our new base.js and weborder.js files get loaded, but that's about all that happens. So far, other than define a new namespace, we're not doing anything – let's change that by overriding the default controller for web orders.

To create our controller add the following code to the weborder.js::

    /* Define our web order controller class */
    COOLSHOES.WebOrderController = function (options) {
        options = UTILS.optionsArg(options, this.defaultOptions);

        DEPLOY.Controller.call(this, {
                 name: 'plugin-store-web-order',
                 verbose_name: 'web order',
                 verbose_name_plural: 'web orders'
        });

        console.log('Custom web order controller was initialized');
    };

    /* Mix-in the default controller */
    UTILS.copyPrototype(
        COOLSHOES.WebOrderController,
        DEPLOY.Controller
    );

    /* Extend the web order controller */
    $$.extend(COOLSHOES.WebOrderController.prototype, {
        __className__: 'COOLSHOES.WebOrderController'
        // Add custom views here
    });

    /*
     Create an instance of our web order controller, doing this will
     also register the controller so that it is used.
     */
    new COOLSHOES.WebOrderController();

Now we've defined and registered our new controller when we load a page in edit mode, we should get a message in our console telling us the 'Custom web order controller was initialized'.

.. note::
    The controller's name must tie in with the name the controller has been assigned server-side.

Now we've connected with the interface via our custom controller we can add our button. Inside our controller class definition replace the console.log('...'); line with the the following code::

    /* Add 'Download PDF' button to the view */
    this.bind('onUpdateOpen', function (event) {
        /* Get the window */
        var win = FLUID.widgetByName(
            'deploy-window-update-' + this.getName()
        );

        /*
        Show the action area which is where our button will be
        located.
        */
        win.flags(win.flags() | FLUID.Window.HAS_ACTIONS);

        /* Create our new button */
        var downloadBtn = new FLUID.Button({
            parent: win,
            group: 'actions',
            label: _('Download PDF')
        });

        /* When the button is clicked call our PDF download view */
        downloadBtn.bind('onClick', function (event, data) {
            if (this.enabled()) {
                data.ctr.downloadPDF(data.id);
            }
        }, {ctr: this, id: win.widgetData('id')});
    });

If you load your page again you should now see our 'Download PDF' button in the bottom left corner of the window? Sweet! OK we've covered quite a lot here, and there's definitely some magic going on so let's look at what each line is doing.

Our very first line is an event binding that says execute our add button code each time the 'update web order' view is called and the window opens (onUpdateOpen). Since the binding is against the on phase, the code will run before the window is opened, which is what we want.

The next line gets the window instance using FLUID.widgetByName('...');. Widgets can be given names when they are created (think ID's for HTML elements) which makes them easy to get. Deploy uses a set standard for naming key widgets such as the windows it creates for the build views. In this case we want the update window so the name is 'deploy-window-update-' followed by the controller's name.

Once we have the window instance, we set its flags to ensure that the actions area of the window is visible. Windows have three child areas, but by default only the main client area is visible. The other two areas are the info and the action area. The info area is yellow by default and located at the top of the window, the action area is blue by default and located at the bottom of the window. The screen grab below shows our window with its download PDF icon.

.. image:: ../../_static/plugin-weborder.jpg

With our window flag set to show the action area we can add our download button. In creating the download button we specify three values: the parent which is the window, the group which is the action area and the label which is the text that will appear in the button, which in this case is 'Download PDF'. Many of you will be familiar with the use of the underscore function to wrap strings _('...'). For those who aren't, the underscore function is used to translate strings to different languages. Marking strings in this way not only helps to make them easy to find in code, and therefore build a translation table, but also performs the conversion.

Finally, with our button added, we want to capture whenever a user clicks it. The binding code captures the onClick event, and provides the controller and the ID of the current order as shared data for the event. When the button is clicked our yet to be defined view downloadPDF is called.

The ID for the current order was obtained using win.widgetData('id'); the widgetData method is available against all interface widgets and allows you to store and retrieve data against a widget using a key. As a standard, Deploy stores the ID of the record it is displaying against the window. You will find that Deploy controllers often use widgetData to store useful information against the window for a view. Usually the information can be obtained in another way but this provides a useful shortcut.

It's time to define our custom view that will download the PDF. Replace the comment Add custom views here with the following code::

    downloadPDF: function (id) {
        /* Create the URL */
        var downloadURL = IO.Request.buildURL(
            this.getName(),
            'download-pdf'
        );

        /* Append the arguments to the PDF */
        downloadURL += '?@=' + IO.Request.getId() + '&_id=' + id;

        /* Point the window at the download URL */
        window.location = downloadURL;
    }

.. note::
    Make sure you add a comma at the end of the first line in the block __className__: 'COOLSHOES.WebOrderController'.

Our custom view is very simple; it generates a URL to a server-side view that will output a PDF of the invoice. Once the URL is created we point the browser window at the URL using location property and, hey-presto, a file download dialog will appear.

The URL is generated using the IO.Request class. All IO with the server should be handled using the IO library within the core JavaScript. Although its usage in our downloadPDF view isn't typical, it does show some useful principals.
Most communications between client and server in Deploy are performed via AJAX and the IO.Request and IO.Response classes handle this. However, in this case we are performing a direct GET to download the PDF and so we are only using the IO.Request class to help us generate the URL.

First we call the buildURL class method, parsing in the name of our controller and the view we are calling. This will correlate directly with Python method against the controller server-side. The dash (-) in the view name will be converted to an underscore server-side. Our returned URL will look something like /edit/plugin-store-web-order/download-pdf/. With our URL ready, we then append two additional arguments, the first represented by @ and the second id. The id param refers to the web order we are downloading a PDF invoice for. The @ is a special case parameter that must always be provided by the client to the server when making a request. It is used to log and validate requests and responses. Usually this value is generated automatically when you make a request using the IO library but in this case, as we are manually building the URL parameters, we have to add it ourselves using the call to IO.Request.getId().

With our new view in place our users can download a PDF of a web order via the interface, and that pretty well concludes our very brief introduction to customising the user interface save for one last topic – user permissions.


What if I don't want everyone to be able to download the PDF invoice?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Up to now we haven't broached the subject of user permissions; the code we just implemented assumes that anyone with administration access to the website can download an invoice PDF. But what if we wanted to limit the administrators who could download the PDF to a set group of users?

By default Deploy provides two user groups: Developers and Editors. Users are assigned to one of these groups when they are created, and as a developer – guess what – your account will almost certainly belong to the Developers group. Developers can do anything, we don't limit them. Editors, on the other hand, are limited in what they can do, for example an Editor can't change another administrator's account (though they can change their own).

If we wanted to prevent the Editors from downloading a PDF invoice we would decorate the view in Python like so::

    @is_logged_in(permission_required='DEVELOPER')
    def download_pdf(self, request, administrator):
        """Download a PDF for the web order"""
        # Code goes here
        pass

Unless you decorate the method of your controller in Python with @is_logged_in then it won't be callable. The permission_required allows you to specify which group of users can call the view. It is possible to write far more complex access rules, but that's beyond the scope of this guide, so for now we'll assume the easy case – that we just don't want Editors to be able to call this view.

Decorating our Python view will ensure that Editors get a 'permission denied' alert now, when they click our download PDF button, but that's a bit shoddy. What we want to be able to do is disable or hide the button so that Editors don't have to be informed by an alert box.

The following code checks if the user has permission to call the download_pdf and, if not, disables the view::

    /* Get the current administator */
    var user = DEPLOY.getAdministrator();

    /* Check if the user has permission to download the PDF */
    if (!user.can(ctr.getName(), 'download-pdf')) {
        /* User doesn't have permission so disable the button */
        downloadBtn.enable(false);
    }

For the code above to work, you have to assume that the ctr variable contains a reference to the web order controller, and the downloadBtn variable a reference to our button. Assumptions made, let's look at the first line of code; DEPLOY.getAdministrator() is an important function call if you're checking permissions because it allows you to get an instance to the current administrator.

You can then query the current user instance to see if they have permission to call a view using user.can(controllerName, viewName). The permissions you set using your Python decorators automatically generate a permissions table for the client-side administrator instance which we can test against.

Now that we know if the user can or cannot download a PDF invoice, we can disable the download button if necessary by calling the enable() method with false.

Creating editable pages
^^^^^^^^^^^^^^^^^^^^^^^

One of the neat features of Deploy is the ability to make your pages editable in place. Whilst in edit mode you can browse between the pages of your website like normal. When you arrive at a page you want to edit, clicking the Edit page button on the application bar will allow you to begin changing its content. If the Edit page button is greyed out, that means it's disabled and that the page you're looking at has no editable content defined within it. For example a product listing page would be unlikely to contain content that can be edited in place.

You can use the Deploy in place editing library to make almost anything editable. The most common thing, and the one available by default, is pages. Editable pages can be created by selecting Site > Content > Pages from the application bar and clicking the Add page button; to create a page you need to select a template to create it from. This section is about how we build and edit those templates.

.. note::
    Unlike most WYSIWYG and in-place editors, Deploy's editable library ensures only valid and clean HTML is created, avoiding the pitfalls associated with using execCommand, where creating a bold tag might instead be interpreted as <span style=”font-weight: bold;”>...</span>.

A 'Hello World' template
^^^^^^^^^^^^^^^^^^^^^^^^

We have to start somewhere so let's start simple, we'll create the most basic of pages with a heading 'Hello World' that can be edited. Create a template by selecting Site > Layout > Templates from the application bar, then clicking Add template. Name the template anything you like, choose Jinja as the language (it doesn't actually matter as our template won't contain any Jinja), and then enter the following code into the Source code field::

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>Hello World example</title>
        <meta charset="utf-8" />
    </head>
    <body data-deploycms="init">
        <div id="my-content" class="edit-local">
            <h1 class="edit-class-Text">Hello World</h1>
        </div>
    </body>
    </html>

Take a look at the template code we've defined above. It looks just like normal HTML5 right? That's because it is. There are however three things that make this page different – which make it editable.

The body has the attribute data-deploycms="init" against it. This tells Deploy that the page should show the edit environment. This attribute is not unique to editable pages, but will be defined against any page that should show a user interface when viewed in edit mode. By adding this attribute to the page body tag, you instruct Deploy to automatically include the necessary JavaScript and CSS for the edit environment.

Within the body tag is a div tag with an ID of my-content and a class of edit- local; this tells Deploy that content within this division is editable. An editable area of the page is known as a collection. There are two possible classes that can be used to define a collection, edit-local and edit-global. The postfix of local or global defines the scope. Changes to an edit-local collection are limited to that page, whereas changes to an edit-global collection will effect that page and any other pages where a edit-global collection is defined with an ID.

You must define an id for any editable collection, and the ID must be unique. If you don't, you'll get a pop-up alert informing you that you haven't done this when you try to edit the page.

Only use open block level elements to define for editable collections. By open I mean that they shouldn't have rules about what type of elements can be placed within them, so for example don't use lists, definition lists, or tables.

On their own, the following two points will give you an editable area on your page, but it's always nice to define some placeholder content. To do this we've added an H1 tag within our editable div with the content 'Hello World'. We've marked our H1 tag with the class edit-class-Text. This tells the editor that the content within the H1 tag should be managed as text. Each type of content that is supported by the editor has its own class; the table below provides an overview of the supported content types and their classes.

===============================================  =============
Content type                                     CSS class
===============================================  =============
Text                                             edit-class-Text
Image                                            edit-class-Image
List > List item                                 edit-class-List, edit-class-ListItem
Table > Table section > Table row > Table cell   edit-class-Table, edit-class-Caption, edit-class-TableSection, edit-class- TableRow, edit-class-TableCell
Rule                                             edit-class-Rule
Video                                            edit-class-Video
===============================================  =============

Any content you markup within a collection must be given a content type class via its class attribute. You can markup any type of content you want within the template with the exception of images. Images cannot be used as placeholder content within templates as they require a reference to an image on the database which must be unique to the collection.

What follows is a list of examples showing how to define each type of editable content as placeholder, excluding images.::

    <!-- Text examples -->
    <h1 class="edit-class-Text">Any heading tag can be used...</h1>
    <h6 class="edit-class-Text">From h1-h6!</h6>
    <p class="edit-class-Text">You can also add in-line tags within the
    text, such as <strong>strong</strong> and <em>emphasise.</em></p>
    <!-- List examples -->
    <ul class=”edit-class-List”>
        <li class=”edit-class-ListItem”>Apple</li>
        <li class=”edit-class-ListItem”>Orange</li>
        <li class=”edit-class-ListItem”>Peach</li>
    </ul>
    <ol class=”edit-class-List”>
        <li class=”edit-class-ListItem”>
            Sub-lists <strong>are</strong> also supported.
            <ul class=”edit-class-List”>
                <li class=”edit-class-ListItem”>I'm a sub item!</li>
            </ul>
        </li>
    </ol>
    <!-- Table example -->
    <table class=”edit-class-Table”>
        <caption class=”edit-class-Caption”>Product prices</caption>
        <thead class=”edit-class-TableSection”>
            <tr class=”edit-class-TableRow”>
                <th class=”edit-class-TableCell”>
                    Product
                </th>
                <th class=”edit-class-TableCell”>
                    Cost
                </th>
            </tr>
        </thead>
        <tbody class=”edit-class-TableSection”>
            <tr class=”edit-class-TableRow”>
                <td class=”edit-class-TableCell”>Bike </td>
                <td class=”edit-class-TableCell”>
                    $40
                </td>
            </tr>
            <tr class=”edit-class-TableRow”>
                <td class=”edit-class-TableCell”>
                    Helemet
                </td>
                <td class=”edit-class-TableCell”>
                    $150
                </td>
            </tr>
        </tbody>
        <!-- You can also add a table footer (tfoot) section -->
    </table>

    <!-- Rule example -->
    <div class=”edit-class-Rule”></div>

    <!-- Video -->
    <a
        href=”http://www.youtube.com/watch?v=LdkcsDueSMM”
        style=”width: 400px; height: 300px”
        class=”edit-class-Video”
        >Twinings Gets You Back To You</a>

Most of the examples are pretty straightforward. However, two of them; Rule and Video might have stood out. Rule because we're using a div tag and not an hr tag, and video because it's represented solely by an a tag.

The reason that we use a div tag and not an hr tag for rules is simply that div tags are easier to style with CSS. Videos on the other hand are represented as using an a tag as this provides a non-visual and search engine friendly way for the content to be presented to non JavaScript-capable browsers.

.. note::
    If you want to support videos on your website, you'll need to create a script that can convert the contents of your links into an embedded video player at the point the page loads. A basic script that handles YouTube URLs, such as the one in the example video, is available from the repo.

Adding style and identifying elements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You have complete freedom in the styles and IDs you can apply to tags within an editable collection.

The standard editable CSS mostly provides styles for when elements are being interacted with, such as being dragged from one location to another. However there are 3 special case styles for the alignment of elements; these are:

- edit-align-left
- edit-align-center
- edit-align-right

For text elements these styles simply alight the content left, center or right. For images and videos, these styles float the elements left, right or clear – positioning centrally on the page. The reason these CSS classes are predefined is that they are heavily used by the editable JavaScript libraries. If you wanted to create some placeholder text with an image aligned to the right of it you would use the edit-align-right tag like so (I should be on commission from Twinings)::

    <a
        href=”http://www.youtube.com/watch?v=LdkcsDueSMM”
        style=”width: 400px; height: 300px”
        class=”edit-class-Video edit-align-right”
        >Twinings Gets You Back To You</a>
    <p class=”edit-class-Text”>
    For some, breakfast is bacon and eggs. Others like to tuck in to a
    bowl of cereal. But most will agree there's nothing like a good cup
    of tea. Whatever you choose to go on your plate, this is the perfect
    tea to go in your morning mug. They say breakfast's the most
    important meal of the day. We say this tea's the most important
    drink. Discover the art of tea with Twinings.
    </p>

Outside of the alignment classes, you're free to apply any other class you like to the elements within your collection. You can also assign IDs which can be useful for local page links and selection in JavaScript.

Although so far we have been discussing placeholder content templates, the same applies for actual pages. Once a page has been created, and even after it's been edited using the in-place editor, you can view and modify its HTML directly.

The last feature I want to look at is inline tags within editable text elements. You can add inline tags such as a span or cite tag directly into text elements, even if the tags themselves are not supported by the in-place editor. For example, you can write::

    <p class=”edit-class-Text”>
    <em>For some, breakfast is bacon and eggs.</em> Others like to tuck
    in to a bowl of cereal. But most will agree there's nothing like a
    good cup of tea. Whatever you choose to go on your plate, this is
    the perfect tea to go in your morning mug. They say breakfast's the
    most important meal of the day. We say this tea's the most important
    drink. <span class=”twinings-blue”>Discover the art of tea with
    Twinings.</span>
    </p>

The user will still be able to modify this text and update content inside (and out) of the inline tags, despite the fact they can't directly insert them themselves. In the near future, support will be added to the allow users to add their own classes to elements and insert span tags for inline styling. Whilst this support doesn't exist currently, in the meantime the manual route is one way around this limitation.

To finish up this section I want to point out a few of the common pitfalls I've come across;

1. Dreaded flash. This isn't just restricted to editable content, if you embed a flash movie into your page then you need to set the wmode parameter. For example::

    <object> ...
       <param name="WMode" value="opaque" />
       <embed src="...” wmode=opaque" wmode="opaque">
    </object>

If you don't then the flash movie will appear on top of everything on the page, no matter what z-index value you specify.

1. Be careful when applying custom CSS to elements, especially site- wide. If the rule value is too high you may inadvertently override the editable styles applied when users interact with the page. If not planned, that might make the page difficult to edit. In some cases there are good reasons to customise the editable CSS directly; a common scenario is when the website uses a light font colour that is difficult to read when the default selected background is applied by the in-place editor.

1. Elements must have an edit class. I often see people wrap a block of HTML in a div, mark it as edit-local, and then add editable classes to just the elements they want the user to be able to change, for example::

    <div id=”my-content” class=”edit-local”>
       <h1 class=”edit-class-Text”>
           Text I want the user to edit.
       </h1>
       <div>
           <img src=”...” alt=”A banner I don't want changed” />
      </div>
       <p class=”edit-class-Text”>
           Some more text I want the user to edit.
       </p>
    </div>

This wont work; you'll simply get an error due to the div tag that has no class. Instead you can solve this problem like so::

    <div id=”my-content-top” class=”edit-local”>
       <h1 class=”edit-class-Text”>
           Text I want the user to edit.
       </h1>
    </div>
    <div>
        <img src=”...” alt=”A banner I don't want changed” />
    </div>
    <div id=”my-content-bottom” class=”edit-local”>
       <p class=”edit-class-Text”>
           Some more text I want the user to edit.
       </p>
    </div>

We solve the problem by splitting the solution into two editable collections: my-content-top and my-content-bottom.

From this rather brief introduction to editable templates and pages, hopefully I've demonstrated just how easy it is to make content editable, and given you some insight into the possibilities to apply this flexibility to any HTML-based content.

Advanced in-page editing options
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are a number of advanced options you can apply to your editable collections. The options are applied using data-edit- attributes against the collections tag, for example::

    <div id=”my-content” class=”edit-local” data-edit-editor=”basic”>
        ...
    </div>

These attributes are split into two groups, the first group allows you to modify the behaviour of the collection for editors, the second group is used to provide in-page editing support for custom models (anything that isn't a page). I've listed each of the options in the table below along with a short description of their purpose:

**BEHAVIOUR OPTIONS**

======================  ================
Attribute               Purpose
======================  ================
data-edit-editor        Sets the editor that will be available when a user edits the collection (defaults to 'default'). Different editors allow you to set what tools are available when editing that region. For example the basic editor will limit the tools for a region to just bold, italic, and insert/break links. The editors available as standard at the time of writing this are just default and basic, however setting up additional editors is very straight forward (skip to the end of this table to see how).

data-edit-no-sharing    Prevent elements from the collection being dragged to other collections, or receiving elements from other collections. By default collections allow you to drag elements to different collections, so you could drag an image in one collection to an image in another collection. Note: A good reason to use this attribute is if you have editable collections on the page that update different records, in that scenario you should not allow collections that contain images to be shared.

data-edit-no-add        Prevents new elements being automatically created when the user hits return or enter.
data-edit-no-remove     Prevents existing elements being removed when the user removes all the content from it.
data-edit-no-move       Prevents elements being moved by dragging them to a new position within the collection.
======================  ================

Creating a custom editor is very straight forward, we recommend that you put custom editors in your base.js file for the website. To define a new editor you simply specify a name and list of tools you would like it to support, below is an example of an editor that supports text only tools::

    new DEPLOY.Editor({
        name: 'tools-only',
        toolGroups: [
            [_('Tools'), [
                'strong', 'em', '|', 'insert-a', 'delete-a'
                '-',
                'align-left', 'align-center', 'align-right', '|', 'h1',
                'h2', 'h3', 'paragraph', '|', 'br',
            ]]
        ]
    });

Tools are organised into groups (which show as tabs in the editor tool window), so tool groups is a list. Each group is a two item list with the title of the group first, followed by a list of tools that will appear in that group. To help further organise the groups you can use the '-' and '|' characters to insert horizontal and vertical dividers between tools.

**The following tools are available by default:** strong, em, insert-a, delete-a, insert-image, insert-video, align-left, align-center, align-right, h1, h2, h3, paragraph, hr, br, ol, ul, indent, unindent, insert-table, th, delete.

**MODEL OPTIONS**

=========================  ===================
Attribute                  Purpose
=========================  ===================
data-edit-id               Allows you to specify the ID of the model to be updated. You only need to specify this attribute if there is no default ID (for example the page shows a list of models not a single model), or if the collection is not related to the default ID.

data-edit-store-content    Required for in-page editing of non- Page models. Specifies which of the model's properties the contents of the collection will be stored against.

data-edit-store-images     Required for in-page editing of non- Page models where images are supported. Specifies which of the model's properties the images inserted into the collection will be stored against.

data-edit-controller       By default the controller used to manage saving the collection's content will be the current page controller. This setting allows you to override that behaviour which may be required if your supporting for editing multiple model types on one page.
￼￼
data-edit-view             By default changes to a collection are sent to the inpage-sync controller view, setting this attribute allows you to specify a different view.
=========================  ===================

Themes
------

In the final section of this guide we're going to look at themes, and how you can use them to change the appearance of the interface. One of the major features of Deploy is you can change the appearance, even the underlying structure, of absolutely everything in the GUI.

The default GUI theme for Deploy is the sw-theme. Themes are made up of a list of CSS, resource, template and extension files; the table below describes the role of each of the file types::

=============  ===========  ===============
Type           Format       Role
=============  ===========  ===============
CSS            CSS          The CSS files are used to add style to the interface widgets. There's a lot of CSS for an interface. The default theme breaks its CSS down into three files: ui.css, ui.forms.css, deploycms.css and editable.css.

Resource       JavaScript   The resource files define a list of resources such as icons. The resources are each given a name, which allows you to overwrite the default resources and add new ones (which in turn can be overwritten by others).

Template       JavaScript   The template files are responsible for defining the underlying HTML structure of interface widgets. By default all widgets have a base template, which you can overwrite in the theme.

Extension      JavaScript   Extension files allow you to change the default layout behaviour of widgets by adding your own JavaScript before and/or after a widget's layout code is called. It is also possible to prevent the default layout code from ever being called.
=============  ===========  ===============

.. note::
    Some of you might already have realised that, given the ability to modify the behaviour of a widget's layout, you could use this flexibility for other purposes, such as binding additional events to the widget or triggering events for other widgets. We strongly advise against this practise. If you need to change behaviour other than layout, look back to the first section: 'Customising the interface'.

Creating a theme
^^^^^^^^^^^^^^^^

If you take a look at the files that make up the Snailwhale theme, especially the CSS files, you might well start to think “Holy fricassee, it's a lot of work to build my own UI”, and it is, but you don't need to build your own UI in order to theme it.

Themes can be mixed together in a cascading format similar to CSS where if two rules are the same, it's the last one that gets applied. By mixing themes together you can simply add new files to modify an existing theme and create your own look and feel. We're going to create our own theme for our fictional company Wozzy in which we'll change the Snailwhale logo and add a custom tag to the bottom of all windows.

We're going to need to create four files for our new theme, def.js, ui.css, templates.js, and extensions.js. We'll also add a folder named wozzy-theme somewhere in our web root to put them in. The exact paths will vary with your setup, but for my standard setup they would now be;

- /media/edit/themes/wozzy-theme/def.js
- /media/edit/themes/wozzy-theme/ui.css
- /media/edit/themes/wozzy-theme/templates.js
- /media/edit/themes/wozzy-theme/extensions.js

Now we've created our empty theme, we need to define it and tell Deploy to use it. Themes are defined in JS; to define our theme add the following code to the def.js file::

    // Register the default theme
    DEPLOY.registerTheme(new FLUID.Theme({
        name: 'wozzy-theme',
        css: [
            '/media/edit/themes/sw-theme/ui.css',
            '/media/edit/themes/sw-theme/ui.forms.css',
            '/media/edit/themes/sw-theme/deploycms.css',
            '/media/edit/themes/sw-theme/editable.css',
            '/media/edit/themes/wozzy-theme/ui.css'
            ],
        resources: ['/media/edit/themes/sw-theme/resources.js'],
        templates: [
            '/media/edit/themes/sw-theme/templates.js'
            '/media/edit/themes/wozzy-theme/templates.js'
            ],
        extensions: [
            '/media/edit/themes/sw-theme/extensions.js',
            '/media/edit/themes/wozzy-theme/extensions.js'
        ]
    }));

You'll notice in the code above we're using the sw-theme files as the base of our new theme and adding our new theme files to the end of their respective list.

The DEPLOY.registerTheme function is used to register themes so that they can be accessed later using the DEPLOY.getTheme function. When the page loads, the JavaScript will call the remote server and ask what theme it should use. The server will return a theme name, which currently will be the default sw-theme. Let's change it to wozzy-theme.

We need to make two server-side Python changes to register and use our new theme. First we have to add the def.js files where our theme is defined to the resources Deploy will load. If you don't already have a resources.py file in the Python folder for the website, create one and add the following code to it::

    from deploy.core.resources import ResourceList
    from deploy.core.resources.types import JSResource

    CMS_JS_RESOURCES = ResourceList(
        JSResource('/media/edit/themes/wozzy-theme/def.js')
    )

.. note::
    If the file already exists just insert the JSResource instance for def.js.

Now when a page loads in edit mode our theme will be defined. Next we need to set the default theme for the website to wozzy-theme. Open the settings.py file in the Python folder for the website and add/change::

    CMS_THEMES = ['wozzy-theme', 'sw-theme',]

This settings change means that the wozzy-theme is now the default. We could simply remove the sw-theme theme from the equation; by keeping it we allow users to choose the original theme instead if they'd prefer. This may not be the behaviour you want so if you'd prefer users to only use your theme simply make it the only theme in the list.

.. note::
    Restart deployserver after making the Python changes.

Switching the logo for our own
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

With the theme designed and set as the default we can make our first change by swapping out the logo for our own.

Add the following code to **/media/edit/themes/wozzy-theme/ui.css**::

    /* Section: Application bar */
    #fluid-ns-outer #fluid-ns-inner .fluid-application-bar {
        background:
            url('/media/edit/themes/wozzy-theme/logo.png')
            0 0 no-repeat,
            #3A3E41 url('/media/edit/themes/sw-theme/ui.png')
            0 -107px repeat-x;
    }

I'm assuming that you have created and saved a logo.png image file of the appropriate size. The CSS we've just added overwrites the default background images for the application bar. We keep the primary background as before, but change the top-level background style to contain our logo.

You'll notice that the CSS rule starts with two Ids: #fluid-ns-outer #fluid-ns- inner. The double definition boosts the rules weight to ensure that it is more important than those subsequently defined in the website's local CSS. You should always start your CSS rules like this.

.. note::
    You'll find that browsers can be a little possessive of files that are loaded as part of your theme; refreshing alone doesn't always force the browser's cache to let go. I find the best solution to this problem is to visit the file you've changed directly and refresh it. After that the browser will normally reload the file on future page refreshes.

Branding our windows
^^^^^^^^^^^^^^^^^^^^

That was fairly easy, now let's look at something more complicated like changing the template and layout behaviour of a widget. We're going to add a small ribbon with our logo in it, to the bottom right of every window. To do that we'll need to add a div tag to our window template, style the element to ensure it's always positioned bottom right and then adjust the behaviour so it isn't visible on tool windows.

We'll start with the template changes; add the following code to **/media/edit/themes/wozzy-theme/templates.js**::

    var fluidTemplates = {
        '/media/edit/themes/wozzy-theme/templates.js': {
            'FLUID.Window':
            '<div id="<%=widget.getHTMLElementId()%>">' +
               '<div class="fluid-widget fluid-title">' +
                    '<div class="fluid-widget fluid-tools">' +
                        '<span class="fluid-minimize fluid-icon" +
                            "title="Minimize"></span><span class="fluid-maximize fluid-icon" +
                            "title="Maximize"></span><span class="fluid-close fluid-icon" +
                            "title="Close"></span>' +
                    '</div>' +
                    '<span class="fluid-icon"></span><span class="fluid-label"><%=widget.label()%></span>' +
                '</div>' +
                '<div class="fluid-info <%=FLUID_GROUP_PREFIX%>info">' +
                    '<%=group_info%>' +
                '</div>' +
                '<div class="fluid-client <%=FLUID_GROUP_PREFIX%><%=FLUID_DEFAULT_GROUP%>">' +
                    '<%=group_' + FLUID_DEFAULT_GROUP + '%>' +
                '</div>' +
                '<div class="fluid-actions <%=FLUID_GROUP_PREFIX%>actions">' +
                    '<%=group_actions%>' +
                '</div>' +
                '<div class="fluid-widget fluid-resize"></div>' +
                '<div class="wozzy-ribbon"></div>' +
            '</div>'
        }
    };

Geez that's a lot of code! Well yes, the window widget is pretty much the most complex of widgets, but all you really need to take away from our template is the last but one line in our template: '<div class="wozzy-ribbon"></div>'. This is the div element that will represent our new logo tag.

You'll notice that the HTML template has a number of non-HTML tags within it, such as <%=widget.label()%>. These tags drag dynamic content into the template when it is used to build the widget, for example in this case the widget's label. It's beyond the scope of this guide to discuss them in detail, but at least now they shouldn't be totally alien to you.

When loading a template file, Deploy's theme manager looks for the variable fluidTemplates, before checking for a key within it that matches the file's URL. If it finds one it will update its internal template store with the templates defined. Templates are defined by referencing the widget class in the key, in this case FLUID.Window followed by the new template.

.. note::
    Resource, template and extension definitions must be keyed using the file's URL. If the URL used as a key doesn't match the URL used to load the file then the definitions will not be read. The definitions are keyed like this to ensure they are loaded and applied in the order intended.

From now on, when fluid creates windows it will use our template and include the additional div element with a class of wozzy-ribbon. An empty div tag isn't going to display, so we need to style it to look like our ribbon. Add the following code to **/media/edit/themes/wozzy-theme/ui.css**::

    /* Section: Window */
    #fluid-ns-outer #fluid-ns-inner .fluid-application-bar {
        background:
            url('/media/edit/themes/wozzy-theme/ribbon-logo.png')
            0 0 no-repeat;
        bottom: 50px;
        height: 20px;
        position: absolute;
        right: -10px; /* We'll make it stick out a little*/
        width: 40px;
    }

With the CSS in place, our windows will now proudly display our wozzy ribbon bottom right. There's one last thing to take care of; we don't want the tag to show in tall windows.

.. note::
    Technically you could hide the ribbons using CSS and the fluid- minimum-border class, but that wouldn't help with our example so persevere please.

Add the following code to **/media/edit/themes/wozzy- theme/templates.js**::

    var fluidExtensions = {
        '/media/edit/themes/wozzy-theme/extensions.js': {
            'FLUID.Window': function (phase) {
                if (phase === FLUID.Widget.LAYOUT_PHASE_ON) {
                    if (this.exists()) {
                        var ribbon = this.getHTMLSelection().find('.wozzy-ribbon');
                        if ((FLUID.Window.HAS_MINIMUM_BORDER & this.flags()) ===
                                FLUID.Window.HAS_MINIMUM_BORDER) {
                            ribbon.hide();
                        } else {
                            ribbon.show();
                        }
                    }
                }
            }
        }
    }

Just like with templates, we have to create a variable that the theme manager is aware of; in this case fluidExtensions, and define our extensions against our URL as a key. We then define a function against for the window class. The function will be called twice every time a window calls its update method. The first call will be before any of the code within update has run, and the second after all the code in update has run. The two calls are referred to as phases – like events these are known as on and after respectively.

Our first line of code checks is an if test, phase === FLUID.Widget.LAYOUT_PHASE_ON, to make sure we only run our code to the first call to our function. The next line checks to see if the widget exists. This is good practice; even though our code would run harmlessly if the window didn't exist, it's more efficient not to run the extra code.
If the widget exists, and we're running in the on phase, then next we find our ribbon element using its class this.getHTMLSelection().find('.wozzy-ribbon') and check to see if the window has the minimum border flag set. If it does we hide the ribbon element, otherwise we show it.

